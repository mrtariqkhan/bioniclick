<?php $submit_name                  = empty($submit_name) ? "Save" : $submit_name; ?>
<?php $method_name                  = empty($method_name) ? "post" : $method_name; ?>
<?php $form_class_name              = empty($form_class_name) ? "" : $form_class_name; ?>
<?php $cancel_attributes            = empty($cancel_attributes) ? array() : $cancel_attributes->getRawValue(); ?>
<?php $do_show_common_error_message = empty($do_show_common_error_message) ? false : $do_show_common_error_message; ?>

<form
    class="form <?php echo $form_class_name; ?>"
    action="<?php echo $action_url ?>"
    method="<?php echo $method_name ?>"
    <?php if ($form->isMultipart()): ?>
        <?php echo 'enctype="multipart/form-data"'; ?>
    <?php endif; ?>
>
    <?php if ($form->hasErrors()):?>
        <div class="flash">
            <div class="message error">
                <?php if ($do_show_common_error_message) : ?>
                    <?php echo 'Error! Form has not been saved.'; ?>
                <?php endif; ?>
                <?php if ($form->hasGlobalErrors()):?>
                    <ul>
                        <?php foreach ($form->getGlobalErrors() as $name => $error): ?>
                            <?php //$g_error_text = !empty($name) ? "$name: $error" : $error; ?>
                            <li>
                                <?php //echo $g_error_text; ?>
                                <?php echo $error; ?>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif;?>
            </div>
        </div>
    <?php endif;?>

    <?php foreach ($form as $field): ?>
        <?php if (!$field->isHidden()): ?>
            <?php $field_type = FormPartial::fieldType($field); ?>
            <?php if ($field_type == 'input'): ?>
                <?php if ($field->getWidget()->getOption('type') == 'text'): ?>
                    <div class="input text">
                        <?php echo $field->renderLabel(null); ?>
                        <?php echo $field->renderHelp(); ?>
                        <?php echo $field->render(); ?>
                        <?php if ($field->hasError()): ?>
                            <div class="error">
                                <?php echo $field->renderError(); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php elseif ($field->getWidget()->getOption('type') == 'checkbox'): ?>
                    <div class="input checkbox">
                        <?php echo $field->renderLabel(null, array('class' => 'label')); ?>
                        <?php echo $field->renderHelp(); ?>
                        <?php echo $field->render(); ?>
                        <?php if ($field->hasError()): ?>
                            <div class="error">
                                <?php echo $field->renderError(); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php elseif ($field->getWidget()->getOption('type') == 'password'): ?>
                    <div class="input password">
                        <?php echo $field->renderLabel(null, array('class' => 'label')); ?>
                        <?php echo $field->renderHelp(); ?>
                        <?php echo $field->render(); ?>
                        <?php if ($field->hasError()): ?>
                            <div class="error">
                                <?php echo $field->renderError(); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php elseif ($field->getWidget()->getOption('type') == 'image'): ?>
                    <div class="input image">
                        <?php echo $field->renderLabel(null, array('class' => 'label')); ?>
                        <?php echo $field->renderHelp(); ?>
                        <?php echo $field->render(); ?>
                        <?php if ($field->hasError()): ?>
                            <div class="error">
                                <?php echo $field->renderError(); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php elseif ($field->getWidget()->getOption('type') == 'file'): ?>
                     <div class="input file">
                        <?php echo $field->renderLabel(null, array('class' => 'label')); ?>
                        <?php echo $field->renderHelp(); ?>
                        <?php echo $field->render(); ?>
                        <?php if ($field->hasError()): ?>
                            <div class="error">
                                <?php echo $field->renderError(); ?>
                            </div>
                        <?php endif; ?>
                    </div>
                <?php endif; ?>
            <?php elseif ($field_type == 'select'): ?>
                 <div class="input select">
                    <?php echo $field->renderLabel(null, array('class' => 'label')); ?>
                    <?php echo $field->renderHelp(); ?>
                    <?php echo $field->render(); ?>
                    <?php if ($field->hasError()): ?>
                        <div class="error">
                            <?php echo $field->renderError(); ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php elseif ($field_type == 'textarea'): ?>
                 <div class="input textarea">
                    <?php echo $field->renderLabel(null, array('class' => 'label')); ?>
                    <?php echo $field->renderHelp(); ?>
                    <?php echo $field->render(); ?>
                    <?php if ($field->hasError()): ?>
                        <div class="error">
                            <?php echo $field->renderError(); ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php elseif ($field_type == 'radio_list'): ?>
                <div class="fieldset radio_list">
                    <?php echo $field->renderLabel(null, array('class' => 'label')); ?>
                    <?php echo $field->renderHelp(); ?>
                    <?php echo $field->render(); ?>
                    <?php if ($field->hasError()): ?>
                        <div class="error">
                            <?php echo $field->renderError(); ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php elseif ($field_type == 'checkbox_list'): ?>
                <div class="fieldset checkbox_list">
                    <?php echo $field->renderLabel(null, array('class' => 'label')); ?>
                    <?php echo $field->renderHelp(); ?>
                    <?php echo $field->render(); ?>
                    <?php if ($field->hasError()): ?>
                        <div class="error">
                            <?php echo $field->renderError(); ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php elseif ($field_type == 'datetime'): ?>
                 <div class="input datetime">
                    <?php echo $field->renderLabel(null, array('class' => 'label')); ?>
                    <?php echo $field->renderHelp(); ?>
                    <?php echo $field->render(); ?>
                    <?php if ($field->hasError()): ?>
                        <div class="error">
                            <?php echo $field->renderError(); ?>
                        </div>
                    <?php endif; ?>
                </div>
            <?php else: ?>
                <div class="group">
                   <?php echo $field->renderLabel(null, array('class' => 'label')); ?>
                    <?php echo $field->renderHelp(); ?>
                   <?php echo $field->render(array('class' => 'text_field')); ?>
                   <?php if ($field->hasError()): ?>
                       <div class="error">
                           <?php echo $field->renderError(); ?>
                       </div>
                   <?php endif; ?>
                </div>
            <?php endif; ?>
        <?php endif;?>
    <?php endforeach;?>

    <div class="input hidden">
        <?php echo $form->renderHiddenFields(); ?>
        <?php if (method_exists($form, 'getObject') && !$form->getObject()->isNew()): ?>
            <input type="hidden" name="sf_method" value="PUT" />
        <?php endif; ?>
    </div>

    <div class="input button">
        <input type="submit" value="<?php echo $submit_name; ?>" class="button"/>

        &nbsp;

        <?php if (!empty($delete_url)) : ?>
            <?php echo link_to('Delete', $delete_url, array('method' => 'delete', 'confirm' => 'Are you sure?')); ?>
        <?php endif; ?>

        <?php if (!empty($back_url)): ?>
           &nbsp; or &nbsp;
           <?php echo link_to('Cancel', $back_url, $cancel_attributes); ?>
        <?php endif; ?>
    </div>
</form>
