<html>
    <head>

    </head>
    <body>
        <p>
            Hello, <?php echo $creator; ?>!
        </p>
        <p>
            
            Your phone number order for your Bionic Click Call Tracking services
            has now been fulfilled. You can now log in to the Call Tracking 
            Dashboard and assign them to a campaign or hold them in your Number 
            Pool for future use.
        </p>
        <p>
            Thank you for your order!
        </p>
        <p>
            The Bionic Click Team
        </p>
        <p style="font-style: italic">
            Please note: this email has been automatically generated. Do not hit reply. If you need further assistance, please contact your support system administrator.
        </p>
    </body>
</html>

