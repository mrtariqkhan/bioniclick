<?php $flash_name   = isset($flash_name) ? $flash_name : 'message'; ?>
<?php $class        = isset($class) ? $class : "ajax-{$flash_name}-global"; ?>

<?php if ($sf_user->hasFlash($flash_name)) :?>
    <div class="<?php echo $class; ?>">
        <p>
            <?php echo $sf_user->getFlash($flash_name); ?>
        </p>
    </div>

    <?php $sf_user->setFlash($flash_name, null); ?>
<?php endif; ?>
