<?php if (count($legend_buttons)): ?>
    <ul class="icon-links-legend">
        <?php foreach ($legend_buttons as $key => $icon): ?>
             <li class="<?php echo $icon['class']?>">
                 <?php echo $key; ?>
             </li>
        <?php endforeach; ?>
    </ul>
<?php endif; ?>

<script>
var legends = $('ul.icon-links-legend:not(*[upperLegend])');

var table = $('table.table').each(function(key){
    
    if ($(this).data('legend'))
        return;
    
    $(this).data('legend', true);
    
    if ( $(this).find('tr').length < 15 )
        return;
    
    $(this).parent().before( legends.eq(key).clone().attr('upperLegend',true) );
});
</script>
