<?php if ($pager->getNbResults() > 0): ?>
    <?php 
        echo 'Displaying results ' . $pager->getFirstIndice() . 
            ' to ' . $pager->getLastIndice() . ' from ' . $pager->getNbResults();
    ?>
<?php else: ?>
    <?php echo $pager->getNbResults() . ' records found'?>
<?php endif; ?>
<?php if ($pager->getLastIndice() - $pager->getFirstIndice() > 15): ?>
    <?php echo html_entity_decode($pagerHtmlString); ?>
<?php endif; ?>

