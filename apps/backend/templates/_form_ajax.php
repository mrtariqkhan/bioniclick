<?php use_helper('Link'); ?>

<?php $submit_name          = isset($submit_name) ? $submit_name : "Save"; ?>
<?php $method_name          = empty($method_name) ? "post" : $method_name; ?>
<?php $form_class_name      = empty($form_class_name) ? "" : $form_class_name; ?>
<?php $form_special_data    = empty($form_special_data) ? '' : $form_special_data; ?>

<?php $cancel_attributes    = empty($cancel_attributes) ? array() : $cancel_attributes->getRawValue(); ?>
<?php $back_url_method      = isset($back_url_method) ? $back_url_method : 'POST'; ?>
<?php $back_url_type        = isset($back_url_type) ? $back_url_type : 'in-tab'; ?>
<?php $cancel_attributes_all= array_merge($cancel_attributes, array('method_type' => $back_url_method, 'back_url_type' => $back_url_type)); ?>

<?php $classErrorGlobalField = 'bionic-ajax-field-error-global bionic-ajax-field-error-global-hidden'; ?>
<?php $classErrorField = 'bionic-ajax-field-error bionic-ajax-field-error-hidden'; ?>



<?php //NOTE:: for embed forms of 1st level. ?>
<?php $fields = array(); ?>
<?php $schema = $form->getFormFieldSchema(); ?>
<?php for ($schema->rewind(); $schema->valid(); $schema->next()): ?>

    <?php $field = $schema->current(); ?>
    <?php if ($field instanceof sfFormFieldSchema): ?>

        <?php $schemaEmbed = $field; ?>
        <?php for ($schemaEmbed->rewind(); $schemaEmbed->valid(); $schemaEmbed->next()): ?>

            <?php $fieldEmbed = $schemaEmbed->current(); ?>
            <?php $fields[] = $fieldEmbed; ?>

        <?php endfor; ?>

    <?php else: ?>

        <?php $fields[] = $field; ?>

    <?php endif; ?>

<?php endfor; ?>




<form
    class="form <?php echo $form_class_name; ?>"
    action="<?php echo $action_url ?>"
    method="<?php echo $method_name ?>"
    <?php if ($form->isMultipart()): ?>
        <?php echo 'enctype="multipart/form-data"'; ?>
    <?php endif; ?>
    special_data="<?php echo $form_special_data; ?>"
>
    <div class="<?php echo $classErrorGlobalField; ?>"></div>

    <?php foreach ($fields as $field): ?>

        <?php if (!$field->isHidden()): ?>
            <?php $field_type = FormPartial::fieldType($field); ?>
            <?php if ($field_type == 'input'): ?>
                <?php if ($field->getWidget()->getOption('type') == 'text'): ?>
                    <div class="input text">
                        <?php echo $field->renderLabel(null); ?>
                        <?php echo $field->renderHelp(); ?>
                        <?php echo $field->render(); ?>
                        <div class="<?php echo $classErrorField; ?>"></div>
                    </div>
                <?php elseif ($field->getWidget()->getOption('type') == 'checkbox'): ?>
                    <div class="input checkbox">
                        <?php echo $field->renderLabel(null, array('class' => 'label')); ?>
                        <?php echo $field->renderHelp(); ?>
                        <?php echo $field->render(); ?>
                        <div class="<?php echo $classErrorField; ?>"></div>
                    </div>
                <?php elseif ($field->getWidget()->getOption('type') == 'password'): ?>
                    <div class="input password">
                        <?php echo $field->renderLabel(null, array('class' => 'label')); ?>
                        <?php echo $field->renderHelp(); ?>
                        <?php echo $field->render(); ?>
                        <div class="<?php echo $classErrorField; ?>"></div>
                    </div>
                <?php elseif ($field->getWidget()->getOption('type') == 'image'): ?>
                    <div class="input image">
                        <?php echo $field->renderLabel(null, array('class' => 'label')); ?>
                        <?php echo $field->renderHelp(); ?>
                        <?php echo $field->render(); ?>
                        <div class="<?php echo $classErrorField; ?>"></div>
                    </div>
                <?php elseif ($field->getWidget()->getOption('type') == 'file'): ?>
                     <div class="input file">
                        <?php echo $field->renderLabel(null, array('class' => 'label')); ?>
                        <?php echo $field->renderHelp(); ?>
                        <?php echo $field->render(); ?>
                        <div class="<?php echo $classErrorField; ?>"></div>
                    </div>
                <?php endif; ?>
            <?php elseif ($field_type == 'select'): ?>
                 <div class="input select">
                    <?php echo $field->renderLabel(null, array('class' => 'label')); ?>
                    <?php echo $field->renderHelp(); ?>
                    <?php echo $field->render(); ?>
                    <div class="<?php echo $classErrorField; ?>"></div>
                </div>
            <?php elseif ($field_type == 'textarea'): ?>
                 <div class="input textarea">
                    <?php echo $field->renderLabel(null, array('class' => 'label')); ?>
                    <?php echo $field->renderHelp(); ?>
                    <?php echo $field->render(); ?>
                    <div class="<?php echo $classErrorField; ?>"></div>
                </div>
            <?php elseif ($field_type == 'radio_list'): ?>
                <div class="fieldset radio_list">
                    <?php echo $field->renderLabel(null, array('class' => 'label')); ?>
                    <?php echo $field->renderHelp(); ?>
                    <?php echo $field->render(); ?>
                    <div class="<?php echo $classErrorField; ?>"></div>
                </div>
            <?php elseif ($field_type == 'checkbox_list'): ?>
                <div class="fieldset checkbox_list">
                    <?php echo $field->renderLabel(null, array('class' => 'label')); ?>
                    <?php echo $field->renderHelp(); ?>
                    <?php echo $field->render(); ?>
                    <div class="<?php echo $classErrorField; ?>"></div>
                </div>
            <?php elseif ($field_type == 'datetime'): ?>
                 <div class="input datetime">
                    <?php echo $field->renderLabel(null, array('class' => 'label')); ?>
                    <?php echo $field->renderHelp(); ?>
                    <?php echo $field->render(); ?>
                    <div class="<?php echo $classErrorField; ?>"></div>
                </div>
            <?php else: ?>
                <div class="group">
                    <?php echo $field->renderLabel(null, array('class' => 'label')); ?>
                    <?php echo $field->renderHelp(); ?>
                    <?php echo $field->render(array('class' => 'text_field')); ?>
                    <div class="<?php echo $classErrorField; ?>"></div>
                </div>
            <?php endif; ?>
        <?php endif; ?>

    <?php endforeach; ?>

    <div class="input hidden">
        <?php echo $form->renderHiddenFields(); ?>
        <?php if (method_exists($form, 'getObject') && !$form->getObject()->isNew()): ?>
            <input type="hidden" name="sf_method" value="PUT" />
        <?php endif; ?>
    </div>

    <div class="input button">
        
        <?php if ($submit_name) : ?>
        <input type="submit" value="<?php echo $submit_name; ?>" class="button"/>
        
        &nbsp;
        <?php endif; ?>
        
        <?php if (!empty($delete_url)) : ?>
            <?php
                echo link_to_ajax_delete(
                    'Delete',
                    $delete_url                 
                )
            ?>
        <?php endif; ?>

        <?php if (!empty($back_url)): ?>
           &nbsp; or &nbsp;
           <?php echo link_to('Cancel', $back_url, $cancel_attributes_all); ?>
        <?php endif; ?>
    </div>
</form>
