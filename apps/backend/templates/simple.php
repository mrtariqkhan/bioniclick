<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
    <head>
        <?php include_http_metas(); ?>
        <?php include_metas(); ?>
        <?php include_title(); ?>
<!--
        <link rel="shortcut icon" href="/images/shortcut-icon.ico" />
-->
        <?php include_stylesheets(); ?>
        <?php include_javascripts(); ?>
<!--[if IE]>
        <link rel="stylesheet" href="/css/project/blueprint/ie.css" type="text/css" media="screen, projection" />
<![endif]-->
<!--[if IE]>
        <script language="javascript" type="text/javascript" src="/js/project/excanvas.pack.js"></script>
<![endif]-->

    </head>
    <body>
        <?php echo $sf_content; ?>
    </body>
</html>
