<nav class="breadcrumb">

    <?php
        $user = $user->getRawValue();
        $breadCrumbs = $object->getChartsParentBreadCrumbs($user)->getRawValue();
        
        if (isset($title)) {
            $breadCrumbs[] = $object->getChartsBreadCrumb()->getRawValue();
        } else {
            $title = $object->getChartsBreadCrumb()->getTitle();
        }
    ?>
    <?php foreach ($breadCrumbs as $i => $breadCrumb): ?>
        <a
            href="<?php echo url_for($breadCrumb->getRouteName(), $breadCrumb->getRouteParams()); ?>"
            class="<?php echo $breadCrumb->getClassName(); ?>"
        >
            <?php echo $breadCrumb->getTitle(); ?>
        </a> →
    <?php endforeach; ?>
</nav>

<h1><?php echo $title; ?></h1>
