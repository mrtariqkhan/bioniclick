<?php $className = isset($className) ? $className : 'time-range'; ?>

<div class="<?php echo $className; ?>">
    <?php $date_info = $date_info->getRawValue(); ?>

    <?php if (isset($date_info['from'])): ?>
        <div class="<?php echo "$className-from"; ?>">
            <div class="<?php echo "$className-from-title"; ?>">
                From:&nbsp;
            </div>
            <div class="<?php echo "$className-from-value"; ?>">
                <?php echo $date_info['from']; ?>
            </div>
        </div>
    <?php endif; ?>

    <?php if (isset($date_info['to'])): ?>
        <div class="<?php echo "$className-to"; ?>">
            <div class="<?php echo "$className-to-title"; ?>">
                To:&nbsp;
            </div>
            <div class="<?php echo "$className-to-value"; ?>">
                <?php echo $date_info['to']; ?>
            </div>
        </div>
    <?php endif; ?>
</div>
