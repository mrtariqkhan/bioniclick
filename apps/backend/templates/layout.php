<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
    <head>
        <?php include_http_metas(); ?>
        <?php include_metas(); ?>
        <?php include_title(); ?>
<!--
        <link rel="shortcut icon" href="/images/shortcut-icon.ico" />
-->
        <?php include_stylesheets(); ?>
        <?php include_javascripts(); ?>
<!--
        <script type="text/javascript" src="https://getfirebug.com/firebug-lite.js"></script>
-->
        <script type="text/javascript" src="/js/libs/html2canvas/build/html2canvas.js"></script>
        <script type="text/javascript" src="/js/libs/html2canvas/build/jquery.plugin.html2canvas.js"></script>


        <script type="text/javascript">
            function init(){
                //load all available advertisers to this user
                Advertiser.assignTextboxesFilteringMenu();
                updateTitle();
                //disable main scroll for home page,   
                //and convert the top navigation menu position to be fixed
                // if($('.active a').attr('tab_code') != 'home')
                //     return;

                // $('html').css('overflow-y', 'hidden');
                // var height = (window.innerHeight - parseInt($('#header').css('height'))
                //     - parseInt($('#footer').css('height')) - 75);

                // $('#pageContent').css('height', height + 'px');
                // $('#pageContent').css('width', (parseInt($('#main-navigation').css('width')) - 20) + 'px');
                // $('#pageContent').css('overflow-y', 'auto');

                // $('#footer').css('top', 500 + 'px');

            }
            function updateTitle(){
                var companyTitle = "<?= $sf_user->getGuardUser()->getCompany()->getTitle()?>";
                $('meta[name=title]').attr('content', companyTitle);
                $('title').html(companyTitle);
            }
            function snapshot(){
                $('body').css('cursor', 'progress');
                var snapshotTarget = $('#wrapper')[0];
                if($('.active a').attr('tab_code') == 'home')
                    snapshotTarget = $('#charts')[0];

                html2canvas( [snapshotTarget], {
                    onrendered: function( canvas ) {
                        var dataURL = canvas.toDataURL("image/png");
                        var base64 = dataURL.replace(/^data:image\/(png|jpg);base64,/, "") ;
                        var url = "/snapshot/export";
                        $.ajax({
                            type: 'POST',
                            url: url,
                            data: {img: base64},
                            success: function(response){
                                if($.parseJSON(response)['status'] == 'success'){
                                    // window.location = '/snapshot/download';
                                    window.location = '/snapshot';
                                }
                                $('body').css('cursor', 'auto');
                            },
                        });

                    }
                });
            }
        </script>
    </head>
    <body onload='init();'>
        <div id="container">
            <div id="header">
                <div class="logo">
                    <a href="<?php echo url_for("@homepage");?>">&nbsp;</a>
                </div>
                <div id="user-navigation">
                    <ul>
                        <li>
                            <?php echo $sf_user->getGuardUser(); ?>
                        </li>
                        <li>
                            <a class="logout" href="<?php echo url_for("@signout");?>">
                                Logout
                            </a>
                        </li>
                        <li>
                            <a id="snapshot" href="javascript:snapshot();">Snapshot</a>
                        </li>
                    </ul>
                </div>
                <div id="main-navigation">
                    <?php include_component('tabs', 'mainTabs', array()); ?>
                </div>
            </div>


            <?php $hasleftBar = has_slot('leftBar');?>
            <div id="wrapper" class="<?php echo $hasleftBar ? 'with-sidebar' : 'without-sidebar'; ?>">
                <div id='send-mail-container'></div>
                <div id="main">
                    <div id='pageContent' class="content" style="border:1px solid #F00;">
                        <?php echo $sf_content;?>
                        <div class="clear"></div>
                    </div>
                </div>

                <?php if ($hasleftBar): ?>
                    <div id="sidebar">
                        <div class="block sidebar-block-content-first">
                            <div class="sidebar-block" id="sidebar-content">
                                <?php include_slot('leftBar'); ?>
                            </div>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="clear"></div>
            </div>


            <div id="footer">
                <div class="block">
                    <p>Copyright &copy; 2015</p>
                </div>
            </div>
        </div>
    </body>
</html>