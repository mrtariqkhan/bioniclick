<html>
    <head>
        
    </head>
    <body>
        <div>
            An order for numbers has been placed for the following Number Pool:

            <ul style="list-style: none;">
                <li>
                    Order Id:
                    <?php echo $id; ?>
                </li>
                <li>
                    Created Date:
                    <?php echo $created_date; ?>
                </li>
                <li>
                    Author:
                    <?php echo $creator; ?>
                </li>
                <li>
                    Advertiser:
                    <?php echo $advertiser; ?>
                </li>
                <li>
                    Campaign:
                    <?php echo $campaign; ?>
                </li>
                <li>
                    Phone Number type:
                    <?php echo $phone_number_type; ?>
                </li>
                <li>
                    Country:
                    <?php echo $country; ?>
                </li>
                <li>
                    State:
                    <?php echo $state; ?>
                </li>
                <li>
                    Area Code:
                    <?php echo $area_code; ?>
                </li>
                <li>
                    Status:
                    <?php echo $status; ?>
                </li>
                <li>
                    Quantity of numbers:
                    <?php echo $amount; ?>
                </li>
                <li>
                    Quantity of fulfilled numbers:
                    <?php echo $done_amount; ?>
                </li>
            </ul>
        </div>        
    </body>
</html>

