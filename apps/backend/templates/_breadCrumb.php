<nav class="breadcrumb">

    <?php
        $breadCrumbs = $object->getParentBreadCrumbs()->getRawValue();
        
        if (isset($title)) {
            $breadCrumbs[] = $object->getBreadCrumb()->getRawValue();
        } else {
            $title = $object->getBreadCrumb()->getTitle();
        }
    ?>
    <?php foreach ($breadCrumbs as $i => $breadCrumb): ?>
        <a
            href="<?php echo url_for($breadCrumb->getRouteName(), $breadCrumb->getRouteParams()); ?>"
            class="<?php echo $breadCrumb->getClassName(); ?> ajax-load"
        >
            <?php echo $breadCrumb->getTitle(); ?>
        </a> →
    <?php endforeach; ?>
</nav>

<h1><?php echo $title; ?></h1>
