<?php

require_once(dirname(__FILE__).'/../../../lib/FairDev/Payment/AuthorizeNet/CIM.php');
require_once(dirname(__FILE__).'/../../../lib/FairDev/Payment/AuthorizeNet/MerchantAuth.php');
require_once 'AuthorizeLogger.class.php';

/**
 * Description of AuthorizeNetCIM
 *
 * @author Kirill chEbba Chebunin <iam at chebba.org>
 */
class AuthorizeNet {

    const OBJECT_TYPE_CODE_COMPANY      = 'c';
    const OBJECT_TYPE_CODE_ADVERTISER   = 'a';

    /**
     * @var FairDev_Payment_AuthorizeNet_CIM
     */
    private static $cim;



    /**
     *
     * @return FairDev_Payment_AuthorizeNet_CIM
     */
    public static function getCIM() {

        if (self::$cim === null) {

            $merchantAuth = new FairDev_Payment_AuthorizeNet_MerchantAuth();
            $merchantAuth
                ->setName(sfConfig::get('app_login_id'))
                ->setTransactionKey(sfConfig::get('app_transaction_key'))
            ;

            self::$cim = new FairDev_Payment_AuthorizeNet_CIM(
                $merchantAuth,
                sfConfig::get('app_authorize_testmode', false)
            );
            self::$cim->setLogger(new AuthorizeLogger());
        }

        return self::$cim;
    }

    public static function createCustomerProfile($objectTypeCode, $objectUniqueIdentifier) {

        if (!sfConfig::get('app_authorize_work_on')) {
            return null;
        }

        $cim = self::getCIM();

        //TODO:: think about lenght of $merchantCustomerId it must be <= 20
        $uniquePrefix = sfConfig::get('my_custom_unique_profile_prefix');
        $merchantCustomerId = "$objectTypeCode-$uniquePrefix-$objectUniqueIdentifier";

        $authorizeCustomerProfile = new FairDev_Payment_AuthorizeNet_CustomerProfile();
        $authorizeCustomerProfile->setMerchantCustomerId($merchantCustomerId);

        try {
            $cim->createCustomerProfile($authorizeCustomerProfile);
        } catch (FairDev_Payment_AuthorizeNet_CIMAPIException $e) {
            if ($e->getCode() == FairDev_Payment_AuthorizeNet_CIMAPIException::DUPLICATE_ENTRY) {
                $cim->deleteCustomerProfile($e->getDuplicateEntry());
                $cim->createCustomerProfile($authorizeCustomerProfile);
            } else {
                throw $e;
            }
        }

        $authorizeCustomerProfileId = $authorizeCustomerProfile->getId();
        return $authorizeCustomerProfileId;
    }

    public static function deleteCustomerProfile($authorizeCustomerProfileId) {

        if (!sfConfig::get('app_authorize_work_on')) {
            return;
        }

        $cim = self::getCIM();

//        $idsBefore = $cim->getCustomerProfileIds();
        $cim->deleteCustomerProfile($authorizeCustomerProfileId);
//        $idsAfter = $cim->getCustomerProfileIds();
    }
}
