<?php

//NOTE:: see sfConfig::get('sf_plugins_dir') . 'plugins/sfDoctrineApplyPlugin/modules/sfApply/lib/BasesfApplyActions.class.php'

class GeneralApplyActions extends GeneralActions {

    //NOTE:: see BasesfApplyActions::executeConfirm
    public function executeConfirm(sfRequest $request) {

        $validate = $this->request->getParameter('validate', null);
        if (empty($validate)) {
            return 'Invalid';
        }

        $sfGuardUser = Doctrine_Query::create()
            ->from("sfGuardUser u")
            ->innerJoin("u.Profile p with p.validate = ?", $validate)
            ->fetchOne()
        ;
        if (!$sfGuardUser) {
            return 'Invalid';
        }

        $type = sfGuardUserProfileTable::getValidationType($validate);
        if ($type == sfGuardUserProfileTable::VALIDATE_TYPE_NAME_INVALID) {
            return 'Invalid';
        }

        $profile = $sfGuardUser->getProfile();
        $profile->setValidateByType(sfGuardUserProfileTable::VALIDATE_TYPE_VALID);
        $profile->save();

        if ($type == sfGuardUserProfileTable::VALIDATE_TYPE_NAME_NEW) {
            $sfGuardUser->setIsActive(true);
            $sfGuardUser->save();
            $this->getUser()->signIn($sfGuardUser);
        }

        if ($type == sfGuardUserProfileTable::VALIDATE_TYPE_NAME_RESET) {
            $this->getUser()->setAttribute('sfApplyReset', $sfGuardUser->getId());
            return $this->redirect('sfApply/reset');
        }
    }

    //NOTE:: see BasesfApplyActions::executeResetRequest
    public function executeResetRequest(sfRequest $request) {

        $user = $this->getUser();
        if ($user->isAuthenticated()) {
            $guardUser = $this->getUser()->getGuardUser();
            $this->forward404Unless($guardUser);
            return $this->resetRequestBody($guardUser);
        } else {
            $this->form = $this->newForm('sfApplyResetRequestForm');
            if ($request->isMethod('post')) {
                $this->form->bind($request->getParameter('sfApplyResetRequest'));
                if ($this->form->isValid()) {
                    $username_or_email = $this->form->getValue('username_or_email');
                    $user = sfGuardUserTable::retrieveByUsernameWithWasDeleted($username_or_email);
                    return $this->resetRequestBody($user);
                }
            }
        }
    }

    //NOTE:: see BasesfApplyActions::executeReset
    public function executeReset(sfRequest $request) {

        $this->form = $this->newForm('sfApplyResetForm');
        if ($request->isMethod('post')) {
            $this->form->bind($request->getParameter('sfApplyReset'));
            if ($this->form->isValid()) {
                $id = $this->getUser()->getAttribute('sfApplyReset', false);
                $this->forward404Unless($id);
                $sfGuardUser = Doctrine::getTable('sfGuardUser')->find($id);
                $this->forward404Unless($sfGuardUser);

                $sfGuardUser->setPassword($this->form->getValue('password'));
                $sfGuardUser->save();

                $this->getUser()->signIn($sfGuardUser);
                $this->getUser()->setAttribute('sfApplyReset', null);

                $this->id = $id;
                $this->sfGuardUser = $sfGuardUser;

                return 'After';
            }
        }
    }

    //NOTE:: see BasesfApplyActions::executeResetCancel
    public function executeResetCancel() {

        $this->getUser()->setAttribute('sfApplyReset', null);
        return $this->redirect(sfConfig::get('app_sfApplyPlugin_after', '@homepage'));
    }

    //NOTE:: see BasesfApplyActions::resetRequestBody
    public function resetRequestBody($sfGuardUser) {

        if (empty($sfGuardUser)) {
            return 'NoSuchUser';
        }
        $profile = $sfGuardUser->getProfile();
        $this->forward404Unless($profile);

        if (!$sfGuardUser->getIsActive()) {
            $validate = $profile->getValidate();
            $type = sfGuardUserProfileTable::getValidationType($validate);
            if ($type === sfGuardUserProfileTable::VALIDATE_TYPE_NAME_NEW) {
                try {
                    $this->sendVerificationMail($profile);
                } catch (Exception $e) {
                    return 'UnverifiedMailerError';
                }
                return 'Unverified';
            } elseif ($type === sfGuardUserProfileTable::VALIDATE_TYPE_NAME_RESET) {
                // NOTE:: They lost their first password reset email. That's OK. let them try again
            } else {
                return 'Locked';
            }
        }

        $profile->setValidateByType(sfGuardUserProfileTable::VALIDATE_TYPE_RESET);
        $profile->save();

        try {
            $host = $this->getRequest()->getHost();
            $defaultSubject = sfContext::getInstance()->getI18N()->__("Please verify your password reset request on %1%", array('%1%' => $host));

            $this->mail(array(
                'fullname'      => $profile->getFullname(),
                'email'         => $profile->getEmail(),
                'text'          => 'sfApply/sendValidateResetText',
                'html'          => 'sfApply/sendValidateReset',
                'subject'       => sfConfig::get(
                    'app_sfApplyPlugin_reset_subject',
                    $defaultSubject
                ),
                'parameters'    => array(
                    'fullname' => $profile->getFullname(),
                    'validate' => $profile->getValidate(),
                    'username' => $sfGuardUser->getUsername(),
                ),
            ));

        } catch (Exception $e) {
            return 'UnverifiedMailerError';
        }

        return 'After';
    }

    //NOTE:: see BasesfApplyActions::getFromAddress
    protected function getFromAddress() {

        $from = sfConfig::get('app_sfApplyPlugin_from', false);
        if (!$from) {
            throw new Exception('app_sfApplyPlugin_from is not set');
        }

        // i18n the full name
        return array(
            'fullname'  => sfContext::getInstance()->getI18N()->__($from['fullname'])
        );
    }

    //NOTE:: see BasesfApplyActions::sendVerificationMail
    protected function sendVerificationMail($profile) {

        $host = $this->getRequest()->getHost();
        $defaultSubject = sfContext::getInstance()->getI18N()->__("Please verify your account on %1%", array('%1%' => $host));

        $this->mail(array(
            'fullname'      => $profile->getFullname(),
            'email'         => $profile->getEmail(),
            'text'          => 'sfApply/sendValidateNewText',
            'html'          => 'sfApply/sendValidateNew',
            'subject'       => sfConfig::get(
                'app_sfApplyPlugin_apply_subject',
                $defaultSubject
            ),
            'parameters'    => array(
                'fullname' => $profile->getFullname(),
                'validate' => $profile->getValidate()
            ),
        ));
    }

    protected function mail($options) {

        $required = array('subject', 'parameters', 'email', 'fullname', 'html', 'text');
        foreach ($required as $option) {
            if (!isset($options[$option])) {
                throw new sfException("Required option $option not supplied to sfApply::mail");
            }
        }

        $from = $this->getFromAddress();

        $options = array(
            EmailHelper::OPTION_FROM                => array(
                EmailHelper::SUB_OPTION_FULLNAME=> $from['fullname'],
            ),
            EmailHelper::OPTION_TO                  => array(
                EmailHelper::SUB_OPTION_EMAIL   => $options['email'],
                EmailHelper::SUB_OPTION_FULLNAME=> $options['fullname'],
            ),
            EmailHelper::OPTION_SUBJECT             => $options['subject'],
            EmailHelper::OPTION_TEMPLATES           => array(
                array(
                    EmailHelper::SUB_OPTION_TEMPLATE                => $options['html'],
                    EmailHelper::SUB_OPTION_TEMPLATE_PARAMETERS     => $options['parameters'],
                    EmailHelper::SUB_OPTION_TEMPLATE_CONTENT_TYPE   => EmailHelper::CONTENT_TYPE_TEXT_HTML,
                ),
                array(
                    EmailHelper::SUB_OPTION_TEMPLATE                => $options['text'],
                    EmailHelper::SUB_OPTION_TEMPLATE_PARAMETERS     => $options['parameters'],
                    EmailHelper::SUB_OPTION_TEMPLATE_CONTENT_TYPE   => EmailHelper::CONTENT_TYPE_TEXT_PLAIN,
                ),
            ),
        );

        EmailHelper::mail($options);
    }

    //NOTE:: see BasesfApplyActions::newForm
    protected function newForm($className, $object = null, $options = array()) {

        $key = "app_sfApplyPlugin_$className" . "_class";
        $class = sfConfig::get($key, $className);

        return new $class($object, $options);
    }

    protected function getNewForm() {

        $currentCompany = $this->user->isAdvertiserUser() 
            ? $this->user->getAdvertiser() 
            : $this->user->getCompany()
        ;

        $currentCompanyAdmin = $currentCompany->getAdmin();
        if (empty($currentCompanyAdmin)) {
            throw new Exception("User's company(advertiser) must have it's admin.");
        }

        return $this->newForm(
            'sfApplyApplyFormNew',
            null,
            array(
                'sf_guard_user_admin' => $currentCompanyAdmin,
            )
        );
    }

    protected function getNewCustomerForm($id, $isCompanyCustomer = false) {

        $object = new sfGuardUserProfile();

        if ($isCompanyCustomer) {
            $object->setCompanyId($id);
            $currentCompany = CompanyTable::getInstance()->findOneBy('id', $id);
        } else {
            $object->setAdvertiserId($id);
            $currentCompany = AdvertiserTable::getInstance()->findOneBy('id', $id);
        }

        $currentCompanyAdmin = $currentCompany->getAdmin();
        if (empty($currentCompanyAdmin)) {
            throw new Exception("User's company(advertiser) must have it's admin.");
        }

        return $this->newForm(
            'sfApplyApplyCustomerFormNew',
            $object,
            array(
                'sf_guard_user_admin' => $currentCompanyAdmin,
            )
        );
    }
}