<?php
/**
 * Description of PagerHelper
 *
 * @author Kirill chEbba Chebunin <iam at chebba.org>
 */


/**
 * Create HTML for pagination
 *
 * @param sfPager $pager Pager
 * @param string $baseUrl url to this page (with table) without parameters to pager.
 * @param string $urlPattern sprintf like string with 2 params: max_per_page and page
 *      (currently support only '%s')
 * @param string $additionalPagerClass additional class name to pager's div.
 * @return string HTML code
 */
function pager(sfPager $pager, array $maxPerPages, $baseUrl, $urlPattern, $additionalPagerClass)
{
    $urlPattern = pager_unescape($urlPattern);

    $pagerClass = 'pager' . ' ' . (empty($additionalPagerClass) ? '' : $additionalPagerClass);

    $html = "<div paging_table_url=\"$baseUrl\" class=\"$pagerClass\">";
    $html .= pager_select_of_max_per_page($pager->getMaxPerPage(), $maxPerPages, $pager->count());
    $html .= pager_links_to_pages($pager, $urlPattern);
    $html .= '</div>';

    return $html;
}


/**
 * Create and HTML for pager max_per_page options
 *
 * @param string $current Current value
 * @param array $maxPerPages Custom maxPerPages.
 * @param integer $count amount of entities.
 * @return string HTML code
 */
function pager_select_of_max_per_page($current = null, array $maxPerPages = null, $count = 0)
{
    $style = '';

    $html = '';
    if (empty($maxPerPages))
    {
        return $html;
    }
    $minMaxPerPages = $maxPerPages[0];
    if ($count <= $minMaxPerPages)
    {
        $style = 'display:none';
    }

    $html = '<select class="max-per-page" style="' . $style . '">%s</select>';
    $content = '';
    foreach ($maxPerPages as $value)
    {
        $selected = ($value == $current) ? 'selected="selected"' : '';
        $content .= "<option value=\"$value\" $selected>$value</option>";
    }

    return sprintf($html, $content);
}

/**
 * Create HTML for pagination links
 *
 * @param sfPager $pager Pager
 * @param string $urlPattern sprintf like string with 2 params: max_per_page and page
 *      (currently support only '%s')
 * @return string HTML code
 */
function pager_links_to_pages(sfPager $pager, $urlPattern)
{
    $html = '';
    if ($pager->haveToPaginate())
    {
        $maxPerPage     = $pager->getMaxPerPage();

        $firstPage      = $pager->getFirstPage();
        $previousPage   = $pager->getPreviousPage();
        $nextPage       = $pager->getNextPage();
        $lastPage       = $pager->getLastPage();

        $html =  link_to_page("&nbsp;", $urlPattern, $maxPerPage, $firstPage, "first-page", "First page");
        $html .= link_to_page("&nbsp;", $urlPattern, $maxPerPage, $previousPage, "previous-page", "Previous page");

        $links = $pager->getLinks();
        $currentMaxLink = $pager->getCurrentMaxLink();
        $currentPage = $pager->getPage();
        foreach ($links as $page) {
            if ($page == $currentPage)
            {
                $html .= $page;
            }
            else
            {
                $html .= link_to_page($page, $urlPattern, $maxPerPage, $page, "link_to_page", "page " . $page);
            }

            if ($page != $currentMaxLink)
            {
                $html .= ' <span class="pager-space">.</span> ';
            }
        }

        $html .= link_to_page("&nbsp;", $urlPattern, $maxPerPage, $nextPage, "next-page", "Next page");
        $html .= link_to_page("&nbsp;", $urlPattern, $maxPerPage, $lastPage, "last-page", "Last page");
    }
    return $html;
}

/**
 *
 * @param <type> $name - name of link
 * @param <type> $urlPattern
 * @param <type> $maxPerPage
 * @param <type> $page
 * @param <type> $linkClassName - class name of HTML element
 * @param <type> $title - title of HTML element
 * @return <type>
 */
function link_to_page($name, $urlPattern, $maxPerPage, $page, $linkClassName, $title)
{
    $routeName = empty($urlPattern) ? '#' : sprintf($urlPattern, $maxPerPage, $page);

    return '<a class="' . $linkClassName . '" href="' . $routeName . '" pager_data="' . $page . '" title="' . $title . '">' . $name . '</a>';
}

/**
 * Unescape sprintf params encoded with urlencode (currently only '%s')
 *
 * @param string $str
 * @return string
 */
function pager_unescape($str)
{
    return str_replace('%25s', '%s', $str);
}
