<?php
/**
 * Create HTML and js for mp3 audio player
 * @param string $url of file
 * @param array $params Params of flash player
 * @return <type>
 * @link http://flash-mp3-player.net/players/maxi/documentation/
 */
function audio_player($url, array $params = array()) {
    $defaultParams = array(
        'width'       => '200',
        'height'      => '20',
        'showvolume'  => 1,
        'showloading' => 'always',
        'autoShow'    => false,
	'autoload'  => 1	
    );

    $params = array_merge($defaultParams, $params);

    $params['mp3'] = $url;

    $query = http_build_query($params);

    $playerPath = '/swf/player_mp3_maxi.swf';
    
    $html = "
<object width=\"{$params['width']}\"
        height=\"{$params['height']}\"
	data=\"$playerPath\"
	value=\"$playerPath\"
        type=\"application/x-shockwave-flash\">
    <param name=\"movie\" value=\"$playerPath\"/>
    <param name=\"FlashVars\" value=\"$query\" />
    <param name=\"allowScriptAccess\" value=\"always\" />
    <param name=\"scale\" value=\"noscale\" />
    <param name=\"quality\" value=\"high\" />

    <embed src=\"$playerPath\"
            width=\"{$params['width']}\"
            height=\"{$params['height']}\"
            name=\"haxe\"
            scale=\"noscale\"
            allowScriptAccess=\"always\"
            type=\"application/x-shockwave-flash\"
            pluginspage=\"http://www.macromedia.com/go/getflashplayer\"
            FlashVars=\"$query\"
            quality=\"high\"
            type=\"application/x-shockwave-flash\"
            onload=\"alert('!!!')\"
    />
</object>
    ";
    
    
    if($params['autoShow']) return $html;
        
    return "<p></p>
<span class=\"audio-player\">
    <a href=\"javascript:void(0);\">Play</a>
    <span style=\"display: none\">" . 
        $html . "
    </span>
</span>
    ";
    
}
