<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Create HTML for one link with icon
 *
 * @param string $name content of <a> tag: <a>$name</a>
 *  * @param array $a_options attributes of <a> tag: <a $options>...</a>
 *        or string: "<a ...>...</a>", for example it may be that comes back link_to()
 * @param array $ul_options attributes of <ul> tag: <ul $options>...</ul>
 * @return string HTML code
 */
    function icon_link(
        $name,
        $a_options,
        $ul_options = array('class' => 'icon-links')
    ) {

        if (is_array($a_options))
        {
            $a = content_tag('a', $name, $a_options);
        }
        elseif(is_string($a_options))
        {
            $a = $a_options;
        }
        $li = content_tag('li', $a);
        $ul = content_tag('ul', $li, $ul_options);
        return $ul;
    }

 /**
 * Create HTML for many links as list <ul> 
 
 * @param array of arrays $a_options attributes of <a> tag: <a $options['link_name']>...</a>
 *        or array of strings: "<a ...>...</a>", for example it may be that comes back link_to()
 * @return string HTML code
 */
    function icon_links(
        $a_options = array(),
        $ul_options = array('class' => 'icon-links clearfix'),
        $with_name = true
    ) {

        $ul_content = '';
        if ($with_name) {
            foreach ($a_options as $name => $options) {           
                $ul_content .= _link_in_li($name, $options);
            }
        } else {
            foreach ($a_options as $name => $options) {
                if (empty($options['title'])) $options['title'] = $name;
                $ul_content .= _link_in_li('', $options);
            }
        }
        if (empty($ul_content)) {
            return '';
        }
        $ul = content_tag('ul', $ul_content, $ul_options);
        return $ul;
    }

 /**
 * Create HTML for one link into <li> tag: <li><a>...</a></li>
 *
 * This is backup function. Don't use it in your templates.
 *
 * @param string $name content of <a> tag: <a>$name</a>
 * @param array $a_options attributes of <a> tag: <a $options>...</a>
 *        or string: "<a ...>...</a>", for example it may be that comes back link_to()
 * @return string HTML code
 */
    function _link_in_li($name, $a_options) {
        
        if (is_array($a_options)) {
            
            if (isset($a_options['href'])) {
                return content_tag('li',
                    content_tag('a', $name, $a_options)
                );
            } else {
                return content_tag('li', $name, $a_options );
            }
        } elseif(is_string($a_options)) {
            
            return content_tag('li', $a_options );
        }
    }

/**
 *
 * @param string $name content of <a> tag: <a>$name</a>
 * @param string $internal_uri
 * @param array $options tag options
 * @return string HTML code
 */
    function link_to_ajax_delete($name, $internal_uri, $options = array()) {

        $html_options = _parse_attributes($options);
        $html_options = add_ajax_delete_options($html_options);

        $html_options['href'] = url_for($internal_uri);

        if (!strlen($name))
        {
         //   $name = $html_options['href'];
        }

        return content_tag('a', $name, $html_options);
    }

    function add_ajax_delete_options($html_options) {

        $html_options['ajax_delete_method'] = 'delete';
        $html_options['ajax_delete_special_form_data'] = 
            array_key_exists('special_form_data', $html_options)
            ? $html_options['special_form_data']
            : ''
        ;
        if (array_key_exists('class', $html_options)) {
            $html_options['class'] .= ' ajax-delete';
        } else {
            $html_options['class'] = ' ajax-delete';
        }
        

        unset($html_options['special_form_data']);

        // CSRF protection
        $form = new BaseForm();
        if ($form->isCSRFProtected())
        {
            $html_options['ajax_delete_csrf_name'] = $form->getCSRFFieldName();
            $html_options['ajax_delete_csrf_value'] = $form->getCSRFToken();
        }

        return $html_options;
    }

?>
