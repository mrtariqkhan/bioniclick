<?php

require_once 'FairDev/Payment/AuthorizeNet/Log.php';

/**
 * Description of AuthorizeLogger
 *
 * @author Kirill chEbba Chebunin <iam at chebba.org>
 */
class AuthorizeLogger implements FairDev_Payment_AuthorizeNet_Log {

    /**
     *
     * @param string $message
     * @param integer $priority 
     */
    public function log($message, $priority) {

        try {
            Loggable::putLogMessage(ApplicationLogger::PAYMENTS_LOG_TYPE, $message, $priority);
        } catch (Exception $e) {
            
        }
    }
}

