<?php

/**
 * General actions.
 *
 * @package    bionic
 * @subpackage lib
 * @author     Your name here
 * @version
 */

class GeneralActions extends sfActions {

    var $tableName = null;
    var $securityHelper = null;
    var $filterAttributeName = '';
    var $user = null;
    var $userId = null;

    public function preExecute() {

        parent::preExecute();

        $this->user = $this->getUser()->getGuardUser();
        $this->userId = empty($this->user) ? null : $this->user->getId();

        $this->securityHelper = new SecurityHelper($this);
        $this->permissions = $this->configurePermissionsBase();

        $this->filterAttributeName = $this->getFilterAttributeName(sfInflector::underscore($this->getModuleName()) . '_filter');

        //FIXME: for loader
        $this->getResponse()->addJavascript('/js/project/_loader.js', 'last');
        $this->getResponse()->addJavascript('/js/project/ajax_global_error.js', 'last');
        $this->addStylesheetByCompany('/css/project/skins/style.css');

        $this->getResponse()->addStylesheet('jquery/jquery-ui.css', 'last');
        //Disable browser cache (temp settings)
        $this->getResponse()->setHttpHeader('Expires', 'Mon, 26 Jul 1997 05:00:00 GMT');
        $this->getResponse()->setHttpHeader('Cache-Control', 'no-store, no-cache, must-revalidate');
        $this->getResponse()->setHttpHeader('Pragma', 'no-cache');
        //header('Cache-Control: post-check=0, pre-check=0', FALSE);
    }

    public function getPagerParams(sfWebRequest $request) {

        $maxPerPages = sfConfig::get('app_pager_max_per_pages');
        $pagerParams = $request->getParameter('pager', array());
        $maxPerPage = !empty($pagerParams) && !empty($pagerParams['max_per_page'])
            ? $pagerParams['max_per_page']
            : $maxPerPages[0]
        ;
        $page = !empty($pagerParams) && !empty($pagerParams['page'])
            ? $pagerParams['page']
            : 1
        ;

        return array(
            'page'         => $page,
            'maxPerPage'   => $maxPerPage,
        );
    }

    /**
     * Creates pager, sets page and maxPerPage parameters
     * @param sfWebRequest $request request, is used to find page
     *     and maxPerPage
     * @return <type> sfDoctrinePager
     */
    public function initPager(sfWebRequest $request, $specialQuery = null) {

        $pagerParams = $this->getPagerParams($request);

        $pager = new sfDoctrinePager($this->tableName, $pagerParams['maxPerPage']);
        if (!empty($specialQuery)) {
            $pager->setQuery($specialQuery);
        }
        $pager->setPage($request->getParameter('page', $pagerParams['page']));
        $pager->init();

        return $pager;
    }

    /**
     * Method binds, validates, saves configured form.
     * @param sfWebRequest $request
     * @param *Form $form configured form
     * @return array of resultof saving
     */
    protected function processForm(sfWebRequest $request, $form) {

        $errorsForm = array();
        $errorGlobal = null;

        $formTagName = $form->getName();
        $parameters = $request->getParameter($formTagName);
        $files = $request->getFiles($formTagName);

        $form->bind($parameters, $files);
        $savedObject = null;
        $isNew = false;
        if (method_exists($form, 'isNew')) {
            $isNew = $form->isNew();
        }
        if ($request->isMethod('put') || $request->isMethod('post')) {
            try {
                $needToSave = $this->preSaveForm($form);
                if ($needToSave) {
                    if ($form->isValid()) {
                        $savedObject = $this->saveValidForm($form);
                    } else {
                        $errorsForm = $this->getErrors($form);
                    }
                } elseif (is_null($needToSave)) {
                    $errorsForm = $this->getErrors($form);
                }
            } catch (Exception $e) {
                $errorGlobal = $e->getMessage();
                $errorGlobal = empty($errorGlobal) ? "Error" : $errorGlobal;
            }
        } else {
            $errorGlobal = 'Method is wrong.';
        }

        $success = empty($errorsForm) && empty($errorGlobal);

        if ($success && $isNew && !empty($savedObject)) {
            try {
                $success = $this->postSuccessfulCreation($savedObject);
                if (!$success) {
                    $savedObject->delete();
                }
            } catch (Exception $e) {
                $success = false;
                $errorGlobal = $e->getMessage();
                $savedObject->delete();
            }
        }

        $result = array();
        $result['success'] = $success;
        $result['error'] = array(
            'global'        => $errorGlobal ? array($errorGlobal) : array(),
            'form_global'   => $errorsForm ? $errorsForm['global'] : array(),
            'form_local'    => $errorsForm ? $errorsForm['local'] : array(),
        );

        return $result;
    }

    /**
     * Needs to be implemented in subclasses to do any pre save actions.
     * Throw an Exception if action was failed.
     * @param sfForm $form
     * @return
     *  - true if form needs to be saved, 
     *  - false if form does not need to be saved, 
     *  - null if form is not valid and preSave need valid one
     */
    protected function preSaveForm(sfForm $form) {
        return true;
    }

    /**
     * Needs to be implemented in subclasses to do any post save actions for the
     * cases of successful saving.
     * Throw an Exception if action was failed.
     * @param $object model of saved form
     * @return boolean true if created object needs to be stored and false for deletion
     */
    protected function postSuccessfulCreation($savedObject) {
        return true;
    }

    /**
     * Saves valid form.
     * 
     * If you want to add some custom processing of form's saving You should
     * redefine this method in Your inheritor.
     * 
     * Attention: You should re-throw exceptions if You catchs them.
     * 
     * @throws Exceptions
     * @param sfForm $validForm
     */
    protected function saveValidForm($validForm) {
        return $validForm->save();
    }

    protected function getErrors(sfForm $form) {

        $errors = $form->getErrorSchema();
        $formTagName = $form->getName();

        $embededForms = $form->getEmbeddedForms();
        $embededFormsNames = array_keys($embededForms);

        $errorMessage = array(
            'global'   => array(),
            'local'    => array(),
        );

        //NOTE:: $errMsg = "globalError fieldName1 [errMsg1] fieldName2 [errMsg2] fieldNameN [errMsgN]"
        $patternAllMessages = '/([^\s]+\s\[[^\[]*\])/';

        //NOTE:: $embeddedError = "fieldName [errMsg]"
        $patternMessage = '/([^\s]+)\s\[(.*)\]/';

        //NOTE:: $embeddedError = "globalError fieldName1 [errMsg1] fieldName2 [errMsg2] fieldNameN [errMsgN]"
        $patternEmbeddedMessageHasGlobalAndLocalErrors = '/^([^\[]*)\s([^\s]+\s\[[^\[]*\])/';

        //NOTE:: $embeddedError = "globalError"
        $patternEmbeddedMessageHasGlobalErrorOnly = '/^([^\[]*)$/';

        foreach ($errors as $fieldName => $error) {
            $errMsg = $error->getMessage();

            if (!is_string($fieldName)) {
                $errorMessage['global'][] = $errMsg;
                continue;
            }

            if (array_search($fieldName, $embededFormsNames) !== false) {

                $embeddedFormTagName = $fieldName;

                preg_match_all($patternEmbeddedMessageHasGlobalErrorOnly, $errMsg, $matchesGlobalOnly);
                if (!empty($matchesGlobalOnly) && count($matchesGlobalOnly) >= 2) {
                    $globalEmbedded = $matchesGlobalOnly[1];
                    if (!empty($globalEmbedded)) {
                        //NOTE:: $embeddedError = "globalError" only
                        $errorMessage['global'][] = $globalEmbedded;
                        continue;
                    }

                }
                preg_match_all($patternEmbeddedMessageHasGlobalAndLocalErrors, $errMsg, $matchesGlobalAndLocal);
                if (!empty($matchesGlobalAndLocal) && count($matchesGlobalAndLocal) >= 3) {
                    $globalEmbedded = $matchesGlobalAndLocal[1];
                    if (!empty($globalEmbedded)) {
                        //NOTE:: $embeddedError = "globalError fieldName1 [errMsg1] fieldName2 [errMsg2] fieldNameN [errMsgN]"
                        $errorMessage['global'][] = $globalEmbedded;
                    }
                }

                preg_match_all($patternAllMessages, $errMsg, $matches);
                if (!empty($matches)) {
                    //NOTE:: $embeddedError = "fieldName1 [errMsg1] fieldName2 [errMsg2] fieldNameN [errMsgN]"
                    $embeddedErrors = $matches[0];

                    foreach ($embeddedErrors as $embeddedError) {
                        preg_match($patternMessage, $embeddedError, $embeddedMatches);
                        if (count($embeddedMatches) >= 2) {
                            $embeddedFieldName  = $embeddedMatches[1];
                            $embeddedErrMsg     = $embeddedMatches[2];

                            $embeddedPathToField = '_' . $embeddedFormTagName . '_' . $embeddedFieldName;

                            $errorMessage['local'][$formTagName . $embeddedPathToField] = $embeddedErrMsg;
                        }
                    }
                }

            } else {
                $pathToField = '_' . $fieldName;
                $errorMessage['local'][$formTagName . $pathToField] = $errMsg;
            }
        }

        return $errorMessage;
    }

    protected function processDelete(sfWebRequest $request) {

        $error = '';

        if ($request->isMethod('delete')) {
            try {
                $request->checkCSRFProtection();
                $object = $this->getRoute()->getObject();

                if ($this->isAllowableObject($object)) {
                    $this->deleteObject($object);
                } else {
                    $error = 'You have no permissions to delete this object.';
                }
            } catch (Exception $e) {
                if ((
                    strpos($e->getMessage(), 'SQLSTATE[HY000]')
                        !== false
                        && strpos($e->getMessage(), 'General error: 1451')
                    ) || (
                        strpos($e->getMessage(), 'SQLSTATE[23000]')
                            !== false
                            && strpos($e->getMessage(), 'Integrity constraint violation: 1451')
                    )
                ) {
                    $error = ' It is impossible to delete object since other objects are connected to it ';
                } else {
                    if (strpos($e->getMessage(), 'SQLSTATE')) {
                        $error = ' ';
                    } else {
                        $error = $e->getMessage();
                    }
                }
                Loggable::putExceptionLogMessage($e);
            }
        } else {
            $error = 'Method is wrong.';
        }

        $result = array();
        $result['success'] = empty($error);
        $result['error'] = $error;

        return $result;
    }

    /**
     * Deletes object.
     *
     * If you want to add some custom processing You should
     * redefine this method in Your inheritor.
     *
     * Attention: You should re-throw exceptions if You catchs them.
     *
     * @throws Exceptions
     * @param <> $object
     */
    protected function deleteObject($object) {
        $object->delete();
    }

    protected function checkIsAllowableObject($object, $forUpdating = true) {
        $this->forward404Unless($this->isAllowableObject($object, $forUpdating));
    }
    
    protected function checkIsAllowableObjectJson($object, $message = "Can't allowable object", $code = 1404, $forUpdating = true) {
        
        if (!$this->isAllowableObject($object, $forUpdating)) {
            throw new jsonRpcException($message, $code);
        }
    }

    protected function isAllowableObject($object, $forUpdating = true) {
        return IsAllowableHelper::isAllowable($object, $this->user, $forUpdating);
    }

    protected function processAjaxForm(sfWebRequest $request, $form) {

        $result = $this->processForm($request, $form);

        return $this->processAjaxResult($request, $result, true);
    }

    protected function processAjaxDelete(sfWebRequest $request) {

        $result = $this->processDelete($request);

        return $this->processAjaxResult($request, $result, true);
    }

    protected function processAjaxResult(
        sfWebRequest $request,
        $result,
        $isForAjaxForm = true
    ) {

        $this->getResponse()->setHttpHeader('Content-Type','application/json');

        //TODO:: is $request nesessary param?
        //NOTE:: we do not use iframe flag to jquery.form.js so we do not use textareas
//        $encoded = $isForAjaxForm
//            ? '<textarea>' . json_encode($result) . '</textarea>'
//            : json_encode($result)
//        ;
        return $this->renderText(json_encode($result));
    }

    protected function getFilterAttributeName($attributeName = null) {

        if (empty($attributeName)) {
            return $this->filterAttributeName;
        }
        return $attributeName . '_' . $this->userId;
    }

    protected function getFilter($attributeName = null) {

        $attributeName = $this->getFilterAttributeName($attributeName);
        return $this->getUser()->getAttribute($attributeName, array());
    }

    protected function setFilter(array $values, $attributeName = null) {

        $attributeName = $this->getFilterAttributeName($attributeName);
        $this->getUser()->setAttribute($attributeName, $values);
    }

    protected function setUserAttribute($value, $attributeName) {

        $attributeName = $this->getFilterAttributeName($attributeName);
        $this->getUser()->setAttribute($attributeName, $value);
    }

    protected function getUserAttribute($attributeName) {

        $attributeName = $this->getFilterAttributeName($attributeName);

        return $this->getUser()->getAttribute($attributeName);
    }

    protected function processFilter(sfWebRequest $request, $filter, $attributeName = null) {

        $filterName = $filter->getName();
        $values = $request->getParameter($filterName);
        $filter->bind($values);

        if ($filter->isValid()) {
            $values = $filter->getValues();
            $this->setFilter($values, $attributeName);
        } else {
            $errors = $filter->getErrorSchema();
        }
    }

    protected function processAjaxFilter(sfWebRequest $request, $filter, $attributeName = null) {

        $this->processFilter($request, $filter, $attributeName);
        return $this->processAjaxResult($request, array('success' => true, 'error' => false));
    }

    public function configurePermissionsBase() {
        return $this->securityHelper->checkBasePermissions();
    }

    public function configurePermissionsAll() {
        return $this->securityHelper->checkAllPermissions();
    }

    /**
     * Use this method when You need to get options for form with branch fields
     * (advertiser, agency, reseller)
     *
     * @return array options
     */
    protected function getBranchFilterOptions() {

        $options = array();

        if (!empty($this->user) && !$this->user->isAdvertiserUser()) {
            $options[CompanyTable::$LEVEL_NAME_ADVERTISER] = true;
            $level = $this->user->getLevel();
            if ($level <= CompanyTable::$LEVEL_RESELLER) {
                $options[CompanyTable::$LEVEL_NAME_AGENCY] = true;
            }
            if ($level == CompanyTable::$LEVEL_BIONIC) {
                $options[CompanyTable::$LEVEL_NAME_RESELLER] = true;
            }
        }

        return $options;
    }

    protected function addStylesheetByCompany($pathToCss) {

        if (empty($this->user)) {
            return;
        }

        $skin = $this->user->getFirstCompany()->getSkin();
        $skin = empty($skin) ? CompanyTable::SKIN_DEFAULT : $skin;
        
        $this->getResponse()->addStylesheet(current(explode('.', $pathToCss)) . '_' . $skin . '.css', 'last');
    }

  /**
   * Sets an alternate GLOBAL template for this sfAction.
   *
   * @param string $name    Template name
   */
    public function setGlobalTempate($name) {
        $this->setTemplate(sfConfig::get('sf_app_template_dir') . DIRECTORY_SEPARATOR . $name);
    }

    public function downloadFile($pathToFile, $mimeType, $outFileName = '') {

        $outFileName = empty($outFileName) ? $pathToFile : $outFileName;

        $response = $this->getResponse();
        
        $response->clearHttpHeaders();
        $response->setHttpHeader('Content-type', $mimeType);
        $response->setHttpHeader('Content-Disposition', "attachment;filename=\"{$outFileName}\"");
        $response->setHttpHeader('Content-Transfer-Encoding', 'binary');
        
//        $response->setHttpHeader('Content-Length: ', filesize($pathToFile));
        
        
        $this->pathToFile = $pathToFile;
        
        $this->setLayout(false);
        $this->setGlobalTempate('downloadFile');

        return sfView::SUCCESS;
    }

    /**
     *
     * @param string $taskClassName
     * @param array $taskArguments
     * @param array $taskOptionsAdditional
     * @return type 
     */
    public function processTask($taskClassName, $taskArguments = array(), $taskOptionsAdditional = array()) {

        $task = TaskHelper::createAndRunTask($taskClassName, $taskArguments, $taskOptionsAdditional);

        return sfView::NONE;
    }

    protected function getFilterFormArray(
        $filterFormClassName, 
        $defaults = array(), 
        $options = array(), 
        $ifCleanedIsNeed = false, 
        $ifSavedIsNeed = false, 
        $attributeName = null
    ) {

        $defaultsSaved = array();
        if ($ifSavedIsNeed) {
            $defaultsSaved = $this->getFilter($attributeName);
        }

        $defaultsNew = array_merge(
            $defaults,
            $defaultsSaved
        );

        $form = new $filterFormClassName($defaultsNew, $options);

        $cleanedValues = array();
        if ($ifCleanedIsNeed) {
            $cleanedValues = $form->getFilteredValues($defaultsNew, true);
        }

        return array(
            'form'      => $form,
            'cleaned'   => $cleanedValues,
            'default'   => $defaultsNew,
        );
    }

}

