<?php

class TreeNodeBranching extends TreeNode {

    /**
     *
     * @param type $type one of TreeNode::NODE_TYPE_*
     * @return string
     */
    public static function getTableNameByType($type) {

        switch ($type) {
            case TreeNode::NODE_TYPE_BIONIC:
            case TreeNode::NODE_TYPE_RESELLERS:
            case TreeNode::NODE_TYPE_RESELLER:
            case TreeNode::NODE_TYPE_AGENCIES:
            case TreeNode::NODE_TYPE_AGENCY:
            case TreeNode::NODE_TYPE_ADVERTISERS:
                $tableName = 'Company';
                break;
            case TreeNode::NODE_TYPE_ADVERTISER:
                $tableName = 'Advertiser';
                break;
            default:
                $tableName = null;
        }

        return $tableName;
    }




    public static function configureTreeNodeBionic($company) {
        return self::configureTreeNode($company, TreeNode::NODE_TYPE_BIONIC);
    }

    public static function configureTreeNodeReseller($company) {
        return self::configureTreeNode($company, TreeNode::NODE_TYPE_RESELLER);
    }

    public static function configureTreeNodeAgency($company) {
        return self::configureTreeNode($company, TreeNode::NODE_TYPE_AGENCY);
    }

    public static function configureTreeNodeResellers($company) {
        return self::configureTreeNode($company, TreeNode::NODE_TYPE_RESELLERS);
    }

    public static function configureTreeNodeAgencies($company) {
        return self::configureTreeNode($company, TreeNode::NODE_TYPE_AGENCIES);
    }

    public static function configureTreeNodeAdvertisers($company) {
        return self::configureTreeNode($company, TreeNode::NODE_TYPE_ADVERTISERS);
    }

    public static function configureTreeNodeAdvertiser($advertiser) {
        return self::configureTreeNode($advertiser, TreeNode::NODE_TYPE_ADVERTISER);
    }


    public static function generateTreeNodeKeyResellers($company) {
        return self::generateTreeNodeKey($company, TreeNode::NODE_TYPE_RESELLERS);
    }

    public static function generateTreeNodeKeyAgencies($company) {
        return self::generateTreeNodeKey($company, TreeNode::NODE_TYPE_AGENCIES);
    }

    public static function generateTreeNodeKeyAdvertisers($company) {
        return self::generateTreeNodeKey($company, TreeNode::NODE_TYPE_ADVERTISERS);
    }

    public static function generateTreeNodeKeyAdvertiser($advertiser) {
        return self::generateTreeNodeKey($advertiser, TreeNode::NODE_TYPE_ADVERTISER);
    }
}
