<?php

include('../lib/simple_html_dom.php');

/**
 * widget actions.
 *
 * @package    bionic
 * @subpackage widget
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class GeneralWidgetActions extends GeneralActions {

    var $formName = null;
    var $widgetType = null;

    public function  preExecute() {

        $this->formName = $this->tableName . 'Form';

        $widgetType = $this->tableName;
        $widgetType = sfInflector::underscore($widgetType);
        $this->widgetType = $widgetType;

        parent::preExecute();
    }

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request) {

        $comboId = $request->getParameter('combo_id');
        $combo = ComboTable::getInstance()->find($comboId);
        $this->forward404If(
            empty($combo),
            sprintf('Object combo does not exist (%s).', $comboId)
        );
        $this->checkIsAllowableObject($combo, false);


        $specialQuery = Doctrine::getTable($this->tableName)->getIndexQueryByUserAndComboId($this->user, $comboId);
        $this->pager = parent::initPager($request, $specialQuery);
        $this->need_was_deleted_column = WasDeletedHelper::ifNeedWasDeletedColumn($this->user);

        $this->combo = $combo;
        $this->page = $combo->getPage();
        $this->campaign = $this->page->getCampaign();
    }

    public function executeNew(sfWebRequest $request) {

        $comboId = $request->getParameter('combo_id');
        $combo = ComboTable::getInstance()->find($comboId);
        $this->checkIsAllowableObject($combo);

        $object = new $this->tableName();
        $object->setComboId($comboId);

        $this->form = new $this->formName($object);
    }

    public function executeCreate(sfWebRequest $request) {

        $comboId = $request->getParameter('combo_id');
        $combo = ComboTable::getInstance()->find($comboId);
        $this->checkIsAllowableObject($combo);

        $object = new $this->tableName();
        $object->setComboId($comboId);

        $form = new $this->formName($object);

        return $this->processAjaxForm($request, $form);
    }

    public function executeEdit(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $this->form = new $this->formName($object);
    }

    public function executeUpdate(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $form = new $this->formName($object);

        return $this->processAjaxForm($request, $form);
    }

    public function executeDelete(sfWebRequest $request) {
        return $this->processAjaxDelete($request);
    }

    public function executeVisualPageCreate(sfWebRequest $request) {

        $newOptions = array(WidgetParentForm::OPTION_IS_NEW => true);

        $comboId = $request->getParameter('combo_id');
        $combo = ComboTable::getInstance()->find($comboId);
        $this->checkIsAllowableObject($combo);

        $object = new $this->tableName();
        $object->setComboId($comboId);

        $form = new $this->formName($object, $newOptions);

        $result = $this->processForm($request, $form);
        $savedObject = $result['success'] ? $form->getObject() : null;
        $result['object'] = empty($savedObject) ? array() : $savedObject->asArray();

        return $this->processAjaxResult($request, $result, true);
    }

    public function executeVisualPageUpdate(sfWebRequest $request) {

        $editOptions = array(WidgetParentForm::OPTION_IS_EDIT => true);

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $form = new $this->formName($object, $editOptions);

        $result = $this->processForm($request, $form);
        $savedObject = $result['success'] ? $form->getObject() : null;
        $result['object'] = empty($savedObject) ? array() : $savedObject->asArray();

        return $this->processAjaxResult($request, $result, true);
    }

    public function executeVisualPage(sfWebRequest $request) {

        $comboId = $request->getParameter('combo_id', null);
        $combo = ComboTable::findById($comboId);
        $this->forward404If(
            empty($combo),
            sprintf('Object combo does not exist (%s).', $comboId)
        );
        $this->checkIsAllowableObject($combo);

        $widgets = WidgetParentTable::findByComboAsArray($comboId);
        $physicalPhoneNumbers = PhysicalPhoneNumberTable::findByComboAsArray($combo);
        $numberFormats = NumberFormatTable::findAsArray($comboId);
        $widgetTypes = WidgetParentTable::findWidgetTypesAsArray();

        $pageUrl = $this->getPageURL($combo);

        $routeName = 'campaign_tree_index_selected_' . $this->widgetType;
        $backUrl = $this->generateUrl($routeName, array('id' => $comboId));

        try {
            $html = file_get_html($pageUrl);

            $strHtml = $html->save();
            if (empty($strHtml)) {
                throw new Exception('URL is underfined.');
            }

            $htmlPartsHead = preg_split('/<head>/i', $html);
            if (count($htmlPartsHead) <= 1) {
                throw new Exception('Connection Error.');
            }
            $htmlPartsBody = preg_split('/<body>/i', $htmlPartsHead[1]);
            if (count($htmlPartsBody) <= 1) {
                throw new Exception('Connection Error.');
            }
            $html_parts = array(
                'pre_head'  => $htmlPartsHead[0],
                'head'      => $htmlPartsBody[0],
                'body'      => $htmlPartsBody[1],
            );

            $urlsCreateUpdate = array(
                'create' => array(
                    'method'=> 'POST',
                    'url'   => array(
                        'widget'        => $this->generateUrl('widget_visual_page_create', array('combo_id' => $comboId)),
                        'widget_phone'  => $this->generateUrl('widget_phone_visual_page_create', array('combo_id' => $comboId)),
                    )
                ),
                'update' => array(
                    'method'=> 'PUT',
                    'url'   => array(
                        'widget'        => $this->generateUrl('widget_visual_page_update', array('id' => 0)),
                        'widget_phone'  => $this->generateUrl('widget_phone_visual_page_update', array('id' => 0)),
                    )
                )
            );

            $newOptions  = array(WidgetParentForm::OPTION_IS_NEW  => true);
            $editOptions = array(WidgetParentForm::OPTION_IS_EDIT => true);

            $widgetObject = new Widget();
            $widgetObject->setComboId($comboId);

            $widgetPhoneObject = new WidgetPhone();
            $widgetPhoneObject->setComboId($comboId);

            $formWidgetEdit         = new WidgetForm($widgetObject, $editOptions);
            $formWidgetNew          = new WidgetForm($widgetObject, $newOptions);
            $formWidgetPhoneEdit    = new WidgetPhoneForm($widgetPhoneObject, $editOptions);
            $formWidgetPhoneNew     = new WidgetPhoneForm($widgetPhoneObject, $newOptions);

            $forms = array(
                'form_widget_edit'      => $formWidgetEdit,
                'form_widget_phone_edit'=> $formWidgetPhoneEdit,
                'form_widget_new'       => $formWidgetNew,
                'form_widget_phone_new' => $formWidgetPhoneNew,
            );

            $this->html_parts = $html_parts;
            $this->base_page_url = $pageUrl;
            $this->editor_data = array(
                'back_url'              => $backUrl,
                'urls'                  => $urlsCreateUpdate,
                'objects'               => $widgets,
                'physical_phone_numbers'=> $physicalPhoneNumbers,
                'number_formats'        => $numberFormats,
                'widget_types'          => $widgetTypes
            );
            $this->forms = $forms;

            $this->setLayout(false);

        } catch (Exception $e) {
            $message = $e->getMessage();

            $this->getUser()->setFlash('url_is_underfined', $message, false);
            $this->redirect($backUrl);
        }
    }

    protected function getPageURL($combo) {

        $page = $combo->getPage();
        $campaign = $page->getCampaign();

        $pageUrl = 'http://' . $campaign->getDomain() . '/' . $page->getPageUrl();

        return $pageUrl;
    }
}
