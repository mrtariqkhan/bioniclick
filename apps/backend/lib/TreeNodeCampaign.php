<?php

class TreeNodeCampaign extends TreeNode {

    /**
     *
     * @param type $type one of TreeNode::NODE_TYPE_*
     * @return string
     */
    public static function getTableNameByType($type) {

        switch ($type) {
            case TreeNode::NODE_TYPE_CAMPAIGNS:
                $tableName = 'Advertiser';
                break;
            case TreeNode::NODE_TYPE_CAMPAIGN:
            case TreeNode::NODE_TYPE_PAGES:
                $tableName = 'Campaign';
                break;
/*             case TreeNode::NODE_TYPE_IVR:
                $tableName = 'ivr';
                break; */
            case TreeNode::NODE_TYPE_PAGE:
                $tableName = 'Page';
                break;
            case TreeNode::NODE_TYPE_COMBO:
            case TreeNode::NODE_TYPE_WIDGET:
            case TreeNode::NODE_TYPE_WIDGET_PHONE:
                $tableName = 'Combo';
                break;
            default:
                $tableName = null;
        }

        return $tableName;
    }




    public static function configureTreeNodeCampaigns($advertiser) {
        return self::configureTreeNode($advertiser, TreeNode::NODE_TYPE_CAMPAIGNS);
    }

    public static function configureTreeNodeCampaign($campaign) {
        return self::configureTreeNode($campaign, TreeNode::NODE_TYPE_CAMPAIGN);
    }

    public static function configureTreeNodePages($campaign) {
        return self::configureTreeNode($campaign, TreeNode::NODE_TYPE_PAGES);
    }

    public static function configureTreeNodePage($page) {
        return self::configureTreeNode($page, TreeNode::NODE_TYPE_PAGE);
    }

    public static function configureTreeNodeCombo($combo) {
        return self::configureTreeNode($combo, TreeNode::NODE_TYPE_COMBO);
    }

    public static function configureTreeNodeWidget($combo) {
        return self::configureTreeNode($combo, TreeNode::NODE_TYPE_WIDGET);
    }

    public static function configureTreeNodeWidgetPhone($combo) {
        return self::configureTreeNode($combo, TreeNode::NODE_TYPE_WIDGET_PHONE);
    }




    public static function generateTreeNodeKeyCampaigns($advertiser) {
        return self::generateTreeNodeKey($advertiser, TreeNode::NODE_TYPE_CAMPAIGNS);
    }

    public static function generateTreeNodeKeyCampaign($campaign) {
        return self::generateTreeNodeKey($campaign, TreeNode::NODE_TYPE_CAMPAIGN);
    }

    public static function generateTreeNodeKeyPages($campaign) {
        return self::generateTreeNodeKey($campaign, TreeNode::NODE_TYPE_PAGES);
    }

    public static function generateTreeNodeKeyPage($page) {
        return self::generateTreeNodeKey($page, TreeNode::NODE_TYPE_PAGE);
    }

    public static function generateTreeNodeKeyCombo($combo) {
        return self::generateTreeNodeKey($combo, TreeNode::NODE_TYPE_COMBO);
    }

    public static function generateTreeNodeKeyWidget($combo) {
        return self::generateTreeNodeKey($combo, TreeNode::NODE_TYPE_WIDGET);
    }

    public static function generateTreeNodeKeyWidgetPhone($combo) {
        return self::generateTreeNodeKey($combo, TreeNode::NODE_TYPE_WIDGET_PHONE);
    }
}
