<?php

class TreeNode {

    const NODE_TYPE_ROOT            = 'Root';
    const NODE_TYPE_BIONIC          = 'Bionic';
    const NODE_TYPE_RESELLERS       = 'Resellers';
    const NODE_TYPE_RESELLER        = 'Reseller';
    const NODE_TYPE_AGENCIES        = 'Agencies';
    const NODE_TYPE_AGENCY          = 'Agency';
    const NODE_TYPE_ADVERTISERS     = 'Advertisers';
    const NODE_TYPE_ADVERTISER      = 'Advertiser';
    const NODE_TYPE_CAMPAIGNS       = 'Campaigns';

    const NODE_TYPE_CAMPAIGN        = 'Campaign';
    const NODE_TYPE_PAGES           = 'Pages';
    const NODE_TYPE_PAGE            = 'Page';
    const NODE_TYPE_COMBO           = 'Combo';
    const NODE_TYPE_WIDGET          = 'Widget';
    const NODE_TYPE_WIDGET_PHONE    = 'WidgetPhone';

    const KEY_DEFAULT_LOAD          = 'default-load';

    const ARRAY_TYPE                = 'type';
    const ARRAY_PARAMETER           = 'parameter';

//    const PARAMETERS_GLUE = ',';


    private $key        = '';
    private $type       = '';
    private $parameter  = '';

    public function __construct() {

        $args = func_get_args();

        $argsTypesStr = '';
        foreach ($args as $arg) {
            $argsTypesStr .= '_' . gettype($arg);
        }

        if (method_exists($this, $constructorMethodName = '__construct' . $argsTypesStr)) {
            call_user_func_array(array($this, $constructorMethodName), $args);
        } else {
            throw new Exception('NO CONSTRUCTOR: ' . get_class() . $constructorMethodName);
        }
    }

    /**
     *
     * @param string $type one from TreeNode::NODE_TYPE_*
     * @param string $parameter string
     */
    public function __construct_string_string($type, $parameter) {

        $this->setType($type);
        $this->setParameter($parameter);

        $key = self::generateKey($type, $parameter);
        $this->setKey($key);
    }

    /**
     *
     * @param string $key string from TreeNode::generateKey
     */
    public function __construct_string($key) {

        $this->setKey($key);

        $info = $this->configureNodeByKey($key);
        $this->setType($info[self::ARRAY_TYPE]);
        $this->setParameter($info[self::ARRAY_PARAMETER]);
    }


    /**
     *
     * @param string $type one of TreeNode::NODE_TYPE_*
     * @param string $parameter
     * @return type
     */
    public static function generateKey($type, $parameter) {

        if ($type == self::NODE_TYPE_ROOT) {
            return self::KEY_DEFAULT_LOAD;
        }

//        $keyForType = implode(self::PARAMETERS_GLUE, $parameter);
        $keyForType = $parameter;
        $key = "$type $keyForType";

        return $key;
    }

    public static function getNodesListByKeyString($glue, $keyListStr = '') {

        $nodes = array();

        if (empty($keyListStr)) {
            return $nodes;
        }

        $keys = explode($glue, $keyListStr);
        foreach ($keys as $key) {
            if (!empty($key)) {
                $node = new TreeNode($key);
                $nodes[] = $node;
            }
        }

        return $nodes;
    }

    public static function nodesToArray($nodes = array()) {

        $nodesAsArray = array();

        if (empty($nodes)) {
            return $nodesAsArray;
        }

        foreach ($nodes as $node) {
            $nodeAsArr = $node->toArray();
            $nodesAsArray[$nodeAsArr[self::ARRAY_TYPE]][] = $nodeAsArr[self::ARRAY_PARAMETER];
        }

        return $nodesAsArray;
    }


    public function isEqualTo(TreeNode $node) {

        if ($node->getType() != $this->getType()) {
            return false;
        }

        if ($node->getParameter() != $this->getParameter()) {
            return false;
        }

        return true;
    }

    public function toArray() {

        $nodeAsArray = array(
            self::ARRAY_TYPE        => $this->getType(),
            self::ARRAY_PARAMETER   => $this->getParameter()
        );

        return $nodeAsArray;
    }

    public function getIfExistsInArray($nodesAsArray) {

        $type = $this->getType();

        $nodesAsArrayOfType = array_key_exists($type, $nodesAsArray) ? $nodesAsArray[$type] : array();

        if (!empty($nodesAsArrayOfType)) {
            $parameter = $this->getParameter();

            if (empty($parameter)) {
                return true;
            }

            $ind = array_search($parameter, $nodesAsArrayOfType);
            if ($ind !== false) {
                return true;
            }
        }

        return false;
    }


    private function configureNodeByKey($key) {

        $info = array(
            self::ARRAY_TYPE        => '',
            self::ARRAY_PARAMETER   => '',
        );

        if (empty($key) || $key == self::KEY_DEFAULT_LOAD) {
            $info[self::ARRAY_TYPE] = self::NODE_TYPE_ROOT;
            return $info;
        }

        $patternKey = '/([^\s]*)\s(.*)/';
        preg_match($patternKey, $key, $matches);
        if (!empty($matches)) {
            if (count($matches) > 0) {
                $info[self::ARRAY_TYPE] = $matches[1];
            }
            if (count($matches) > 1) {
//                $info[self::ARRAY_PARAMETER] = explode(self::PARAMETERS_GLUE, $matches[2]);
                $info[self::ARRAY_PARAMETER] = $matches[2];
            }
        }

        return $info;
    }


    public function getParameter() {
        return $this->parameter;
    }
    private function setParameter($parameter) {
        $this->parameter = $parameter;
    }

    public function getType() {
        return $this->type;
    }
    private function setType($type) {
        $this->type = $type;
    }

    public function getKey() {
        return $this->key;
    }
    private function setKey($key) {
        $this->key = $key;
    }


    public static function configureTreeNodeRoot() {
        return self::configureTreeNode(null, TreeNode::NODE_TYPE_ROOT);
    }

    public static function generateTreeNodeKeyRoot() {
        return self::generateTreeNodeKey(null, TreeNode::NODE_TYPE_ROOT);
    }

    protected static function configureTreeNode($object, $type) {

        $parentId = empty($object) ? '' : $object->getId();

        $treeNode = new TreeNode($type, $parentId);

        return $treeNode;
    }

    protected static function generateTreeNodeKey($object, $type) {
    
        $treeNode = TreeNodeCampaign::configureTreeNode($object, $type);
        $treeNodeKey = $treeNode->getKey();

        return $treeNodeKey;
    }
}
