<?php

class TreeActions extends GeneralActions {

    private $defaultNode        = array();
    private $selectedTreeNode   = null;
    private $expandedTreeNodes  = array();
    private $selectedTreeNodes  = array();

    public function preExecute() {

        parent::preExecute();

        $defaultNode = array(
            'title'             => '',
            'tooltip'           => '',
            'key'               => '',
            'url'               => null,
            'select'            => false,
            'isFolder'          => true,
            'isActive'          => false,
            'isLazy'            => true,
            'lazy_params'       => array(),
            'children'          => array(),
        );
        $this->setDefaultNode($defaultNode);

        $activeKey          = $this->getRequestParameter('activeKey', '');
        $expandedKeyListStr = $this->getRequestParameter('expandedKeyList', '');

        $this->configureSelectedTreeNode($activeKey);
        $this->configureExpandedTreeNodes($expandedKeyListStr);
    }

    /**
     *
     * @param type $type one of TreeNode::NODE_TYPE_*
     * @return string
     */
    public function getTableNameByType($type) {

        $tableName = TreeNodeBranching::getTableNameByType($type);

        if (!empty($tableName)) {
            return $tableName;
        }

        return TreeNodeCampaign::getTableNameByType($type);
    }

    /**
     *
     * @return array Data for dynatree
     */
    public function configureTree() {

        $this->configureSelectedTreeNodes();
        /*
        $children = $this->configureChildrenForRoot();

        $root = $this->configureNodeRoot();
        $root['children'] = $children;
        
        return $root;
        */
        
        return $this->configureChildrenForRoot();
    }

    public function configureChildrenForRoot() {

        if ($this->user->isCompanyUser()) {
            $company = $this->user->getCompany();
            return $this->configureChildrenForRootOfCompany($company);
        }

        if ($this->user->isAdvertiserUser()) {
            $advertiser = $this->user->getAdvertiser();
            return $this->configureChildrenForRootOfAdvertiser($advertiser);
        }

        return array();
    }

    protected function configureChildrenForRootOfCompany($company) {

        $children = $this->configureNodeCompany($company, true);

        return $children;
    }

    protected function configureChildrenForRootOfAdvertiser($advertiser) {

        $children = $this->configureNodeAdvertiser($advertiser, true);

        return $children;
    }

    public function configureChildrenForBionic($company, $ifParentIsSelected = true) {
        return $this->configureChildrenForCompany($company, $ifParentIsSelected);
    }

    public function configureChildrenForReseller($company, $ifParentIsSelected = true) {
        return $this->configureChildrenForCompany($company, $ifParentIsSelected);
    }

    public function configureChildrenForAgency($company, $ifParentIsSelected = true) {
        return $this->configureChildrenForCompany($company, $ifParentIsSelected);
    }

    protected function configureChildrenForCompany($company, $ifParentIsSelected = true) {

        $nodeAdvertisers = $this->configureNodeAdvertisers($company, $ifParentIsSelected);

        $level = $company->getLevel();
        switch ($level) {
            case CompanyTable::$LEVEL_BIONIC:
            case CompanyTable::$LEVEL_RESELLER:
                $nodeSubCompanies = $this->configureNodeSubCompanies($company, $ifParentIsSelected);
                break;
            default:
                $nodeSubCompanies = null;
        }

        $children = empty($nodeSubCompanies) ? $nodeAdvertisers : array($nodeAdvertisers, $nodeSubCompanies);

        return $children;
    }

    public function configureChildrenForResellers($parentCompany, $ifParentIsSelected = true) {
        return $this->configureChildrenForSubCompanies($parentCompany, $ifParentIsSelected);
    }

    public function configureChildrenForAgencies($parentCompany, $ifParentIsSelected = true) {
        return $this->configureChildrenForSubCompanies($parentCompany, $ifParentIsSelected);
    }

    protected function configureChildrenForSubCompanies($parentCompany, $ifParentIsSelected = true) {

        $level = $parentCompany->getLevel();
        switch ($level) {
            case CompanyTable::$LEVEL_BIONIC:
            case CompanyTable::$LEVEL_RESELLER:
                $companies = CompanyTable::findFirstIndex($this->user, $parentCompany);
                break;
            default:
                $this->forward404('There are no subcompanies.');
        }

        $children = array();
        if (!empty($companies)) {
            foreach ($companies as $company) {
                $children[] = $this->configureNodeCompany($company, $ifParentIsSelected);
            }
        }

        return $children;
    }

    public function configureChildrenForAdvertisers($company, $ifParentIsSelected = true) {

        $advertisers = AdvertiserTable::findFirstIndex($this->user, $company);
        $advertisers = empty($advertisers) ? array() : $advertisers;

        $children = array();
        foreach ($advertisers as $advertiser) {
            $children[] = $this->configureNodeAdvertiser($advertiser, $ifParentIsSelected);
        }

        return $children;
    }

    public function configureChildrenForAdvertiser($advertiser, $ifParentIsSelected = true) {

        $children = $this->configureNodeCampaigns($advertiser, $ifParentIsSelected);

        return $children;
    }

    public function configureChildrenForCampaigns($advertiser, $ifParentIsSelected = true) {

        $campaigns = CampaignTable::findCampaignsOrderedForAdvertiser($this->user, $advertiser);
        $campaigns = empty($campaigns) ? array() : $campaigns;

        $children = array();
        foreach ($campaigns as $campaign) {
            $children[] = $this->configureNodeCampaign($campaign, $ifParentIsSelected);
        }

        return $children;
    }

    public function configureChildrenForCampaign($campaign, $ifParentIsSelected = true) {

        $children = $this->configureNodePages($campaign, $ifParentIsSelected);

        return $children;
    }

    public function configureChildrenForPages($campaign, $ifParentIsSelected = true) {

        $pages = $campaign->findPagesOrdered($this->user);

        $children = array();
        foreach ($pages as $page) {
            $children[] = $this->configureNodePage($page, $ifParentIsSelected);
        }

        return $children;
    }

    public function configureChildrenForPage($page, $ifParentIsSelected = true) {

        $combos = $page->findCombosOrdered($this->user);

        $children = array();
        foreach ($combos as $combo) {
            $children[] = $this->configureNodeCombo($combo, $ifParentIsSelected);
        }

        return $children;
    }

    public function configureChildrenForCombo($combo, $ifParentIsSelected = true) {

        $phoneNode = $this->configureNodeWidgetPhone($combo);
        $htmlNode  = $this->configureNodeWidgetHtml($combo);

        $children = array($htmlNode, $phoneNode);

        return $children;
    }

    protected function configureNodeRoot() {

        $name = 'Navigation';

        $url = $this->generateUrl('sub_tree_filter');

//        if ($this->user->isCompanyUser()) {
//            $company = $this->user->getCompany();
//            $url = $this->generateUrl('sub_tree_company_index_full', array('parent_company_id' => $company->getId()));
//        }
//
//        if ($this->user->isAdvertiserUser()) {
//            $advertiser = $this->user->getAdvertiser();
//            $url = $this->generateUrl('sub_tree_advertiser_index_full', array('parent_company_id' => $company->getId()));
//        }
        
        $treeNode = TreeNode::configureTreeNodeRoot();

        $node = $this->getCommonNode($treeNode, 'root');
        $node['url']    = $url;

        return self::addNamesToNodeSimple($node, $name);
    }

    protected function countChildrenForAdvertisers($company) {
        return AdvertiserTable::countFirstIndex($this->user, $company);
    }

    protected function configureNodeCompany($company, $ifParentIsSelected) {

        $companyId  = $company->getId();
        $name       = $company->getName();
        $wasDeleted = $company->getWasDeleted();

        $level = $company->getLevel();
        switch ($level) {
            case CompanyTable::$LEVEL_BIONIC:
                $iconType = 'bionic';
                $treeNode = TreeNodeBranching::configureTreeNodeBionic($company);
                break;
            case CompanyTable::$LEVEL_RESELLER:
                $iconType = 'reseller';
                $treeNode = TreeNodeBranching::configureTreeNodeReseller($company);
                break;
            case CompanyTable::$LEVEL_AGENCY:
                $iconType = 'agency';
                $treeNode = TreeNodeBranching::configureTreeNodeAgency($company);
                break;
            default:
                $this->forward404('There is no such type of subcompany.');
        }

        //NOTE:: to decrease expanded nodes after reloading
        $levelUser = $this->user->getLevel();
        $isFirstTreeLevel = ($levelUser == $level);

        $children = array();
        if ($isFirstTreeLevel
            || $this->getIfOneOfNeedExpandedTreeNodes($treeNode, $ifParentIsSelected)
        ) {
            $isSelected = $isFirstTreeLevel ? false : $ifParentIsSelected || $this->isSelectedTreeNode($treeNode);
            $children = $this->configureChildrenForCompany($company, $isSelected);
        }

        $node = $this->getCommonNode($treeNode, $iconType, $wasDeleted, $children);
        $node['url']    = $this->generateUrl('company_show', array('id' => $companyId));

        return self::addNamesToNode($treeNode, $node, $name);
    }

    protected function configureNodeSubCompanies($parentCompany, $ifParentIsSelected) {

        $companyId  = $parentCompany->getId();
        $wasDeleted = $parentCompany->getWasDeleted();

        $level = $parentCompany->getLevel();
        switch ($level) {
            case CompanyTable::$LEVEL_BIONIC:
                $name = 'Resellers';
                $iconType = 'resellers';
                $treeNode = TreeNodeBranching::configureTreeNodeResellers($parentCompany);
                break;
            case CompanyTable::$LEVEL_RESELLER:
                $name = 'Agencies';
                $iconType = 'agencies';
                $treeNode = TreeNodeBranching::configureTreeNodeAgencies($parentCompany);
                break;
            default:
                $this->forward404('There are no subcompanies.');
        }

        $children = array();
        if ($this->getIfOneOfNeedExpandedTreeNodes($treeNode, $ifParentIsSelected)) {
            $isSelected = $ifParentIsSelected || $this->isSelectedTreeNode($treeNode);
            $children = $this->configureChildrenForSubCompanies($parentCompany, $isSelected);
        }

        $node = $this->getCommonNode($treeNode, $iconType, $wasDeleted, $children);
        $node['url']    = $this->generateUrl('sub_tree_company_index', array('parent_company_id' => $companyId));

        return self::addNamesToNodeSimple($node, $name);
    }

    protected function configureNodeAdvertisers($parentCompany, $ifParentIsSelected) {

        $companyId  = $parentCompany->getId();
        $wasDeleted = $parentCompany->getWasDeleted();
        $name = 'Advertisers';

        $treeNode = TreeNodeBranching::configureTreeNodeAdvertisers($parentCompany);

        $children   = array();
        
        if ($this->getIfOneOfNeedExpandedTreeNodes($treeNode, $ifParentIsSelected)) {
            $isSelected = $ifParentIsSelected || $this->isSelectedTreeNode($treeNode);
            $children = $this->configureChildrenForAdvertisers($parentCompany, $isSelected);
        }

        $node = $this->getCommonNode($treeNode, 'advertisers', $wasDeleted, $children,
            $this->countChildrenForAdvertisers($parentCompany)
        );
        
        $node['url']    = $this->generateUrl('sub_tree_advertiser_index', array('parent_company_id' => $companyId));

        return self::addNamesToNodeSimple($node, $name);
    }

    protected function configureNodeAdvertiser($advertiser, $ifParentIsSelected) {

        //NOTE:: to decrease expanded nodes after reloading
        $isFirstTreeLevel = $this->user->isAdvertiserUser();

        $advertiserId   = $advertiser->getId();
        $name           = $advertiser->getName();
        $wasDeleted     = $advertiser->getWasDeleted();

        $treeNode = TreeNodeBranching::configureTreeNodeAdvertiser($advertiser);

        $children = array();
        if ($isFirstTreeLevel
            || $this->getIfOneOfNeedExpandedTreeNodes($treeNode, $ifParentIsSelected)
        ) {
            $isSelected = $isFirstTreeLevel ? false : $ifParentIsSelected || $this->isSelectedTreeNode($treeNode);
            $children = $this->configureChildrenForAdvertiser($advertiser, $isSelected);
        }

        $node = $this->getCommonNode($treeNode, 'advertiser', $wasDeleted, $children);
        $node['url']    = $this->generateUrl('advertiser_show', array('id' => $advertiserId));

        return self::addNamesToNode($treeNode, $node, $name);
    }

    protected function configureNodeCampaigns($advertiser, $ifParentIsSelected) {

        $advertiserId   = $advertiser->getId();
        $wasDeleted     = $advertiser->getWasDeleted();
        $name = 'Campaigns';

        $treeNode = TreeNodeCampaign::configureTreeNodeCampaigns($advertiser);

        $children = array();
        if ($this->getIfOneOfNeedExpandedTreeNodes($treeNode, $ifParentIsSelected)) {
            $isSelected = $ifParentIsSelected || $this->isSelectedTreeNode($treeNode);
            $children = $this->configureChildrenForCampaigns($advertiser, $isSelected);
        }

        $node = $this->getCommonNode($treeNode, 'campaigns', $wasDeleted, $children);
        $node['url']    = $this->generateUrl('campaigns_for_advertiser', array('advertiser_id' => $advertiserId));

        return self::addNamesToNodeSimple($node, $name);
    }

    protected function configureNodeCampaign($campaign, $ifParentIsSelected) {

        $campaignId = $campaign->getId();
        $name       = $campaign->getName();
        $wasDeleted = $campaign->getWasDeleted();

        $treeNode = TreeNodeCampaign::configureTreeNodeCampaign($campaign);

        $children = array();
        if ($this->getIfOneOfNeedExpandedTreeNodes($treeNode, $ifParentIsSelected)) {
            $isSelected = $ifParentIsSelected || $this->isSelectedTreeNode($treeNode);
            $children = $this->configureChildrenForCampaign($campaign, $isSelected);
        }

        $node = $this->getCommonNode($treeNode, 'campaign', $wasDeleted, $children);
        $node['url']    = $this->generateUrl('campaign_show', array('id' => $campaignId));

        return self::addNamesToNode($treeNode, $node, $name);
    }

    protected function configureNodePages($campaign, $ifParentIsSelected) {

        $campaignId = $campaign->getId();
        $wasDeleted = $campaign->getWasDeleted();
        $name = 'Pages';

        $treeNode = TreeNodeCampaign::configureTreeNodePages($campaign);

        $children = array();
        if ($this->getIfOneOfNeedExpandedTreeNodes($treeNode, $ifParentIsSelected)) {
            $isSelected = $ifParentIsSelected || $this->isSelectedTreeNode($treeNode);
            $children = $this->configureChildrenForPages($campaign, $isSelected);
        }

        $node = $this->getCommonNode($treeNode, 'pages', $wasDeleted, $children);
        $node['url']    = $this->generateUrl('pages', array('campaign_id' => $campaignId));

        return self::addNamesToNodeSimple($node, $name);
    }

    protected function configureNodePage($page, $ifParentIsSelected) {

        $pageId     = $page->getId();
        $name       = $page->getPageUrl();
        $wasDeleted = $page->getWasDeleted();

        $treeNode = TreeNodeCampaign::configureTreeNodePage($page);

        $children = array();
        if ($this->getIfOneOfNeedExpandedTreeNodes($treeNode, $ifParentIsSelected)) {
            $isSelected = $ifParentIsSelected || $this->isSelectedTreeNode($treeNode);
            $children = $this->configureChildrenForPage($page, $isSelected);
        }

        $node = $this->getCommonNode($treeNode, 'page', $wasDeleted, $children);
        $node['url']    = $this->generateUrl('combos', array('page_id' => $pageId));

        return self::addNamesToNode($treeNode, $node, $name);
    }

    protected function configureNodeCombo($combo, $ifParentIsSelected) {

        $comboId    = $combo->getId();
        $name       = $combo->getName();
        $wasDeleted = $combo->getWasDeleted();

        $treeNode = TreeNodeCampaign::configureTreeNodeCombo($combo);

        $children = array();
        if ($this->getIfOneOfNeedExpandedTreeNodes($treeNode, $ifParentIsSelected)) {
            $isSelected = $ifParentIsSelected || $this->isSelectedTreeNode($treeNode);
            $children = $this->configureChildrenForCombo($combo, $isSelected);
        }
        
        $node = $this->getCommonNode($treeNode, 'combo', $wasDeleted, $children);
        $node['url'] = $this->generateUrl('combo_show', array('id' => $comboId));

        return self::addNamesToNode($treeNode, $node, $name);
    }

    protected function configureNodeWidgetHtml($combo) {

        $comboId    = $combo->getId();
        $wasDeleted = $combo->getWasDeleted();
        $name = 'html widgets';

        $treeNode = TreeNodeCampaign::configureTreeNodeWidget($combo);

        $node = $this->getCommonNode($treeNode, 'widgets-html', $wasDeleted);
        $node['url']      = $this->generateUrl('widget', array('combo_id' => $comboId));
        $node['isFolder'] = false;
        $node['isLazy']   = false;

        return self::addNamesToNodeSimple($node, $name);
    }

    protected function configureNodeWidgetPhone($combo) {

        $comboId    = $combo->getId();
        $wasDeleted = $combo->getWasDeleted();
        $name = 'phone widgets';

        $treeNode = TreeNodeCampaign::configureTreeNodeWidgetPhone($combo);

        $node = $this->getCommonNode($treeNode, 'widgets-phone', $wasDeleted);
        $node['url']      = $this->generateUrl('widget_phone', array('combo_id' => $comboId));
        $node['isFolder'] = false;
        $node['isLazy']   = false;

        return self::addNamesToNodeSimple($node, $name);
    }

    private function getDefaultNode() {
        return $this->defaultNode;
    }
    private function setDefaultNode($defaultNode) {
        $this->defaultNode = $defaultNode;
    }

    private function getSelectedTreeNode() {
        return $this->selectedTreeNode;
    }
    private function configureSelectedTreeNode($activeKey) {
        $this->selectedTreeNode = new TreeNode($activeKey);
    }
    private function isSelectedTreeNode($treeNode) {
        return $treeNode->isEqualTo($this->getSelectedTreeNode());
    }

    private function getExpandedTreeNodes() {
        return $this->expandedTreeNodes;
    }
    private function configureExpandedTreeNodes($expandedKeyListStr) {

        $nodes = TreeNode::getNodesListByKeyString(',', $expandedKeyListStr);
        $expandedTreeNodes = TreeNode::nodesToArray($nodes);

        $this->expandedTreeNodes = $expandedTreeNodes;
    }

    private function getIfOneOfNeedExpandedTreeNodes(TreeNode $treeNode, $ifParentIsSelected = true) {
////        return (
////            $this->getIfOneOfExpandedTreeNodes($treeNode)
////            && $this->getIfOneOfSelectedTreeNodes($treeNode)
////        );
//        return $this->getIfOneOfSelectedTreeNodes($treeNode);

        if ($ifParentIsSelected) {
            return $this->getIfOneOfExpandedTreeNodes($treeNode);
        }
        return $this->getIfOneOfSelectedTreeNodes($treeNode);
    }

    private function getIfOneOfExpandedTreeNodes(TreeNode $treeNode) {
        return $treeNode->getIfExistsInArray($this->getExpandedTreeNodes());
    }

    private function getIfOneOfSelectedTreeNodes(TreeNode $treeNode) {
        return $treeNode->getIfExistsInArray($this->getSelectedTreeNodes());
    }

    private function getSelectedTreeNodes() {
        return $this->selectedTreeNodes;
    }
    private function configureSelectedTreeNodes() {

        $treeNodes = array();

        $treeNode = $this->getSelectedTreeNode();

        $this->configureSelectedTreeNodesRecursive($treeNodes, $treeNode);

        $selectedTreeNodes = TreeNode::nodesToArray($treeNodes);
        $this->selectedTreeNodes = $selectedTreeNodes;
    }

    private function findObjectById($tableName, $id) {
        return Doctrine::getTable($tableName)->find($id);
    }

    private function configureEmptyTreeNodes(&$treeNodes) {

        $treeNodes = array();

        return false;
    }

    private function configureSelectedTreeNodesRecursive(&$treeNodes, $treeNode) {

        $type       = $treeNode->getType();
        $parameter  = $treeNode->getParameter();

        if ($type == TreeNode::NODE_TYPE_ROOT) {
            return;
        }

        $treeNodes[] = $treeNode;

        $nextTreeNode = null;
        switch ($type) {
            case TreeNode::NODE_TYPE_BIONIC:
                break;
            case TreeNode::NODE_TYPE_RESELLERS:
                $companyId = $parameter;

                $company = $this->findObjectById('Company', $companyId);
                if (empty($company)) {
                    return $this->configureEmptyTreeNodes($treeNodes);
                }

                $nextTreeNode = TreeNodeBranching::configureTreeNodeBionic($company);
                break;
            case TreeNode::NODE_TYPE_RESELLER:

                $level = $this->user->getLevel();
                switch ($level) {
                    case CompanyTable::$LEVEL_BIONIC:
                    case CompanyTable::$LEVEL_RESELLER:
                        $companyId = $parameter;

                        $company = $this->findObjectById('Company', $companyId);
                        $parentCompany = empty($company) ? null : $company->getParentCompany();
                        if (empty($parentCompany)) {
                            return $this->configureEmptyTreeNodes($treeNodes);
                        }

                        $nextTreeNode = TreeNodeBranching::configureTreeNodeResellers($parentCompany);
                        break;
                    default:
                        return $this->configureEmptyTreeNodes($treeNodes);
                }
                break;
            case TreeNode::NODE_TYPE_AGENCIES:

                $level = $this->user->getLevel();
                switch ($level) {
                    case CompanyTable::$LEVEL_BIONIC:
                        $companyId = $parameter;

                        $company = $this->findObjectById('Company', $companyId);
                        if (empty($company)) {
                            return $this->configureEmptyTreeNodes($treeNodes);
                        }

                        $nextTreeNode = TreeNodeBranching::configureTreeNodeReseller($company);
                        break;
                    default:
                        return $this->configureEmptyTreeNodes($treeNodes);
                }
                break;
            case TreeNode::NODE_TYPE_AGENCY:

                $level = $this->user->getLevel();
                switch ($level) {
                    case CompanyTable::$LEVEL_BIONIC:
                    case CompanyTable::$LEVEL_RESELLER:
                        $companyId = $parameter;

                        $company = $this->findObjectById('Company', $companyId);
                        $parentCompany = empty($company) ? null : $company->getParentCompany();
                        if (empty($parentCompany)) {
                            return $this->configureEmptyTreeNodes($treeNodes);
                        }

                        $nextTreeNode = TreeNodeBranching::configureTreeNodeAgencies($parentCompany);
                        break;
                    default:
                        return $this->configureEmptyTreeNodes($treeNodes);
                }
                break;
            case TreeNode::NODE_TYPE_ADVERTISERS:
                $companyId = $parameter;

                $company = $this->findObjectById('Company', $companyId);
                if (empty($company)) {
                    return $this->configureEmptyTreeNodes($treeNodes);
                }

                $level = empty($company) ? null : $company->getLevel();
                switch ($level) {
                    case CompanyTable::$LEVEL_BIONIC:
                        $nextTreeNode = TreeNodeBranching::configureTreeNodeBionic($company);
                        break;
                    case CompanyTable::$LEVEL_RESELLER:
                        $nextTreeNode = TreeNodeBranching::configureTreeNodeReseller($company);
                        break;
                    case CompanyTable::$LEVEL_AGENCY:
                        $nextTreeNode = TreeNodeBranching::configureTreeNodeAgency($company);
                        break;
                    default:
                        return $this->configureEmptyTreeNodes($treeNodes);
                }
                break;
            case TreeNode::NODE_TYPE_ADVERTISER:
                $advertiserId = $parameter;

                $advertiser = $this->findObjectById('Advertiser', $advertiserId);
                $company = empty($advertiser) ? null : $advertiser->getCompany();
                if (empty($company)) {
                    return $this->configureEmptyTreeNodes($treeNodes);
                }

                $nextTreeNode = TreeNodeBranching::configureTreeNodeAdvertisers($company);
                break;
            case TreeNode::NODE_TYPE_CAMPAIGNS:
                $advertiserId = $parameter;

                $advertiser = $this->findObjectById('Advertiser', $advertiserId);
                if (empty($advertiser)) {
                    return $this->configureEmptyTreeNodes($treeNodes);
                }

                $nextTreeNode = TreeNodeBranching::configureTreeNodeAdvertiser($advertiser);
                break;
            case TreeNode::NODE_TYPE_CAMPAIGN:
                $campaignId = $parameter;

                $campaign = $this->findObjectById('Campaign', $campaignId);
                $advertiser = empty($campaign) ? null : $campaign->getAdvertiser();
                if (empty($advertiser)) {
                    return $this->configureEmptyTreeNodes($treeNodes);
                }

                $nextTreeNode = TreeNodeCampaign::configureTreeNodeCampaigns($advertiser);
                break;
            case TreeNode::NODE_TYPE_PAGES:
                $campaignId = $parameter;

                $campaign = $this->findObjectById('Campaign', $campaignId);
                if (empty($campaign)) {
                    return $this->configureEmptyTreeNodes($treeNodes);
                }

                $nextTreeNode = TreeNodeCampaign::configureTreeNodeCampaign($campaign);
                break;
            case TreeNode::NODE_TYPE_PAGE:
                $pageId = $parameter;

                $page = $this->findObjectById('Page', $pageId);
                $campaign = empty($page) ? null : $page->getCampaign();
                if (empty($campaign)) {
                    return $this->configureEmptyTreeNodes($treeNodes);
                }

                $nextTreeNode = TreeNodeCampaign::configureTreeNodePages($campaign);
                break;
            case TreeNode::NODE_TYPE_COMBO:
                $comboId = $parameter;

                $combo = $this->findObjectById('Combo', $comboId);
                $page = empty($combo) ? null : $combo->getPage();
                if (empty($page)) {
                    return $this->configureEmptyTreeNodes($treeNodes);
                }

                $nextTreeNode = TreeNodeCampaign::configureTreeNodePage($page);
                break;
            case TreeNode::NODE_TYPE_WIDGET:
            case TreeNode::NODE_TYPE_WIDGET_PHONE:
                $comboId = $parameter;

                $combo = $this->findObjectById('Combo', $comboId);
                if (empty($combo)) {
                    return $this->configureEmptyTreeNodes($treeNodes);
                }

                $nextTreeNode = TreeNodeCampaign::configureTreeNodeCombo($combo);
                break;
            default:
                return $this->configureEmptyTreeNodes($treeNodes);
        }

        if (!empty($nextTreeNode)) {
            return $this->configureSelectedTreeNodesRecursive($treeNodes, $nextTreeNode);
        }

        return false;
    }

    protected function getCommonNode(TreeNode $treeNode, $iconType, $wasDeleted = false, $children = array(), $isLazy = true) {

        $node = $this->getDefaultNode();

        $node['children']       = $children;
        $node['key']            = $treeNode->getKey();
        $node['icon']           = self::generateIconName($iconType, $wasDeleted);
        
        if ($isLazy) {
            $node['lazy_params'] = $treeNode->toArray();
        } else {
            $node['isLazy'] = (bool)$isLazy;
        }
        
        return $node;
    }

    protected static function addNamesToNodeSimple($node, $nameOriginal) {

        $title      = $nameOriginal;
        $tooltip    = $nameOriginal;

        $node['title']  = $title;
        $node['tooltip']= $tooltip;

        return $node;
    }

    protected static function addNamesToNode(TreeNode $treeNode, $node, $nameOriginal) {

        $type = $treeNode->getType();
        $type = ($type == TreeNode::NODE_TYPE_BIONIC) ? 'Company' : $type;

        $title  = substr($nameOriginal, 0, 25) . (strlen($nameOriginal) > 26 ? '...' : '');
        $tooltip = "$type \"$nameOriginal\"";

        $node['title']  = $title;
        $node['tooltip']= $tooltip;

        return $node;
    }

    protected static function generateIconName($iconType, $wasDeleted = false) {

        $iconType = empty($iconType) ? 'default' : $iconType;

        $iconType = $wasDeleted ? "$iconType-deleted" : $iconType;
        $iconFileName = "$iconType.png";

        return $iconFileName;
    }
}