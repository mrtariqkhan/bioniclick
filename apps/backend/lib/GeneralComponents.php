<?php

/**
 * General components.
 *
 * @package    bionic
 * @subpackage lib
 * @author
 * @version    
 */

class GeneralComponents extends sfComponents {

    public function configurePermissionsBase() {

        $securityHelper = $this->configureSecurityHelper();

        return $securityHelper->checkBasePermissions();
    }

    public function configurePermissionsAll() {

        $securityHelper = $this->configureSecurityHelper();

        return $securityHelper->checkAllPermissions();
    }

    protected function configureSecurityHelper() {

        $className = get_class($this);
        $moduleName = str_replace('Components', '', $className);

        $securityHelper = new SecurityHelper($this, $moduleName);

        return $securityHelper;
    }
}
