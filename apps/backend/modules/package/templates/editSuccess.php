<?php $object = $form->getObject(); ?>

<h1>Edit package</h1>

<div class="inner">

    <?php $id = $object->getId(); ?>

    <?php $urlAction = url_for('package_update', array('id' => $id)); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'back_url'          => $url_after,
                'submit_name'       => 'Update',
                'form_class_name'   => 'ajax-form form-with-snippet',
                'cancel_attributes' => array('class' => 'ajax-form-cancel'),
            )
        );
    ?>
</div>