<?php $object = $form->getObject(); ?>

<h1>New Package</h1>

<div class="inner">

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $url_action,
                'back_url'          => $url_after,
                'submit_name'       => 'Save',
                'form_class_name'   => 'ajax-form form-with-snippet',
                'cancel_attributes' => array('class' => 'ajax-form-cancel'),
            )
        );
    ?>
</div>