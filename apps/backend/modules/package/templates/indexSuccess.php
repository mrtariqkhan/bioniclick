<?php use_helper('Link'); ?>
<?php use_helper('Pager'); ?>

<h2>Packages List</h2>

<div class="ajax-pagable">
    <?php
        include_partial(
            'list',
            array(
                'pager'                     => $pager,
                'permissions'               => $permissions,
                'need_was_deleted_column'   => $need_was_deleted_column,
            )
        );
    ?>
</div>

<?php if ($permissions['can_create']): ?>
    <?php
        $a_options = array();

        $url_new = url_for('package_new');
        $a_options['New'] = array(
            'href'  => $url_new,
            'class' => 'icon new ajax-form-load',
        );

        echo icon_links($a_options);
     ?>
 <?php endif; ?>