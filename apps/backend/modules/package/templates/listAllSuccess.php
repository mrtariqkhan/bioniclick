<?php
    include_partial(
        'listAll',
        array(
            'pager'         => $pager,
            'branch_data'   => $branch_data,
            'permissions'   => $permissions,
        )
    );
?>
