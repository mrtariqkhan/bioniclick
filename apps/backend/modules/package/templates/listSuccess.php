<?php
    include_partial(
        'list',
        array(
            'pager'                     => $pager,
            'permissions'               => $permissions,
            'need_was_deleted_column'   => $need_was_deleted_column,
        )
    );
?>
