<?php use_helper('Link'); ?>
<?php use_helper('Pager'); ?>

<div class="inner">
    <h2>Package List</h2>
    <div class="account-wrap clearfix">        
        <div class="ajax-filter-container">

            <?php if ($permissions['can_filter']): ?>
                <div class="filter-form-wrap">
                    <h1>Search</h1>
                    <div class="filter-form-wraper">
                        <?php
                            include_partial(
                                'global/form_ajax',
                                array(
                                    'form'              => $filter_form,
                                    'action_url'        => url_for('package_filter_all'),
                                    'submit_name'       => 'Go!',
                                    'back_url'          => '',
                                    'form_class_name'   => 'ajax-filter',
                                    'cancel_attributes' => array('class' => 'ajax-form-cancel')
                                )
                            );
                        ?>
                    </div>
                </div>
            <?php endif; ?>
            
            <div class="ajax-pagable">
                <?php
                    include_partial(
                        'listAll',
                        array(
                            'pager'         => $pager,
                            'branch_data'   => $branch_data,
                            'permissions'   => $permissions,
                        )
                    );
                ?>
            </div>

            <?php if ($permissions['can_create']): ?>
                <?php
                    $a_options = array();

                    $url_new = url_for('package_new_all');
                    $a_options['New'] = array(
                        'href'  => $url_new,
                        'class' => 'icon new ajax-form-load',
                    );

                    echo icon_links($a_options);
                 ?>
             <?php endif; ?>
        </div>
    </div>
</div>