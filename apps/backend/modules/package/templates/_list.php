<?php use_helper('Link'); ?>
<?php use_helper('Pager'); ?>

<?php
    $pagerHtmlString = pager(
        $pager->getRawValue(),
        sfConfig::get('app_pager_max_per_pages'),
        url_for('package_list'),
        null,
        'ajax-pager'
    );
?>

<p>
    <?php 
        include_partial(
            'global/pager_list_header',
            array(
                'pager' => $pager,
                'pagerHtmlString' => $pagerHtmlString
            )
        ); 
    ?>
</p>

<div class="table-block">
    <table class="table">
        <thead>
            <tr>
                <th class="first">&nbsp;</th>
                <th>Name</th>
                <th>Monthly fee</th>
                <th>Toll free number monthly fee</th>
                <th>Local number monthly fee</th>
                <th>Toll free number per minute</th>
                <th>Local number per minute</th>
                <?php if ($need_was_deleted_column) : ?>
                    <th><?php echo WasDeletedHelper::TITLE; ?></th>
                <?php endif; ?>
                <th class="last">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $list_buttons = array(
                    'Edit' => array(
                        'class' => 'icon edit ajax-form-load'
                    ),
                    'Delete' => array(
                        'class' => 'icon delete'
                    )
                );

                $legend_buttons = array();
            ?>
            <?php $i = $pager->getFirstIndice(); ?>
            <?php $list = $pager->getResults(); ?>
            <?php $list = $list->getRawValue(); ?>
            <?php $currencySign = sfConfig::get('app_package_default_currency_sign', null); ?>
            <?php foreach ($list as $object): ?>
                <tr class="<?php echo ($i%2) ? 'odd' : 'even'; ?>">
                    <td><?php echo $i; ?></td>
                    <td><?php echo $object->getName(); ?></td>
                    <td><?php echo $currencySign . $object->getMonthlyFee(); ?></td>
                    <td><?php echo $currencySign . $object->getTollFreeNumberMonthlyFee(); ?></td>
                    <td><?php echo $currencySign . $object->getLocalNumberMonthlyFee(); ?></td>
                    <td><?php echo $currencySign . $object->getTollFreeNumberPerMinute(); ?></td>
                    <td><?php echo $currencySign . $object->getLocalNumberPerMinute(); ?></td>

                    <?php if ($need_was_deleted_column) : ?>
                        <td><?php echo $object->getWasDeletedString(); ?></td>
                    <?php endif; ?>

                    <td>
                        <?php $id = $object->getId(); ?>
                        <?php $urlAfter = url_for('package_index', array()); ?>
                        <?php
                            $a_options = array();

                            $ifShowUpdateLinks = WasDeletedHelper::ifCanUpdate($object);
                            if ($ifShowUpdateLinks) {
                                if ($permissions['can_update']) {
                                    $a_options['Edit'] = $list_buttons['Edit'];
                                    $a_options['Edit']['href'] = url_for('package_edit', array('id' => $id));

                                    $legend_buttons['Edit'] = & $list_buttons['Edit'];
                                }

                                if ($permissions['can_delete']) {
                                    $a_options['Delete'] = link_to_ajax_delete(
                                        '',
                                        url_for('package_delete', array('id' => $id)),
                                        array(
                                            'title' => 'Delete',
                                            'class' => 'icon delete',
                                            'after_url' => $urlAfter
                                        )
                                    );

                                    $legend_buttons['Delete'] = & $list_buttons['Delete'];
                                }
                            }

                            echo icon_links($a_options, array('class' => 'icon-links clearfix'), false);
                        ?>
                    </td>
                </tr>
                <?php ++$i; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php
    include_partial(
         'global/legend',
          array(
            'legend_buttons'  => $legend_buttons,
          )
    ); 
?>

<?php echo $pagerHtmlString; ?>
