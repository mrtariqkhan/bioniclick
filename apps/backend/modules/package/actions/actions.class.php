<?php

/**
 * package actions.
 *
 * @package    bionic
 * @subpackage package
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class packageActions extends GeneralActions {

    public function executeIndexAll(sfWebRequest $request) {

        $this->executeListAll($request);
        $this->setTemplate('indexAll');

        $filterValues = $this->getFilter();
        $this->filter_form = $this->getFilterForm();
        $this->filter_form->setDefaults($filterValues);
    }

    public function executeListAll(sfWebRequest $request) {

        $filterValues = $this->getFilter();
        $filterForm = $this->getFilterForm();        
        $queryParams = $filterForm->getFilteredValues($filterValues, true);

        $specialQuery = PackageTable::createQueryIndexAll($this->user, $queryParams);
        $this->pager = parent::initPager($request, $specialQuery);
        $list = $this->pager->getResults();

        $objectFieldsHeaders = array(
            'getName'                       => 'Name',
            'getMonthlyFee'                 => 'Monthly fee',
            'getTollFreeNumberMonthlyFee'   => 'Toll free number monthly fee',
            'getLocalNumberMonthlyFee'      => 'Local number monthly fee',
            'getTollFreeNumberPerMinute'    => 'Toll free number per minutee',
            'getLocalNumberPerMinute'       => 'Local number per minute',
        );
        WasDeletedHelper::addHeader($this->user, $objectFieldsHeaders);

        $branchHelper = new BranchHelperCompany();
        $this->branch_data = $branchHelper->makeRaws($this->user, $list, $objectFieldsHeaders);
    }

    public function executeIndex(sfWebRequest $request) {

        $this->executeList($request);
        $this->setTemplate('index');
    }

    public function executeList(sfWebRequest $request) {

        $specialQuery = PackageTable::createQueryIndex($this->user);
        $this->pager = parent::initPager($request, $specialQuery);
        $this->need_was_deleted_column = WasDeletedHelper::ifNeedWasDeletedColumn($this->user);
    }

    public function executeNew(sfWebRequest $request) {

        $object = new Package();
        $this->form = new PackageForm($object);

        $this->url_after = $this->generateUrl('package_index');
        $this->url_action = $this->generateUrl('package_create');
    }

    public function executeNewAll(sfWebRequest $request) {

        $this->executeNew($request);
        $this->setTemplate('new');

        $this->url_after = $this->generateUrl('package_index_all');
    }

    public function executeCreate(sfWebRequest $request) {

        $object = new Package();
        $object->setCompanyId($this->user->getCompanyId());
        $form = new PackageForm($object);

        return $this->processAjaxForm($request, $form);
    }

    public function executeCustomerCompanyNew(sfWebRequest $request) {

        $companyId = $request->getParameter('company_id', null);
        $company = CompanyTable::getInstance()->find($companyId);
        $this->forward404If(empty($company), 'Company has not been found.');

        $object = new Package();
        $this->form = new PackageForm($object);

        $this->url_after = $this->generateUrl('company_index');
        $this->url_action = $this->generateUrl('customer_company_package_create', array('company_id' => $companyId));
        $this->setTemplate('new');
    }

    public function executeCustomerCompanyCreate(sfWebRequest $request) {

        $companyId = $request->getParameter('company_id', null);
        $company = CompanyTable::getInstance()->find($companyId);
        $this->forward404If(empty($company), 'Company has not been found.');

        $object = new Package();
        $object->setCompanyId($companyId);
        $form = new PackageForm($object);

        return $this->processAjaxForm($request, $form);
    }

    public function executeEdit(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);
        $this->form = new PackageForm($object);

        $this->url_after = $this->generateUrl('package_index');
    }

    public function executeEditAll(sfWebRequest $request) {

        $this->executeEdit($request);
        $this->setTemplate('edit');

        $this->url_after = $this->generateUrl('package_index_all');
    }

    public function executeUpdate(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);
        $form = new PackageForm($object);

        return $this->processAjaxForm($request, $form);
    }

    public function executeDelete(sfWebRequest $request) {
        return $this->processAjaxDelete($request);
    }

    protected function deleteObject($object) {
        $object->safeDelete();
    }

    public function executeFilterAll(sfWebRequest $request) {
        $filterForm = $this->getFilterForm();
        return $this->processAjaxFilter($request, $filterForm);
    }

    protected function getFilterForm() {
        $options = $this->getBranchFilterOptions();
        return new PackageFilterForm(null, $options);
    }

    protected function getBranchFilterOptions() {

        $options = parent::getBranchFilterOptions();
        unset($options[CompanyTable::$LEVEL_NAME_ADVERTISER]);

        return $options;
    }
}
