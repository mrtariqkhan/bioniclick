<?php $object = $form->getObject(); ?>

<h1>New business phone number</h1>

<div class="inner">

    <?php $campaignId = $object->getCampaignId(); ?>

    <?php $urlAction = url_for('physical_phone_number_create'); ?>
    <?php $urlAfter = url_for('campaign_show', array('id' => $campaignId)); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'back_url'          => $urlAfter,
                'submit_name'       => 'Create',
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel'),
                'form_special_data' => ''
            )
        );
    ?>
</div>
