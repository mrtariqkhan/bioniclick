
<?php
    include_component(
        'physicalPhoneNumber',
        'list',
        array(
            'campaign'      => $campaign,
            'pager'         => $pager
        )
    );
?>

<?php
      $a_options = array();
      if ($permissions['can_create']) {
          $a_options['New'] = array(
              'href' => url_for('physical_phone_number_new', array('campaign_id' => $campaign->getId())),
              'class' => "ajax-load icon new"
          );
      }
      echo icon_links($a_options);
 ?>
