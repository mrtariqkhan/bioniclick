
<?php $object = $form->getObject(); ?>

<h1>Edit business phone number</h1>

<div class="inner">
    <?php $id = $object->getId(); ?>
    <?php $campaignId = $object->getCampaignId(); ?>

    <?php $urlAction = url_for('physical_phone_number_update', array('id' => $id)); ?>
    <?php $urlAfter = url_for('campaign_show', array('id' => $campaignId)); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'back_url'          => $urlAfter,
                'submit_name'       => 'Update',
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel'),
                'form_special_data' => ''
            )
        );
    ?>
</div>
