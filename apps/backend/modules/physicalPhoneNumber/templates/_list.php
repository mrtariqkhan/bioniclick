<?php use_helper('Link'); ?>
<?php use_helper('Pager'); ?>
       
<?php if ($permissions['can_read']): ?>

    <?php $campaignId = $campaign->getId(); ?>

    <?php $ifShowUpdateLinks = WasDeletedHelper::ifCanUpdate($campaign->getRawValue()); ?>

    <?php 
        $pagerHtmlString = pager(
            $pager->getRawValue(),
            sfConfig::get('app_pager_max_per_pages'),
            url_for('physical_phone_number_list', array('campaign_id' => $campaign->getId())),
            null,
            'ajax-pager'
        );
    ?>

    <p>
        <?php 
            include_partial(
                'global/pager_list_header',
                array(
                    'pager' => $pager,
                    'pagerHtmlString' => $pagerHtmlString
                )
            ); 
        ?>
    </p>

    <div class="table-block">
        <table class="table">
            <thead>
                <tr>
                    <th class="first">&nbsp;</th>
                    <th>Business Phone Number</th>
                    <th class="last">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $list_buttons = array(
                        'Edit' => array(
                            'class' => 'icon edit ajax-load'
                        ),
                        'Delete' => array(
                            'class' => 'icon delete'
                        )
                    );

                    $legend_buttons = array();
                ?>
                <?php $i = $pager->getFirstIndice(); ?>
                <?php $list = $pager->getResults(); ?>
                <?php $list = $list->getRawValue(); ?>
                <?php foreach ($list as $object): ?>
                    <tr class="<?php echo ($i%2) ? 'odd' : 'even'; ?>">
                        <td><?php echo $i; ?></td>
                        <td><?php echo $object->getPhoneNumber(); ?></td>
                        <td>
                            <?php $id = $object->getId(); ?>
                            <?php $urlEdit = url_for('physical_phone_number_edit', array('id' => $id)); ?>
                            <?php $urlDelete = url_for('physical_phone_number_delete', array('id' => $id)); ?>
                            <?php $urlAfter = url_for('campaign_show', array('id' => $campaignId)); ?>
                            <?php
                                $a_options = array();

                                if ($ifShowUpdateLinks) {
                                    if ($permissions['can_update']) {
                                        $a_options['Edit'] = $list_buttons['Edit'];
                                        $a_options['Edit']['href'] = $urlEdit;

                                        $legend_buttons['Edit'] = & $list_buttons['Edit'];
                                    }

                                    if ($permissions['can_delete']) {
                                        $a_options['Delete'] = link_to_ajax_delete(
                                            '',
                                            $urlDelete,
                                            array(
                                                'title'     => 'Delete',
                                                'class'     => 'icon delete',
                                                'after_url' => $urlAfter
                                            )
                                        );

                                        $legend_buttons['Delete'] = & $list_buttons['Delete'];
                                    }
                                }

                                echo icon_links($a_options, array('class' => 'icon-links clearfix'), false);
                            ?>
                        </td>
                    </tr>
                    <?php ++$i; ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <?php
        include_partial(
             'global/legend',
              array(
                'legend_buttons'  => $legend_buttons,
              )
        ); 
    ?>

    <?php echo $pagerHtmlString; ?>    
    
<?php endif; ?>
