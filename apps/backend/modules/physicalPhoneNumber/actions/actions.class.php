<?php

/**
 * physicalPhoneNumber actions.
 *
 * @package    bionic
 * @subpackage physicalPhoneNumber
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class physicalPhoneNumberActions extends GeneralActions {

    public function  preExecute() {

        $this->tableName = 'PhysicalPhoneNumber';
        parent::preExecute();
        $this->getResponse()->addStylesheet(
            '/css/project/reset.css',
            'last'
        );
    }

    public function executeIndex(sfWebRequest $request) {

    }
    
    public function executeList(sfWebRequest $request) {
        
        $campaignId = $request->getParameter('campaign_id');
        $campaign = CampaignTable::findById($campaignId);
        $this->forward404If(
            empty($campaign),
            sprintf('Object campaign does not exist (%s).', $campaignId)
        );
        $this->checkIsAllowableObject($campaign, false);

        $specialQuery = PhysicalPhoneNumberTable::getInstance()
            ->getIndexQuery($campaignId);
        $this->pager = $this->initPager($request, $specialQuery);
        $this->campaign = $campaign;
    }

    public function executeNew(sfRequest $request) {

        $campaignId = $request->getParameter('campaign_id');
        $campaign = CampaignTable::findById($campaignId);
        $this->forward404If(
            empty($campaign),
            sprintf('Object campaign does not exist (%s).', $campaignId)
        );
        $this->checkIsAllowableObject($campaign);

        $object = new PhysicalPhoneNumber();
        $object->setCampaign($campaign);

        $this->form = new PhysicalPhoneNumberForm($object);
    }

    public function executeCreate(sfRequest $request) {

        $form = new PhysicalPhoneNumberForm();

        return $this->processAjaxForm($request, $form);
    }

    public function executeEdit(sfRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $this->form = new PhysicalPhoneNumberForm($object);
    }

    public function executeUpdate(sfRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);
        $form = new PhysicalPhoneNumberForm($object);

        return $this->processAjaxForm($request, $form);
    }

    public function executeDelete(sfRequest $request) {
        return $this->processAjaxDelete($request);
    }
}