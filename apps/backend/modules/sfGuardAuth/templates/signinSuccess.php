<?php use_helper('I18N'); ?>
<div class="wrap-login">
    <h1><?php echo __('Sign in', null, 'sf_guard'); ?></h1>
    <div class="content-login">
        <?php
            include_partial(
                'global/form',
                array(
                    'form' => $form,
                    'action_url' =>  url_for('@sf_guard_signin'),
                    'submit_name' => 'Sign in'
                )
            );
        ?>
        <script>
            $('#signin_username').focus();
        </script>
<!--
        <?php //echo link_to("Create Account", "@apply"); ?>
        <br/>
-->
        <?php echo link_to("Forgot password?", "@resetRequest"); ?>
    </div>
</div>