<?php

require_once(sfConfig::get('sf_plugins_dir').'/sfDoctrineGuardPlugin/modules/sfGuardAuth/lib/BasesfGuardAuthActions.class.php');

class sfGuardAuthActions extends BasesfGuardAuthActions {

    public function preExecute() {

        $this->getResponse()->addStylesheet('/css/project/reset.css', 'last');
        //TODO::think about user colors...
        $skin = CompanyTable::SKIN_DEFAULT;
        $this->getResponse()->addStylesheet("/css/project/skins/login_$skin.css", 'last');
        parent::preExecute();
    }

    public function executeSignover($request) {

        $user = $this->getUser();
        if (!$user->isAuthenticated()) {
            if ($request->isXmlHttpRequest()) {
                $this->getResponse()->setHeaderOnly(true);
                $this->getResponse()->setStatusCode(401);

                return sfView::NONE;
            }
        }

        $this->forward('sfGuardAuth', 'signin');
    }

    public function  executeSignin($request) {
        $this->getResponse()->addStylesheet('/css/project/login.css', 'last');
        return parent::executeSignin($request);
    }

}
