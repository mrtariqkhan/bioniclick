<?php

/**
 * errors actions.
 *
 * @package    bionic
 * @subpackage errors
 * @author     Efremochkin Yury <yury.efremochkin@fairdevteam.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class errorsActions extends GeneralActions {
    
    public function executeJsonRpc(sfWebRequest $request) {
        
        $this->getResponse()->setStatusCode(403);
        $this->getResponse()->setHttpHeader('Content-Type','application/json');
        
        return $this->renderText(json_encode(array(
            'jsonrpc' => '2.0',
            'error'   => array(
                'code'    => $this->getRequest()->getParameter('error.code'),
                'message' => $this->getRequest()->getParameter('error.message')
            ),
            'id' => null
        )));
    }
}