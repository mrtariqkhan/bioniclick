<?php

/**
 * incomingNumbersQueue actions.
 *
 * @package    bionic
 * @subpackage incomingNumbersQueue
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class incomingNumbersQueueActions extends GeneralActions {

    public function preExecute() {

        $this->tableName = 'IncomingNumbersQueue';
        parent::preExecute();
    }

    public function executeIndex(sfWebRequest $request) {

        $this->executeList($request);
    }

    public function executeList(sfWebRequest $request) {

        $specialQuery = null;

        $this->params_additional = array();
        $this->urlBack = null;
        $this->routePostfix = $request->getParameter('routePostfix', '');
        switch ($this->routePostfix) {
            case 'User':
                $specialQuery = IncomingNumbersQueueTable::createQueryIndexByUserId($this->userId);

                $this->urlList = $this->generateUrl('incomingNumbersQueue_listUser');
                $this->urlIndex = $this->generateUrl('incomingNumbersQueue_indexUser');
                break;
            case 'Advertiser':
                $advertiserId = $request->getParameter('advertiser_id');
                $specialQuery = IncomingNumbersQueueTable::createQueryIndexByAdvertiserId($advertiserId);

                $this->urlList = $this->generateUrl('incomingNumbersQueue_listAdvertiser', array('advertiser_id' => $advertiserId));
                $this->urlIndex = $this->generateUrl('incomingNumbersQueue_indexAdvertiser', array('advertiser_id' => $advertiserId));
                $this->urlBack = $this->generateUrl('advertiser_show', array('id' => $advertiserId));
                break;
            case 'Company':
                $companyId = $request->getParameter('company_id');
                $company = CompanyTable::getInstance()->find($companyId);
                $this->forward404Unless(!empty($company), 'Cannot find Company.');
                $specialQuery = IncomingNumbersQueueTable::createQueryIndexByCompany($company);

                $this->urlList = $this->generateUrl('incomingNumbersQueue_listCompany', array('company_id' => $companyId));
                $this->urlIndex = $this->generateUrl('incomingNumbersQueue_indexCompany', array('company_id' => $companyId));
                $this->urlBack = $this->generateUrl('company_show', array('id' => $companyId));
                $this->params_additional = array('company_id' => $companyId);
                break;
            default:
                $specialQuery = IncomingNumbersQueueTable::createQueryIndex();
                $this->urlList = $this->generateUrl('incomingNumbersQueue_list');
                $this->urlIndex = $this->generateUrl('incomingNumbersQueue_index');
        }

        $this->pager = parent::initPager($request, $specialQuery);
        $this->list = $this->pager->getResults();

        $securityHelperTIPN = new SecurityHelper($this, 'twilioIncomingPhoneNumber');
        $this->permissionsTIPN = $securityHelperTIPN->checkBasePermissions();

        $this->timezoneName = $this->user->findTimezoneName();
    }

    public function executeShow(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $isAllowable = $object->isAllowable($this->user, false, false);
        $this->forward404Unless($isAllowable);

        $this->params_additional = array();
        $this->urlBack = null;
        $this->routePostfix = $request->getParameter('routePostfix', '');
        switch ($this->routePostfix) {
            case 'User':
                $this->urlBack = $this->generateUrl('incomingNumbersQueue_indexUser');
                break;
            case 'Advertiser':
                $advertiserId = $object->getAdvertiserId();

                $this->urlBack = $this->generateUrl('incomingNumbersQueue_indexAdvertiser', array('advertiser_id' => $advertiserId));
                $this->params_additional = array('advertiser_id' => $advertiserId);
                break;
            case 'Company':
                $companyId = $request->getParameter('company_id');
                $company = CompanyTable::getInstance()->find($companyId);
                $this->forward404Unless(!empty($company), 'Cannot find Company.');

                $this->urlBack = $this->generateUrl('incomingNumbersQueue_indexCompany', array('company_id' => $companyId));
                $this->params_additional = array('company_id' => $companyId);
                break;
            default:
                $this->urlBack = $this->generateUrl('incomingNumbersQueue_index');
        }

        $this->order = $object;

        $securityHelperTIPN = new SecurityHelper($this, 'twilioIncomingPhoneNumber');
        $this->permissionsTIPN = $securityHelperTIPN->checkBasePermissions();

        $this->timezoneName = $this->user->findTimezoneName();

        $orderId = $object->getId();
        $specialQuery = AdvertiserIncomingNumberPoolLogTable::createIndexQueryByOrderId(
            $orderId
        );
        $this->pagerDetails = parent::initPager($request, $specialQuery);
    }

    public function executeCopy(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $isAllowable = $object->isAllowable($this->user, false, false);
        $this->forward404Unless($isAllowable);

        switch ($object->getOrderType()) {
            case IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE:
            case IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE_AND_ASSIGN:
                $this->setTemplate('new', 'advertiserIncomingNumberPool');
                $this->form = $this->configureFormPurchase($object, false);
                $this->advertiser = $object->getAdvertiser();
                break;

            case IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_ASSIGN:
                $this->setTemplate('addNumbersNew', 'campaign');
                $this->form = $this->configureFormAssignCopy($object);
                break;

            default:
                throw new Exception('Cannot copy this order of such type. May be yet.');
        }

        $routePostfix = $request->getParameter('routePostfix', '');
        switch ($routePostfix) {
            case 'Advertiser':
                $this->url_after = $this->generateUrl('incomingNumbersQueue_indexAdvertiser', array('advertiser_id' => $object->getAdvertiserId()));
                break;
            default:
                $this->url_after = $this->generateUrl('incomingNumbersQueue_index' . $routePostfix );
        }
    }

    public function executeCopyCompany(sfWebRequest $request) {

        $companyId = $request->getParameter('company_id', null);
        $company = CompanyTable::getInstance()->find($companyId);
        $this->forward404Unless(!empty($company), 'Cannot find Company.');
        $this->checkIsAllowableObject($company);

        $this->executeCopy($request);

        $this->urlAfter = $this->generateUrl('incomingNumbersQueue_indexCompany', array('company_id' => $companyId));
    }

    public function executeEdit(sfWebRequest $request) {

        $this->setTemplate('edit');

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObjectJson($object, 'You cannot edit information on this order.', 250009);

        if ($object->getIsBlocked()) {
            throw new jsonRpcException('Cannot edit this order is blocked now.', 250010);
        }

        //TODO:: think about Exception codes 
        switch ($object->getOrderType()) {
            case IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE:
            case IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE_AND_ASSIGN:
                break;
            default:
                throw new Exception('Cannot edit this order of such type. May be yet.');
        }

        $this->form = $this->configureFormPurchase($object, true);
        $this->advertiser = $object->getAdvertiser();
        $this->incomingNumbersQueueId = $object->getId();

        $routePostfix = $request->getParameter('routePostfix', '');
        switch ($routePostfix) {
            case 'Advertiser':
                $this->url_after = $this->generateUrl('incomingNumbersQueue_indexAdvertiser', array('advertiser_id' => $object->getAdvertiserId()));
                break;
            default:
                $this->url_after = $this->generateUrl('incomingNumbersQueue_index' . $routePostfix );
        }
    }

    public function executeEditCompany(sfWebRequest $request) {
        
        $companyId = $request->getParameter('company_id', null);
        $company = CompanyTable::getInstance()->find($companyId);
        $this->checkIsAllowableObjectJson($company, 'You cannot edit information on this company.', 250013);
        
        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObjectJson($object, 'You cannot edit information on this order.', 250014);

        $this->executeEdit($request);

        $this->url_after = $this->generateUrl('incomingNumbersQueue_indexCompany', array('company_id' => $companyId));
    }

    public function executeUpdate(sfWebRequest $request) {

        $objectId = $request->getParameter('incoming_numbers_queue_id', null);
        $object = IncomingNumbersQueueTable::getInstance()->find($objectId);

        $this->checkIsAllowableObjectJson($object, 'You cannot edit information on this order.', 250012);
        
        if ($object->getIsBlocked()) {
            throw new jsonRpcException('Cannot edit this order is blocked now.', 250011);
        }

        //TODO:: think about Exception codes 
        switch ($object->getOrderType()) {
            case IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE:
            case IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE_AND_ASSIGN:
                break;
            default:
                throw new Exception('Cannot update this order of such type. May be yet.');
        }

        $form = $this->configureFormPurchase($object, true);

        return $this->processAjaxForm($request, $form);
    }

    public function executeClose(sfWebRequest $request) {
        return $this->processAjaxDelete($request);
    }

    protected function deleteObject($object) {
        $object->close($this->userId);
    }
    
    public function executeBuyNumbersTask(sfWebRequest $request) {

        return $this->processTask(
            'BuyVoIpNumbersTask',
            array(),
            array(
                'url'           => 'http://fairdevteam.dyndns.org:8010/backend_dev.php/incomingCalls/create',
                'friendlyName'  => 'testing',
            )
        );
    }
    
    public function executeRestoreNumberLogTask(sfWebRequest $request) {

        return $this->processTask(
            'RestoreNumberLogTask',
            array(),
            array(
            )
        );
    }

    protected function configureFormPurchase($object, $toEdit) {

        $lac = $object->getTwilioLocalAreaCode();

        $formOptions = array(
            AdvertiserIncomingNumberPoolFormNew::OPTION_USER                            => $this->user,
            AdvertiserIncomingNumberPoolFormNew::OPTION_PHONE_NUMBER_TYPE_CODE          => $lac->getPhoneNumberTypeCode(),
            AdvertiserIncomingNumberPoolFormNew::OPTION_PHONE_NUMBER_COUNTRY_ID         => $lac->getPhoneNumberCountryId(),
            AdvertiserIncomingNumberPoolFormNew::OPTION_PHONE_NUMBER_LOCAL_AREA_CODE_ID => $lac->getId(),
            AdvertiserIncomingNumberPoolFormNew::OPTION_PHONE_NUMBERS_COUNT             => $object->getAmount(),
            AdvertiserIncomingNumberPoolFormNew::OPTION_INCOMING_NUMBERS_QUEUE          => ($toEdit) ? $object : null,
            AdvertiserIncomingNumberPoolFormNew::OPTION_PHONE_NUMBER_NOTES              => $object->getAuthorNotes(),
        );

        $objectAINP = $object->copyAdvertiserIncomingNumberPool();
        $form = new AdvertiserIncomingNumberPoolFormNew($objectAINP, $formOptions);

        return $form;
    }

    /**
     *
     * @param IncomingNumbersQueue $object
     * @param boolean $toEdit
     * @return CampaignAdvertiserIncomingNumberPoolFormEdit 
     */
    protected function configureFormAssignCopy($object) {

        $lac        = $object->getTwilioLocalAreaCode();
        $campaignId = $object->getCampaignId();

        $formOptions = array(
            CampaignAdvertiserIncomingNumberPoolFormEdit::OPTION_USER                           => $this->user,
            CampaignAdvertiserIncomingNumberPoolFormEdit::OPTION_PHONE_NUMBER_TYPE_CODE         => $lac->getPhoneNumberTypeCode(),
            CampaignAdvertiserIncomingNumberPoolFormEdit::OPTION_PHONE_NUMBER_COUNTRY_ID        => $lac->getPhoneNumberCountryId(),
            CampaignAdvertiserIncomingNumberPoolFormEdit::OPTION_PHONE_NUMBER_LOCAL_AREA_CODE_ID=> $lac->getId(),
            CampaignAdvertiserIncomingNumberPoolFormEdit::OPTION_PHONE_NUMBERS_COUNT            => $object->getAmount(),
            CampaignAdvertiserIncomingNumberPoolFormEdit::OPTION_PHONE_NUMBER_NOTES             => $object->getAuthorNotes(),
        );

        $campaign = CampaignTable::findById($campaignId);
        $object = $campaign;

        $form = new CampaignAdvertiserIncomingNumberPoolFormEdit($object, $formOptions);
        
        return $form;
    }
}
