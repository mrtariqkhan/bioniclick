
<h2>Order List</h2>

<div>
    <div class="ajax-pagable">
        <?php
            include_partial(
                'list',
                array(
                    'pager'             => $pager,
                    'permissions'       => $permissions,
                    'permissionsTIPN'   => $permissionsTIPN,
                    'raws'              => $list,
                    'timezoneName'      => $timezoneName,
                    'routePostfix'      => $routePostfix,
                    'urlList'           => $urlList,
                    'urlIndex'          => $urlIndex,
                    'params_additional' => $params_additional,
                )
            );
        ?>
    </div>

    <?php
        $a_options = array();

        if (!empty($urlBack)) {
            if ($permissions['can_read']) {
                $a_options['Back'] = array(
                    'href'  => $urlBack,
                    'class' => "icon back ajax-load"
                );
            }
        }

        echo icon_links($a_options);
    ?>
</div>