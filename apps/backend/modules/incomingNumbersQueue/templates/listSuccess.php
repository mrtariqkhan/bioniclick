<?php
    include_partial(
        'list',
        array(
            'pager'             => $pager,
            'permissions'       => $permissions,
            'permissionsTIPN'   => $permissionsTIPN,
            'raws'              => $list,
            'timezoneName'      => $timezoneName,
            'routePostfix'      => $routePostfix,
            'urlList'           => $urlList,
            'urlIndex'          => $urlIndex,
            'params_additional' => $params_additional,
        )
    );
?>