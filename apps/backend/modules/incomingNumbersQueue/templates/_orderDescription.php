<?php use_helper('Link'); ?>

<div class="table-block">
    <table class="table">
        <thead>
            <tr>
                <th class="first">&nbsp;</th>
                <th>Date</th>
                <th>User</th>
                <th>Type</th>
                <th>Status</th>
                <th>Description</th>
                <th class="last">Update</th>
            </tr>
        </thead>
        <tbody>
            <tr class="odd">
                <td><?php echo $order->getId(); ?></td>
                <td><?php echo TimeConverter::getDateTime($order->getCreatedDate(), $timezoneName, 'h:i a D d/m/Y'); ?></td>
                <td><?php echo $order->getUser(); ?></td>
                <td><?php echo $order->getOrderTypeString(); ?></td>

                <?php
                    switch ($order->getStatus()) {
                        case IncomingNumbersQueueTable::STATUS_TYPE_DONE:
                            $description = 'Processed successfully';
                            $progressClass = 'done';
                            break;

                        case IncomingNumbersQueueTable::STATUS_TYPE_CLOSED:
                            $description = 'Proccessed is stoped: ' . $order->getDoneAmount() . '/' . $order->getAmount() . ' numbers';
                            $progressClass = 'closed';
                            break;

                        case IncomingNumbersQueueTable::STATUS_TYPE_IN_PROGRESS:
                            if ($order->getDoneAmount()) {
                                $description = 'In processing: ' . $order->getDoneAmount() . '/' . $order->getAmount() . ' numbers';
                            } else {
                                $description = 'In processing';
                            }
                            $progressClass = 'progress';
                            break;

                        default:
                            $description = 'In processing';
                            $progressClass = 'open';
                            break;
                    }
                ?>
                <td class="progressbar progressbar-<?php echo $progressClass; ?>">
                    <div>
                        <div style="width:<?php echo $order->getDoneAmountPercentage(); ?>%;">&nbsp;</div>
                        <span><?php echo $description; ?></span>
                    </div>
                </td>
                <td>
                    <div class="order-description">
                        <?php echo $order->getDescriptionString(); ?>
                    </div>
                    <div class="author-details">
                        <?php $notes = $order->getAuthorNotes(); ?>
                        <?php $notes = empty($notes) ? '' : "($notes)"; ?>
                        <?php echo $notes; ?>
                    </div>
                </td>
                <td><?php echo TimeConverter::getDateTime($order->getUpdatedDate(), $timezoneName, 'h:i a D d/m/Y'); ?></td>
            </tr>
        </tbody>
    </table>
</div>
