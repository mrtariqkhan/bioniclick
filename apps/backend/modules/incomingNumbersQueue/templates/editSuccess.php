
<span id="advertiser_id_handler" advertiser_id="<?php echo $advertiser->getId(); ?>"></span>

<?php $object = $form->getObject(); ?>

<h1>Edit order</h1>

<div class="inner">

    <?php $urlAction = url_for('incomingNumbersQueue_update', array('incoming_numbers_queue_id' => $incomingNumbersQueueId)); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'back_url'          => $url_after,
                'submit_name'       => 'Update',
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel'),
            )
        );
    ?>
</div>
