<?php use_helper('Link'); ?>

<h1>Order</h1>
<?php
    include_partial(
        'orderDescription',
        array(
            'order'         => $order,
            'timezoneName'  => $timezoneName,
        )
    );
?>

<h2>Details</h2>
<div class="ajax-pagable">
    <?php
        include_component(
            'advertiserIncomingNumberPoolLog',
            'listForOrder',
            array(
                'pager'             => $pagerDetails,
                'order'             => $order,
                'timezoneName'      => $timezoneName,
                'historyIsNeed'     => true,
                'routePostfix'      => $routePostfix,
                'params_additional' => $params_additional,
            )
        );
    ?>
</div>

<?php
    $a_options = array();

    if ($permissions['can_read']) {
        $a_options['Back'] = array(
            'href'  => $urlBack,
            'class' => "icon back ajax-load"
        );
    }

    echo icon_links($a_options);
?>