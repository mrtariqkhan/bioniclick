<?php use_helper('Link'); ?>
<?php use_helper('Pager'); ?>

<?php $params_additional = $params_additional->getRawValue(); ?>
<?php $maxAmountDetails = sfConfig::get('app_max_amount_details', 2); ?>

<?php
    $pagerHtmlString = pager(
        $pager->getRawValue(),
        sfConfig::get('app_pager_max_per_pages'),
        $urlList,
        null,
        'ajax-pager'
    );
?>

<?php include_partial('global/flash_message', array('flash_name' => 'tab-message')); ?>
<?php include_partial('global/flash_message', array('flash_name' => 'tab-warning')); ?>

<p>
    <?php
        include_partial(
            'global/pager_list_header',
            array(
                'pager' => $pager,
                'pagerHtmlString' => $pagerHtmlString
            )
        );
    ?>
</p>

<?php
    $list_buttons = array(
        'Copy' => array(
            'class' => 'icon copy ajax-form-load'
        ),
        'Edit' => array(
            'class' => 'icon edit ajax-form-load'
        ),
        'Cancel' => array(
            'class' => 'icon delete'
        ),
        'Fulfill' => array(
            'class' => 'icon fulfill ajax-form-load'
        ),
        'Details' => array(
            'class' => 'icon details ajax-load'
        ),
    );

    $legend_buttons = array();
?>

<div class="table-block">
    <table class="table">
        <thead>
            <tr>
                <th class="first">&nbsp;</th>
                <th>Date</th>
                <th>User</th>
                <th>Type</th>
                <th>Status</th>
                <th>Description</th>
                <th>Details</th>
                <th></th>
                <th class="last">Update</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = $pager->getFirstIndice(); ?>
            <?php next($raws); ?>
            <?php foreach ($raws as $raw): ?>
                <tr class="<?php echo ($i%2) ? 'odd' : 'even'; ?>">
                    <td><?php echo $raw->getId(); ?></td>
                    
                    <td><?php echo TimeConverter::getDateTime($raw->getCreatedDate(), $timezoneName, 'd/m/Y'); ?></td>
                    <td><?php echo $raw->getUser(); ?></td>
                    
                    <td>
                        <?php $orderType = $raw->getOrderType(); ?>
                        <?php echo $raw->getOrderTypeString(); ?>
                    </td>
                    
                    <?php
                    switch ($raw->getStatus()) {
                        case IncomingNumbersQueueTable::STATUS_TYPE_DONE:
                            $description = 'Processed successfully';
                            $progressClass = 'done';
                            break;

                        case IncomingNumbersQueueTable::STATUS_TYPE_CLOSED:
                            $description = 'Proccessed is stoped: ' . $raw->getDoneAmount() . '/' . $raw->getAmount() . ' numbers';
                            $progressClass = 'closed';
                            break;

                        case IncomingNumbersQueueTable::STATUS_TYPE_IN_PROGRESS:
                            if ($raw->getDoneAmount()) {
                                $description = 'In processing: ' . $raw->getDoneAmount() . '/' . $raw->getAmount() . ' numbers';
                            } else {
                                $description = 'In processing';
                            }
                            $progressClass = 'progress';
                            break;

                        default:
                            $description = 'In processing';
                            $progressClass = 'open';
                            break;
                    }
                    ?>
                    <td class="progressbar progressbar-<?php echo $progressClass; ?>">
                        <div>
                            <div style="width:<?php echo $raw->getDoneAmountPercentage(); ?>%;">&nbsp;</div>
                            <span><?php echo $description; ?></span>
                        </div>
                    </td>
                    <td>
                        <div class="order-description">
                            <?php echo $raw->getDescriptionString(); ?>
                        </div>
                        <div class="author-details">
                            <?php $notes = $raw->getAuthorNotes(); ?>
                            <?php $notes = empty($notes) ? '' : "($notes)"; ?>
                            <?php echo $notes; ?>
                        </div>
                    </td>

                    <td>
                        <?php $phoneNumberLogs = $raw->getAdvertiserIncomingNumberPoolLogs(); ?>
                        <?php if ($phoneNumberLogs->count() <= 0): ?>
                            Undefined
                        <?php else: ?>
                            <?php $j = 1; ?>
                            <?php foreach ($phoneNumberLogs as $phoneNumberLog): ?>
                                <?php if ($j > $maxAmountDetails): ?>
                                    ...
                                    <?php break; ?>
                                <?php elseif ($j > 1): ?>
                                    ;
                                <?php endif; ?>
                                <?php echo $phoneNumberLog->getTwilioIncomingPhoneNumber(); ?>
                                <?php ++$j; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </td>

                    <td>
                        <?php
                            $params = array_merge(array('id' => $raw->getId()), $params_additional);

                            $a_options = array();

                            if ($permissions['can_read']) {
                                $urlDetails = url_for('incomingNumbersQueue_show' . $routePostfix, $params);

                                $a_options['Details'] = $list_buttons['Details'];
                                $a_options['Details']['href'] = $urlDetails;

                                $legend_buttons['Details'] = & $list_buttons['Details'];
                            }

                            if ($permissions['can_update']) {
                                if ($raw->getStatus() == IncomingNumbersQueueTable::STATUS_TYPE_OPEN) {
                                    //TODO:: think if orderType checking is necessary
                                    if (   $orderType == IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE_AND_ASSIGN
                                        || $orderType == IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE
                                    ) {
                                        $urlEdit = url_for('incomingNumbersQueue_edit' . $routePostfix, $params);

                                        $a_options['Editing order'] = $list_buttons['Edit'];
                                        $a_options['Editing order']['href'] = $urlEdit;

                                        $legend_buttons['Editing order'] = & $list_buttons['Edit'];
                                    } else {
                                        $a_options["Cannot Edit order because of its type"] = $list_buttons['Edit'];
                                    }
                                } else {
                                    $a_options["Cannot Edit order because of its status"] = $list_buttons['Edit'];
                                }
                            }
                            
                            if ($permissions['can_create']) {
                                //TODO:: think if orderType checking is necessary
                                if (   $orderType == IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE_AND_ASSIGN
                                    || $orderType == IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE
                                    || $orderType == IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_ASSIGN
                                ) {
                                    $a_options['Copy order'] = $list_buttons['Copy'];
                                    $a_options['Copy order']['href'] = url_for('incomingNumbersQueue_copy' . $routePostfix, $params);

                                    $legend_buttons['Copy order'] = & $list_buttons['Copy'];
                                } else {
                                    $a_options['Cannot Copy order because of its type'] = $list_buttons['Copy'];
                                }
                            }

                            if ($permissionsTIPN['can_create']) {
                                if (   $raw->getStatus() == IncomingNumbersQueueTable::STATUS_TYPE_OPEN
                                    || $raw->getStatus() == IncomingNumbersQueueTable::STATUS_TYPE_IN_PROGRESS
                                ) {
                                    //TODO:: think if orderType checking is necessary
                                    if (   $orderType == IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE_AND_ASSIGN
                                        || $orderType == IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE
                                    ) {
                                        $urlBuy = url_for('buy_numbers_task_new_specific' . $routePostfix, $params);

                                        $a_options['Fulfill order'] = $list_buttons['Fulfill'];
                                        $a_options['Fulfill order']['href'] = $urlBuy;

                                        $legend_buttons['Fulfill order'] = & $list_buttons['Fulfill'];
                                    } else {
                                        $a_options["Cannot Fulfill order because of its type"] = $list_buttons['Fulfill'];
                                    }
                                } else {
                                    $a_options["Cannot Fulfill order because of its status"] = $list_buttons['Fulfill'];
                                }
                            }

                            if ($permissions['can_delete']) {
                                if ($raw->getStatus() == IncomingNumbersQueueTable::STATUS_TYPE_OPEN ||
                                    $raw->getStatus() == IncomingNumbersQueueTable::STATUS_TYPE_IN_PROGRESS
                                ) {
                                    //TODO:: think if orderType checking is necessary
                                    if (   $orderType == IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE_AND_ASSIGN
                                        || $orderType == IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE
                                    ) {
                                        $urlDelete = url_for('incomingNumbersQueue_close', array('id' => $raw->getId()));
                                        $a_options['Cancel order'] = link_to_ajax_delete(
                                            '',
                                            $urlDelete,
                                            array(
                                                'title'             => 'Cancel',
                                                'class'             => 'icon delete',
                                                'after_url'         => $urlIndex,
                                            )
                                        );

                                        $legend_buttons['Cancel order'] = & $list_buttons['Cancel'];
                                    } else {
                                        $a_options["Cannot Cancel order because of its type"] = $list_buttons['Cancel'];
                                    }
                                } else {
                                    $a_options["Cannot Cancel order because of its status"] = $list_buttons['Cancel'];
                                }
                            }

                            echo icon_links($a_options, array('class' => 'icon-links clearfix'), false);
                        ?>
                    </td>
                    
                    <td><?php echo TimeConverter::getDateTime($raw->getUpdatedDate(), $timezoneName, 'h:i a D d/m/Y'); ?></td>
                </tr>
                <?php ++$i; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php
    include_partial(
         'global/legend',
          array(
            'legend_buttons'  => $legend_buttons,
          )
    ); 
?>

<?php echo $pagerHtmlString; ?>