<?php

/**
 * sfGuardGroup actions.
 *
 * @package    bionic
 * @subpackage sfGuardGroup
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sfGuardGroupActions extends GeneralActions {

    public function preExecute() {

        $this->tableName = 'sfGuardGroup';

        parent::preExecute();

        $this->getResponse()->addStylesheet('/css/project/reset.css', 'last');
        $this->getResponse()->addStylesheet(
            '/css/project/bionic_form_ajax_error.css',
            'last'
        );

        $this->getResponse()->addJavascript(
            '/js/jquery/jquery.form.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/project/ajax_form.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/project/ajax_delete.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/project/ajax_filter.js',
            'last'
        );
    }

    public function executeIndex(sfWebRequest $request) {

        $this->executeList($request);
        $this->setTemplate('index');
    }

    public function executeList(sfWebRequest $request) {

        $specialQuery = sfGuardGroupTable::createIndexQuery();
        $this->pager = parent::initPager($request, $specialQuery);
    }

//NOTE:: nobody cannot create group
//    public function executeNew(sfWebRequest $request) {
//
//        $this->form = $this->configureForm();
//    }
//
//    public function executeCreate(sfWebRequest $request) {
//
//        $form = $this->configureForm();
//
//        return $this->processAjaxForm($request, $form);
//    }

    public function executeEdit(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $this->form = $this->configureForm($object);
    }

    public function executeUpdate(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $form = $this->configureForm($object);

        return $this->processAjaxForm($request, $form);
    }

//NOTE:: nobody cannot delete package now
//    public function executeDelete(sfWebRequest $request) {
//        return $this->processAjaxDelete($request);
//    }

    protected function configureForm($object = null) {

        $formOptions = array();

        $object = empty($object) ? new sfGuardGroup() : $object;

        $form = new sfGuardGroupForm($object, $formOptions);
        return $form;
    }
}
