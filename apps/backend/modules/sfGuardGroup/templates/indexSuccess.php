<?php use_helper('Link'); ?>
<div class="ajax-pagable">
    <?php
        include_partial(
            'list',
            array(
                'pager'         => $pager,
                'permissions'   => $permissions,
            )
        );
    ?>
</div>

<?php //if ($permissions['can_create']): ?>
    <?php
//        $a_options = array();
//
//        $url_new = url_for('sf_guard_group_new');
//        $a_options['New'] = array(
//            'href'      => $url_new,
//            'class'     => 'icon new ajax-form-load',
//        );
//        echo icon_links($a_options);
    ?>
<?php //endif; ?>