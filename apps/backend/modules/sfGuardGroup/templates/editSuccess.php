<?php $object = $form->getObject(); ?>

<h1>
    Edit Group '<?php echo $object->getName(); ?>'
</h1>

<div class="inner">

    <?php $urlAction = url_for('sf_guard_group_update', array('id' => $object->getId())); ?>
    <?php $urlAfter = url_for('sf_guard_groups'); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'submit_name'       => 'Update',
                'back_url'          => $urlAfter,
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel')
            )
        );
    ?>
</div>
