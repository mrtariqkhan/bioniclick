<h1>Create new Group</h1>

<div class="inner">

    <?php $urlAction = url_for('sf_guard_group_create'); ?>
    <?php $urlAfter = url_for('sf_guard_groups'); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'submit_name'       => 'Create',
                'back_url'          => $urlAfter,
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel')
            )
        );
    ?>
</div>
