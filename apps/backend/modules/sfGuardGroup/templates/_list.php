<?php use_helper('Link'); ?>
<?php use_helper('Pager'); ?>

<?php
    $pagerHtmlString = pager(
        $pager->getRawValue(),
        sfConfig::get('app_pager_max_per_pages'),
        url_for('sf_guard_group_list', array()),
        null,
        'ajax-pager'
    );
?>

<p>
    <?php 
        include_partial(
            'global/pager_list_header',
            array(
                'pager' => $pager,
                'pagerHtmlString' => $pagerHtmlString
            )
        ); 
    ?>
</p>

<div class="table-block">
    <table class="table">
        <thead>
            <tr>
                <th class="first">&nbsp;</th>
                <th>Name</th>
                <th>Description</th>
                <th class="last">&nbsp; </th>
            </tr>
        </thead>
        <tbody>
            <?php
                $list_buttons = array(
                    'Edit' => array(
                        'class' => 'icon edit ajax-form-load'
                    ),
                    'Delete' => array(
                        'class' => 'icon delete'
                    )
                );

                $legend_buttons = array();
            ?>
            <?php $i = $pager->getFirstIndice(); ?>
            <?php $list = $pager->getResults(); ?>
            <?php foreach ($list as $object): ?>
                <?php $id = $object->getId(); ?>
                <tr class="<?php echo ($i%2) ? 'odd' : 'even'; ?>">
                    <td><?php echo $i; ?></td>
                    <td><?php echo $object->getName(); ?></td>
                    <td><?php echo $object->getDescription(); ?></td>
                    <td class="last">
                        <?php $url = url_for('sf_guard_group_edit', array('id' => $id)); ?>
                        <?php $urlAfter = url_for('sf_guard_groups'); ?>
                        <?php
                            $a_options = array();
                            
                            if ($permissions['can_update']) {
                                $a_options['Edit'] = $list_buttons['Edit'];
                                $a_options['Edit']['href'] = $url;

                                $legend_buttons['Edit'] = & $list_buttons['Edit'];
                            }

//                            if ($permissions['can_delete']) {
//                                $a_options['Delete'] = link_to_ajax_delete(
//                                    '',
//                                    url_for('sf_guard_group_delete', array('id' => $id)),
//                                    array(
//                                        'title' => 'Delete',
//                                        'class' => 'icon delete',
//                                        'after_url' => $urlAfter
//                                    )
//                                );
//
//                                $legend_buttons['Delete'] = & $list_buttons['Delete'];
//                            }

                            echo icon_links($a_options, array('class' => 'icon-links clearfix'), false);
                        ?>
                    </td>
                </tr>
                <?php ++$i; ?>
           <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php
    include_partial(
         'global/legend',
          array(
            'legend_buttons'  => $legend_buttons,
          )
    ); 
?>

<?php echo $pagerHtmlString; ?>