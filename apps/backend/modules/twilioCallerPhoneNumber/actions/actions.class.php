<?php

/**
 * twilioCallerPhoneNumber actions.
 *
 * @package    bionic
 * @subpackage twilioCallerPhoneNumber
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class twilioCallerPhoneNumberActions extends GeneralActions {

    //FIXME: atarasenko Who will have permissions to use this module?
    public function  preExecute() {

        $this->tableName = 'TwilioCallerPhoneNumber';
        $this->getResponse()->addStylesheet('/css/project/reset.css', 'last');
        parent::preExecute();
    }

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request) {

        $this->executeList($request);
        $this->setTemplate('index');

        $filterValues = $this->getFilter();
        $this->form_filter = $this->getFilterForm();
        $this->form_filter->setDefaults($filterValues);
    }


    public function executeList(sfWebRequest $request) {

        $filterValues = $this->getFilter();
        $filterForm = $this->getFilterForm();
        $queryParams = $filterForm->getFilteredValues($filterValues, true);

        $specialQuery = TwilioCallerPhoneNumberTable::getIndexQuery(
            $this->user,
            $queryParams
        );

        $this->pager = parent::initPager($request, $specialQuery);
    }


    public function executeUpdate(sfWebRequest $request) {

        $formTagName = sfInflector::underscore($this->tableName);
        $parameters = $request->getParameter($formTagName);

        $object = $this->getRoute()->getObject();
        $object->setIsBlocked($parameters['is_blocked']);

        $form = new TwilioCallerPhoneNumberForm($object);

        return $this->processAjaxForm($request, $form);
    }

    /**
     * Show pre-block message or block a phone number
     *
     * @param sfWebRequest $request
     */
    public function executeBlock(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->forward404If(
            $object->getIsBlocked(),
            "Caller is blocked ($object) already."
        );

        $object->setIsBlocked(true);
        $this->form = new TwilioCallerPhoneNumberForm($object);
    }

    /**
     * Show pre-unblock message or unblock a number
     *
     * @param sfWebRequest $request
     */
    public function executeUnblock(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->forward404If(
            !$object->getIsBlocked(),
            "Caller is unblocked ($object) already."
        );

        $object->setIsBlocked(false);
        $this->form = new TwilioCallerPhoneNumberForm($object);
    }

     public function executeFilter(sfWebRequest $request) {

        $options = $request->getPostParameter('caller_filter', array());

        $filterForm = new CallerFilterForm(null, $options);
        return $this->processAjaxFilter($request, $filterForm);
    }

    protected function getFilterForm() {
        return new CallerFilterForm();
    }
}
