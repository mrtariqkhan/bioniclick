<?php use_helper('Pager'); ?>
<?php use_helper('Link'); ?>

<div class="account-wrap clearfix">
    <div class="ajax-filter-container">
        <div class="filter-form-wrap">
            <h1>Search</h1>
            <div class="filter-form-wraper">
                <?php
                    include_partial(
                        'global/form_ajax',
                        array(
                            'form'              => $form_filter,
                            'action_url'        => url_for('twilio_caller_phone_number_filter'),
                            'submit_name'       => 'Go!',
                            'back_url'          => '',
                            'form_class_name'   => 'ajax-filter',
                            'cancel_attributes' => array('class' => 'ajax-form-cancel')
                        )
                    );
                ?>
            </div>
        </div>

        <div class="ajax-pagable">
            <?php
                include_partial(
                    'list',
                    array(
                        'pager'                 => $pager//,
                        //'permissions'           => $permissions,

                    )
                );
            ?>
        </div>

    </div>
</div>