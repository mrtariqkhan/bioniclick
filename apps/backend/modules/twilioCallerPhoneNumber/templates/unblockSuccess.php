<?php $object = $form->getObject(); ?>

<h1>You are going to unblock <?php echo $object->getPhoneNumber(); ?></h1>
<div class="inner">
    <div class="block-reason-content">
        <p>
            The reason it was blocked is:
            <strong>
                <?php echo $object->getReason(); ?>
            </strong>
        </p>
        <p>
            Are you sure you want to unblock this number?
        </p>
    </div>

    <?php $urlAction = url_for('twilio_caller_phone_number_update', array('id' => $object->getId())); ?>
    <?php $urlAfter = url_for('twilio_caller_phone_number_index'); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'back_url'          => $urlAfter,
                'submit_name'       => 'Unblock',
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel')
            )
        );
    ?>
</div>
