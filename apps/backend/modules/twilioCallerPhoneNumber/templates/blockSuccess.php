<?php $object = $form->getObject(); ?>

<h2>You are going to block number <?php echo $object->getPhoneNumber(); ?></h2>
<div class="inner">

    <?php $urlAction = url_for('twilio_caller_phone_number_update', array('id' => $object->getId())); ?>
    <?php $urlAfter = url_for('twilio_caller_phone_number_index'); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'back_url'          => $urlAfter,
                'submit_name'       => 'Block',
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel')
            )
        );
    ?>
</div>
