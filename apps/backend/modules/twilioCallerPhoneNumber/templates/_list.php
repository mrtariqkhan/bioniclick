<?php use_helper('Pager'); ?>
<?php use_helper('Link'); ?>

<?php
    $pagerHtmlString = pager(
        $pager->getRawValue(),
        sfConfig::get('app_pager_max_per_pages'),
        url_for('twilio_caller_phone_number_list'),
        null,
        'ajax-pager'
    );
?>

<p>
    <?php 
        include_partial(
            'global/pager_list_header',
            array(
                'pager' => $pager,
                'pagerHtmlString' => $pagerHtmlString
            )
        ); 
    ?>
</p>

<div class="table-block">
    <table class="table">
        <thead>
            <tr>
                <th class="first">Number</th>
                <th>Block/Unblock</th>
                <th class="last">Block Reason</th>
            </tr>
        </thead>
        <tbody>
            <?php
                $list_buttons = array(
                    'Block' => array(
                        'class' => 'icon block-number ajax-form-load'
                    ),
                    'Unblock' => array(
                        'class' => 'icon unblock-number ajax-form-load'
                    )
                );

                $legend_buttons = array();
            ?>
            <?php $i = $pager->getFirstIndice(); ?>
            <?php $list = $pager->getResults(); ?>
            <?php foreach ($list as $object): ?>
                <?php $callerBlocked = $object->getIsBlocked(); ?>
                <?php $blockAction = $callerBlocked ? 'Unblock' : 'Block'; ?>
                <?php $url = url_for('twilio_caller_phone_number_' . strtolower($blockAction), array('id' => $object->getId())); ?>

                <tr class="<?php echo ($i & 1) ? 'odd' : 'even'?>">
                    <td>
                        <span class="<?php echo $callerBlocked ? 'blocked-phone' : ''?>">
                            <?php echo $object->getPhoneNumber(); ?>
                        </span>
                    </td>
                    <td class="block-td">
                        <?php
                            $a_options = array();
                            
                            $a_options[$blockAction] = $list_buttons[$blockAction];
                            $a_options[$blockAction]['href'] = $url;

                            $legend_buttons[$blockAction] = & $list_buttons[$blockAction];

                            echo icon_links($a_options, array('class' => 'icon-links clearfix'), false);
                        ?>
                    </td>
                    <td class="block-reason">
                        <?php echo $object->getReason(); ?>
                    </td>
                </tr>
                <?php ++$i; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php
    include_partial(
         'global/legend',
          array(
            'legend_buttons'  => $legend_buttons,
          )
    ); 
?>

<?php echo $pagerHtmlString; ?>