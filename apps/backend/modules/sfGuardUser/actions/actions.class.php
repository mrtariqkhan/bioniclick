<?php

/**
 * sfGuardUser actions.
 *
 * @package    bionic
 * @subpackage sfGuardUser
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sfGuardUserActions extends GeneralActions {

    const FILTER_ATTRIBUTE_NAME_COMPANY_SUB_COMPANIES   = 'company_sub_companies_filter';
    const FILTER_ATTRIBUTE_NAME_COMPANY_SUB_ADVERTISERS = 'company_sub_advertisers_filter';

    public function preExecute() {

        $this->tableName = 'sfGuardUser';

        parent::preExecute();

        $this->getResponse()->addStylesheet('/css/project/reset.css', 'last');
        $this->getResponse()->addStylesheet(
            '/css/project/bionic_form_ajax_error.css',
            'last'
        );

        $this->getResponse()->addJavascript(
            '/js/jquery/jquery.form.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/project/ajax_form.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/project/ajax_delete.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/project/ajax_filter.js',
            'last'
        );
    }

    public function executeIndex(sfWebRequest $request) {

        $this->executeList($request);

        $filterValues = $this->getFilter();
        $this->filter_form = $this->getFilterForm();
        $this->filter_form->setDefaults($filterValues);
    }

    public function executeList(sfWebRequest $request) {

        $filterValues = $this->getFilter();
        $filterForm = $this->getFilterForm();
        $queryParams = $filterForm->getFilteredValues($filterValues, true);

        $specialQuery = sfGuardUserTable::createIndexQuery($this->user, $queryParams);
        $this->pager = parent::initPager($request, $specialQuery);
        $list = $this->pager->getResults();

        $objectFieldsHeaders = array(
            'getFirstName'          => 'First Name',
            'getLastName'           => 'Last Name',
            'getEmailAddress'       => 'Email',
            'getUsername'           => 'Username',            
            'getLastLogin'          => 'Last Login',
            'getIsActiveString'     => 'Is Active',
        );
        WasDeletedHelper::addHeader($this->user, $objectFieldsHeaders);

        $branchHelper = new BranchHelper();
        $this->branch_data = $branchHelper->makeRaws($this->user, $list, $objectFieldsHeaders);
        $this->is_advertiser = $this->user->isAdvertiserUser();
        $this->is_super_admin = $this->user->getIsSuperAdmin();
        
        $this->curUserId = $this->user->getId();
    }

    public function executeAdvertiserIndex(sfWebRequest $request) {

        $this->executeAdvertiserList($request);
    }

    public function executeAdvertiserList(sfWebRequest $request) {

        $specialQuery = sfGuardUserTable::createAdvertiserIndexQuery($this->user);
        $this->pager = parent::initPager($request, $specialQuery);
        
        $this->curUserId = $this->user->getId();
    }


    public function executeCompanyIndex(sfWebRequest $request) {

        $this->executeCompanyList($request);
    }

    public function executeCompanyList(sfWebRequest $request) {

        $specialQuery = sfGuardUserTable::createCompanyIndexQuery($this->user);
        $this->pager = parent::initPager($request, $specialQuery);
        
        $this->curUserId = $this->user->getId();
    }

    public function executeCompanySubCompaniesIndex(sfWebRequest $request) {

        $this->executeCompanySubCompaniesList($request);

        $filterValues = $this->getFilter(self::FILTER_ATTRIBUTE_NAME_COMPANY_SUB_COMPANIES);
        $this->filter_form = $this->getFilterFormCompanySubCompanies();
        $this->filter_form->setDefaults($filterValues);
    }

    public function executeCompanySubCompaniesList(sfWebRequest $request) {

        $filterValues = $this->getFilter(self::FILTER_ATTRIBUTE_NAME_COMPANY_SUB_COMPANIES);
        $filterForm = $this->getFilterFormCompanySubCompanies();
        $queryParams = $filterForm->getFilteredValues($filterValues, true);

        $specialQuery = sfGuardUserTable::createCompanySubCompaniesIndexQuery($this->user, $queryParams);
        $this->pager = parent::initPager($request, $specialQuery);
        $list = $this->pager->getResults();

        $objectFieldsHeaders = array(
            'getFirstName'      => 'First Name',
            'getLastName'       => 'Last Name',
            'getEmailAddress'   => 'Email',
            'getUsername'       => 'Username',
            'getIsActiveString' => 'Is Active',
            'getLastLogin'      => 'Last Login',
        );
        $branchHelper = new BranchHelper();
        $this->branch_data = $branchHelper->makeRaws($this->user, $list, $objectFieldsHeaders);
    }


    public function executeCompanySubAdvertisersIndex(sfWebRequest $request) {

        $this->executeCompanySubAdvertisersList($request);

        $filterValues = $this->getFilter(self::FILTER_ATTRIBUTE_NAME_COMPANY_SUB_ADVERTISERS);
        $this->filter_form = $this->getFilterFormCompanySubAdvertisers();
        $this->filter_form->setDefaults($filterValues);
    }

    public function executeCompanySubAdvertisersList(sfWebRequest $request) {

        $filterValues = $this->getFilter(self::FILTER_ATTRIBUTE_NAME_COMPANY_SUB_ADVERTISERS);
        $filterForm = $this->getFilterFormCompanySubAdvertisers();
        $queryParams = $filterForm->getFilteredValues($filterValues, true);

        $specialQuery = sfGuardUserTable::createCompanySubAdvertisersIndexQuery($this->user, $queryParams);
        $this->pager = parent::initPager($request, $specialQuery);
        $list = $this->pager->getResults();

        $objectFieldsHeaders = array(
            'getFirstName'      => 'First Name',
            'getLastName'       => 'Last Name',
            'getEmailAddress'   => 'Email',
            'getUsername'       => 'Username',
            'getIsActiveString' => 'Is Active',
            'getLastLogin'      => 'Last Login',
        );
        $branchHelper = new BranchHelper();
        $this->branch_data = $branchHelper->makeRaws($this->user, $list, $objectFieldsHeaders);
    }

    public function executeEdit(sfWebRequest $request) {

        $this->setTemplate('edit');

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $this->form = $this->configureFormEdit($object);

        $this->url_after = $this->generateUrl('sf_guard_users');
    }

    public function executeCompanyEdit(sfWebRequest $request) {

        $this->executeEdit($request);

        $this->url_after = $this->generateUrl('sf_guard_company_users');
    }

    public function executeAdvertiserEdit(sfWebRequest $request) {

        $this->executeEdit($request);

        $this->url_after = $this->generateUrl('sf_guard_advertiser_users');
    }

    public function executeUpdate(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $form = $this->configureFormEdit($object);

        return $this->processAjaxForm($request, $form);
    }

    public function executeCustomerEdit(sfWebRequest $request) {

        $this->setTemplate('customerEdit');

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $this->form = $this->configureFormEdit($object);

        $this->url_after = '';
    }

    public function executeCustomerAdvertiserEdit(sfWebRequest $request) {

        $this->forward404If(!$this->user->isCompanyUser(), "You are not Company's user");

        $this->executeCustomerEdit($request);
        $this->url_after = $this->generateUrl('sf_guard_company_sub_advertisers_users');
    }

    public function executeCustomerCompanyEdit(sfWebRequest $request) {

        $this->forward404If(!$this->user->isCompanyUser(), "You are not Company's user");

        $this->executeCustomerEdit($request);
        $this->url_after = $this->generateUrl('sf_guard_company_sub_companies_users');
    }

    public function executeCustomerUpdate(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $form = $this->configureFormEdit($object);

        return $this->processAjaxForm($request, $form);
    }

    public function executeDelete(sfWebRequest $request) {
        return $this->processAjaxDelete($request);
    }

    protected function deleteObject($object) {
        $object->deleteChecked();
    }

    protected function configureFormEdit($object) {

        $parent = $object->isAdvertiserUser() ? $object->getAdvertiser() : $object->getCompany();
        $currentCompanyAdmin = $parent->getAdmin();
        if (empty($currentCompanyAdmin)) {
            throw new Exception("User's company(advertiser) must have it's admin.");
        }

        $formOptions = array(
            sfGuardUserFormEdit::OPTION_SF_GUARD_USER_ADMIN => $currentCompanyAdmin,
        );

        return new sfGuardUserFormEdit($object, $formOptions);
    }

    public function executeFilter(sfWebRequest $request) {
        $filterForm = $this->getFilterForm();
        return $this->processAjaxFilter($request, $filterForm);
    }

    protected function getFilterForm() {
        $options = $this->getBranchFilterOptions();
        return new sfGuardUserFilterForm(null, $options);
    }

    public function executeCompanySubCompaniesFilter(sfWebRequest $request) {
        $filterForm = $this->getFilterFormCompanySubCompanies();
        return $this->processAjaxFilter($request, $filterForm, self::FILTER_ATTRIBUTE_NAME_COMPANY_SUB_COMPANIES);
    }

    protected function getFilterFormCompanySubCompanies() {

        $options = array();

        if (!empty($this->user) && !$this->user->isAdvertiserUser()) {
            $level = $this->user->getLevel();
            if ($level == CompanyTable::$LEVEL_BIONIC) {
                $options[CompanyTable::$LEVEL_NAME_RESELLER] = true;
            }
            if ($level == CompanyTable::$LEVEL_RESELLER) {
                $options[CompanyTable::$LEVEL_NAME_AGENCY] = true;
            }
        }

        return new sfGuardUserFilterForm(null, $options);
    }

    public function executeCompanySubAdvertisersFilter(sfWebRequest $request) {
        $filterForm = $this->getFilterFormCompanySubAdvertisers();
        return $this->processAjaxFilter($request, $filterForm, self::FILTER_ATTRIBUTE_NAME_COMPANY_SUB_ADVERTISERS);
    }

    protected function getFilterFormCompanySubAdvertisers() {

        $options = array();

        if (!empty($this->user) && !$this->user->isAdvertiserUser()) {
            $options[CompanyTable::$LEVEL_NAME_ADVERTISER] = true;
        }

        return new sfGuardUserFilterForm(null, $options);
    }

}
