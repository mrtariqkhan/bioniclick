<?php
    include_partial(
        'companySubCompaniesList',
        array(
            'pager'         => $pager,
            'permissions'   => $permissions,
            'branch_data'   => $branch_data,
        )
    );
?>