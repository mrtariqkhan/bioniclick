<?php use_helper('Link'); ?>
<div class="account-wrap clearfix">
    <h1><?php echo $pager->getNbResults(); ?> users found</h1>

    <div class="ajax-pagable">
        <?php
            include_partial(
                'advertiserList',
                array(
                    'pager'         => $pager,
                    'permissions'   => $permissions,
                    'curUserId'     => $curUserId,
                )
            );
        ?>
    </div>
</div>

<?php if ($permissions['can_create']): ?>
    <?php
        $a_options = array();

        $url_new = url_for('user_new_for_current_advertiser');
        $a_options['New'] = array(
            'href'  => $url_new,
            'class' => 'icon new ajax-form-load',
        );

        echo icon_links($a_options);
    ?>
<?php endif; ?>