<?php
    include_partial(
        'list',
        array(
            'pager'         => $pager,
            'branch_data'   => $branch_data,
            'permissions'   => $permissions,
            'is_advertiser' => $is_advertiser,
            'is_super_admin'=> $is_super_admin,
            'curUserId'     => $curUserId
        )
    );
?>