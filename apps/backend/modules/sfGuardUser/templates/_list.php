<?php use_helper('Link'); ?>
<?php use_helper('Pager'); ?>

<?php $headers = $branch_data['headers']; ?>
<?php $raws    = $branch_data['raws']; ?>
<?php
    $pagerHtmlString = pager(
        $pager->getRawValue(),
        sfConfig::get('app_pager_max_per_pages'),
        url_for('sf_guard_users_list', array()),
        null,
        'ajax-pager'
    );
?>

<p>
    <?php
        include_partial(
            'global/pager_list_header',
            array(
                'pager' => $pager,
                'pagerHtmlString' => $pagerHtmlString
            )
        );
    ?>
</p>

<div class="table-block">
    <table  class="table">
        <thead>
            <tr>
                <th class="first">&nbsp;</th>
                <?php
                    $list_buttons = array(
                        'Edit' => array(
                            'class' => 'icon edit ajax-form-load'
                        ),
                        'Delete' => array(
                            'class' => 'icon delete'
                        )
                    );

                    $legend_buttons = array();
                ?>
                <?php foreach ($headers as $headerTitle): ?>
                    <th><?php echo $headerTitle; ?></th>
                <?php endforeach; ?>
                <th class="last">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = $pager->getFirstIndice(); ?>
            <?php $list = $pager->getResults(); ?>
            <?php foreach ($raws as $id => $raw): ?>
                <tr class="<?php echo ($i%2) ? 'odd' : 'even'; ?>">

                    <td><?php echo $i; ?></td>

                    <?php $raw = $raw->getRawValue(); ?>
                    <?php foreach ($raw as $key => $cell): ?>
                        <?php $value = (WasDeletedHelper::KEY == $key) ? WasDeletedHelper::getAsString($cell) : $cell; ?>
                        <td><?php echo $value; ?></td>
                    <?php endforeach; ?>

                    <td>
                        <?php $urlAfter = url_for('sf_guard_company_users', array()); ?>
                        <?php
                            if ($is_super_admin) {
                                $urlAfter = url_for('sf_guard_users');
                            } elseif ($is_advertiser) {
                                $urlAfter = url_for('sf_guard_advertiser_users');
                            } else {
                                $urlAfter = url_for('sf_guard_company_users');
                            }
                        ?>

                        <?php
                            $a_options = array();
                            
                            if (WasDeletedHelper::ifCanUpdateAsArray($raw)) {

                                if ($permissions['can_update']) {
                                    $a_options['Edit'] = $list_buttons['Edit'];
                                    $a_options['Edit']['href'] = url_for('sf_guard_user_edit', array('id' => $id));

                                    $legend_buttons['Edit'] = & $list_buttons['Edit'];
                                }

                                if ($permissions['can_delete']) {
                                    
                                    if ($id != $curUserId) {
                                        $a_options['Delete'] = link_to_ajax_delete(
                                            '',
                                            url_for('sf_guard_user_delete', array('id' => $id)),
                                            array(
                                                'title' => 'Delete',
                                                'class' => 'icon delete',
                                                'after_url' => $urlAfter
                                            )
                                        );

                                        $legend_buttons['Delete'] = & $list_buttons['Delete'];
                                    } else {
                                        $a_options['Can not delete yourself'] = $list_buttons['Delete'];
                                    }
                                }
                            }

                            echo icon_links($a_options, array('class' => 'icon-links clearfix'), false);
                        ?>
                    </td>
                </tr>
                <?php ++$i; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php
    include_partial(
         'global/legend',
          array(
            'legend_buttons'  => $legend_buttons,
          )
    ); 
?>

<?php echo $pagerHtmlString; ?>
