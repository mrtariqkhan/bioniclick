<?php
    include_partial(
        'advertiserList',
        array(
            'pager'         => $pager,
            'permissions'   => $permissions,
            'curUserId'     => $curUserId,
        )
    );
?>