<?php use_helper('Link'); ?>
<?php use_helper('Pager'); ?>

<?php
    $pagerHtmlString = pager(
        $pager->getRawValue(),
        sfConfig::get('app_pager_max_per_pages'),
        url_for('sf_guard_advertiser_users_list', array()),
        null,
        'ajax-pager'
    );
?>

<p>
    <?php
        include_partial(
            'global/pager_list_header',
            array(
                'pager' => $pager,
                'pagerHtmlString' => $pagerHtmlString
            )
        );
    ?>
</p>

<div class="table-block">
    <table  class="table">
        <thead>
            <tr>
                <th class="first">&nbsp;</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th>Username</th>
                <th>Is Active</th>
                <th>Last Login</th>
                <th class="last"></th>
            </tr>
        </thead>
        <tbody>
            <?php
                $list_buttons = array(
                    'Edit' => array(
                        'class' => 'icon edit ajax-form-load'
                    ),
                    'Delete' => array(
                        'class' => 'icon delete'
                    )
                );

                $legend_buttons = array();
            ?>
            <?php $i = $pager->getFirstIndice(); ?>
            <?php $list = $pager->getResults(); ?>
            <?php foreach ($list as $object): ?>
                <tr class="<?php echo ($i%2) ? 'odd' : 'even'; ?>">
                    <td><?php echo $i; ?></td>
                    <td><?php echo $object->getFirstName(); ?></td>
                    <td><?php echo $object->getLastName(); ?></td>
                    <td><?php echo $object->getEmailAddress(); ?></td>
                    <td><?php echo $object->getUsername(); ?></td>
                    <td><?php echo $object->getIsActiveString(); ?></td>
                    <td><?php echo $object->getLastLogin(); ?></td>
                    <td>
                        <?php $id = $object->getId(); ?>
                        <?php $urlAfter = url_for('sf_guard_advertiser_users', array()); ?>

                        <?php
                            $a_options = array();

                            if ($permissions['can_update']) {
                                $a_options['Edit'] = $list_buttons['Edit'];
                                $a_options['Edit']['href'] = url_for(
                                    'sf_guard_user_advertiser_edit',
                                    array('id' => $id)
                                );

                                $legend_buttons['Edit'] = & $list_buttons['Edit'];
                            }

                            if ($permissions['can_delete']) {
                                
                                if ($id != $curUserId) {
                                    $a_options['Delete'] = link_to_ajax_delete(
                                        '',
                                        url_for('sf_guard_user_delete', array('id' => $id)),
                                        array(
                                            'title' => 'Delete',
                                            'class' => 'icon delete',
                                            'after_url' => $urlAfter,
                                        )
                                    );

                                    $legend_buttons['Delete'] = & $list_buttons['Delete'];
                                } else {
                                    $a_options['Can not delete yourself'] = $list_buttons['Delete'];
                                }
                            }

                            echo icon_links($a_options, array('class' => 'icon-links clearfix'), false);
                        ?>
                    </td>
                </tr>
                <?php ++$i; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php
    include_partial(
         'global/legend',
          array(
            'legend_buttons'  => $legend_buttons,
          )
    ); 
?>

<?php echo $pagerHtmlString; ?>