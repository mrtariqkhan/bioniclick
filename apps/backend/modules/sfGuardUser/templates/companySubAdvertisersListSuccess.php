<?php
    include_partial(
        'companySubAdvertisersList',
        array(
            'pager'         => $pager,
            'permissions'   => $permissions,
            'branch_data'   => $branch_data,
        )
    );
?>