<?php use_helper('Link'); ?>

<h1>Instructions</h1>
<div class="code">
    <h3>Please add following script before closing of body tag in the page(s).</h3>
    <span>
    <?php
        echo htmlentities(
            "<script src=\"http://jqueryjs.googlecode.com/files/jquery-1.3.2.min.js\"></script>
            <script type=\"text/javascript\">
                var one = \"\"; 
                var two = \"\";
                var three = \"\";
                var four = \"\";
                var five = \"\";
                var six = \"\";
                var seven = \"\";
                var eight = \"\";
                var nine = \"\";
                var ten = \"\";
                var eleven = \"\";
                var twelve = \"\";
                var datastring=document.referrer;
                if(datastring) {
                    var vars = datastring.split(\"=\");
                    one=escape(datastring);
                    if(vars) {
                        two=escape(vars[1]);
                        three=escape(vars[2]);
                        four = escape(vars[3]);
                        five= escape(vars[4]);
                        six = escape(vars[5]);
                        seven = escape(vars[6]);
                        eight = escape(vars[7]);
                        nine = escape(vars[8]);
                        ten = escape(vars[9]);
                        eleven = escape(vars[10]);
                        twelve = escape(vars[11]);
                    } 
                }
                var host1 = escape(window.location.host);
                var path = escape(window.location.pathname);
                var wh = escape(window.location.href);
                jQuery.getJSON(\"http://dev.bionicclick.com/bionic/p4/returnjs7.php?test=test&api-key={$page->getCampaign()->getApiKey()}&querystring={$page->getApiKey()}&host=\" + host1 + \"&path=\" + path + \"&wh=\" + wh + \"&one=\" + one + \"&two=\" + two + \"&three=\" + three + \"&four=\" + four + \"&five=\" + five + \"&six=\" + six + \"&seven=\" + seven + \"&eight=\" + eight + \"&nine=\" + nine + \"&ten=\" + ten + \"&eleven=\" + eleven + \"&twelve=\" + twelve + \"&callback=?\", function(data) { eval(data.test); });
             </script>"
        );
    ?>
    </span>
</div>
<div id="modal_width">    
    <?php
        $url = url_for('pages', array('campaign_id' => $page->getCampaignId()));
        $a_options = array(
            'Back' => array(
                'href' => $url,
                'class' => "icon back ajax-load"
            )
        );
        echo icon_links($a_options);
    ?>
</div>