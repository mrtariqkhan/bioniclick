<?php
    include_partial(
        'list',
        array(
            'pager'                     => $pager,
            'campaign'                  => $campaign,
            'permissions'               => $permissions,
            'permissions_combos'        => $permissions_combos,
            'need_was_deleted_column'   => $need_was_deleted_column,
        )
    );
?>