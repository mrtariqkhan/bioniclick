
<?php $object = $form->getObject(); ?>

<?php $reloadTreeKey = TreeNodeCampaign::generateTreeNodeKeyPages($object->getCampaign()); ?>

<h1>Edit page</h1>

<div class="inner">
    <?php $id = $object->getId(); ?>
    <?php $campaignId = $object->getCampaignId(); ?>

    <?php $urlAction = url_for('page_update', array('id' => $id)); ?>
    <?php $urlAfter = url_for('pages', array('campaign_id' => $campaignId)); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'back_url'          => $urlAfter,
                'submit_name'       => 'Update',
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel'),
                'form_special_data' => $reloadTreeKey
            )
        );
    ?>
</div>
