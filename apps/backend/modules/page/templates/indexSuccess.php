<?php use_helper('Link'); ?>
<?php use_helper('Pager'); ?>

<?php
    include_partial(
        'global/breadCrumb',
        array(
            'object' => $campaign,
            'title'  => 'Page list',
        )
    );
?>

<div>
    <div class="ajax-pagable">
        <?php
            include_partial(
                'list',
                array(
                    'pager'                     => $pager,
                    'campaign'                  => $campaign,
                    'permissions'               => $permissions,
                    'permissions_combos'        => $permissions_combos,
                    'need_was_deleted_column'   => $need_was_deleted_column,
                )
            );
        ?>
    </div>

    <?php
        $a_options = array();

        $ifShowUpdateLinksParent = WasDeletedHelper::ifCanUpdate($campaign->getRawValue());
        if ($ifShowUpdateLinksParent) {
            if ($permissions['can_update']) {
                $a_options['New'] = array(
                    'href' => url_for('page_new', array('campaign_id' => $campaign->getId())),
                    'class' => "icon new ajax-load"
                );
            }
        }

        if ($permissions['can_read']) {
            $a_options['Back'] = array(
                'href'  => url_for('campaigns_for_advertiser', array('advertiser_id' => $campaign->getAdvertiserId())),
                'class' => "icon back ajax-load"
            );
        }

        echo icon_links($a_options);
    ?>
</div>