<?php use_helper('Link'); ?>
<?php use_helper('Pager'); ?>

<?php $reloadTreeKey = TreeNodeCampaign::generateTreeNodeKeyPages($campaign->getRawValue()); ?>

<?php
    $pagerHtmlString = pager(
        $pager->getRawValue(),
        sfConfig::get('app_pager_max_per_pages'),
        url_for('pages_list', array('campaign_id' => $campaign->getId())),
        null,
        'ajax-pager'
    );
?>

<p>
    <?php 
        include_partial(
            'global/pager_list_header',
            array(
                'pager' => $pager,
                'pagerHtmlString' => $pagerHtmlString
            )
        ); 
    ?>
</p>

<div class="table-block">
    <table  class="table">
        <thead>
            <tr>
                <th class="first">&nbsp;</th>
                <th>Type</th>
                <th>Page url</th>
                <?php if ($need_was_deleted_column) : ?>
                    <th><?php echo WasDeletedHelper::TITLE; ?></th>
                <?php endif; ?>
                <th class="last"></th>
            </tr>
        </thead>
        <tbody>
            <?php
                $list_buttons = array(
                    'Instructions' => array(
                        'class' => 'icon instructions ajax-load'
                    ),
                    'Combos' => array(
                        'class' => 'icon combos ajax-load'
                    ),
                    'Edit' => array(
                        'class' => 'icon edit ajax-load'
                    ),
                    'Delete' => array(
                        'class' => 'icon delete'
                    )
                );

                $legend_buttons = array();
            ?>
            <?php $i = $pager->getFirstIndice(); ?>
            <?php $list = $pager->getResults(); ?>
            <?php $list = $list->getRawValue(); ?>
            <?php foreach ($list as $object): ?>
                <tr class="<?php echo ($i%2) ? 'odd' : 'even'; ?>">
                    <td><?php echo $i; ?></td>
                    <td><?php echo $object->getProtocolType(); ?></td>
                    <td><?php echo $object->getPageUrl(); ?></td>

                    <?php if ($need_was_deleted_column) : ?>
                        <td><?php echo $object->getWasDeletedString(); ?></td>
                    <?php endif; ?>

                    <td>
                        <?php $id = $object->getId(); ?>
                        <?php $urlAfter = url_for('pages', array('campaign_id' => $object->getCampaignId())); ?>
                        <?php $urlInstrictions = url_for('page_instructions', array('id' => $id)); ?>

                        <?php
                            $a_options = array();
                            
                            if ($permissions['can_instructions']) {
                                $a_options['Instructions'] = $list_buttons['Instructions'];
                                $a_options['Instructions']['href'] = $urlInstrictions;

                                $legend_buttons['Instructions'] = & $list_buttons['Instructions'];
                            }

                            if ($permissions_combos['can_read']) {
                                $a_options['Combos'] = $list_buttons['Combos'];
                                $a_options['Combos']['href'] = url_for('combos', array('page_id' => $id));

                                $legend_buttons['Combos'] = & $list_buttons['Combos'];
                            }


                            $ifShowUpdateLinks = WasDeletedHelper::ifCanUpdate($object);
                            if ($ifShowUpdateLinks) {
                                if ($permissions['can_update']) {
                                    $a_options['Edit'] = $list_buttons['Edit'];
                                    $a_options['Edit']['href'] = url_for('page_edit', array('id' => $id));

                                    $legend_buttons['Edit'] = & $list_buttons['Edit'];
                                }

                                if ($permissions['can_delete']) {

                                    $a_options['Delete'] = link_to_ajax_delete(
                                        '',
                                        url_for('page_delete', array('id' => $id)),
                                        array(
                                            'title'             => 'Delete',
                                            'class'             => 'icon delete',
                                            'special_form_data' => $reloadTreeKey,
                                            'after_url'         => $urlAfter,
                                        )
                                    );

                                    $legend_buttons['Delete'] = & $list_buttons['Delete'];
                                }
                            }

                            echo icon_links($a_options, array('class' => 'icon-links clearfix'), false);
                        ?>
                    </td>
                </tr>
                <?php ++$i; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php
    include_partial(
         'global/legend',
          array(
            'legend_buttons'  => $legend_buttons,
          )
    ); 
?>

<?php echo $pagerHtmlString; ?>

