<?php $object = $form->getObject(); ?>

<?php $reloadTreeKey = TreeNodeCampaign::generateTreeNodeKeyPages($object->getCampaign()); ?>

<h1>New page</h1>

<div class="inner">
    <?php $campaignId = $object->getCampaignId(); ?>

    <?php $urlAction = url_for('page_create'); ?>
    <?php $urlAfter = url_for('pages', array('campaign_id' => $campaignId)); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'back_url'          => $urlAfter,
                'submit_name'       => 'Create',
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel'),
                'form_special_data' => $reloadTreeKey,
            )
        );
    ?>
</div>