<?php

/**
 * page actions.
 *
 * @package    bionic
 * @subpackage page
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class pageActions extends GeneralActions {

    public function  preExecute() {

        $this->tableName = 'Page';
        parent::preExecute();

        $this->getResponse()->addStylesheet(
            '/css/project/reset.css',
            'last'
        );
    }

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request) {

        $this->executeList($request);
        $this->setTemplate('index');
    }
    
    public function executeList(sfWebRequest $request) {
        
        $campaignId = $request->getParameter('campaign_id');
        $campaign = CampaignTable::findById($campaignId);
        $this->forward404If(
            empty($campaign),
            sprintf('Object campaign does not exist (%s).', $campaignId)
        );
        $this->checkIsAllowableObject($campaign, false);
        $this->campaign = $campaign;

        $specialQuery = PageTable::getIndexQueryByUserAndCampaignId($this->user, $campaignId);
        $this->pager = parent::initPager($request, $specialQuery);
        $this->need_was_deleted_column = WasDeletedHelper::ifNeedWasDeletedColumn($this->user);

        $this->permissions = $this->configurePermissionsAll();

        $securityHelperCombo = new SecurityHelper($this, 'combo');
        $this->permissions_combos = $securityHelperCombo->checkBasePermissions();
    }

    public function executeNew(sfWebRequest $request) {

        $campaignId = $request->getParameter('campaign_id');
        $campaign = CampaignTable::findById($campaignId);
        $this->forward404If(
            empty($campaign),
            sprintf('Object campaign does not exist (%s).', $campaignId)
        );
        $this->checkIsAllowableObject($campaign);

        $object = new Page();
        $object->setCampaign($campaign);

        $this->form = new PageForm($object);
    }

    public function executeCreate(sfWebRequest $request) {

        $form = new PageForm();

        return $this->processAjaxForm($request, $form);
    }

    public function executeEdit(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $this->form = new PageForm($object);
    }

    public function executeUpdate(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $form = new PageForm($object);
        return $this->processAjaxForm($request, $form);
    }

    public function executeInstructions(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object, false);

        $this->page = $object;
    }

    public function executeDelete(sfWebRequest $request) {
        
        return $this->processAjaxDelete($request);
    }
}
