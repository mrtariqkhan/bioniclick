<?php

/**
 * advertiserIncomingNumberPool actions.
 *
 * @package    bionic
 * @subpackage advertiserIncomingNumberPool
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class advertiserIncomingNumberPoolActions extends GeneralActions {

    const FILTER_ATTRIBUTE_NAME_COMPANY_SUB_ADVERTISERS     = 'sub_advertisers_numbers_filter';

    public function  preExecute() {

        $this->tableName = 'AdvertiserIncomingNumberPool';
        parent::preExecute();
    }

    public function executeIndex(sfWebRequest $request) {

//        $this->advertiser_numbers_count = $this->user->calcPurchasedAdvertiserNumbers();
//        //$this->availableNumberCountToPurchase = $user->calcAvailableNumberCountToPurchase();
//        $this->purchased_parent_company_numbers = $this->advertiser_free_numbers_count;

        $filterValues = $this->getFilter();
        $this->filter_form = $this->getFilterForm($filterValues);
        $this->filter_form->setDefaults($filterValues);

        $this->executeList($request);

        $this->is_advertiser = $this->user->isAdvertiserUser();
        $this->is_debtor = false;
        if ($this->is_advertiser) {
            $advertiser = $this->user->getAdvertiser();
            $this->advertiser_free_numbers_count = $advertiser->calcAvailibleToPurchaseNumbers();
            $this->is_debtor = $advertiser->getIsDebtor();
        }

        $this->setTemplate('index');
    }

    public function executeList(sfWebRequest $request) {

        $filterValues = $this->getFilter();
        $filterForm = $this->getFilterForm($filterValues);
        $queryParams = $filterForm->getFilteredValues($filterValues, true);

        $specialQuery = AdvertiserIncomingNumberPoolTable::createIndexQueryByUser(
            $this->user,
            $queryParams
        );

        $this->pager = parent::initPager($request, $specialQuery);
        $list = $this->pager->getResults();

        $objectFieldsHeaders = array(
            'getTwilioIncomingPhoneNumber'  => 'Dynamic Phone Number',
            'getCampaign'                   => 'Campaign assigned to',
            'getIsDefault'                  => 'Is Default',
        );

        $branchHelper = new BranchHelper();
        $this->branch_data = $branchHelper->makeRaws($this->user, $list, $objectFieldsHeaders);
    }

    public function executeListForCompany(sfWebRequest $request) {

        $companyId = $request->getParameter('company_id', null);
        $company = empty($companyId) ? null : CompanyTable::getInstance()->find($companyId);
    
        $filterValues = array();
        $userAdmin = $company->getAdmin();
        $specialQuery = AdvertiserIncomingNumberPoolTable::createIndexQueryByUser(
            $userAdmin,
            $filterValues
        );

        $this->pager = parent::initPager($request, $specialQuery);
        $list = $this->pager->getResults();

        $objectFieldsHeaders = array(
            'getTwilioIncomingPhoneNumber'  => 'Dynamic Phone Number',
            'getCampaign'                   => 'Campaign assigned to',
            'getIsDefault'                  => 'Is Default',
        );

        $branchHelper = new BranchHelper();
        $this->branch_data = $branchHelper->makeRaws($this->user, $list, $objectFieldsHeaders);
        $this->company = $company;
    }

    public function executeListForCampaign(sfWebRequest $request) {

        $campaignId = $request->getParameter('campaign_id', null);
        $campaign = empty($campaignId) ? null : CampaignTable::getInstance()->find($campaignId);

        $specialQuery = AdvertiserIncomingNumberPoolTable::createQueryByCampaignId($campaignId);

        $this->pager = parent::initPager($request, $specialQuery);

        $this->campaign_id  = $campaignId;
        $this->campaign     = $campaign;
    }

    public function executeListForAdvertiser(sfWebRequest $request) {

        $advertiserId = $request->getParameter('advertiser_id', null);
        $advertiser = empty($advertiserId) ? null : AdvertiserTable::getInstance()->find($advertiserId);

        $specialQuery = AdvertiserIncomingNumberPoolTable::createQueryByAdvertiserId($advertiserId);

        $this->pager = parent::initPager($request, $specialQuery);

        $this->advertiser_id  = $advertiserId;
        $this->advertiser     = $advertiser;

        $this->is_debtor = $advertiser->getIsDebtor();
    }

    public function executeNewForCurrentAdvertiser(sfWebRequest $request) {

        $this->forward404If(!$this->user->isAdvertiserUser(), "User is not Advertiser user.");
        $advertiserId = $this->user->getAdvertiserId();

        $advertiser = $this->user->getAdvertiser();
        $this->forward404If($advertiser->getIsDebtor(), "Cannot purchase numbers for Debtor.");

        $object = new AdvertiserIncomingNumberPool();
        $object->setAdvertiserId($advertiserId);

        $this->form = $this->configureForm($object);
    }

    public function executeNewForAdvertiser(sfWebRequest $request) {

        $this->executeNew($request);

        $advertiserId = $request->getParameter('advertiser_id', null);
        $this->url_after = $this->generateUrl('advertiser_show', array('id' => $advertiserId));

        $this->setTemplate('new');
    }

    public function executeNew(sfWebRequest $request) {
  
        $advertiserId = $request->getParameter('advertiser_id', null);
        $advertiser = empty($advertiserId) ? null : AdvertiserTable::findById($advertiserId);
        $this->forward404If(empty($advertiser), "Advertiser has not been found.");
        $this->forward404If($advertiser->getIsDebtor(), "Cannot purchase numbers for Debtor.");

        $object = new AdvertiserIncomingNumberPool();
        $object->setAdvertiserId($advertiserId);
        $this->advertiser_name = $advertiser->getName();

        $this->form = $this->configureForm($object);

        $this->advertiser = $advertiser;

        $this->url_after = $this->generateUrl('advertiser_index');
    }

    public function executeCreateForCurrentAdvertiser(sfWebRequest $request) {

        $this->forward404If(!$this->user->isAdvertiserUser(), "User is not Advertiser user.");
        $advertiserId = $this->user->getAdvertiserId();

        $advertiser = $this->user->getAdvertiser();
        $this->forward404If($advertiser->getIsDebtor(), "Cannot purchase numbers for Debtor.");

        $object = new AdvertiserIncomingNumberPool();
        $object->setAdvertiserId($advertiserId);

        $form = $this->configureForm($object);

        return $this->processAjaxForm($request, $form);
    }

    public function executeCreate(sfWebRequest $request) {

        $advertiserId = $request->getParameter('advertiser_id', null);
        $advertiser = empty($advertiserId) ? null : AdvertiserTable::findById($advertiserId);
        $this->forward404If(empty($advertiser), "Advertiser has not been found.");
        $this->forward404If($advertiser->getIsDebtor(), "Cannot purchase numbers for Debtor.");
        
        $object = new AdvertiserIncomingNumberPool();
        $object->setAdvertiserId($advertiserId);

        $form = $this->configureForm($object);

        return $this->processAjaxForm($request, $form);
    }

    protected function configureForm($object) {

        $formOptions = array(
            AdvertiserIncomingNumberPoolFormNew::OPTION_USER => $this->user,
        );

        $form = new AdvertiserIncomingNumberPoolFormNew($object, $formOptions);

        return $form;
    }

    public function executeEditForCompany(sfWebRequest $request) {

        $this->executeEdit($request);

        $companyId = $request->getParameter('company_id', null);
        $company = CompanyTable::getInstance()->find($companyId);

        $this->checkIsAllowableObject($company);

        $this->url_after = $this->generateUrl('company_show', array('id' => $companyId));

        $this->setTemplate('edit');
    }

    public function executeEditForAdvertiser(sfWebRequest $request) {

        $this->executeEdit($request);

        $object = $this->getRoute()->getObject();
        $advertiserId = $object->getAdvertiserId();

        $this->url_after = $this->generateUrl('advertiser_show', array('id' => $advertiserId));

        $this->setTemplate('edit');
    }

    public function executeEdit(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $formOptions = array(
            AdvertiserIncomingNumberPoolFormEdit::OPTION_USER => $this->user,
        );
        $this->form = new AdvertiserIncomingNumberPoolFormEdit($object, $formOptions);
        $this->url_after = $this->generateUrl('advertiser_incoming_number_pool_index');
    }

    public function executeUpdate(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $options = array(
            AdvertiserIncomingNumberPoolFormEdit::OPTION_USER => $this->user,
        );
        $form = new AdvertiserIncomingNumberPoolFormEdit($object, $options);

        return $this->processAjaxForm($request, $form);
    }

    protected function saveValidForm($validForm) {

        $wasNew = $validForm->isNew();

        $savedObject = $validForm->save();

        if ($wasNew) {
            $message = 'Order to assign number(s) has been successfully created.';
            $this->getUser()->setFlash('message', $message, true);
        }

        return $savedObject;
    }

    public function executeDelete(sfWebRequest $request) {
        return $this->processAjaxDelete($request);
    }

    protected function deleteObject($object) {
        $object->deleteByUser($this->user);
    }

    protected function getFilterForm($defaults = array()) {

        $options = $this->getBranchFilterOptions();
        $options['user'] = $this->user;

        $filterForm = new AdvertiserIncomingNumberPoolFilterForm($defaults, $options);

        return $filterForm;
    }

    public function executeFilter(sfWebRequest $request) {

        $filterForm = $this->getFilterForm();
        return $this->processAjaxFilter($request, $filterForm);
    }

    public function executeRevert(sfWebRequest $request) {

        $error = '';

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $campaignId = $object->getCampaignId();

        try {
            $object->revertToAdvertiser($this->user);
        } catch (Exception $e) {
            $error = $e->getMessage();
        }

        $url = $this->generateUrl('campaign_show', array('id' => $campaignId));
        $result = array(
            'success'   => empty($error),
            'error'     => $error,
            'url'       => $url,
        );

        return $this->processAjaxResult($request, $result, false);
    }

    public function executeAjaxFillLocalAreaSelect(sfWebRequest $request) {

        $advertiserId = $request->getParameter(
            'advertiser_id', 
            $this->user->getAdvertiserId()
        );
        $advertiser = AdvertiserTable::findById($advertiserId);
        $this->checkIsAllowableObject($advertiser);

        $result = array();
        if ($request->isXmlHttpRequest()) {
            $phoneNumberType = $request->getParameter('phone_number_type');

            $resultExistent = TwilioLocalAreaCodeTable::findExistentInBionicPoolByPhoneNumberType(
                $phoneNumberType
            );

            $resultAll = TwilioLocalAreaCodeTable::findByPhoneNumberType(
                $phoneNumberType
            );

            $result = array_merge($resultExistent, $resultAll);
        }

        return $this->processAjaxResult($request, $result);
    }

    public function executeAjaxFillLocalAreaSelectForCampaign(sfWebRequest $request) {

        $advertiserId = $request->getParameter(
            'advertiser_id',
            $this->user->getAdvertiserId()
        );

        $result = array();
        if ($request->isXmlHttpRequest()) {
            $phoneNumberType = $request->getParameter('phone_number_type');

            $result = TwilioLocalAreaCodeTable::findExistentInAdvertiserPoolByPhoneNumberType(
                $advertiserId,
                $phoneNumberType
            );
        }

        return $this->processAjaxResult($request, $result);
    }

    public function executeAjaxFillPhoneNumbersCount(sfWebRequest $request) {

        $advertiserId = $request->getParameter(
            'advertiser_id', 
            $this->user->getAdvertiserId()
        );
        $advertiser = AdvertiserTable::getInstance()->find($advertiserId);
        $this->checkIsAllowableObject($advertiser);

        $result = array();
        if ($request->isXmlHttpRequest()) {
            $localAreaCodeId = $request->getParameter('local_area_code_id');

            $count = TwilioIncomingPhoneNumberTable::countFreeNumbersByLocalAreaCode(
                $localAreaCodeId
            );
            
            $result = array('count' => $count);
        }

        return $this->processAjaxResult($request, $result);
    }

    public function executeAjaxFillPhoneNumbersCountForCampaign(sfWebRequest $request) {

        $advertiserId = $request->getParameter(
            'advertiser_id',
            $this->user->getAdvertiserId()
        );
        $advertiser = AdvertiserTable::getInstance()->find($advertiserId);
        $this->checkIsAllowableObject($advertiser);

        $result = array();
        if ($request->isXmlHttpRequest()) {
            $localAreaCodeId = $request->getParameter('local_area_code_id');

            $count = TwilioIncomingPhoneNumberTable::countFreeNumbersByLocalAreaCodeForCampaign(
                $advertiserId,
                $localAreaCodeId
            );

            $result = array('count' => $count);
        }

        return $this->processAjaxResult($request, $result);
    }

//    protected function postSuccessfulCreation($savedObject) {
//
//        if (empty($savedObject) || !($savedObject instanceof AdvertiserIncomingNumberPool)) {
//            throw(new Exception('Saved number is empty or wrong typed!'));
//        }
//
//        return $this->doPostSuccessfulCreation($savedObject);
//    }

    public function executeSubAdvertisersIndex(sfWebRequest $request) {

        $filterValues = $this->getSubAdvertisersNumbersFilter();
        $this->filter_form = $this->getSubAdvertisersNumbersFilterForm();
        $this->filter_form->setDefaults($filterValues);

        $this->executeSubAdvertisersList($request);
        $this->setTemplate('subAdvertisersIndex');
    }

    public function executeSubAdvertisersList(sfWebRequest $request) {

        $filterValues = $this->getSubAdvertisersNumbersFilter();
        $filterForm = $this->getSubAdvertisersNumbersFilterForm();
        $queryParams = $filterForm->getFilteredValues($filterValues, true);

        $specialQuery = TwilioIncomingPhoneNumberTable::createIndexQueryForCompanySubAdvertiser($this->user, $queryParams);
        $this->pager = parent::initPager($request, $specialQuery);


        $objectFieldsHeaders = array(
            'getPhoneNumber' => 'Incoming number'
        );
        $list = $this->pager->getResults();
        $branchHelper = new BranchHelper();
        $this->branch_data = $branchHelper->makeRaws($this->user, $list, $objectFieldsHeaders);
    }

    public function executeSubAdvertisersNumbersFilter(sfWebRequest $request) {
        $filterForm = $this->getSubAdvertisersNumbersFilterForm();
        return $this->processAjaxFilter($request, $filterForm, self::FILTER_ATTRIBUTE_NAME_COMPANY_SUB_ADVERTISERS);
    }

    protected function getSubAdvertisersNumbersFilter() {
        return $this->getFilter(self::FILTER_ATTRIBUTE_NAME_COMPANY_SUB_ADVERTISERS);
    }

    protected function getSubAdvertisersNumbersFilterForm() {

        $options = array();

        if (!empty($this->user) && !$this->user->isAdvertiserUser()) {
            $options[CompanyTable::$LEVEL_NAME_ADVERTISER] = true;
        }

        return new CustomersNumbersFilterForm(null, $options);
    }
}
