<?php

class advertiserIncomingNumberPoolComponents extends GeneralComponents {

    public function executeListForAdvertiser() {

        $this->permissions = $this->configurePermissionsAll();

        $advertiser = $this->getVar('advertiser');
        $this->purchased_company_numbers = $advertiser->calcAvailibleToPurchaseNumbers();
    }

    public function executeListForCompany() {
        $this->permissions = $this->configurePermissionsAll();
    }

    public function executeListSimple() {
        $this->permissions = $this->configurePermissionsAll();
    }

    public function executeListForCampaign() {
        $this->permissions = $this->configurePermissionsAll();
    }

    public function executeSubAdvertisersList() {
        $this->permissions = $this->configurePermissionsAll();
    }
}
