<div class="account-wrap clearfix">
<!-- TODO:: add text for advertisers later
    <p class="display-text display-text-two">
            You have already purchased <?php //echo $advertiser_numbers_count; ?> incoming number(s) <br/>
            There are <?php //echo $advertiser_free_numbers_count; ?> incoming number(s) available to purchase
    </p>
-->
    <div class="ajax-filter-container">

        <?php if ($permissions['can_filter']): ?>
            <div class="filter-form-wrap filter-form-wrap-account-numbers">
                <h1>Search</h1>
                <div class="filter-form-wraper">
                    <?php
                        include_partial(
                            'global/form_ajax',
                            array(
                                'form'              => $filter_form,
                                'action_url'        => url_for('advertiser_incoming_number_pool_filter'),
                                'submit_name'       => 'Go!',
                                'back_url'          => '',
                                'form_class_name'   => 'ajax-filter',
                                'cancel_attributes' => array('class' => 'ajax-form-cancel')
                            )
                        );
                    ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="ajax-pagable ajax-pagable-two">
            <?php
                include_component(
                    'advertiserIncomingNumberPool',
                    'listSimple',
                    array(
                        'pager'         => $pager,
                        'branch_data'   => $branch_data,
                    )
                );
            ?>
        </div>

    </div>
</div>

<?php
//TODO:: add $purchased_parent_company_numbers checking for advertisers later
//    if ($permissions['can_create'] && $purchased_parent_company_numbers > 0) {
    if ($permissions['can_create'] && $is_advertiser) {
        $a_options = array();

        if ($is_debtor) {
            $a_options['Cannot purchase numbers for Debtor'] = array(
                'class'     => "icon new ajax-form-load",
            );
        } else {
            $a_options['Purchase numbers for your account'] = array(
                'class'     => "icon new ajax-form-load",
                'href'      => url_for('advertiser_incoming_number_pool_new_for_current_advertiser'),
                'back_url'  => url_for('advertiser_incoming_number_pool_index')
            );
        }
        echo icon_links($a_options);
    }
?>
