
<span id="advertiser_id_handler" advertiser_id="<?php echo $advertiser->getId(); ?>"></span>

<h1>Create order to purchase new numbers for '<?php echo $advertiser->getName(); ?>'</h1>

<div class="inner">

    <?php $urlAction = url_for('advertiser_incoming_number_pool_create', array('advertiser_id' => $advertiser->getId())); ?>

    <?php
        
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'submit_name'       => 'Order',
                'back_url'          => $url_after,
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel')
            )
        );
    ?>
</div>
