<h1>Create order to Purchase new number for your account</h1>

<div class="inner">

    <?php $urlAction = url_for('advertiser_incoming_number_pool_create_for_current_advertiser'); ?>
    <?php $urlAfter = url_for('advertiser_incoming_number_pool_index'); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'submit_name'       => 'Order',
                'back_url'          => $urlAfter,
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel')
            )
        );
    ?>
</div>

