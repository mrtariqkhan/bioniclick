
<div class="account-wrap clearfix">
    <div class="ajax-filter-container">
        <?php if ($permissions['can_filter']): ?>
            <div class="filter-form-wrap">
                <h1>Search</h1>
                <div class="filter-form-wraper">
                    <?php
                        include_partial(
                            'global/form_ajax',
                            array(
                                'form'              => $filter_form,
                                'action_url'        => url_for('company_sub_advertisers_incoming_number_pool_filter'),
                                'back_url'          => '',
                                'submit_name'       => 'Go!',
                                'form_class_name'   => 'ajax-filter',
                                'cancel_attributes' => array('class' => 'ajax-form-cancel')
                            )
                        );
                     ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="ajax-pagable">
            <?php
                include_component(
                    'advertiserIncomingNumberPool',
                    'subAdvertisersList',
                    array(
                        'pager'         => $pager,
                        'branch_data'   => $branch_data,
                    )
                );
            ?>
        </div>
    </div>
</div>

