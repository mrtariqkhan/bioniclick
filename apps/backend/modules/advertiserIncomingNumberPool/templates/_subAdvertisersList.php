<?php use_helper('Link'); ?>
<?php use_helper('Pager')?>

<?php    
    $pagerHtmlString = pager(
        $pager->getRawValue(),
        sfConfig::get('app_pager_max_per_pages'),
        url_for('company_sub_advertisers_incoming_number_pool_list'),
        null,
        'ajax-pager'
    );
?>

<p>
    <?php 
        include_partial(
            'global/pager_list_header',
            array(
                'pager' => $pager,
                'pagerHtmlString' => $pagerHtmlString
            )
        ); 
    ?>
</p>

<?php $headers = $branch_data['headers']; ?>
<?php $raws    = $branch_data['raws']; ?>
<div class="table-block">
    <table class="table">
        <thead>
            <tr>
                <th class="first">&nbsp;</th>
                <?php foreach ($headers as $headerTitle): ?>
                    <th>
                        <?php echo $headerTitle; ?>
                    </th>
                <?php endforeach; ?>
                <th class="last">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = $pager->getFirstIndice(); ?>
            <?php foreach ($raws as $raw): ?>

                <tr class="<?php echo ($i%2) ? 'odd' : 'even'; ?>">
                    <td><?php echo $i; ?></td>

                    <?php foreach ($raw as $cell): ?>
                        <td><?php echo $cell; ?></td>
                    <?php endforeach; ?>

                    <td class="last">
                        <!-- TODO:: think if we can delete and edit customers phones -->
                        &nbsp;
                    </td>
                </tr>
                <?php ++$i; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>
<?php echo $pagerHtmlString; ?>
