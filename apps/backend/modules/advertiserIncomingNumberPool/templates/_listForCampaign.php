
<?php $campaign_id = $campaign->getId(); ?>
<?php $advertiser = $campaign->getAdvertiser(); ?>

<?php
    include_partial(
        'advertiserIncomingNumberPool/list',
        array(
            'pager'                 => $pager,
            'permissions'           => $permissions,
            'url'                   => url_for('advertiser_incoming_number_pool_list_campaign', array('campaign_id' => $campaign_id)),
            'urlAfterDelete'        => url_for('campaign_show', array('id' => $campaign_id)),
            'show_campaign_column'  => false,
            'show_revert_link'      => true,
            'show_update_links'     => false,
        )
    );
?>

<?php
    $a_options = array();

    $ifShowUpdateLinks = WasDeletedHelper::ifCanUpdate($campaign->getRawValue());
    if ($ifShowUpdateLinks) {
        if ($permissions['can_update']) {
            //TODO:: Add checking direct ownership between user and advertiser
            if (AdvertiserIncomingNumberPoolTable::calcFreeAdvertiserNumbers($campaign_id) > 0) {
                $a_options['Assign numbers'] = array(
                    'href'  => url_for('campaign_show_add_numbers_new', array('id' => $campaign_id)),
                    'class' => 'edit_campaign main icon assign-numbers ajax-load',
                );
            } else {
                $a_options['Assign numbers'] = array(
                    'class' => 'edit_campaign main icon assign-numbers',
                    'title' => "To assign new numbers to this campaign, purchase new phone numbers for the '$advertiser' Number Pool. There are currently no available numbers to assign to campaign for this company.",
                );
            }
        }
    }

    echo icon_links($a_options);
?>