<?php
    include_component(
        'advertiserIncomingNumberPool',
        'listForAdvertiser',
        array(
            'pager'         => $pager,
            'advertiser'    => $advertiser,
            'is_debtor'     => $is_debtor,
        )
    );
?>
