<?php use_helper('Link'); ?>
<?php use_helper('Pager'); ?>

<?php if ($permissions['can_read']): ?>

    <?php include_partial('global/flash_message', array('flash_name' => 'message')); ?>

    <?php
        $pagerHtmlString = pager(
            $pager->getRawValue(),
            sfConfig::get('app_pager_max_per_pages'),
            $url,
            null,
            'ajax-pager'
        );
    ?>

    <p>
        <?php
            include_partial(
                'global/pager_list_header',
                array(
                    'pager' => $pager,
                    'pagerHtmlString' => $pagerHtmlString
                )
            );
        ?>
    </p>
    <div class="table-block">
        <table class="table">
            <thead>
                <tr>
                    <th class="first">&nbsp;</th>
                    <th>Dynamic Phone Number</th>
                    <?php if ($show_campaign_column): ?>
                        <th>Campaign assigned to</th>
                    <?php endif;?>
                    <th class="last"></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $list_buttons = array(
                        'Edit' => array(
                            'class' => 'icon edit ajax-form-load'
                        ),
                        'Revert' => array(
                            'class' => 'icon reload ajax-load'
                        ),
                        'Delete' => array(
                            'class' => 'icon delete'
                        )
                    );

                    $legend_buttons = array();
                ?>
                <?php $i = $pager->getFirstIndice(); ?>
                <?php $list = $pager->getResults(); ?>
                <?php $list = $list->getRawValue(); ?>
                <?php foreach ($list as $object): ?>

                    <?php $isDefault = $object->getIsDefault(); ?>

                    <tr class="<?php echo ($i%2) ? 'odd' : 'even'; ?>">
                        <td><?php echo $i; ?></td>
                        <td>
                            <?php echo $object->getTwilioIncomingPhoneNumber(); ?>
                            <?php if ($isDefault): ?>
                                <strong>(default)</strong>
                            <?php endif; ?>
                        </td>
                        <?php if ($show_campaign_column): ?>
                            <td>
                                <?php $campaignId = $object->getCampaignId(); ?>
                                <?php echo empty($campaignId) ? '' : $object->getCampaign(); ?>
                            </td>
                        <?php endif;?>
                        <td class="last">
                            <?php
                                $a_options = array();

                                $id = $object->getId();
                                $ifShowUpdateLinks = WasDeletedHelper::ifCanUpdate($object);
                                if ($ifShowUpdateLinks) {
                                    if ($show_revert_link) {
                                        if ($permissions['can_revert']) {
                                            if ($isDefault) {
                                                $a_options["Cannot revert default phone number to advertiser's pool"] = & $list_buttons['Revert'];
                                            } else {
                                                $a_options["Revert to advertiser's pool"] = $list_buttons['Revert'];
                                                $a_options["Revert to advertiser's pool"]['href'] = url_for(
                                                    'advertiser_incoming_number_pool_revert',
                                                    array('id' => $id)
                                                );

                                                $legend_buttons["Revert to advertiser's pool"] = & $list_buttons['Revert'];
                                            }
                                        }
                                    }

                                    if ($show_update_links) {

                                        $url = url_for('advertiser_incoming_number_pool_edit', array('id' => $id));

                                        if ($permissions['can_update']) {
                                            if ($isDefault) {
                                                $a_options['Cannot edit default phone number'] = & $list_buttons['Edit'];
                                            } else {
                                                $a_options['Edit'] = $list_buttons['Edit'];
                                                $a_options['Edit']['href'] = $url;

                                                $legend_buttons['Edit'] = & $list_buttons['Edit'];
                                            }
                                        }

                                        if ($permissions['can_delete']) {
                                            if ($isDefault) {
                                                $a_options['Cannot delete default phone number'] = & $list_buttons['Delete'];
                                            } else {
                                                $a_options['Delete'] = link_to_ajax_delete(
                                                    '',
                                                    url_for('advertiser_incoming_number_pool_delete', array('id' => $id)),
                                                    array(
                                                        'title' => 'Delete',
                                                        'class' => 'icon delete',
                                                        'after_url' => $urlAfterDelete
                                                    )
                                                );

                                                $legend_buttons['Delete'] = & $list_buttons['Delete'];
                                            }
                                        }
                                    }
                                }

                                echo icon_links($a_options, array('class' => 'icon-links clearfix'), false);
                            ?>
                        </td>
                    </tr>
                    <?php ++$i; ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <?php
        include_partial(
             'global/legend',
              array(
                'legend_buttons'  => $legend_buttons,
              )
        ); 
    ?>

    <?php echo $pagerHtmlString; ?>
<?php endif; ?>
