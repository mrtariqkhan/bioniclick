<?php $object = $form->getObject(); ?>

<h1>
    Select the campaign which you would like to assign this number: 
    <?php echo ($object->getTwilioIncomingPhoneNumber())?>
</h1>

<div class="inner">

    <?php $urlAction = url_for('advertiser_incoming_number_pool_update', array('id' => $object->getId())); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'submit_name'       => 'Update',
                'back_url'          => $url_after,
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel')
            )
        );
    ?>
</div>
