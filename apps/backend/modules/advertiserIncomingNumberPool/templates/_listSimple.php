
<?php
//    include_partial(
//        'list',
//        array(
//            'pager'                 => $pager,
//            'branch_data'           => $branch_data,
//            'permissions'           => $permissions,
//            'url'                   => url_for('advertiser_incoming_number_pool_list'),
//            'urlAfterDelete'        => url_for('advertiser_incoming_number_pool_index'),
//            'show_campaign_column'  => true,
//            'show_revert_link'      => false,
//            'show_update_links'     => true,
//        )
//    );
?>

<?php
    $url              = url_for('advertiser_incoming_number_pool_list');
    $urlAfterDelete   = url_for('advertiser_incoming_number_pool_index');
//    $show_campaign_column   = true;
//    $show_revert_link       = false;
//    $show_update_links      = true;
?>





<?php use_helper('Link'); ?>
<?php use_helper('Pager'); ?>

<?php if ($permissions['can_read']): ?>

    <?php $headers = $branch_data['headers']; ?>
    <?php $raws    = $branch_data['raws']; ?>

    <?php include_partial('global/flash_message', array('flash_name' => 'message')); ?>

    <?php
        $pagerHtmlString = pager(
            $pager->getRawValue(),
            sfConfig::get('app_pager_max_per_pages'),
            $url,
            null,
            'ajax-pager'
        );
    ?>

    <p>
        <?php
            include_partial(
                'global/pager_list_header',
                array(
                    'pager' => $pager,
                    'pagerHtmlString' => $pagerHtmlString
                )
            );
        ?>
    </p>
    <div class="table-block">
        <table class="table">
            <thead>
                <tr>
                    <th class="first">&nbsp;</th>
                        
                    <?php foreach ($headers as $headerKey => $headerTitle): ?>
                        <?php if ($headerKey != 'getIsDefault'): ?>
                            <th><?php echo $headerTitle; ?></th>
                        <?php endif; ?>
                    <?php endforeach; ?>
                    <th class="last"></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $list_buttons = array(
                        'Edit' => array(
                            'class' => 'icon edit ajax-form-load'
                        ),
                        'Revert' => array(
                            'class' => 'icon reload ajax-load'
                        ),
                        'Delete' => array(
                            'class' => 'icon delete'
                        )
                    );

                    $legend_buttons = array();
                ?>
                <?php $i = $pager->getFirstIndice(); ?>
                <?php foreach ($raws as $id => $raw): ?>

                    <?php $isDefault = $raw['getIsDefault']; ?>

                    <tr class="<?php echo ($i%2) ? 'odd' : 'even'; ?>">

                        <td><?php echo $i; ?></td>

                        <?php $raw = $raw->getRawValue(); ?>
                        <?php foreach ($raw as $key => $cell): ?>
                            <?php if ($key != 'getIsDefault'): ?>
                                <td><?php echo $cell; ?></td>
                            <?php endif; ?>
                        <?php endforeach; ?>

                        <td class="last">
                            <?php
                                $a_options = array();

                                $ifShowUpdateLinks = WasDeletedHelper::ifCanUpdateAsArray($raw);
                                if ($ifShowUpdateLinks) {

                                    $url = url_for('advertiser_incoming_number_pool_edit', array('id' => $id));

                                    if ($permissions['can_update']) {
                                        if ($isDefault) {
                                            $a_options['Cannot edit default phone number'] = & $list_buttons['Edit'];
                                        } else {
                                            $a_options['Edit'] = $list_buttons['Edit'];
                                            $a_options['Edit']['href'] = $url;

                                            $legend_buttons['Edit'] = & $list_buttons['Edit'];
                                        }
                                    }

                                    if ($permissions['can_delete']) {
                                        if ($isDefault) {
                                            $a_options['Cannot delete default phone number'] = & $list_buttons['Delete'];
                                        } else {
                                            $a_options['Delete'] = link_to_ajax_delete(
                                                '',
                                                url_for('advertiser_incoming_number_pool_delete', array('id' => $id)),
                                                array(
                                                    'title' => 'Delete',
                                                    'class' => 'icon delete',
                                                    'after_url' => $urlAfterDelete
                                                )
                                            );

                                            $legend_buttons['Delete'] = & $list_buttons['Delete'];
                                        }
                                    }
                                }

                                echo icon_links($a_options, array('class' => 'icon-links clearfix'), false);
                            ?>
                        </td>
                    </tr>
                    <?php ++$i; ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <?php
        include_partial(
             'global/legend',
              array(
                'legend_buttons'  => $legend_buttons,
              )
        ); 
    ?>

    <?php echo $pagerHtmlString; ?>
<?php endif; ?>
