
<?php //$advertiser_id = $advertiser->getId();?>

<?php
//    include_partial(
//        'advertiserIncomingNumberPool/list',
//        array(
//            'pager'                 => $pager,
//            'permissions'           => $permissions,
//            'url'                   => url_for('advertiser_incoming_number_pool_list_advertiser', array('advertiser_id' => $advertiser_id)),
//            'urlAfterDelete'        => url_for('advertiser_show', array('id' => $advertiser_id)),
//            'show_campaign_column'  => true,
//            'show_revert_link'      => false,
//            'show_update_links'     => false,
//        )
//    );
?>


<?php use_helper('Link'); ?>
<?php use_helper('Pager'); ?>

<?php
    $advertiser_id = $advertiser->getId();

    $url            = url_for('advertiser_incoming_number_pool_list_advertiser', array('advertiser_id' => $advertiser_id));
    $urlAfterDelete = url_for('advertiser_show', array('id' => $advertiser_id));
    $routeNameEdit  = 'advertiser_incoming_number_pool_edit_for_advertiser';
//    $show_campaign_column   = true;
//    $show_revert_link       = false;
//    $show_update_links      = false;
?>

<?php if ($permissions['can_read']): ?>

    <?php include_partial('global/flash_message', array('flash_name' => 'message')); ?>

    <?php
        $pagerHtmlString = pager(
            $pager->getRawValue(),
            sfConfig::get('app_pager_max_per_pages'),
            $url,
            null,
            'ajax-pager'
        );
    ?>

    <p>
        <?php
            include_partial(
                'global/pager_list_header',
                array(
                    'pager' => $pager,
                    'pagerHtmlString' => $pagerHtmlString
                )
            );
        ?>
    </p>
    <div class="table-block">
        <table class="table">
            <thead>
                <tr>
                    <th class="first">&nbsp;</th>
                    <th>Dynamic Phone Number</th>
                    <th>Campaign assigned to</th>
                    <th class="last"></th>
                </tr>
            </thead>
            <tbody>
                <?php
                    $list_buttons = array(
                        'Edit' => array(
                            'class' => 'icon edit ajax-form-load'
                        ),
                        'Revert' => array(
                            'class' => 'icon reload ajax-load'
                        ),
                        'Delete' => array(
                            'class' => 'icon delete'
                        )
                    );

                    $legend_buttons = array();
                ?>
                <?php $i = $pager->getFirstIndice(); ?>
                <?php $list = $pager->getResults(); ?>
                <?php $list = $list->getRawValue(); ?>
                <?php foreach ($list as $object): ?>

                    <?php $isDefault = $object->getIsDefault(); ?>

                    <tr class="<?php echo ($i%2) ? 'odd' : 'even'; ?>">
                        <td><?php echo $i; ?></td>
                        <td>
                            <?php echo $object->getTwilioIncomingPhoneNumber(); ?>
                            <?php if ($isDefault): ?>
                                <strong>(default)</strong>
                            <?php endif; ?>
                        </td>
                        <td>
                            <?php $campaignId = $object->getCampaignId(); ?>
                            <?php echo empty($campaignId) ? '' : $object->getCampaign(); ?>
                        </td>
                        <td class="last">
                            <?php
                                $a_options = array();

                                $id = $object->getId();
                                $ifShowUpdateLinks = WasDeletedHelper::ifCanUpdate($object);
                                if ($ifShowUpdateLinks) {

                                    $url = url_for($routeNameEdit, array('id' => $id));

                                    if ($permissions['can_update']) {
                                        if ($isDefault) {
                                            $a_options['Cannot edit default phone number'] = & $list_buttons['Edit'];
                                        } else {
                                            $a_options['Edit'] = $list_buttons['Edit'];
                                            $a_options['Edit']['href'] = $url;

                                            $legend_buttons['Edit'] = & $list_buttons['Edit'];
                                        }
                                    }

                                    if ($permissions['can_delete']) {
                                        if ($isDefault) {
                                            $a_options['Cannot delete default phone number'] = & $list_buttons['Delete'];
                                        } else {
                                            $a_options['Delete'] = link_to_ajax_delete(
                                                '',
                                                url_for('advertiser_incoming_number_pool_delete', array('id' => $id)),
                                                array(
                                                    'title' => 'Delete',
                                                    'class' => 'icon delete',
                                                    'after_url' => $urlAfterDelete
                                                )
                                            );

                                            $legend_buttons['Delete'] = & $list_buttons['Delete'];
                                        }
                                    }
                                }

                                echo icon_links($a_options, array('class' => 'icon-links clearfix'), false);
                            ?>
                        </td>
                    </tr>
                    <?php ++$i; ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <?php
        include_partial(
             'global/legend',
              array(
                'legend_buttons'  => $legend_buttons,
              )
        ); 
    ?>

    <?php echo $pagerHtmlString; ?>
<?php endif; ?>

    
    
<?php
    $a_options = array();

    if ($is_debtor) {
        $a_options['Cannot purchase numbers for Debtor'] = array(
            'class'     => "icon assign-numbers ajax-load",
        );
    } else {
        $ifShowUpdateLinks = WasDeletedHelper::ifCanUpdate($advertiser->getRawValue());
        if ($ifShowUpdateLinks) {
            if ($permissions['can_create'] && $purchased_company_numbers) {
                $a_options['Purchase numbers'] = array(
                    'href'      => url_for('advertiser_incoming_number_pool_new_for_advertiser', array('advertiser_id' => $advertiser_id)),
                    'back_url'  => url_for('advertiser_show', array('id' => $advertiser_id)),
                    'class'     => "icon assign-numbers ajax-load",
                );
            }
        }
    }

    echo icon_links($a_options);
?>
