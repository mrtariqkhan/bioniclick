<?php 
    echo "<?xml version=\"1.0\"?><Response>";
    if (isset($errorMessageSpecific)) 
	{
        echo "<Say>$errorMessageSpecific</Say>";
    } 
	else if (isset($TwiMLBody)) 
	{
        echo html_entity_decode($TwiMLBody);
    } 
	else 
	{
        echo "<Say>We are sorry. This is error.</Say>";
    }
    echo "</Response>";
?>