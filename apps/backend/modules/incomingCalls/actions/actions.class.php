<?php

/**
 * incomingCalls actions.
 *
 * @package    bionic
 * @subpackage incomingCalls
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class incomingCallsActions extends sfActions {

    /**
     * Is executed by Twilio request for registering incoming Twilio calls.
     * Sets the parameters for createSuccess.php.
     * @param $request web-request wrapped by Symfony
     */
    public function preExecute() {

        parent::preExecute();

        $this->getResponse()->addStylesheet(
            '/css/project/reset.css',
            'last'
        );
    }

    /**
     * Here we must set:
     *  call has been started   : params are (CallSid, AccountSid, Caller, Called, CallerCity, CallerState, CallerZip, CallerCountry)
     *  call has been finished  : params are (CallSid, AccountSid, CallStatus)
     * @param sfWebRequest $request
     */
/*
    public function executeCreate(sfWebRequest $request) {

        $this->TwiMLBody = '';
        $method = TwilioIncomingCallTable::METHOD_POST;
        $twilioParams = $request->getPostParameters();
        if (empty($twilioParams)) {
            $twilioParams = $request->getGetParameters();
            $method = TwilioIncomingCallTable::METHOD_GET;
        }

        $result = TwilioIncomingCallTable::processTwilioCallSummary($method, $twilioParams);

        Loggable::putArrayLogMessage(
            ApplicationLogger::TWILIO_LOG_TYPE,
            sfLogger::INFO,
            $result
        );

        $this->TwiMLBody            = $result['TwiMLBody'];
        $this->errorMessageSpecific = $result['errorMessageSpecific'];
    }
*/
    //TODO:: think why above does not work
    public function executeCreate(sfWebRequest $request) {

        $success = true;
        $this->TwiMLBody = '';
        $method = 'POST';
        $twilioParams = $request->getPostParameters();
        if (empty($twilioParams)) {
            $twilioParams = $request->getGetParameters();
            $method = 'GET';
        }
        
        Loggable::putArrayLogMessage(
            ApplicationLogger::TWILIO_LOG_TYPE,
            sfLogger::INFO, $twilioParams,
            "\n Twilio $method Params:"
        );
        if (empty($twilioParams) ||
            !array_key_exists('AccountSid', $twilioParams) ||
            !TwilioAccountTable::findBySid($twilioParams['AccountSid'])
        ) {
            return sfView::SUCCESS;
        }
        
        $incomingCall = TwilioIncomingCallTable::getBySid($twilioParams['CallSid']);
        if (empty($incomingCall)) {
            $incomingCall = TwilioIncomingCallTable::createIncomingCall($twilioParams);
            if ($incomingCall) {
                if ($incomingCall->getCallStatus() == TwilioIncomingCallTable::STATUS_COMPLETED) {
                    $incomingCall->createQueueForUpdating();
                }
                $this->TwiMLBody = IncomingCallHandler::getTwiMlBody($incomingCall);
                Loggable::putLogMessage(
                    ApplicationLogger::TWILIO_LOG_TYPE,
                    "TwiMlBody = " . $this->TwiMLBody,
                    sfLogger::INFO
                );
            } else {
                $this->errorMessageSpecific = "We are sorry. This is error.";
            }
        } else {
            TwilioIncomingCallTable::updateIncomingCall($incomingCall, $twilioParams);
            $incomingCall->createQueueForUpdating();
        }
    }

    public function executeCorrectPhoneCallsTask(sfWebRequest $request) {

        $task = TaskHelper::createAndRunTask(
            'CorrectPhoneCallsTask',
            array(),
            array()
        );

        return sfView::NONE;
    }
    
    public function executeMarkToUpdateOldInProgressCallsTask(sfWebRequest $request) {

        $task = TaskHelper::createAndRunTask(
            'MarkToUpdateOldInProgressCallsTask',
            array(),
            array()
        );

        return sfView::NONE;
    }
    
    public function executeWhisper(sfWebRequest $request) {
    	
    	$this->whisperText = $request->getParameter('whisper');
    	
    	//show nothing if no whisper text is available
    	if(!$this->whisperText){die();}
    	
    }
    
    
    
    
    
}
