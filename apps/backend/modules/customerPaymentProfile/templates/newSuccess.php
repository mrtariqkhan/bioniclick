<h1>New credit card</h1>

<div class="inner">
    
    <?php $urlAfter = url_for('customer_payment_profile'); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $actionUrl,
                'back_url'          => $urlAfter,
                'submit_name'       => 'Save',
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel'),
            )
        );
    ?>
</div>