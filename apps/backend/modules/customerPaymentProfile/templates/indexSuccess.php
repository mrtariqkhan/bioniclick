<?php use_helper('Link'); ?>

<?php
    $buttonUp       = "Up card's priority";
    $buttonDown     = "Down card's priority";
    $buttonEdit     = 'Edit';
    $buttonDelete   = 'Delete';

    $list_buttons = array(
        $buttonUp => array(
            'class' => 'icon priority-up ajax-form-load'
        ),
        $buttonDown => array(
            'class' => 'icon priority-down ajax-form-load'
        ),
        $buttonEdit => array(
            'class' => 'icon edit ajax-form-load'
            // icon edit ajax-form-load
        ),
        $buttonDelete => array(
            'class' => 'icon delete'
        )
    );

    $legend_buttons = array();
?>

<div class="credit-cards">
    <div class="table-block">
        <table class="table">
            <thead>
                <tr>
                    <th class="first">Priority</th>
                    <th>Card type</th>
                    <th>Card number</th>
                    <th>Expires</th>
                    <th>Cardholder</th>
                    <th>Street address</th>
                    <th>Zip</th>
                    <th class="last">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = 1; ?>
                <?php $count = $customerPaymentProfiles->count(); ?>
                <?php foreach ($customerPaymentProfiles as $customerPaymentProfile): ?>
                    <tr class="<?php echo (($i%2) ? 'odd' : 'even'); ?>">
                        <td><?php echo $customerPaymentProfile->getPriority(); ?></td>
                        <td><?php echo $customerPaymentProfile->getCreditCardType(); ?></td>
                        <td><?php echo $customerPaymentProfile->getCardNumber(); ?></td>
                        <td><?php echo $customerPaymentProfile->getExpires(); ?></td>
                        <td><?php echo $customerPaymentProfile->getName(); ?></td>
                        <td><?php echo $customerPaymentProfile->getAddress(); ?></td>
                        <td><?php echo $customerPaymentProfile->getZip(); ?></td>
                        <td>
                            <?php
                                $a_options = array();

                                if ($i != 1) {
                                    $a_options[$buttonUp] = $list_buttons[$buttonUp];
                                    $a_options[$buttonUp]['href'] = url_for(
                                        'customer_payment_profile_priority_up', 
                                        array('id' => $customerPaymentProfile->getId())
                                    );
                                    $legend_buttons[$buttonUp] = & $list_buttons[$buttonUp];
                                } else {
                                    $a_options[$buttonUp] = $list_buttons[$buttonUp];
                                    $legend_buttons[$buttonUp] = & $list_buttons[$buttonUp];
                                }


                                if ($i != $count) {
                                    $a_options[$buttonDown] = $list_buttons[$buttonDown];
                                    $a_options[$buttonDown]['href'] = url_for(
                                        'customer_payment_profile_priority_down', 
                                        array('id' => $customerPaymentProfile->getId())
                                    );
                                    $legend_buttons[$buttonDown] = & $list_buttons[$buttonDown];
                                } else {
                                    $a_options[$buttonDown] = $list_buttons[$buttonDown];
                                    $legend_buttons[$buttonDown] = & $list_buttons[$buttonDown];
                                }


                                $a_options[$buttonEdit] = $list_buttons[$buttonEdit];
                                $a_options[$buttonEdit]['href'] = url_for('customer_payment_profile_edit', $customerPaymentProfile);
                                $legend_buttons[$buttonEdit] = & $list_buttons[$buttonEdit];


                                $a_options[$buttonDelete] = link_to_ajax_delete(
                                    '',
                                    url_for('customer_payment_profile_delete', $customerPaymentProfile),
                                    array(
                                        'title' => $buttonDelete,
                                        'class' => 'icon delete',
                                        'after_url' => url_for('customer_payment_profile')
                                    )
                                );
                                $legend_buttons[$buttonDelete] = & $list_buttons[$buttonDelete];

                                echo icon_links($a_options, array('class' => 'icon-links clearfix'), false);
                            ?>
                        </td>
                    </tr>
                    <?php ++$i; ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <?php
        include_partial(
             'global/legend',
              array(
                'legend_buttons'  => $legend_buttons,
              )
        ); 
    ?>

    <br/>

    <?php
        $a_options = array(
            'New' => array(
                'href'  => url_for('customer_payment_profile_new'),
                'class' => 'icon edit ajax-form-load'
            )
        );
        echo icon_links($a_options);
    ?>
</div>