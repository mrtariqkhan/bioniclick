
<?php $object = $form->getObject(); ?>

<h1>Edit credit card <?php echo $object->getCardNumber() ?></h1>

<div class="inner">

    <?php $urlAction = url_for('customer_payment_profile_update', $object); ?>
    <?php $urlAfter = url_for('customer_payment_profile'); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'back_url'          => $urlAfter,
                'submit_name'       => 'Update',
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel'),                
            )
        );
    ?>
</div>
