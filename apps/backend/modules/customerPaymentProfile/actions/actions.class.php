<?php

/**
 * paymentProfile actions.
 *
 * @package    bionic
 * @subpackage paymentProfile
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class customerPaymentProfileActions extends GeneralActions {

    /**
    * Executes index action
    *
    * @param sfRequest $request A request object
    */
    public function executeIndex(sfWebRequest $request) {

        $this->customerPaymentProfiles = array();
        if ($this->user->isCompanyUser()) {
            $this->customerPaymentProfiles = $this->user->getCompany()->getCustomerPaymentProfiles();
        } elseif($this->user->isAdvertiserUser()) {
            $this->customerPaymentProfiles = $this->user->getAdvertiser()->getCustomerPaymentProfiles();
        }
    }

    public function executeNew(sfWebRequest $request) {
        
        $this->form = new CustomerPaymentProfileForm();        
        if ($this->user->isCompanyUser()) {
            $this->actionUrl = $this->generateUrl(
                'customer_payment_profile_create_for_company',
                array('company_id' => $this->user->getCompanyId())
            );
        } else {
            $this->actionUrl = $this->generateUrl(
                'customer_payment_profile_create_for_advertiser',
                array('advertiser_id' => $this->user->getAdvertiserId())
            );
        }
    }

    public function executeCreate(sfWebRequest $request) {
        
        $customerPaymentProfile = new CustomerPaymentProfile();
        if ($advertiserId = $request->getParameter('advertiser_id')) {
            $this->checkIsAllowableObjectJson(AdvertiserTable::findById($advertiserId), 'You cannot edit information on this advertiser.', 240001);
            $customerPaymentProfile->setAdvertiserId($advertiserId);

        } elseif ($companyId = $request->getParameter('company_id')) {
            $this->checkIsAllowableObjectJson(CompanyTable::findById($companyId), 'You cannot edit information on this company.', 240002);
            $customerPaymentProfile->setCompanyId($companyId);

        } else {
            throw new jsonRpcException('one of advertiser_id or company_id is required parameter', 240003);
        }

        $form = new CustomerPaymentProfileForm($customerPaymentProfile);
        return $this->processAjaxForm($request, $form);
    }

    public function executeEdit(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);
        $this->form = new CustomerPaymentProfileForm($object);
    }

    public function executeUpdate(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);
        $form = new CustomerPaymentProfileForm($object);

        return $this->processAjaxForm($request, $form);
    }

    public function executeDelete(sfWebRequest $request) {
        return $this->processAjaxDelete($request);
    }

    public function executePriorityUp(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $object->upPriority();

        $this->setTemplate('index');
        $this->executeIndex($request);
    }

    public function executePriorityDown(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $object->downPriority();

        $this->setTemplate('index');
        $this->executeIndex($request);
    }
}