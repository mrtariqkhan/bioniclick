<?php

/**
 * campaign actions.
 *
 * @package    bionic
 * @subpackage campaign
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class campaignActions extends GeneralActions {

    public function preExecute() {

        $this->tableName = 'Campaign';
        parent::preExecute();

//        $this->getResponse()->addJavascript(
//            '/js/project/getSnippet.js',
//            'last'
//        );
        
        $this->callResultsSecurityHelper = new SecurityHelper($this, 'callResult');
        $this->call_result_permissions = $this->callResultsSecurityHelper->checkBasePermissions();
    }

    public function executeIndex(sfWebRequest $request) {
        //TODO:: think if it is in use

        $this->executeList($request);
        $this->setTemplate('index');

        $filterValues = $this->getFilter();
        $this->filter_form = $this->getFilterForm();
        $this->filter_form->setDefaults($filterValues);

        $this->free_numbers_count = AdvertiserIncomingNumberPoolTable::calcFreeNumbersCount(
            $this->user->getAdvertiserId()
        );
        $this->is_campaign_limit_exhausted = AdvertiserTable::isCampaignLimitExhausted(
            $this->user->getAdvertiserId()
        );
        $this->is_advertiser = $this->user->isAdvertiserUser();
        $this->is_debtor = $this->user->getIsDebtor();
    }

    public function executeList(sfWebRequest $request) {

        $filterValues = $this->getFilter();
        $filterForm = $this->getFilterForm();        
        $queryParams = $filterForm->getFilteredValues($filterValues, true);

        $specialQuery = CampaignTable::createQueryByUser($this->user, $queryParams);
        $this->pager = parent::initPager($request, $specialQuery);
        $list = $this->pager->getResults();

        $objectFieldsHeaders = array(
            'getName' => 'Name'
        );
        WasDeletedHelper::addHeader($this->user, $objectFieldsHeaders);

        $branchHelper = new BranchHelper();
        $this->branch_data = $branchHelper->makeRaws($this->user, $list, $objectFieldsHeaders);

        $this->getResponse()->addStylesheet(
            '/css/project/filter_block.css',
            'last'
        );

        $securityHelperCallLog = new SecurityHelper($this, 'callLog');
        $this->permissions_call_log = $securityHelperCallLog->checkBasePermissions();

        $securityHelperPages = new SecurityHelper($this, 'page');
        $this->permissions_page = $securityHelperPages->checkBasePermissions();

        $securityHelperAINP = new SecurityHelper($this, 'advertiserIncomingNumberPool');
        $this->permissions_ainp = $securityHelperAINP->checkBasePermissions();
    }

    public function executeIndexForAdvertiser(sfWebRequest $request) {

        $this->executeListForAdvertiser($request);
        $this->setTemplate('indexForAdvertiser');

        $isAdvertiserUser   = $this->user->isAdvertiserUser();
        $isCompanyUser      = $this->user->isCompanyUser();

        $this->is_advertiser_user   = $isAdvertiserUser;
        $this->is_company_user      = $isCompanyUser;

//        $filterValues = $this->getFilter();
//        $this->filter_form = $this->getFilterForm();
//        $this->filter_form->setDefaults($filterValues);
    }

    public function executeListForAdvertiser(sfWebRequest $request) {

        $advertiserId = $request->getParameter('advertiser_id');
        $advertiser = empty($advertiserId) ? null : AdvertiserTable::getInstance()->find($advertiserId);
        $this->forward404If(empty($advertiser), 'There is no such advertiser');

        $this->checkIsAllowableObject($advertiser, false);

//        $filterValues = $this->getFilter();
//        $filterForm = $this->getFilterForm();
//        $queryParams = $filterForm->getFilteredValues($filterValues, true);
        $queryParams = array();

        $specialQuery = CampaignTable::createQueryByAdvertiser($this->user, $advertiser, $queryParams);
        $this->pager = parent::initPager($request, $specialQuery);
        $list = $this->pager->getResults();

        $objectFieldsHeaders = array(
            'getName' => 'Name',
            'getKind' => 'Kind',
        );
        $canSeeExpiry = $this->securityHelper->checkPermissionCommon(SecurityHelper::PERMISSION_SPECIAL_SUFFIX_SEE_EXPIRY_FIELD);
        if ($canSeeExpiry) {
            $objectFieldsHeaders = array_merge(
                $objectFieldsHeaders,
                array(
                    'getDynamicNumberExpiry' => 'Dynamic number expiry (minutes)'
                )
            );
        }
        WasDeletedHelper::addHeader($this->user, $objectFieldsHeaders);

        $branchHelper = new BranchHelper();
        $this->branch_data = $branchHelper->makeRaws($this->user, $list, $objectFieldsHeaders);

        $this->getResponse()->addStylesheet(
            '/css/project/filter_block.css',
            'last'
        );

        $securityHelperCallLog = new SecurityHelper($this, 'callLog');
        $this->permissions_call_log = $securityHelperCallLog->checkBasePermissions();

        $securityHelperPages = new SecurityHelper($this, 'page');
        $this->permissions_page = $securityHelperPages->checkBasePermissions();

        $securityHelperAINP = new SecurityHelper($this, 'advertiserIncomingNumberPool');
        $this->permissions_ainp = $securityHelperAINP->checkBasePermissions();

        $this->advertiser = $advertiser;
    }

    public function executeNew(sfWebRequest $request) {

        $this->permissions = $this->configurePermissionsAll();
        $this->form = $this->configureForm();

        $this->forward404IfCannotCreateNewCampaign($this->user->getAdvertiser());

        $this->url_action = $this->generateUrl('campaign_create');
        $this->url_after = $this->generateUrl('campaigns');
    }

    public function executeNewForAdvertiser(sfWebRequest $request) {

        $advertiserId = $request->getParameter('advertiser_id', null);
        $advertiser = empty($advertiserId) ? null : AdvertiserTable::findById($advertiserId);

        $this->forward404IfCannotCreateNewCampaign($advertiser);

        $this->permissions = $this->configurePermissionsAll();
        $this->form = $this->configureForm(null, $advertiserId);

        $this->url_action = $this->generateUrl('campaign_create_for_advertiser', array('advertiser_id' => $advertiserId));
        $this->url_after = $this->generateUrl('advertiser_index');

        $this->setTemplate('new');
    }

    public function executeSubTreeNewForAdvertiser(sfWebRequest $request) {

        $this->executeNewForAdvertiser($request);

        $advertiserId = $request->getParameter('advertiser_id', null);
        $this->url_after = $this->generateUrl('campaigns_for_advertiser', array('advertiser_id' => $advertiserId));

        $advertiser = empty($advertiserId) ? null : AdvertiserTable::findById($advertiserId);
        $this->forward404IfCannotCreateNewCampaign($advertiser);

        $this->setTemplate('subTreeNewForAdvertiser');
    }

    public function executeSubTreeEditForAdvertiser(sfWebRequest $request) {

        $this->executeEdit($request);

        $object = $this->form->getObject();
        $advertiserId = $object->getAdvertiserId();
        $this->url_after = $this->generateUrl('campaigns_for_advertiser', array('advertiser_id' => $advertiserId));

        $this->setTemplate('subTreeEditForAdvertiser');
    }

    public function executeCreate(sfWebRequest $request) {
        
        $object = Campaign::generateCampaignWithApiKey();
        $form = $this->configureForm($object);
        
        $advertiser = $form->getObject()->getAdvertiser();
        $this->forward404IfCannotCreateNewCampaign($advertiser);

        return $this->processAjaxForm($request, $form);
    }

    public function executeCreateForAdvertiser(sfWebRequest $request) {

        $advertiserId = $request->getParameter('advertiser_id', null);
        $advertiser = empty($advertiserId) ? null : AdvertiserTable::findById($advertiserId);

        $this->forward404IfCannotCreateNewCampaign($advertiser);

        $object = Campaign::generateCampaignWithApiKey();
        $form = $this->configureForm($object, $advertiserId);

        return $this->processAjaxForm($request, $form);
    }

    public function executeEdit(sfWebRequest $request) {

        $this->permissions = $this->configurePermissionsAll();

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $this->form = $this->configureForm($object);
        
        $this->url_action = $this->generateUrl('campaign_update', array('id' => $object->getId()));
//        $this->url_after = $this->generateUrl('campaigns');
        $this->url_after = $this->generateUrl('campaign_show', array('id' => $object->getId()));
    }

    public function executeUpdate(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $form = $this->configureForm($object);

        return $this->processAjaxForm($request, $form);
    }

    public function executeShow(sfWebRequest $request) {

        $campaignId = $request->getParameter('id');

        $campaign = $this->getRoute()->getObject();
        $campaign->getDefaultTwilioIncomingPhoneNumberId();
        $this->checkIsAllowableObject($campaign, false);

        $specialQuery = PhysicalPhoneNumberTable::getInstance()
            ->getIndexQuery($campaignId)
        ;
        $this->pagerPhysical = parent::initPager($request, $specialQuery);

        $specialQuery = AdvertiserIncomingNumberPoolTable::createQueryByCampaignId(
            $campaignId
        );
        $this->pagerIncoming = parent::initPager($request, $specialQuery);

        $this->campaign = $campaign;

        $securityHelperCallLog = new SecurityHelper($this, 'callLog');
        $this->permissions_call_log = $securityHelperCallLog->checkBasePermissions();
        
        $securityHelperPhysicalPhoneNumber = new SecurityHelper($this, 'physicalPhoneNumber');
        $this->permissions_physical_phone_number = $securityHelperPhysicalPhoneNumber->checkBasePermissions();
    }

    public function executeDelete(sfWebRequest $request) {
        return $this->processAjaxDelete($request);
    }

    protected function deleteObject($object) {
        $object->deleteByUser($this->user);
    }

    public function executeFilter(sfWebRequest $request) {
        $filterForm = $this->getFilterForm();
        return $this->processAjaxFilter($request, $filterForm);
    }

    public function executeSnippet(sfWebRequest $request) {

        //TODO: rewrite this method
        $email      = $request->getPostParameter('email');
        $password   = $request->getPostParameter('password');
        $actionType = $request->getPostParameter('action_type');
        $actionName = $request->getPostParameter('action_name');

        $this->forward404Unless($request->isMethod('post'));

        $errorMsg = '';
        $output = '';

        try {
            $curl = new cURL();
            $google = new Google(
                array(
                    'trackingAction'=> $actionType,
                    'userName'      => $email,
                    'password'      => $password,
                    'actionName'    => $actionName,
                    'curl'          => $curl,
                    'referer'       => $email,
                )
            );

            $google->updateData();

            if ($google->getStatus() === Google::$STATUS_FAILURE) {
                $errorMsg = $google->getErrorMessageOne();
                throw new Exception($errorMsg);
            } else {
                $output = $google->getSuccess();
                $output = array_key_exists('snippetCode', $output) ? $output['snippetCode'] : '';
            }
        } catch (Exception $e) {
            Loggable::putExceptionLogMessage($e, ApplicationLogger::MAIN_LOG_TYPE, 'Google snippet error: ');
        }

        $result = array();
        $result['success'] = empty($errorMsg);
        $result['output']  = $output;
        $result['error']   = $errorMsg;

        return $this->processAjaxResult($request, $result, false);
    }

    protected function saveValidForm($validForm) {

        try {
            return $validForm->save();
        } catch (Exception $e) {
            Loggable::putExceptionFullLogMessage($e);
            throw $e;
        }
    }

    protected function configureForm($object = null, $advertiserId = null) {

        $canSeeExpiry = $this->securityHelper->checkPermissionCommon(SecurityHelper::PERMISSION_SPECIAL_SUFFIX_SEE_EXPIRY_FIELD);
        $formOptions = array(
            'user'              => $this->user,
            'can_see_expiry'    => $canSeeExpiry,
        );

        if (empty($object)) {
            $object = new Campaign();
        }
        if (!$object->getAdvertiserId()) {
            $advertiserId = empty($advertiserId) ? $this->user->getAdvertiserId() : $advertiserId;
            $object->setAdvertiserId($advertiserId);
        }
        $form = new CampaignForm($object, $formOptions);
        return $form;
    }

    protected function getFilterForm() {
        $options = $this->getBranchFilterOptions();
        return new CampaignFilterForm(null, $options);
    }

    public function executeAddNumbersNew(sfWebRequest $request) {

        $this->setTemplate('addNumbersNew');

        $campaignId = $request->getParameter('id', null);
        $this->form = $this->configureAddNumbersForm(null, $campaignId);

        $this->url_after = $this->generateUrl('campaigns');
    }

    public function executeAddShowNumbersNew(sfWebRequest $request) {

        $this->executeAddNumbersNew($request);

        $campaignId = $request->getParameter('id', null);
        $this->url_after = $this->generateUrl('campaign_show', array('id' => $campaignId));
    }

    public function executeAddNumbersNewForAdvertiser(sfWebRequest $request) {

        $this->executeAddNumbersNew($request);

        $campaign = $this->form->getObject();
        $advertiserId = $campaign->getAdvertiserId();

        $this->url_after = $this->generateUrl('campaigns_for_advertiser', array('advertiser_id' => $advertiserId));
    }

    public function executeAddNumbersCreate(sfWebRequest $request) {

        $campaignId = $request->getParameter('id', null);
        $form = $this->configureAddNumbersForm(null, $campaignId);

        return $this->processAjaxForm($request, $form);
    }

    protected function configureAddNumbersForm($object, $campaignId) {

        $formOptions = array(
            CampaignAdvertiserIncomingNumberPoolFormEdit::OPTION_USER => $this->user,
        );

        $campaign = CampaignTable::findById($campaignId);
        $object = $campaign;

        $form = new CampaignAdvertiserIncomingNumberPoolFormEdit($object, $formOptions);
        
        return $form;
    }

    protected function forward404IfCannotCreateNewCampaign(Advertiser $advertiser) {

        $this->checkIsAllowableObject($advertiser);

        $errMsg = AdvertiserTable::getWhyCannotCreateNewCampaign($advertiser->getId(), $advertiser->getName(), $advertiser->getWasDeleted());
        $this->forward404If(!empty($errMsg), $errMsg);
    }
    
    /*
     * Upload custom audio redirect message
     * use Plupload lib
     */
    public function executeAjaxUpdateRedirectMsg(sfWebRequest $request) {
        
        // Settings
        $pathBase       = sfConfig::get('sf_web_dir');
        $targetDir      = $pathBase . DIRECTORY_SEPARATOR . 'audio' . DIRECTORY_SEPARATOR . 'custom-redirect-msg' . DIRECTORY_SEPARATOR;
        $maxFileSize    = 3; //mb
//        $maxChankSize   = 1.5; //mb


        // Get parameters
        $chunk    = $request->getParameter('chunk',  0);
        $chunks   = $request->getParameter('chunks', 0);
        
        $fileExtension = substr(strrchr($request->getParameter('name'), '.'), 1 );

        // Valid file: extension mp3 or wav
        if (
                   $fileExtension != 'mp3'
                && $fileExtension != 'wav'
        ) {
            die('{"jsonrpc" : "2.0", "error" : {"code": 105, "message": "Invalid file: only mp3 and wav"}, "id" : "id"}');
        }
        
        $id = $this->getRoute()->getObject()->getId();
        $filePath = "{$targetDir}{$id}.{$fileExtension}";

        // Create target dir
        //TODO:: think if can use child of FileGeneratorBase. Look at FileGeneratorBase::generatePathToDir
        if (!file_exists($targetDir)) {
            mkdir($targetDir, FileGeneratorBase::MODE, true);
            chmod($targetDir, FileGeneratorBase::MODE);
        }
//        if (!file_exists($targetDir)) {
//            @mkdir($targetDir);
//        }
        
        /*
        // Valid file: max chanks
        if ($chunk > ceil($maxFileSize / $maxChankSize)) {
            // Remove upload file
            if (file_exists($filePath))
                unlink($filePath);
            
            die('{"jsonrpc" : "2.0", "error" : {"code": 105, "message": "Invalid file size"}, "id" : "id"}');
        }
        */

        // Remove old file
        if ($chunk == 0) {
            $mp3FilePath = "{$targetDir}{$id}.mp3";
			$wavFilePath = "{$targetDir}{$id}.wav";
            if (file_exists($mp3FilePath))
                unlink($mp3FilePath);
			if (file_exists($wavFilePath))
                unlink($wavFilePath);

        }
        
        // Look for the content type header
        if (isset($_SERVER["HTTP_CONTENT_TYPE"]))
            $contentType = $_SERVER["HTTP_CONTENT_TYPE"];

        if (isset($_SERVER["CONTENT_TYPE"]))
            $contentType = $_SERVER["CONTENT_TYPE"];

        // Handle non multipart uploads older WebKit versions didn't support multipart in HTML5
        if (strpos($contentType, "multipart") !== false) {
            if (isset($_FILES['file']['tmp_name']) && is_uploaded_file($_FILES['file']['tmp_name'])) {

                // Read binary input stream
                $in = fopen($_FILES['file']['tmp_name'], "rb");
                if ($in) {
                    /*
                    //Valid file: max chank size
                    if (filesize($_FILES['file']['tmp_name']) > $maxChankSize * 1024 * 1024 + 50) {
                        // Remove upload file
                        if (file_exists($filePath))
                            unlink($filePath);
                        
                        die('{"jsonrpc" : "2.0", "error" : {"code": 105, "message": "Invalid chunk file size"}, "id" : "id"}');
                    }
                    */
                    
                    $this->appendUploadFile($in, $filePath, $chunk);
                    
                    fclose($in);
                    @unlink($_FILES['file']['tmp_name']);
                } else
                    die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
            }
            
        } else {
            // Read binary input stream
            $in = fopen("php://input", "rb");
            if ($in) {
                $this->appendUploadFile($in, $filePath, $chunk);
                
                fclose($in);
            } else
                die('{"jsonrpc" : "2.0", "error" : {"code": 101, "message": "Failed to open input stream."}, "id" : "id"}');
        }
        
        // Valid file: max file size
        if (filesize($filePath) > $maxFileSize * 1024 * 1024) {
            // Remove upload file
            $mp3FilePath = "{$targetDir}{$id}.mp3";
			$wavFilePath = "{$targetDir}{$id}.wav";
            if (file_exists($mp3FilePath))
                unlink($mp3FilePath);
			if (file_exists($wavFilePath))
                unlink($wavFilePath);

            die('{"jsonrpc" : "2.0", "error" : {"code": 105, "message": "Invalid file size"}, "id" : "id"}');
        }
        
        // Return JSON-RPC response
        die('{"jsonrpc" : "2.0", "result" : null, "id" : "id"}');
    }
    
    /*
     * Open output file and append input to output file
     */
    protected function appendUploadFile($in, $filePath, $chunk) {
        
        // Open output file
        $out = fopen($filePath, $chunk == 0 ? "wb" : "ab");
        if ($out) {
            // append input to output file
            while ($buff = fread($in, 4096))
                fwrite($out, $buff);

            fclose($out);
        } else
            die('{"jsonrpc" : "2.0", "error" : {"code": 102, "message": "Failed to open output stream."}, "id" : "id"}');
    }
}