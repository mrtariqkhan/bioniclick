<?php use_helper('AudioPlayer'); ?>

<div class="form form-panel-min" id="redirectMessage" name="<?php
    echo url_for('campaign_update_redirect_msg', array('id' => $object->getId()))
?>">
    <h2>Redirect message settings</h2>
    <p>
        If you are recording calls, you must use a redirect message to follow FCC regulations. You can use our Default recording or upload 
        your own. All uploaded files must be mp3 or wav files and less than 3mb. 
        If you would like to turn Call Recording off, then choose that option below.
    </p>
    <label class="input">
        <input type="radio" name="typeMessage" value="1" <?php if ($object->getIsRecordingsEnabled() == 1) echo 'checked' ?>/>Default message
    </label>
    <div class="form-sub-panel">
        <?php echo audio_player('/audio/redirect_message.mp3', array(
            'autoShow' => true
        )); ?>
    </div>
    
    <label>
        <input type="radio" name="typeMessage" value="2" <?php if ($object->getIsRecordingsEnabled() == 2) echo 'checked' ?>/>Custom message
    </label>

    <div class="form-sub-panel" id="upload-container">
        <?php
            //$extension = '';
            $path = '';
            
            if (is_readable('audio/custom-redirect-msg/' . $object->getId() . '.mp3')) {
                $path = '/audio/custom-redirect-msg/' . $object->getId() . '.mp3';
            } elseif (is_readable('audio/custom-redirect-msg/' . $object->getId() . '.wav')) {
                $path = '/audio/custom-redirect-msg/' . $object->getId() . '.wav';
            }
        ?>
        <a href="#" id="upload-selectfiles">Upload new file</a>
        <span id="upload-info">No runtime found.</span>
        
        <a href="#" id="upload-cancel" style="display: none;">Cancel file</a>
        <a href="<?php echo $path; ?>" id="download-custom" style="display: none;">Download</a>
        <!-- <span id="player-custom" style="display: none;"></span> -->
        
        <div id="error-custom" style="color: red; margin: 5px 0 0; display: none;"></div>
    </div>
    
    <div id="custom-message-player" class="form-sub-panel" style="display: none;">
        <?php echo audio_player($path, array(
            'autoShow' => true
        )); ?>
    </div>

    <div>
		<label>
	        <input type="radio" name="typeMessage" value="3" <?php if ($object->getIsRecordingsEnabled() == 3) echo 'checked' ?>/>No Recording message
	    </label>    	
    </div>
    
    <label>
        <input type="radio" name="typeMessage" value="0" <?php if ($object->getIsRecordingsEnabled() == 0) echo 'checked' ?>/>Recordings disabled
    </label>
        
</div>
