<?php use_helper('Link'); ?>
<?php use_helper('Pager'); ?>

<?php if ($permissions['can_read']): ?>

    <?php $reloadTreeKey = TreeNodeCampaign::generateTreeNodeKeyCampaigns($advertiser->getRawValue()); ?>

    <?php $headers = $branch_data['headers']; ?>
    <?php $raws    = $branch_data['raws']; ?>
    
    <?php 
        $pagerHtmlString = pager(
            $pager->getRawValue(),
            sfConfig::get('app_pager_max_per_pages'),
            url_for('campaign_list'),
            null,
            'ajax-pager'
        );
    ?>
    
    <p>
        <?php 
            include_partial(
                'global/pager_list_header',
                array(
                    'pager' => $pager,
                    'pagerHtmlString' => $pagerHtmlString
                )
            ); 
        ?>
    </p>
    <div class="table-block">
        <table class="table">
            <thead>
                <tr>
                    <th class="first">&nbsp;</th>
                    <?php
                        $list_buttons = array(
                            'Assign numbers' => array(
                                'class' => 'icon assign-numbers ajax-load'
                                // edit_campaign main icon assign-numbers ajax-load
                            ),
                            'Pages' => array(
                                'class' => 'icon pages ajax-load'
                                // icon pages ajax-load
                            ),
                            'Log' => array(
                                'class' => 'icon log'
                            ),
                            'Edit' => array(
                                'class' => 'icon edit ajax-load'
                                // edit_campaign main icon edit ajax-load
                            ),
                            'Delete' => array(
                                'class' => 'icon delete'
                            )
                        );
                        
                        $legend_buttons = array();
                        
                        foreach ($headers as $headerTitle):
                    ?>
                        <th><?php echo $headerTitle; ?></th>
                    <?php endforeach; ?>
                    <th class="last">&nbsp;</th>
                </tr>
            </thead>
            <tbody>
                <?php $i = $pager->getFirstIndice(); ?>
                <?php foreach ($raws as $id => $raw): ?>
                    <tr class="<?php echo ($i%2) ? 'odd' : 'even'; ?>">

                        <td><?php echo $i; ?></td>

                        <?php $raw = $raw->getRawValue(); ?>
                        <?php foreach ($raw as $key => $cell): ?>
                            <?php $value = (WasDeletedHelper::KEY == $key) ? WasDeletedHelper::getAsString($cell) : $cell; ?>
                            <td><?php echo $value; ?></td>
                        <?php endforeach; ?>

                        <td class="last">
                            <?php
                                $a_options = array();

                                $ifShowUpdateLinks = WasDeletedHelper::ifCanUpdateAsArray($raw);
                                if ($ifShowUpdateLinks) {

                                    if (
                                           $permissions_ainp['can_update'] &&
                                           (AdvertiserIncomingNumberPoolTable::calcFreeAdvertiserNumbers($id) > 0)
                                           //TODO:: Add checking direct ownership between user and advertiser
                                    ) {
                                        $a_options['Assign numbers'] = $list_buttons['Assign numbers'];
                                        $a_options['Assign numbers']['href'] = url_for('campaign_add_numbers_new', array('id' => $id));

                                        $legend_buttons['Assign numbers'] = & $list_buttons['Assign numbers'];
                                    }
                                }

                                if ($permissions_page['can_read']) {
                                    $a_options['Pages'] = $list_buttons['Pages'];
                                    $a_options['Pages']['href'] = url_for('pages', array('campaign_id' => $id));

                                    $legend_buttons['Pages'] = & $list_buttons['Pages'];
                                }

                                if ($permissions_call_log['can_read']) {
                                    $a_options['Log'] = $list_buttons['Log'];
                                    $a_options['Log']['href'] = url_for(
                                        'tabs_logs',
                                        array('sub_tab_code' => 'call_log', 'campaign_id' => $id)
                                    );

                                    $legend_buttons['Log'] = & $list_buttons['Log'];
                                }

                                if ($ifShowUpdateLinks) {
                                    if ($permissions['can_update']) {
                                        $a_options['Edit'] = $list_buttons['Edit'];
                                        $a_options['Edit']['href'] = url_for('campaign_edit', array('id' => $id));

                                        $legend_buttons['Edit'] = & $list_buttons['Edit'];
                                    }

                                    if ($permissions['can_delete']) {
                                        $a_options['Delete'] = link_to_ajax_delete(
                                            '',
                                            url_for('campaign_delete', array('id' => $id)),
                                            array(
                                                'title'             => 'Delete',
                                                'class'             => 'icon delete',
                                                'special_form_data' => $reloadTreeKey
                                            )
                                        );

                                        $legend_buttons['Delete'] = & $list_buttons['Delete'];
                                    }
                                }

                                echo icon_links($a_options, array('class' => 'icon-links clearfix'), false);
                            ?>
                        </td>
                    </tr>
                    <?php ++$i; ?>
                <?php endforeach;?>
            </tbody>
        </table>
    </div>

<?php endif; ?>

<?php
    include_partial(
         'global/legend',
          array(
            'legend_buttons'  => $legend_buttons,
          )
    ); 
?>

<?php echo $pagerHtmlString; ?>