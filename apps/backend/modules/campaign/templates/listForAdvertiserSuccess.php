<?php
    include_partial(
        'listForAdvertiser',
        array(
            'pager'                     => $pager,
            'branch_data'               => $branch_data,
            'permissions'               => $permissions,
            'permissions_call_log'      => $permissions_call_log,
            'permissions_page'          => $permissions_page,
            'permissions_ainp'          => $permissions_ainp,
            'call_result_permissions'   => $call_result_permissions,
            'advertiser'                => $advertiser,
        )
    );
?>