<?php use_helper('Link'); ?>
<div class="account-wrap clearfix">
<!--
    <div class="ajax-filter-container">
        <div class="filter-form-wrap">
            <h1>Search</h1>
            <div class="filter-form-wraper">
                <?php
//                    if ($permissions['can_filter']) {
//                        include_partial(
//                            'global/form_ajax',
//                            array(
//                                'form'              => $filter_form,
//                                'action_url'        => url_for('campaign_filter'),
//                                'back_url'          => '',
//                                'submit_name'       => 'Go!',
//                                'form_class_name'   => 'ajax-filter',
//                                'cancel_attributes' => array('class' => 'ajax-form-cancel')
//                            )
//                        );
//                    }
                ?>
            </div>
        </div>
-->
        <div class="ajax-pagable">
            <?php
                include_partial(
                    'listForAdvertiser',
                    array(
                        'pager'                     => $pager,
                        'branch_data'               => $branch_data,
                        'permissions'               => $permissions,
                        'permissions_call_log'      => $permissions_call_log,
                        'permissions_page'          => $permissions_page,
                        'permissions_ainp'          => $permissions_ainp,
                        'call_result_permissions'   => $call_result_permissions,
                        'advertiser'                => $advertiser,
                    )
                );
            ?>
        </div>
<!--
    </div>
-->
</div>

<?php $id = $advertiser->getId(); ?>

<?php $a_options = array(); ?>

<?php if ($permissions['can_create']): ?>
    <?php $errMsg = AdvertiserTable::getWhyCannotCreateNewCampaign($id, $advertiser->getName(), $advertiser->getWasDeleted()); ?>
    <?php if (empty($errMsg)): ?>
        <?php
            $a_options['New campaign'] = array(
                'href'  => url_for('sub_tree_campaign_new_for_advertiser', array('advertiser_id' => $id)),
                'class' => 'icon new-campaign ajax-load',
            );
        ?>
    <?php else: ?>
        <?php 
            $a_options['New campaign'] = array(
                'class' => 'icon new-campaign',
                'title' => $errMsg,
            );
        ?>
    <?php endif; ?>
<?php endif; ?>


<?php
    if ($permissions['can_read']) {
        if ($is_advertiser_user) {
            $a_options['Back'] = array(
                'href'  => url_for('advertiser_show', array('id' => $id)),
                'class' => 'icon back ajax-load',
            );
        }
        if ($is_company_user) {
            $parentCompany = $advertiser->getCompany();
            $levelParentCompany = $parentCompany->getLevel();
            $a_options['Back'] = array(
                'href'  => url_for('sub_tree_advertiser_index', array('parent_company_id' => $parentCompany->getId())),
                'class' => 'icon back ajax-load',
            );
        }
    }
?>

<?php echo icon_links($a_options); ?>
