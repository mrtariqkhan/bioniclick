<?php $object = $form->getObject(); ?>

<span id="advertiser_id_handler"advertiser_id="<?php echo $object->getAdvertiserId(); ?>"></span>

<h1>Assign numbers to campaign "<?php echo $object; ?>"</h1>

<div class="inner">
    <?php $id = $object->getId(); ?>

    <?php $urlAction = url_for('campaign_add_numbers_create', array('id' => $id)); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'back_url'          => $url_after,
                'submit_name'       => 'Assign',
                'form_class_name'   => 'ajax-form form-with-snippet',
                'cancel_attributes' => array('class' => 'ajax-form-cancel'),
            )
        );
    ?>
</div>