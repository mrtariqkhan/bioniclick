<?php use_helper('AudioPlayer'); ?>

<div class="form form-panel-min" id="redirectMessage" name="<?php
    echo url_for('campaign_update_redirect_msg', array('id' => $object->getId()))
?>">
    <h2>Redirect message settings</h2>
    
    <label class="input">
        <input type="radio" name="typeMessage" value="1" <?php if ($object->getIsRecordingsEnabled() == 1) echo 'checked' ?>/>Default message
    </label>
    <div class="form-sub-panel">
        <?php echo audio_player('/audio/redirect_message.mp3', array(
            'autoShow' => true
        )); ?>
    </div>
    
    <label>
        <input type="radio" name="typeMessage" value="0" <?php if ($object->getIsRecordingsEnabled() == 0) echo 'checked' ?>/>Recordings disabled
    </label>
        
</div>