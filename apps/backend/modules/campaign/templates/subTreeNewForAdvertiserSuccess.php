
<?php $object = $form->getObject(); ?>

<?php $reloadTreeKey = TreeNodeCampaign::generateTreeNodeKeyCampaigns($object->getAdvertiser()); ?>

<div class="inner form-panel-min">
    <h1>New Campaign</h1>
    
    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $url_action,
                'back_url'          => $url_after,
                'submit_name'       => 'Create',
                'form_class_name'   => 'ajax-form form-with-snippet',
                'cancel_attributes' => array('class' => 'ajax-form-cancel'),
                'form_special_data' => $reloadTreeKey,
            )
        );
    ?>
</div>

<?php include_partial('redirectMessage', array('object' => $object)); ?>