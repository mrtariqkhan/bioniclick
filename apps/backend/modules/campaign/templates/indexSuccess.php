<?php use_helper('Link'); ?>
<div class="account-wrap clearfix">   
    <div class="ajax-filter-container">
        <div class="filter-form-wrap">
            <h1>Search</h1>
            <div class="filter-form-wraper">
                <?php
                    if ($permissions['can_filter']) {
                        include_partial(
                            'global/form_ajax',
                            array(
                                'form'              => $filter_form,
                                'action_url'        => url_for('campaign_filter'),
                                'back_url'          => '',
                                'submit_name'       => 'Go!',
                                'form_class_name'   => 'ajax-filter',
                                'cancel_attributes' => array('class' => 'ajax-form-cancel')
                            )
                        );
                    }
                ?>
            </div>
        </div>
     
        <div class="ajax-pagable">
            <?php
                include_partial(
                    'list',
                    array(
                        'pager'                 => $pager,
                        'branch_data'           => $branch_data,
                        'permissions'           => $permissions,
                        'permissions_call_log'  => $permissions_call_log,
                        'permissions_page'      => $permissions_page,
                        'permissions_ainp'      => $permissions_ainp,
                        'call_result_permissions'   => $call_result_permissions
                    )
                );
            ?>
        </div>
    </div>
</div>

<?php if ($is_advertiser): ?>
    <?php if ($permissions['can_create']): ?>
        <p><?php echo $free_numbers_count; ?> number(s) available at pool</p>
        <?php if ($is_debtor): ?>
            <p>
                Cannot create new campaigns because this company is Debtor.
            </p>
        <?php elseif ($is_campaign_limit_exhausted) : ?>
            <p>
                You can't create new campaigns because of limit exhausting.
            </p>
        <?php elseif ($free_numbers_count <= 0) : ?>
            <p>
                Sorry, Bionic number pool is exhausting. You will have ability to create new campaign later.
            </p>
        <?php else: ?>
            <?php
                $a_options = array();
                if ($permissions['can_update']) {
                    $a_options['New'] = array(
                        'href' => url_for('campaign_new'),
                        'class' => 'icon new ajax-load'
                    );
                }

                echo icon_links($a_options);
            ?>
        <?php endif; ?>
    <?php endif; ?>
<?php endif; ?>
