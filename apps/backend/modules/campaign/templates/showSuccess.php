<?php use_helper('Link'); ?>

<?php $id = $campaign->getId(); ?>
<?php $ifShowUpdateLinks = WasDeletedHelper::ifCanUpdate($campaign->getRawValue()); ?>

<?php
    include_partial(
        'global/breadCrumb',
        array(
            'object'        => $campaign,
            'objectIsLink'  => false,
        )
    );
?>

<?php
    $a_options = array();

    if ($ifShowUpdateLinks) {
        if ($permissions['can_update']) {
            $a_options['Edit campaign'] = array(
                'href' => url_for('campaign_edit', array('id' => $id)),
                'class' => 'main icon edit ajax-load'
            );
        }
    }

    if ($permissions_call_log['can_read']) {
        $a_options['Campaign Log'] = array(
            'href' => url_for(
                'tabs_logs',
                array('sub_tab_code' => 'call_log', 'campaign_id' => $id)
            ),
            'class' => 'icon log'
        );
    }
    if ($ifShowUpdateLinks) {
        if ($permissions_physical_phone_number['can_create']) {
            $a_options['New Business Number'] = array(
                'href' => url_for('physical_phone_number_new', array('campaign_id' => $id)),
                'class' => "ajax-load icon new"
            );
        }
    }

    echo icon_links($a_options, array('class' => 'icon-links campaign clearfix'));
?>
<br/>
<div>
    <h2>Call Forwarding Business Numbers List</h2>
    <div class="ajax-pagable">
        <?php
            include_component(
                'physicalPhoneNumber',
                'list',
                array(
                    'campaign'  => $campaign,
                    'pager'     => $pagerPhysical,
                )
            );
        ?>
    </div>
</div>
<h2>Dynamic Phone Number List</h2>
<div class="ajax-pagable">
    <?php
        include_component(
            'advertiserIncomingNumberPool',
            'listForCampaign',
            array(
                'pager'         => $pagerIncoming,
                'campaign'      => $campaign,
            )
        );
    ?>
</div>



<?php
    $a_options = array();

    if ($permissions['can_read']) {
        $a_options['Back'] = array(
            'href' => url_for('campaigns_for_advertiser', array('advertiser_id' => $campaign->getAdvertiserId())),
            'class' => "icon back ajax-load"
        );
    }

    echo icon_links($a_options);
?>