<?php

/**
 * incomingNumbersQueue actions.
 *
 * @package    bionic
 * @subpackage incomingNumbersQueueTask
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class incomingNumbersQueueTaskActions extends GeneralActions {

    const ATTRIBUTE_NAME_FORM_DEFAULT_URL = 'fulfill_order_url';

    public function executeNew(sfWebRequest $request) {

        $id = $request->getParameter('id', null);
        $object = IncomingNumbersQueueTable::getInstance()->find($id);
        $this->forward404Unless(!empty($object), 'Cannot find order.');

        $this->url_action = empty($id) 
            ? $this->generateUrl('buy_numbers_task_create') 
            : $this->generateUrl('buy_numbers_task_create_specific', array('id' => $id))
        ;

        $this->form = $this->configureForm($object);

        $routePostfix = $request->getParameter('routePostfix', '');
        switch ($routePostfix) {
            case 'User':
                $this->url_after = $this->generateUrl('incomingNumbersQueue_indexUser');
                break;
            case 'Advertiser':
                $advertiserId = $object->getAdvertiserId();
                $this->url_after = $this->generateUrl('incomingNumbersQueue_indexAdvertiser', array('advertiser_id' => $advertiserId));
                break;
            case 'Company':
                $companyId = $request->getParameter('company_id');
                $company = CompanyTable::getInstance()->find($companyId);
                $this->forward404Unless(!empty($company), 'Cannot find Company.');

                $this->url_after = $this->generateUrl('incomingNumbersQueue_indexCompany', array('company_id' => $companyId));
                break;
            default:
                $this->url_after = $this->generateUrl('incomingNumbersQueue_index');
        }

        $orderId = $object->getId();
        $specialQuery = AdvertiserIncomingNumberPoolLogTable::createIndexQueryByOrderId(
            $orderId
        );
        $this->pagerDetails = parent::initPager($request, $specialQuery);

        $this->order = $object;
        $this->timezoneName = $this->user->findTimezoneName();
    }

    public function executeCreate(sfWebRequest $request) {

        $id = $request->getParameter('id', null);
        $order = IncomingNumbersQueueTable::getInstance()->find($id);
        $this->forward404Unless(!empty($order), 'Cannot find order.');

        $form = $this->configureForm($order);

        return $this->processAjaxForm($request, $form);
    }

    protected function configureForm($order) {

        $url = $this->getUser()->getAttribute(self::ATTRIBUTE_NAME_FORM_DEFAULT_URL, '');

        $object = new TwilioIncomingPhoneNumber();
        $object->setUrl($url);

        $formOptions = array(
            IncomingNumberQueueTaskForm::OPTION_USER  => $this->user,
            IncomingNumberQueueTaskForm::OPTION_ORDER => $order,
        );

        $form = new IncomingNumberQueueTaskForm($object, $formOptions);

        return $form;
    }

    protected function saveValidForm($validForm) {

        $this->getUser()->setAttribute(self::ATTRIBUTE_NAME_FORM_DEFAULT_URL, $validForm->getValue('url', ''));

        try {
            $isResultSuccess = $validForm->save();

            if (is_null($isResultSuccess)) {
                $msg = "There was no order to fulfill.";
            } else {
                $msg = "Order has been successfully processed.";
            }
            $this->getUser()->setFlash('tab-message', $msg);

        } catch (Exception $e) {
            $this->getUser()->setFlash(
                'tab-warning',
                "Order's fulfilling has been failed."
            );
        }
    }
}
