
<h1>Fulfill order</h1>

<?php
    include_partial(
        'incomingNumbersQueue/orderDescription',
        array(
            'order'         => $order,
            'timezoneName'  => $timezoneName,
        )
    );
?>

<div class="fulfill-order-details">
    <h2>
        Details
        <span class="fulfill-order-details-buttons">
            <span class="icon fulfill-order-details-toggle fulfill-order-details-show" title="Show details">&nbsp;</span>
        </span>
    </h2>

    <div class="fulfill-order-details-stowable">
        <div class="ajax-pagable">
            <?php
                include_component(
                    'advertiserIncomingNumberPoolLog',
                    'listForOrder',
                    array(
                        'pager'             => $pagerDetails,
                        'order'             => $order,
                        'timezoneName'      => $timezoneName,
                        'historyIsNeed'     => false,
                    )
                );
            ?>
        </div>
    </div>
</div>

<div class="inner">
    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $url_action,
                'back_url'          => $url_after,
                'submit_name'       => 'Fulfill',
                'form_class_name'   => 'ajax-form fulfill-order-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel'),
            )
        );
    ?>
</div>
