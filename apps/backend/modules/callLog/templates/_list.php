<?php use_helper('Link'); ?>
<?php use_helper('Pager'); ?>
<?php use_helper('AudioPlayer')?>
<?php

$paramsCommon = empty($campaign) ? array() : array('campaign_id' => $campaign->getId());

$call_result_permissions = $call_result_permissions->getRawValue();

$logTitles = $indexHelper->getTitles()->getRawValue();

$routeName = empty($campaign) ? 'call_log_list' : 'call_log_list_campaign';

$pagerHtmlString = pager(
    $pager->getRawValue(),
    sfConfig::get('app_logs_pager_max_per_pages'),
    url_for($routeName, $paramsCommon),
    null,
    'ajax-pager'
);


$date_info = $date_info->getRawValue();
?>

<?php
    if(!empty($date_info))
        include_partial(
            'global/timeRange', 
            array(
                'date_info' => $date_info,
                'class'     => '',
            )
        );
?>

<p>
    <?php
    include_partial(
        'global/pager_list_header',
        array(
            'pager' => $pager,
            'pagerHtmlString' => $pagerHtmlString
        )
    );
    if($is_user_superadmin == 1 ){
      echo '</br>Show call logs which is only assigned to campaigns or advertisers (not belonged to bionic)'; 
	}
    ?>
</p>

<?php if($is_user_superadmin == 1 ){ ?>
<div>
    Available seconds between visitor analytics and call:
    <span>
        <?php  echo $available_seconds_between_analytics_and_call; ?>
    </span>
</div>
<?php }?>

<?php
    $listButtonTitleEdit = 'Edit additional call info';
    $listButtonTitleDel = 'Delete additional call info';
    $listButtonTitleDelCall = 'Delete call forever';

    $list_buttons = array(
        $listButtonTitleEdit => array(
            'class' => 'icon edit ajax-form-load'
        ),
        $listButtonTitleDel => array(
            'class' => 'icon delete'
        ),
    );
    if ($can_delete) {
        $list_buttons = array_merge(
            $list_buttons,
            array(
                $listButtonTitleDelCall => array(
                    'class' => 'icon delete-call'
                )
            )
        );
    }

    $legend_buttons = & $list_buttons;
?>

<?php
    $vaClasses = array(
        VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE        => 'icon-small call-with-va',
        VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD=> 'icon-small call-with-old-va',
        VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE        => 'icon-small call-without-va',
        VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT    => 'icon-small call-to-default',
    );
?>

<div id='callLogsContainer' class="log-data">
    <table id='callslogTable' class="log-table call-log-table table flexme" >
        <script type="text/javascript">
            columnsModel = Array();
            var columnModel1 = Array();
            columnModel1['display'] = "Call Record";
            columnModel1['name'] = "call_record";
            columnModel1['width'] = 50;
            columnModel1['sortable'] = 'false';
            columnModel1['align'] = 'center';

            var columnModel2 = Array();
            columnModel2['display'] = "Call Info";
            columnModel2['name'] = "call_info";
            columnModel2['width'] = 75;
            columnModel2['sortable'] = 'false';
            columnModel2['align'] = 'center';

            columnsModel.push(columnModel1);
            columnsModel.push(columnModel2);
            <?php foreach ($logTitles as $fieldKey => $fieldTitle): ?>
                var columnModel = Array();
                columnModel['display'] = "<?php echo $fieldTitle; ?>";
                columnModel['name'] = "<?php echo $fieldKey; ?>";
                if("<?php echo $fieldKey; ?>" == "recording"
                    || "<?php echo $fieldKey; ?>" == "url")
                    columnModel['width'] = 200;
                else  if("<?php echo $fieldKey; ?>" == "startTime"
                    || "<?php echo $fieldKey; ?>" == "endTime")
                    columnModel['width'] = 175;
                else if("<?php echo $fieldKey; ?>" == "duration")
                    columnModel['width'] = 50;
                else
                    columnModel['width'] = 100;
                columnModel['sortable'] = 'true';
                columnModel['align'] = 'center';
                columnsModel.push(columnModel);
            <?php endforeach; ?>        
        </script>
        <tbody>
            <?php
            
            $i = $pager->getFirstIndice();
            $list = $pager->getResults()->getRawValue();
            
            foreach ($list as $rawArray):
            ?>
                <?php
                    //TODO:: rewrite it
                    $vaInfoElementStart = '';
                    $vaInfoElementEnd   = '';
                    if ($do_show_icons) {
                        $vaStatus = $rawArray['va_status_description'];
                        $vaStatusCode = $rawArray['va_status'];
                        $vaClass = array_key_exists($vaStatusCode, $vaClasses) ? $vaClasses[$vaStatusCode] : '';

                        $vaInfoElementStart = "<span class='$vaClass' title='$vaStatus'><span class='icon-small-holder'></span>";
                        $vaInfoElementEnd   = "</span>";
                    }
                ?>
                <tr class="<?php echo ($i % 2) ? 'odd' : 'even'; ?>">
                    <td>
                        <?php echo $vaInfoElementStart;?>
                        <?php echo $i; ?>
                        <?php echo $vaInfoElementEnd;?>
                    </td>
                    <td class="links"><?php
                        
//                        echo var_dump($rawArray);
//                        echo $rawArray['id'];
//                        echo ' ' . $rawArray['vaId'];
                        
                        $a_options = array();

                        if ($call_result_permissions['can_update']):
                            $params_edit = array_merge(
                                array('twilio_incoming_call_id' => $rawArray['id'],
                                    'record_campaign_name' => $rawArray['campaign']),
                                $paramsCommon
                            );
                            $route_name_edit = empty($campaign) ? 'call_result_edit' : 'call_result_campaign_edit';
                            $url_edit = url_for($route_name_edit, $params_edit);
                            $a_options[$listButtonTitleEdit] = array(
                                'href'  => $url_edit,
                                'class' => 'icon edit ajax-form-load'
                            );
                            $a_options[$listButtonTitleEdit]['class'] = $list_buttons[$listButtonTitleEdit]['class'];
                            $a_options[$listButtonTitleEdit]['href']  = $url_edit;
                        endif;

                        if (!empty($rawArray['call_result_id']) && $call_result_permissions['can_delete']):
                            $params_delete = array_merge(
                                array('id' => $rawArray['call_result_id']),
                                $paramsCommon
                            );
                            $url_delete = url_for('call_result_delete', $params_delete);

                            $a_options[$listButtonTitleDel] = link_to_ajax_delete(
                                '',
                                $url_delete,
                                array(
                                    'title'     => $listButtonTitleDel,
                                    'class'     => 'icon delete',
                                    'after_url' => url_for('call_logs', $paramsCommon),
                                )
                            );
                        endif;

                        if ($can_delete):
                            $params_delete_call = array_merge(
                                array('id' => $rawArray['id']),
                                $paramsCommon
                            );
                            $url_delete_call = url_for('call_log_delete', $params_delete_call);

                            $a_options[$listButtonTitleDelCall] = link_to_ajax_delete(
                                '',
                                $url_delete_call,
                                array(
                                    'title'     => $listButtonTitleDelCall,
                                    'class'     => 'icon delete-call',
                                    'after_url' => url_for('call_logs', $paramsCommon),
                                )
                            );
                        endif;
                        
                        echo icon_links($a_options, array('class' => 'icon-links'), false);
                        
                        ?>
                    </td>

                    <?php foreach ($logTitles as $fieldKey => $fieldTitle): ?>
                        <td column_code="<?php echo $fieldKey; ?>">
                            <?php if (isset($rawArray[$fieldKey . '.is_va_column'])): ?>
                                <?php echo $vaInfoElementStart; ?>
                            <?php endif; ?>

                            <?php if ('callerNumber' == $fieldKey): ?>
                                <?php

                                    $blockAction = $rawArray['callerNumber.blocked'] ? 'unblock' : 'block';

                                    $blockUrl = url_for(
                                        'tabs_logs',
                                        array(
                                            'sub_tab_code' => 'callers',
                                            'sub_tab_type' => $blockAction,
                                            'id' => $rawArray['caller_id']
                                        )
                                    );

                                    $blockClass = $rawArray['callerNumber.blocked'] ? 'blocked-phone' : '';
                                ?>
                                <a
                                    class="<?php echo $blockClass; ?>"
                                    href="<?php echo $blockUrl; ?>"
                                    title="<?php echo ucfirst($blockAction); ?>"
                                >
                                    <?php echo $rawArray[$fieldKey]; ?>
                                </a>
                            <?php elseif ('url' == $fieldKey && $rawArray[$fieldKey] != 'Undefined'): ?>
                                <?php 
                                    $value = $rawArray[$fieldKey];
                                    $isCampaignName = strpos($value,'http:');
                                ?>
                                    <?php if ($isCampaignName === false): ?>
                                        <?php echo $rawArray[$fieldKey]; ?>                                            
                                    <?php else: ?>
                                    <a href="<?php echo $rawArray[$fieldKey]; ?>"
                                           title="Go to URL"
                                           target="_blank"
                                        >
                                            <?php echo $rawArray[$fieldKey]; ?>
                                        </a>
                                    <?php endif; ?>
                            <?php elseif ('recording' == $fieldKey): ?>
                                <?php if (!empty($rawArray['recording_id'])): ?>
                                    <a href="<?php
                                        echo url_for(
                                            'call_log_download_recording',
                                            array('id' => $rawArray['recording_id'])
                                        );
                                        ?>"
                                    >
                                        Download
                                    </a>
                                    <?php echo audio_player($rawArray[$fieldKey] . '.mp3'); ?>
                                <?php else: ?>
                                    <?php echo TwilioIncomingCallIndexHelper::RECORDING_UNDEFINED; ?>
                                <?php endif; ?>
                            <?php elseif (isset($rawArray[$fieldKey . '.short'])): ?>
                                <span
                                    class="changeable-url"
                                    data-url-short="<?php echo $rawArray[$fieldKey . '.short'] ?>"
                                    data-url-long="<?php echo $rawArray[$fieldKey] ?>"
                                >
                                    <?php echo $rawArray[$fieldKey] ?>
                                </span>
                            <?php
                            else:
                                echo $rawArray[$fieldKey];
                            endif;
                            ?>
                            <?php if (isset($rawArray[$fieldKey . '.is_va_column'])): ?>
                                <?php echo $vaInfoElementEnd; ?>
                            <?php endif; ?>
                        </td>
                    <?php endforeach; ?>
                </tr>
                <?php $i++; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php
    include_partial(
         'global/legend',
          array(
            'legend_buttons'  => $legend_buttons,
          )
    ); 
?>

<?php echo $pagerHtmlString; ?>
        <script>
            function adjustLogTable(){
                //apply table jquery plugin
                $('#callslogTable').flexigrid({
                    colModel : columnsModel,
                    // sortorder: "asc",
                    // title: 'Call logs',
                    showTableToggleBtn: true,
                    minColToggle: 10,
                    minwidth: 100,
                    // usepager: true,
                    // rpOptions: [10, 20, 30, 40, 50], //allowed per-page values
                });  
                var headerElementsSelector = '#callLogsContainer.log-data div.hDivBox table th';
                var checkboxesElementsSelector = '#callLogsContainer.log-data input.togCol';
                var tableElementsSelector = '#callLogsContainer.log-data div.bDiv td';
                var filterElementBoxSelector = '#callLogsContainer.log-data div.cDrag div';
                confgureFlexigrid(headerElementsSelector, checkboxesElementsSelector, 
                    tableElementsSelector, filterElementBoxSelector);
            }
            setTimeout('adjustLogTable()', 100);
            
        </script>
