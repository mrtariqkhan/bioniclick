<?php if ($permissions['can_read']): ?>

    <?php if (!empty($campaign)): ?>
        <h3>Logs for campaign: <?php echo $campaign->getName(); ?></h3>
    <?php endif; ?>

    <div class="inner">
        <?php $logTitles = $indexHelper->getTitles(); ?>
        <!--<?php include_partial('fieldsSetSelect', array('logTitles' => $logTitles)); ?>-->


        <div class="account-wrap clearfix">
            <div class="ajax-filter-container">
                <?php if ($permissions['can_filter']): ?>
                    <div class="filter-form-wrap filter-form-wrap-account-log">
                        <h1>Search</h1>
                        <div class="filter-form-wraper">
                            <?php
                                include_partial(
                                    'global/form_ajax',
                                    array(
                                        'form'              => $filter_form,
                                        'action_url'        => url_for('call_log_filter'),
                                        'back_url'          => '',
                                        'submit_name'       => 'Go!',
                                        'form_class_name'   => 'ajax-filter',
                                        'cancel_attributes' => array('class' => 'ajax-form-cancel')
                                    )
                                );
                            ?>
                        </div>
                    </div>
                <?php endif; ?>

                <div class="ajax-pagable">
                    <?php
                        include_partial(
                            'list',
                            array(
                                'pager'                                         => $pager,
                                'indexHelper'                                   => $indexHelper,
                                'campaign'                                      => $campaign,
                                'date_info'                                     => $date_info,
                                'permissions'                                   => $permissions,
                                'call_result_permissions'                       => $call_result_permissions,
                                'can_delete'                                    => $can_delete,
                                'available_seconds_between_analytics_and_call'  => $available_seconds_between_analytics_and_call,
                                'do_show_icons'                                 => $do_show_icons,
                                'is_user_superadmin'							=> $sf_user->getGuardUser()->getIsSuperAdmin(),
                            )
                        );
                    ?>
                </div>
            </div>
        </div>
    </div>

    <?php if ($permissions['can_read']): ?>
        <?php
            $a_options = array();

            if (empty($campaign)) {
                $url_csv = url_for('call_log_csv');
            } else {
                $url_csv = url_for('call_log_csv_campaign', array('campaign_id' => $campaign->getId()));
            }

            $a_options['Get csv result'] = array(
                'href'  => $url_csv,
                'class' => 'icon csv',
            );

            echo icon_links($a_options);
        ?>

    <?php endif; ?>
<?php endif; ?>
     
