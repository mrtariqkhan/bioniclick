<?php 
    include_partial(
        'list',
        array(
            'pager'                                         => $pager,
            'indexHelper'                                   => $indexHelper,
            'campaign'                                      => $campaign,
            'date_info'                                     => $date_info,
            'permissions'                                   => $permissions,
            'call_result_permissions'                       => $call_result_permissions,
            'can_delete'                                    => $can_delete,
            'available_seconds_between_analytics_and_call'  => $available_seconds_between_analytics_and_call,
            'do_show_icons'                                 => $do_show_icons,
        )
    );
?>