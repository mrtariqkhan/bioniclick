<?php $logTitles = $logTitles->getRawValue(); ?>

<div id="filter" class="filter-call-log-fields">
    <h3><a href="#">Fields</a></h3>
    <div style="display:none;">
        <div>
            <div>
                <label>
                    <input type="checkbox" id="select_all" />
                    <span><strong>select / deselect all</strong></span>
                </label>
            </div>
            <?php $j = 1; ?>
            <?php $value = 2; ?>
            <?php foreach ($logTitles as $fieldKey => $fieldTitle): ?>
                <?php if (($j % 6) == 1): ?>
                    <div class="filter_div">
                <?php endif;?>

                <label>
                    <input
                        type="checkbox"
                        name="filed[]"
                        class="field_list"
                        value="<?php echo $value++; ?>"
                        column_code="<?php echo $fieldKey; ?>"
                        field_title="<?php echo $fieldTitle; ?>"
                    />
                    <span><?php echo $fieldTitle; ?></span>
                </label>

                <?php if (($j % 6) == 0): ?>
                    </div>             
                <?php endif; ?>

                <?php $j++; ?>
            <?php endforeach; ?>

            <?php if (($j % 6) != 1): ?>
                </div>
            <?php endif;?>
        </div>
    </div>
</div>