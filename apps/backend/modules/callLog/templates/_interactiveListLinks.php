<?php use_helper('Link'); ?>

<?php

$listButtonTitleEdit = 'Edit additional call info';
$listButtonTitleDel = 'Delete additional call info';

if (!isset($list_buttons)) {
    $list_buttons = array(
        $listButtonTitleEdit => array(
            'class' => 'icon edit ajax-form-load'
        ),
        $listButtonTitleDel => array(
            'class' => 'icon delete'
        )
    );
}


$paramsCommon = $paramsCommon->getRawValue();

$a_options = array();

if ($call_result_permissions['can_update']):
    $params_edit = array_merge(
        array('twilio_incoming_call_id' => $rawArray['id']),
        $paramsCommon
    );
    $route_name_edit = empty($campaign) ? 'call_result_interactive_edit' : 'call_result_interactive_campaign_edit';
    $url_edit = url_for($route_name_edit, $params_edit);

    $a_options[$listButtonTitleEdit] = array(
        'href'  => $url_edit,
        'class' => 'icon edit ajax-form-load'
    );
    $a_options[$listButtonTitleEdit]['class'] = $list_buttons[$listButtonTitleEdit]['class'];
    $a_options[$listButtonTitleEdit]['href'] = $url_edit;
endif;

if (isset($rawArray['CallResult']) && !empty($rawArray['CallResult']['id']) && $call_result_permissions['can_delete']):
    $params_delete = array_merge(
        array('id' => $rawArray['CallResult']['id']),
        $paramsCommon
    );
    $url_delete = url_for('call_result_delete', $params_delete);

    $a_options[$listButtonTitleDel] = link_to_ajax_delete(
        '',
        $url_delete,
        array(
            'title'     => $listButtonTitleDel,
            'class'     => 'icon delete',
            'after_url' => url_for('call_logs_interactive', $paramsCommon),
        )
    );
endif;

echo icon_links($a_options, array('class' => 'icon-links'), false);

?>