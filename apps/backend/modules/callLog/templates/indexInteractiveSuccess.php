<div class="inner">

    <?php if (!empty($campaign)): ?>
        <h3>Interactive Logs for campaign: <?php echo $campaign->getName(); ?></h3>
    <?php endif; ?>

    <?php $logTitles = $interactiveIndexHelper->getTitles(); ?>
    <!--<?php include_partial('fieldsSetSelect', array('logTitles' => $logTitles)); ?>-->

    <div class="clearfix">
        <div class="clear"></div>
        <div class="pause">Pause</div>
        <div class="start">Play</div>
        <?php
            include_partial(
                'listInteractiveProgress',
                array(
                    'pager'                                         => $interactivePager,
                    'campaign'                                      => $campaign,
                    'indexHelper'                                   => $interactiveIndexHelper,
                    'begin_timestamp'                               => $begin_timestamp,
                    'date_info'                                     => $date_info,
                    'call_result_permissions'                       => $call_result_permissions,
                    'available_seconds_between_analytics_and_call'  => $available_seconds_between_analytics_and_call,
                    'is_user_superadmin'							=> $sf_user->getGuardUser()->getIsSuperAdmin(),
                )
            );
        ?>
    </div>
</div>