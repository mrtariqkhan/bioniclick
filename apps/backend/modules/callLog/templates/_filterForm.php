<div class="form-filter-log">
    <h3>Filter date:</h3>
    <?php
        $url_for_filter = array_merge(
            array(
                'module' => 'log',
                'action' => 'index'
            ),
            $backURL
        );

        include_partial(
            'global/searchFilter',
            array(
                'form'    => $date_filter,
                'action_url' => url_for($url_for_filter)
            )
        );
    ?>
</div>