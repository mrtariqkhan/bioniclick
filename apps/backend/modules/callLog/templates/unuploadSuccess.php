<?php use_stylesheet('/css/project/upload_errors.css') ?>
<?php use_helper('Link'); ?>

<?php
   $backURL_array = array();
   foreach ($backURL as $key => $value):
       $backURL_array[$key] = $value;
   endforeach;
   $backURL = $backURL_array;
?>
<div class="upload-errors">
    <p><h3>Errors have occured:</h3></p>
    <?php if (count($errors) > 0): ?>
        <ul>
            <?php foreach($errors as $field => $error): ?>
            <li>
                <strong><?php echo $field . ':' ?></strong>
                <?php echo $error; ?>
            </li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
</div>

<?php
 $return_url = array_merge(
        array(
            'module' => 'log',
            'action' => 'index',
        ),
        $backURL
);
$url = url_for($return_url);
$a_options = array(
'Back to log page' => array(
'href' => $url,
'class' => "icon reload"
  )
);
echo icon_links($a_options);
?>
<br/>