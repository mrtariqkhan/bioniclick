<?php use_helper('Link'); ?>

<?php $logTitles = $indexHelper->getTitles()->getRawValue(); ?>

<?php
    $campaignId = empty($campaign) ? null : $campaign->getId();
    $paramsCampaign = empty($campaign) ? array() : array('campaign_id' => $campaignId);

    $paramsCommon = array('begin_timestamp' => $begin_timestamp);
    $paramsCommon = array_merge($paramsCommon, $paramsCampaign);
?>


<div class="log-interactive-date-info display-text">
    <h3>Last <?php echo $pager->getMaxPerPage(); ?> calls</h3>
    Up to: &nbsp;
    <span class="interactive-date-info-to">
        <?php echo "{$date_info['to']}"; ?>
    </span>
</div>
<?php if($is_user_superadmin == 1 ){ ?>
<div>
    Available seconds between visitor analytics and call:
    <span>
        <?php echo $available_seconds_between_analytics_and_call; ?>
    </span>
</div>
<?php }?>
<div class="clear"></div>
<?php
    $listButtonTitleEdit = 'Edit additional call info';
    $listButtonTitleDel = 'Delete additional call info';

    $list_buttons = array(
        $listButtonTitleEdit => array(
            'class' => 'icon edit ajax-form-load'
        ),
        $listButtonTitleDel => array(
            'class' => 'icon delete'
        )
    );

    $legend_buttons = & $list_buttons;
?>

<div id="interactiveContainer" class="log-data">
    <table id='callslogInteractiveTable'
        class="log-table log-table-interactive-progress table flexme"
        begin_timestamp="<?php echo $begin_timestamp; ?>"
        max_raws_amount="<?php echo $pager->getMaxPerPage(); ?>"
        data_campaign_id="<?php echo $campaignId; ?>"
    >
         <script type="text/javascript">
            columnsModel = Array();
            var columnModel1 = Array();
            columnModel1['display'] = "Call Record";
            columnModel1['name'] = "call_record";
            columnModel1['width'] = 50;
            columnModel1['sortable'] = 'false';
            columnModel1['align'] = 'center';

            var columnModel2 = Array();
            columnModel2['display'] = "Call Info";
            columnModel2['name'] = "call_info";
            columnModel2['width'] = 75;
            columnModel2['sortable'] = 'false';
            columnModel2['align'] = 'center';

            columnsModel.push(columnModel1);
            columnsModel.push(columnModel2);
            <?php foreach ($logTitles as $fieldKey => $fieldTitle): ?>
                var columnModel = Array();
                columnModel['display'] = "<?php echo $fieldTitle; ?>";
                columnModel['name'] = "<?php echo $fieldKey; ?>";
                if("<?php echo $fieldKey; ?>" == "recording"
                    || "<?php echo $fieldKey; ?>" == "url")
                    columnModel['width'] = 200;
                else  if("<?php echo $fieldKey; ?>" == "startTime"
                    || "<?php echo $fieldKey; ?>" == "endTime")
                    columnModel['width'] = 175;
                else if("<?php echo $fieldKey; ?>" == "duration")
                    columnModel['width'] = 50;
                else
                    columnModel['width'] = 150;
                columnModel['sortable'] = 'true';
                columnModel['align'] = 'center';
                columnsModel.push(columnModel);
            <?php endforeach; ?>        
        </script>
        <tbody>
           <?php
            
            $i = $pager->getFirstIndice();
            $list = $pager->getResults()->getRawValue();
            
            foreach ($list as $rawArray):
            ?>
                <tr class="<?php echo (($i % 2) ? 'odd' : 'even') . ' id-' . $rawArray['sid']; ?>">
                    <td ><?php echo $i;?></td>
                    <td class="links">
                        <?php
                            $a_options = array();

                            if ($call_result_permissions['can_update']) {
                                $params_edit = array_merge(
                                    array('twilio_incoming_call_id' => $rawArray['id']),
                                    $paramsCommon
                                );
                                $route_name_edit = empty($campaign) ? 'call_result_interactive_edit' : 'call_result_interactive_campaign_edit';
                                $url_edit = url_for($route_name_edit, $params_edit);

                                $a_options[$listButtonTitleEdit] = array(
                                    'href'  => $url_edit,
                                    'class' => 'icon edit ajax-form-load'
                                );
                                $a_options[$listButtonTitleEdit]['class'] = $list_buttons[$listButtonTitleEdit]['class'];
                                $a_options[$listButtonTitleEdit]['href']  = $url_edit;
                            }

                            if (!empty($rawArray['call_result_id']) && $call_result_permissions['can_delete']) {
                                $params_delete = array_merge(
                                    array('id' => $rawArray['call_result_id']),
                                    $paramsCommon
                                );
                                $url_delete = url_for('call_result_delete', $params_delete);

                                $a_options[$listButtonTitleDel] = link_to_ajax_delete(
                                    '',
                                    $url_delete,
                                    array(
                                        'title'     => $listButtonTitleDel,
                                        'class'     => 'icon delete',
                                        'after_url' => url_for('call_logs_interactive', $paramsCommon),
                                    )
                                );
                            }

                            echo icon_links($a_options, array('class' => 'icon-links'), false);
                        ?>
                    </td>

                    <?php foreach ($logTitles as $fieldKey => $fieldTitle): ?>

                        <?php $value = $rawArray[$fieldKey]; ?>

                        <?php $tdClass = ($fieldKey == 'callStatus') ? 'status' : ''; ?>
                        <td class="<?php echo $tdClass; ?>" column_code="<?php echo $fieldKey; ?>">
                            <?php echo $value; ?>
                        </td>

                    <?php endforeach; ?>
                </tr>
                <?php $i++; ?>
            <?php endforeach; ?>


        </tbody>
    </table>
     <script>
            function adjustLogInteractiveTable(){
                //apply table jquery plugin
                $('#callslogInteractiveTable').flexigrid({
                    colModel : columnsModel,
                    dblClickResize: true,
                    // sortorder: "asc",
                    // title: 'Call logs',
                    showTableToggleBtn: true,
                    minColToggle: 10,
                    minwidth: 150,
                });  
                var headerElementsSelector = '#interactiveContainer.log-data div.hDivBox table th';
                var checkboxesElementsSelector = '#interactiveContainer.log-data input.togCol';
                var tableElementsSelector = '#interactiveContainer.log-data div.bDiv td';
                var filterElementBoxSelector = '#interactiveContainer.log-data div.cDrag div';

                confgureFlexigrid(headerElementsSelector, checkboxesElementsSelector, 
                    tableElementsSelector, filterElementBoxSelector);
            }
            setTimeout('adjustLogInteractiveTable()', 100);
            
        </script>
</div>

<?php
    include_partial(
         'global/legend',
          array(
            'legend_buttons'  => $legend_buttons,
          )
    ); 
?>