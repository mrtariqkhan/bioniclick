<?php

/**
 * callLog actions.
 *
 * @package    bionic
 * @subpackage callLog
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class callLogActions extends GeneralActions {

    var $callResultsSecurityHelper = null;

    public function  preExecute() {

        $this->tableName = 'TwilioIncomingCall';
        parent::preExecute();

        //Permissions for call results managing at call log page
        $this->callResultsSecurityHelper = new SecurityHelper($this, 'callResult');
        $this->call_result_permissions = $this->callResultsSecurityHelper->checkBasePermissions();
    }

    public function executeIndex(sfWebRequest $request) {
        
        $this->executeList($request);
        $filterValues = $this->getFilter();
        $this->filter_form = $this->getFilterForm();
        $this->filter_form->setDefaults($filterValues);
        $this->setTemplate('index');
    }

    public function executeList(sfWebRequest $request) {
        $campaignId = $request->getParameter('campaign_id');
        $this->campaign = $this->getCheckedCampaign($campaignId);
        $indexHelper = new TwilioIncomingCallIndexHelper(
            $campaignId,
            $this->user
        );

        $pagerParams = $this->getPagerParams($request);
        $this->initPagerLog($request, $indexHelper, $pagerParams['maxPerPage'], $pagerParams['page']);

        $this->can_delete = $this->user->getIsSuperAdmin();

        $this->available_seconds_between_analytics_and_call = TimeConverter::convertSecondsToTime(sfConfig::get('app_available_seconds_between_visitor_analytics_and_call'));
        $this->do_show_icons = $this->user->getIsSuperAdmin();
    }

    public function executeDelete(sfWebRequest $request) {
        return $this->processAjaxDelete($request);
    }

    public function initPagerLog(
        sfWebRequest $request, 
        TwilioIncomingCallIndexHelper $indexHelper, 
        $maxPerPage = 0, 
        $page = 0,
        $csv =0
    ) {

        $filterValues = $this->getFilter();
        $filterForm = $this->getFilterForm();
        $filterForm->setDefaults($filterValues);
        $cleanedValues = $filterForm->getFilteredValues($filterValues, true);
        
        $timezoneName = $this->user->findTimezoneName();
        $this->date_info = $this->configureDateInfo($cleanedValues, $timezoneName);

        $pager = new TwilioIncomingCallIndexPager('', $maxPerPage);
        $pager->init();
        $pager->setPage($page);

        $pager = $indexHelper->retrieveAll($pager, $cleanedValues, $csv);


        $pager->init();

        $this->pager = $pager;        
        $this->indexHelper = $indexHelper;
    }

    public function executeIndexInteractive(sfWebRequest $request) {

        $beginTimestamp = $request->getParameter('begin_timestamp', null);

        $interactiveResults = $this->configureInteractiveResults($request, $beginTimestamp);
        $this->interactivePager         = $interactiveResults['pager'];
        $this->interactiveIndexHelper   = $interactiveResults['indexHelper'];
        $this->begin_timestamp          = $interactiveResults['beginTimestamp'];
        $this->date_info                = $interactiveResults['dateInfo'];
        $this->campaign                 = $interactiveResults['campaign'];

        $this->available_seconds_between_analytics_and_call = TimeConverter::convertSecondsToTime(sfConfig::get('app_available_seconds_between_visitor_analytics_and_call'));
    }

    public function executeAjaxInProgressCalls(sfWebRequest $request) {

        $error = false;

        $beginTimestamp = $request->getPostParameter('begin_timestamp', null);
        if (empty($beginTimestamp)) {
            $error = true;
        }

        $callsFullInfoBeginTimestamp = $beginTimestamp;
        $interactiveResults = $this->configureInteractiveResults($request, $callsFullInfoBeginTimestamp);
        $pager              = $interactiveResults['pager'];
        $indexHelper        = $interactiveResults['indexHelper'];
        $dateInfo           = $interactiveResults['dateInfo'];
        $newBeginTimestamp  = $interactiveResults['beginTimestamp'];
        $campaignId         = $interactiveResults['campaignId'];

        $callsInfo = $this->configureCallsInfo($pager, $beginTimestamp, $campaignId);

        $result = array(
            'calls'             => $callsInfo,
            'begin_timestamp'   => $beginTimestamp,
            'date_to'           => $dateInfo['to'],
            'success'           => empty($error),
        );

        return $this->processAjaxResult($request, $result);
    }

    protected function configureCallsInfo($interactivePager, $beginTimestamp, $campaignId) {

        $incomingCallsCells = $interactivePager->getResults();

        $paramsCommon = array(
            'begin_timestamp'   => $beginTimestamp,
            'campaign_id'       => $campaignId,
        );
        $partialParams = array(
            'paramsCommon'              => $paramsCommon,
            'campaign'                  => null,
            'call_result_permissions'   => $this->call_result_permissions,
        );

        foreach ($incomingCallsCells as $i => $rawArray) {
            
            $partialParams['rawArray'] = $rawArray;
            
            $rawArray['links'] = $this->getPartial(
                'callLog/interactiveListLinks',
                $partialParams
            );

            $incomingCallsCells[$i] = $rawArray;
        }
        
        //print_r($incomingCallsCells);
        //die();
        
        return $incomingCallsCells;
    }

    protected function configureInteractiveResults(sfWebRequest $request, $beginTimestamp) {

        //TODO:: remove unnecessary parameter $beginTimestamp when it will be possible.
        $campaignId = $request->getParameter('campaign_id');
        $campaign = $this->getCheckedCampaign($campaignId);

        $nowTimestamp = TimeConverter::getUnixTimestamp();
        $cleanedValues = array(
            'to' => $nowTimestamp,
        );
        if (!empty($beginTimestamp)) {
            $cleanedValues['from'] = $beginTimestamp;
        }


        $limit = sfConfig::get('app_call_logs_interactive_last_amount', null);
        $pager = new TwilioIncomingCallIndexPager('', $limit);
        $pager->init();
        $pager->setPage(0);

        $indexHelper = new TwilioIncomingCallIndexInteractiveHelper(
            $campaignId,
            $this->user
        );
        $pager = $indexHelper->retrieveAll($pager, $cleanedValues);

//        $incomingCallsCells = $pager->getResults();
//        if (!empty($incomingCallsCells)) {
//            $keys = array_keys($incomingCallsCells);
//            $oldestCallKey = $keys[count($keys) - 1];
//            $oldestCall = $incomingCallsCells[$oldestCallKey];
//            $beginTimestamp = $oldestCall['twilioIncomingCall_startTime'];
//        }

        $timezoneName = $this->user->findTimezoneName();
        $dateInfo = $this->configureDateInfo($cleanedValues, $timezoneName);

        return array(
            'pager'             => $pager,
            'indexHelper'       => $indexHelper,
            'campaign'          => $campaign,
            'campaignId'        => $campaignId,
            'beginTimestamp'    => $beginTimestamp,
            'dateInfo'          => $dateInfo,
        );
    }

    public function executeCsv(sfWebRequest $request) {
        $campaignId = $request->getParameter('campaign_id');

        $indexHelper = new TwilioIncomingCallCSVHelper(
            $campaignId,
            $this->user
        );

        //TODO:: think about max_per_page to see ALL logs, not 200
        $this->initPagerLog($request, $indexHelper, 600,0,1);


        $csvGenerator = new CsvGenerator($this->pager, $indexHelper);

        $pathToFile     = $csvGenerator->generateFile();
        $outFileName    = $csvGenerator->getFileName();


        $mimeType = 'application/csv';
        return $this->downloadFile($pathToFile, $mimeType, $outFileName);
    }
    
    public function executeDownloadRecording(sfWebRequest $request) {
        
        $recording = TwilioCallRecordingTable::getInstance()
            ->findOneBy('id', $request->getParameter('id'));
        
        $pathToFile     = $recording->getFileUrl(). '.mp3';
        $outFileName    = $request->getParameter('id') . '.mp3';

        $mimeType = 'audio/mpeg';
        return $this->downloadFile($pathToFile, $mimeType, $outFileName);
    }

    public function executeFilter(sfWebRequest $request) {

        $filterForm = $this->getFilterForm();

        return $this->processAjaxFilter($request, $filterForm);
    }

    protected function getFilterForm() {

        $options = $this->getBranchFilterOptions();
        $timezoneName = $this->user->findTimezoneName();
        $options['timezone_name'] = $timezoneName;
        $options['do_show_calls_type'] = $this->user->getIsSuperAdmin();
        return new CallLogFilterForm(null, $options);
    }

    protected function configureDateInfo($cleanedValues, $timezoneName) {

        $date_info = array();

        if (array_key_exists('from', $cleanedValues) && !empty($cleanedValues['from'])) {
            $date_info['from'] = TimeConverter::getDateTime($cleanedValues['from'], $timezoneName);
        }

        if (array_key_exists('to', $cleanedValues) && !empty($cleanedValues['to'])) {
            $date_info['to'] = TimeConverter::getDateTime($cleanedValues['to'], $timezoneName);
        }

        return $date_info;
    }

    protected function getCheckedCampaign($campaignId) {

        $campaign = null;
        if (!empty($campaignId)) {
            $campaign = CampaignTable::findById($campaignId);
            $this->forward404If(
                empty($campaign),
                sprintf('Object campaign does not exist (%s).', $campaignId)
            );
            $this->checkIsAllowableObject($campaign, false);
        }

        return $campaign;
    }

    public function executeUpdateTwilioInfoTask(sfWebRequest $request) {

        return $this->processTask(
            'CallInfoTask',
            array(),
            array()
        );
    }
}