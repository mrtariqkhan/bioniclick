<?php

/**
 * advertiserIncomingNumberPoolLog actions.
 *
 * @package    bionic
 * @subpackage advertiserIncomingNumberPoolLog
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class advertiserIncomingNumberPoolLogActions extends GeneralActions {

    /**
    * Executes index action
    *
    * @param sfRequest $request A request object
    */
    public function executeListForOrder(sfWebRequest $request) {
        
        $object = $this->getRoute()->getObject();
        $isAllowable = $object->isAllowable($this->user, true, false);
        $this->forward404Unless($isAllowable);

        $this->order = $object;

        $orderId = $object->getId();
        $specialQuery = AdvertiserIncomingNumberPoolLogTable::createIndexQueryByOrderId(
            $orderId
        );
        $this->pager = parent::initPager($request, $specialQuery);

        $this->timezoneName = $this->user->findTimezoneName();


        $this->params_additional = array();
        $this->urlBack = null;
        $this->routePostfix = $request->getParameter('routePostfix', '');
        switch ($this->routePostfix) {
            case 'User':
                $this->urlBack = $this->generateUrl('incomingNumbersQueue_indexUser');
                break;
            case 'Advertiser':
                $advertiserId = $object->getAdvertiserId();

                $this->urlBack = $this->generateUrl('incomingNumbersQueue_indexAdvertiser', array('advertiser_id' => $advertiserId));
                $this->params_additional = array('advertiser_id' => $advertiserId);
                break;
            case 'Company':
                $companyId = $request->getParameter('company_id');
                $company = CompanyTable::getInstance()->find($companyId);
                $this->forward404Unless(!empty($company), 'Cannot find Company.');

                $this->urlBack = $this->generateUrl('incomingNumbersQueue_indexCompany', array('company_id' => $companyId));
                $this->params_additional = array('company_id' => $companyId);
                break;
            default:
                $this->urlBack = $this->generateUrl('incomingNumbersQueue_index');
        }

        $this->historyIsNeed = $request->getParameter('history', false);
    }
}
