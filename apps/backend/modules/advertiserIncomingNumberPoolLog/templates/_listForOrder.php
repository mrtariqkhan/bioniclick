
<?php use_helper('Link'); ?>
<?php use_helper('Pager'); ?>

<?php
    $orderId = $order->getId();

    if ($historyIsNeed) {
        $params_additional = $params_additional->getRawValue();
    }

    $url = url_for('advertiser_incoming_number_pool_log_list_order', array('id' => $orderId, 'history' => $historyIsNeed));

    $maxAmountPrevNextRelated = sfConfig::get('app_max_amount_previous_next_related', 2);
    $limitPrevNextRelated = $maxAmountPrevNextRelated + 1;
?>

<?php if ($permissions['can_read']): ?>
    <?php
        $list_buttons = array(
        );

        $legend_buttons = array();
    ?>

    <?php
        $pagerHtmlString = pager(
            $pager->getRawValue(),
            sfConfig::get('app_pager_max_per_pages'),
            $url,
            null,
            'ajax-pager'
        );
    ?>

    <p>
        <?php
            include_partial(
                'global/pager_list_header',
                array(
                    'pager' => $pager,
                    'pagerHtmlString' => $pagerHtmlString
                )
            );
        ?>
    </p>
    <div class="table-block">
        <table class="table">
            <thead>
                <tr>
                    <th class="first">&nbsp;</th>
                    <th>Date</th>
                    <th>Dynamic Phone Number</th>
                    <?php if ($historyIsNeed): ?>
                        <th>Previous related orders</th>
                        <th>Next related orders</th>
                    <?php endif; ?>
                    <th class="last"></th>
                </tr>
            </thead>
            <tbody>
                <?php $i = $pager->getFirstIndice(); ?>
                <?php $list = $pager->getResults(); ?>
                <?php $list = $list->getRawValue(); ?>
                <?php foreach ($list as $object): ?>
                    <tr class="<?php echo ($i%2) ? 'odd' : 'even'; ?>">
                        <td><?php echo $i; ?></td>
                        <td><?php echo $object->getActionDateFormatted($timezoneName, 'h:i a D d/m/Y'); ?></td>
                        <td><?php echo $object->getTwilioIncomingPhoneNumber(); ?></td>
                        <?php if ($historyIsNeed): ?>
                            <td>
                                <?php $prevIncomingNumbersQueues = $object->getPreviousQueuesOfPhoneNumber($limitPrevNextRelated); ?>
                                <?php if ($prevIncomingNumbersQueues->count() > 0): ?>
                                    <?php if ($prevIncomingNumbersQueues->count() > $maxAmountPrevNextRelated): ?>
                                        ...
                                    <?php endif; ?>

                                    <?php $j = 1; ?>
                                    <?php foreach ($prevIncomingNumbersQueues as $incomingNumbersQueue): ?>
                                        <?php if ($j > $maxAmountPrevNextRelated): ?>
                                            <?php break; ?>
                                        <?php elseif ($j > 1): ?>
                                            ;
                                        <?php endif; ?>
                                        <?php
                                            $params = array_merge(array('id' => $incomingNumbersQueue->getId()), $params_additional);
                                            $options = array(
                                                'class' => 'ajax-load',
                                                'title' => $incomingNumbersQueue->getDescriptionStringShort(),
                                            );
                                            echo link_to2(
                                                $incomingNumbersQueue->getId(), 
                                                'incomingNumbersQueue_show' . $routePostfix,
                                                $params, 
                                                $options
                                            );
                                        ?>
                                        <?php ++$j; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </td>
                            <td>
                                <?php $nextIncomingNumbersQueues = $object->getNextQueuesOfPhoneNumber($limitPrevNextRelated); ?>
                                <?php if ($nextIncomingNumbersQueues->count() > 0): ?>
                                    <?php $j = 1; ?>
                                    <?php foreach ($nextIncomingNumbersQueues as $incomingNumbersQueue): ?>
                                        <?php if ($j > $maxAmountPrevNextRelated): ?>
                                            ...
                                            <?php break; ?>
                                        <?php elseif ($j > 1): ?>
                                            ;
                                        <?php endif; ?>
                                        <?php
                                            $params = array_merge(array('id' => $incomingNumbersQueue->getId()), $params_additional);
                                            $options = array(
                                                'class' => 'ajax-load',
                                                'title' => $incomingNumbersQueue->getDescriptionStringShort(),
                                            );
                                            echo link_to2(
                                                $incomingNumbersQueue->getId(), 
                                                'incomingNumbersQueue_show' . $routePostfix,
                                                $params, 
                                                $options
                                            );
                                        ?>
                                        <?php ++$j; ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </td>
                        <?php endif; ?>
                        <td class="last">
                        </td>
                    </tr>
                    <?php ++$i; ?>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>

    <?php
        include_partial(
             'global/legend',
              array(
                'legend_buttons'  => $legend_buttons,
              )
        ); 
    ?>

    <?php echo $pagerHtmlString; ?>
<?php endif; ?>

