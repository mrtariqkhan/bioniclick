<?php use_helper('Link'); ?>

<?php $id = $advertiser->getId(); ?>
<?php $ifShowUpdateLinks = WasDeletedHelper::ifCanUpdate($advertiser->getRawValue()); ?>

<?php
    $a_options = array();

    if ($ifShowUpdateLinks) {
        if ($permissions['can_update']) {
            $a_options['Edit advertiser'] = array(
                'href' => url_for('sub_tree_advertiser_edit', array('id' => $id)),
                'class' => 'main icon edit ajax-load'
            );
        }
    }
    
    $a_options['Orders'] = array(
        'href' => url_for('incomingNumbersQueue_indexAdvertiser', array('advertiser_id' => $id)),
        'class' => 'main icon orders ajax-load'
    );

    if ($permissions_finance['can_read']) {
        $a_options['Usage Report'] = array(
            'href' => url_for('finance_show_sub_tree_for_advertiser', array('id' => $id)),
            'class' => 'main icon finance-position ajax-load'
        );
    }

    $a_options['Charts'] = array(
        'href' => url_for('homepage_for_child_advertiser', array('advertiser_id' => $id)),
        'class' => 'main icon icon-charts'
    );

    echo icon_links($a_options, array('class' => 'icon-links clearfix'));
?>
<h1><?php echo $advertiser->getName(); ?></h1>

<h2>Dynamic Phone Number List</h2>
<div class="ajax-pagable">
    <?php
        include_component(
            'advertiserIncomingNumberPool',
            'listForAdvertiser',
            array(
                'pager'         => $pagerIncoming,
                'advertiser'    => $advertiser,
                'is_debtor'     => $is_debtor,
            )
        );
    ?>
</div>



<?php
    $a_options = array();

    if ($permissions['can_read']) {
        if ($is_company_user) {
            $a_options['Back'] = array(
                'href'  => url_for('sub_tree_advertiser_index', array('parent_company_id' => $advertiser->getCompanyId())),
                'class' => "icon back ajax-load"
            );
        }
    }

    echo icon_links($a_options);
?>