<h1>Create new advertiser</h1>

<div class="inner">

    <?php $urlAction = url_for('advertiser_create'); ?>
    <?php $urlAfter = url_for('advertiser_index'); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'submit_name'       => 'Create',
                'back_url'          => $urlAfter,
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel')
            )
        );
    ?>
</div>
