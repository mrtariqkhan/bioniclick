<?php $object = $form->getObject(); ?>

<h1>
    Edit "<?php echo $object; ?>" advertiser
</h1>

<div class="inner">

    <?php $urlAction = url_for('advertiser_current_update'); ?>
    <?php $urlAfter = url_for('homepage'); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'submit_name'       => 'Update',
                'back_url'          => $urlAfter,
                'back_url_type'     => 'not-in-tab',
                'back_url_method'   => 'GET',
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel')
            )
        );
    ?>
</div>
