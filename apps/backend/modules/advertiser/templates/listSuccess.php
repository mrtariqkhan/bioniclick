<?php 
    include_partial(
        'list',
        array(
            'pager'             => $pager,
            'branch_data'       => $branch_data,
            'permissions'       => $permissions,
            'permissions_user'  => $permissions_user,
            'permissions_advertiser_incoming_number_pool'   => $permissions_advertiser_incoming_number_pool,
            'bionic_company_free_numbers'                   => $bionic_company_free_numbers,
        )
    );
?>