<?php use_helper('Link'); ?>
<?php use_helper('Pager'); ?>

<?php include_partial('global/flash_message', array('flash_name' => 'message')); ?>

<?php $reloadTreeKey = TreeNodeBranching::generateTreeNodeKeyAdvertisers($parent_company->getRawValue()); ?>

<?php
    $pagerHtmlString = pager(
        $pager->getRawValue(),
        sfConfig::get('app_pager_max_per_pages'),
        url_for('sub_tree_advertiser_list', array('parent_company_id' => $parent_company->getId())),
        null,
        'ajax-pager'
    );
?>
<?php $headers = $branch_data['headers']; ?>
<?php $raws    = $branch_data['raws']; ?>
<p>
    <?php
        include_partial(
            'global/pager_list_header',
            array(
                'pager' => $pager,
                'pagerHtmlString' => $pagerHtmlString
            )
        );
    ?>
</p>
<div class="table-block">
    <table class="table">
        <thead>
            <tr>
                <th class="first">&nbsp;</th>
                <?php
                    $list_buttons = array(
                        'Number Pool' => array(
                            'class' => 'icon pool ajax-load'
                        ),
                        'Campaigns' => array(
                            'class' => 'icon campaigns ajax-load'
                        ),
                        'Edit' => array(
                            'class' => 'icon edit ajax-form-load'
                        ),
                        'Delete' => array(
                            'class' => 'icon delete'
                        )
                    );

                    $legend_buttons = array();

                    foreach ($headers as $headerTitle):
                ?>
                <th><?php echo $headerTitle; ?></th>
                <?php endforeach; ?>
                <th class="last">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = $pager->getFirstIndice(); ?>
            <?php foreach ($raws as $id => $raw): ?>
                <tr class="<?php echo ($i%2) ? 'odd' : 'even'; ?>">

                    <td><?php echo $i; ?></td>

                    <?php $raw = $raw->getRawValue(); ?>
                    <?php foreach ($raw as $key => $cell): ?>
                        <?php $value = (WasDeletedHelper::KEY == $key) ? WasDeletedHelper::getAsString($cell) : $cell; ?>
                        <td><?php echo $value; ?></td>
                    <?php endforeach; ?>
                    <td>
                        <?php $urlAfter = url_for('sub_tree_advertiser_index', array('parent_company_id' => $parent_company->getId())); ?>
                        <?php
                            $a_options = array();

                            if ($permissions['can_read']) {
                                $title = 'Number Pool';
                                $a_options[$title] = $list_buttons[$title];
                                $a_options[$title]['href'] = url_for('advertiser_show', array('id' => $id));

                                $legend_buttons[$title] = & $list_buttons[$title];
                            }

                            if ($permissions_campaign['can_read']) {
                                $a_options['Campaigns'] = $list_buttons['Campaigns'];
                                $a_options['Campaigns']['href'] = url_for('campaigns_for_advertiser', array('advertiser_id' => $id));

                                $legend_buttons['Campaigns'] = & $list_buttons['Campaigns'];
                            }

                            $ifShowUpdateLinks = WasDeletedHelper::ifCanUpdateAsArray($raw);
                            if ($ifShowUpdateLinks) {

                                if ($permissions['can_update']) {
                                    $a_options['Edit'] = $list_buttons['Edit'];
                                    $a_options['Edit']['href'] = url_for('sub_tree_advertiser_edit_list', array('id' => $id));

                                    $legend_buttons['Edit'] = & $list_buttons['Edit'];
                                }

                                if ($permissions['can_delete']) {
                                    $a_options['Delete'] = link_to_ajax_delete(
                                        '',
                                        url_for('advertiser_delete', array('id' => $id)),
                                        array(
                                            'title'             => 'Delete',
                                            'class'             => 'icon delete',
                                            'special_form_data' => $reloadTreeKey,
                                            'after_url'         => $urlAfter,
                                        )
                                    );

                                    $legend_buttons['Delete'] = & $list_buttons['Delete'];
                                }
                            }

                            echo icon_links($a_options, array('class' => 'icon-links clearfix'), false);
                        ?>
                    </td>
                </tr>
                <?php ++$i; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php
    include_partial(
         'global/legend',
          array(
            'legend_buttons'  => $legend_buttons,
          )
    ); 
?>

<?php echo $pagerHtmlString; ?>