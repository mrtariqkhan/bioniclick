<?php $object = $form->getObject(); ?>

<h1>
    Edit "<?php echo $object; ?>" advertiser
</h1>

<div class="inner">

    <?php $urlAction = url_for('advertiser_update', array('id' => $object->getId())); ?>
    <?php $urlAfter = url_for('advertiser_show', array('id' => $object->getId())); ?>

    <?php
        $reloadTreeKey = '';
        if ($is_advertiser_user) {
            $reloadTreeKey = TreeNodeBranching::generateTreeNodeKeyRoot();
        } else if ($is_company_user) {
            $parent_company = $object->getCompany();
            $reloadTreeKey = TreeNodeBranching::generateTreeNodeKeyAdvertisers($parent_company);
        }
    ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'submit_name'       => 'Update',
                'back_url'          => $urlAfter,
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel'),
                'form_special_data' => $reloadTreeKey,
            )
        );
    ?>
</div>
