<?php use_helper('Link'); ?>
<?php use_helper('Pager'); ?>

<?php include_partial('global/flash_message', array('flash_name' => 'message')); ?>

<?php
    $pagerHtmlString = pager(
        $pager->getRawValue(),
        sfConfig::get('app_pager_max_per_pages'),
        url_for('advertiser_list'),
        null,
        'ajax-pager'
    );
?>
<?php $headers = $branch_data['headers']; ?>
<?php $raws    = $branch_data['raws']; ?>
<p>
    <?php
        include_partial(
            'global/pager_list_header',
            array(
                'pager' => $pager,
                'pagerHtmlString' => $pagerHtmlString
            )
        );
    ?>
</p>
<div class="table-block">
    <table class="table">
        <thead>
            <tr>
                <th class="first">&nbsp;</th>
                <?php
                    $list_buttons = array(
                        'New campaign' => array(
                            'class' => 'icon new-campaign ajax-form-load'
                        ),
                        'Purchase numbers for advertiser' => array(
                            'class' => 'icon assign-numbers ajax-form-load'
                        ),
                        'New user' => array(
                            'class' => 'icon new-user ajax-form-load'
                        ),
                        'Edit' => array(
                            'class' => 'icon edit ajax-form-load'
                        ),
                        'Delete' => array(
                            'class' => 'icon delete'
                        )
                    );

                    $legend_buttons = array();

                    foreach ($headers as $headerTitle):
                ?>
                <th><?php echo $headerTitle; ?></th>
                <?php endforeach; ?>
                <th class="last">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = $pager->getFirstIndice(); ?>
            <?php foreach ($raws as $id => $raw): ?>
                <tr class="<?php echo ($i%2) ? 'odd' : 'even'; ?>">

                    <td><?php echo $i; ?></td>

                    <?php $raw = $raw->getRawValue(); ?>
                    <?php foreach ($raw as $key => $cell): ?>
                        <?php
                            if (WasDeletedHelper::KEY == $key) {
                                $value = WasDeletedHelper::getAsString($cell);
                            } elseif ('getIsDebtor' == $key) {
                                $value = BooleanHelper::getAsString($cell);
                            } else {
                                $value = $cell;
                            }
                        ?>
                        <td><?php echo $value; ?></td>
                    <?php endforeach; ?>
                    <td>
                        <?php $urlAfter = url_for('advertiser_index', array()); ?>
                        <?php
                            $a_options = array();

                            $ifShowUpdateLinks = WasDeletedHelper::ifCanUpdateAsArray($raw);

                            //TODO:: add displaying of GREY all possibilities when $ifShowUpdateLinks = false
                            if ($ifShowUpdateLinks) {

                                if ($permissions['can_create']) {
                                    $advertiserName = array_key_exists('getName', $raw) ? $raw['getName'] : '';
                                    $wasDeleted = array_key_exists(WasDeletedHelper::KEY, $raw) ? $raw[WasDeletedHelper::KEY] : '';
                                    $errMsg = AdvertiserTable::getWhyCannotCreateNewCampaign($id, $advertiserName, $wasDeleted);
                                    if (empty($errMsg)) {
                                        $a_options['New campaign'] = $list_buttons['New campaign'];
                                        $a_options['New campaign']['href'] = url_for('campaign_new_for_advertiser', array('advertiser_id' => $id));
                                    } else {
                                        $a_options['New campaign'] = $list_buttons['New campaign'];
                                        $a_options['New campaign']['title'] = $errMsg;
                                    }

                                    $legend_buttons['New campaign'] = & $list_buttons['New campaign'];
                                }

                                if ($permissions_advertiser_incoming_number_pool['can_create']) {
                                    $title = 'Purchase numbers for advertiser';

                                    if (!$raw['getIsDebtor']) {
                                        $a_options[$title] = $list_buttons[$title];
                                        $a_options[$title]['href'] = url_for(
                                            'advertiser_incoming_number_pool_new',
                                            array('advertiser_id' => $id)
                                        );
                                    } else {
                                        $a_options['Cannot purchase numbers for Debtor'] = $list_buttons[$title];
                                    }

                                    $legend_buttons[$title] = & $list_buttons[$title];
                                }

                                if ($permissions_user['can_create']) {
                                    $a_options['New user'] = $list_buttons['New user'];
                                    $a_options['New user']['href'] = url_for(
                                        'customer_advertiser_user_new', array('advertiser_id' => $id)
                                    );

                                    $legend_buttons['New user'] = & $list_buttons['New user'];
                                }

                                if ($permissions['can_update']) {
                                    $a_options['Edit'] = $list_buttons['Edit'];
                                    $a_options['Edit']['href'] = url_for('advertiser_edit', array('id' => $id));

                                    $legend_buttons['Edit'] = & $list_buttons['Edit'];
                                }

                                if ($permissions['can_delete']) {
                                    $a_options['Delete'] = link_to_ajax_delete(
                                        '',
                                        url_for('advertiser_delete', array('id' => $id)),
                                        array(
                                            'title'     => 'Delete',
                                            'class'     => 'icon delete',
                                            'after_url' => $urlAfter,
                                        )
                                    );

                                    $legend_buttons['Delete'] = & $list_buttons['Delete'];
                                }
                            }

                            echo icon_links($a_options, array('class' => 'icon-links clearfix'), false);
                        ?>
                    </td>
                </tr>
                <?php ++$i; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php
    include_partial(
         'global/legend',
          array(
            'legend_buttons'  => $legend_buttons,
          )
    ); 
?>

<?php echo $pagerHtmlString; ?>