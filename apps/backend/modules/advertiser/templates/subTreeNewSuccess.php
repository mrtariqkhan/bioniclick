<h1>Create new advertiser for "<?php echo $parent_company; ?>" company</h1>

<div class="inner">

    <?php $parentCompanyId = $parent_company->getId(); ?>
    <?php $urlAction = url_for('sub_tree_advertiser_create', array('parent_company_id' => $parentCompanyId)); ?>
    <?php $urlAfter = url_for('sub_tree_advertiser_index', array('parent_company_id' => $parentCompanyId)); ?>

    <?php $reloadTreeKey = TreeNodeBranching::generateTreeNodeKeyAdvertisers($parent_company->getRawValue()); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'submit_name'       => 'Create',
                'back_url'          => $urlAfter,
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel'),
                'form_special_data' => $reloadTreeKey,
            )
        );
    ?>
</div>
