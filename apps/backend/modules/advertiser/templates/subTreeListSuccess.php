<?php 
    include_partial(
        'subTreeList',
        array(
            'pager'                 => $pager,
            'branch_data'           => $branch_data,
            'permissions'           => $permissions,
            'permissions_user'      => $permissions_user,
            'permissions_campaign'  => $permissions_campaign,
            'parent_company'        => $parent_company,
        )
    );
?>