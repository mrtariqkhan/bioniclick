<div class="inner">
    <h2>Advertiser List</h2>
    <div class="account-wrap clearfix">
        <div class="ajax-filter-container">

            <?php if ($permissions['can_filter']): ?>
                <div class="filter-form-wrap">
                    <h1>Search</h1>
                    <div class="filter-form-wraper">
                        <?php
                            include_partial(
                                'global/form_ajax',
                                array(
                                    'form'              => $form_filter,
                                    'action_url'        => url_for('advertiser_filter'),
                                    'submit_name'       => 'Go!',
                                    'back_url'          => '',
                                    'form_class_name'   => 'ajax-filter',
                                    'cancel_attributes' => array('class' => 'ajax-form-cancel')
                                )
                            );
                        ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="ajax-pagable">
                <?php
                    include_partial(
                        'list',
                        array(
                            'pager'             => $pager,
                            'branch_data'       => $branch_data,
                            'permissions'       => $permissions,
                            'permissions_user'  => $permissions_user,
                            'permissions_advertiser_incoming_number_pool'   => $permissions_advertiser_incoming_number_pool,
                            'bionic_company_free_numbers'                   => $bionic_company_free_numbers,
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</div>

<?php if ($permissions['can_create']): ?>
    <?php if ($packages_amount > 0): ?>
        <?php
            $a_options = array();

            $url_new = url_for('advertiser_new');
            $a_options['New'] = array(
                'href'  => $url_new,
                'class' => 'icon new ajax-form-load',
            );

            echo icon_links($a_options);
        ?>
    <?php else: ?>
        You cannot create new advertisers because Your company has no packages. Create package firstly.
    <?php endif; ?>
<?php endif; ?>
