<?php

/**
 * advertiser actions.
 *
 * @package    bionic
 * @subpackage advertiser
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class advertiserActions extends GeneralApplyActions {

    public function preExecute() {

        $this->tableName = 'Advertiser';

        parent::preExecute();
    }

    public function executeIndex(sfWebRequest $request) {

        $this->executeList($request);
        $this->setTemplate('index');

        $filterValues = $this->getFilter();
        $this->form_filter = $this->getFilterForm();
        $this->form_filter->setDefaults($filterValues);

        $this->packages_amount = PackageTable::countByUser($this->user);
    }

    public function executeList(sfWebRequest $request) {

        $filterValues = $this->getFilter();
        $filterForm = $this->getFilterForm();
        $queryParams = $filterForm->getFilteredValues($filterValues, true);

        $specialQuery = AdvertiserTable::createQueryIndex($this->user, $queryParams);
        $this->pager = parent::initPager($request, $specialQuery);
        $list = $this->pager->getResults();

        $objectFieldsHeaders = array(
            'getName'               => 'Name',
            'getUrl'                => 'Url',
            'getDefaultPhoneNumber' => 'Contact number',
            'getPackage'            => 'Package',
            'getIsDebtor'           => 'Is Debtor',
        );
        WasDeletedHelper::addHeader($this->user, $objectFieldsHeaders);

        $escapeHeaders = array(
            CompanyTable::$LEVEL_NAME_ADVERTISER,
        );
        $branchHelper = new BranchHelper();
        $this->branch_data = $branchHelper->makeRaws($this->user, $list, $objectFieldsHeaders, $escapeHeaders);

        //Permissions for user managing at advertiser list
        $securityHelperUser = new SecurityHelper($this, 'sfGuardUser');
        $this->permissions_user = $securityHelperUser->checkBasePermissions();

        //Permissions for advertiserIncomingNumberPool managing at company list
        $securityHelperAINP = new SecurityHelper($this, 'advertiserIncomingNumberPool');
        $this->permissions_advertiser_incoming_number_pool = $securityHelperAINP->checkBasePermissions();

        $this->bionic_company_free_numbers = BionicIncomingNumberPoolTable::calcAvailibleToPurchaseNumbers();
    }

    public function executeSubTreeIndex(sfWebRequest $request) {

        $this->executeSubTreeList($request);
        $this->setTemplate('subTreeIndex');
//
//        $filterValues = $this->getFilter();
//        $this->form_filter = $this->getFilterForm();
//        $this->form_filter->setDefaults($filterValues);

        $this->packages_amount = PackageTable::countByUser($this->user);
        $this->is_company_user = $this->user->isCompanyUser();
        $this->level_user = $this->user->getLevel();
    }

    public function executeSubTreeList(sfWebRequest $request) {

//        $filterValues = $this->getFilter();
//        $filterForm = $this->getFilterForm();
//        $queryParams = $filterForm->getFilteredValues($filterValues, true);
        $queryParams = array();

        $company = $request->getParameter('parent_company_id', null);
        $parentCompany = CompanyTable::getInstance()->find($company);
        $this->forward404If(empty($parentCompany), 'Company has not been found.');

        $this->checkIsAllowableObject($parentCompany, false);

        $specialQuery = AdvertiserTable::createQueryFirstByParentCompanyAndUser($this->user, $parentCompany, $queryParams);
        $this->pager = parent::initPager($request, $specialQuery);
        $list = $this->pager->getResults();

        $objectFieldsHeaders = array(
            'getName'               => 'Name',
            'getUrl'                => 'Url',
            'getDefaultPhoneNumber' => 'Contact number',
            'getPackage'            => 'Package',
        );
        WasDeletedHelper::addHeader($this->user, $objectFieldsHeaders);

        $escapeHeaders = array(
            CompanyTable::$LEVEL_NAME_ADVERTISER,
        );
        $branchHelper = new BranchHelper();
        $this->branch_data = $branchHelper->makeRaws($this->user, $list, $objectFieldsHeaders, $escapeHeaders);

        //Permissions for user managing at advertiser list
        $securityHelperUser = new SecurityHelper($this, 'sfGuardUser');
        $this->permissions_user = $securityHelperUser->checkBasePermissions();

        //Permissions for campaigns at advertiser list
        $securityHelperCampaign = new SecurityHelper($this, 'campaign');
        $this->permissions_campaign = $securityHelperCampaign->checkBasePermissions();

        $this->parent_company = $parentCompany;
    }

    public function executeSubTreeNew(sfWebRequest $request) {

        $companyId = $request->getParameter('parent_company_id', null);
        $parentCompany = CompanyTable::getInstance()->find($companyId);
        $this->forward404If(empty($parentCompany), "Company has not been found");

        $this->checkIsAllowableObject($parentCompany);

        $this->form = $this->configureSubTreeForm($parentCompany);
        $this->parent_company = $parentCompany;
    }

    public function executeSubTreeCreate(sfWebRequest $request) {

        $companyId = $request->getParameter('parent_company_id', null);
        $parentCompany = CompanyTable::getInstance()->find($companyId);
        $this->forward404If(empty($parentCompany), "Company has not been found");

        $this->checkIsAllowableObject($parentCompany);

        $form = $this->configureSubTreeForm($parentCompany);

        return $this->processAjaxForm($request, $form);
    }

    public function executeSubTreeEditList(sfWebRequest $request) {

        $this->executeEdit($request);
        $this->setTemplate('subTreeEditList');
    }

    public function executeSubTreeEdit(sfWebRequest $request) {

        $this->executeEdit($request);
        $this->setTemplate('subTreeEdit');
    }

    public function executeShow(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object, false);

        $advertiserId = $request->getParameter('id');

        $specialQuery = AdvertiserIncomingNumberPoolTable::createQueryByAdvertiserId(
            $advertiserId
        );
        $this->pagerIncoming = parent::initPager($request, $specialQuery);

        $this->advertiser = $object;
        $this->is_company_user = $this->user->isCompanyUser();

        $securityHelperFinance = new SecurityHelper($this, 'finance');
        $this->permissions_finance = $securityHelperFinance->checkBasePermissions();
        $this->is_debtor = $object->getIsDebtor();
    }

    public function executeNew(sfWebRequest $request) {
        $this->form = $this->configureForm();
    }

    public function executeCreate(sfWebRequest $request) {

        $form = $this->configureForm();

        return $this->processAjaxForm($request, $form);
    }

    public function executeEdit(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $this->form = $this->configureForm($object);

        $this->is_company_user      = $this->user->isCompanyUser();
        $this->is_advertiser_user   = $this->user->isAdvertiserUser();
    }

    public function executeUpdate(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $form = $this->configureForm($object);

        return $this->processAjaxForm($request, $form);
    }

    public function executeDelete(sfWebRequest $request) {
        return $this->processAjaxDelete($request);
    }

    protected function deleteObject($object) {
        $object->deleteByUser($this->user);
    }

    public function executeEditCurrent(sfWebRequest $request) {
        $this->form = $this->configureCurrentForm();
    }

    public function executeUpdateCurrent(sfWebRequest $request) {

        $form = $this->configureCurrentForm();

        return $this->processAjaxForm($request, $form);
    }

    public function executeFilter(sfWebRequest $request) {
        $filterForm = $this->getFilterForm();
        return $this->processAjaxFilter($request, $filterForm);
    }

    protected function getFilterForm() {
        $options = $this->getBranchFilterOptions();
        return new AdvertiserFilterForm(null, $options);
    }

    protected function getBranchFilterOptions() {

        $options = parent::getBranchFilterOptions();
        unset($options[CompanyTable::$LEVEL_NAME_ADVERTISER]);

        return $options;
    }

    protected function saveValidForm($validForm) {

        $wasNew = $validForm->isNew();

        $savedObject = $validForm->save();

        if ($wasNew && !sfGuardUserProfileTable::ifUseSimpleUserCreation()) {
            try {
                $advertiser = $validForm->getObject();
                $user = $advertiser->getAdmin();
                $profile = $user->getProfile();
                $this->sendVerificationMail($profile);
            } catch (Exception $e) {
                $advertiser = $validForm->getObject();
                $user = $advertiser->getAdmin();
                $user->delete();
                //TODO:: think if it needs to delete advertiser also
                throw $e;
            }
        }

        return $savedObject;
    }

    protected function configureCurrentForm() {

        $object = $this->user->getAdvertiser();

        $formOptions = array();

        $form = new AdvertiserFormCurrentEdit($object, $formOptions);
        return $form;
    }

    protected function configureForm($object = null) {

        $formOptions = array(
            AdvertiserForm::OPTION_PARENT_COMPANY => $this->user->getCompany(),
        );

        if (empty($object)) {
            $object = new Advertiser();
            $object->setCompanyId($this->user->getCompanyId());
        }

        $form = new AdvertiserForm($object, $formOptions);
        return $form;
    }

    protected function configureSubTreeForm($parentCompany, $object = null) {

        $formOptions = array(
            AdvertiserForm::OPTION_PARENT_COMPANY => $parentCompany,
        );

        if (empty($object)) {
            $object = new Advertiser();
            $object->setCompanyId($parentCompany->getId());
        }

        $form = new AdvertiserForm($object, $formOptions);
        return $form;
    }
}