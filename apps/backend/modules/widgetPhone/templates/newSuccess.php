<?php $object = $form->getObject(); ?>

<h1>
    New Phone Widget
</h1>

<div class="inner">

    <?php $urlAction = url_for('widget_phone_create', array('combo_id'=> $object->getComboId())); ?>
    <?php $urlAfter = url_for('widget_phone', array('combo_id'=> $object->getComboId())); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'submit_name'       => 'Create',
                'back_url'          => $urlAfter,
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel')
            )
        );
    ?>
</div>
