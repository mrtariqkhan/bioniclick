<?php use_helper('Link'); ?>

<?php if ($permissions['can_read']): ?>
    <div>

        <?php include_partial('global/flash_message', array('flash_name' => 'url_is_underfined', 'class' => 'flash-notice')); ?>

        <?php
            include_partial(
                'global/breadCrumb',
                array(
                    'object' => $combo,
                    'title'  => 'Phone widget list',
                )
            );
        ?>

        <h3>
            URL:
            <?php $url = $combo->buildUrl(); ?>
            <a href="<?php echo $url;?>" target='_blank'>
                <?php echo $url;?>
            </a>
        </h3>

        <?php $urlAfter = url_for('widget_phone', array('combo_id' => $combo->getId())); ?>

        <div class="table-block">
            <table class="table">
                <thead>
                    <tr>
                        <th class="first">&nbsp;</th>
                        <th>Html type</th>
                        <th>Physical phone</th>
                        <th>Html id</th>
                        <th>Html content1</th>
                        <th>Html content2</th>
                        <th>Number format</th>
                        <?php if ($need_was_deleted_column) : ?>
                            <th><?php echo WasDeletedHelper::TITLE; ?></th>
                        <?php endif; ?>
                        <th class="last">&nbsp; </th>
                    </tr>
                </thead>

                <tbody>
                    <?php
                        $list_buttons = array(
                            'Edit' => array(
                                'class' => 'icon edit ajax-load'
                            ),
                            'Delete' => array(
                                'class' => 'icon delete'
                            )
                        );

                        $legend_buttons = array();
                    ?>
                    <?php $i = $pager->getFirstIndice(); ?>
                    <?php $list = $pager->getResults(); ?>
                    <?php $list = $list->getRawValue(); ?>
                    <?php foreach ($list as $object): ?>
                        <?php $id = $object->getId(); ?>
                        <tr class="<?php echo ($i%2) ? 'odd' : 'even'; ?>">
                            <td><?php echo $i; ?></td>
                            <td><?php echo $object->getHtmlType(); ?></td>
                            <td><?php echo $object->getPhysicalPhoneNumber();?></td>
                            <td><?php echo $object->getHtmlId(); ?></td>
                            <td><?php echo htmlentities($object->getHtmlContent_1()); ?></td>
                            <td><?php echo htmlentities($object->getHtmlContent_2()); ?></td>
                            <td><?php echo $object->getNumberFormat(); ?></td>

                            <?php if ($need_was_deleted_column) : ?>
                                <td><?php echo $object->getWasDeletedString(); ?></td>
                            <?php endif; ?>

                            <td class="last">
                                <?php
                                    $a_options = array();

                                    $ifShowUpdateLinks = WasDeletedHelper::ifCanUpdate($object);
                                    if ($ifShowUpdateLinks) {
                                        if ($permissions['can_update']) {
                                            $a_options['Edit'] = $list_buttons['Edit'];
                                            $a_options['Edit']['href'] = url_for(
                                                'widget_phone_edit',
                                                array('id' => $object->getId())
                                            );

                                            $legend_buttons['Edit'] = & $list_buttons['Edit'];
                                        }
                                        if ($permissions['can_delete']) {
                                            $a_options['Delete'] = link_to_ajax_delete(
                                                '',
                                                url_for('widget_phone_delete', array('id' => $id)),
                                                array(
                                                    'title'     => 'Delete',
                                                    'class'     => 'icon delete',
                                                    'after_url' => $urlAfter,
                                                )
                                            );

                                            $legend_buttons['Delete'] = & $list_buttons['Delete'];
                                        }
                                    }

                                    echo icon_links($a_options, array('class' => 'icon-links clearfix'), false);
                                ?>
                            </td>
                        </tr>
                        <?php $i++; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
        
        <?php
            include_partial(
                 'global/legend',
                  array(
                    'legend_buttons'  => $legend_buttons,
                  )
            ); 
        ?>

            <?php
                $a_options = array();

                $ifShowUpdateLinksParent = WasDeletedHelper::ifCanUpdate($combo->getRawValue());
                if ($ifShowUpdateLinksParent) {
                    if ($permissions['can_update']) {
                        $a_options['New']  = array(
                            'href' => url_for(
                                'widget_phone_new',
                                array(
                                    'combo_id' => $combo->getId()
                                )
                            ),
                            'class' => "icon new ajax-load",
                        );
                    }

                    if ($permissions['can_update']) {
                        $a_options['Visual editor'] = array(
                            'href' => url_for('widget_phone_visual_page', array('combo_id' => $combo->getId())),
                            'class' => "icon visual-editor"
                        );
                    }
                }

                if ($permissions['can_read']) {
                    $a_options['Back'] = array(
                        'href' => url_for(
                            'combos',
                            array(
                                'page_id' => $page->getId()
                            )
                        ),
                        'class' => "icon back ajax-load"
                    );
                }

                echo icon_links($a_options);
            ?>
        </div>
    </div>
<?php endif; ?>