<?php
    include_partial(
        'widget/wveInjection',
        array(
            'base_page_url'     => $base_page_url,
            'editor_data'       => $editor_data,
            'html_parts'        => $html_parts,
            'forms'             => $forms,
        )
    );
?>