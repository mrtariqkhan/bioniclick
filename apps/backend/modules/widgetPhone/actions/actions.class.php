<?php

/**
 * phoneWidget actions.
 *
 * @package    bionic
 * @subpackage phoneWidget
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class widgetPhoneActions extends GeneralWidgetActions {

    public function  preExecute() {

        $this->tableName = 'WidgetPhone';
        parent::preExecute();
    }
}
