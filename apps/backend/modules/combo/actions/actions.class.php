<?php

/**
 * combo actions.
 *
 * @package    bionic
 * @subpackage combo
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class comboActions extends GeneralActions {

    public function preExecute() {

        $this->tableName = 'Combo';
        parent::preExecute();

        $this->getResponse()->addStylesheet(
            '/css/project/reset.css',
            'last'
        );
    }

   /**
    * Executes index action
    *
    * @param sfRequest $request A request object
    */
    public function executeIndex(sfWebRequest $request) {

        $this->executeList($request);
        $this->setTemplate('index');
    }
    
    public function executeShow(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object, false);

        $this->combo = $object;
        
        $this->widgetPhoneCount = WidgetPhoneTable::getInstance()->getIndexQueryByUserAndComboId($this->user, $object->getId())->count();
        $this->widgetHTMLCount = WidgetTable::getInstance()->getIndexQueryByUserAndComboId($this->user, $object->getId())->count();
    }

    public function executeList(sfWebRequest $request) {

        $pageId = $request->getParameter('page_id');
        $page = PageTable::getInstance()->find($pageId);
        $this->forward404If(
            empty($page),
            sprintf('Object page does not exist (%s).', $pageId)
        );
        $this->checkIsAllowableObject($page, false);

        $this->campaign = $page->getCampaign();
        $this->page = $page;

        $filterValues = $this->getFilter();
        $this->form_filter = new ComboFormFilter($filterValues);
        $filterQuery = $this->form_filter->buildQuery($filterValues);

        $specialQuery = ComboTable::getIndexQueryByUserAndPageId($this->user, $pageId, $filterQuery);
        $this->pager = parent::initPager($request, $specialQuery);
        $this->need_was_deleted_column = WasDeletedHelper::ifNeedWasDeletedColumn($this->user);

        $securityHelperWidget = new SecurityHelper($this, 'widget');
        $this->permissions_widget = $securityHelperWidget->checkBasePermissions();

        $securityHelperWidgetPhone = new SecurityHelper($this, 'widgetPhone');
        $this->permissions_widget_phone = $securityHelperWidgetPhone->checkBasePermissions();
    }

    public function executeFilter(sfWebRequest $request) {

        $filterForm = new ComboFormFilter();

        return $this->processAjaxFilter($request, $filterForm);
    }

    public function executeNew(sfWebRequest $request) {

        $pageId = $request->getParameter('page_id');
        $page = PageTable::getInstance()->findOneById($pageId);
        $this->forward404If(
            empty($page),
            sprintf('Object page does not exist (%s).', $pageId)
        );
        $this->checkIsAllowableObject($page);

        $object = new Combo();
        $object->setPage($page);

        $this->form = new ComboForm($object);
    }

    public function executeCreate(sfWebRequest $request) {

        $form = new ComboForm();

        return $this->processAjaxForm($request, $form);
    }

    public function executeEdit(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $this->form = new ComboForm($object);
    }

    public function executeUpdate(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $form = new ComboForm($object);

        return $this->processAjaxForm($request, $form);
    }

    public function executeDelete(sfWebRequest $request) {
        return $this->processAjaxDelete($request);
    }
}
