<?php use_helper('Link'); ?>
<?php use_helper('Pager'); ?>


<?php if ($permissions['can_read']): ?>

    <?php
        include_partial(
            'global/breadCrumb',
            array(
                'object' => $page
            )
        );
    ?>

    <h2>Combo List</h2>
    <div class="account-wrap clearfix">

        <div class="ajax-filter-container">
            <div class="filter-form-wrap">
                <h1>Search</h1>
                <div class="filter-form-wraper">
                    <?php
                        include_partial(
                            'global/form_ajax',
                            array(
                                'form'              => $form_filter,
                                'action_url'        => url_for('combo_filter'),
                                'submit_name'       => 'Go!',
                                'back_url'          => '',
                                'form_class_name'   => 'ajax-filter',
                                'cancel_attributes' => array('class' => 'ajax-form-cancel')
                            )
                        );
                    ?>
                </div>
            </div>
            <div class="ajax-pagable">
                <?php
                    include_partial(
                        'list',
                        array(
                            'pager'                     => $pager,
                            'page'                      => $page,
                            'permissions'               => $permissions,
                            'permissions_widget'        => $permissions_widget,
                            'permissions_widget_phone'  => $permissions_widget_phone,
                            'need_was_deleted_column'   => $need_was_deleted_column,
                        )
                    );
                ?>
            </div>

            <?php
                $a_options = array();

                $ifShowUpdateLinksParent = WasDeletedHelper::ifCanUpdate($page->getRawValue());
                if ($ifShowUpdateLinksParent) {
                    if ($permissions['can_update']) {
                        $a_options['New'] = array(
                            'href' => url_for('combo_new', array('page_id' => $page->getId())),
                            'class' => "icon new ajax-load"
                        );
                    }
                }

                if ($permissions['can_read']) {
                    $a_options['Back'] = array(
                        'href' => url_for('pages', array('campaign_id' => $campaign->getId())),
                        'class' => "icon back ajax-load"
                    );
                }
                echo icon_links($a_options);
            ?>
        </div>
    </div>
<?php endif; ?>