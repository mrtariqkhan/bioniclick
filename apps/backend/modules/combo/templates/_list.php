<?php use_helper('Link'); ?>
<?php use_helper('Pager'); ?>

<?php $reloadTreeKey = TreeNodeCampaign::generateTreeNodeKeyPage($page->getRawValue()); ?>

<?php
    $pagerHtmlString = pager(
        $pager->getRawValue(),
        sfConfig::get('app_pager_max_per_pages'),
        url_for('combos_list', array('page_id' => $page->getId())),
        null,
        'ajax-pager'
    );
?>

<p>
    <?php 
        include_partial(
            'global/pager_list_header',
            array(
                'pager' => $pager,
                'pagerHtmlString' => $pagerHtmlString
            )
        ); 
    ?>
</p>

<div class="table-block">
    <table class="table">
        <thead>
            <tr>
                <th class="first">&nbsp;</th>
                <th>Name</th>
                <th>Api Key</th>
                <?php if ($need_was_deleted_column) : ?>
                    <th><?php echo WasDeletedHelper::TITLE; ?></th>
                <?php endif; ?>
                <th class="last"></th>
            </tr>
        </thead>
        <tbody>
            <?php
                $list_buttons = array(
                    'Html Widgets' => array(
                        'class' => 'icon html-widgets ajax-load'
                    ),
                    'Phone Widgets' => array(
                        'class' => 'icon phone-widgets ajax-load'
                    ),
                    'Edit' => array(
                        'class' => 'icon edit ajax-load'
                    ),
                    'Delete' => array(
                        'class' => 'icon delete'
                    )
                );

                $legend_buttons = array();
            ?>
            <?php $i = $pager->getFirstIndice(); ?>
            <?php $list = $pager->getResults(); ?>
            <?php $list = $list->getRawValue(); ?>
            <?php foreach ($list as $object): ?>
                <tr class="<?php echo ($i%2) ? 'odd' : 'even'; ?>">
                    <td><?php echo $i; ?></td>

                    <td><?php echo $object->getName(); ?></td>
                    <td><?php echo $object->getApiKey(); ?></td>

                    <?php if ($need_was_deleted_column) : ?>
                        <td><?php echo $object->getWasDeletedString(); ?></td>
                    <?php endif; ?>

                    <td>
                        <?php $id = $object->getId(); ?>
                        <?php
                            $a_options = array();

                            if ($permissions_widget['can_read']) {
                                $a_options['Html Widgets'] = $list_buttons['Html Widgets'];
                                $a_options['Html Widgets']['href'] = url_for('widget', array('combo_id' => $id));

                                $legend_buttons['Html Widgets'] = & $list_buttons['Html Widgets'];
                            }

                            if ($permissions_widget_phone['can_read']) {
                                $a_options['Phone Widgets'] = $list_buttons['Phone Widgets'];
                                $a_options['Phone Widgets']['href'] = url_for('widget_phone', array('combo_id' => $id));

                                $legend_buttons['Phone Widgets'] = & $list_buttons['Phone Widgets'];
                            }

                            $ifShowUpdateLinks = WasDeletedHelper::ifCanUpdate($object);
                            if ($ifShowUpdateLinks) {
                                if ($permissions['can_update']) {
                                    $a_options['Edit'] = $list_buttons['Edit'];
                                    $a_options['Edit']['href'] = url_for('combo_edit', array('id' => $id));

                                    $legend_buttons['Edit'] = & $list_buttons['Edit'];
                                }

                                if ($permissions['can_delete']) {
                                    $a_options['Delete'] = link_to_ajax_delete(
                                        '',
                                        url_for('combo_delete', array('id' => $id)),
                                        array(
                                            'title'             => 'Delete',
                                            'class'             => 'icon delete',
                                            'special_form_data' => $reloadTreeKey
                                        )
                                    );

                                    $legend_buttons['Delete'] = & $list_buttons['Delete'];
                                }
                            }

                            echo icon_links($a_options, array('class' => 'icon-links clearfix'), false);
                        ?>
                    </td>
                </tr>
                <?php ++$i; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php
    include_partial(
         'global/legend',
          array(
            'legend_buttons'  => $legend_buttons,
          )
    ); 
?>

<?php echo $pagerHtmlString; ?>