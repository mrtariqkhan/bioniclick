<?php use_helper('Link'); ?>

<?php $id = $combo->getId(); ?>
<?php $ifShowUpdateLinks = WasDeletedHelper::ifCanUpdate($combo->getRawValue()); ?>

<?php
    include_partial(
        'global/breadCrumb',
        array(
            'object' => $combo
        )
    );
?>

<?php
    $a_options = array();

    if ($ifShowUpdateLinks) {
        if ($permissions['can_update']) {
            $a_options['Edit combo'] = array(
                'href' => url_for('combo_edit', array('id' => $id)),
                'class' => 'icon edit ajax-load'
            );
        }
    }
    
    $a_options['HTML widgets (' . $widgetHTMLCount . ')'] = array(
        'href' => url_for('widget', array('combo_id' => $id)),
        'class' => 'icon html-widgets ajax-load'
    );
    
    $a_options['Phone widgets (' . $widgetPhoneCount . ')'] = array(
        'href' => url_for('widget_phone', array('combo_id' => $id)),
        'class' => 'icon phone-widgets ajax-load'
    );

    echo icon_links($a_options, array('class' => 'icon-links clearfix'));
?>
