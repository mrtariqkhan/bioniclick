<?php 
    include_partial(
        'list',
        array(
            'pager'                     => $pager,
            'page'                      => $page,
            'permissions'               => $permissions,
            'permissions_widget'        => $permissions_widget,
            'permissions_widget_phone'  => $permissions_widget_phone,
            'need_was_deleted_column'   => $need_was_deleted_column,
        )
    );
?>