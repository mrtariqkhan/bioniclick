
<?php $object = $form->getObject(); ?>

<?php $reloadTreeKey = TreeNodeCampaign::generateTreeNodeKeyPage($object->getPage()); ?>

<h1>New combo</h1>

<div class="inner">
    <?php $pageId = $object->getPageId(); ?>

    <?php $urlAction = url_for('combo_create'); ?>
    <?php $urlAfter = url_for('combos', array('page_id' => $pageId)); ?>
    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'back_url'          => $urlAfter,
                'submit_name'       => 'Create',
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel'),
                'form_special_data' => $reloadTreeKey
            )
        );
    ?>
</div>
