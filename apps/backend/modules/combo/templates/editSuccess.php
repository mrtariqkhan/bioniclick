
<?php $object = $form->getObject(); ?>

<?php $reloadTreeKey = TreeNodeCampaign::generateTreeNodeKeyPage($object->getPage()); ?>

<?php
    include_partial(
        'global/breadCrumb',
        array(
            'object' => $object,
            'title'  => 'Edit combo: ' . $object->getName(),
        )
    );
?>

<div class="inner">
    <?php $id = $object->getId(); ?>
    <?php $pageId = $object->getPageId(); ?>

    <?php $urlAction = url_for('combo_update', array('id' => $id)); ?>
    <?php $urlAfter = url_for('combos', array('page_id' => $pageId)); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'back_url'          => $urlAfter,
                'submit_name'       => 'Update',
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel'),
                'form_special_data' => $reloadTreeKey,
            )
        );
    ?>
</div>
