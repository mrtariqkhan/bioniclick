<?php

/**
 * campaignTree actions.
 *
 * @package    bionic
 * @subpackage campaignTree
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class campaignTreeActions extends TreeActions {

    public function preExecute() {

        parent::preExecute();

        $this->getResponse()->addStylesheet(
            '/css/project/tabs_accordion.css',
            'last'
        );
        $this->getResponse()->addStylesheet(
            '/css/project/reset.css',
            'last'
        );
        $this->getResponse()->addStylesheet(
            '/css/project/dynatree/ui.dynatree.css',
            'last'
        );
        $this->getResponse()->addStylesheet(
            '/css/project/dynatree/custom.dynatree.css',
            'last'
        );
        $this->getResponse()->addStylesheet(
            '/css/project/icon.css',
            'last'
        );
        $this->getResponse()->addStylesheet(
            '/css/project/table.css',
            'last'
        );
        $this->getResponse()->addStylesheet(
            '/css/project/filter_block.css',
            'last'
        );
        $this->getResponse()->addStylesheet(
            '/css/project/form_general.css',
            'last'
        );
        $this->getResponse()->addStylesheet(
            '/css/project/campaign.css',
            'last'
        );
        $this->getResponse()->addStylesheet(
            '/css/project/bionic_form_ajax_error.css',
            'last'
        );
        $this->getResponse()->addStylesheet(
            '/css/project/financial-position.css',
            'last'
        );
        $this->getResponse()->addStylesheet('/css/project/datepicker.css', 'last');
        
        $this->getResponse()->addJavascript('/js/jquery/daterangepicker/js/daterangepicker.jQuery.js', 'last');
        
        $this->getResponse()->addJavascript('/js/project/editCompany.js', 'last');

        $this->getResponse()->addJavascript(
            '/js/jquery/jquery.cookie.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/jquery/jquery.dynatree.min.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/jquery/jquery.form.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/project/ajax_form.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/project/ajax_campaign_tree.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/project/ajax_delete.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/project/paging_simple.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/project/getSnippet.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/project/ajax_filter.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/project/orders.js',
            'last'
        );

        $this->getResponse()->addJavascript(
            '/js/jquery/livequery/jquery.livequery.js',
            'last'
        );

        $this->getResponse()->addJavascript(
            '/js/project/ajax_links.js',
            'last'
        );

        $this->getResponse()->addJavascript(
            '/js/project/finance_position.js',
            'last'
        );
        $this->getResponse()->addStylesheet(
            '/css/project/datepicker.css', 
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/jquery/daterangepicker/js/daterangepicker.jQuery.js', 
            'last'
        );


        // upload audio files - custom redirect msg
        /*$this->getResponse()->addJavascript(
            '/js/plupload/js/plupload.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/plupload/js/plupload.html5.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/plupload/js/plupload.flash.js',
            'last'
        );
         */
        $this->getResponse()->addJavascript(
            '/js/plupload/js/plupload.full.js',
            'last'
        );
        
        
        $this->getResponse()->addJavascript(
            '/js/project/editCampaign.js',
            'last'
        );
        
        
        // audio player
        $this->getResponse()->addJavascript(
            '/js/jquery/jPlayer/jquery.jplayer.min.js',
            'last'
        );
        
        $this->getResponse()->addJavascript(
            '/js/project/audio_player.js',
            'last'
        );

        $this->getResponse()->addStylesheet(
            '/css/project/autocompleter.css',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/sfFormExtraPlugin/js/jquery.autocompleter.js',
            'last'
        );


        
        $this->getUser()->setAttribute('menu', 'campaigns_tree');
    }

    /**
     * Executes index action
     *
     * @param sfRequest $request A request object
     */
    public function executeIndex(sfWebRequest $request) {
        $this->doIndex($request);
    }

    public function executeIndexSelectedCampaign(sfWebRequest $request) {
        $this->doIndex($request, TreeNode::NODE_TYPE_CAMPAIGN);
    }

    public function executeIndexSelectedPage(sfWebRequest $request) {
        $this->doIndex($request, TreeNode::NODE_TYPE_PAGE);
    }

    public function executeIndexSelectedWidget(sfWebRequest $request) {
        $this->doIndex($request, TreeNode::NODE_TYPE_WIDGET);
    }

    public function executeIndexSelectedWidgetPhone(sfWebRequest $request) {
        $this->doIndex($request, TreeNode::NODE_TYPE_WIDGET_PHONE);
    }

    public function executeAjaxGetTreeData(sfWebRequest $request) {

        //NOTE:: do not delete this params.
        // 1). They need for ajax
        // 2). they need to be checked.
        $selectedType = $request->getParameter('selected_type', null);
        /*
        $tableName = $this->getTableNameByType($selectedType);
        $selectedId = empty($tableName) ? null : $this->getCheckedObjectId($request, $tableName, 'selected_id');
        */
        
        $data = $this->configureTree();
        
        return $this->processAjaxResult($request, $data, false);
    }

    public function executeAjaxGetLazyDataCampaign(sfWebRequest $request) {

        $type  = $request->getParameter('type', null);

        $tableName = $this->getTableNameByType($type);
        $lazyTreeParam = empty($tableName) ? null : $this->getCheckedObject($request, $tableName, 'parameter');

        $methodName = "configureChildrenFor{$type}";
        if (method_exists($this, $methodName)) {
            $data = $this->$methodName($lazyTreeParam);
        } else {
            $data = array();
        }

        return $this->processAjaxResult($request, $data, false);
    }

    protected function getCheckedObjectId(
        sfWebRequest $request,
        $tableName,
        $parameterIdName = 'id'
    ) {

        $object = $this->getCheckedObject($request, $tableName, $parameterIdName);
        $id = empty($object) ? null : $object->getId();

        return $id;
    }

    protected function getCheckedObject(
        sfWebRequest $request,
        $tableName,
        $parameterIdName = 'id'
    ) {

        $id = $request->getParameter($parameterIdName, null);
        if (empty($tableName) || empty($id)) {
            return null;
        }

        $object = Doctrine::getTable($tableName)->findOneBy('id', $id);
        $this->forward404If(
            empty($object),
            sprintf('Object %s does not exist (%s).', $tableName, $id)
        );
        $this->checkIsAllowableObject($object, false);

        return $object;
    }

    protected function doIndex(sfWebRequest $request, $typeTreeNode = '') {

        //TODO:: think about forced node.
        $tableName = $this->getTableNameByType($typeTreeNode);

        $selectedId = empty($tableName) ? null : $this->getCheckedObjectId($request, $tableName);
        $selectedType = $typeTreeNode;

        $routeName = 'campaign_tree_get_tree_data';
        $params = array();
        if (!empty($selectedType) && !empty($selectedId)) {
            $routeName = 'campaign_tree_get_tree_data_selected_node';
            $params = array(
                'selected_type' => $selectedType,
                'selected_id'   => $selectedId
            );
        }
        $this->data_load_url        = $this->generateUrl($routeName, $params);
        $this->data_lazy_load_url   = $this->generateUrl('campaign_tree_lazy_level_campaign');
        $this->selected_key = TreeNode::generateKey($typeTreeNode, $selectedId);

        $this->setTemplate('index');
    }

    public function executeGetFilteredTreeData(sfWebRequest $request) {

        $campaignNamePart = $request->getParameter('campaign_name_part', '');
        $data = $this->configureTree($campaignNamePart);

        return $this->processAjaxResult($request, $data, false);
    }

    public function executeSubTreeFilterPage(sfWebRequest $request) {

//        if ($this->user->isCompanyUser()) {
//            $company = $this->user->getCompany();
//        }
//
//        if ($this->user->isAdvertiserUser()) {
//            $advertiser = $this->user->getAdvertiser();
//        }
    }
}
