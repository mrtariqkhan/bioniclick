<?php slot('leftBar') ?>
    <div
        class="campaign-tree"
        id="campaign-tree"
        data_url="<?php echo $data_load_url; ?>"
        data_lazy_url="<?php echo $data_lazy_load_url; ?>"
        selected_key="<?php echo $selected_key; ?>"
    >
        &nbsp;
    </div>
<?php end_slot() ?>

<?php include_partial('global/ajax_error', array()); ?>

<?php include_partial('global/ajax_loader'); ?>

<div class="campaign-ajax-loadable ajax-blocked">
    &nbsp;
</div>
