<?php

/**
 * api actions.
 *
 * @package    bionic
 * @subpackage api
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class apiActions extends GeneralActions
{	
	public function preExecute(){
		parent::preExecute();
	}

	public function executeAgencyAdvertisers(sfWebRequest $request){
		$user = $this->user;
		$advertisersIds = AdvertiserTable::findAdvertiserIdsInBranch($user, array());
		$advertisersNames = array();
		foreach ($advertisersIds as $advertiserId){
			$advertiser = AdvertiserTable::findById($advertiserId);
			array_push($advertisersNames, $advertiser->getName());
		}
	 	return $this->renderText(json_encode($advertisersNames));
	}

	public function executeAudioPlayer(sfWebRequest $request){
		$this->path = $request->getParameter('path');
	}
}
