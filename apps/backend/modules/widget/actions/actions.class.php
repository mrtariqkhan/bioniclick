<?php

/**
 * widget actions.
 *
 * @package    bionic
 * @subpackage widget
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class widgetActions extends GeneralWidgetActions {

    public function  preExecute() {

        $this->tableName = 'Widget';
        parent::preExecute();
    }
}