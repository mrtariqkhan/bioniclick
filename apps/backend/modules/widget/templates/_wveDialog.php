
<?php $editorData = $editorData->getRawValue(); ?>

<?php $forms = $forms->getRawValue(); ?>
<?php $formWidgetEdit       = $forms['form_widget_edit']; ?>
<?php $formWidgetPhoneEdit  = $forms['form_widget_phone_edit']; ?>
<?php $formWidgetNew        = $forms['form_widget_new']; ?>
<?php $formWidgetPhoneNew   = $forms['form_widget_phone_new']; ?>

<?php
    $editButtonsInfo = array(
        'Preview'   => array('class' => 'WVE-do-preview'),
        'Save'      => array('class' => 'WVE-do-save', 'type' => 'submit'),
    );
?>
<?php
    $newButtonsInfo = array(
        'Preview'   => array('class' => 'WVE-do-preview'),
        'Save'      => array('class' => 'WVE-do-save', 'type' => 'submit'),
//        'Cancel'    => array('class' => 'WVE-do-cancel'),
    );
?>


<div
    id="wve-bionic-dialog"
    class="WVE-ajax-blocked"
    editor_data="<?php echo htmlentities(json_encode($editorData)); ?>"
    style="display: none;"
>

    <?php include_partial('global/ajax_error', array('className' => 'WVE-error-message')); ?>

    <div id="wve-bionic-wrapper">
        <div id="wve-bionic-widget-select">
            <label for="wve-bionic-widget-list">ID List</label>
            <select id="wve-bionic-widget-list" size="10"></select>
        </div>

        <div id="wve-bionic-forms">

            <?php
                include_partial(
                    'widget/wveForm',
                    array(
                        'form'              => $formWidgetEdit,
                        'buttons_info'      => $editButtonsInfo,
                        'form_title'        => 'Edit HTML Widget',
                        'form_container_id' => 'wve-bionic-edit-widget-html',
                    )
                );
            ?>

            <?php
                include_partial(
                    'widget/wveForm',
                    array(
                        'form'              => $formWidgetPhoneEdit,
                        'buttons_info'      => $editButtonsInfo,
                        'form_title'        => 'Edit Phone Widget',
                        'form_container_id' => 'wve-bionic-edit-widget-phone',
                    )
                );
            ?>

            <?php
                include_partial(
                    'widget/wveForm',
                    array(
                        'form'              => $formWidgetNew,
                        'buttons_info'      => $newButtonsInfo,
                        'form_title'        => 'New HTML Widget',
                        'form_container_id' => 'wve-bionic-new-widget-html',
                    )
                );
            ?>

            <?php
                include_partial(
                    'widget/wveForm',
                    array(
                        'form'              => $formWidgetPhoneNew,
                        'buttons_info'      => $newButtonsInfo,
                        'form_title'        => 'New Phone Widget',
                        'form_container_id' => 'wve-bionic-new-widget-phone',
                    )
                );
            ?>

        </div>
    </div>
    <div id="wve-bionic-dialog-buttons">
        <button id="wve-bionic-dialog-new-html">Create new html widget</button>
        <button id="wve-bionic-dialog-new-phone">Create new phone widget</button>
        <button id="wve-bionic-dialog-cancel">Back to widgets index</button>
    </div>
</div>
