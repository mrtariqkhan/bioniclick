
<?php $buttons_info = isset($buttons_info) ? $buttons_info->getRawValue() : array(); ?>

<?php $form_title = isset($form_title) ? $form_title : ''; ?>

<?php $classCommon = 'WVE-form'; ?>
<?php $class = $classCommon . (isset($class) ? ' ' . $class : ''); ?>
<?php $form_id = $form->getName(); ?>

<?php $form_container_id = isset($form_container_id) ? $form_container_id : ''; ?>

<?php $formAttributes = array('class' => $class, 'id' => $form_id); ?>


<?php $classErrorGlobalField = 'bionic-ajax-field-error-global bionic-ajax-field-error-global-hidden'; ?>
<?php $classErrorField = 'bionic-ajax-field-error bionic-ajax-field-error-hidden'; ?>



<div 
    id="<?php echo $form_container_id; ?>"
    class="WVE-each-form-container"
    style="display: none"
>
    <?php echo $form->renderFormTag('', $formAttributes); ?>

        <h3><?php echo $form_title; ?></h3>

        <ul class="WVE-form-list">

            <?php //echo $form->renderUsing('list'); ?>

            <div class="<?php echo $classErrorGlobalField; ?>">
                &nbsp;
            </div>

            <?php foreach ($form as $field): ?>
                <?php if (!$field->isHidden()): ?>
                    <li>
                        <?php echo $field->renderLabel(null); ?>
                        <?php echo $field->render(); ?>
                        <div class="<?php echo $classErrorField; ?>">
                            &nbsp;
                        </div>
                    </li>
                <?php endif;?>
            <?php endforeach;?>

            <li class="wve-bionic-input-hidden">
                <?php echo $form->renderHiddenFields(); ?>
                <?php if (method_exists($form, 'getObject') && !$form->getObject()->isNew()): ?>
                    <input type="hidden" name="sf_method" value="PUT" />
                <?php endif; ?>
            </li>

            <li class="wve-bionic-form-buttons">
                <?php foreach ($buttons_info as $name => $buttonInfo): ?>
                    <?php $type = array_key_exists('type', $buttonInfo) ? $buttonInfo['type'] : 'button'; ?>
                    <?php $buttonClass = array_key_exists('class', $buttonInfo) ? $buttonInfo['class'] : ''; ?>
                    <input
                        class="<?php echo $buttonClass; ?>"
                        type="<?php echo $type; ?>"
                        value="<?php echo $name; ?>"
                    >
                <?php endforeach; ?>
            </li>

        </ul>

    </form>
</div>
