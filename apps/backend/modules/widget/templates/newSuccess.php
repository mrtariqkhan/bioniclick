<?php $object = $form->getObject(); ?>

<h1>
    New Html Widget
</h1>

<div class="inner">

    <?php $urlAction = url_for('widget_create', array('combo_id'=> $object->getComboId())); ?>
    <?php $urlAfter = url_for('widget', array('combo_id'=> $object->getComboId())); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'submit_name'       => 'Create',
                'back_url'          => $urlAfter,
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel')
            )
        );
    ?>
</div>
