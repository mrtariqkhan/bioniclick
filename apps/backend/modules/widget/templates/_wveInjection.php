
<?php echo html_entity_decode($html_parts['pre_head'], ENT_QUOTES); ?>

<head>
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
    <script type="text/javascript" src="/js/jquery/jquery-ui/jquery-ui-1.8.12/js/jquery-ui-1.8.12.custom.min.js"></script>
    <script type="text/javascript" src="/js/jquery/ckeditor/ckeditor.js"></script>
    <script type="text/javascript" src="/js/jquery/ckeditor/adapters/jquery.js"></script>
    <script type="text/javascript" src="/js/jquery/jquery.scrollTo/jquery.scrollTo-min.js"></script>
    <script type="text/javascript" src="/js/jquery/jquery.form.js"></script>
    <script type="text/javascript" src="/js/jquery/jquery.blockUI.js"></script>
    <script type="text/javascript" src="/js/project/widget_visual_editor.js"></script>

    <link rel="stylesheet" type="text/css" media="screen" href="/css/jquery/custom/jquery-ui-1.8.12.custom.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="/css/project/widget_visual_editor.css" />
    <link rel="stylesheet" type="text/css" media="screen" href="/css/project/bionic_form_ajax_error.css" />

    <base href="<?php echo html_entity_decode($base_page_url, ENT_QUOTES); ?>">

    <?php echo html_entity_decode($html_parts['head'], ENT_QUOTES); ?>

<body>

    <?php
        echo get_partial(
            'widget/wveDialog',
            array(
                'editorData' => $editor_data,
                'forms'      => $forms,
            )
        );
    ?>

    <?php echo html_entity_decode($html_parts['body'], ENT_QUOTES); ?>