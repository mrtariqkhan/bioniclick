<?php

/**
 * twilioIncomingPhoneNumber actions.
 *
 * @package    bionic
 * @subpackage twilioIncomingPhoneNumber
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class twilioIncomingPhoneNumberActions extends GeneralActions {

    const FILTER_ATTRIBUTE_NAME_FREE_NUMBERS   = 'twilio_incoming_phone_number_free_filter';

    public function  preExecute() {

        $this->tableName = 'TwilioIncomingPhoneNumber';
        parent::preExecute();
    }

    /**
    * Executes index action
    *
    * @param sfRequest $request A request object
    */
    public function executeIndex(sfWebRequest $request) {

        $filterValues = $this->getFilter();
        $this->form_filter = $this->createFilterForm($filterValues);        
        $this->executeList($request);
        $this->setTemplate('index');
    }

    public function executeList(sfWebRequest $request) {

        $formFilter = $this->createFilterForm();
        $filterValues = $this->getFilter();
        $query = $formFilter->buildQuery($filterValues);

        $this->convertedPurchasedDateColumnName = 'converted_purchased_date';
        $this->convertedCleaningDateColumnName = '';

        $specialQuery = TwilioIncomingPhoneNumberTable::createIndexQuery(
            $this->user, 
            $this->convertedPurchasedDateColumnName,
            $query
        );
        $this->pager = $this->initPager($request, $specialQuery);
        $this->need_was_deleted_column  = WasDeletedHelper::ifNeedWasDeletedColumn($this->user);
        $this->need_last_calls_columns  = false;
    }

    public function executeNew(sfWebRequest $request) {

        $this->setTemplate('new');

        if (!$this->user->getIsSuperAdmin() && $this->user->getLevel() != CompanyTable::$LEVEL_BIONIC) {
            $this->forward404("You cannot purchase numbers from VoIP server.");
        }

        $this->form = $this->configureFormNew();

        $this->url_after = $this->generateUrl('twilio_incoming_phone_number_index');
    }

    public function executeNewForBionic(sfWebRequest $request) {

        $this->executeNew($request);

        $this->url_after = $this->generateUrl('company_index');
    }

    public function executeCreate(sfWebRequest $request) {

        $form = $this->configureFormNew();

        return $this->processAjaxForm($request, $form);
    }

    public function executeEdit(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->form = $this->configureFormEdit($object);
        
        $this->routePostfix = $request->getParameter('routePostfix', '');
        switch ($this->routePostfix) {
            case 'Free':
                $this->url_after = $this->generateUrl('twilio_incoming_phone_number_free_index');
                break;
            default:
                $this->url_after = $this->generateUrl('twilio_incoming_phone_number_index');
        }
    }

    public function executeUpdate(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $form = $this->configureFormEdit($object);

        return $this->processAjaxForm($request, $form);
    }

    public function executeDelete(sfWebRequest $request) {
        return $this->processAjaxDelete($request);
    }

    protected function deleteObject($object) {

        try {
            $object->deleteFromVoIPServer($this->user);

            $this->getUser()->setFlash(
                'message',
                "Number (Sid {$object->getSid()}) has been successfully released."
            );
        } catch (TwilioIncomingPhoneNumberException $e) {
            $this->getUser()->setFlash(
                'warning',
                $e->getMessage()
            );
        } catch (Exception $e) {
            Loggable::putExceptionFullLogMessage($e, ApplicationLogger::TWILIO_LOG_TYPE);

            $this->getUser()->setFlash(
                'warning',
                "Number has not been released."
            );
        }
    }

    protected function saveValidForm($validForm) {
        if ($validForm->isNew()) {
            try {
                $validForm->save();
                $this->getUser()->setFlash(
                    'message',
                    "{$validForm->getValue('count_of_numbers')} numbers have been successfully registered"
                );
            } catch (Exception $e) {
                throw $e;
            }
        } else {
            return parent::saveValidForm($validForm);
        }
    }
    
    protected function preSaveForm(sfForm $form) {

        if (!$form->isValid()) {
            return null;
        }

        $needToSave = false;
        $requestResult = array();
        try {
            if ($form->isNew()) {
                return true;
            } else {
                $object = $form->getObject();
                $twilioAccount = TwilioAccountTable::getInstance()->findById(
                    $object->getTwilioAccountId()
                );

                $message = 'Result of the updating incoming number';
                $requestResult = $this->updateNumber(
                    $twilioAccount->getSid(),
                    $twilioAccount->getAuthToken(),
                    $object->getSid(),
                    $form->getValues(),
                    $object
                );
                $needToSave = true;
            }
            Loggable::putArrayLogMessage(
                ApplicationLogger::TWILIO_LOG_TYPE,
                sfLogger::DEBUG,
                $requestResult,
                $message
            );
            if (isset($requestResult['error'])) {
                throw(new Exception($requestResult['error']));
            }
            if (isset($requestResult['message'])) {
                $this->getUser()->setFlash('message', $requestResult['message']);
            }

            return $needToSave;
        } catch (TwilioPhoneNumberAPIException $e) {
            Loggable::putExceptionLogMessage($e, ApplicationLogger::TWILIO_LOG_TYPE);
            throw($e);
        }
    }
    
    protected function updateNumber(
        $accountSid,
        $authToken,
        $sid,
        Array $formValues,
        TwilioIncomingPhoneNumber $object
    ) {

        $url  = array_key_exists('url', $formValues)
            ? $formValues['url']
            : ''
        ;
        $friendlyName   = array_key_exists('friendly_name', $formValues)
            ? $formValues['friendly_name']
            : ''
        ;
        $httpMethodType = array_key_exists('http_method_type', $formValues)
            ? $formValues['http_method_type']
            : ''
        ;

        return TwilioPhoneNumberAPI::sendIncomingPhoneNumber(
            $accountSid,
            $authToken,
            $sid,
            $url,
            $friendlyName,
            null,
            null,
            $httpMethodType
        );
    }

    public function executeAjaxFillAreaCodeAutocompleteFullNew(sfWebRequest $request) {

        $result = TwilioLocalAreaCodeTable::findForSelectToCreate(
            $request->getParameter('q', ''),
            $request->getParameter('limit'),
            $request->getParameter('phone_number_type_short', null),
            $request->getParameter('phone_number_country_id', null)
        );

        return $this->processAjaxResult($request, $result, false);
    }

    public function executeAjaxFillAreaCodeAutocompleteFullFilter(sfWebRequest $request) {

        $result = TwilioLocalAreaCodeTable::findForSelectToFilter(
            $request->getParameter('q', ''),
            $request->getParameter('limit'),
            $request->getParameter('phone_number_type_short', null),
            $request->getParameter('phone_number_country_id', null)
        );

        return $this->processAjaxResult($request, $result, false);
    }

    public function executeAjaxFillAreaCodeAutocompleteFullFreeFilter(sfWebRequest $request) {

        $result = TwilioLocalAreaCodeTable::findForSelectToFreeFilter(
            $request->getParameter('q', ''),
            $request->getParameter('limit'),
            $request->getParameter('phone_number_type_short', null),
            $request->getParameter('phone_number_country_id', null)
        );

        return $this->processAjaxResult($request, $result, false);
    }

    public function executeAjaxFillAreaCodeAutocompleteForCampaign(sfWebRequest $request) {

        $result = TwilioLocalAreaCodeTable::findForSelectForCampaign(
            $request->getParameter('q', ''),
            $request->getParameter('limit'),
            $request->getParameter('phone_number_type_short', null),
            $request->getParameter('phone_number_country_id', null),
            $request->getParameter('advertiser_id', null)
        );

        return $this->processAjaxResult($request, $result, false);
    }

    protected function createFilterForm($defaults = array()) {

        $options = array(
            'user'                      => $this->user,
            'ajax_url_local_area_codes' => 'twilioIncomingPhoneNumber/ajaxFillAreaCodeAutocompleteFullFilter',
        );
        $filterForm = new TwilioIncomingPhoneNumberFormFilter($defaults, $options);

        return $filterForm;
    }

    public function executeFilter(sfWebRequest $request) {

        $filterForm = $this->createFilterForm();
        return $this->processAjaxFilter($request, $filterForm);
    }

    protected function configureFormNew() {

        $formOptions = array(
            TwilioIncomingPhoneNumberNewForm::OPTION_USER => $this->user,
        );

        $object = null;

        $form = new TwilioIncomingPhoneNumberNewForm($object, $formOptions);
        return $form;
    }

    protected function configureFormEdit($object) {

        $formOptions = array(
            TwilioIncomingPhoneNumberEditForm::OPTION_USER => $this->user,
        );

        $form = new TwilioIncomingPhoneNumberEditForm($object, $formOptions);
        return $form;
    }

    public function executeFreeIndex(sfWebRequest $request) {

        $filterValues = $this->getFilterFree();
        $this->form_filter = $this->createFreeFilterForm($filterValues);

        $this->executeFreeList($request);
        $this->setTemplate('freeIndex');
    }

    public function executeFreeList(sfWebRequest $request) {

        $formFilter = $this->createFreeFilterForm();
        $filterValues = $this->getFilterFree();
        $query = $formFilter->buildQuery($filterValues);

        $this->convertedPurchasedDateColumnName = 'converted_purchased_date';
        $this->convertedCleaningDateColumnName = 'converted_cleaning_date';
        $specialQuery = TwilioIncomingPhoneNumberTable::createFreeIndexQuery(
            $this->user, 
            $this->convertedPurchasedDateColumnName,
            $this->convertedCleaningDateColumnName,
            $query
        );
        $this->pager = $this->initPager($request, $specialQuery);
        $this->need_was_deleted_column = WasDeletedHelper::ifNeedWasDeletedColumn($this->user);

        $this->need_last_calls_columns = true;
        $this->last_calls_info = TwilioIncomingCallTable::findLastCallsInfo(
            $this->pager->getQuery(), 
            $this->user->findTimezoneName(), 
            'Y-m-d H:i:s'
        );
    }

    protected function getFilterFree() {
        return $this->getFilter(self::FILTER_ATTRIBUTE_NAME_FREE_NUMBERS);
    }

    protected function createFreeFilterForm($defaults = array()) {

        $options = array(
            'user'                      => $this->user,
            'ajax_url_local_area_codes' => 'twilioIncomingPhoneNumber/ajaxFillAreaCodeAutocompleteFullFreeFilter',
            'addClass'                  => 'free',
        );
        $filterForm = new TwilioIncomingPhoneNumberFormFilter($defaults, $options);

        return $filterForm;
    }

    public function executeFreeFilter(sfWebRequest $request) {

        $filterForm = $this->createFreeFilterForm();
        return $this->processAjaxFilter($request, $filterForm, self::FILTER_ATTRIBUTE_NAME_FREE_NUMBERS);
    }
    
    public function executeSynchronizeTask(sfWebRequest $request) {

        return $this->processTask(
            'IncomingNumbersSyncTask',
            array(),
            array(
                'direction'     => 'to',
                'delete'        => 'true',
                'belong'        => 'true',
            )
        );
    }
}