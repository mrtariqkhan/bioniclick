<?php $object = $form->getObject(); ?>

<h1>
    Edit incoming number
    (<?php echo $object->getPhoneNumber(); ?>)
</h1>

<div class="inner">

    <?php $urlAction = url_for('twilio_incoming_phone_number_update', array('id' => $object->getId())); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'submit_name'       => 'Update',
                'back_url'          => $url_after,
                'form_class_name'   => 'ajax-form wide-from',
                'cancel_attributes' => array('class' => 'ajax-form-cancel'),
                'back_url_method'   => 'get',
            )
        );
    ?>
</div>
