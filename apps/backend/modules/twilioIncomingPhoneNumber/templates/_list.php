<?php use_helper('Link'); ?>
<?php use_helper('Pager'); ?>

<?php include_partial('global/flash_message', array('flash_name' => 'message')); ?>
<?php include_partial('global/flash_message', array('flash_name' => 'warning')); ?>

<?php
    $pagerHtmlString = pager(
        $pager->getRawValue(),
        sfConfig::get('app_pager_max_per_pages'),
        $pager_url,
        null,
        'ajax-pager'
    );
?>

<p>
    <?php 
        include_partial(
            'global/pager_list_header',
            array(
                'pager' => $pager,
                'pagerHtmlString' => $pagerHtmlString
            )
        ); 
    ?>
</p>
<?php
    $list_buttons = array(
        'Edit' => array(
            'class' => 'icon edit ajax-form-load'
        ),
        'Release number' => array(
            'class' => 'icon reload'
        ),
    );

    $legend_buttons = array();
?>
<?php
    $need_purchased_date = 
        isset($convertedPurchasedDateColumnName)
        && is_string($convertedPurchasedDateColumnName) 
        && !empty($convertedPurchasedDateColumnName)
    ;

    $need_cleaning_date = 
        isset($convertedCleaningDateColumnName) 
        && is_string($convertedCleaningDateColumnName) 
        && !empty($convertedCleaningDateColumnName)
    ;
?>
<?php $last_calls_info = isset($last_calls_info) ? $last_calls_info->getRawValue() : array(); ?>
<div class="table-block">
    <table class="table table-small">
        <thead>
            <tr>
                <th class="first">&nbsp;</th>
                <th>Sid</th>
                <th>Account</th>
                <th>Phone number type</th>
                <th>Url</th>
                <th>Method type</th>
                <th>Area code</th>
                <th>Phone number</th>
                <th>Friendly name</th>
                <th>Is default</th>
                <?php if ($need_was_deleted_column) : ?>
                    <th><?php echo WasDeletedHelper::TITLE; ?></th>
                <?php endif; ?>
                <?php if ($need_last_calls_columns) : ?>
                    <th>Calls amount in last 7 days</th>
                    <th>Calls amount in last 30 days</th>
                    <th>Start Time of the last call</th>
                <?php endif; ?>
                <th>Is in use</th>
                <?php if ($need_purchased_date) : ?>
                    <th>Purchased Date</th>
                <?php endif; ?>
                <?php if ($need_cleaning_date) : ?>
                    <th>Added For Cleaning Date</th>
                <?php endif; ?>
                <th class="last">&nbsp; </th>
            </tr>
        </thead>
        <tbody>
            <?php $i = $pager->getFirstIndice(); ?>
            <?php $list = $pager->getResults(); ?>
            <?php $list = $list->getRawValue(); ?>
            <?php foreach ($list as $object): ?>
                <?php $id = $object->getId(); ?>
                <tr class="<?php echo ($i%2) ? 'odd' : 'even'; ?>">
                    <td><?php echo $i; ?></td>
                    <td><?php echo $object->getSid(); ?></td>
                    <td><?php echo $object->getTwilioAccount(); ?></td>
                    <td><?php echo $object->getTwilioLocalAreaCode()->getPhoneNumberType(); ?></td>
                    <td><?php echo $object->getUrl(); ?></td>
                    <td><?php echo $object->getHttpMethodType(); ?></td>
                    <td><?php echo $object->getTwilioLocalAreaCode()->getCode(); ?></td>
                    <td><?php echo $object->getPhoneNumber(); ?></td>
                    <td><?php echo $object->getFriendlyName(); ?></td>
                    <td><?php echo $object->getIsDefaultString(); ?></td>

                    <?php if ($need_was_deleted_column) : ?>
                        <td><?php echo $object->getWasDeletedString(); ?></td>
                    <?php endif; ?>
                    <?php if ($need_last_calls_columns) : ?>
                        <?php $lastCallsInfoForNumber = $last_calls_info[$id]; ?>
                        <td><?php echo $lastCallsInfoForNumber['amount_7']; ?></td>
                        <td><?php echo $lastCallsInfoForNumber['amount_30']; ?></td>
                        <td><?php echo $lastCallsInfoForNumber['last_call']; ?></td>
                    <?php endif; ?>
                    <td><?php echo $object->findIsInUseString(); ?></td>
                    <?php if ($need_purchased_date) : ?>
                        <td><?php echo $object->get($convertedPurchasedDateColumnName); ?></td>
                    <?php endif; ?>
                    <?php if ($need_cleaning_date) : ?>
                        <td><?php echo $object->get($convertedCleaningDateColumnName); ?></td>
                    <?php endif; ?>

                    <td class="last">
                        <?php $url = url_for($edit_route_name, array('id' => $id)); ?>
                        <?php $urlAfter = $edit_url_after; ?>
                        <?php
                            $a_options = array();
                            $ifShowUpdateLinks = WasDeletedHelper::ifCanUpdate($object);

                            if ($permissions['can_update']) {
                                if ($ifShowUpdateLinks) {
                                    $title = 'Edit';
                                    $a_options[$title] = $list_buttons[$title];
                                    $a_options[$title]['href'] = $url;

                                    $legend_buttons[$title] = & $list_buttons[$title];
                                } else {
                                    $a_options['Cannot edit deleted phone number'] = $list_buttons['Edit'];
                                }
                            }

                            if ($permissions['can_delete']) {
                                if ($ifShowUpdateLinks) {
                                    if ($object->getIsDefault()) {
                                        $a_options['Cannot release default phone number'] = & $list_buttons['Release number'];
                                    } else {
                                        $title = 'Release number';

                                        $simple_options = $list_buttons[$title];
                                        $simple_options['title'] = $title;
                                        $simple_options['after_url'] = $urlAfter;

                                        $a_options[$title] = link_to_ajax_delete(
                                            '',
                                            url_for('twilio_incoming_phone_number_delete', array('id' => $id)),
                                            $simple_options
                                        );

                                        $legend_buttons[$title] = & $list_buttons[$title];
                                    }
                                } else {
                                    $a_options['Cannot release deleted phone number'] = $list_buttons['Release number'];
                                }
                            }

                            echo icon_links($a_options, array('class' => 'icon-links clearfix'), false);
                        ?>
                    </td>
                </tr>
                <?php ++$i; ?>
           <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php
    include_partial(
         'global/legend',
          array(
            'legend_buttons'  => $legend_buttons,
          )
    ); 
?>

<?php echo $pagerHtmlString; ?>