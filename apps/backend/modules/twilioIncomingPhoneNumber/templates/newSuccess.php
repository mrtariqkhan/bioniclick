<h1>Registering new incoming numbers</h1>

<div class="inner">

    <?php $urlAction = url_for('twilio_incoming_phone_number_create'); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'submit_name'       => 'Register',
                'back_url'          => $url_after,
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel')
            )
        );       
    ?>
</div>
