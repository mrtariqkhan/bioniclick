<?php
    include_partial(
        'twilioIncomingPhoneNumber/list',
        array(
            'pager'                             => $pager,
            'permissions'                       => $permissions,
            'need_was_deleted_column'           => $need_was_deleted_column,
            'need_last_calls_columns'           => $need_last_calls_columns,
            'convertedPurchasedDateColumnName'  => $convertedPurchasedDateColumnName,
            'convertedCleaningDateColumnName'   => $convertedCleaningDateColumnName,
            'pager_url'                         => url_for('twilio_incoming_phone_number_list'),
            'edit_route_name'                   => 'twilio_incoming_phone_number_edit',
            'edit_url_after'                    => url_for('twilio_incoming_phone_number_index'),
        )
    );
?>