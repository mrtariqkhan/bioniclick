
<div class="account-wrap clearfix">

    <div class="ajax-filter-container">

        <?php if ($permissions['can_filter']): ?>
            <div class="filter-form-wrap filter-form-wrap-account-numbers">
                <h1>Search</h1>
                <div class="filter-form-wraper">
                    <?php
                        include_partial(
                            'global/form_ajax',
                            array(
                                'form'              => $form_filter,
                                'action_url'        => url_for('twilio_incoming_phone_number_free_filter'),
                                'submit_name'       => 'Go!',
                                'back_url'          => '',
                                'form_class_name'   => 'ajax-filter',
                                'cancel_attributes' => array('class' => 'ajax-form-cancel')
                            )
                        );
                    ?>
                </div>
            </div>
        <?php endif; ?>

        <div class="ajax-pagable">
            <?php
                include_partial(
                    'twilioIncomingPhoneNumber/list',
                    array(
                        'pager'                             => $pager,
                        'permissions'                       => $permissions,
                        'need_was_deleted_column'           => $need_was_deleted_column,
                        'need_last_calls_columns'           => $need_last_calls_columns,
                        'last_calls_info'                   => $last_calls_info,
                        'convertedPurchasedDateColumnName'  => $convertedPurchasedDateColumnName,
                        'convertedCleaningDateColumnName'   => $convertedCleaningDateColumnName,
                        'pager_url'                         => url_for('twilio_incoming_phone_number_free_list'),
                        'edit_route_name'                   => 'twilio_incoming_phone_number_edit_free',
                        'edit_url_after'                    => url_for('twilio_incoming_phone_number_free_index'),
                    )
                );
            ?>            
        </div>
    </div>
</div>