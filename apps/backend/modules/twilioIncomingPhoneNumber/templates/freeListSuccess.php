<?php
    include_partial(
        'twilioIncomingPhoneNumber/list',
        array(
            'pager'                             => $pager,
            'permissions'                       => $permissions,
            'need_was_deleted_column'           => $need_was_deleted_column,
            'need_last_calls_columns'           => $need_last_calls_columns,
            'last_calls_info'                   => $last_calls_info,
            'convertedPurchasedDateColumnName'  => $convertedPurchasedDateColumnName,
            'convertedCleaningDateColumnName'   => $convertedCleaningDateColumnName,
            'pager_url'                         => url_for('twilio_incoming_phone_number_free_list'),
            'edit_route_name'                   => 'twilio_incoming_phone_number_edit_free',
            'edit_url_after'                    => url_for('twilio_incoming_phone_number_free_index'),
        )
    );
?>