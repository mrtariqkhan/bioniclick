<?php

/**
 * sfGuardUserProfile actions.
 *
 * @package    bionic
 * @subpackage sfGuardUserProfile
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sfGuardUserProfileActions extends GeneralActions {

    const CALL_SUMMARY_FILTER_ATTRIBUTE_NAME        = 'call_summary_filter';
    const CALLS_BY_PARAMETER_FILTER_ATTRIBUTE_NAME  = 'calls_by_parameter_filter';
    const CALL_STATUS_FILTER_ATTRIBUTE_NAME         = 'call_status_filter';
    const TOP_CLIENTS_FILTER_ATTRIBUTE_NAME         = 'top_clients_filter';


    public function preExecute() {

        $this->getResponse()->addStylesheet('/css/project/reset.css', 'last');
        $this->tableName = 'sfGuardUserProfile';
        parent::preExecute();
    }    

    public function executeShow(sfWebRequest $request) {
        
        $this->getUser()->setAttribute('menu', 'home');

        $this->getResponse()->addJavascript('/js/fusion_charts_free/FusionCharts.js', 'last');
        $this->getResponse()->addJavascript('/js/jquery/excanvas.min.js', 'last');

        $this->getResponse()->addJavascript('/js/jquery/jquery.flot.js', 'last');
        $this->getResponse()->addJavascript('/js/jquery/jquery.flot.pie.js', 'last');
        $this->getResponse()->addJavascript('/js/project/flotCharts.js', 'last');
        $this->getResponse()->addJavascript('/js/project/home_charts.js', 'last');


        $this->getResponse()->addJavascript(
            '/js/jquery/jquery.form.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/project/ajax_filter.js',
            'last'
        );
        $this->getResponse()->addStylesheet('/css/project/icon.css', 'last');
        $this->getResponse()->addStylesheet('/css/project/tabs_accordion.css', 'last');
        $this->getResponse()->addStylesheet('/css/project/form_general.css', 'last');
        $this->getResponse()->addStylesheet('/css/project/datepicker.css', 'last');
        $this->getResponse()->addJavascript('/js/jquery/livequery/jquery.livequery.js', 'last');
        $this->getResponse()->addJavascript('/js/jquery/daterangepicker/js/daterangepicker.jQuery.js', 'last');
        $this->getResponse()->addJavascript('/js/project/date_range_common.js', 'last');
        $this->getResponse()->addJavascript('/js/project/graphs_filter.js', 'last');

        $this->getResponse()->addStylesheet('/css/project/home.css', 'last');
        $this->getResponse()->addStylesheet('/css/project/filter_block.css', 'last');

        $this->nadvId = $request->getParameter('advertiser_id');
        

        $owner = $this->configureChartsOwnerInfo($request);


        $filterFormArray = $this->getFilterFormArraySpecific(true);
        $filterForm     = $filterFormArray['form'];
        $cleanedValues  = $filterFormArray['cleaned'];

        $this->filter_form = $filterForm;

        $chartDataCallSummary = $this->configureChartDataCallSummary($request);
        $this->call_summary_filter_form = $chartDataCallSummary['form'];
        $this->chart_call_summary_options       = $chartDataCallSummary['data'];

        $chartDataCallStatus = $this->configureChartDataCallStatus($request);
        $this->call_status_filter_form  = $chartDataCallStatus['form'];
        $this->chart_call_status_options        = $chartDataCallStatus['data'];

        $this->chart_unique_callers_options = $this->configureChartDataUniqueCallers($owner, $cleanedValues);

        $timezoneName = $this->user->findTimezoneName();
        $timeRange = array();
        if (array_key_exists('from', $cleanedValues)) {
            $timeRange['from'] = TimeConverter::getDateTime($cleanedValues['from'], $timezoneName, ChartFilteredHelper::RESULT_PARAMETER_FORMAT_TIME);
        }
        if (array_key_exists('to', $cleanedValues)) {
            $timeRange['to'] = TimeConverter::getDateTime($cleanedValues['to'], $timezoneName, ChartFilteredHelper::RESULT_PARAMETER_FORMAT_TIME);
        } else {
            $timeRange['to'] = TimeConverter::getDateTimeFormattedByString('now', $timezoneName, ChartFilteredHelper::RESULT_PARAMETER_FORMAT_TIME);
        }
        $this->chart_date_info = array(
            // 'call_summary'      => $chartDataCallSummary['time_range'],
            'call_summary'      => $timeRange,
            'unique_callers'    => $timeRange,
            'top_clients'       => $timeRange,
            'overall_values'    => $timeRange,
            'calls_by'          => $timeRange,
            'call_status'       => $timeRange,
        );
        $isForCompany = !empty($owner['companyId']);
        $this->chart_titles_info = array(
            'call_summary'      => 'Call Summary',
            'unique_callers'    => 'Unique Call Summary',
            'top_clients'       => 'Top 10 ' . ($isForCompany ? 'customers' : 'campaigns'),
            'overall_values'    => 'Overall values',
            'calls_by'          => 'Calls By',
            'call_status'       => 'Call Status Summary',
        );

        $this->overall_values = $this->configureOverallValues($owner, $cleanedValues);

        $callsByParameterFormArray = $this->getFilterFormArraySpecificCallsByParameter(true);
        $this->calls_by_parameter_filter_form = $callsByParameterFormArray['form'];

        $callsTopClientsFormArray = $this->getFilterFormArraySpecificTopClientsFilter(true);
        $this->top_clients_filter_form = $callsTopClientsFormArray['form'];

        $this->charts_owner = $owner;
        $this->current_user = $this->user;
        $this->advertisers = Doctrine::getTable('Advertiser')->createQuery()->where('company_id=?', $this->user->Company->getId())->execute();
        $this->setTemplate('show');
    }
    

    public function executeAjaxChartDataTopClients(sfWebRequest $request) {

        $filterFormArray = $this->getFilterFormArraySpecificTopClientsFilter(true);
        $cleanedValues = $filterFormArray['cleaned'];

        $chartParameters = $this->configureChartsOwnerInfo($request);
        $chartHelper = new ChartTopCustomersHelper(
            $chartParameters['advertiser'],
            $chartParameters['company'],
            $cleanedValues
        );
        $result = $chartHelper->findData();
        $result = array(
            'data'          => $result,
            'need_links'    => false,
            'escape_links'  => array(),
        );

        return $this->processAjaxResult($request, $result);
    }
    
    public function executeAjaxChartDataCallsByParameter(sfWebRequest $request) {

        $callsByParameterFormArray = $this->getFilterFormArraySpecificCallsByParameter(true);
        $cleanedValues = $callsByParameterFormArray['cleaned'];

        $result = array();
        $byParameter = array_key_exists('by_parameter', $cleanedValues) ? $cleanedValues['by_parameter'] : null;
        $chartBuilderClassName = '';
        $needLinks = false;
        switch ($byParameter) {
            case GraphCallsByParameterFilterForm::BY_STATUS:
                $chartBuilderClassName = 'ChartCallsByStatusHelper';
                break;
            case GraphCallsByParameterFilterForm::BY_REFERRER_SOURCE:
                $chartBuilderClassName = 'ChartCallsByReferrerSourceHelper';
                break;
            case GraphCallsByParameterFilterForm::BY_SEARCH_TYPE:
                $chartBuilderClassName = 'ChartCallsBySearchTypeHelper';
                break;
            case GraphCallsByParameterFilterForm::BY_KEYWORD:
                $chartBuilderClassName = 'ChartCallsByKeywordHelper';
                break;
            case GraphCallsByParameterFilterForm::BY_URL:
                $chartBuilderClassName = 'ChartCallsByUrlHelper';
                $needLinks = true;
                break;
            default:
                $chartBuilderClassName = 'ChartCallsByStatusHelper';
        }


        $owner = $this->configureChartParametersByTzDiff($request);
        $chartHelper = new $chartBuilderClassName(
            $owner['advertiser'],
            $owner['company'],
            $owner['tzDiff'],
            $cleanedValues,
            $this->user
        );
        $result = $chartHelper->findData(true);
        $result = array(
            'data'          => $result,
            'need_links'    => $needLinks,
            'escape_links'  => ChartCallsByParameterHelper::$LINKS_ESCAPE,
        );

        return $this->processAjaxResult($request, $result);
    }

    protected function configureChartDataUniqueCallers($owner, $cleanedValues) {

        $chartHelper = new ChartUniqueCallersHelper(
            $owner['advertiser'],
            $owner['company'],
            $cleanedValues
        );
        $result = $chartHelper->findData(1, 8);

        $data = array('result' => $result, 'type' => 'bar_chart', 'width' => 500, 'height' => 347);

        return $data;
    }

    protected function configureChartDataCallSummary($request) {

        $callSummaryFilterFormArray = $this->getFilterFormArraySpecificCallSummary(true);
        $callSummaryFilterForm      = $callSummaryFilterFormArray['form'];
        $callSummaryCleanedValues   = $callSummaryFilterFormArray['cleaned'];

        $fromToValues = $callSummaryFilterForm->getFromTo();
        $cleanedValues = array_merge($callSummaryCleanedValues, $fromToValues);


        $owner = $this->configureChartParametersByTzName($request);
        $chartHelper = new ChartCallSummaryHelper(
            $owner['advertiser'],
            $owner['company'],
            $owner['tzName'],
            $cleanedValues
        );
        $result = $chartHelper->findData();

        $graphData = $result[ChartCallSummaryHelper::RESULT_PARAMETER_GRAPH_DATA];
        $timeRange = $result[ChartCallSummaryHelper::RESULT_PARAMETER_TIME_RANGE];

        $data = array('result' => $graphData, 'type' => 'column_chart', 'width' => 900, 'height' => 350);
        return array(
            'form'      => $callSummaryFilterForm,
            'data'      => $data,
            'time_range'=> $timeRange,
        );
    }

    protected function configureChartDataCallStatus($request) {

        $callStatusFilterFormArray = $this->getFilterFormArraySpecificCallStatus(true);
        $callStatusFilterForm      = $callStatusFilterFormArray['form'];
        $callStatusCleanedValues   = $callStatusFilterFormArray['cleaned'];

        $fromToValues = $callStatusFilterForm->getFromTo();
        $callStatusCleanedValuesAll = array_merge($callStatusCleanedValues, $fromToValues);

        $owner = $this->configureChartParametersByTzDiff($request);
        $chartHelper = new ChartCallStatusHelper(
            $owner['advertiser'],
            $owner['company'],
            $owner['tzDiff'],
            $callStatusCleanedValuesAll
        );
        $callStatusResult = $chartHelper->findData();
        $graphData = $callStatusResult[ChartCallStatusHelper::RESULT_PARAMETER_GRAPH_DATA];

        $data = array('result' => $graphData, 'type' => 'stack_column_chart', 'width' => 900, 'height' => 350);
        return array(
            'form'      => $callStatusFilterForm,
            'data'      => $data,
        );
    }

    protected function configureOverallValues($owner, $cleanedValues) {

        return TwilioIncomingCallTable::getOverallValues(
            $owner['advertiser'],
            $owner['company'],
            $cleanedValues
        );
    }

    public function executeAjaxChartDataCallSummary(sfWebRequest $request) {

        $chartData = $this->configureChartDataCallSummary($request);
        $this->chart_call_summary_options   = $chartData['data'];
        $this->time_range           = $chartData['time_range'];
    }

    public function executeAjaxChartDataCallStatus(sfWebRequest $request) {

        $chartData = $this->configureChartDataCallStatus($request);
        $this->chart_call_status_options = $chartData['data'];
    }

    public function executeEdit(sfWebRequest $request) {

        $this->getResponse()->addStylesheet('/css/project/form_general.css', 'last');
        $this->getUser()->setAttribute('menu', 'account');

        $this->form = $this->configureFormEdit();
    }

    public function executeUpdate(sfWebRequest $request) {

        $this->getResponse()->addStylesheet('/css/project/form_general.css', 'last');
        $this->getUser()->setAttribute('menu', 'account');
        $this->forward404Unless($request->isMethod(sfRequest::POST) || $request->isMethod(sfRequest::PUT));

        $this->form = $this->configureFormEdit();
        $this->processFormCustom($request, $this->form);
        $this->setTemplate('edit');
    }

    public function configureFormEdit() {

        $object = $this->user->getProfile();
        $this->forward404If(empty($object), 'There are no such profile.');

        return new sfGuardUserProfileFormEdit($object);
    }

//    //TODO:: think if it is need
//    public function executeDelete(sfWebRequest $request) {
//
//        $request->checkCSRFProtection();
//
//        $this->forward404Unless($object = Doctrine::getTable('sfGuardUserProfile')->find(array($request->getParameter('id'))), sprintf('Object sf_guard_user_profile does not exist (%s).', $request->getParameter('id')));
//        $object->delete();
//
//        $this->redirect('sfGuardUserProfile/index');
//    }

    protected function processFormCustom(sfWebRequest $request, sfForm $form) {

        $form->bind($request->getParameter($form->getName()), $request->getFiles($form->getName()));
        if ($form->isValid()) {
            $object = $form->save();
            $this->redirect('@homepage');
        }
    }

    public function executeFilter(sfWebRequest $request) {

        $formArray = $this->getFilterFormArraySpecific();
        $filterForm = $formArray['form'];

        return $this->processAjaxFilter($request, $filterForm);
    }

    protected function getFilterFormArraySpecific($ifSavedIsNeed = false) {

        $defaults = array(
            GraphFilterForm::EMBED_FORM_DATING => array(
                DateRangePickerFilterForm::FIELD_NAME_TYPE => DateRangePickerFilterForm::TYPE_CODE_THIS_MONTH,
            ),
        );

        $timezoneName = $this->user->findTimezoneName();
        $options = array(
            'timezone_name' => $timezoneName,
        );

        return $this->getFilterFormArray(
            'GraphFilterForm',
            $defaults,
            $options,
            true,
            $ifSavedIsNeed
        );
    }

    public function executeCallSummaryFilter(sfWebRequest $request) {

        $formArray = $this->getFilterFormArraySpecificCallSummary(false);
        $filterForm = $formArray['form'];

        return $this->processAjaxFilter($request, $filterForm, self::CALL_SUMMARY_FILTER_ATTRIBUTE_NAME);
    }

    protected function getFilterFormArraySpecificCallSummary($ifSavedIsNeed = false) {
        
        $filterFormArray = $this->getFilterFormArraySpecific(true);
        $cleanedValues = $filterFormArray['cleaned'];

        $options = $cleanedValues;

        return $this->getFilterFormArray(
            'GraphCallSummaryFilterForm',
            array(),
            $options,
            true,
            $ifSavedIsNeed,
            self::CALL_SUMMARY_FILTER_ATTRIBUTE_NAME
        );
    }

    public function executeCallsByParameterFilter(sfWebRequest $request) {

        $formArray = $this->getFilterFormArraySpecificCallsByParameter(false);
        $filterForm = $formArray['form'];

        return $this->processAjaxFilter($request, $filterForm, self::CALLS_BY_PARAMETER_FILTER_ATTRIBUTE_NAME);
    }

    protected function getFilterFormArraySpecificCallsByParameter($ifSavedIsNeed = false) {
        
        $filterFormArray = $this->getFilterFormArraySpecific(true);
        $cleanedValues = $filterFormArray['cleaned'];

        $options = $cleanedValues;

        //NOTE:: issue #2860. Remove it when keywords will be necessary for all users
//        $options[GraphCallsByParameterFilterForm::OPTION_BY_KEYWORD_IS_NEED] = $this->user->getIsSuperAdmin();
        $options[GraphCallsByParameterFilterForm::OPTION_BY_KEYWORD_IS_NEED] = true;

        return $this->getFilterFormArray(
            'GraphCallsByParameterFilterForm',
            array(
                'by_parameter' => GraphCallsByParameterFilterForm::BY_PARAMETER_DEFAULT,
            ),
            $options,
            true,
            $ifSavedIsNeed,
            self::CALLS_BY_PARAMETER_FILTER_ATTRIBUTE_NAME
        );
    }

    public function executeTopClientsFilter(sfWebRequest $request) {

        $formArray = $this->getFilterFormArraySpecificTopClientsFilter(false);
        $filterForm = $formArray['form'];

        return $this->processAjaxFilter($request, $filterForm, self::TOP_CLIENTS_FILTER_ATTRIBUTE_NAME);
    }

    protected function getFilterFormArraySpecificTopClientsFilter($ifSavedIsNeed = false) {
        
        $filterFormArray = $this->getFilterFormArraySpecific(true);
        $cleanedValues = $filterFormArray['cleaned'];

        $options = $cleanedValues;

        return $this->getFilterFormArray(
            'GraphTopClientsFilterForm',
            array(
                'kind' => GraphTopClientsFilterForm::BY_DEFAULT,
            ),
            $options,
            true,
            $ifSavedIsNeed,
            self::TOP_CLIENTS_FILTER_ATTRIBUTE_NAME
        );
    }

    public function executeCallStatusFilter(sfWebRequest $request) {

        $formArray = $this->getFilterFormArraySpecificCallStatus(false);
        $filterForm = $formArray['form'];

        return $this->processAjaxFilter($request, $filterForm, self::CALL_STATUS_FILTER_ATTRIBUTE_NAME);
    }

    protected function getFilterFormArraySpecificCallStatus($ifSavedIsNeed = false) {
        
        $filterFormArray = $this->getFilterFormArraySpecific(true);
        $cleanedValues = $filterFormArray['cleaned'];

        $options = $cleanedValues;

        return $this->getFilterFormArray(
            'GraphCallStatusFilterForm',
            array(),
            $options,
            true,
            $ifSavedIsNeed,
            self::CALL_STATUS_FILTER_ATTRIBUTE_NAME
        );
    }

    protected function configureChartsOwnerInfo(sfWebRequest $request) {

       $advId = $request->getParameter('advertiser_id', null);
       $comId = $request->getParameter('company_id', null);

        $userAdvId = null;
        $userComId = null;
        if ($this->user->isAdvertiserUser()) {
            $userAdvId = $this->user->getAdvertiserId();
        } else {
            $userComId = $this->user->getFirstCompanyId();
        }

        $advertiser = null;
        $company    = null;
        $isForChild = false;

        if (!empty($advId)) {
            $advertiser = AdvertiserTable::getInstance()->find($advId);
            if (!empty($advertiser) && $advertiser->exists()) {
                $isForChild = ($userAdvId != $advId);
                $this->forward404Unless($this->user->ifHasSubAdvertiser($advertiser), 'Access denied.');
            }
        } elseif (!empty($comId)) {
            $company = CompanyTable::getInstance()->find($comId);
            if (!empty($company) && $company->exists()) {
                $isForChild = ($userComId != $comId);
                $this->forward404Unless($this->user->ifHasSubCompany($company), 'Access denied.');
            }
        }

        if (
                (empty($advertiser) || !$advertiser->exists())
            &&  (empty($company) || !$company->exists())
        ) {
            if ($this->user->isAdvertiserUser()) {
                $advertiser = $this->user->getAdvertiser();
            } else {
                $company = $this->user->getFirstCompany();
            }
        }


        return array(
            'advertiser'    => $advertiser,
            'company'       => $company,
            'advertiserId'  => empty($advertiser) ? 0 : $advertiser->getId(),
            'companyId'     => empty($company) ? 0 : $company->getId(),
            'isForChild'    => $isForChild,
        );
    }

    protected function configureChartParametersByTzDiff(sfWebRequest $request) {

        $result = $this->configureChartsOwnerInfo($request);
        $result['tzDiff'] = $this->user->findGmtDifference();

        return $result;
    }

    protected function configureChartParametersByTzName(sfWebRequest $request) {

        $result = $this->configureChartsOwnerInfo($request);
        $result['tzName'] = $this->user->findTimezoneName();

        return $result;
    }

    protected function getFilterAttributeName($attributeName = null) {

        $request = $this->getRequest();
        $owner = $this->configureChartsOwnerInfo($request);

        if (empty($attributeName)) {
            return $this->filterAttributeName;
        }
        return $attributeName . '_' . $this->userId . '_' . $owner['advertiserId'] . '_' . $owner['companyId'];
    }
}
