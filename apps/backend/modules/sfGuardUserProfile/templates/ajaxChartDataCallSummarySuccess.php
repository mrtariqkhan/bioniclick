
<?php
    $time_range = $time_range->getRawValue();

    $time_range_from = array_key_exists('from', $time_range) ? $time_range['from'] : '';
    $time_range_to = array_key_exists('to', $time_range) ? $time_range['to'] : '';
?>
<div
    range_data_from="<?php echo $time_range_from; ?>"
    range_data_to="<?php echo $time_range_to; ?>"
>
	<?php include_component('chart','generate', array('chart_options' => $chart_call_summary_options));?>
    <!--<?php //$chart_call_summary->renderChart(); ?>-->
</div>
