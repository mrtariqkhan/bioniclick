
<?php if (isset($overall_value)): ?>
    <div class="overall-part">
        <div
            class="overall-value-help icon help"
            title="<?php echo $overall_help; ?>"
        >
            &nbsp;
        </div>
        <div class="overall-title">
            <?php echo "$overall_title:"; ?>
        </div>
        <div class="overall-value">
            <?php echo $overall_value; ?>
        </div>
    </div>
<?php endif; ?>