<?php
    include_partial(
        'global/form',
        array(
            'form'                          => $form,
            'submit_name'                   => 'Update',
            'action_url'                    => url_for('sfGuardUserProfile/update'),
            'back_url'                      => url_for('@homepage'),
            'do_show_common_error_message'  => true,
        )
    )
?>
