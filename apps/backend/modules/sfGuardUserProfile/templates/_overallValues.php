
<?php
    $overall_values = $overall_values->getRawValue();

    $strDuringDateRange = 'during selected date range';
?>

<div class="overall-values-group overall-values-group-first">
    <?php
        include_partial(
            'overallPart',
            array(
                'overall_value' => $overall_values['calls_total'],
                'overall_title' => 'Total calls',
                'overall_help'  => "The total number of received calls $strDuringDateRange",
            )
        );
        include_partial(
            'overallPart',
            array(
                'overall_value' => $overall_values['leads_unique'],
                'overall_title' => 'Unique Leads',
                'overall_help'  => "The total number of unique received calls $strDuringDateRange (first calls)",
            )
        );
    ?>
</div>
<div class="overall-values-group">
    <?php
        include_partial(
            'overallPart',
            array(
                'overall_value' => $overall_values['calls_assigned'],
                'overall_title' => 'Good calls',
                'overall_help'  => "The number of good received calls(phone number belonged to some campaign) $strDuringDateRange",
            )
        );
        include_partial(
            'overallPart',
            array(
                'overall_value' => $overall_values['calls_unassigned_advertiser_owner'],
                'overall_title' => 'Bad calls',
                'overall_help'  => "The number of bad received calls(phone number did not belong to any campaign, belonged to some advertiser) $strDuringDateRange",
            )
        );
        include_partial(
            'overallPart',
            array(
                'overall_value' => $overall_values['calls_unassigned_bionic_owner'],
                'overall_title' => 'Almost Bad calls',
                'overall_help'  => "The number of bad received calls(phone number did not belong to any advertiser, belonged to Bionic) $strDuringDateRange",
            )
        );
    ?>
</div>
<div class="overall-values-group">
    <?php
        include_partial(
            'overallPart',
            array(
                'overall_value' => $overall_values['incoming_duration_avg'],
                'overall_title' => 'Avg Call Duration',
                'overall_help'  => "The average duration of all received incoming calls $strDuringDateRange",
            )
        );
        include_partial(
            'overallPart',
            array(
                'overall_value' => $overall_values['incoming_duration_max'],
                'overall_title' => 'Max Call Duration',
                'overall_help'  => "The maximum duration of all received incoming calls $strDuringDateRange",
            )
        );
        include_partial(
            'overallPart',
            array(
                'overall_value' => $overall_values['incoming_duration_total'],
                'overall_title' => 'Total Calls Duration',
                'overall_help'  => "The total duration of all received incoming calls $strDuringDateRange",
            )
        );
//        include_partial(
//            'overallPart',
//            array(
//                'overall_value' => $overall_values['incoming_duration_round'],
//                'overall_title' => 'Round incoming Duration',
//                'overall_help'  => "The round duration of all received incoming calls $strDuringDateRange",
//            )
//        );
    ?>
</div>
<div class="overall-values-group">
    <?php
         include_partial(
            'overallPart',
            array(
                'overall_value' => $overall_values['duration_incoming_unique'],
                'overall_title' => 'Total Unique Calls Duration',
                'overall_help'  => "The total duration of all received incoming unique calls $strDuringDateRange (some calls)",
            )
        );
    ?>
</div>
<div class="overall-values-group  overall-values-group-last">
    <?php
        include_partial(
            'overallPart',
            array(
                'overall_value' => $overall_values['redialed_duration_avg'],
                'overall_title' => 'Avg Redialed Duration',
                'overall_help'  => "The average duration of all redialed calls received $strDuringDateRange",
            )
        );
        include_partial(
            'overallPart',
            array(
                'overall_value' => $overall_values['redialed_duration_max'],
                'overall_title' => 'Max Redialed Duration',
                'overall_help'  => "The maximum duration of all redialed calls received $strDuringDateRange",
            )
        );
        include_partial(
            'overallPart',
            array(
                'overall_value' => $overall_values['redialed_duration_total'],
                'overall_title' => 'Total Redialed Duration',
                'overall_help'  => "The total duration of all redialed calls received $strDuringDateRange",
            )
        );
//        include_partial(
//            'overallPart',
//            array(
//                'overall_value' => $overall_values['redialed_duration_round'],
//                'overall_title' => 'Round redialed Duration',
//                'overall_help'  => "The round duration of all received redialed calls $strDuringDateRange",
//            )
//        );
    ?>
</div>
