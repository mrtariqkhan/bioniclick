
<?php
$almostBadCalls = $overall_values->getRaw('calls_unassigned_bionic_owner');
$strGraphAndCallsKindNotAll = is_null($almostBadCalls) ? '' : "This graph does not work with 'Almost Bad' calls. ";
$strGraphAndCallsKindNotAll .= "This graph works with 'Good' and 'Bad' calls. ";

$strGraphAndCallsKindAll = 'This graph works with all calls. ';


$charts_owner = $charts_owner->getRawValue();

$suffix = '';
$ownerParams = array();
if (!empty($charts_owner['advertiser'])) {
    $ownerParams['advertiser_id'] = $charts_owner['advertiserId'];
    $suffix = '_for_advertiser';
}
if (!empty($charts_owner['company'])) {
    $ownerParams['company_id'] = $charts_owner['companyId'];
    $suffix = '_for_company';
}
?>
 <?php
        $advcss = ' ';
        $srccss = ' ';
            if($current_user->isAdvertiserUser()){
                $advcss = 'display:none;';
                $srccss = ' ';
            }
            else{
                 $advcss = 'width: 465px; float: left;';
                 $srccss = 'width: 465px;float: left;margin-left: 20px;';
            }
?>
<div class="chart-block-very-large block-shadow">
    <div class="home-floating clearfix">
        <div class="help-charts-for">
            <?php if ($charts_owner['isForChild']): ?>
                <?php
                $obj = null;
                if ($charts_owner['advertiser']):
                    $obj = $charts_owner['advertiser'];
                elseif ($charts_owner['company']):
                    $obj = $charts_owner['company'];
                endif;
                ?>
            
                <?php
                include_partial(
                        'global/chartsBreadCrumb', array(
                    'object' => $obj,
                    'user' => $current_user,
                        )
                );
                ?>

            <?php endif; ?>
        </div>
    
     <div class="filter-form-wrap-home" style="<?php echo $advcss; ?>">
            <h1>Advertiser</h1>
              <div class="filter-form-wraper-home1">
                  
                <?php
                include_partial(
                        'global/home_advertiser_form', array(
                    'advertisers' => $advertisers,
                    'selecteduser' => $charts_owner['advertiserId'],
                        )
                );
                ?>

                  
            </div>
        </div>
        
    <div class="filter-form-wrap-home" style="<?php echo $srccss; ?>">
            <h1>Search</h1>
            <div class="filter-form-wraper-home">
                <?php
                include_partial(
                        'global/form_ajax', array(
                    'form' => $filter_form,
                    'action_url' => url_for("profile_filter$suffix", $ownerParams),
                    'submit_name' => 'Go!',
                    'back_url' => '',
                    'form_class_name' => 'ajax-filter',
                    'cancel_attributes' => array('class' => 'ajax-form-cancel')
                        )
                );
                ?>
             </div>
     </div>         



        <div id='charts'>
            <div class="chart-block-double">
                <div class="overall-values-container chart-block">
                    <h3>
                        <div class="chart-block-title">
<?php echo $chart_titles_info['overall_values']; ?>
                        </div>
                            <?php
                            include_partial(
                                    'global/timeRange', array(
                                'date_info' => $chart_date_info['overall_values'],
                                'className' => 'chart-time-range',
                                    )
                            );
                            ?>
                    </h3>
                        <?php
                        include_partial(
                                'overallValues', array(
                            'overall_values' => $overall_values,
                                )
                        );
                        ?>
                </div>

                <div class="chart-block pie-block ajax-filter-container chart-block-calls-by-parameter">
                    <h3>
                        <div
                            class="overall-value-help icon help"
                            title="<?php echo "$strGraphAndCallsKindAll
     1). Calls By Call Status:     amount of calls (all calls) for each possible defined call Status.
     2). Calls By Referrer Source: amount of calls (calls with suitable not old visitor analytics) for groups by defined Referrer Source.
     3). Calls By Search Type:     amount of calls (calls with suitable not old visitor analytics) for groups by defined Search Types.
     4). Calls By Url:             amount of calls (calls with suitable not old visitor analytics) for groups by defined URLs.
     5). Calls By Keyword:         amount of calls (calls with suitable not old visitor analytics) for groups by defined Keywords."; ?>"
                            >
                            &nbsp;
                        </div>
                        <div class="chart-block-title">
<?php echo $chart_titles_info['calls_by']; ?>
                        </div>
<?php
include_partial(
        'global/timeRange', array(
    'date_info' => $chart_date_info['calls_by'],
    'className' => 'chart-time-range',
        )
);
?>
                    </h3>
                        <?php
                        include_partial(
                                'global/form_ajax', array(
                            'form' => $calls_by_parameter_filter_form,
                            'action_url' => url_for("profile_calls_by_parameter_filter$suffix", $ownerParams),
                            'submit_name' => false,
                            'back_url' => '',
                            'form_class_name' => 'ajax-filter ajax-chart-calls-by-parameter-filter',
                            'cancel_attributes' => array('class' => 'ajax-form-cancel')
                                )
                        );
                        ?>
                    <div
                        id="chart-calls-by-parameter"
                        class="chart"
                        data_load_url="<?php echo url_for("profile_ajax_chart_data_calls_by_parameter$suffix", $ownerParams); ?>"
                        >
                    </div>
                    <div
                        id="chart-calls-by-parameter-legend"
                        class="chart-legend"
                        >
                    </div>
                    <div id="chart-calls-by-parameter-hover"></div>
                </div>
            </div>
            <div class="chart-block-double">
                <div class="chart-block chart-block-left">
                    <h3>
                        <div
                            class="overall-value-help icon help"
                            title="<?php echo "$strGraphAndCallsKindNotAll
    '2 Calls, 67' means that 67 different phone numbers have called twice from, no more calls have been from them. So there is 67*2=134 calls in this category."; ?>"
                            >
                            &nbsp;
                        </div>
                        <div class="chart-block-title">
                            <?php echo $chart_titles_info['unique_callers']; ?>
                        </div>
<?php
include_partial(
        'global/timeRange', array(
    'date_info' => $chart_date_info['unique_callers'],
    'className' => 'chart-time-range',
        )
);
?>
                    </h3>
                    <div id="unique-callers" class="chart">
                        <!--<?php //$chart_unique_callers->renderChart(); ?>-->
                        <?php include_component('chart', 'generate', array('chart_options' => $chart_unique_callers_options)); ?>
                    </div>
                </div>
                <div class="chart-block pie-block">
                    <h3>
                        <div
                            class="overall-value-help icon help"
                            title="<?php echo "$strGraphAndCallsKindAll
    1). ''name' 123 (9%)' means that there were 123 calls to this client at all. And it is 9% from all calls to your company.
    2). 'Unassigned 5 (1%)' means that there were 5 calls to phone number which belonged to your company which did not belong to any of your clients. And it is 1% from all calls to your company."; ?>"
                            >
                            &nbsp;
                        </div>
                        <div class="chart-block-title">
                            <?php echo $chart_titles_info['top_clients']; ?>
                        </div>
                            <?php
                            include_partial(
                                    'global/timeRange', array(
                                'date_info' => $chart_date_info['top_clients'],
                                'className' => 'chart-time-range',
                                    )
                            );
                            ?>
                    </h3>
                        <?php
                        include_partial(
                                'global/form_ajax', array(
                            'form' => $top_clients_filter_form,
                            'action_url' => url_for("profile_top_clients_filter$suffix", $ownerParams),
                            'submit_name' => false,
                            'back_url' => '',
                            'form_class_name' => 'ajax-filter ajax-chart-top-clients-filter',
                            'cancel_attributes' => array('class' => 'ajax-form-cancel')
                                )
                        );
                        ?>
                    <div
                        id="chart-top-clients"
                        class="chart"
                        data_load_url="<?php echo url_for("profile_ajax_chart_data_top_clients$suffix", $ownerParams); ?>"
                        >
                    </div>
                    <div
                        id="chart-top-clients-legend"
                        class="chart-legend"
                        >
                    </div>
                    <div id="chart-top-clients-hover"></div>
                </div>
            </div>
            <div class="clear"></div>
            <div class="ajax-filter-container chart-block">
                <h3>
                    <div
                        class="overall-value-help icon help"
                        title="<?php echo "$strGraphAndCallsKindNotAll
    1). 'Unique callers, 1998 Oct 23, 35' means that 35 first calls were from different phone numbers in 1998 Oct 23.
    2). 'Repeat callers, 1998 Oct 23, 4' means that 4 not first calls from phone numbers in 1998 Oct 23.
    3). 'Total callers, 1998 Oct 23, 39' means that 39 calls were at all in 1998 Oct 23."; ?>"
                        >
                        &nbsp;
                    </div>
                    <div class="chart-block-title">
<?php echo $chart_titles_info['call_summary']; ?>
                    </div>
                    <div class="reloadable-chart-time-range">
                        <?php
                        include_partial(
                                'global/timeRange', array(
                            'date_info' => $chart_date_info['call_summary'],
                            'className' => 'chart-time-range',
                                )
                        );
                        ?>
                    </div>
                </h3>

                        <?php
                        include_partial(
                                'global/form_ajax', array(
                            'form' => $call_summary_filter_form,
                            'action_url' => url_for("profile_call_summary_filter$suffix", $ownerParams),
                            'submit_name' => false,
                            'back_url' => '',
                            'form_class_name' => 'ajax-filter ajax-chart-call-summary-filter',
                            'cancel_attributes' => array('class' => 'ajax-form-cancel')
                                )
                        );
                        ?>
                <div
                    class="reloadable-chart"
                    data_load_url="<?php echo url_for("profile_ajax_chart_data_call_summary$suffix", $ownerParams); ?>"
                    >
                <?php include_component('chart', 'generate', array('chart_options' => $chart_call_summary_options)); ?>
                    <!-- <div
                        id="chart-call-summary"
                        class="chart"
                    >
                <?php //$chart_call_summary->renderChart(); ?>
                    </div> -->
                </div>
            </div>
            <div class="clear"></div>
            <div class="ajax-filter-container chart-block">
                <h3>
                    <div
                        class="overall-value-help icon help"
                        title="<?php echo "$strGraphAndCallsKindNotAll
     1). Call Statuses Per Month Day: amount of calls for each status for each month day. 
     2). Call Statuses Per Week Day: amount of calls for each status for each week day. 
     3). Call Statuses Per Hour: amount of calls for each status for each hour."; ?>"
                        >
                        &nbsp;
                    </div>
                    <div class="chart-block-title">
<?php echo $chart_titles_info['call_status']; ?>
                    </div>
<?php
include_partial(
        'global/timeRange', array(
    'date_info' => $chart_date_info['call_status'],
    'className' => 'chart-time-range',
        )
);
?>
                </h3>

                    <?php
                    include_partial(
                            'global/form_ajax', array(
                        'form' => $call_status_filter_form,
                        'action_url' => url_for("profile_call_status_filter$suffix", $ownerParams),
                        'submit_name' => false,
                        'back_url' => '',
                        'form_class_name' => 'ajax-filter ajax-chart-call-status-filter',
                        'cancel_attributes' => array('class' => 'ajax-form-cancel')
                            )
                    );
                    ?>
                <div
                    class="reloadable-chart"
                    data_load_url="<?php echo url_for("profile_ajax_chart_data_call_status$suffix", $ownerParams); ?>"
                    >
                    <!-- <div
                        id="chart-call-summary"
                        class="chart"
                    >
                <?php //$chart_call_status->renderChart(); ?>
                    </div> -->
                <?php include_component('chart', 'generate', array('chart_options' => $chart_call_status_options)); ?>
                </div>
            </div>
            <div class="clear"></div>
        </div>
    </div>   
</div>