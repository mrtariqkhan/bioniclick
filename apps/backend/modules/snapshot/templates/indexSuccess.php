<?php echo link_to('Download Snapshot', 'snapshot/download', array()); ?>
<div id="send-mail">
    <form id='send-mail-form' method='post' >
        <strong>Email Address:</strong> <input id ="mail-addresses" name="to-email" type="text"><br>
        <strong>Subject      :</strong> <input id ="mail-subject" name="subject" type="text" value="Snapshot"><br>
        <textarea id='mail-message-content' cols="25" name="body"></textarea><br>
        <strong>Attached PDF preview: </strong><a href='<?=$snapshotPath?>' target="_blank"> snapshot</a><br>
        <input id='send-mail-button' type="submit" value="Send"></input>
    </form>
</div>