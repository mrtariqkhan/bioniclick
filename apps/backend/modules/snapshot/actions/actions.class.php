<?php

/**
 * api actions.
 *
 * @package    bionic
 * @subpackage api
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class snapshotActions extends GeneralActions
{	
	public function preExecute(){
		parent::preExecute();
		
		// $this->getResponse()->addJavascript('/js/jquery/jquery.validate.js', 'last');

		$this->getResponse()->addStylesheet('/css/project/reset.css', 'last');
		$this->getResponse()->addStylesheet('/css/project/snapshot.css', 'last');

		$this->getResponse()->addJavascript('/js/jquery/jquery.cookie.js', 'last');
		
		$this->getResponse()->addJavascript('/js/libs/actb/actb.js', 'last');
		$this->getResponse()->addJavascript('/js/libs/actb/common.js', 'last');

		$this->getResponse()->addJavascript('/js/project/mail_handler.js', 'last');
		
	}
	public function executeIndex(sfWebRequest $request){
		$this->user = $this->getUser()->getGuardUser();
		$timestamp = time();
		$this->snapshotPath = "/uploads/temp/screenshot".$this->user->getId().".pdf?". $timestamp;
	}

	public function executeExport(sfWebRequest $request){
		$fileName = "screenshot" . $this->user->getId();
		$img = $_POST['img'];
		$data = base64_decode($img);
		$file = "uploads/temp/" . $fileName;
		$status = 'unable to download file';	
		$success = file_put_contents($file . '.png', $data);
		if($success){
			$status = 'success';
			$baseUrl = sfConfig::get('sf_root_dir').'/web/';
			$file = $baseUrl . $file;
			exec('rm ' . $file . '.pdf');
			exec('convert -quality 100 '. $file . '.png ' . $file . '.pdf');
		}
			
		return $this->renderText(json_encode(array('status' => $status)));
	}
	public function executeDownload(sfWebRequest $request){
		$fileName = "screenshot" . $this->user->getId() . '.pdf';
		$file = "uploads/temp/" . $fileName;
		return $this->downloadFile($file, 'application/x-pdf', 'screenshot.pdf');
	}
	
}
