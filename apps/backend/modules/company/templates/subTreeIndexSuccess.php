<div class="inner">
    <h2>SubCompany List of "<?php echo $parent_company; ?>" company</h2>
    <div class="account-wrap clearfix">        
        <div class="ajax-filter-container">
<?php /*
            <?php if ($permissions['can_filter']): ?>
                <div class="filter-form-wrap">
                    <h1>Search</h1>
                    <div class="filter-form-wraper">
                        <?php
                            include_partial(
                                'global/form_ajax',
                                array(
                                    'form'              => $form_filter,
                                    'action_url'        => url_for('company_filter'),
                                    'submit_name'       => 'Go!',
                                    'back_url'          => '',
                                    'form_class_name'   => 'ajax-filter',
                                    'cancel_attributes' => array('class' => 'ajax-form-cancel')
                                )
                            );
                        ?>
                    </div>
                </div>
            <?php endif; ?>
*/
?>

            <div class="ajax-pagable">
                <?php
                    include_partial(
                        'subTreeList',
                        array(
                            'pager'                     => $pager,
                            'branch_data'               => $branch_data,
                            'permissions'               => $permissions,
                            'permissions_user'          => $permissions_user,
                            'permissions_advertiser'    => $permissions_advertiser,
                            'bionic_id'                 => $bionic_id,
                            'parent_company'            => $parent_company,
//                            'permissions_twilio_incoming_number_pool'   => $permissions_twilio_incoming_number_pool,
                        )
                    );
                ?>
            </div>
        </div>
    </div>
</div>

<?php
    $a_options = array();

    $ifShowUpdateLinksParent = WasDeletedHelper::ifCanUpdate($parent_company->getRawValue());
    if ($ifShowUpdateLinksParent) {
        if ($permissions['can_update']) {
            if ($packages_amount > 0) {
                $a_options['New'] = array(
                    'href'  => url_for('sub_tree_company_new', array('parent_company_id' => $parent_company->getId())),
                    'class' => "icon new ajax-form-load"
                );
            } else {
                echo 'You cannot create new companies because Your company has no packages. Create package firstly.';
            }
        }
    }

    if ($permissions['can_read']) {
        if ($is_company_user) {
            $levelParentCompany = $parent_company->getLevel();
            if ($level_user == $levelParentCompany) {
                $url = url_for('company_show', array('id' => $parent_company->getId()));
            } else {
                $grand_parent_company_id = $parent_company->getParentCompanyId();
                $url = url_for('sub_tree_company_index', array('parent_company_id' => $grand_parent_company_id));
            }
            $a_options['Back'] = array(
                'href'  => $url,
                'class' => "icon back ajax-load"
            );
        }
    }

    echo icon_links($a_options);
?>
