<?php 
    include_partial(
        'list',
        array(
            'pager'                                     => $pager,
            'branch_data'                               => $branch_data,
            'permissions'                               => $permissions,
            'permissions_user'                          => $permissions_user,
            'permissions_package'                       => $permissions_package,
            'permissions_twilio_incoming_number_pool'   => $permissions_twilio_incoming_number_pool,
            'bionic_id'                                 => $bionic_id,
        )
    );
?>