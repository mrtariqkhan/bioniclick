<?php $object = $form->getObject(); ?>

<h1>Create new company for "<?php echo $parent_company; ?>" company</h1>

<div class="inner">

    <?php $parentCompanyId = $parent_company->getId(); ?>
    <?php $urlAction = url_for('sub_tree_company_create', array('parent_company_id' => $parentCompanyId)); ?>
    <?php $urlAfter = url_for('sub_tree_company_index', array('parent_company_id' => $parentCompanyId)); ?>

    <?php
        $parent_company = $parent_company->getRawValue();
        $levelCompany = $parent_company->getLevel();
        switch ($levelCompany) {
            case CompanyTable::$LEVEL_BIONIC:
                $reloadTreeKey = TreeNodeBranching::generateTreeNodeKeyResellers($parent_company);
                break;
            case CompanyTable::$LEVEL_RESELLER:
                $reloadTreeKey = TreeNodeBranching::generateTreeNodeKeyAgencies($parent_company);
                break;
            default:
                $reloadTreeKey = '';
        }
    ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'submit_name'       => 'Create',
                'back_url'          => $urlAfter,
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel'),
                'form_special_data' => $reloadTreeKey,
            )
        );
    ?>
</div>
