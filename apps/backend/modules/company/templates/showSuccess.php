<?php use_helper('Link'); ?>

<?php $id = $company->getId(); ?>
<?php $ifShowUpdateLinks = WasDeletedHelper::ifCanUpdate($company->getRawValue()); ?>

<?php
    $a_options = array();

    if ($ifShowUpdateLinks) {
        if ($permissions['can_update']) {
            $a_options['Edit company'] = array(
                'href' => url_for('sub_tree_company_edit', array('id' => $id)),
                'class' => 'main icon edit ajax-load'
            );
        }
    }
    
    $a_options['Orders'] = array(
        'href' => url_for('incomingNumbersQueue_indexCompany', array('company_id' => $id)),
        'class' => 'main icon orders ajax-load'
    );

    if ($permissions_finance['can_read']) {
        $a_options['Usage Report'] = array(
            'href' => url_for('finance_show_sub_tree_for_company', array('id' => $id)),
            'class' => 'main icon finance-position ajax-load'
        );
    }

    $a_options['Charts'] = array(
        'href' => url_for('homepage_for_child_company', array('company_id' => $id)),
        'class' => 'main icon icon-charts'
    );

    echo icon_links($a_options, array('class' => 'icon-links clearfix'));
?>
<h1><?php echo $company->getName(); ?></h1>

<h2>Dynamic Phone Number List</h2>
<div class="ajax-pagable">
    <?php
        include_component(
            'advertiserIncomingNumberPool',
            'listForCompany',
            array(
                'pager'         => $pagerIncoming,
                'branch_data'   => $branch_data_incoming,
                'company'       => $company,
            )
        );
    ?>
</div>


<?php
    $a_options = array();

    if ($permissions['can_read']) {
        if (!empty($parent_company_id)) {
            $a_options['Back'] = array(
                'href'  => url_for('sub_tree_company_index', array('parent_company_id' => $parent_company_id)),
                'class' => "icon back ajax-load"
            );
        }
    }

    echo icon_links($a_options);
?>