<?php $object = $form->getObject(); ?>

<h1>
    Edit "<?php echo $object; ?>" company
</h1>

<div class="inner">

    <?php $id = $object->getId(); ?>
    <?php $urlAction = url_for('sub_tree_company_update', array('id' => $id)); ?>
    <?php $urlAfter = url_for('company_show', array('id' => $id)); ?>

    <?php
        $reloadTreeKey = '';
        $company = $object;
        $levelCompany = $company->getLevel();
        if ($level_user == $levelCompany) {
            $reloadTreeKey = TreeNode::generateTreeNodeKeyRoot();
        } else {
            $parent_company = $object->getParentCompany(); 
            switch ($levelCompany) {
                case CompanyTable::$LEVEL_RESELLER:
                    $reloadTreeKey = TreeNodeBranching::generateTreeNodeKeyResellers($parent_company);
                    break;
                case CompanyTable::$LEVEL_AGENCY:
                    $reloadTreeKey = TreeNodeBranching::generateTreeNodeKeyAgencies($parent_company);
                    break;
            }
        }
    ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'submit_name'       => 'Update',
                'back_url'          => $urlAfter,
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel'),
                'form_special_data' => $reloadTreeKey,
            )
        );
    ?>
</div>
