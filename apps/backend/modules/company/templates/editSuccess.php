<?php $object = $form->getObject(); ?>

<h1>
    Edit company
</h1>

<div class="inner">

    <?php $urlAction = url_for('company_update', array('id' => $object->getId())); ?>
    <?php $urlAfter = url_for('company_index'); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'submit_name'       => 'Update',
                'back_url'          => $urlAfter,
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel')
            )
        );
    ?>
</div>
