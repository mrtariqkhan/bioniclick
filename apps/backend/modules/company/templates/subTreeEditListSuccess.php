<?php $object = $form->getObject(); ?>

<h1>
    Edit "<?php echo $object; ?>" company
</h1>

<div class="inner">

    <?php $id = $object->getId(); ?>
    <?php $parent_company_id = $object->getParentCompanyId(); ?>
    <?php $urlAction = url_for('sub_tree_company_update_list', array('id' => $id)); ?>
    <?php $urlAfter = url_for('sub_tree_company_index', array('parent_company_id' => $parent_company_id)); ?>

    <?php
        $reloadTreeKey = '';
        if (!$object->isBionicCompany()) {
            $parent_company = $object->getParentCompany(); 
            $levelParentCompany = $parent_company->getLevel();
            switch ($levelParentCompany) {
                case CompanyTable::$LEVEL_BIONIC:
                    $reloadTreeKey = TreeNodeBranching::generateTreeNodeKeyResellers($parent_company);
                    break;
                case CompanyTable::$LEVEL_RESELLER:
                    $reloadTreeKey = TreeNodeBranching::generateTreeNodeKeyAgencies($parent_company);
                    break;
            }
        }
    ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $urlAction,
                'submit_name'       => 'Update',
                'back_url'          => $urlAfter,
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel'),
                'form_special_data' => $reloadTreeKey,
            )
        );
    ?>
</div>
