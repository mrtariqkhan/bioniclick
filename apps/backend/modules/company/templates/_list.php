<?php use_helper('Link'); ?>
<?php use_helper('Pager'); ?>

<?php include_partial('global/flash_message', array('flash_name' => 'message')); ?>

<?php $headers = $branch_data['headers']; ?>
<?php $raws    = $branch_data['raws']; ?>

<?php
    $pagerHtmlString = pager(
        $pager->getRawValue(),
        sfConfig::get('app_pager_max_per_pages'),
        url_for('company_list'),
        null,
        'ajax-pager'
    );
?>

<p>
    <?php
        include_partial(
            'global/pager_list_header',
            array(
                'pager' => $pager,
                'pagerHtmlString' => $pagerHtmlString
            )
        );
    ?>
</p>

<?php
    $list_buttons = array(
        'Purchase numbers' => array(
            'class' => 'icon purchase-numbers ajax-form-load'
            // icon purchase-numbers ajax-form-load
        ),
        'New user' => array(
            'class' => 'icon new-user ajax-form-load'
            // icon new-user ajax-form-load
        ),
        'New package' => array(
            'class' => 'icon new-package ajax-form-load'
            // icon new-user ajax-form-load
        ),
        'Edit' => array(
            'class' => 'icon edit ajax-form-load'
            // icon edit ajax-form-load
        ),
        'Delete' => array(
            'class' => 'icon delete'
        )
    );

    $legend_buttons = array();
?>

<div class="table-block">
    <table class="table">
        <thead>
            <tr>
                <th class="first">&nbsp;</th>
                <?php foreach ($headers as $headerTitle): ?>
                    <th><?php echo $headerTitle; ?></th>
                <?php endforeach; ?>
                <th class="last">&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php $i = $pager->getFirstIndice(); ?>
            <?php foreach ($raws as $id => $raw): ?>
                <tr class="<?php echo ($i%2) ? 'odd' : 'even'; ?>">

                    <td><?php echo $i; ?></td>

                    <?php $raw = $raw->getRawValue(); ?>
                    <?php foreach ($raw as $key => $cell): ?>
                        <?php $value = (WasDeletedHelper::KEY == $key) ? WasDeletedHelper::getAsString($cell) : $cell; ?>
                        <td><?php echo $value; ?></td>
                    <?php endforeach; ?>

                    <td>
                        <?php $urlAfter = url_for('company_index', array()); ?>
                        <?php
                            $a_options = array();

                            $ifShowUpdateLinks = WasDeletedHelper::ifCanUpdateAsArray($raw);
                            if ($ifShowUpdateLinks) {

                                if ($permissions['can_update']) {
                                    $a_options['Edit'] = $list_buttons['Edit'];
                                    $a_options['Edit']['href'] = url_for('company_edit', array('id' => $id));

                                    $legend_buttons['Edit'] = & $list_buttons['Edit'];
                                }

                                if ($permissions_user['can_create']) {
                                    $a_options['New user'] = $list_buttons['New user'];
                                    $a_options['New user']['href'] = url_for(
                                        'customer_company_user_new',
                                        array('company_id' => $id)
                                    );

                                    $legend_buttons['New user'] = & $list_buttons['New user'];
                                }

                                if ($permissions_package['can_create']) {
                                    $linkName = 'New package';
                                    $a_options[$linkName] = $list_buttons[$linkName];
                                    $a_options[$linkName]['href'] = url_for(
                                        'customer_company_package_new',
                                        array('company_id' => $id)
                                    );

                                    $legend_buttons[$linkName] = & $list_buttons[$linkName];
                                }

                                if ($bionic_id == $id) {
                                    if ($permissions_twilio_incoming_number_pool['can_update']) {

                                        $companyLevelName = CompanyTable::findCompanyLevelById($id);
                                        $title = "Purchase new numbers for $companyLevelName from VoIP server";

                                        $a_options[$title] = $list_buttons['Purchase numbers'];
                                        $a_options[$title]['href'] = url_for(
                                            'twilio_incoming_phone_number_new_for_bionic'
                                        );
                                        $legend_buttons[$title] = & $list_buttons['Purchase numbers'];
                                    }
                                }

                                if ($permissions['can_delete']) {
                                    $a_options['Delete'] = link_to_ajax_delete(
                                        '',
                                        url_for('company_delete', array('id' => $id)),
                                        array(
                                            'title' => 'Delete',
                                            'class' => 'icon delete',
                                            'after_url' => $urlAfter,
                                        )
                                    );

                                    $legend_buttons['Delete'] = & $list_buttons['Delete'];
                                }
                            }

                            echo icon_links($a_options, array('class' => 'icon-links clearfix'), false);
                        ?>
                    </td>
                </tr>
                <?php ++$i; ?>
            <?php endforeach; ?>
        </tbody>
    </table>
</div>

<?php
    include_partial(
         'global/legend',
          array(
            'legend_buttons'  => $legend_buttons,
          )
    ); 
?>

<?php echo $pagerHtmlString; ?>