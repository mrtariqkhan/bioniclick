<?php

/**
 * company actions.
 *
 * @package    bionic
 * @subpackage company
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class companyActions extends GeneralApplyActions {

    public function preExecute() {

        $this->tableName = 'Company';

        parent::preExecute();
    }

    public function executeIndex(sfWebRequest $request) {

        $this->executeList($request);
        $this->setTemplate('index');
        
        $filterValues = $this->getFilter();
        $this->form_filter = $this->getFilterForm();
        $this->form_filter->setDefaults($filterValues);

        $this->packages_amount = PackageTable::countByUser($this->user);
    }

    public function executeList(sfWebRequest $request) {

        $filterValues = $this->getFilter();
        $filterForm = $this->getFilterForm();
        $queryParams = $filterForm->getFilteredValues($filterValues, true);

        $specialQuery = CompanyTable::createQueryIndex($this->user, $queryParams);
        $this->pager = parent::initPager($request, $specialQuery);
        $list = $this->pager->getResults();

        $objectFieldsHeaders = array(
            'getName'               => 'Name',
            'getUrl'                => 'Url',
            'getDefaultPhoneNumber' => 'Contact number',
            'getPackage'            => 'Package',
        );
        WasDeletedHelper::addHeader($this->user, $objectFieldsHeaders);

        $escapeHeaders = array(
            CompanyTable::$LEVEL_NAME_ADVERTISER,
        );
        $branchHelperCompany = new BranchHelperCompany();
        $this->branch_data = $branchHelperCompany->makeRaws(
            $this->user,
            $list,
            $objectFieldsHeaders,
            $escapeHeaders
        );

        $this->bionic_id = CompanyTable::getBionicId();

        //Permissions for user managing at company list
        $securityHelperUser = new SecurityHelper($this, 'sfGuardUser');
        $this->permissions_user = $securityHelperUser->checkBasePermissions();

        //Permissions for package managing at company list
        $securityHelperPackage = new SecurityHelper($this, 'package');
        $this->permissions_package = $securityHelperPackage->checkBasePermissions();

        //Permissions for twilioIncomingNumberPool managing at company list
        $securityHelperTINP = new SecurityHelper($this, 'twilioIncomingNumberPool');
        $this->permissions_twilio_incoming_number_pool = $securityHelperTINP->checkBasePermissions();
    }

    public function executeSubTreeIndex(sfWebRequest $request) {

        $this->executeSubTreeList($request);
        $this->setTemplate('subTreeIndex');
        
//        $filterValues = $this->getFilter();
//        $this->form_filter = $this->getFilterForm();
//        $this->form_filter->setDefaults($filterValues);

        $this->packages_amount = PackageTable::countByUser($this->user);
        $this->is_company_user = $this->user->isCompanyUser();
        $this->level_user = $this->user->getLevel();
    }

    public function executeSubTreeList(sfWebRequest $request) {

//        $filterValues = $this->getFilter();
//        $filterForm = $this->getFilterForm();
//        $queryParams = $filterForm->getFilteredValues($filterValues, true);
        $queryParams = array();

        $company = $request->getParameter('parent_company_id', null);
        $parentCompany = CompanyTable::getInstance()->find($company);
        $this->forward404If(empty($parentCompany), 'Company has not been found.');

        $this->checkIsAllowableObject($parentCompany, false);


        $specialQuery = CompanyTable::createQueryFirstByParentCompanyAndUser($this->user, $parentCompany, $queryParams);
        $this->pager = parent::initPager($request, $specialQuery);
        $list = $this->pager->getResults();

        $objectFieldsHeaders = array(
            'getName'               => 'Name',
            'getUrl'                => 'Url',
            'getDefaultPhoneNumber' => 'Contact number',
            'getPackage'            => 'Package',
        );
        WasDeletedHelper::addHeader($this->user, $objectFieldsHeaders);

        $escapeHeaders = array(
            CompanyTable::$LEVEL_NAME_ADVERTISER,
        );
        $branchHelperCompany = new BranchHelperCompany();
        $this->branch_data = $branchHelperCompany->makeRaws($this->user, $list, $objectFieldsHeaders, $escapeHeaders);

        $this->bionic_id = CompanyTable::getBionicId();

        //Permissions for user managing at company list
        $securityHelperUser = new SecurityHelper($this, 'sfGuardUser');
        $this->permissions_user = $securityHelperUser->checkBasePermissions();

        //Permissions for twilioIncomingNumberPool managing at company list
        $securityHelperTINP = new SecurityHelper($this, 'twilioIncomingNumberPool');
        $this->permissions_twilio_incoming_number_pool = $securityHelperTINP->checkBasePermissions();

        //Permissions for advertisers at company list
        $securityHelperAdvertiser = new SecurityHelper($this, 'advertiser');
        $this->permissions_advertiser = $securityHelperAdvertiser->checkBasePermissions();

        $this->parent_company = $parentCompany;
    }

    public function executeSubTreeEditList(sfWebRequest $request) {

        $this->executeSubTreeEdit($request);
        $this->setTemplate('subTreeEditList');
    }

    public function executeSubTreeEdit(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $level = $object->getLevel();
        $parentCompany = ($level == CompanyTable::$LEVEL_BIONIC) ? $this->user->getCompany() : $object->getParentCompany();
        $this->form = $this->configureSubTreeForm($parentCompany, $object);
        $this->level_user = $this->user->getLevel();
    }

    public function executeSubTreeUpdate(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $level = $object->getLevel();
        $parentCompany = ($level == CompanyTable::$LEVEL_BIONIC) ? $this->user->getCompany() : $object->getParentCompany();
        $form = $this->configureSubTreeForm($parentCompany, $object);

        return $this->processAjaxForm($request, $form);
    }

    public function executeSubTreeUpdateList(sfWebRequest $request) {
        return $this->executeSubTreeUpdate($request);
    }

    public function executeSubTreeNew(sfWebRequest $request) {

        $companyId = $request->getParameter('parent_company_id', null);
        $parentCompany = CompanyTable::getInstance()->find($companyId);
        $this->forward404If(empty($parentCompany), "Company has not been found");

        $this->checkIsAllowableObject($parentCompany);

        $this->form = $this->configureSubTreeForm($parentCompany);
        $this->parent_company = $parentCompany;
    }

    public function executeSubTreeCreate(sfWebRequest $request) {

        $companyId = $request->getParameter('parent_company_id', null);
        $parentCompany = CompanyTable::getInstance()->find($companyId);
        $this->forward404If(empty($parentCompany), "Company has not been found");

        $this->checkIsAllowableObject($parentCompany);

        $form = $this->configureSubTreeForm($parentCompany);

        return $this->processAjaxForm($request, $form);
    }

    public function executeShow(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object, false);

        $this->company = $object;
        $this->parent_company_id = null;
        if ($this->user->isCompanyUser()) {
            $levelUser = $this->user->getLevel();
            $levelCompany = $object->getLevel();
            $this->parent_company_id = ($levelUser == $levelCompany) ? null : $object->getParentCompanyId();
        }

        $filterValues = array();
        $userAdmin = $object->getAdmin();
        $specialQuery = AdvertiserIncomingNumberPoolTable::createIndexQueryByUser(
            $userAdmin,
            $filterValues
        );

        $this->pagerIncoming = parent::initPager($request, $specialQuery);
        $list = $this->pagerIncoming->getResults();

        $objectFieldsHeaders = array(
            'getTwilioIncomingPhoneNumber'  => 'Dynamic Phone Number',
            'getCampaign'                   => 'Campaign assigned to',
            'getIsDefault'                  => 'Is Default',
        );

        $branchHelper = new BranchHelper();
        $this->branch_data_incoming = $branchHelper->makeRaws($this->user, $list, $objectFieldsHeaders);

        $securityHelperFinance = new SecurityHelper($this, 'finance');
        $this->permissions_finance = $securityHelperFinance->checkBasePermissions();
    }

    public function executeNew(sfWebRequest $request) {
        $this->form = $this->configureForm();
    }

    public function executeCreate(sfWebRequest $request) {

        $form = $this->configureForm();

        return $this->processAjaxForm($request, $form);
    }

    public function executeEdit(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $this->form = $this->configureForm($object);
        $this->level_user = $this->user->getLevel();
    }

    public function executeUpdate(sfWebRequest $request) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object);

        $form = $this->configureForm($object);

        return $this->processAjaxForm($request, $form);
    }

    public function executeDelete(sfWebRequest $request) {
        return $this->processAjaxDelete($request);
    }

    protected function deleteObject($object) {
        $object->deleteAsNodeByUser($this->user);
    }

    public function executeEditCurrent(sfWebRequest $request) {
        $this->form = $this->configureCurrentForm();
    }

    public function executeUpdateCurrent(sfWebRequest $request) {

        $form = $this->configureCurrentForm();

        return $this->processAjaxForm($request, $form);
    }

    protected function saveValidForm($validForm) {

        $wasNew = $validForm->isNew();

        $savedObject = $validForm->save();

        if ($wasNew && !sfGuardUserProfileTable::ifUseSimpleUserCreation()) {
            try {
                $company = $validForm->getObject();
                $user = $company->getAdmin();
                $profile = $user->getProfile();
                $this->sendVerificationMail($profile);
            } catch (Exception $e) {
                $company = $validForm->getObject();
                $user = $company->getAdmin();
                $user->delete();
                //TODO:: think if it needs to delete company also
                throw $e;
            }
        }

        return $savedObject;
    }

    public function executeFilter(sfWebRequest $request) {
        $filterForm = $this->getFilterForm();
        return $this->processAjaxFilter($request, $filterForm);
    }

    protected function getFilterForm() {
        $options = $this->getBranchFilterOptions();
        return new CompanyFilterForm(null, $options);
    }

    protected function getBranchFilterOptions() {

        $options = parent::getBranchFilterOptions();
        unset($options[CompanyTable::$LEVEL_NAME_ADVERTISER]);

        return $options;
    }

    protected function configureCurrentForm() {

        $object = $this->user->getCompany();

        $formOptions = array();

        $form = new CompanyFormCurrentEdit($object, $formOptions);
        return $form;
    }

    protected function configureForm($object = null) {

        $formOptions = array(
            CompanyForm::OPTION_PARENT_COMPANY => $this->user->getCompany(),
        );

        $object = empty($object) ? new Company() : $object;

        $form = new CompanyForm($object, $formOptions);
        return $form;
    }

    protected function configureSubTreeForm($parentCompany, $object = null) {

        $formOptions = array(
            CompanyForm::OPTION_PARENT_COMPANY => $parentCompany,
        );

        $object = empty($object) ? new Company() : $object;

        $form = new CompanyForm($object, $formOptions);
        return $form;
    }
}
