<?php

/**
 * finance actions.
 *
 * @package    bionic
 * @subpackage finance
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class financeActions extends GeneralActions {

    public function executeShowSubTreeForAdvertiser(sfWebRequest $request) {

        $this->executeShowSubTreeForAdvertiserList($request);
        $this->setTemplate('showSubTreeForAdvertiser');
    }

    public function executeShowSubTreeForAdvertiserList(sfWebRequest $request) {

        $filterFormArray = $this->getFilterFormArraySpecific(true);
        $this->filter_form = $filterFormArray['form'];

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object, false);

        $this->advertiser_id = $object->getId();

        $invoiceTypeMoney   = InvoicesGeneratorBase::INVOICE_TYPE_HAVE_TO_PAY;
        $invoiceTypeDetails = InvoicesGeneratorBase::INVOICE_TYPE_DETAILS_SUMMARY;

        $range = $this->getRangeForInvoice();

        $invoiceGenerator = new InvoicesGeneratorSummary(
            $object, 
            $invoiceTypeMoney, 
            $invoiceTypeDetails, 
            $range
        );

        $this->data = $invoiceGenerator->generateData();
    }

    public function executeShowSubTreeForCompany(sfWebRequest $request) {

        $this->executeShowSubTreeForCompanyList($request);
        $this->setTemplate('showSubTreeForCompany');
    }

    public function executeShowSubTreeForCompanyList(sfWebRequest $request) {

        $filterFormArray = $this->getFilterFormArraySpecific(true);
        $this->filter_form = $filterFormArray['form'];

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object, false);

        $this->company_id = $object->getId();

        $invoiceTypeMoney   = 
            ($object->getLevel() == CompanyTable::$LEVEL_BIONIC) 
            ? InvoicesGeneratorBase::INVOICE_TYPE_HAVE_TO_GET 
            : InvoicesGeneratorBase::INVOICE_TYPE_HAVE_TO_PAY
        ;
        $invoiceTypeDetails = InvoicesGeneratorBase::INVOICE_TYPE_DETAILS_SUMMARY;

        $range = $this->getRangeForInvoice();

        $invoiceGenerator = new InvoicesGeneratorSummary(
            $object, 
            $invoiceTypeMoney, 
            $invoiceTypeDetails, 
            $range
        );

        $this->data = $invoiceGenerator->generateData();
    }

    public function executeXlsForAdvertiser(sfWebRequest $request) {
        return $this->generateAndReturnInvoiceFile($request);
    }

    public function executeXlsForCompany(sfWebRequest $request) {
        return $this->generateAndReturnInvoiceFile($request);
    }

    public function generateAndReturnInvoiceFile(
        sfWebRequest $request
    ) {

        $object = $this->getRoute()->getObject();
        $this->checkIsAllowableObject($object, false);

        $invoiceTypeMoney   = InvoicesGeneratorBase::INVOICE_TYPE_HAVE_TO_PAY;
        if ($object->getLevel() == CompanyTable::$LEVEL_BIONIC) {
            $invoiceTypeMoney = InvoicesGeneratorBase::INVOICE_TYPE_HAVE_TO_GET;
        }

        $invoiceTypeDetails = $this->getRequestParameter('invoice_type_code', null);
        if (empty($invoiceTypeDetails)) {
            throw new Exception('Invoice Type Details is wrong');
        }

        $range = $this->getRangeForInvoice();

        $fileName = '';
        $pathToFile = GenerateInvoicesTask::generateInvoiceFile(
            $object, 
            $invoiceTypeMoney, 
            $invoiceTypeDetails,
            $range,
            $fileName
        );

        $mimeType = 
            'application/x-excel;'
            . 'application/x-msexcel;'
            . 'application/x-msexcel;'
            . 'application/excel;'
            . 'application/vnd.ms-excel'
        ;

        $invoiceTypeDetailsName = InvoicesGeneratorBase::getTypesDetails()->getTypeName($invoiceTypeDetails);
        $companyName = $object->getName();
        $outFileName = "Usage Report $fileName";

        return $this->downloadFile($pathToFile, $mimeType, $outFileName);
    }

    protected function getRangeForInvoice() {

        $filterFormArray = $this->getFilterFormArraySpecific(true);
        $filterForm     = $filterFormArray['form'];
        $cleanedValues  = $filterFormArray['cleaned'];

        $range = $cleanedValues;
        if (empty($range)) {
            $range = TimeConverter::getThisMonthUnixTimestampRange(TimeConverter::TIMEZONE_UTC);
        }

        return $range;
    }

    //NOTE:: to delete invoices folder
    public function executeRemoveInvoiceDir(sfWebRequest $request) {

        $data = array();
        $excelInvoiceHelper = new ExcelInvoiceHelperFull($data);
        $excelInvoiceHelper->removeFolder();

        die;
    }

    public function executeFilter(sfWebRequest $request) {

        $filterFormArray = $this->getFilterFormArraySpecific();
        $filterForm = $filterFormArray['form'];

        return $this->processAjaxFilter($request, $filterForm);
    }

    protected function getFilterFormArraySpecific($ifSavedIsNeed = false) {

        $options = array(
            'timezone_name' => TimeConverter::TIMEZONE_UTC,
        );

        return $this->getFilterFormArray(
            'FinanceFilterForm',
            array(),
            $options,
            true,
            $ifSavedIsNeed
        );
    }

    protected function configureDateInfo($cleanedValues, $timezoneName) {

        $date_info = array();

        if (array_key_exists('from', $cleanedValues) && !empty($cleanedValues['from'])) {
            $date_info['from'] = TimeConverter::getDateTime($cleanedValues['from'], $timezoneName);
        }

        if (array_key_exists('to', $cleanedValues) && !empty($cleanedValues['to'])) {
            $date_info['to'] = TimeConverter::getDateTime($cleanedValues['to'], $timezoneName);
        }

        return $date_info;
    }
    
    public function executeGenerateInvoicesTask(sfWebRequest $request) {

        return $this->processTask(
            'GenerateInvoicesTask',
            array(),
            array(
                'emails'    => 'false',
                'type'      => 'Pay',
                'level'     => 'Advertiser',
//                'details'   => 'true',
            )
        );
    }
    
    public function executePaymentToBionicTask(sfWebRequest $request) {

        return $this->processTask(
            'PaymentToBionicTask',
            array(),
            array()
        );
    }

}
