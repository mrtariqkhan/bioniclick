
<div class="account-wrap clearfix">
    <div class="ajax-filter-container">
        <?php if ($permissions['can_filter']): ?>
            <div class="filter-form-wrap filter-form-wrap-finance">
                <h1>Search</h1>
                <div class="filter-form-wraper">
                    <?php
                        include_partial(
                            'global/form_ajax',
                            array(
                                'form'              => $filter_form,
                                'action_url'        => url_for('finance_filter'),
                                'back_url'          => '',
                                'submit_name'       => 'Go!',
                                'form_class_name'   => 'ajax-filter-finance',
                                'cancel_attributes' => array('class' => 'ajax-form-cancel')
                            )
                        );
                    ?>
                </div>
            </div>
        <?php endif; ?>

        <div
            class="ajax-filterable"
            data_url="<?php echo $data_url; ?>"
        >

            <?php
                include_partial('table', array(
                    'data'          => $data,
                    'filter_form'   => $filter_form,
                    'permissions'   => $permissions,
                ));
            ?>

        </div>

    </div>
</div>