<?php
    $info = $info->getRawValue();
    $itemNamePrefix     = $prefix;
    $itemNamePostfix    = $postfix;

    $infoTotal = $info[InvoicesGenerator::LEVEL_TOTAL];
    $account = $itemNamePrefix . $infoTotal[InvoicesGenerator::INFO_NAME] . $itemNamePostfix;

    $trClass = ($trNumber%2) ? 'odd' : 'even';
    
    $needNumbersAmount = $infoTotal[InvoicesGenerator::INFO_NEED_NUMBERS_AMOUNT];
    $needCallTimeAmount = $infoTotal[InvoicesGenerator::INFO_NEED_CALL_TIME_AMOUNT];
?>

<tr class="<?php echo $trClass; ?>">
    <td><?php echo $trNumber; ?></td>
    <td><?php echo $account; ?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <!-- <td>&nbsp;</td>
    <td>&nbsp;</td>-->
    <td>&nbsp;</td> 
</tr>

<?php if ($needNumbersAmount): ?>
    <tr class="<?php echo $trClass; ?>">
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><?php echo InvoicesGenerator::INFO_AMOUNT_NUMBERS_LOCAL; ?></td>
        <td class="finance-number">
            <?php echo $infoTotal[InvoicesGenerator::INFO_AMOUNT_NUMBERS_LOCAL]; ?>
        </td>
        <!-- <td class="finance-number">
            <?php echo InvoicesGenerator::getDouble($infoTotal[InvoicesGenerator::INFO_RATE_PER_MONTH_NUMBER_LOCAL]); ?>
        </td>
        <td class="finance-number">
            <?php echo InvoicesGenerator::getDouble($infoTotal[InvoicesGenerator::INFO_PRICE_NUMBERS_LOCAL]); ?>
        </td> -->
        <td>&nbsp;</td>
    </tr>
    <tr class="<?php echo $trClass; ?>">
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><?php echo InvoicesGenerator::INFO_AMOUNT_NUMBERS_TOLL_FREE; ?></td>
        <td class="finance-number">
            <?php echo $infoTotal[InvoicesGenerator::INFO_AMOUNT_NUMBERS_TOLL_FREE]; ?>
        </td>
        <!-- <td class="finance-number">
            <?php echo InvoicesGenerator::getDouble($infoTotal[InvoicesGenerator::INFO_RATE_PER_MONTH_NUMBER_TOLL_FREE]); ?>
        </td>
        <td class="finance-number">
            <?php echo InvoicesGenerator::getDouble($infoTotal[InvoicesGenerator::INFO_PRICE_NUMBERS_TOLL_FREE]); ?>
        </td> -->
        <td>&nbsp;</td>
    </tr>
<?php endif; ?>

<?php if ($needCallTimeAmount): ?>
    <tr class="<?php echo $trClass; ?>">
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><?php echo InvoicesGenerator::INFO_AMOUNT_MINUTES_LOCAL; ?></td>
        <td class="finance-number">
            <?php echo $infoTotal[InvoicesGenerator::INFO_AMOUNT_MINUTES_LOCAL]; ?>
        </td>
        <!-- <td class="finance-number">
            <?php echo InvoicesGenerator::getDouble($infoTotal[InvoicesGenerator::INFO_RATE_PER_MINUTE_LOCAL]); ?>
        </td>
        <td class="finance-number">
            <?php echo InvoicesGenerator::getDouble($infoTotal[InvoicesGenerator::INFO_PRICE_MINUTES_LOCAL]); ?>
        </td> -->
        <td>&nbsp;</td>
    </tr>
    <tr class="<?php echo $trClass; ?>">
        <td>&nbsp;</td>
        <td>&nbsp;</td>
        <td><?php echo InvoicesGenerator::INFO_AMOUNT_MINUTES_TOLL_FREE; ?></td>
        <td class="finance-number">
            <?php echo $infoTotal[InvoicesGenerator::INFO_AMOUNT_MINUTES_TOLL_FREE]; ?>
        </td>
        <!-- <td class="finance-number">
            <?php echo InvoicesGenerator::getDouble($infoTotal[InvoicesGenerator::INFO_RATE_PER_MINUTE_TOLL_FREE]); ?>
        </td>
        <td class="finance-number">
            <?php echo InvoicesGenerator::getDouble($infoTotal[InvoicesGenerator::INFO_PRICE_MINUTES_TOLL_FREE]); ?>
        </td> -->
        <td>&nbsp;</td>
    </tr>
<?php endif; ?>
    
<!-- <tr class="<?php echo $trClass; ?> financial-position-total">
    <td>&nbsp;</td>
    <td><?php echo ExcelInvoiceHelperTable::ROW_TOTAL; ?></td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td class="financial-value">
        <?php echo InvoicesGenerator::getDouble($infoTotal[InvoicesGenerator::INFO_PRICE_TOTAL]); ?>
    </td>
    <td>&nbsp;</td>
</tr> -->
<tr class="<?php echo $trClass; ?>">
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <!-- <td>&nbsp;</td>
    <td>&nbsp;</td>-->
    <td>&nbsp;</td>
</tr>
