<?php use_helper('Link'); ?>

<?php
    include_partial('tableAndFilter', array(
        'data'          => $data,
        'filter_form'   => $filter_form,
        'permissions'   => $permissions,
        'data_url'      => url_for('finance_show_sub_tree_for_company_list', array('id' => $company_id)),
    ));
?>

<?php
    $a_options = array();

    if ($permissions['can_read']) {

        $a_options['XLS Summary'] = array(
            'href' => url_for(
                'finance_xls_for_company',
                array(
                    'id'                => $company_id,
                    'invoice_type_code' => InvoicesGeneratorBase::INVOICE_TYPE_DETAILS_SUMMARY,
                )
            ),
            'class' => "icon xls-summary"
        );

        $a_options['XLS Full'] = array(
            'href' => url_for(
                'finance_xls_for_company',
                array(
                    'id'                => $company_id,
                    'invoice_type_code' => InvoicesGeneratorBase::INVOICE_TYPE_DETAILS_FULL,
                )
            ),
            'class' => "icon xls-full"
        );

        $a_options['Back'] = array(
            'href' => url_for(
                'company_show',
                array(
                    'id' => $company_id
                )
            ),
            'class' => "icon back ajax-load"
        );
    }

    echo icon_links($a_options);
?>