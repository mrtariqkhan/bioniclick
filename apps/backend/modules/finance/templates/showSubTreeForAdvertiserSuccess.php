<?php use_helper('Link'); ?>

<?php
    include_partial('tableAndFilter', array(
        'data'          => $data,
        'filter_form'   => $filter_form,
        'permissions'   => $permissions,
        'data_url'      => url_for('finance_show_sub_tree_for_advertiser_list', array('id' => $advertiser_id)),
    ));
?>

<?php
    $a_options = array();

    if ($permissions['can_read']) {

        $a_options['XLS Summary'] = array(
            'href' => url_for(
                'finance_xls_for_advertiser',
                array(
                    'id'                => $advertiser_id,
                    'invoice_type_code' => InvoicesGeneratorBase::INVOICE_TYPE_DETAILS_SUMMARY,
                )
            ),
            'class' => "icon xls-summary"
        );

        $a_options['Back'] = array(
            'href' => url_for(
                'advertiser_show',
                array(
                    'id' => $advertiser_id,
                )
            ),
            'class' => "icon back ajax-load"
        );
    }

    echo icon_links($a_options);
?>
