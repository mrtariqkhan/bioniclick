<?php
    $data = $data->getRawValue();

    $infoTotal = $data[InvoicesGenerator::LEVEL_TOTAL];
    $priceTotal = $infoTotal[InvoicesGenerator::INFO_PRICE_TOTAL];
    $level      = $infoTotal[InvoicesGenerator::INFO_COMPANY_LEVEL];
    
    $name               = $infoTotal[InvoicesGenerator::INFO_NAME];
    $headerTimeFrom     = $infoTotal[InvoicesGenerator::INFO_TIME_UTC_STR_FROM];
    $headerTimeTo       = $infoTotal[InvoicesGenerator::INFO_TIME_UTC_STR_TO];
    $invoiceTypeMoney   = $infoTotal[InvoicesGenerator::INFO_INVOICE_TYPE_MONEY];
    $invoiceTypeDetails = $infoTotal[InvoicesGenerator::INFO_INVOICE_TYPE_DETAILS];
?>

<h1><?php echo "Usage Report ($invoiceTypeDetails)"; ?></h1>
<br>
<!-- <h2><?php echo "\"$name\" should $invoiceTypeMoney"; ?></h2> -->
<h2><?php echo $name; ?></h2>

<br>
<?php echo 'From: ' . $headerTimeFrom; ?>
<br>
<?php echo 'To: ' . $headerTimeTo; ?>

<div class="table-block">
    <table class="table">
        <thead>
            <tr>
                <th class="first">
                    &nbsp;
                </th>
                <th>
                    <?php echo ExcelInvoiceHelperTable::COLUMN_ACCOUNT; ?>
                </th>
                <th>
                    <?php echo ExcelInvoiceHelperTable::COLUMN_ITEM; ?>
                </th>
                <th class="finance-number-title">
                    <?php echo ExcelInvoiceHelperTable::COLUMN_QUANTITY; ?>
                </th>

                <!-- <th class="finance-number-title">
                    <?php echo ExcelInvoiceHelperTable::COLUMN_RATE; ?>
                </th>
                <th class="finance-number-title">
                    <?php echo ExcelInvoiceHelperTable::COLUMN_TOTAL_DUE; ?>
                </th> -->
                
                <th class="last">
                    &nbsp;
                </th>
            </tr>
        </thead>
        <tbody>
            <?php
                $trNumber = 1;

                switch ($level) {
                    case CompanyTable::$LEVEL_BIONIC:
                    case CompanyTable::$LEVEL_RESELLER:
                    case CompanyTable::$LEVEL_AGENCY:
                        $infoSubCompanies = $data[InvoicesGenerator::LEVEL_SUB_COMPANY];
                        foreach ($infoSubCompanies as $info) {
                            include_partial('tableBlock', array(
                                'info'      => $info,
                                'prefix'    => ExcelInvoiceHelperTable::ITEM_NAME_PREFIX_SUB_COMPANY,
                                'postfix'   => ExcelInvoiceHelperTable::ITEM_NAME_POSTFIX_SUB_COMPANY,
                                'trNumber'  => $trNumber++,
                            ));
                        }

                        $infoSubAdvertisers = $data[InvoicesGenerator::LEVEL_SUB_ADVERTISER];
                        foreach ($infoSubAdvertisers as $info) {
                            include_partial('tableBlock', array(
                                'info'      => $info,
                                'prefix'    => ExcelInvoiceHelperTable::ITEM_NAME_PREFIX_ADVERTISER,
                                'postfix'   => ExcelInvoiceHelperTable::ITEM_NAME_POSTFIX_ADVERTISER,
                                'trNumber'  => $trNumber++,
                            ));
                        }
                        break;
                    case CompanyTable::$LEVEL_ADVERTISER:

                        $infoNumbersAmount = $data[InvoicesGenerator::LEVEL_NUMBERS_AMOUNT];
                        include_partial('tableBlock', array(
                            'info'      => $infoNumbersAmount,
                            'prefix'    => ExcelInvoiceHelperTable::ITEM_NAME_PREFIX_ADVERTISER,
                            'postfix'   => ExcelInvoiceHelperTable::ITEM_NAME_POSTFIX_ADVERTISER_NUMBER_AMOUNT,
                            'trNumber'  => $trNumber++,
                        ));

                        $infoUnassigned = $data[InvoicesGenerator::LEVEL_UNASSIGNED];
                        include_partial('tableBlock', array(
                            'info'      => $infoUnassigned,
                            'prefix'    => ExcelInvoiceHelperTable::ITEM_NAME_PREFIX_ADVERTISER,
                            'postfix'   => ExcelInvoiceHelperTable::ITEM_NAME_POSTFIX_ADVERTISER_UNASSIGNED,
                            'trNumber'  => $trNumber++,
                        ));

                        $infoCampaigns = $data[InvoicesGenerator::LEVEL_CAMPAIGN];
                        foreach ($infoCampaigns as $info) {
                            include_partial('tableBlock', array(
                                'info'      => $info,
                                'prefix'    => ExcelInvoiceHelperTable::ITEM_NAME_PREFIX_CAMPAIGN,
                                'postfix'   => ExcelInvoiceHelperTable::ITEM_NAME_POSTFIX_CAMPAIGN,
                                'trNumber'  => $trNumber++,
                            ));
                        }
                        break;
                }
            ?>

            <?php $trClass = '';//($trNumber++ % 2) ? 'odd' : 'even'; ?>
            <!-- <tr class="<?php echo $trClass; ?> financial-position-grand-total">
                <td>&nbsp;</td>
                <td><?php echo ExcelInvoiceHelperTable::ROW_GRAND_TOTAL; ?></td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td>&nbsp;</td>
                <td class="financial-value">
                    <?php echo InvoicesGenerator::getDouble($priceTotal); ?>
                </td>
                <td>&nbsp;</td>
            </tr> -->
        </tbody>
    </table>
</div>
