<?php
    include_partial('table', array(
        'data'          => $data,
        'filter_form'   => $filter_form,
        'permissions'   => $permissions,
    ));
?>
