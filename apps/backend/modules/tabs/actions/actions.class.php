<?php

/**
 * tabs actions.
 *
 * @package    bionic
 * @subpackage tabs
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class tabsActions extends GeneralActions {

    public function preExecute() {

        parent::preExecute();

        $this->getResponse()->addStylesheet(
            '/css/project/tabs_accordion.css',
            'last'
        );

        $this->getResponse()->addJavascript(
            '/js/jquery/jquery.cookie.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/jquery/jquery.form.js',
            'last'
        );
        $this->getResponse()->addStylesheet(
            '/css/project/bionic_form_ajax_error.css',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/project/tabs.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/project/paging_simple.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/project/ajax_form.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/project/ajax_delete.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/project/ajax_filter.js',
            'last'
        );
        $this->getResponse()->addStylesheet(
            '/css/project/reset.css',
            'last'
        );

        $this->getResponse()->addJavascript(
            '/js/project/ajax_links.js',
            'last'
        );

        $this->getResponse()->addStylesheet('/css/project/icon.css', 'last');
    }

    public function executeNumberPoolAdvertiser(sfWebRequest $request) {

        //FIXME:: include all need JS, CSS for all inner pages.
        $this->getResponse()->addStylesheet('/css/project/table.css', 'last');
        $this->getResponse()->addStylesheet(
            '/css/project/form_general.css',
            'last'
        );
        $this->getResponse()->addStylesheet(
            '/css/project/filter_block.css',
            'last'
        );
        $this->getResponse()->addStylesheet(
            '/css/project/autocompleter.css',
            'last'
        );

        $this->getResponse()->addJavascript(
            '/js/jquery/livequery/jquery.livequery.js',
            'last'
        );

        $this->getResponse()->addJavascript(
            '/sfFormExtraPlugin/js/jquery.autocompleter.js',
            'last'
        );

        $actionName = $this->getActionName();
        $this->setTabParams($actionName);
        $this->setTemplate('tabs');
    }

    public function executeNumberPoolCompany(sfWebRequest $request) {

        //FIXME:: include all need JS, CSS for all inner pages.
        $this->getResponse()->addStylesheet('/css/project/table.css', 'last');
        $this->getResponse()->addStylesheet(
            '/css/project/form_general.css',
            'last'
        );
        $this->getResponse()->addStylesheet(
            '/css/project/filter_block.css',
            'last'
        );
        $this->getResponse()->addStylesheet(
            '/css/project/autocompleter.css',
            'last'
        );

        $this->getResponse()->addJavascript(
            '/js/jquery/livequery/jquery.livequery.js',
            'last'
        );

        $this->getResponse()->addJavascript(
            '/sfFormExtraPlugin/js/jquery.autocompleter.js',
            'last'
        );

        $actionName = $this->getActionName();
        $this->setTabParams($actionName);
        $this->setTemplate('tabs');
    }

    public function executeLogs(sfWebRequest $request) {

        //FIXME:: include all need JS, CSS for all inner pages.
        $this->getResponse()->addStylesheet('/css/project/table.css', 'last');
        $this->getResponse()->addStylesheet('/css/project/table_block.css', 'last');
        $this->getResponse()->addStylesheet('/css/project/filter_block.css', 'last');
        $this->getResponse()->addStylesheet('/css/project/form_general.css', 'last');
        $this->getResponse()->addStylesheet('/css/project/block_reason_content.css', 'last');

        //NOTE:: for callLog
        $this->getResponse()->addStylesheet('/css/project/datepicker.css', 'last');
        $this->getResponse()->addJavascript('/js/jquery/livequery/jquery.livequery.js', 'last');
        $this->getResponse()->addJavascript('/js/jquery/daterangepicker/js/daterangepicker.jQuery.js', 'last');
        $this->getResponse()->addJavascript('/js/project/date_range_common.js', 'last');
        $this->getResponse()->addJavascript('/js/project/call_log_tab.js', 'last');
        $this->getResponse()->addJavascript('/js/project/callLogs.js', 'last');
        $this->getResponse()->addJavascript('/js/project/callLogsChangable.js', 'last');
        $this->getResponse()->addJavascript('/js/project/audio_player.js', 'last');
        $this->getResponse()->addStylesheet('/css/project/form_log.css', 'last');
        $this->getResponse()->addStylesheet('/css/project/callLogForm.css', 'last');

        $selectedSubTabCode = $request->getParameter('sub_tab_code', null);
        $selectedSubTabParams = array();
        $commonSubTabParams = array();
        if ($selectedSubTabCode == 'call_log') {
            $campaignId = $request->getParameter('campaign_id', null);
            if (!empty($campaignId)) {
                $commonSubTabParams = array('campaign_id' => $campaignId);
            }
        } elseif ($selectedSubTabCode == 'callers') {
            $id = $request->getParameter('id', null);
            if (!empty($id)) {
                $selectedSubTabParams = array('id' => $id);
            }
        }

        $selectedSubTabUrlType = $request->getParameter('sub_tab_type', null);

        $actionName = $this->getActionName();
        $this->setTabParams(
            $actionName,
            $selectedSubTabCode,
            $selectedSubTabParams,
            $selectedSubTabUrlType,
            $commonSubTabParams
        );
        $this->setTemplate('tabs');
    }

    public function executeBilling(sfWebRequest $request) {

        //NOTE:: superadmin cannot see this tab, see executeBilling action.
        $havePermissions = $this->securityHelper->checkCanSeeTabBilling();
        if (!$havePermissions) {
            $this->redirect('sfGuardAuth/secure');
        }

        $this->getResponse()->addStylesheet('/css/project/table.css', 'last');
        $this->getResponse()->addStylesheet(
            '/css/project/form_general.css',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/jquery/livequery/jquery.livequery.js',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/project/billing_tab.js',
            'last'
        );
        $this->getResponse()->addStylesheet('/css/project/billing.css', 'last');
        $actionName = $this->getActionName();
        $this->setTabParams($actionName);
        $this->setTemplate('tabs');
    }

    public function executeAdministration(sfWebRequest $request) {
        
        $this->getResponse()->addJavascript(
            '/js/jquery/livequery/jquery.livequery.js',
            'last'
        );

        $this->getResponse()->addJavascript(
            '/sfFormExtraPlugin/js/jquery.autocompleter.js',
            'last'
        );

        $this->getResponse()->addStylesheet('/css/project/table.css', 'last');
        $this->getResponse()->addStylesheet(
            '/css/project/form_general.css',
            'last'
        );
        $this->getResponse()->addStylesheet(
            '/css/project/filter_block.css',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/project/editCampaign.js',
            'last'
        );

        $actionName = $this->getActionName();
        $this->setTabParams($actionName);
        $this->setTemplate('tabs');
    }

    public function executeSuperAdministration(sfWebRequest $request) {

        $this->getResponse()->addJavascript(
            '/js/jquery/livequery/jquery.livequery.js',
            'last'
        );

        $this->getResponse()->addJavascript(
            '/sfFormExtraPlugin/js/jquery.autocompleter.js',
            'last'
        );
        
        $this->getResponse()->addStylesheet('/css/project/table.css', 'last');
        $this->getResponse()->addStylesheet(
            '/css/project/form_general.css',
            'last'
        );
        $this->getResponse()->addStylesheet(
            '/css/project/filter_block.css',
            'last'
        );
        $this->getResponse()->addStylesheet(
            '/css/project/autocompleter.css',
            'last'
        );
        $this->getResponse()->addJavascript(
            '/js/project/editCampaign.js',
            'last'
        );

        $actionName = $this->getActionName();
        $this->setTabParams($actionName);
        $this->setTemplate('tabs');
    }

    private function setTabParams(
        $actionName,
        $selectedSubTabCode = null,
        $selectedSubTabParams = null,
        $selectedSubTabUrlType = null,
        $commonSubTabParams = null
    ) {

        $codeName = sfInflector::underscore($actionName);

        if (!$this->checkUserMainTabPermissions($codeName)) {
            $this->redirect('sfGuardAuth/secure');
        }

        $this->getUser()->setAttribute('menu', $codeName);

        $this->tabs_code                = $codeName . '_tab';
        $this->selected_sub_tab_code    = $selectedSubTabCode;
        $this->selected_sub_tab_params  = $selectedSubTabParams;
        $this->selected_sub_tab_url_type= $selectedSubTabUrlType;
        $this->selected_tab_class       = 'selected-tab';
        $this->common_sub_tab_params    = $commonSubTabParams;
    }

    protected function checkUserMainTabPermissions($tabCode) {

        if ($tabCode == 'billing') {
            if (!$this->user->ifHasToPay()) {
                $this->getResponse()->setStatusCode(403);
                return false;
            }
        }

        $moduleName = strtolower($this->getModuleName());
        $tabsSettings = sfConfig::get('mod_' . $moduleName . '_tabs_settings', array());

        $menuTabCode = 'main_tabs';
        $menuTabParameters = array_key_exists($menuTabCode, $tabsSettings) ? $tabsSettings[$menuTabCode] : array();

        $tabParameters = array_key_exists($tabCode, $menuTabParameters) ? $menuTabParameters[$tabCode] : array();

        $credentials = array_key_exists('credentials', $tabParameters) ? $tabParameters['credentials'] : array();
        return $this->securityHelper->checkUserPermissions($credentials);
    }
}
