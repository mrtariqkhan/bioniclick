<?php

/**
 * tabs components.
 *
 * @package
 * @subpackage
 * @author
 * @version
 */

class tabsComponents extends GeneralComponents {

    const DEFAULT_SUB_TAB_URL_TYPE = 'index';

    public function executeMainTabs() {

        $this->tabs_code                = 'main_tabs';
        $this->selected_sub_tab_code    = $this->getUser()->getAttribute('menu', null);
        $this->selected_sub_tab_params  = array();
        $this->selected_sub_tab_url_type= self::DEFAULT_SUB_TAB_URL_TYPE;
        $this->selected_tab_class       = 'active';
        $this->common_sub_tab_params    = array();
    }

    public function executeTabs() {

        $tabsCode               = $this->tabs_code;
        $selectedSubTabCode     = $this->selected_sub_tab_code;

        if (empty($this->selected_sub_tab_params)) {
            $this->selected_sub_tab_params = array();
        }
        if (empty($this->selected_sub_tab_url_type)) {
            $this->selected_sub_tab_url_type = self::DEFAULT_SUB_TAB_URL_TYPE;
        }
        if (empty($this->selected_tab_class)) {
            $this->selected_tab_class = '';
        }
        if (empty($this->common_sub_tab_params)) {
            $this->common_sub_tab_params = array();
        }

        $moduleName = strtolower($this->getModuleName());
        $tabsSettings = sfConfig::get('mod_' . $moduleName . '_tabs_settings', array());

        $tabs = array_key_exists($tabsCode, $tabsSettings)
            ? $tabsSettings[$tabsCode]
            : array()
        ;

        $user = $this->getUser();
        $guardUser = empty($user) ? null : $user->getGuardUser();

        $this->tabs = $this->configureCheckedTabs(
            $tabs,
            $guardUser,
            $selectedSubTabCode,
            $this->selected_sub_tab_url_type,
            $this->selected_sub_tab_params,
            $this->common_sub_tab_params
        );
    }

    protected function configureCheckedTabs(
        $tabs,
        $guardUser,
        $selectedSubTabCode,
        $selectedSubTabUrlType,
        $selectedSubTabParams,
        $commonSubTabParams
    ) {

        $userRole = empty($guardUser) ? null : $guardUser->getRole();

        $securityHelper = new SecurityHelper($this);

        $routing = $this->getContext()->getRouting();
        $routes = $routing->getRoutes();

        $resultTabs = array();
        foreach ($tabs as $tabCode => $tabParameters) {

            if ($tabCode == 'billing') {
                if (!$guardUser->ifHasToPay()) {
                    continue;
                }
            }

            $credentials = array_key_exists('credentials', $tabParameters) ? $tabParameters['credentials'] : array();
            if ($securityHelper->checkUserPermissions($credentials)) {

                $userRoutes = $this->getAllRoutesForUser($userRole, $tabParameters['url']);
                if (empty($userRoutes)) {
                    continue;
                }

                $isSelectedSubTab = ($tabCode == $selectedSubTabCode);

                $subTabUrlType = $isSelectedSubTab ? $selectedSubTabUrlType : self::DEFAULT_SUB_TAB_URL_TYPE;
                $routeName = array_key_exists($subTabUrlType, $userRoutes) ? $userRoutes[$subTabUrlType] : null;
                if (empty($routeName)) {
                    continue;
                }

                $routeMethod = $this->getRouteMethod($routes, $routeName);
                $urlParams = $isSelectedSubTab ? $selectedSubTabParams : array();
                $urlParams = array_merge($commonSubTabParams, $urlParams);

                $routeForUser = array(
                    'route'     => $routeName,
                    'method'    => $routeMethod,
                    'params'    => $urlParams,
                );
                $tabParameters['route_for_user'] = $routeForUser;

                if (!array_key_exists('class', $tabParameters)) {
                    $tabParameters['class'] = '';
                }

                $resultTabs[$tabCode] = $tabParameters;
            }
        }

        return $resultTabs;
    }

    protected function getAllRoutesForUser($userRole, $routesAll) {

        $routesForUser = null;
        if (array_key_exists($userRole, $routesAll)) {
            $routesForUser = $routesAll[$userRole];
        } elseif (empty($routesForUser)
            && array_key_exists(CompanyTable::$LEVEL_NAME_ALL, $routesAll)
        ) {
            $routesForUser = $routesAll[CompanyTable::$LEVEL_NAME_ALL];
        }

        return $routesForUser;
    }

    protected function getRouteMethod($routes, $routeName) {

        $routeMethod = 'post';

        $route = array_key_exists($routeName, $routes) ? $routes[$routeName] : null;
        if (!empty($route)) {
            $requirements = $route->getRequirements();
            $methods = array_key_exists('sf_method', $requirements)
                ? $requirements['sf_method']
                : null
            ;
            $routeMethod = empty($methods) ? 'post' : $methods[0];
        }

        return $routeMethod;
    }
}