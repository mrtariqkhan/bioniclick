
<?php include_partial('global/ajax_error', array()); ?>

<?php include_partial('global/ajax_loader'); ?>

<div class="ajax-blocked">

    <div class="secondary-navigation">
        <div id="tabs">

            <?php
                include_component(
                    'tabs',
                    'tabs',
                    array(
                        'tabs_code'                 => $tabs_code,
                        'selected_sub_tab_code'     => $selected_sub_tab_code,
                        'selected_sub_tab_params'   => $selected_sub_tab_params,
                        'selected_sub_tab_url_type' => $selected_sub_tab_url_type,
                        'selected_tab_class'        => $selected_tab_class,
                        'common_sub_tab_params'     => $common_sub_tab_params,
                    )
                );
            ?>

        </div>
    </div>

</div>