<ul>

    <?php foreach ($tabs as $tabCode => $tabParameters) : ?>
        <?php $tabParameters = $tabParameters->getRawValue(); ?>
        <?php $routeInfo = $tabParameters['route_for_user']; ?>

        <?php $url          = url_for($routeInfo['route'], $routeInfo['params']); ?>
        <?php $urlMethod    = $routeInfo['method']; ?>

        <?php $liClass = ($tabCode == $selected_sub_tab_code) ? $selected_tab_class : ''; ?>

        <li class="<?php echo $liClass; ?>">
            <a
                href="<?php echo $url; ?>"
                first_method="<?php echo $urlMethod; ?>"
                class="<?php echo $tabParameters['class']; ?>"
                tab_code="<?php echo $tabCode; ?>"
            >
                <span>
                    <?php echo $tabParameters['name']; ?>
                </span>
            </a>
        </li>
    <?php endforeach; ?>

</ul>
