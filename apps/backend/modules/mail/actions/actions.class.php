<?php

/**
 * api actions.
 *
 * @package    bionic
 * @subpackage api
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class mailActions extends GeneralActions
{	
	public function preExecute(){
		parent::preExecute();
	}

	public function executeIndex(sfWebRequest $request){
	}

	public function executeSendWithSnapshotAttached(sfWebRequest $request){
		
		$from = $this->user->getEmailAddress();
		$to = $_POST["to-email"];
		$subject = $_POST["subject"];
		$body = $_POST["body"];
		$baseUrl = sfConfig::get('sf_root_dir').'/web/uploads/temp/';

		//save mail in file
		$mail = array('from' => $from, 'to' => $to, 'subject' => $subject,
					 'body' => $body);
		$mailEncoded = json_encode($mail);
		file_put_contents($baseUrl . 'mail' . $this->user->getId(), $mailEncoded);

		// $mailer = $this->getMailer();
		// create a message object
		// $message = $mailer
		//   ->compose($this->user->getEmailAddress(), $to, $subject, $body)
		//   ->attach(Swift_Attachment::fromPath($attachement));
		// send the message
		// $mail_sent = $mailer->send($message);

		exec('php ' . sfConfig::get('sf_root_dir') . '/send_mail.php ' . $this->user->getId(), $mail_sent);
		if(count($mail_sent) > 0 && $mail_sent[0] == "success")
			$status = 'success';
		else
			$status = 'failed';

		return $this->renderText(json_encode(array('status' => $status)));
	}
}
