
<?php
  // Override the login slot so that we don't get a login prompt on the
  // apply page, which is just odd-looking. 0.6
?>

<?php slot('sf_apply_login'); ?>
<?php end_slot(); ?>

<div class="wrap-login">
    <h1>Apply for an Account</h1>
   <div class="content-login">
        <?php
            include_partial(
                'global/form',
                array(
                    'form' => $form,
                    'action_url' => url_for('@apply'),
                    'submit_name' => 'Create My Account',
                    'back_url' => sfConfig::get('app_sfApplyPlugin_after', '@homepage')
                )
            );
        ?>
  </div>
</div>

