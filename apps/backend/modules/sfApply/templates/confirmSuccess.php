<?php use_helper('I18N'); ?>

<div class="wrap-login">
    <div class="content-login">
        <p>
            <?php echo __("Thank you for confirming your account! You are now logged into the site."); ?>
        </p>
        <?php include_partial('sfApply/continue'); ?>
    </div>
</div>
