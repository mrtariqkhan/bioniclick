<?php use_helper('I18N'); ?>

<div class="wrap-login">
    <div class="sf_apply_notice content-login">
        <p>
<?php echo __(
<<<EOM
An error took place during the email delivery process. Please try again later.
EOM
); ?>
        </p>
        <?php include_partial('sfApply/continue'); ?>
    </div>
</div>
