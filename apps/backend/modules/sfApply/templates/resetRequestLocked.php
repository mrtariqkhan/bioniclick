<?php use_helper('I18N'); ?>

<?php slot('sf_apply_login'); ?>
<?php end_slot(); ?>

<div class="wrap-login">
    <div class="sf_apply_notice content-login">
        <p>
<?php echo __(
<<<EOM
This account is inactive. Please contact the administrator.
EOM
); ?>
        </p>
        <?php include_partial('sfApply/continue'); ?>
    </div>
</div>
