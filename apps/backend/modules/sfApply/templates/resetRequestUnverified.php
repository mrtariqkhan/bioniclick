<?php use_helper('I18N'); ?>

<div class="wrap-login">
    <div class="sf_apply_notice content-login">
        <p>
<?php echo __(
<<<EOM
That account was never verified. You must verify the account before you can log in or, if
necessary, reset the password. We have resent your verification email, which contains
instructions to verify your account. If you do not see that email, please be sure to check 
your "spam" or "bulk" folder.
EOM
); ?>
        </p>
        <?php include_partial('sfApply/continue'); ?>
    </div>
</div>    
