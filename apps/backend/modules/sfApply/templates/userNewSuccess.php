<h1>Create new user</h1>

<div class="inner">
    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => url_for('user_create'),
                'submit_name'       => 'Create',
                'back_url'          => $url_after,
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel')
            )
        );
    ?>
</div>
