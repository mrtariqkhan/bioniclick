<?php use_helper('I18N'); ?>

<div class="wrap-login">
    <div class="content-login">
        <p>
<?php echo __(
<<<EOM
Your password has been successfully reset. You are now logged
in to this site. In the future, be sure to log in with your new password.
EOM
); ?>
        </p>
        <?php include_partial('sfApply/continue'); ?>
    </div>
</div>
