<?php use_helper('I18N'); ?>

<?php slot('sf_apply_login'); ?>
<?php end_slot(); ?>

<div class="wrap-login">
    <div class="content-login">
        <p>
<?php echo __(
<<<EOM
Forgot your password? No problem! Just enter your email address and click "Reset My Password." You will receive an email message containing both your username and
a link permitting you to change your password if you wish.
EOM
); ?>
        </p>

            <?php
                include_partial(
                    'global/form',
                    array(
                        'form' => $form,
                        'action_url' =>  url_for('@resetRequest'),
                        'submit_name' => __("Reset My Password"),
                        'back_url' => sfConfig::get('app_sfApplyPlugin_after', '@homepage')

                    )
                );
            ?>
    </div>
</div>
