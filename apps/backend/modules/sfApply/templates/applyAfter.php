<?php use_helper('I18N'); ?>

<div class="wrap-login">
    <div class="content-login">
        <p>
<?php echo __(
<<<EOM
Thank you for applying for an account. You will receive a verification
email shortly. If you do not see that email, please be sure to check 
your "spam" or "bulk" folder.
EOM
); ?>
        </p>
        <?php include_partial('sfApply/continue'); ?>
    </div>
</div>
