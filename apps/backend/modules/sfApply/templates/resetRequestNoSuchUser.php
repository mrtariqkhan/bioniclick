<?php use_helper('I18N'); ?>

<div class="wrap-login">
    <div class="sf_apply_notice content-login">
        <p>
<?php echo __(
<<<EOM
Sorry, there is no user with that username or email address.
EOM
); ?>
        </p>
        <?php include_partial('sfApply/continue'); ?>
        </div>
</div>

