<?php use_helper('I18N'); ?>

<?php slot('sf_apply_login'); ?>
<?php end_slot(); ?>

<div class="wrap-login">
    <div class="content-login">
        <p>
<?php echo __(<<<EOM
Thanks for confirming your email address. You may now change your
password using the form below.
EOM
) ?>
        </p>

        <?php
            include_partial(
                'global/form',
                array(
                    'form' => $form,
                    'action_url' =>  url_for('@reset'),
                    'submit_name' => __("Reset My Password"),
                    'back_url' => 'sfApply/resetCancel'

                )
            );
        ?>
    </div>
</div>
