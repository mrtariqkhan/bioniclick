<?php

class sfApplyActions extends GeneralApplyActions {

    public function preExecute() {

        parent::preExecute();

        $this->getResponse()->addStylesheet('/css/project/login.css', 'last');
        $this->getResponse()->addStylesheet('/css/project/reset.css', 'last');
    }

    public function executeUserNew(sfRequest $request) {

        $this->setTemplate('userNew');
        $this->form = $this->getNewForm();

        $this->url_after = $this->generateUrl('sf_guard_users');
    }

    public function executeCurrentAdvertiserUserNew(sfRequest $request) {

        $this->forward404If(!$this->user->isAdvertiserUser(), "You are not Advertiser's user");

        $this->executeUserNew($request);
        $this->url_after = $this->generateUrl('sf_guard_advertiser_users');
    }

    public function executeCurrentCompanyUserNew(sfRequest $request) {

        $this->forward404If(!$this->user->isCompanyUser(), "You are not Company's user");

        $this->executeUserNew($request);
        $this->url_after = $this->generateUrl('sf_guard_company_users');
    }

    public function executeUserCreate(sfRequest $request) {

        $form = $this->getNewForm();

        return $this->processAjaxForm($request, $form);
    }

    public function executeCustomerCompanyUserNew(sfRequest $request) {

        $companyId = $request->getParameter('company_id', null);
        $this->form = $this->getNewCustomerForm($companyId, true);
        $this->company_id = $companyId;
    }

    public function executeCustomerCompanyUserCreate(sfRequest $request) {

        $companyId = $request->getParameter('company_id', null);
        $form = $this->getNewCustomerForm($companyId, true);

        return $this->processAjaxForm($request, $form);
    }

    public function executeCustomerAdvertiserUserNew(sfRequest $request) {

        $advertiserId = $request->getParameter('advertiser_id', null);
        $this->form = $this->getNewCustomerForm($advertiserId);
        $this->advertiser_id = $advertiserId;
    }

    public function executeCustomerAdvertiserUserCreate(sfRequest $request) {

        $advertiserId = $request->getParameter('advertiser_id', null);
        $form = $this->getNewCustomerForm($advertiserId);

        return $this->processAjaxForm($request, $form);
    }

    protected function saveValidForm($validForm) {

        $savedObject = $validForm->save();

        if (!sfGuardUserProfileTable::ifUseSimpleUserCreation()) {
            try {
                $profile = $validForm->getObject();
                $this->sendVerificationMail($profile);
            } catch (Exception $e) {
                $profile = $validForm->getObject();
                $user = $profile->getUser();
                $user->delete();
                throw $e;
            }
        }

        return $savedObject;
    }
}