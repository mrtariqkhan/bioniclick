<?php use_helper('AudioPlayer')?>

<div class='information'>
    <span class="information-strong">
        Information about incoming call
    </span>
   
    <?php if (!empty($info)): ?>
    <ul class="information-ul">
        <?php foreach ($info as $name => $value): ?>
            <li>
                <strong>
                    <?php echo "$name:"; ?>
                </strong>
                <?php echo "$value"; ?>
            </li>
        <?php endforeach; ?>
    </ul>
    <?php endif; ?>

    <strong>
        Recording:
    </strong>

    <?php if (!empty($recording)):
        $url = $recording->getFileUrl() . '.mp3';
        echo audio_player($url);
    ?>
        <a 
            href="
                <?php
                    echo url_for(
                        'call_log_download_recording',
                        array(
                            'id' => $recording->getId()
                        )
                    );
                ?>
            "
        >
            Download
        </a>
    <?php else: ?>
        None
    <?php endif; ?>
</div>