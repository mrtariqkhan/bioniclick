
<div class="create-result-form">
    <h1>Edit call result</h1>

    <?php $object = $form->getObject(); ?>
    <?php $id = $object->getId(); ?>
    <?php $twilioIncomingCallId = $object->getTwilioIncomingCallId(); ?>

    <?php $url_action = url_for('call_result_update', array('twilio_incoming_call_id' => $twilioIncomingCallId)); ?>

    <?php
        include_partial(
            'global/form_ajax',
            array(
                'form'              => $form,
                'action_url'        => $url_action,
                'back_url'          => $url_after,
                'submit_name'       => 'Update',
                'form_class_name'   => 'ajax-form',
                'cancel_attributes' => array('class' => 'ajax-form-cancel'),
            )
        );
    ?>


    <?php
        include_component(
            'callResult',
            'additionalEditInfo',
            array(
                'call_result'          => $info_call_result,
                'campaign_id'          => $campaign_id,
                'twilio_incoming_call' => $info_twilio_incoming_call,
            )
        );
    ?>
</div>
