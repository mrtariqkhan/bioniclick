<?php

/**
 * callResult actions.
 *
 * @package    bionic
 * @subpackage callResult
 * @author     Your name here
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class callResultActions extends GeneralActions {

    public function executeEdit(sfWebRequest $request) {

        $twilioIncomingCallId = $request->getParameter('twilio_incoming_call_id');
        $twilioIncomingCall = TwilioIncomingCallTable::getInstance()->findOneById($twilioIncomingCallId);
        $this->forward404If(
            empty($twilioIncomingCall),
            sprintf('Object TwilioIncomingCall does not exist (%s).', $twilioIncomingCallId)
        );

        $object = $twilioIncomingCall->getCallResult();
        if (!empty($object) && $object->exists()) {
            //NOTE:: editing of existing object
            $this->checkIsAllowableObject($object);

            $infoCallResult = $object;

        } else {
            //NOTE:: new object
            $object = new CallResult();
            $object->setTwilioIncomingCallId($twilioIncomingCallId);

            $infoCallResult = null;
        }

        $this->form = new CallResultForm($object);
		
        $this->campaign_id = $request->getParameter('campaign_id', null);
        $this->info_twilio_incoming_call = $twilioIncomingCall;
        $this->info_call_result = $infoCallResult;
        $this->url_after = $this->configureAfterUrl($request);
		
        if($this->campaign_id){
            $campaign = CampaignTable::findById($this->campaign_id);

        }else{
            $twilioIncomingCallId = $twilioIncomingCall->getId();
            $startTime = $twilioIncomingCall->getStartTime();
            $visitorAnalytics = VisitorAnalyticsTable::getInstance()->findByIncomingCall(
                $twilioIncomingCallId,
                $startTime
            );
            if (!empty($visitorAnalytics)) {
                $combo = $visitorAnalytics->getCombo();
                $page = $combo->getPage();
                $campaign = $page->getCampaign();  
            }else{
                //check record campaign name
                $campaignName = $request->getParameter('record_campaign_name');
                $campaign = CampaignTable::findByName($campaignName);
            }
        }

        if ($campaign) {
			$advertiser     = $campaign->getAdvertiser();
            if(isset($advertiser)){
                $ratingLevel = $advertiser->getRatingLevel();
                $ratingLevelItems = $ratingLevel->getRatingLevelItem();

                $widgetSchema = $this->form->getWidgetSchema();
                $widgetValidator = $this->form->getValidatorSchema();
                $choices = array(' ');
                foreach($ratingLevelItems as $ratingLevelItem){
                    $name = $ratingLevelItem->getName();
                    $choices = array_merge($choices, array($name => $name));
                }
                $widgetSchema['rating'] = new sfWidgetFormChoice(array('choices' => $choices));
                $widgetValidator['rating'] = new sfValidatorChoice(array('choices' => $choices));
            }
        }
			
    }
    
    public function executeUpdate(sfWebRequest $request) {

        $twilioIncomingCallId = $request->getParameter('twilio_incoming_call_id');
        $twilioIncomingCall = TwilioIncomingCallTable::getInstance()->findOneById($twilioIncomingCallId);
        $this->forward404If(
            empty($twilioIncomingCall),
            sprintf('Object TwilioIncomingCall does not exist (%s).', $twilioIncomingCallId)
        );

        $object = $twilioIncomingCall->getCallResult();
        if (!empty($object) && $object->exists()) {
            //NOTE:: updating of existing object
            $this->checkIsAllowableObject($object);

        } else {
            //NOTE:: creating new object
            $object = new CallResult();
            $object->setTwilioIncomingCallId($twilioIncomingCallId);
        }

        $form = new CallResultForm($object);

        return $this->processAjaxForm($request, $form);
    }

    public function executeDelete(sfWebRequest $request) {
        return $this->processAjaxDelete($request);
    }

    protected function configureAfterUrl(sfWebRequest $request) {

        $params = array();

        $beginTimestamp = $request->getParameter('begin_timestamp', null);
        $campaignId     = $request->getParameter('campaign_id', null);

        $params['begin_timestamp']  = $beginTimestamp;
        $params['campaign_id']      = $campaignId;

        $routePostfix = $request->getParameter('routePostfix', null);
        switch ($routePostfix) {
            case 'interactive':
                $routeName = 'call_logs_interactive';
                break;
            default:
                $routeName = 'call_logs';
        }
        $url_after = $this->generateUrl($routeName, $params);

        return $url_after;
    }
}
