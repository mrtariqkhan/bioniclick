<?php

/**
 * callResult components.
 *
 * @package
 * @subpackage
 * @author
 * @version
 */

class callResultComponents extends GeneralComponents {

    public function executeAdditionalEditInfo() {

        $object             = $this->call_result;
        $campaignId         = $this->campaign_id;
        $twilioIncomingCall = $this->twilio_incoming_call;

        $user = $this->getUser()->getGuardUser();

        $twilioIncomingCallId = $twilioIncomingCall->getId();
        $startTime = $twilioIncomingCall->getStartTime();
        $visitorAnalytics = VisitorAnalyticsTable::getInstance()->findByIncomingCall(
            $twilioIncomingCallId,
            $startTime
        );

        $redialedCall = $twilioIncomingCall->getTwilioRedialedCall();
        $callStartTime = $startTime;
        if ($redialedCall->exists()) {
            $callStartTime = $redialedCall->getStartTime();
        }

        $this->info = array(
            'Campaign name'     => '',
            'Called number'     => $twilioIncomingCall->getTwilioIncomingPhoneNumber()->getPhoneNumber(),
            'Start time'        => TimeConverter::getDateTime(
                $callStartTime,
                $user->findTimezoneName(),
                'Y-m-d H:i:s'
            ),
            'Caller city'       => $twilioIncomingCall->getTwilioCity()->getName()
        );

        if (!empty($visitorAnalytics)) {
            $combo = $visitorAnalytics->getCombo();
            $page = $combo->getPage();
            $campaign = $page->getCampaign();

            $this->info['Campaign name']    = $campaign->getName();
            $this->info['Page URL']         = $page->getPageUrl();
            $this->info['Combo']            = $combo->getName();
            $this->info['Traffic source']   = $visitorAnalytics->getTrafficSource();
            $this->info['Type']             = VisitorAnalyticsTable::convertIsOrganic($visitorAnalytics->getIsOrganic());
            $referrerSource = ReferrerSourceTable::getInstance()->findById($visitorAnalytics->getReferrerSourceId());
            if (!empty($referrerSource)) {
                $this->info['Referrer source'] = $referrerSource->getUrl();
            } else {
                $this->info['Referrer source'] = 'Undefined';
            }

            //TODO:: roll back "tmpAnalytics_keyword" when Matt will correct situation : 
            // "Currently if search type is Direct keyword is site URL. Direct can not have keyword, it must be Undefined."
            $searchTypeDirect = VisitorAnalyticsTable::TYPE_DIRECT;
            $this->info['Keyword']          = $searchTypeDirect == $this->info['Type'] ? $visitorAnalytics->getKeyword() : 'Undefined';
            
        } else {
            $this->info['Page URL']         = 'Undefined';
            $this->info['Combo']            = 'Undefined';
            $this->info['Traffic source']   = 'Undefined';
            $this->info['Type']             = 'Undefined';
            $this->info['Keyword']          = 'N/A';
            $this->info['Referrer source']  = 'Undefined';
        }

        if (!empty($campaignId)) {
            // Campaign log
            $campaign = CampaignTable::findById($campaignId);
            if (empty($campaign) || !IsAllowableHelper::isAllowable($campaign, $user, false)) {
                throw new sfError404Exception(
                    sprintf('Object campaign does not exist (%s).', $campaignId)
                );
            }

            $this->info['Campaign name']    = $campaign->getName();
        }

        $recording = $twilioIncomingCall->getTwilioCallRecording();
        $this->recording = $recording->exists() ? $recording : null;
    }
}