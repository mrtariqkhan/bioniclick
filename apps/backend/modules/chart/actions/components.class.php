<?php

class chartComponents extends sfComponents
{
    public function executeGenerate(sfWebRequest $request) {
        $this->data = $this->chart_options['result'];
        $this->type = $this->chart_options['type'];
        $this->width = $this->chart_options['width'];
        $this->height = $this->chart_options['height'];
    }
}
