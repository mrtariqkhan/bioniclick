<?php
	if($type == 'bar_chart')
		include_partial('chart/barChart', array('dataSet' => $data, 'width' => $width, 'height' => $height));
	else if($type == 'column_chart')
		include_partial('chart/columnChart', array('dataSet' => $data, 'width' => $width, 'height' => $height, 'isStackSeries' => false));
	else if($type == 'stack_column_chart')
		include_partial('chart/columnChart', array('dataSet' => $data, 'width' => $width, 'height' => $height, 'isStackSeries' => true));
?>