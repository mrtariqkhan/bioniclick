<?php
	$rand = uniqid();
	$randChartId = 'chart'.$rand;
	$randPlotId = 'plot'.$rand;
?>
<div id='<?=$randChartId?>' class="jqplot-target">
	<script>
		$.jqplot.config.enablePlugins = true;
		xLabels = Array();
		data = Array();
		<?php
			$index = 0;
			foreach ($dataSet as $key => $value) {
				?>
				xLabels.push('<?= $key?>');
				var objData = ['<?= $key?>', <?= $value?>];
                data.push(<?= $value?>);
				<?php
				$index ++;
			}
		?>
	    var <?= $randPlotId?> = $.jqplot('<?=$randChartId?>', [data], {
	    	animate: !$.jqplot.use_excanvas,
	        animateReplot: !$.jqplot.use_excanvas,
        	
	    	height: <?= $height?>,
            width: <?= $width?>,
	        series:[{
	        	renderer:$.jqplot.BarRenderer,
	        	rendererOptions: {
                    animation: {
                        speed: 1500
                    },
                    barWidth: 15,
                    barPadding: -15,
                    barMargin: 0,
                    highlightMouseOver: true,
                    fillToZero: true
                }
	        	}],
	        axes: {
	            xaxis: {
	                renderer: $.jqplot.CategoryAxisRenderer,
	                tickRenderer: $.jqplot.CanvasAxisTickRenderer ,
	                tickInterval: 20, 
	                drawMajorGridlines: false,
	                drawMinorGridlines: true,
	                drawMajorTickMarks: false,
	                ticks: xLabels,

	            },
	            yaxis: {
	                pad: 0,
	            },
	        },
	        cursor:{
                show:false,
                showTooltip:false
            },
	        highlighter: {
	            show: false,
        	}
	    });
	</script>
</div>   
