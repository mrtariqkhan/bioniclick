<?php
	$rand = uniqid();
	$randChartId = 'chart'.$rand;
	$randPlotId = 'plot'.$rand;
?>
<div id='<?=$randChartId?>' class="jqplot-target">
	<?php
		foreach ($dataSet as $key => $value) {
			foreach ($value as $seriesKey => $seriesValue) {
			}
		}
	
	?>
	<script>
		$.jqplot.config.enablePlugins = true;
		xLabels = Array();
		legendLabels = Array();
		var serieses = Array();
		<?php
			foreach ($dataSet as $key => $value) {
				$index = 0;
				foreach ($value as $seriesKey => $seriesValue) {
					?>
					if(!serieses[<?php echo $index?>]){
						serieses[<?php echo $index?>] = Array();
						legendLabels.push('<?= $seriesKey?>');
					}
					serieses[<?php echo $index?>].push(<?php echo $seriesValue?>);
					<?php
					$index ++;
				}
				?>
				xLabels.push('<?= $key?>');
				<?php
			}
		?>
	    var <?= $randPlotId?> = $.jqplot('<?=$randChartId?>', serieses, {
	    	animate: !$.jqplot.use_excanvas,
	        animateReplot: !$.jqplot.use_excanvas,
        	
        	<?php if($isStackSeries == true) echo "stackSeries:  $isStackSeries,"?>

	    	height: <?= $height?>,
            width: <?= $width?>,
	        seriesDefaults:{
	            renderer:$.jqplot.BarRenderer,
	            rendererOptions: {fillToZero: true}
        	},
	        axes: {
	            xaxis: {
	                renderer: $.jqplot.CategoryAxisRenderer,
	                tickInterval: 20, 
	                drawMajorGridlines: true,
	                drawMinorGridlines: true,
	                drawMajorTickMarks: true,
	                ticks: xLabels,
			        tickRenderer:$.jqplot.CanvasAxisTickRenderer,
			        tickOptions:{ 
			        	fontFamily: 'Georgia',
		          		fontSize: '10pt',
		          		fontWeight: 'bold',
			        	angle: 90,
			        },
	            },
	            yaxis: {
	                pad: 0,
	            },
	        },
	        legend: { 
	        	show:true,
	        	labels: legendLabels,
        	},
        	cursor:{
                show:false,
                showTooltip:false
            },
        	highlighter: {
	            show: false,
        	}
	    });
		$("#<?=$randChartId?>").bind('jqplotMouseMove', function(ev, gridpos, datapos, neighbor, plot) {
		    if (neighbor) {
		    	$("div#<?=$randChartId?> .jqplot-highlighter-tooltip").html(plot.axes.xaxis.ticks[neighbor.pointIndex]);
		    }
		});
	</script>
</div>   
