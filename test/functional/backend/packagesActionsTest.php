<?php

/* Тестирование модуля packages */
/* Проверка на существование экшенов */
/* Реализовать проверку на параметры вызова экшенов */



include(dirname(__FILE__).'/../../bootstrap/functional.php');

$browser = new sfTestFunctional(new sfBrowser());

$browser->
  get('/packages/index')->

  with('request')->begin()->
    isParameter('module', 'packages')->
    isParameter('action', 'index')->
  end()->

  with('response')->begin()->
    isStatusCode(401)->
    checkElement('body', '!/This is a temporary page/')->
  end()
;

$browser = new sfTestFunctional(new sfBrowser());

$browser->
  get('/packages/delete')->

  with('request')->begin()->
    isParameter('module', 'packages')->
    isParameter('action', 'delete')->
  end()->

  with('response')->begin()->
    isStatusCode(401)->
    checkElement('body', '!/This is a temporary page/')->
  end()
;

$browser = new sfTestFunctional(new sfBrowser());

$browser->
  get('/packages/create')->

  with('request')->begin()->
    isParameter('module', 'packages')->
    isParameter('action', 'create')->
  end()->

  with('response')->begin()->
    isStatusCode(401)->
    checkElement('body', '!/This is a temporary page/')->
  end()
;

$browser = new sfTestFunctional(new sfBrowser());


$browser->
  get('/packages/edit')->

  click('Edit', array(), array('position' => 1))->
  with('request')->begin()->
    isParameter('module', 'packages')->
    isParameter('action', 'edit')->
 //   isParameter('action', 'id', $browser->getId())->
  end()->

  with('response')->begin()->
    isStatusCode(401)->
    checkElement('body', '!/This is a temporary page/')->
  end()
;

$browser = new sfTestFunctional(new sfBrowser());

$browser->
  get('/packages/update')->

  with('request')->begin()->
    isParameter('module', 'packages')->
    isParameter('action', 'update')->
  end()->

  with('response')->begin()->
    isStatusCode(401)->
    checkElement('body', '!/This is a temporary page/')->
  end()
;

$browser = new sfTestFunctional(new sfBrowser());

$browser->
  get('/packages/new')->

  with('request')->begin()->
    isParameter('module', 'packages')->
    isParameter('action', 'new')->
  end()->

  with('response')->begin()->
    isStatusCode(401)->
    checkElement('body', '!/This is a temporary page/')->
  end()
;