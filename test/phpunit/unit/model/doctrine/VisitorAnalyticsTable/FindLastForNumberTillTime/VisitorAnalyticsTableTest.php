<?php

require_once dirname(__FILE__) . '/VisitorAnalyticsTableBase.php';

/**
 * Description of VisitorAnalyticsTableTest
 * 
 * @author fairdev
 */
class VisitorAnalyticsTableTest extends VisitorAnalyticsTableBase {

    const ANALYTICS_ID_01__TO_TIPN_02__CAMPAIGN_01 = 1; //1323000010
    const ANALYTICS_ID_02__TO_TIPN_01__CAMPAIGN_01 = 2; //1323000020
    const ANALYTICS_ID_03__TO_TIPN_02__CAMPAIGN_02 = 3; //1323000030
    const ANALYTICS_ID_04__TO_TIPN_01__CAMPAIGN_01 = 4; //1323000040
    const ANALYTICS_ID_05__TO_TIPN_02__CAMPAIGN_02 = 5; //1323000050
    const ANALYTICS_ID_06__TO_TIPN_01__CAMPAIGN_01 = 6; //1323000060


    const TEST_KEY_01_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    = 'Less then ALL visitor analytics to number 1 (tillTimestamp is less then all visitor analytics to all numbers): ';
    const TEST_KEY_02_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    = 'Less then ALL visitor analytics to number 1 (tillTimestamp is not the largest among possible): ';
    const TEST_KEY_03_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    = 'Less then ALL visitor analytics to number 1 (tillTimestamp is the largest among possible): ';
    const TEST_KEY_11_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    = 'Less then ALL visitor analytics to number 2 (tillTimestamp is less then all visitor analytics to all numbers): ';
    const TEST_KEY_12_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    = 'Less then ALL visitor analytics to number 2 (tillTimestamp is not the largest among possible): ';
    const TEST_KEY_13_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    = 'Less then ALL visitor analytics to number 2 (tillTimestamp is the largest among possible): ';
    const TEST_KEY_21_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    = 'More then ALL visitor analytics to number 1 (tillTimestamp is the largest among possible): ';
    const TEST_KEY_22_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    = 'More then ALL visitor analytics to number 1 (tillTimestamp is not the largest and not the most small among possible):';
    const TEST_KEY_23_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    = 'More then ALL visitor analytics to number 1 (tillTimestamp is the most small among possible):';
    const TEST_KEY_31_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    = 'More then ALL visitor analytics to number 2 (tillTimestamp is the largest among possible): ';
    const TEST_KEY_32_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    = 'More then ALL visitor analytics to number 2 (tillTimestamp is not the largest and not the most small among possible):';
    const TEST_KEY_33_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    = 'More then ALL visitor analytics to number 2 (tillTimestamp is the most small among possible):';
    const TEST_KEY_41_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_1    = 'More then ONE visitor analytics to number 1 (tillTimestamp is the largest among possible): ';
    const TEST_KEY_42_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_1    = 'More then ONE visitor analytics to number 1 (tillTimestamp is not the largest and not the most small among possible):';
    const TEST_KEY_43_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_1    = 'More then ONE visitor analytics to number 1 (tillTimestamp is the most small among possible):';
    const TEST_KEY_51_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_2    = 'More then ONE visitor analytics to number 2 (tillTimestamp is the largest among possible): ';
    const TEST_KEY_52_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_2    = 'More then ONE visitor analytics to number 2 (tillTimestamp is not the largest and not the most small among possible):';
    const TEST_KEY_53_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_2    = 'More then ONE visitor analytics to number 2 (tillTimestamp is the most small among possible):';
    const TEST_KEY_61_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_1= 'More then SEVERAL visitor analytics to number 1 (tillTimestamp is the largest among possible): ';
    const TEST_KEY_62_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_1= 'More then SEVERAL visitor analytics to number 1 (tillTimestamp is not the largest and not the most small among possible):';
    const TEST_KEY_63_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_1= 'More then SEVERAL visitor analytics to number 1 (tillTimestamp is the most small among possible):';
    const TEST_KEY_71_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_2= 'More then SEVERAL visitor analytics to number 2 (tillTimestamp is the largest among possible): ';
    const TEST_KEY_72_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_2= 'More then SEVERAL visitor analytics to number 2 (tillTimestamp is not the largest and not the most small among possible):';
    const TEST_KEY_73_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_2= 'More then SEVERAL visitor analytics to number 2 (tillTimestamp is the most small among possible):';


    protected $PARAMETER_TIMESTAMP = array(
        self::TEST_KEY_01_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    => 1323000000,
        self::TEST_KEY_02_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    => 1323000019,
        self::TEST_KEY_03_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    => 1323000020,
        self::TEST_KEY_11_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    => 1323000000,
        self::TEST_KEY_12_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    => 1323000009,
        self::TEST_KEY_13_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    => 1323000010,
        self::TEST_KEY_21_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    => 1323000070,
        self::TEST_KEY_22_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    => 1323000062,
        self::TEST_KEY_23_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    => 1323000061,
        self::TEST_KEY_31_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    => 1323000080,
        self::TEST_KEY_32_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    => 1323000060,
        self::TEST_KEY_33_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    => 1323000051,
        self::TEST_KEY_41_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_1    => 1323000040,
        self::TEST_KEY_42_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_1    => 1323000031,
        self::TEST_KEY_43_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_1    => 1323000021,
        self::TEST_KEY_51_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_2    => 1323000030,
        self::TEST_KEY_52_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_2    => 1323000022,
        self::TEST_KEY_53_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_2    => 1323000011,
        self::TEST_KEY_61_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_1=> 1323000060,
        self::TEST_KEY_62_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_1=> 1323000052,
        self::TEST_KEY_63_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_1=> 1323000041,
        self::TEST_KEY_71_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_2=> 1323000050,
        self::TEST_KEY_72_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_2=> 1323000046,
        self::TEST_KEY_73_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_2=> 1323000031,
    );


    const TIPN_ID_01 = 1;
    const TIPN_ID_02 = 6;
    protected $PARAMETER_TIPN_ID = array(
        self::TEST_KEY_01_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    => self::TIPN_ID_01,
        self::TEST_KEY_02_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    => self::TIPN_ID_01,
        self::TEST_KEY_03_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    => self::TIPN_ID_01,
        self::TEST_KEY_11_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    => self::TIPN_ID_02,
        self::TEST_KEY_12_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    => self::TIPN_ID_02,
        self::TEST_KEY_13_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    => self::TIPN_ID_02,
        self::TEST_KEY_21_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    => self::TIPN_ID_01,
        self::TEST_KEY_22_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    => self::TIPN_ID_01,
        self::TEST_KEY_23_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    => self::TIPN_ID_01,
        self::TEST_KEY_31_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    => self::TIPN_ID_02,
        self::TEST_KEY_32_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    => self::TIPN_ID_02,
        self::TEST_KEY_33_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    => self::TIPN_ID_02,
        self::TEST_KEY_41_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_1    => self::TIPN_ID_01,
        self::TEST_KEY_42_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_1    => self::TIPN_ID_01,
        self::TEST_KEY_43_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_1    => self::TIPN_ID_01,
        self::TEST_KEY_51_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_2    => self::TIPN_ID_02,
        self::TEST_KEY_52_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_2    => self::TIPN_ID_02,
        self::TEST_KEY_53_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_2    => self::TIPN_ID_02,
        self::TEST_KEY_61_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_1=> self::TIPN_ID_01,
        self::TEST_KEY_62_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_1=> self::TIPN_ID_01,
        self::TEST_KEY_63_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_1=> self::TIPN_ID_01,
        self::TEST_KEY_71_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_2=> self::TIPN_ID_02,
        self::TEST_KEY_72_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_2=> self::TIPN_ID_02,
        self::TEST_KEY_73_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_2=> self::TIPN_ID_02,
    );


    protected $EXCEPTED_RESULTS = array(
        self::TEST_KEY_01_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    => null,
        self::TEST_KEY_02_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    => null,
        self::TEST_KEY_03_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    => null,
        self::TEST_KEY_11_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    => null,
        self::TEST_KEY_12_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    => null,
        self::TEST_KEY_13_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    => null,
        self::TEST_KEY_21_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    => self::ANALYTICS_ID_06__TO_TIPN_01__CAMPAIGN_01,
        self::TEST_KEY_22_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    => self::ANALYTICS_ID_06__TO_TIPN_01__CAMPAIGN_01,
        self::TEST_KEY_23_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1    => self::ANALYTICS_ID_06__TO_TIPN_01__CAMPAIGN_01,
        self::TEST_KEY_31_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    => self::ANALYTICS_ID_05__TO_TIPN_02__CAMPAIGN_02,
        self::TEST_KEY_32_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    => self::ANALYTICS_ID_05__TO_TIPN_02__CAMPAIGN_02,
        self::TEST_KEY_33_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2    => self::ANALYTICS_ID_05__TO_TIPN_02__CAMPAIGN_02,
        self::TEST_KEY_41_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_1    => self::ANALYTICS_ID_02__TO_TIPN_01__CAMPAIGN_01,
        self::TEST_KEY_42_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_1    => self::ANALYTICS_ID_02__TO_TIPN_01__CAMPAIGN_01,
        self::TEST_KEY_43_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_1    => self::ANALYTICS_ID_02__TO_TIPN_01__CAMPAIGN_01,
        self::TEST_KEY_51_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_2    => self::ANALYTICS_ID_01__TO_TIPN_02__CAMPAIGN_01,
        self::TEST_KEY_52_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_2    => self::ANALYTICS_ID_01__TO_TIPN_02__CAMPAIGN_01,
        self::TEST_KEY_53_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_2    => self::ANALYTICS_ID_01__TO_TIPN_02__CAMPAIGN_01,
        self::TEST_KEY_61_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_1=> self::ANALYTICS_ID_04__TO_TIPN_01__CAMPAIGN_01,
        self::TEST_KEY_62_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_1=> self::ANALYTICS_ID_04__TO_TIPN_01__CAMPAIGN_01,
        self::TEST_KEY_63_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_1=> self::ANALYTICS_ID_04__TO_TIPN_01__CAMPAIGN_01,
        self::TEST_KEY_71_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_2=> self::ANALYTICS_ID_03__TO_TIPN_02__CAMPAIGN_02,
        self::TEST_KEY_72_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_2=> self::ANALYTICS_ID_03__TO_TIPN_02__CAMPAIGN_02,
        self::TEST_KEY_73_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_2=> self::ANALYTICS_ID_03__TO_TIPN_02__CAMPAIGN_02,
    );


    /**
     * @dataProvider providerCases
     * 
     */
    public function testCase($testKey, $specificAdditionalMessage = '') {

        $tipnIds        = $this->PARAMETER_TIPN_ID;
        $timestamps     = $this->PARAMETER_TIMESTAMP;
        $expectedVAIds  = $this->EXCEPTED_RESULTS;

        $tipnId         = $tipnIds[$testKey];
        $tillTimestamp  = $timestamps[$testKey];
        $expectedVAId   = $expectedVAIds[$testKey];

        $va = $this->findVisitorAnalytics($tipnId, $tillTimestamp);
        if (empty($expectedVAId)) {
            if (!empty($va) && $va->exists()) {
                $actualId = $va->getId();
                $this->assertTrue(
                    false,
                    "$testKey $specificAdditionalMessage Visitor Anlalytics should NOT been found for TIPN <$tipnId> before $tillTimestamp. But it has been found VA <$actualId>."
                );
            }
        } else {
            $this->assertTrue(
                !empty($va) && $va->exists(),
                "$testKey $specificAdditionalMessage Visitor Anlalytics should been found for TIPN <$tipnId> before $tillTimestamp. It must be VA <$expectedVAId>"
            );
            $actualId = $va->getId();
            $this->assertEquals(
                $expectedVAId,
                $actualId,
                "$testKey $specificAdditionalMessage Visitor Anlalytics should been found for TIPN <$tipnId> before $tillTimestamp."
            );
        }
    }

    public function providerCases() {
        return array(
            "case 01"   => array(self::TEST_KEY_01_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1),
            "case 02"   => array(self::TEST_KEY_02_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1),
            "case 03"   => array(self::TEST_KEY_03_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1),
            "case 11"   => array(self::TEST_KEY_11_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2),
            "case 12"   => array(self::TEST_KEY_12_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2),
            "case 13"   => array(self::TEST_KEY_13_LESS_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2),
            "case 21"   => array(self::TEST_KEY_21_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1),
            "case 22"   => array(self::TEST_KEY_22_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1),
            "case 23"   => array(self::TEST_KEY_23_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_1),
            "case 31"   => array(self::TEST_KEY_31_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2),
            "case 32"   => array(self::TEST_KEY_32_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2),
            "case 33"   => array(self::TEST_KEY_33_MORE_THEN_ALL_OF_ANALYTICS_TO_NUMBER_2),
            "case 41"   => array(self::TEST_KEY_41_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_1),
            "case 42"   => array(self::TEST_KEY_42_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_1),
            "case 43"   => array(self::TEST_KEY_43_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_1),
            "case 51"   => array(self::TEST_KEY_51_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_2),
            "case 52"   => array(self::TEST_KEY_52_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_2),
            "case 53"   => array(self::TEST_KEY_53_MORE_THEN_ONE_OF_ANALYTICS_TO_NUMBER_2),
            "case 61"   => array(self::TEST_KEY_61_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_1),
            "case 62"   => array(self::TEST_KEY_62_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_1),
            "case 63"   => array(self::TEST_KEY_63_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_1),
            "case 71"   => array(self::TEST_KEY_71_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_2),
            "case 72"   => array(self::TEST_KEY_72_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_2),
            "case 73"   => array(self::TEST_KEY_73_MORE_THEN_SEVERAL_OF_ANALYTICS_TO_NUMBER_2),
        );
    }
}
