<?php

/**
 * Description of VisitorAnalyticsTableBase
 * 
 * @author fairdev
 */
abstract class VisitorAnalyticsTableBase extends sfBasePhpunitTestCase
    implements sfPhpunitFixtureDoctrineAggregator {

    /**
     *
     * @param int $tipnId
     * @param timestamp $tillTimestamp 
     */
    protected function findVisitorAnalytics($tipnId, $tillTimestamp) {
        return VisitorAnalyticsTable::findLastForNumberTillTime($tipnId, $tillTimestamp);
    }
}
