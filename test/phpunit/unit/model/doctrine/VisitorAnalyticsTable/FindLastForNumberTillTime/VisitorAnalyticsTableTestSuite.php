<?php

/**
 * Description of VisitorAnalyticsTableSuite
 *
 * @author fairdev
 */
class VisitorAnalyticsTableTestSuite extends sfBasePhpunitTestSuite
    implements sfPhpunitContextInitilizerInterface, sfPhpunitFixtureDoctrineAggregator {

    /**
     * Dev hook for custom "setUp" stuff
     */
    protected function _start() {

        $this->_initFilters();
        $this
            ->fixture()
            ->clean()
            ->loadOwn('01_CompanyTree_01_Bionic.yml')
            ->loadOwn('01_CompanyTree_02_SubBionic.yml')
            ->loadOwn('01_CompanyTree_03_SubResellers.yml')
            ->loadOwn('01_CompanyTree_04_SubAgencies.yml')
            ->loadOwn('01_CompanyTree_05_Campaigns.yml')
            ->loadOwn('02_Users_01_Timezone.yml')
            ->loadOwn('02_Users_02_Groups.yml')
            ->loadOwn('02_Users_03_Users.yml')
            ->loadOwn('03_Pool_01_TwilioAccount.yml')
            ->loadOwn('03_Pool_02_TwilioLocalAreaCode.yml')
            ->loadOwn('03_Pool_03_TwilioIncomingPhoneNumber.yml')
            ->loadOwn('03_Pool_04_BionicIncomingNumberPool.yml')
            ->loadOwn('03_Pool_05_AdvertiserIncomingNumberPool.yml')
            ->loadOwn('04_Calls_01_TwilioCity.yml')
            ->loadOwn('04_Calls_10_ReferrerSource.yml')
            ->loadOwn('04_Calls_11_AnalyticsIp.yml')
            ->loadOwn('04_Calls_12_Page.yml')
            ->loadOwn('04_Calls_13_Combo.yml')
            ->loadOwn('04_Calls_14_PhysicalPhoneNumber.yml')
            ->loadOwn('04_Calls_15_PhoneNumberAssignment.yml')
            ->loadOwn('04_Calls_16_VisitorAnalytics.yml')
            ->loadOwn('04_Calls_17_AnalyticsPhoneNumber.yml')
        ;
    }

    protected function _initFilters() {

        $filters = sfConfig::get('app_sfPhpunitPlugin_filter', array());
        foreach ($filters as $filter) {
            PHPUnit_Util_Filter::addDirectoryToFilter($filter['path'], $filter['ext']);
        }
    }

    public function getApplication() {
        return 'backend';
    }

    public function getOwnFixtureDir() {

        $sep = DIRECTORY_SEPARATOR;
        $reflection = new ReflectionClass($this);
        $path = str_replace($sep . get_class($this) . '.php', '', $reflection->getFileName());

        $ownDir = str_replace(
            "{$sep}test{$sep}phpunit",
            "{$sep}test{$sep}phpunit{$sep}fixtures",
            $path
        );
//var_dump($ownDir);
//var_dump($path);
//die;
        return $ownDir;
    }

    protected function _initFixture(array $options = array()) {

        $options = array(
            'fixture_ext' => ''
        );
        return parent::_initFixture($options);
    }

}

