<?php

require_once dirname(__FILE__) . '/TwilioIncomingCallTableBaseCompanyTree.php';

/**
 * Description of TwilioIncomingCallTableCompanyTreeAdvertiserOfBionicTest
 * 
 * @author fairdev
 */
class TwilioIncomingCallTableCompanyTreeAdvertiserOfBionicTest extends TwilioIncomingCallTableBaseCompanyTree {

    protected function configureFilterAndExpectedValues() {

        $this->setFilterValues(     self::$FILTER_VALUES);
        $this->setExpectedValues(   self::$EXPECTED_VALUES);
    }

    protected function configureUserWithTz() {
        $this->configureCompanyTreeUserWithTz(
            null,
            self::ID_ADVERTISER_BIONIC_ADVERTISER,
            self::TIMEZONE_ID_GMT
        );
    }


    protected static $FILTER_VALUES = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            'from'  => '1317524403', //2011-10-02 03:00:03 +0000
            'to'    => '1317528002', //2011-10-02 04:00:02 +0000
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            'from'  => '1319878577', //2011-10-29 08:56:17 +0000
            'to'    => '1319964976', //2011-10-30 08:56:16 +0000
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            'from'  => '1317254547', //2011-09-29 00:02:27 +0000
            'to'    => '1317859346', //2011-10-06 00:02:26 +0000
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            'from'  => '1313802048', //2011-08-20 01:00:48 +0000
            'to'    => '1316480447', //2011-09-20 01:00:47 +0000
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            'from'  => '1278011098', //2010-07-01 19:04:58 +0000
            'to'    => '1309547097', //2011-07-01 19:04:57 +0000
        ),

        self::TYPE_ALL => array(
        ),

        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'from'  => '1317525541', //2011-10-02 03:19:01 +0000
            'to'    => '1317525600', //2011-10-02 03:20:00 +0000
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
            'from'  => '1319864406', //2011-10-29 05:00:06 +0000
            'to'    => '1319950805', //2011-10-30 05:00:05 +0000
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
            'from'  => '1318386008', //2011-10-12 02:20:08 +0000
            'to'    => '1318386008', //2011-10-19 02:20:07 +0000
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
            'from'  => '1313899215', //2011-08-21 04:00:15 +0000
            'to'    => '1316577614', //2011-09-21 04:00:14 +0000
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
            'from'  => '1247356801', //2009-07-12 00:00:01 +0000
            'to'    => '1278892800', //2010-07-12 00:00:00 +0000
        ),
    );


    protected static $EXPECTED_VALUES = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            'calls_total'                       => 1,
            'leads_unique'                      => 1,
            'duration_incoming_unique'          => '00:00:02',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 1,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:02',
            'incoming_duration_max'             => '00:00:02',
//            'incoming_duration_round'           => '00:01:00',
            'incoming_duration_total'           => '00:00:02',
            'redialed_duration_avg'             => '00:00:01',
            'redialed_duration_max'             => '00:00:01',
            'redialed_duration_total'           => '00:00:01',
//            'redialed_duration_round'           => '00:01:00',
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            'calls_total'                       => 1,
            'leads_unique'                      => 1,
            'duration_incoming_unique'          => '00:00:01',
            'calls_assigned'                    => 1,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:01',
            'incoming_duration_max'             => '00:00:01',
//            'incoming_duration_round'           => '00:01:00',
            'incoming_duration_total'           => '00:00:01',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            'calls_total'                       => 5,
            'leads_unique'                      => 3,
            'duration_incoming_unique'          => '00:00:16',
            'calls_assigned'                    => 4,
            'calls_unassigned_advertiser_owner' => 1,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:09',
            'incoming_duration_max'             => '00:00:30',
//            'incoming_duration_round'           => '00:05:00',
            'incoming_duration_total'           => '00:00:48',
            'redialed_duration_avg'             => '00:00:07',
            'redialed_duration_max'             => '00:00:26',
            'redialed_duration_total'           => '00:00:37',
//            'redialed_duration_round'           => '00:05:00',
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            'calls_total'                       => 1,
            'leads_unique'                      => 1,
            'duration_incoming_unique'          => '00:00:01',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 1,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:01',
            'incoming_duration_max'             => '00:00:01',
//            'incoming_duration_round'           => '00:01:00',
            'incoming_duration_total'           => '00:00:01',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            'calls_total'                       => 2,
            'leads_unique'                      => 2,
            'duration_incoming_unique'          => '00:00:10',
            'calls_assigned'                    => 2,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:05',
            'incoming_duration_max'             => '00:00:07',
//            'incoming_duration_round'           => '00:02:00',
            'incoming_duration_total'           => '00:00:10',
            'redialed_duration_avg'             => '00:00:02',
            'redialed_duration_max'             => '00:00:04',
            'redialed_duration_total'           => '00:00:05',
//            'redialed_duration_round'           => '00:02:00',
        ),

        self::TYPE_ALL => array(
            'calls_total'                       => 10,
            'leads_unique'                      => 4,
            'duration_incoming_unique'          => '00:00:24',
            'calls_assigned'                    => 8,
            'calls_unassigned_advertiser_owner' => 2,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:07',
            'incoming_duration_max'             => '00:00:30',
//            'incoming_duration_round'           => '00:10:00',
            'incoming_duration_total'           => '00:01:18',
            'redialed_duration_avg'             => '00:00:06',
            'redialed_duration_max'             => '00:00:26',
            'redialed_duration_total'           => '00:00:55',
//            'redialed_duration_round'           => '00:08:00',
        ),

        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
    );

}
