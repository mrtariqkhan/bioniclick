<?php

require_once dirname(__FILE__) . '/TwilioIncomingCallTableBaseCompanyTree.php';

/**
 * Description of TwilioIncomingCallTableCompanyTreeBionicTest
 * 
 * @author fairdev
 */
class TwilioIncomingCallTableCompanyTreeBionicTest extends TwilioIncomingCallTableBaseCompanyTree {

    protected function configureFilterAndExpectedValues() {

        $this->setFilterValues(     self::$FILTER_VALUES);
        $this->setExpectedValues(   self::$EXPECTED_VALUES);
    }

    protected function configureUserWithTz() {
        $this->configureCompanyTreeUserWithTz(
            self::ID_COMPANY_BIONIC,
            null,
            self::TIMEZONE_ID_MINUS_5
        );
    }


    protected static $FILTER_VALUES = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            'from'  => '1318377242', //2011-10-11 18:54:02 -0500
            'to'    => '1318380841', //2011-10-11 19:54:01 -0500
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            'from'  => '1317315600', //2011-09-29 12:00:00 -0500
            'to'    => '1317401999', //2011-09-30 11:59:59 -0500
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            'from'  => '1316474634', //2011-09-19 18:23:54 -0500
            'to'    => '1317079433', //2011-09-26 18:23:53 -0500
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            'from'  => '1317402001', //2011-09-30 12:00:01 -0500
            'to'    => '1319994000', //2011-10-30 12:00:00 -0500
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            'from'  => '1284941419', //2010-09-19 19:10:19 -0500
            'to'    => '1316477418', //2011-09-19 19:10:18 -0500
        ),

        self::TYPE_ALL => array(
        ),

        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'from'  => '1278997212', //2010-07-13 00:00:12 -0500
            'to'    => '1279000811', //2010-07-13 01:00:11 -0500
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
            'from'  => '1317776401', //2011-10-04 20:00:01 -0500
            'to'    => '1317862800', //2011-10-05 20:00:00 -0500
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
            'from'  => '1315115820', //2011-09-04 00:57:00 -0500
            'to'    => '1315720620', //2011-09-11 00:57:01 -0500
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
            'from'  => '1309730626', //2011-07-03 17:03:46 -0500
            'to'    => '1309730626', //2011-08-03 17:03:45 -0500
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
            'from'  => '1247356856', //2009-07-11 19:00:56 -0500
            'to'    => '1247356856', //2010-07-11 19:00:55 -0500
        ),
    );


    protected static $EXPECTED_VALUES = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            'calls_total'                       => 3,
            'leads_unique'                      => 3,
            'duration_incoming_unique'          => '00:02:35',
            'calls_assigned'                    => 1,
            'calls_unassigned_advertiser_owner' => 1,
            'calls_unassigned_bionic_owner'     => 1,
            'incoming_duration_avg'             => '00:00:51',
            'incoming_duration_max'             => '00:01:18',
//            'incoming_duration_round'           => '00:04:00',
            'incoming_duration_total'           => '00:02:35',
            'redialed_duration_avg'             => '00:00:47',
            'redialed_duration_max'             => '00:01:11',
            'redialed_duration_total'           => '00:02:21',
//            'redialed_duration_round'           => '00:04:00',
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            'calls_total'                       => 4,
            'leads_unique'                      => 3,
            'duration_incoming_unique'          => '00:00:12',
            'calls_assigned'                    => 4,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => 0,
            'incoming_duration_avg'             => '00:00:03',
            'incoming_duration_max'             => '00:00:05',
//            'incoming_duration_round'           => '00:04:00',
            'incoming_duration_total'           => '00:00:13',
            'redialed_duration_avg'             => '00:00:02',
            'redialed_duration_max'             => '00:00:04',
            'redialed_duration_total'           => '00:00:07',
//            'redialed_duration_round'           => '00:03:00',
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            'calls_total'                       => 3,
            'leads_unique'                      => 2,
            'duration_incoming_unique'          => '00:00:10',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 2,
            'calls_unassigned_bionic_owner'     => 1,
            'incoming_duration_avg'             => '00:00:04',
            'incoming_duration_max'             => '00:00:10',
//            'incoming_duration_round'           => '00:02:00',
            'incoming_duration_total'           => '00:00:12',
            'redialed_duration_avg'             => '00:00:08',
            'redialed_duration_max'             => '00:00:08',
            'redialed_duration_total'           => '00:00:08',
//            'redialed_duration_round'           => '00:01:00',
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            'calls_total'                       => 17,
            'leads_unique'                      => 8,
            'duration_incoming_unique'          => '00:03:21',
            'calls_assigned'                    => 13,
            'calls_unassigned_advertiser_owner' => 3,
            'calls_unassigned_bionic_owner'     => 1,
            'incoming_duration_avg'             => '00:00:16',
            'incoming_duration_max'             => '00:01:18',
//            'incoming_duration_round'           => '00:16:00',
            'incoming_duration_total'           => '00:04:47',
            'redialed_duration_avg'             => '00:00:17',
            'redialed_duration_max'             => '00:01:11',
            'redialed_duration_total'           => '00:03:58',
//            'redialed_duration_round'           => '00:14:00',
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            'calls_total'                       => 5,
            'leads_unique'                      => 3,
            'duration_incoming_unique'          => '00:00:08',
            'calls_assigned'                    => 3,
            'calls_unassigned_advertiser_owner' => 1,
            'calls_unassigned_bionic_owner'     => 1,
            'incoming_duration_avg'             => '00:00:04',
            'incoming_duration_max'             => '00:00:10',
//            'incoming_duration_round'           => '00:05:00',
            'incoming_duration_total'           => '00:00:22',
            'redialed_duration_avg'             => '00:00:03',
            'redialed_duration_max'             => '00:00:08',
            'redialed_duration_total'           => '00:00:13',
//            'redialed_duration_round'           => '00:04:00',
        ),

        self::TYPE_ALL => array(
            'calls_total'                       => 31,
            'leads_unique'                      => 8,
            'duration_incoming_unique'          => '00:00:54',
            'calls_assigned'                    => 23,
            'calls_unassigned_advertiser_owner' => 6,
            'calls_unassigned_bionic_owner'     => 2,
            'incoming_duration_avg'             => '00:00:11',
            'incoming_duration_max'             => '00:01:18',
//            'incoming_duration_round'           => '00:29:00',
            'incoming_duration_total'           => '00:05:43',
            'redialed_duration_avg'             => '00:00:11',
            'redialed_duration_max'             => '00:01:11',
            'redialed_duration_total'           => '00:04:28',
//            'redialed_duration_round'           => '00:24:00',
        ),

        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => 0,
            'incoming_duration_avg'             => '00:00:0',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => 0,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => 0,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => 0,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => 0,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => 0,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
    );

}
