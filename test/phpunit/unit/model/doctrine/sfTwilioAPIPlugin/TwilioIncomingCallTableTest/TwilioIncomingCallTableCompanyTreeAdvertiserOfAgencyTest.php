<?php

require_once dirname(__FILE__) . '/TwilioIncomingCallTableBaseCompanyTree.php';

/**
 * Description of TwilioIncomingCallTableCompanyTreeAdvertiserOfAgencyTest
 * 
 * @author fairdev
 */
class TwilioIncomingCallTableCompanyTreeAdvertiserOfAgencyTest extends TwilioIncomingCallTableBaseCompanyTree {

    protected function configureFilterAndExpectedValues() {

        $this->setFilterValues(     self::$FILTER_VALUES);
        $this->setExpectedValues(   self::$EXPECTED_VALUES);
    }

    protected function configureUserWithTz() {
        $this->configureCompanyTreeUserWithTz(
            null,
            self::ID_ADVERTISER_AGENCY_REALSEARCH_IPS,
            self::TIMEZONE_ID_MINUS_2_AND_A_HALF
        );
    }


    protected static $FILTER_VALUES = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            'from'  => '1315873801', //2011-09-12 22:00:01 -0230
            'to'    => '1315877400', //2011-09-12 23:00:00 -0230
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            'from'  => '1317440101', //2011-10-01 01:05:01 -0230
            'to'    => '1317526500', //2011-10-02 01:05:00 -0230
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            'from'  => '1317940801', //2011-10-06 20:10:01 -0230
            'to'    => '1318545600', //2011-10-13 20:10:00 -0230
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            'from'  => '1314688811', //2011-08-30 04:50:11 -0230
            'to'    => '1317367210', //2011-09-30 04:50:10 -0230
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            'from'  => '1284339437', //2010-09-12 22:27:17 -0230
            'to'    => '1315875436', //2011-09-12 22:27:16 -0230
        ),

        self::TYPE_ALL => array(
        ),

        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'from'  => '1278981069', //2010-07-12 22:01:09 -0230
            'to'    => '1278981068', //2010-07-12 22:01:08 -0230
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
            'from'  => '1317526201', //2011-10-02 01:00:01 -0230
            'to'    => '1317612600', //2011-10-03 01:00:00 -0230
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
            'from'  => '1313899403', //2011-08-21 01:33:23 -0230
            'to'    => '1314504202', //2011-08-28 01:33:22 -0230
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
            'from'  => '1313115905', //2011-08-11 23:55:05 -0230
            'to'    => '1315794304', //2011-09-11 23:55:04 -0230
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
            'from'  => '1277173801', //2010-06-22 00:00:01 -0230
            'to'    => '1308709800', //2011-06-22 00:00:00 -0230
        ),
    );


    protected static $EXPECTED_VALUES = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            'calls_total'                       => 1,
            'leads_unique'                      => 1,
            'duration_incoming_unique'          => '00:00:02',
            'calls_assigned'                    => 1,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:02',
            'incoming_duration_max'             => '00:00:02',
//            'incoming_duration_round'           => '00:01:00',
            'incoming_duration_total'           => '00:00:02',
            'redialed_duration_avg'             => '00:00:01',
            'redialed_duration_max'             => '00:00:01',
            'redialed_duration_total'           => '00:00:01',
//            'redialed_duration_round'           => '00:01:00',
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            'calls_total'                       => 1,
            'leads_unique'                      => 1,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 1,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            'calls_total'                       => 2,
            'leads_unique'                      => 1,
            'duration_incoming_unique'          => '00:00:15',
            'calls_assigned'                    => 2,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:15',
            'incoming_duration_max'             => '00:00:15',
//            'incoming_duration_round'           => '00:02:00',
            'incoming_duration_total'           => '00:00:30',
            'redialed_duration_avg'             => '00:00:12',
            'redialed_duration_max'             => '00:00:13',
            'redialed_duration_total'           => '00:00:25',
//            'redialed_duration_round'           => '00:02:00',
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            'calls_total'                       => 4,
            'leads_unique'                      => 4,
            'duration_incoming_unique'          => '00:00:08',
            'calls_assigned'                    => 3,
            'calls_unassigned_advertiser_owner' => 1,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:02',
            'incoming_duration_max'             => '00:00:05',
//            'incoming_duration_round'           => '00:03:00',
            'incoming_duration_total'           => '00:00:08',
            'redialed_duration_avg'             => '00:00:01',
            'redialed_duration_max'             => '00:00:02',
            'redialed_duration_total'           => '00:00:03',
//            'redialed_duration_round'           => '00:02:00',
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            'calls_total'                       => 1,
            'leads_unique'                      => 1,
            'duration_incoming_unique'          => '00:00:02',
            'calls_assigned'                    => 1,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:02',
            'incoming_duration_max'             => '00:00:02',
//            'incoming_duration_round'           => '00:01:00',
            'incoming_duration_total'           => '00:00:02',
            'redialed_duration_avg'             => '00:00:01',
            'redialed_duration_max'             => '00:00:01',
            'redialed_duration_total'           => '00:00:01',
//            'redialed_duration_round'           => '00:01:00',
        ),

        self::TYPE_ALL => array(
            'calls_total'                       => 9,
            'leads_unique'                      => 4,
            'duration_incoming_unique'          => '00:00:08',
            'calls_assigned'                    => 7,
            'calls_unassigned_advertiser_owner' => 2,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:05',
            'incoming_duration_max'             => '00:00:15',
//            'incoming_duration_round'           => '00:07:00',
            'incoming_duration_total'           => '00:00:50',
            'redialed_duration_avg'             => '00:00:06',
            'redialed_duration_max'             => '00:00:13',
            'redialed_duration_total'           => '00:00:30',
//            'redialed_duration_round'           => '00:05:00',
        ),

        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
    );

}
