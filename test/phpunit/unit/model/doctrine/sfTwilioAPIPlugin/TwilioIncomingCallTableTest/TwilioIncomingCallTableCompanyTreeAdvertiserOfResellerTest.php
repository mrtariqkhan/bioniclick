<?php

require_once dirname(__FILE__) . '/TwilioIncomingCallTableBaseCompanyTree.php';

/**
 * Description of TwilioIncomingCallTableCompanyTreeAdvertiserOfResellerTest
 * 
 * @author fairdev
 */
class TwilioIncomingCallTableCompanyTreeAdvertiserOfResellerTest extends TwilioIncomingCallTableBaseCompanyTree {

    protected function configureFilterAndExpectedValues() {

        $this->setFilterValues(     self::$FILTER_VALUES);
        $this->setExpectedValues(   self::$EXPECTED_VALUES);
    }

    protected function configureUserWithTz() {
        $this->configureCompanyTreeUserWithTz(
            null,
            self::ID_ADVERTISER_RESELLER_ACQUISIO_TRIPS,
            self::TIMEZONE_ID_MINUS_5
        );
    }


    protected static $FILTER_VALUES = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            'from'  => '1319929201', //2011-10-29 18:00:01 -0500
            'to'    => '1319932800', //2011-10-29 19:00:00 -0500
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            'from'  => '1318371776', //2011-10-11 17:22:56 -0500
            'to'    => '1318458175', //2011-10-12 17:22:55 -0500
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            'from'  => '1316469937', //2011-09-19 17:05:37 -0500
            'to'    => '1317074736', //2011-09-26 17:05:36 -0500
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            'from'  => '1317272361', //2011-09-28 23:59:21 -0500
            'to'    => '1317877160', //2011-10-05 23:59:20 -0500
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            'from'  => '1284800401', //2010-09-18 04:00:01 -0500
            'to'    => '1316336400', //2011-09-18 04:00:00 -0500
        ),

        self::TYPE_ALL => array(
        ),

        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'from'  => '1319927837', //2011-10-29 17:37:17 -0500
            'to'    => '1319931436', //2011-10-29 18:37:16 -0500
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
            'from'  => '1278910801', //2010-07-12 00:00:01 -0500
            'to'    => '1278997200', //2010-07-13 00:00:00 -0500
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
            'from'  => '1316732535', //2011-09-22 18:02:15 -0500
            'to'    => '1317337334', //2011-09-29 18:02:14 -0500
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
            'from'  => '1311218063', //2011-07-20 22:14:23 -0500
            'to'    => '1313896463', //2011-08-20 22:14:23 -0500
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
            'from'  => '1279588595', //2010-07-19 20:16:35 -0500
            'to'    => '1311124594', //2011-07-19 20:16:34 -0500
        ),
    );


    protected static $EXPECTED_VALUES = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            'calls_total'                       => 1,
            'leads_unique'                      => 1,
            'duration_incoming_unique'          => '00:00:23',
            'calls_assigned'                    => 1,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:23',
            'incoming_duration_max'             => '00:00:23',
//            'incoming_duration_round'           => '00:01:00',
            'incoming_duration_total'           => '00:00:23',
            'redialed_duration_avg'             => '00:00:18',
            'redialed_duration_max'             => '00:00:18',
            'redialed_duration_total'           => '00:00:18',
//            'redialed_duration_round'           => '00:01:00',
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            'calls_total'                       => 2,
            'leads_unique'                      => 2,
            'duration_incoming_unique'          => '00:01:28',
            'calls_assigned'                    => 1,
            'calls_unassigned_advertiser_owner' => 1,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:44',
            'incoming_duration_max'             => '00:01:18',
//            'incoming_duration_round'           => '00:03:00',
            'incoming_duration_total'           => '00:01:28',
            'redialed_duration_avg'             => '00:00:39',
            'redialed_duration_max'             => '00:01:11',
            'redialed_duration_total'           => '00:01:19',
//            'redialed_duration_round'           => '00:03:00',
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            'calls_total'                       => 1,
            'leads_unique'                      => 1,
            'duration_incoming_unique'          => '00:00:02',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 1,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:02',
            'incoming_duration_max'             => '00:00:02',
//            'incoming_duration_round'           => '00:01:00',
            'incoming_duration_total'           => '00:00:02',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            'calls_total'                       => 4,
            'leads_unique'                      => 4,
            'duration_incoming_unique'          => '00:00:24',
            'calls_assigned'                    => 4,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:06',
            'incoming_duration_max'             => '00:00:16',
//            'incoming_duration_round'           => '00:03:00',
            'incoming_duration_total'           => '00:00:24',
            'redialed_duration_avg'             => '00:00:04',
            'redialed_duration_max'             => '00:00:13',
            'redialed_duration_total'           => '00:00:17',
//            'redialed_duration_round'           => '00:03:00',
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            'calls_total'                       => 2,
            'leads_unique'                      => 2,
            'duration_incoming_unique'          => '00:00:09',
            'calls_assigned'                    => 2,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:04',
            'incoming_duration_max'             => '00:00:05',
//            'incoming_duration_round'           => '00:02:00',
            'incoming_duration_total'           => '00:00:09',
            'redialed_duration_avg'             => '00:00:02',
            'redialed_duration_max'             => '00:00:02',
            'redialed_duration_total'           => '00:00:04',
//            'redialed_duration_round'           => '00:02:00',
        ),

        self::TYPE_ALL => array(
            'calls_total'                       => 10,
            'leads_unique'                      => 7,
            'duration_incoming_unique'          => '00:02:01',
            'calls_assigned'                    => 8,
            'calls_unassigned_advertiser_owner' => 2,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:14',
            'incoming_duration_max'             => '00:01:18',
//            'incoming_duration_round'           => '00:10:00',
            'incoming_duration_total'           => '00:02:26',
            'redialed_duration_avg'             => '00:00:13',
            'redialed_duration_max'             => '00:01:11',
            'redialed_duration_total'           => '00:01:58',
//            'redialed_duration_round'           => '00:09:00',
        ),

        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
    );

}
