<?php

require_once dirname(__FILE__) . '/TwilioIncomingCallTableBase.php';

/**
 * Description of TwilioIncomingCallTableBaseProvided
 * 
 * @author fairdev
 */
abstract class TwilioIncomingCallTableBaseProvided extends TwilioIncomingCallTableBase {

    protected $filterValues     = array();
    protected $expectedValues   = array();


    protected function _start() {

        parent::_start();

        $this->configureFilterAndExpectedValues();
    }

    abstract protected function configureFilterAndExpectedValues();


    public function setFilterValues(array $filterValues) {
        $this->filterValues = $filterValues;
    }

    public function setExpectedValues(array $expectedValues) {
        $this->expectedValues = $expectedValues;
    }
}
