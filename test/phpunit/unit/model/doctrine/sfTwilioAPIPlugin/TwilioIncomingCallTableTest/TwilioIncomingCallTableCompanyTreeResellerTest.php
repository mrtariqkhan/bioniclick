<?php

require_once dirname(__FILE__) . '/TwilioIncomingCallTableBaseCompanyTree.php';

/**
 * Description of TwilioIncomingCallTableCompanyTreeResellerTest
 * 
 * @author fairdev
 */
class TwilioIncomingCallTableCompanyTreeResellerTest extends TwilioIncomingCallTableBaseCompanyTree {

    protected function configureFilterAndExpectedValues() {

        $this->setFilterValues(     self::$FILTER_VALUES);
        $this->setExpectedValues(   self::$EXPECTED_VALUES);
    }

    protected function configureUserWithTz() {
        $this->configureCompanyTreeUserWithTz(
            self::ID_COMPANY_RESELLER_ACQUISIO,
            null,
            self::TIMEZONE_ID_GMT
        );
    }


    protected static $FILTER_VALUES = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            'from'  => '1317524462', //2011-10-02 03:01:02 +0000
            'to'    => '1317528061', //2011-10-02 04:01:01 +0000
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            'from'  => '1317345808', //2011-09-30 01:23:28 +0000
            'to'    => '1317432207', //2011-10-01 01:23:27 +0000
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            'from'  => '1317942655', //2011-10-06 23:10:55 +0000
            'to'    => '1318547454', //2011-10-13 23:10:54 +0000
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            'from'  => '1314933541', //2011-09-02 03:19:01 +0000
            'to'    => '1317525540', //2011-10-02 03:19:00 +0000
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            'from'  => '1283126407', //2010-08-30 00:00:07 +0000
            'to'    => '1314662406', //2011-08-30 00:00:06 +0000
        ),

        self::TYPE_ALL => array(
        ),

        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'from'  => '1313802007', //2011-08-20 01:00:07 +0000
            'to'    => '1313805606', //2011-08-20 02:00:06 +0000
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
            'from'  => '1317229164', //2011-09-28 16:59:24 +0000
            'to'    => '1317315563', //2011-09-29 16:59:23 +0000
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
            'from'  => '1315875619', //2011-09-13 01:00:19 +0000
            'to'    => '1316480187', //2011-09-20 00:56:27 +0000
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
            'from'  => '1277942459', //2010-07-01 00:00:59 +0000
            'to'    => '1280620858', //2010-08-01 00:00:58 +0000
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
            'from'  => '1282348800', //2010-08-21 00:00:00 +0000
            'to'    => '1313884745', //2011-08-20 23:59:59 +0000
        ),
    );


    protected static $EXPECTED_VALUES = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            'calls_total'                       => 4,
            'leads_unique'                      => 4,
            'duration_incoming_unique'          => '00:00:20',
            'calls_assigned'                    => 3,
            'calls_unassigned_advertiser_owner' => 1,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:05',
            'incoming_duration_max'             => '00:00:16',
//            'incoming_duration_round'           => '00:02:00',
            'incoming_duration_total'           => '00:00:20',
            'redialed_duration_avg'             => '00:00:05',
            'redialed_duration_max'             => '00:00:13',
            'redialed_duration_total'           => '00:00:16',
//            'redialed_duration_round'           => '00:02:00',
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            'calls_total'                       => 2,
            'leads_unique'                      => 2,
            'duration_incoming_unique'          => '00:00:09',
            'calls_assigned'                    => 2,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:04',
            'incoming_duration_max'             => '00:00:05',
//            'incoming_duration_round'           => '00:02:00',
            'incoming_duration_total'           => '00:00:09',
            'redialed_duration_avg'             => '00:00:01',
            'redialed_duration_max'             => '00:00:02',
            'redialed_duration_total'           => '00:00:03',
//            'redialed_duration_round'           => '00:02:00',
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            'calls_total'                       => 4,
            'leads_unique'                      => 3,
            'duration_incoming_unique'          => '00:01:43',
            'calls_assigned'                    => 3,
            'calls_unassigned_advertiser_owner' => 1,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:29',
            'incoming_duration_max'             => '00:01:18',
//            'incoming_duration_round'           => '00:05:00',
            'incoming_duration_total'           => '00:01:58',
            'redialed_duration_avg'             => '00:00:26',
            'redialed_duration_max'             => '00:01:11',
            'redialed_duration_total'           => '00:01:44',
//            'redialed_duration_round'           => '00:05:00',
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            'calls_total'                       => 9,
            'leads_unique'                      => 6,
            'duration_incoming_unique'          => '00:00:19',
            'calls_assigned'                    => 6,
            'calls_unassigned_advertiser_owner' => 3,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:02',
            'incoming_duration_max'             => '00:00:05',
//            'incoming_duration_round'           => '00:07:00',
            'incoming_duration_total'           => '00:00:23',
            'redialed_duration_avg'             => '00:00:01',
            'redialed_duration_max'             => '00:00:03',
            'redialed_duration_total'           => '00:00:09',
//            'redialed_duration_round'           => '00:05:00',
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            'calls_total'                       => 1,
            'leads_unique'                      => 1,
            'duration_incoming_unique'          => '00:00:04',
            'calls_assigned'                    => 1,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:04',
            'incoming_duration_max'             => '00:00:04',
//            'incoming_duration_round'           => '00:01:00',
            'incoming_duration_total'           => '00:00:04',
            'redialed_duration_avg'             => '00:00:02',
            'redialed_duration_max'             => '00:00:02',
            'redialed_duration_total'           => '00:00:02',
//            'redialed_duration_round'           => '00:01:00',
        ),

        self::TYPE_ALL => array(
            'calls_total'                       => 19,
            'leads_unique'                      => 7,
            'duration_incoming_unique'          => '00:00:48',
            'calls_assigned'                    => 15,
            'calls_unassigned_advertiser_owner' => 4,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:10',
            'incoming_duration_max'             => '00:01:18',
//            'incoming_duration_round'           => '00:17:00',
            'incoming_duration_total'           => '00:03:16',
            'redialed_duration_avg'             => '00:00:10',
            'redialed_duration_max'             => '00:01:11',
            'redialed_duration_total'           => '00:02:28',
//            'redialed_duration_round'           => '00:14:00',
        ),

        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
    );

}
