<?php

require_once dirname(__FILE__) . '/TwilioIncomingCallTableBaseProvided.php';

/**
 * Description of TwilioIncomingCallTableBaseCompanyTree
 * 
 * @author fairdev
 */
abstract class TwilioIncomingCallTableBaseCompanyTree extends TwilioIncomingCallTableBaseProvided {

    const TYPE_FOUND_ONE_HOUR       = '(Found) One Hour';
    const TYPE_FOUND_ONE_DAY        = '(Found) One Day';
    const TYPE_FOUND_ONE_WEEK       = '(Found) One Week';
    const TYPE_FOUND_ONE_MONTH      = '(Found) One Month';
    const TYPE_FOUND_ONE_YEAR       = '(Found) One Year';

    const TYPE_NOT_FOUND_ONE_HOUR   = '(Not Found) One Hour';
    const TYPE_NOT_FOUND_ONE_DAY    = '(Not Found) One Day';
    const TYPE_NOT_FOUND_ONE_WEEK   = '(Not Found) One Week';
    const TYPE_NOT_FOUND_ONE_MONTH  = '(Not Found) One Month';
    const TYPE_NOT_FOUND_ONE_YEAR   = '(Not Found) One Year';

    const TYPE_ALL                  = 'All time';


    const ID_COMPANY_BIONIC                     = 1;
    const ID_COMPANY_RESELLER_ACQUISIO          = 2;
    const ID_COMPANY_AGENCY_REALSEARCH          = 4;
    const ID_ADVERTISER_BIONIC_ADVERTISER       = 1;
    const ID_ADVERTISER_RESELLER_ACQUISIO_TRIPS = 3;
    const ID_ADVERTISER_AGENCY_REALSEARCH_IPS   = 4;


    /**
     * @dataProvider providerCases
     * 
     */
    public function testCase($testKey, $specificAdditionalMessage = '') {

        $filterValues = $this->filterValues[$testKey];

        $expected = $this->expectedValues[$testKey];
        $actual = parent::getResultArray($filterValues);
//var_dump($testKey);
//var_dump($filterValues);
//var_dump($expected);
//var_dump($actual);
//var_dump($this->expectedValues);
//die;

        $this->assertOverallValuesAreSame(
            $expected, 
            $actual, 
<<<EOD
    Error in Test ($testKey)
        (an attempt to find overall values for '{$this->userKind}' user with timezone '{$this->timezoneName}').
        $specificAdditionalMessage
EOD
        );
    }

    public function providerCases() {
        return array(
            "case 01"   => array(self::TYPE_FOUND_ONE_HOUR),
            "case 02"   => array(self::TYPE_FOUND_ONE_DAY),
            "case 03"   => array(self::TYPE_FOUND_ONE_WEEK),
            "case 04"   => array(self::TYPE_FOUND_ONE_MONTH),
            "case 05"   => array(self::TYPE_FOUND_ONE_YEAR),

            "case 10"   => array(self::TYPE_ALL),

            "case 21"   => array(self::TYPE_NOT_FOUND_ONE_HOUR),
            "case 22"   => array(self::TYPE_NOT_FOUND_ONE_DAY),
            "case 23"   => array(self::TYPE_NOT_FOUND_ONE_WEEK),
            "case 24"   => array(self::TYPE_NOT_FOUND_ONE_MONTH),
            "case 25"   => array(self::TYPE_NOT_FOUND_ONE_YEAR),
        );
    }
}
