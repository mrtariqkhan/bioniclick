<?php

require_once dirname(__FILE__) . '/TwilioIncomingCallTableBaseCompanyTree.php';

/**
 * Description of TwilioIncomingCallTableCompanyTreeAgencyTest
 * 
 * @author fairdev
 */
class TwilioIncomingCallTableCompanyTreeAgencyTest extends TwilioIncomingCallTableBaseCompanyTree {

    protected function configureFilterAndExpectedValues() {

        $this->setFilterValues(     self::$FILTER_VALUES);
        $this->setExpectedValues(   self::$EXPECTED_VALUES);
    }

    protected function configureUserWithTz() {
        $this->configureCompanyTreeUserWithTz(
            self::ID_COMPANY_AGENCY_REALSEARCH,
            null,
            self::TIMEZONE_ID_PLUS_6
        );
    }

    protected static $FILTER_VALUES = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            'from'  => '1319929201', //2011-10-30 05:00:01 +0600
            'to'    => '1319932800', //2011-10-30 06:00:00 +0600
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            'from'  => '1318356137', //2011-10-12 00:02:17 +0600
            'to'    => '1318442536', //2011-10-13 00:02:16 +0600
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            'from'  => '1317254478', //2011-09-29 06:01:18 +0600
            'to'    => '1317859277', //2011-10-06 06:01:17 +0600
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            'from'  => '1317366765', //2011-09-30 13:12:45 +0600
            'to'    => '1319958764', //2011-10-30 13:12:44 +0600
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            'from'  => '1284941411', //2010-09-20 06:10:11 +0600
            'to'    => '1316477410', //2011-09-20 06:10:10 +0600
        ),

        self::TYPE_ALL => array(
        ),

        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'from'  => '1317688201', //2011-10-04 06:30:01 +0600
            'to'    => '1317691800', //2011-10-04 07:30:00 +0600
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
            'from'  => '1316466034', //2011-09-20 03:00:34 +0600
            'to'    => '1316466034', //2011-09-21 03:00:33 +0600
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
            'from'  => '1318389163', //2011-10-12 09:12:43 +0600
            'to'    => '1318993962', //2011-10-19 09:12:42 +0600
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
            'from'  => '1312135385', //2011-08-01 00:03:05 +0600
            'to'    => '1314727384', //2011-08-31 00:03:04 +0600
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
            'from'  => '1277944932', //2010-07-01 06:42:12 +0600
            'to'    => '1309480931', //2011-07-01 06:42:11 +0600
        ),
    );


    protected static $EXPECTED_VALUES = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            'calls_total'                       => 1,
            'leads_unique'                      => 1,
            'duration_incoming_unique'          => '00:00:03',
            'calls_assigned'                    => 1,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:03',
            'incoming_duration_max'             => '00:00:03',
//            'incoming_duration_round'           => '00:01:00',
            'incoming_duration_total'           => '00:00:03',
            'redialed_duration_avg'             => '00:00:02',
            'redialed_duration_max'             => '00:00:02',
            'redialed_duration_total'           => '00:00:02',
//            'redialed_duration_round'           => '00:01:00',
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            'calls_total'                       => 1,
            'leads_unique'                      => 1,
            'duration_incoming_unique'          => '00:00:15',
            'calls_assigned'                    => 1,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:15',
            'incoming_duration_max'             => '00:00:15',
//            'incoming_duration_round'           => '00:01:00',
            'incoming_duration_total'           => '00:00:15',
            'redialed_duration_avg'             => '00:00:13',
            'redialed_duration_max'             => '00:00:13',
            'redialed_duration_total'           => '00:00:13',
//            'redialed_duration_round'           => '00:01:00',
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            'calls_total'                       => 3,
            'leads_unique'                      => 2,
            'duration_incoming_unique'          => '00:00:06',
            'calls_assigned'                    => 2,
            'calls_unassigned_advertiser_owner' => 1,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:02',
            'incoming_duration_max'             => '00:00:05',
//            'incoming_duration_round'           => '00:02:00',
            'incoming_duration_total'           => '00:00:06',
            'redialed_duration_avg'             => '00:00:02',
            'redialed_duration_max'             => '00:00:02',
            'redialed_duration_total'           => '00:00:02',
//            'redialed_duration_round'           => '00:01:00',
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            'calls_total'                       => 6,
            'leads_unique'                      => 3,
            'duration_incoming_unique'          => '00:00:29',
            'calls_assigned'                    => 5,
            'calls_unassigned_advertiser_owner' => 1,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:07',
            'incoming_duration_max'             => '00:00:15',
//            'incoming_duration_round'           => '00:05:00',
            'incoming_duration_total'           => '00:00:47',
            'redialed_duration_avg'             => '00:00:07',
            'redialed_duration_max'             => '00:00:13',
            'redialed_duration_total'           => '00:00:29',
//            'redialed_duration_round'           => '00:04:00',
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            'calls_total'                       => 1,
            'leads_unique'                      => 1,
            'duration_incoming_unique'          => '00:00:02',
            'calls_assigned'                    => 1,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:02',
            'incoming_duration_max'             => '00:00:02',
//            'incoming_duration_round'           => '00:01:00',
            'incoming_duration_total'           => '00:00:02',
            'redialed_duration_avg'             => '00:00:01',
            'redialed_duration_max'             => '00:00:01',
            'redialed_duration_total'           => '00:00:01',
//            'redialed_duration_round'           => '00:01:00',
        ),

        self::TYPE_ALL => array(
            'calls_total'                       => 9,
            'leads_unique'                      => 4,
            'duration_incoming_unique'          => '00:00:08',
            'calls_assigned'                    => 7,
            'calls_unassigned_advertiser_owner' => 2,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:05',
            'incoming_duration_max'             => '00:00:15',
//            'incoming_duration_round'           => '00:07:00',
            'incoming_duration_total'           => '00:00:50',
            'redialed_duration_avg'             => '00:00:06',
            'redialed_duration_max'             => '00:00:13',
            'redialed_duration_total'           => '00:00:30',
//            'redialed_duration_round'           => '00:05:00',
        ),

        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
            'calls_total'                       => 0,
            'leads_unique'                      => 0,
            'duration_incoming_unique'          => '00:00:00',
            'calls_assigned'                    => 0,
            'calls_unassigned_advertiser_owner' => 0,
            'calls_unassigned_bionic_owner'     => NULL,
            'incoming_duration_avg'             => '00:00:00',
            'incoming_duration_max'             => '00:00:00',
//            'incoming_duration_round'           => '00:00:00',
            'incoming_duration_total'           => '00:00:00',
            'redialed_duration_avg'             => '00:00:00',
            'redialed_duration_max'             => '00:00:00',
            'redialed_duration_total'           => '00:00:00',
//            'redialed_duration_round'           => '00:00:00',
        ),
    );

}
