<?php

require_once dirname(__FILE__) . '/../../../../UserTimezoneTestBase.php';

/**
 * Description of TwilioIncomingCallTableBase
 * 
 * @author fairdev
 */
abstract class TwilioIncomingCallTableBase extends UserTimezoneTestBase {

    /**
     *
     * @param array $filterValues
     * @return array
     */
    protected function getResultArray($filterValues) {

        $user = $this->getUser();

        $advertiser = null;
        $company    = null;
        if ($user->isAdvertiserUser()) {
            $advertiser = $user->getAdvertiser();
        } else {
            $company = $user->getFirstCompany();
        }

        return TwilioIncomingCallTable::getOverallValues(
            $advertiser,
            $company,
            $filterValues
        );
    }

    public function assertOverallValuesAreSame($expected, $actual, $message) {
        $this->assertEquals($expected, $actual, $message);
    }
}
