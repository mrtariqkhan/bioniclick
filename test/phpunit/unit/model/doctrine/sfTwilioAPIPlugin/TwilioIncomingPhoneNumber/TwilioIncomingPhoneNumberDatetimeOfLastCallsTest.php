<?php

/**
 * Description of TwilioIncomingPhoneNumberDatetimeOfLastCallsTest
 * 
 * @author fairdev
 */
class TwilioIncomingPhoneNumberDatetimeOfLastCallsTest extends sfBasePhpunitTestCase
    implements sfPhpunitFixtureDoctrineAggregator {

    const TIPN_ID_01 = 1; // bionic advertiser's campaign's phone number
    const TIPN_ID_02 = 3; // bionic advertiser's phone number
    const TIPN_ID_03 = 4; // bionic's phone number
    const TIPN_ID_04 = 5; // reseller's campaign's phone number
    const TIPN_ID_05 = 6; // agency's campaign's phone number

    const TIMEZONE_NAME = 'GMT';
    const FORMAT        = 'Y-m-d H:i:s';

    protected static $CALL_DATETIME = array(
        self::TIPN_ID_01 => '2011-10-30 07:12:38',
        self::TIPN_ID_02 => '2011-10-02 03:44:53',
        self::TIPN_ID_03 => '2011-10-11 23:58:05',
        self::TIPN_ID_04 => '2011-10-30 00:00:00',
        self::TIPN_ID_05 => '2011-10-29 23:02:00',
    );

    /**
     * @dataProvider providerCases
     * 
     */
    public function testCases($tipnIds, $message = '') {

        if (is_array($tipnIds)) {
            if (empty($tipnIds)) {
                $expected = array();
            } else {
                $expected = self::$CALL_DATETIME ;
            }
        } else {
            $expected = array($tipnIds => self::$CALL_DATETIME[$tipnIds]);
            $tipnIds = array($tipnIds);
        }

        foreach ($tipnIds as $tipnId) {
            $twilioIncomingPhoneNumber = TwilioIncomingPhoneNumberTable::getInstance()->find($tipnId);
            $this->assertTrue(
                !empty($twilioIncomingPhoneNumber) && $twilioIncomingPhoneNumber->exists(),
                "$message. Twilio Incoming Phone Number doesn't exist."
            );
        }

        $this->checkDatetimeOfLastCallsToPhoneNumbers(
            $tipnIds,
            $expected,
            $message
        );
    }

    public function providerCases() {
        return array(
            "case 01"       => array(self::TIPN_ID_01, "TIPN is campaign's of advertiser of bionic phone number."),
            "case 02"       => array(self::TIPN_ID_02, "TIPN is advertiser's of bionic phone number."),
            "case 03"       => array(self::TIPN_ID_03, "TIPN is bionic's phone number."),
            "case 04"       => array(self::TIPN_ID_04, "TIPN is campaign's of advertiser of reseller phone number."),
            "case 05"       => array(self::TIPN_ID_05, "TIPN is campaign's of advertiser of agency phone number."),
            "case all"      => array(
                array(
                    self::TIPN_ID_01,
                    self::TIPN_ID_02,
                    self::TIPN_ID_03,
                    self::TIPN_ID_04,
                    self::TIPN_ID_05,
                ), 
                "TIPN is campaign's of advertiser of agency phone number."
            ),
            "case noone 07" => array(array(), "Noone TIPN."),
            "case noone 30" => array(array(), "Noone TIPN."),
        );
    }

    protected function checkDatetimeOfLastCallsToPhoneNumbers(
        $tipnIds,
        $expectedDateTimes,
        $message = ''
    ) {

        $dateTimes = TwilioIncomingCallTable::findDatetimeOfLastCallsToPhoneNumbers(
            $tipnIds, 
            self::TIMEZONE_NAME,
            self::FORMAT
        );

        $this->assertEquals(
            $expectedDateTimes, 
            $dateTimes, 
            "$message Datetime is wrong."
        );
    }
}
