<?php

/**
 * Description of TwilioIncomingPhoneNumberFindIfBelongToAtTimeTest
 * 
 * @author fairdev
 */
class TwilioIncomingPhoneNumberFindIfBelongToAtTimeTest extends sfBasePhpunitTestCase
    implements sfPhpunitFixtureDoctrineAggregator {


    const CASE_CODE_01 = 1;
    const CASE_CODE_02 = 2;
    const CASE_CODE_03 = 3;
    const CASE_CODE_04 = 4;
    const CASE_CODE_05 = 5;
    const CASE_CODE_06 = 6;
    const CASE_CODE_07 = 7;
    const CASE_CODE_08 = 8;
    const CASE_CODE_09 = 9;
    const CASE_CODE_10 = 10;
    const CASE_CODE_11 = 11;
    const CASE_CODE_12 = 12;
    const CASE_CODE_13 = 13;
    const CASE_CODE_14 = 14;
    const CASE_CODE_15 = 15;
    const CASE_CODE_16 = 16;
    const CASE_CODE_17 = 17;
    const CASE_CODE_18 = 18;
    const CASE_CODE_19 = 19;
    const CASE_CODE_20 = 20;
    const CASE_CODE_21 = 21;
    const CASE_CODE_22 = 22;
    const CASE_CODE_23 = 23;
    const CASE_CODE_24 = 24;
    const CASE_CODE_25 = 25;
    const CASE_CODE_26 = 26;
    const CASE_CODE_27 = 27;
    const CASE_CODE_28 = 28;
    const CASE_CODE_29 = 29;
    const CASE_CODE_30 = 30;
    const CASE_CODE_31 = 31;
    const CASE_CODE_32 = 32;
    const CASE_CODE_33 = 33;
    const CASE_CODE_34 = 34;
    const CASE_CODE_35 = 35;
    const CASE_CODE_36 = 36;
    const CASE_CODE_37 = 37;
    const CASE_CODE_38 = 38;
    const CASE_CODE_39 = 39;
    const CASE_CODE_40 = 40;
    const CASE_CODE_41 = 41;
    const CASE_CODE_42 = 42;
    const CASE_CODE_43 = 43;
    const CASE_CODE_44 = 44;


    protected static $CASE_DESCRIPTIONS = array(
        self::CASE_CODE_01 => 'All phone numbers have Bionic as owner. Before all orders.',
        self::CASE_CODE_02 => 'All phone numbers have owner "Snowing" campaign.',
        self::CASE_CODE_03 => 'All phone numbers have Bionic as owner. Timestamp is between orders.',

        self::CASE_CODE_04 => 'All phone numbers have Bionic as owner. Timestamp is between orders for 2, 3, 4, 5 phone numbers. Timestamp is the last before orders for 1, 6 phone numbers.',
        self::CASE_CODE_05 => 'Phone numbers 2, 3, 4, 5 have Bionic as owner. Phone numbers 1, 6 have Advertisers as owners. Timestamp is the start of period of belonging.',
        self::CASE_CODE_06 => 'Phone numbers 2, 3, 4, 5 have Bionic as owner. Phone numbers 1, 6 have Advertisers as owners. Timestamp is the middle of period of belonging.',
        self::CASE_CODE_07 => 'Phone numbers 2, 3, 4, 5 have Bionic as owner. Phone numbers 1, 6 have Advertisers as owners. Timestamp is the end of period of belonging.',
        self::CASE_CODE_08 => 'All phone numbers have Bionic as owner. Timestamp is between orders for 2, 3, 4, 5 phone numbers. Timestamp is the first after orders for 1, 6 phone numbers.',

        self::CASE_CODE_09 => 'Phone numbers 1, 2, 3, 4, 5 have Bionic as owner. Phone number 6 has owner "Snowing" campaign.',

        self::CASE_CODE_10 => 'All phone numbers have Bionic as owner. Timestamp is between orders for 1, 3, 4, 5 phone numbers. Timestamp is the last before orders for 2, 6 phone numbers.',
        self::CASE_CODE_11 => 'Phone numbers 1, 3, 4, 5 have Bionic as owner. Phone numbers 2, 6 have Campaigns as owners. Timestamp is the start of period of belonging.',
        self::CASE_CODE_12 => 'Phone numbers 1, 3, 4, 5 have Bionic as owner. Phone numbers 2, 6 have Campaigns as owners. Timestamp is the middle of period of belonging.',
        self::CASE_CODE_13 => 'Phone numbers 1, 3, 4, 5 have Bionic as owner. Phone numbers 2, 6 have Campaigns as owners. Timestamp is the end of period of belonging.',
        self::CASE_CODE_14 => 'All phone numbers have Bionic as owner. Timestamp is between orders for 1, 3, 4, 5 phone numbers. Timestamp is the first after orders for 2, 6 phone numbers.',

        self::CASE_CODE_15 => 'Phone numbers 1, 2, 3, 4, 5 have Bionic as owner. Phone number 6 has owner "Snowing" campaign.',

        self::CASE_CODE_16 => 'All phone numbers have Bionic as owner. Timestamp is between orders for 1, 2, 4, 5 phone numbers. Timestamp is the last before orders for 3, 6 phone numbers.',
        self::CASE_CODE_17 => 'Phone numbers 1, 2, 4, 5 have Bionic as owner. Phone numbers 3, 6 have Campaigns as owners. Timestamp is the start of period of belonging.',
        self::CASE_CODE_18 => 'Phone numbers 1, 2, 4, 5 have Bionic as owner. Phone numbers 3, 6 have Campaigns as owners. Timestamp is the middle of period of belonging.',
        self::CASE_CODE_19 => 'Phone numbers 1, 2, 4, 5 have Bionic as owner. Phone numbers 3, 6 have Campaigns as owners. Timestamp is the end of period of belonging.',
        self::CASE_CODE_20 => 'Phone numbers 1, 2, 4, 5 have Bionic as owner. Phone numbers 3, 6 have Advertisers as owners. Timestamp is after belonging to Campaign. Timestamp is the middle of period of belonging to Advertiser.',
        self::CASE_CODE_21 => 'Phone numbers 1, 2, 4, 5 have Bionic as owner. Phone numbers 3, 6 have Advertisers as owners. Timestamp is after belonging to Campaign. Timestamp is the end of period of belonging to Advertiser.',
        self::CASE_CODE_22 => 'All phone numbers have Bionic as owner. Timestamp is between orders for 1, 2, 4, 5 phone numbers. Timestamp is the first after orders for 3, 6 phone numbers.',

        self::CASE_CODE_23 => 'All phone numbers have Bionic as owner. Phone number 6 has NO owner too ("Snowing" campaign was not owner).',

        self::CASE_CODE_24 => 'All phone numbers have Bionic as owner. Timestamp is between orders for 1, 2, 3, 5 phone numbers. Timestamp is the last before orders for 4, 6 phone numbers.',
        self::CASE_CODE_25 => 'Phone numbers 1, 2, 3, 5 have Bionic as owner. Phone numbers 4, 6 have Advertisers as owners. Timestamp is the start of period of belonging. Timestamp is before belonging to Campaigns.',
        self::CASE_CODE_26 => 'Phone numbers 1, 2, 3, 5 have Bionic as owner. Phone numbers 4, 6 have Campaigns as owners. Timestamp is the start of period of belonging. Timestamp is the last before belonging to Campaigns.',
        self::CASE_CODE_27 => 'Phone numbers 1, 2, 3, 5 have Bionic as owner. Phone numbers 4, 6 have Campaigns as owners. Timestamp is the middle of period of belonging.',
        self::CASE_CODE_28 => 'Phone numbers 1, 2, 3, 5 have Bionic as owner. Phone numbers 4, 6 have Campaigns as owners. Timestamp is the end of period of belonging.',
        self::CASE_CODE_29 => 'Phone numbers 1, 2, 3, 5 have Bionic as owner. Phone numbers 4, 6 have Advertisers as owners. Timestamp is the mifdlle of period of belonging. Timestamp is the first after belonging to Campaigns.',
        self::CASE_CODE_30 => 'Phone numbers 1, 2, 3, 5 have Bionic as owner. Phone numbers 4, 6 have Advertisers as owners. Timestamp is the end of period of belonging. Timestamp is after belonging to Campaigns.',
        self::CASE_CODE_31 => 'All phone numbers have Bionic as owner. Timestamp is between orders for 1, 2, 3, 5 phone numbers. Timestamp is the first after orders for 4, 6 phone numbers.',

        self::CASE_CODE_32 => 'Phone numbers 1, 2, 3, 4, 5 have Bionic as owner. Phone number 6 has owner "Snowing" campaign.',

        self::CASE_CODE_33 => 'All phone numbers have Bionic as owner. Timestamp is between orders for 1, 2, 3, 4 phone numbers. Timestamp is the last before orders for 5, 6 phone numbers.',
        self::CASE_CODE_34 => 'Phone numbers 1, 2, 3, 4 have Bionic as owner. Phone numbers 5, 6 have Advertisers as owners. Timestamp is the start of period of belonging.',
        self::CASE_CODE_35 => 'Phone numbers 1, 2, 3, 4 have Bionic as owner. Phone numbers 5, 6 have Advertisers as owners. Timestamp is the middle of period of belonging.',
        self::CASE_CODE_36 => 'Phone numbers 1, 2, 3, 4 have Bionic as owner. Phone numbers 5, 6 have Campaigns as owners. Timestamp is the middle of period of belonging to Advertisers. Timestamp is the start of period of belonging to Campaigns.',
        self::CASE_CODE_37 => 'Phone numbers 1, 2, 3, 4 have Bionic as owner. Phone numbers 5, 6 have Campaigns as owners. Timestamp is the middle of period of belonging to Advertisers. Timestamp is the middle of period of belonging to Campaigns.',
        self::CASE_CODE_38 => 'Phone numbers 1, 2, 3, 4 have Bionic as owner. Phone numbers 5, 6 have Campaigns as owners. Timestamp is the end of period of belonging to Advertisers. Timestamp is the end of period of belonging to Campaigns.',
        self::CASE_CODE_39 => 'All phone numbers have Bionic as owner. Timestamp is between orders for 1, 2, 3, 4 phone numbers. Timestamp is the first after orders for 5, 6 phone numbers.',

        self::CASE_CODE_40 => 'All phone numbers have Bionic as owner. Before all orders.',

        self::CASE_CODE_41 => 'All phone numbers have owner "Snowing" campaign.',

        self::CASE_CODE_42 => 'All phone numbers have Bionic as owner. Timestamp is some before last orders.',

        self::CASE_CODE_43 => 'All phone numbers have Bionic as owner. Timestamp is the last before last orders.',
        self::CASE_CODE_44 => 'All phone numbers have Adevrtiser or Campaign as owner. The last orders.',
    );


    protected static $TIMESTAMPS = array(
        self::CASE_CODE_01 => 1323000001,
        self::CASE_CODE_02 => 1323000005,
        self::CASE_CODE_03 => 1323000009,
        self::CASE_CODE_04 => 1323000011,
        self::CASE_CODE_05 => 1323000012,
        self::CASE_CODE_06 => 1323000014,
        self::CASE_CODE_07 => 1323000015,
        self::CASE_CODE_08 => 1323000016,
        self::CASE_CODE_09 => 1323000020,
        self::CASE_CODE_10 => 1323000023,
        self::CASE_CODE_11 => 1323000024,
        self::CASE_CODE_12 => 1323000026,
        self::CASE_CODE_13 => 1323000027,
        self::CASE_CODE_14 => 1323000028,
        self::CASE_CODE_15 => 1323000032,
        self::CASE_CODE_16 => 1323000035,
        self::CASE_CODE_17 => 1323000036,
        self::CASE_CODE_18 => 1323000038,
        self::CASE_CODE_19 => 1323000039,
        self::CASE_CODE_20 => 1323000042,
        self::CASE_CODE_21 => 1323000043,
        self::CASE_CODE_22 => 1323000044,
        self::CASE_CODE_23 => 1323000048,
        self::CASE_CODE_24 => 1323000051,
        self::CASE_CODE_25 => 1323000052,
        self::CASE_CODE_26 => 1323000054,
        self::CASE_CODE_27 => 1323000056,
        self::CASE_CODE_28 => 1323000057,
        self::CASE_CODE_29 => 1323000058,
        self::CASE_CODE_30 => 1323000059,
        self::CASE_CODE_31 => 1323000060,
        self::CASE_CODE_32 => 1323000064,
        self::CASE_CODE_33 => 1323000067,
        self::CASE_CODE_34 => 1323000068,
        self::CASE_CODE_35 => 1323000070,
        self::CASE_CODE_36 => 1323000072,
        self::CASE_CODE_37 => 1323000074,
        self::CASE_CODE_38 => 1323000075,
        self::CASE_CODE_39 => 1323000076,
        self::CASE_CODE_40 => 1323000079,
        self::CASE_CODE_41 => 1323000083,
        self::CASE_CODE_42 => 1323000087,
        self::CASE_CODE_43 => 1323000089,
        self::CASE_CODE_44 => 1323000090,
    );


    const TIPN_ID_01 = 1;
    const TIPN_ID_02 = 2;
    const TIPN_ID_03 = 5;
    const TIPN_ID_04 = 6;
    const TIPN_ID_05 = 9;
    const TIPN_ID_06 = 10;

    const ADVERTISER_ID_BIONIC  = 1;
    const ADVERTISER_ID_TRIPS   = 3;
    const ADVERTISER_ID_IPS     = 4;
    const ADVERTISER_ID_SNOW    = 5;

    const CAMPAIGN_ID_4ALLPROMOS= 1;
    const CAMPAIGN_ID_TRIPS     = 3;
    const CAMPAIGN_ID_IPS       = 4;
    const CAMPAIGN_ID_SNOWING   = 5;

    const ANY                   = 0;

    const IF_BELONG = 'if_belong';
    const ADV_ID    = 'advertiser_id';
    const CAM_ID    = 'campaign_id';


    protected static $EXPECTED = array(
        self::CASE_CODE_01 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_02 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_03 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_04 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_05 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_06 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_07 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_08 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_09 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_10 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_11 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_12 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_13 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_14 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_15 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_16 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_17 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_18 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_19 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_20 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_21 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_22 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_23 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_24 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_25 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_26 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_27 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_28 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_29 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_30 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_31 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_32 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_33 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_34 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_35 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_36 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_37 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_38 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_39 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_40 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_41 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_42 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_43 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
        self::CASE_CODE_44 => array(
            self::TIPN_ID_01 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_02 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_03 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_TRIPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_TRIPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_04 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_IPS,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_IPS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_05 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
            self::TIPN_ID_06 => array(
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => NULL,
                    self::CAM_ID    => NULL,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ADVERTISER_ID_BIONIC,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => true,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_4ALLPROMOS,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ADVERTISER_ID_SNOW,
                    self::CAM_ID    => self::ANY,
                ),
                array(
                    self::IF_BELONG => false,
                    self::ADV_ID    => self::ANY,
                    self::CAM_ID    => self::CAMPAIGN_ID_SNOWING,
                ),
            ),
        ),
    );

    /**
     * @dataProvider providerCases
     * 
     */
    public function testCases($caseCode, $tipnId, $timestamp, $expectedArr, $description = '') {

        $expected       = $expectedArr[self::IF_BELONG];
        $advertiserId   = $expectedArr[self::ADV_ID];
        $campaignId     = $expectedArr[self::CAM_ID];
        $actual = $this->findResults($tipnId, $advertiserId, $campaignId, $timestamp);


        $msg = '';
        if (is_null($advertiserId)) {
            if (is_null($campaignId)) {
                $msg .= 'Bionic';
            }
        } elseif (empty($advertiserId)) {
            if (is_null($campaignId)) {
                
            } elseif (empty($campaignId)) {
                $msg .= 'Any Advertiser and Any Campaign';
            } else {
                $msg .= 'Any Advertiser and Campaign';
            }
        } else {
            if (is_null($campaignId)) {
                $msg .= 'Advertiser and noone Campaign';
            } elseif (empty($campaignId)) {
                $msg .= 'Advertiser and Any Campaign';
            } else {
                $msg .= 'Advertiser and Campaign';
            }
        }
        $msg = "Phone number must belong to " . (!empty($msg) ? $msg : '????');
//        if ($expected != $actual) {
//            var_dump('---:');
//            var_dump($expected);
//            var_dump($actual);
//            var_dump($advertiserId);
//            var_dump($campaignId);
//            var_dump($msg);
//            var_dump($caseCode);
//            var_dump($tipnId);
//            var_dump($timestamp);
//            var_dump($expectedArr);
//            var_dump($description);
//            die;
//        }

        $description = str_replace('. ', "\n  ", "$description");
        $this->assertEquals(
            $expected,
            $actual,
            "Case: $description \nError: Belonging is wrong. $msg."
        );
    }

    public function providerCases() {

        $cases = array();
        foreach (self::$EXPECTED as $caseCode => $expectedForTimestamp) {
            $description = self::$CASE_DESCRIPTIONS[$caseCode];
            $timestamp = self::$TIMESTAMPS[$caseCode];
            foreach ($expectedForTimestamp as $tipnId => $expectedForPhoneNumber) {
                foreach ($expectedForPhoneNumber as $i => $expected) {
                    $cases["case phone timestamp #$caseCode <id=$tipnId> case #$i"] = array($caseCode, $tipnId, $timestamp, $expected, $description);
                }
            }
        }

        return $cases;
    }

    protected function findResults($tipnId, $advertiserId, $campaignId, $atTime) {
        return TwilioIncomingPhoneNumberTable::findIfBelongToAtTime($tipnId, $advertiserId, $campaignId, $atTime);
    }
}
