<?php

/**
 * Description of TwilioIncomingPhoneNumberAmountOfCallsTest
 * 
 * @author fairdev
 */
class TwilioIncomingPhoneNumberAmountOfCallsTest extends sfBasePhpunitTestCase
    implements sfPhpunitFixtureDoctrineAggregator {

    const TIPN_ID_01 = 1; // bionic advertiser's campaign's phone number
    const TIPN_ID_02 = 3; // bionic advertiser's phone number
    const TIPN_ID_03 = 4; // bionic's phone number
    const TIPN_ID_04 = 5; // reseller's campaign's phone number
    const TIPN_ID_05 = 6; // agency's campaign's phone number

    const AMOUNT_OF_DAYS_07     = 7;
    const AMOUNT_OF_DAYS_30     = 30;

    // in GMT0
    const TO = '2011-10-30 08:00:00';

    protected static $AVG_CALLS_AMOUNT = array(
        self::AMOUNT_OF_DAYS_07 => array(
            self::TIPN_ID_01 => 1,
            self::TIPN_ID_02 => 0,
            self::TIPN_ID_03 => 0,
            self::TIPN_ID_04 => 1,
            self::TIPN_ID_05 => 2,
        ),
        self::AMOUNT_OF_DAYS_30 => array(
            self::TIPN_ID_01 => 3,
            self::TIPN_ID_02 => 1,
            self::TIPN_ID_03 => 1,
            self::TIPN_ID_04 => 6,
            self::TIPN_ID_05 => 4,
        ),
    );

    /**
     * @dataProvider providerCases
     * 
     */
    public function testCases($tipnIds, $amountOfDays, $message = '') {

        if (is_array($tipnIds)) {
            if (empty($tipnIds)) {
                $expected = array();
            } else {
                $expected = self::$AVG_CALLS_AMOUNT[$amountOfDays];
            }
        } else {
            $expected = array($tipnIds => self::$AVG_CALLS_AMOUNT[$amountOfDays][$tipnIds]);
        }

        $tipnIdsToCheck = is_array($tipnIds) ? $tipnIds : array($tipnIds);
        foreach ($tipnIdsToCheck as $tipnId) {
            $twilioIncomingPhoneNumber = TwilioIncomingPhoneNumberTable::getInstance()->find($tipnId);
            $this->assertTrue(
                !empty($twilioIncomingPhoneNumber) && $twilioIncomingPhoneNumber->exists(),
                "$message. Twilio Incoming Phone Number doesn't exist."
            );
        }

        $this->checkAmountOfCallsToPhoneNumbersDuringLastNDays(
            $tipnIds,
            $amountOfDays,
            $expected,
            $message
        );
    }

    public function providerCases() {
        return array(
            "case 01 07"    => array(self::TIPN_ID_01, self::AMOUNT_OF_DAYS_07, "TIPN is campaign's of advertiser of bionic phone number."),
            "case 01 30"    => array(self::TIPN_ID_01, self::AMOUNT_OF_DAYS_30, "TIPN is campaign's of advertiser of bionic phone number."),

            "case 02 07"    => array(self::TIPN_ID_02, self::AMOUNT_OF_DAYS_07, "TIPN is advertiser's of bionic phone number."),
            "case 02 30"    => array(self::TIPN_ID_02, self::AMOUNT_OF_DAYS_30, "TIPN is advertiser's of bionic phone number."),

            "case 03 07"    => array(self::TIPN_ID_03, self::AMOUNT_OF_DAYS_07, "TIPN is bionic's phone number."),
            "case 03 30"    => array(self::TIPN_ID_03, self::AMOUNT_OF_DAYS_30, "TIPN is bionic's phone number."),

            "case 04 07"    => array(self::TIPN_ID_04, self::AMOUNT_OF_DAYS_07, "TIPN is campaign's of advertiser of reseller phone number."),
            "case 04 30"    => array(self::TIPN_ID_04, self::AMOUNT_OF_DAYS_30, "TIPN is campaign's of advertiser of reseller phone number."),

            "case 05 07"    => array(self::TIPN_ID_05, self::AMOUNT_OF_DAYS_07, "TIPN is campaign's of advertiser of agency phone number."),
            "case 05 30"    => array(self::TIPN_ID_05, self::AMOUNT_OF_DAYS_30, "TIPN is campaign's of advertiser of agency phone number."),

            "case all 07"   => array(
                array(
                    self::TIPN_ID_01,
                    self::TIPN_ID_02,
                    self::TIPN_ID_03,
                    self::TIPN_ID_04,
                    self::TIPN_ID_05,
                ), 
                self::AMOUNT_OF_DAYS_07,
                "All TIPNs."
            ),
            "case all 30 "  => array(
                array(
                    self::TIPN_ID_01,
                    self::TIPN_ID_02,
                    self::TIPN_ID_03,
                    self::TIPN_ID_04,
                    self::TIPN_ID_05,
                ), 
                self::AMOUNT_OF_DAYS_30,
                "All TIPNs."
            ),
            "case noone 07" => array(array(), self::AMOUNT_OF_DAYS_07, "Noone TIPN."),
            "case noone 30" => array(array(), self::AMOUNT_OF_DAYS_30, "Noone TIPN."),
        );
    }

    protected function checkAmountOfCallsToPhoneNumbersDuringLastNDays(
        $tipnIds,
        $amountOfDays,
        $expectedCallsAmounts,
        $message = ''
    ) {

        $needOffset = DataBaseTimezoneHelper::DEFAULT_OFFSET;
        $offset     = DataBaseTimezoneHelper::findOffset();

        $amounts = TwilioIncomingCallTable::getAmountOfCallsToPhoneNumbersDuringLastNDays(
            $tipnIds, 
            $amountOfDays,
            self::TO
        );
//        switch ($amounts) {
//            case self::AMOUNT_OF_DAYS_07:
//                $actual = $amounts['calls_7'];
//                break;
//            case self::AMOUNT_OF_DAYS_07:
//                $actual = $amounts['calls_30'];
//                break;
//            default:
//                $actual = array();
//        }

        $this->assertEquals(
            $expectedCallsAmounts, 
            $amounts, 
            "$message Amount is wrong."
        );
    }
}
