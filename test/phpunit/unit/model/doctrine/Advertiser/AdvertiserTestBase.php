<?php

/**
 * Description of AdvertiserBase
 * 
 * @author fairdev
 */
abstract class AdvertiserTestBase extends sfBasePhpunitTestCase
    implements sfPhpunitFixtureDoctrineAggregator {

    const COMPANY_ID_BIONIC         = 1;
    const COMPANY_ID_RESELLER       = 2;
    const COMPANY_ID_AGENCY         = 4;

    const ADVERTISER_ID_BIONIC      = 1;
    const ADVERTISER_ID_RESELLER    = 3;
    const ADVERTISER_ID_AGENCY      = 4;


    const USER_ID_SUPER_ADMIN                   = 1;

    const USER_ID_COMPANY_BIONIC_ADMIN          = 2;
    const USER_ID_COMPANY_RESELLER_ADMIN        = 5;
    const USER_ID_COMPANY_AGENCY_ADMIN          = 7;

    const USER_ID_ADVERTISER_BIONIC_ADMIN       = 3;
    const USER_ID_ADVERTISER_RESELLER_ADMIN     = 6;
    const USER_ID_ADVERTISER_AGENCY_ADMIN       = 8;


    /**
     *
     * @param int $advertiserId one of self::ADVERTISER_ID_*
     * @return Advertiser 
     */
    protected function findAdvertiser($advertiserId) {

        $object = null;
        if (!empty($advertiserId)) {
            $object = AdvertiserTable::getInstance()->find($advertiserId);
            if (empty($object) || !$object->exists()) {
                throw new Exception('Cannot find such Advertiser.');
            }
        }

        return $object;
    }

    /**
     *
     * @param int $advertiserId one of self::USER_ID_*
     * @return sfGuardUser 
     */
    protected function findUser($userId) {

        $object = null;
        if (!empty($userId)) {
            $object = sfGuardUserTable::getInstance()->find($userId);
            if (empty($object) || !$object->exists()) {
                throw new Exception('Cannot find such sfGuardUser.');
            }
        } else {
            throw new Exception('userId must be not empty.');
        }

        return $object;
    }
}
