<?php

require_once dirname(__FILE__) . '/AdvertiserTestBase.php';

/**
 * Description of PhoneCallTableBaseAdvertiserTreeTest
 * 
 * @author fairdev
 */
class AdvertiserChartsBreadCrumbsTest extends AdvertiserTestBase {

    // CompanyIds and AdvertiserIds by bread crumb name
     protected static $COMPANY_NAMES = array(
        "'Bionic Click'"=> self::COMPANY_ID_BIONIC,
        "'Acquisio'"    => self::COMPANY_ID_RESELLER,
        "'Real Search'" => self::COMPANY_ID_AGENCY,
    );

    // array (
    //   userId => array(advertiserId => array(CompanyIds which must be in result parent breadCrumbs)),
    //   userId => array(advertiserId => null <when method 'find parent breadCrumbs' must throw exception>),
    // )
    protected static $EXPECTED_COMPANY_IDS = array(
        self::USER_ID_SUPER_ADMIN => array(
            self::ADVERTISER_ID_BIONIC     => array(
                self::COMPANY_ID_BIONIC,
            ),
            self::ADVERTISER_ID_RESELLER   => array(
                self::COMPANY_ID_BIONIC,
                self::COMPANY_ID_RESELLER,
            ),
            self::ADVERTISER_ID_AGENCY     => array(
                self::COMPANY_ID_BIONIC,
                self::COMPANY_ID_RESELLER,
                self::COMPANY_ID_AGENCY,
            ),
        ),
        self::USER_ID_COMPANY_BIONIC_ADMIN => array(
            self::ADVERTISER_ID_BIONIC     => array(
                self::COMPANY_ID_BIONIC,
            ),
            self::ADVERTISER_ID_RESELLER   => array(
                self::COMPANY_ID_BIONIC,
                self::COMPANY_ID_RESELLER,
            ),
            self::ADVERTISER_ID_AGENCY     => array(
                self::COMPANY_ID_BIONIC,
                self::COMPANY_ID_RESELLER,
                self::COMPANY_ID_AGENCY,
            ),
        ),
        self::USER_ID_COMPANY_RESELLER_ADMIN => array(
            self::ADVERTISER_ID_BIONIC     => null,
            self::ADVERTISER_ID_RESELLER   => array(
                self::COMPANY_ID_RESELLER,
            ),
            self::ADVERTISER_ID_AGENCY     => array(
                self::COMPANY_ID_RESELLER,
                self::COMPANY_ID_AGENCY,
            ),
        ),
        self::USER_ID_COMPANY_AGENCY_ADMIN => array(
            self::ADVERTISER_ID_BIONIC     => null,
            self::ADVERTISER_ID_RESELLER   => null,
            self::ADVERTISER_ID_AGENCY     => array(
                self::COMPANY_ID_AGENCY,
            ),
        ),
        self::USER_ID_ADVERTISER_BIONIC_ADMIN => array(
            self::ADVERTISER_ID_BIONIC     => array(
            ),
            self::ADVERTISER_ID_RESELLER   => null,
            self::ADVERTISER_ID_AGENCY     => null,
        ),
        self::USER_ID_ADVERTISER_RESELLER_ADMIN => array(
            self::ADVERTISER_ID_BIONIC     => null,
            self::ADVERTISER_ID_RESELLER   => array(
            ),
            self::ADVERTISER_ID_AGENCY     => null,
        ),
        self::USER_ID_ADVERTISER_AGENCY_ADMIN => array(
            self::ADVERTISER_ID_BIONIC     => null,
            self::ADVERTISER_ID_RESELLER   => null,
            self::ADVERTISER_ID_AGENCY     => array(
            ),
        ),
    );

    /**
     * @dataProvider providerCases
     * 
     */
    public function testCase($userId, $advId, $specificAdditionalMessage = '') {

        $object = $this->findAdvertiser($advId);
        $user = $this->findUser($userId);

        $expectedCompanyIds = self::$EXPECTED_COMPANY_IDS[$userId][$advId];
        if (is_null($expectedCompanyIds)) {
            $this->checkException(
                $user, 
                $object, 
                $specificAdditionalMessage
            );
        } else {
            $expectedCompanyIds = is_null($expectedCompanyIds) ? array() : $expectedCompanyIds;

            $this->checkResult(
                $user, 
                $object,
                $expectedCompanyIds,
                $specificAdditionalMessage
            );
        }
    }

    protected function checkResult($user, $object, $expectedCompanyIds, $specificAdditionalMessage = '') {

        $breadCrumbs = $object->getChartsParentBreadCrumbs($user);
        foreach ($breadCrumbs as $breadCrumb) {
            $actualCompanyName = $breadCrumb->getName();

            if (array_key_exists($actualCompanyName, self::$COMPANY_NAMES)) {
                $actualCompanyId = self::$COMPANY_NAMES[$actualCompanyName];

                $this->assertTrue(
                    !(array_search($actualCompanyId, $expectedCompanyIds) === false), 
                    "$specificAdditionalMessage Bread crumbs have wrong node ($actualCompanyName)."
                );
            } else {
                $this->assertTrue(false, "$specificAdditionalMessage Bread crumbs have wrong node($actualCompanyName).");
            }
        }


        $expectedCount = count(array_keys($expectedCompanyIds, true));
        $actualCount = count($breadCrumbs);

        $this->assertEquals(
            $expectedCount,
            $actualCount,  
            "$specificAdditionalMessage Bread crumbs have wrong count."
        );
    }
    
    protected function checkException($user, $object, $specificAdditionalMessage = '') {

//        $this->setExpectedException('Exception');
        try {
            $breadCrumbs = $object->getChartsParentBreadCrumbs($user);
        } catch (Exception $e) {
            return;
        }
        $this->fail("$specificAdditionalMessage An expected exception has not been raised.");
    }

    public function providerCases() {
        return array(
            "case Super Admin looks at Advertiser Bionic" => array(
                self::USER_ID_SUPER_ADMIN,
                self::ADVERTISER_ID_BIONIC,
                "Bread crumbs for Super Admin user starting from Bionic's advertiser."
            ),
            "case Super Admin looks at Advertiser Reseller" => array(
                self::USER_ID_SUPER_ADMIN,
                self::ADVERTISER_ID_RESELLER,
                "Bread crumbs for Super Admin user starting from Reseller's advertiser."
            ),
            "case Super Admin looks at Advertiser Agency" => array(
                self::USER_ID_SUPER_ADMIN,
                self::ADVERTISER_ID_AGENCY,
                "Bread crumbs for Super Admin user starting from Agency's advertiser."
            ),


            "case Bionic Admin looks at Advertiser Bionic" => array(
                self::USER_ID_COMPANY_BIONIC_ADMIN,
                self::ADVERTISER_ID_BIONIC,
                "Bread crumbs for Bionic Admin user starting from Bionic's advertiser."
            ),
            "case Bionic Admin looks at Advertiser Reseller" => array(
                self::USER_ID_COMPANY_BIONIC_ADMIN,
                self::ADVERTISER_ID_RESELLER,
                "Bread crumbs for Bionic Admin user starting from Reseller's advertiser."
            ),
            "case Bionic Admin looks at Advertiser Agency" => array(
                self::USER_ID_COMPANY_BIONIC_ADMIN,
                self::ADVERTISER_ID_AGENCY,
                "Bread crumbs for Bionic Admin user starting from Agency's advertiser."
            ),


            "case Reseller Admin looks at Advertiser Bionic" => array(
                self::USER_ID_COMPANY_RESELLER_ADMIN,
                self::ADVERTISER_ID_BIONIC,
                "Bread crumbs for Reseller Admin user starting from Bionic's advertiser."
            ),
            "case Reseller Admin looks at Advertiser Reseller" => array(
                self::USER_ID_COMPANY_RESELLER_ADMIN,
                self::ADVERTISER_ID_RESELLER,
                "Bread crumbs for Reseller Admin user starting from Reseller's advertiser."
            ),
            "case Reseller Admin looks at Advertiser Agency" => array(
                self::USER_ID_COMPANY_RESELLER_ADMIN,
                self::ADVERTISER_ID_AGENCY,
                "Bread crumbs for Reseller Admin user starting from Agency's advertiser."
            ),


            "case Agency Admin looks at Advertiser Bionic" => array(
                self::USER_ID_COMPANY_AGENCY_ADMIN,
                self::ADVERTISER_ID_BIONIC,
                "Bread crumbs for Agency Admin user starting from Bionic's advertiser."
            ),
            "case Agency Admin looks at Advertiser Reseller" => array(
                self::USER_ID_COMPANY_AGENCY_ADMIN,
                self::ADVERTISER_ID_RESELLER,
                "Bread crumbs for Agency Admin user starting from Reseller's advertiser."
            ),

            "case Agency Admin looks at Advertiser Agency" => array(
                self::USER_ID_COMPANY_AGENCY_ADMIN,
                self::ADVERTISER_ID_AGENCY,
                "Bread crumbs for Agency Admin user starting from Agency's advertiser."
            ),


            "case Bionic Advertiser Admin looks at Advertiser Bionic" => array(
                self::USER_ID_ADVERTISER_BIONIC_ADMIN,
                self::ADVERTISER_ID_BIONIC,
                "Bread crumbs for Bionic Advertiser Admin user starting from Bionic's advertiser."
            ),
            "case Bionic Advertiser Admin looks at Advertiser Reseller" => array(
                self::USER_ID_ADVERTISER_BIONIC_ADMIN,
                self::ADVERTISER_ID_RESELLER,
                "Bread crumbs for Bionic Advertiser Admin user starting from Reseller's advertiser."
            ),
            "case Bionic Advertiser Admin looks at Advertiser Agency" => array(
                self::USER_ID_ADVERTISER_BIONIC_ADMIN,
                self::ADVERTISER_ID_AGENCY,
                "Bread crumbs for Bionic Advertiser Admin user starting from Agency's advertiser."
            ),


            "case Reseller Advertiser Admin looks at Advertiser Bionic" => array(
                self::USER_ID_ADVERTISER_RESELLER_ADMIN,
                self::ADVERTISER_ID_BIONIC,
                "Bread crumbs for Reseller Advertiser Admin user starting from Bionic's advertiser."
            ),
            "case Reseller Advertiser Admin looks at Advertiser Reseller" => array(
                self::USER_ID_ADVERTISER_RESELLER_ADMIN,
                self::ADVERTISER_ID_RESELLER,
                "Bread crumbs for Reseller Advertiser Admin user starting from Reseller's advertiser."
            ),
            "case Reseller Advertiser Admin looks at Advertiser Agency" => array(
                self::USER_ID_ADVERTISER_RESELLER_ADMIN,
                self::ADVERTISER_ID_AGENCY,
                "Bread crumbs for Reseller Advertiser Admin user starting from Agency's advertiser."
            ),


            "case Agency Advertiser Admin looks at Advertiser Bionic" => array(
                self::USER_ID_ADVERTISER_AGENCY_ADMIN,
                self::ADVERTISER_ID_BIONIC,
                "Bread crumbs for Agency Advertiser Admin user starting from Bionic's advertiser."
            ),
            "case Agency Advertiser Admin looks at Advertiser Reseller" => array(
                self::USER_ID_ADVERTISER_AGENCY_ADMIN,
                self::ADVERTISER_ID_RESELLER,
                "Bread crumbs for Agency Advertiser Admin user starting from Reseller's advertiser."
            ),
            "case Agency Advertiser Admin looks at Advertiser Agency" => array(
                self::USER_ID_ADVERTISER_AGENCY_ADMIN,
                self::ADVERTISER_ID_AGENCY,
                "Bread crumbs for Agency Advertiser Admin user starting from Agency's advertiser."
            ),
        );
    }
}
