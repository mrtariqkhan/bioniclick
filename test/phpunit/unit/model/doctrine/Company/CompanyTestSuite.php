<?php

/**
 * Description of CompanyTestSuite
 *
 * @author fairdev
 */
class CompanyTestSuite extends sfBasePhpunitTestSuite
    implements sfPhpunitContextInitilizerInterface, sfPhpunitFixtureDoctrineAggregator {

    /**
     * Dev hook for custom "setUp" stuff
     */
    protected function _start() {

        $this->_initFilters();
        $this
            ->fixture()
            ->clean()
            ->loadOwn('01_CompanyTree_01_Bionic.yml')
            ->loadOwn('01_CompanyTree_02_SubBionic.yml')
            ->loadOwn('01_CompanyTree_03_SubResellers.yml')
            ->loadOwn('01_CompanyTree_04_SubAgencies.yml')
            ->loadOwn('02_Users_01_Timezone.yml')
            ->loadOwn('02_Users_02_Groups.yml')
            ->loadOwn('02_Users_03_Users.yml')
        ;
    }

    protected function _initFilters() {

        $filters = sfConfig::get('app_sfPhpunitPlugin_filter', array());
        foreach ($filters as $filter) {
            PHPUnit_Util_Filter::addDirectoryToFilter($filter['path'], $filter['ext']);
        }
    }

    public function getApplication() {
        return 'backend';
    }

    public function getOwnFixtureDir() {

        $sep = DIRECTORY_SEPARATOR;
        $reflection = new ReflectionClass($this);
        $path = str_replace($sep . get_class($this) . '.php', '', $reflection->getFileName());

        $ownDir = str_replace(
            "{$sep}test{$sep}phpunit",
            "{$sep}test{$sep}phpunit{$sep}fixtures",
            $path
        );
//var_dump($ownDir);
//var_dump($path);
//die;
        return $ownDir;
    }

    protected function _initFixture(array $options = array()) {

        $options = array(
            'fixture_ext' => ''
        );
        return parent::_initFixture($options);
    }

}

