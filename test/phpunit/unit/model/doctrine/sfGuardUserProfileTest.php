<?php

/**
 * Description of sfGuardUserProfileTest
 *
 * @author fairdev
 */
//class sfGuardUserProfileTest extends sfBasePhpunitTestCase
//    implements sfPhpunitFixtureDoctrineAggregator {
//
//    //TODO:: write tests instead of this:
//    // 1). create new Advertiser(Company) -> authorizeCustomerProfileId will be create
//    // 2). delete Advertiser(Company) -> authorizeCustomerProfileId will be delete
//    // 3). isDebtor
//
//    const amount = 10;
//
//    protected function _start() {
//
//        parent::_start();
//        $this->user = sfGuardUserTable::findByUsername('bionic-admin-MOORE@bionicclick.com');
//        $this->profile = $this->user->getProfile();
//    }
//
//    public function testNeedToDebit() {
//        $this->assertTrue($this->profile->needToDebit());
//    }
//
//    public function testDebitUserBalanceFail() {
//
//        $credit = $this->profile->getCredit();
//        $result = $this->profile->debitUserBalance(-1);
//        $this->assertFalse($result);
//    }
//
//    public function testDebitUserBalanceSuccess() {
//
//        $credit = $this->profile->getCredit();
//        $result = $this->profile->debitUserBalance(self::amount);
//        $this->assertTrue($result);
//        $this->assertEquals($credit + self::amount, $this->profile->getCredit());
//    }
//
//    public function testReduceUserBalanceFail() {
//
//        $credit = $this->profile->getCredit();
//        $result = $this->profile->reduceUserBalance(-1);
//        $this->assertFalse($result);
//    }
//
//    public function testReduceUserBalanceSuccess() {
//
//        $credit = $this->profile->getCredit();
//        $result = $this->profile->reduceUserBalance(self::amount);
//        $this->assertTrue($result);
//        $this->assertEquals($credit - self::amount, $this->profile->getCredit());
//    }
//
//}
