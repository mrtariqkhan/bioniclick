<?php

require_once dirname(__FILE__) . '/PackageCompanyTestBase.php';

/**
 * Description of CountNonDeletedPackagesForParentCompanyTest
 * 
 * @author fairdev
 */
class CountNonDeletedPackagesForParentCompanyTest extends PackageCompanyTestBase {

    protected static $EXPECTED_COUNT_OF_PACKAGES = array(
        self::COMPANY_ID_RESELLER                       => 2,
        self::COMPANY_ID_AGENCY                         => 2,
        self::COMPANY_ID_BIONIC_WITH_DELETED_SUBTREE    => 3,
        self::COMPANY_ID_RESELLER_WITH_DELETED_SUBTREE  => 1,
        self::COMPANY_ID_AGENCY_WITH_DELETED_SUBTREE    => 1,
        self::COMPANY_ID_RESELLER_DELETED               => 0,
        self::COMPANY_ID_AGENCY_DELETED                 => 0,
    );

    /**
     * @dataProvider providerCases
     * 
     */
    public function testCase(
        $comId, 
        $specificAdditionalMessage = ''
    ) {

        $company = $this->findCompany($comId);

        $actual = PackageTable::countNonDeletedPackagesForParentCompany($comId);
        $expected = self::$EXPECTED_COUNT_OF_PACKAGES[$comId];
        $this->assertEquals(
            $expected,
            $actual,
            "$specificAdditionalMessage Amount of existent packages which company '$company' has  is worng."
        );
    }

    public function providerCases() {
        return array(
            "case existent packages from Reseller" => array(
                self::COMPANY_ID_RESELLER,
                "Reseller's existent Packages."
            ),
            "case existent packages from Agency" => array(
                self::COMPANY_ID_AGENCY,
                "Agency's existent Packages."
            ),


            "case existent packages from Bionic with deleted subtree" => array(
                self::COMPANY_ID_BIONIC_WITH_DELETED_SUBTREE,
                "Bionic's existent Packages."
            ),
            "case existent packages from Reseller with deleted subtree" => array(
                self::COMPANY_ID_RESELLER_WITH_DELETED_SUBTREE,
                "Reseller's existent Packages."
            ),
            "case existent packages from Agency with deleted subtree" => array(
                self::COMPANY_ID_AGENCY_WITH_DELETED_SUBTREE,
                "Agency's existent Packages."
            ),


            "case existent packages from deleted Reseller" => array(
                self::COMPANY_ID_RESELLER_DELETED,
                "Deleted Reseller's existent Packages."
            ),
            "case existent packages from deleted Agency" => array(
                self::COMPANY_ID_AGENCY_DELETED,
                "Deleted Agency's existent Packages."
            ),
        );
    }
}
