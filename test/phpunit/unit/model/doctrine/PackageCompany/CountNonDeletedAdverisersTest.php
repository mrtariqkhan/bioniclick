<?php

require_once dirname(__FILE__) . '/PackageCompanyTestBase.php';

/**
 * Description of CountNonDeletedAdverisersTest
 * 
 * @author fairdev
 */
class CountNonDeletedAdverisersTest extends PackageCompanyTestBase {

    protected static $EXPECTED_COUNT_OF_ADVERTISERS = array(
        self::PACKAGE_ID_FROM_BIONIC_NOT_IN_USE         => 0,
        self::PACKAGE_ID_FROM_RESELLER_NOT_IN_USE       => 0,
        self::PACKAGE_ID_FROM_AGENCY_NOT_IN_USE         => 0,

        self::PACKAGE_ID_FOR_ADVERTISER_BIONIC          => 1,
        self::PACKAGE_ID_FOR_ADVERTISER_RESELLER        => 1,
        self::PACKAGE_ID_FOR_ADVERTISER_AGENCY          => 1,

        self::PACKAGE_ID_FROM_BIONIC_IN_USE_BY_DELETED  => 0,
        self::PACKAGE_ID_FROM_RESELLER_IN_USE_BY_DELETED=> 0,
        self::PACKAGE_ID_FROM_AGENCY_IN_USE_BY_DELETED  => 0,
    );

    /**
     * @dataProvider providerCases
     * 
     */
    public function testCase(
        $packageId, 
        $specificAdditionalMessage = ''
    ) {

        $package = $this->findPackage($packageId);

        $actual = PackageTable::countNonDeletedAdverisers($packageId);
        $expected = self::$EXPECTED_COUNT_OF_ADVERTISERS[$packageId];
        $this->assertEquals(
            $expected,
            $actual,
            "$specificAdditionalMessage Amount of existent advertisers which uses package '$package' is worng."
        );
    }

    public function providerCases() {
        return array(
            "case existent advertisers for package from Bionic which is not in use" => array(
                self::PACKAGE_ID_FROM_BIONIC_NOT_IN_USE,
                "Advertisers for Package from Bionicwhich is not in use."
            ),
            "case existent advertisers for package from Reseller which is not in use" => array(
                self::PACKAGE_ID_FROM_RESELLER_NOT_IN_USE,
                "Advertisers for Package from Reseller which is not in use."
            ),
            "case existent advertisers for package from Agency which is not in use" => array(
                self::PACKAGE_ID_FROM_AGENCY_NOT_IN_USE,
                "Advertisers for Package from Agency which is not in use."
            ),


            "case existent advertisers for package for Bionic Advertiser" => array(
                self::PACKAGE_ID_FOR_ADVERTISER_BIONIC,
                "Advertisers for Package from Bionic."
            ),
            "case existent advertisers for package for Reseller Advertiser" => array(
                self::PACKAGE_ID_FOR_ADVERTISER_RESELLER,
                "Advertisers for Package from Reseller."
            ),
            "case existent advertisers for package for Agency Advertiser" => array(
                self::PACKAGE_ID_FOR_ADVERTISER_AGENCY,
                "Advertisers for Package from Agency."
            ),


            "case existent advertisers for package from Bionic with deleted Advertisers" => array(
                self::PACKAGE_ID_FROM_BIONIC_IN_USE_BY_DELETED,
                "Advertisers for Package from Bionic with deleted Advertisers."
            ),
            "case existent advertisers for package from Reseller with deleted Advertisers" => array(
                self::PACKAGE_ID_FROM_RESELLER_IN_USE_BY_DELETED,
                "Advertisers for Package from Reseller with deleted Advertisers."
            ),
            "case existent advertisers for package from Agency with deleted Advertisers" => array(
                self::PACKAGE_ID_FROM_AGENCY_IN_USE_BY_DELETED,
                "Advertisers for Package from Agency with deleted Advertisers."
            ),
        );
    }
}
