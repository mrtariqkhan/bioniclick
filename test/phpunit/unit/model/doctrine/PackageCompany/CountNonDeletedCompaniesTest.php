<?php

require_once dirname(__FILE__) . '/PackageCompanyTestBase.php';

/**
 * Description of CountNonDeletedCompaniesTest
 * 
 * @author fairdev
 */
class CountNonDeletedCompaniesTest extends PackageCompanyTestBase {

    protected static $EXPECTED_COUNT_OF_COMPANIES = array(
        self::PACKAGE_ID_FROM_BIONIC_NOT_IN_USE         => 0,
        self::PACKAGE_ID_FROM_RESELLER_NOT_IN_USE       => 0,
        self::PACKAGE_ID_FROM_AGENCY_NOT_IN_USE         => 0,

        self::PACKAGE_ID_FOR_RESELLER                   => 1,
        self::PACKAGE_ID_FOR_AGENCY                     => 1,

        self::PACKAGE_ID_FROM_BIONIC_IN_USE_BY_DELETED  => 0,
        self::PACKAGE_ID_FROM_RESELLER_IN_USE_BY_DELETED=> 0,
        self::PACKAGE_ID_FROM_AGENCY_IN_USE_BY_DELETED  => 0,
    );

    /**
     * @dataProvider providerCases
     * 
     */
    public function testCase(
        $packageId, 
        $specificAdditionalMessage = ''
    ) {

        $package = $this->findPackage($packageId);

        $actual = PackageTable::countNonDeletedAdverisers($packageId);
        $expected = self::$EXPECTED_COUNT_OF_COMPANIES[$packageId];
        $this->assertEquals(
            $expected,
            $actual,
            "$specificAdditionalMessage Amount of existent companies which uses package '$package' is worng."
        );
    }

    public function providerCases() {
        return array(
            "case existent companies for package from Bionic which is not in use" => array(
                self::PACKAGE_ID_FROM_BIONIC_NOT_IN_USE,
                "Companies for Package from Bionicwhich is not in use."
            ),
            "case existent companies for package from Reseller which is not in use" => array(
                self::PACKAGE_ID_FROM_RESELLER_NOT_IN_USE,
                "Companies for Package from Reseller which is not in use."
            ),
            "case existent companies for package from Agency which is not in use" => array(
                self::PACKAGE_ID_FROM_AGENCY_NOT_IN_USE,
                "Companies for Package from Agency which is not in use."
            ),


            "case existent companies for package for Reseller Company" => array(
                self::PACKAGE_ID_FOR_RESELLER,
                "Companies for Package for Reseller."
            ),
            "case existent companies for package for Agency Company" => array(
                self::PACKAGE_ID_FOR_AGENCY,
                "Companies for Package for Agency."
            ),


            "case existent companies for package from Bionic with deleted Companies" => array(
                self::PACKAGE_ID_FROM_BIONIC_IN_USE_BY_DELETED,
                "Companies for Package from Bionic with deleted Companies."
            ),
            "case existent companies for package from Reseller with deleted Companies" => array(
                self::PACKAGE_ID_FROM_RESELLER_IN_USE_BY_DELETED,
                "Companies for Package from Bionic with deleted Companies."
            ),
            "case existent companies for package from Agency with deleted Companies" => array(
                self::PACKAGE_ID_FROM_AGENCY_IN_USE_BY_DELETED,
                "Companies for Package from Bionic with deleted Companies."
            ),
        );
    }
}
