<?php

/**
 * Description of PackageCompanyTestBase
 * 
 * @author fairdev
 */
abstract class PackageCompanyTestBase extends sfBasePhpunitTestCase
    implements sfPhpunitFixtureDoctrineAggregator {

    const COMPANY_ID_BIONIC         = 1;
    const COMPANY_ID_RESELLER       = 2;
    const COMPANY_ID_AGENCY         = 6;

    const COMPANY_ID_BIONIC_WITH_DELETED_SUBTREE    = 1;
    const COMPANY_ID_RESELLER_WITH_DELETED_SUBTREE  = 3;
    const COMPANY_ID_AGENCY_WITH_DELETED_SUBTREE    = 8;

    const COMPANY_ID_RESELLER_DELETED  = 5;
    const COMPANY_ID_AGENCY_DELETED    = 7;


    const PACKAGE_ID_FOR_RESELLER               = 1;
    const PACKAGE_ID_FOR_AGENCY                 = 4;
    const PACKAGE_ID_FOR_ADVERTISER_BIONIC      = 1;
    const PACKAGE_ID_FOR_ADVERTISER_RESELLER    = 4;
    const PACKAGE_ID_FOR_ADVERTISER_AGENCY      = 9;

    const PACKAGE_ID_FROM_BIONIC_NOT_IN_USE     = 2;
    const PACKAGE_ID_FROM_RESELLER_NOT_IN_USE   = 8;
    const PACKAGE_ID_FROM_AGENCY_NOT_IN_USE     = 10;

    const PACKAGE_ID_FROM_BIONIC_IN_USE_BY_DELETED     = 3;
    const PACKAGE_ID_FROM_RESELLER_IN_USE_BY_DELETED   = 5;
    const PACKAGE_ID_FROM_AGENCY_IN_USE_BY_DELETED     = 11;


    /**
     *
     * @param int $packageId one of self::PACKAGE_ID_*
     * @return Package 
     */
    protected function findPackage($packageId) {

        $object = null;
        if (!empty($packageId)) {
            $object = PackageTable::getInstance()->find($packageId);
        }
        if (empty($object) || !$object->exists()) {
            throw new Exception('Cannot find such Package.');
        }

        return $object;
    }

    /**
     *
     * @param int $companyId one of self::COMPANY_ID_*
     * @return Company 
     */
    protected function findCompany($companyId) {

        $object = null;
        if (!empty($companyId)) {
            $object = CompanyTable::getInstance()->find($companyId);
        }
        if (empty($object) || !$object->exists()) {
            throw new Exception('Cannot find such Company.');
        }

        return $object;
    }
}
