<?php

/**
 * Description of PhoneCallTableUpdateByActualTwilioIncomingCallTest
 * 
 * @author fairdev
 */
class PhoneCallTableUpdateByActualTwilioIncomingCallTest extends sfBasePhpunitTestCase
    implements sfPhpunitFixtureDoctrineAggregator {

    const CALL_TIMESTAMP_003 = 1323000003;
    const CALL_TIMESTAMP_004 = 1323000004;
    const CALL_TIMESTAMP_005 = 1323000005;
    const CALL_TIMESTAMP_006 = 1323000006;
    const CALL_TIMESTAMP_007 = 1323000007;
    const CALL_TIMESTAMP_008 = 1323000008;
    const CALL_TIMESTAMP_009 = 1323000009;
    const CALL_TIMESTAMP_010 = 1323000010;
    const CALL_TIMESTAMP_013 = 1323000013;
    const CALL_TIMESTAMP_015 = 1323000015;
    const CALL_TIMESTAMP_016 = 1323000016;
    const CALL_TIMESTAMP_017 = 1323000017;
    const CALL_TIMESTAMP_018 = 1323000018;
    const CALL_TIMESTAMP_019 = 1323000019;
    const CALL_TIMESTAMP_020 = 1323000020;
    const CALL_TIMESTAMP_021 = 1323000021;
    const CALL_TIMESTAMP_022 = 1323000022;
    const CALL_TIMESTAMP_026 = 1323000026;
    const CALL_TIMESTAMP_029 = 1323000029;
    const CALL_TIMESTAMP_030 = 1323000030;
    const CALL_TIMESTAMP_031 = 1323000031;
    const CALL_TIMESTAMP_032 = 1323000032;
    const CALL_TIMESTAMP_034 = 1323000034;
    const CALL_TIMESTAMP_035 = 1323000035;
    const CALL_TIMESTAMP_036 = 1323000036;
    const CALL_TIMESTAMP_040 = 1323000040;
    const CALL_TIMESTAMP_043 = 1323000043;
    const CALL_TIMESTAMP_044 = 1323000044;
    const CALL_TIMESTAMP_045 = 1323000045;
    const CALL_TIMESTAMP_046 = 1323000046;
    const CALL_TIMESTAMP_047 = 1323000047;
    const CALL_TIMESTAMP_048 = 1323000048;
    const CALL_TIMESTAMP_049 = 1323000049;
    const CALL_TIMESTAMP_050 = 1323000050;
    const CALL_TIMESTAMP_051 = 1323000051;
    const CALL_TIMESTAMP_052 = 1323000052;
    const CALL_TIMESTAMP_053 = 1323000053;
    const CALL_TIMESTAMP_054 = 1323000054;
    const CALL_TIMESTAMP_055 = 1323000055;
    const CALL_TIMESTAMP_056 = 1323000056;
    const CALL_TIMESTAMP_059 = 1323000059;
    const CALL_TIMESTAMP_062 = 1323000062;
    const CALL_TIMESTAMP_063 = 1323000063;
    const CALL_TIMESTAMP_065 = 1323000065;
    const CALL_TIMESTAMP_066 = 1323000066;
    const CALL_TIMESTAMP_067 = 1323000067;
    const CALL_TIMESTAMP_068 = 1323000068;
    const CALL_TIMESTAMP_069 = 1323000069;
    const CALL_TIMESTAMP_070 = 1323000070;
    const CALL_TIMESTAMP_071 = 1323000071;
    const CALL_TIMESTAMP_072 = 1323000072;
    const CALL_TIMESTAMP_074 = 1323000074;
    const CALL_TIMESTAMP_075 = 1323000075;
    const CALL_TIMESTAMP_076 = 1323000076;
    const CALL_TIMESTAMP_077 = 1323000077;
    const CALL_TIMESTAMP_078 = 1323000078;
    const CALL_TIMESTAMP_082 = 1323000082;
    const CALL_TIMESTAMP_085 = 1323000085;
    const CALL_TIMESTAMP_086 = 1323000086;
    const CALL_TIMESTAMP_087 = 1323000087;
    const CALL_TIMESTAMP_088 = 1323000088;
    const CALL_TIMESTAMP_089 = 1323000089;
    const CALL_TIMESTAMP_090 = 1323000090;
    const CALL_TIMESTAMP_091 = 1323000091;
    const CALL_TIMESTAMP_092 = 1323000092;
    const CALL_TIMESTAMP_093 = 1323000093;
    const CALL_TIMESTAMP_094 = 1323000094;
    const CALL_TIMESTAMP_095 = 1323000095;
    const CALL_TIMESTAMP_096 = 1323000096;
    const CALL_TIMESTAMP_100 = 1323000100;
    const CALL_TIMESTAMP_101 = 1323000101;
    const CALL_TIMESTAMP_102 = 1323000102;
    const CALL_TIMESTAMP_103 = 1323000103;
    const CALL_TIMESTAMP_104 = 1323000104;
    const CALL_TIMESTAMP_105 = 1323000105;
    const CALL_TIMESTAMP_106 = 1323000106;
    const CALL_TIMESTAMP_107 = 1323000107;
    const CALL_TIMESTAMP_109 = 1323000109;
    const CALL_TIMESTAMP_113 = 1323000113;
    const CALL_TIMESTAMP_114 = 1323000114;
    const CALL_TIMESTAMP_115 = 1323000115;
//    const CALL_TIMESTAMP_XXX = 1323000XXX;


    const TIPN_ID_01 = 1;
    const TIPN_ID_02 = 2;
    const TIPN_ID_03 = 5;
    const TIPN_ID_04 = 6;
    const TIPN_ID_05 = 9;
    const TIPN_ID_06 = 10;


    
    const CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE= 'Wrong: PhoneCall does not exist, but it should.';

    const CASE_TYPE_CRRCT__OLD_EMPTY__NEW_EMPTY             = 'Phone Call exists, and is Correct: old is [Adv <   >, Cam <   >, VA <   >], correct one is [Adv <   >, Cam <   >, VA <   >]';
    const CASE_TYPE_WRONG__OLD_EMPTY__NEW_ADV               = 'Phone Call exists, but is Wrong:   old is [Adv <   >, Cam <   >, VA <   >], correct one is [Adv <YYY>, Cam <   >, VA <   >]';
    const CASE_TYPE_WRONG__OLD_EMPTY__NEW_CAM               = 'Phone Call exists, but is Wrong:   old is [Adv <   >, Cam <   >, VA <   >], correct one is [Adv <YYY>, Cam <ZZZ>, VA <   >]';
    const CASE_TYPE_WRONG__OLD_EMPTY__NEW_CAM_VA            = 'Phone Call exists, but is Wrong:   old is [Adv <   >, Cam <   >, VA <   >], correct one is [Adv <YYY>, Cam <ZZZ>, VA <AAA>]';

    const CASE_TYPE_WRONG__OLD_ADV__NEW_EMPTY               = 'Phone Call exists, but is Wrong:   old is [Adv <YYY>, Cam <   >, VA <   >], correct one is [Adv <   >, Cam <   >, VA <   >]';
    const CASE_TYPE_CRRCT__OLD_ADV__NEW_ADV                 = 'Phone Call exists, and is Correct: old is [Adv <YYY>, Cam <   >, VA <   >], correct one is [Adv <YYY>, Cam <   >, VA <   >]';
    const CASE_TYPE_WRONG__OLD_ADV__NEW_CAM                 = 'Phone Call exists, but is Wrong:   old is [Adv <YYY>, Cam <   >, VA <   >], correct one is [Adv <YYY>, Cam <ZZZ>, VA <   >]';
    const CASE_TYPE_WRONG__OLD_ADV__NEW_CAM_VA              = 'Phone Call exists, but is Wrong:   old is [Adv <YYY>, Cam <   >, VA <   >], correct one is [Adv <YYY>, Cam <ZZZ>, VA <AAA>]';
    const CASE_TYPE_WRONG__OLD_ADV__NEW_ADV1                = 'Phone Call exists, but is Wrong:   old is [Adv <YYY>, Cam <   >, VA <   >], correct one is [Adv <XXX>, Cam <   >, VA <   >]';
    const CASE_TYPE_WRONG__OLD_ADV__NEW_ADV1_CAM1           = 'Phone Call exists, but is Wrong:   old is [Adv <YYY>, Cam <   >, VA <   >], correct one is [Adv <XXX>, Cam <SSS>, VA <   >]';
    const CASE_TYPE_WRONG__OLD_ADV__NEW_ADV1_CAM1_VA1       = 'Phone Call exists, but is Wrong:   old is [Adv <YYY>, Cam <   >, VA <   >], correct one is [Adv <XXX>, Cam <SSS>, VA <OOO>]';

    const CASE_TYPE_WRONG__OLD_CAM__NEW_EMPTY               = 'Phone Call exists, but is Wrong:   old is [Adv <YYY>, Cam <ZZZ>, VA <   >], correct one is [Adv <   >, Cam <   >, VA <   >]';
    const CASE_TYPE_WRONG__OLD_CAM__NEW_ADV                 = 'Phone Call exists, but is Wrong:   old is [Adv <YYY>, Cam <ZZZ>, VA <   >], correct one is [Adv <YYY>, Cam <   >, VA <   >]';
    const CASE_TYPE_CRRCT__OLD_CAM__NEW_CAM                 = 'Phone Call exists, and is Correct: old is [Adv <YYY>, Cam <ZZZ>, VA <   >], correct one is [Adv <YYY>, Cam <ZZZ>, VA <   >]';
    const CASE_TYPE_WRONG__OLD_CAM__NEW_CAM_VA              = 'Phone Call exists, but is Wrong:   old is [Adv <YYY>, Cam <ZZZ>, VA <   >], correct one is [Adv <YYY>, Cam <ZZZ>, VA <AAA>]';
    const CASE_TYPE_WRONG__OLD_CAM__NEW_CAM1                = 'Phone Call exists, but is Wrong:   old is [Adv <YYY>, Cam <ZZZ>, VA <   >], correct one is [Adv <YYY>, Cam <SSS>, VA <   >]';
    const CASE_TYPE_WRONG__OLD_CAM__NEW_CAM1_VA1            = 'Phone Call exists, but is Wrong:   old is [Adv <YYY>, Cam <ZZZ>, VA <   >], correct one is [Adv <YYY>, Cam <SSS>, VA <OOO>]';
    const CASE_TYPE_WRONG__OLD_CAM__NEW_ADV1                = 'Phone Call exists, but is Wrong:   old is [Adv <YYY>, Cam <ZZZ>, VA <   >], correct one is [Adv <XXX>, Cam <   >, VA <   >]';
    const CASE_TYPE_WRONG__OLD_CAM__NEW_ADV1_CAM1           = 'Phone Call exists, but is Wrong:   old is [Adv <YYY>, Cam <ZZZ>, VA <   >], correct one is [Adv <XXX>, Cam <SSS>, VA <   >]';
    const CASE_TYPE_WRONG__OLD_CAM__NEW_ADV1_CAM1_VA1       = 'Phone Call exists, but is Wrong:   old is [Adv <YYY>, Cam <ZZZ>, VA <   >], correct one is [Adv <XXX>, Cam <SSS>, VA <OOO>]';

    const CASE_TYPE_WRONG__OLD_CAM_VA__NEW_EMPTY            = 'Phone Call exists, but is Wrong:   old is [Adv <YYY>, Cam <ZZZ>, VA <AAA>], correct one is [Adv <   >, Cam <   >, VA <   >]';
    const CASE_TYPE_WRONG__OLD_CAM_VA__NEW_ADV              = 'Phone Call exists, but is Wrong:   old is [Adv <YYY>, Cam <ZZZ>, VA <AAA>], correct one is [Adv <YYY>, Cam <   >, VA <   >]';
    const CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM              = 'Phone Call exists, but is Wrong:   old is [Adv <YYY>, Cam <ZZZ>, VA <AAA>], correct one is [Adv <YYY>, Cam <ZZZ>, VA <   >]';
    const CASE_TYPE_CRRCT__OLD_CAM_VA__NEW_CAM_VA           = 'Phone Call exists, and is Correct: old is [Adv <YYY>, Cam <ZZZ>, VA <AAA>], correct one is [Adv <YYY>, Cam <ZZZ>, VA <AAA>]';
    const CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM1             = 'Phone Call exists, but is Wrong:   old is [Adv <YYY>, Cam <ZZZ>, VA <AAA>], correct one is [Adv <YYY>, Cam <SSS>, VA <   >]';
    const CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM1_VA1         = 'Phone Call exists, but is Wrong:   old is [Adv <YYY>, Cam <ZZZ>, VA <AAA>], correct one is [Adv <YYY>, Cam <SSS>, VA <OOO>]';
    const CASE_TYPE_WRONG__OLD_CAM_VA__NEW_ADV1             = 'Phone Call exists, but is Wrong:   old is [Adv <YYY>, Cam <ZZZ>, VA <AAA>], correct one is [Adv <XXX>, Cam <   >, VA <   >]';
    const CASE_TYPE_WRONG__OLD_CAM_VA__NEW_ADV1_CAM1        = 'Phone Call exists, but is Wrong:   old is [Adv <YYY>, Cam <ZZZ>, VA <AAA>], correct one is [Adv <XXX>, Cam <SSS>, VA <   >]';
    const CASE_TYPE_WRONG__OLD_CAM_VA__NEW_ADV1_CAM1_VA1    = 'Phone Call exists, but is Wrong:   old is [Adv <YYY>, Cam <ZZZ>, VA <AAA>], correct one is [Adv <XXX>, Cam <SSS>, VA <OOO>]';
    const CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM_VA_TIME      = 'Phone Call exists, but is Wrong:   old is [time of VA >= time of start od fthe call]';
    const CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM_VA_TIPN      = 'Phone Call exists, but is Wrong:   old is [VA is for another phone number]';



    const PARAM_PHONE_CALL_EXISTS       = 'exists';
    const PARAM_NEED_VA_ID              = 'va_id';
    const PARAM_NEED_ADVERTISER_ID      = 'advertiser_id';
    const PARAM_NEED_CAMPAIGN_ID        = 'campaign_id';
    const PARAM_CASE_TYPE               = 'case type';
    const PARAM_DESCRIPTION             = 'description';
    const PARAM_NOTES                   = 'notes';


    const BELONGING_TYPE_BIONIC     = 'Bionic';
    const BELONGING_TYPE_ADVERTISER = 'Advertiser';
    const BELONGING_TYPE_CAMPAIGN   = 'Campaign';

    const NOTES_UNREAL_VA = 'It is impossible to have such VA in DB in relity, so method does not check it now';

    const ADVERTISER_ID_BIONIC      = 1;
    const ADVERTISER_ID_TRIPS       = 3;
    const ADVERTISER_ID_IPS         = 4;
    const ADVERTISER_ID_SNOW        = 5;

    const CAMPAIGN_ID_4ALLPROMOS        = 1;
    const CAMPAIGN_ID_TRIPS             = 3;
    const CAMPAIGN_ID_IPS               = 4;
    const CAMPAIGN_ID_SNOWING           = 5;
    const CAMPAIGN_ID_OTHER_4ALLPROMOS  = 2;
    const CAMPAIGN_ID_OTHER_TRIPS       = 7;
    const CAMPAIGN_ID_OTHER_IPS         = 8;
    const CAMPAIGN_ID_OTHER_SNOWING     = 6;


    protected static $EXPECTED = array(
//        self::TIPN_ID_* => array(
//            self::CALL_TIMESTAMP_* =>  array(
//                self::PARAM_NEED_VA_ID              => null or id,
//                self::PARAM_CASE_TYPE               => self::CASE_TYPE_*,
//                self::PARAM_DESCRIPTION             => '',
//                self::PARAM_NOTES                   => self::NOTES_*,
//            ),
//        ),

        self::TIPN_ID_01 => array(
            self::CALL_TIMESTAMP_003 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_CRRCT__OLD_EMPTY__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_004 => array(
                self::PARAM_NEED_VA_ID              => 7,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_EMPTY__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_005 => array(
                self::PARAM_NEED_VA_ID              => 13,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_006 => array(
                self::PARAM_NEED_VA_ID              => 13,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_ADV1_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_007 => array(
                self::PARAM_NEED_VA_ID              => 19,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_008 => array(
                self::PARAM_NEED_VA_ID              => 19,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_009 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_010 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_013 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_015 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_016 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_EMPTY__NEW_ADV,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_017 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_ADV1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_018 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_CRRCT__OLD_ADV__NEW_ADV,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_019 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_ADV,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_020 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_ADV,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_021 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_022 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_100 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_101 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_ADV1_CAM1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_102 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_CAM,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_103 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_EMPTY__NEW_CAM,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_104 => array(
                self::PARAM_NEED_VA_ID              => 107,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_ADV1_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_105 => array(
                self::PARAM_NEED_VA_ID              => 107,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_CRRCT__OLD_CAM_VA__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_106 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_107 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_109 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_113 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_114 => array(
                self::PARAM_NEED_VA_ID              => 119,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_115 => array(
                self::PARAM_NEED_VA_ID              => 125,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_ADV1_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
            ),
        ),
        self::TIPN_ID_02 => array(
            self::CALL_TIMESTAMP_003 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_004 => array(
                self::PARAM_NEED_VA_ID              => 8,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_ADV1_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_005 => array(
                self::PARAM_NEED_VA_ID              => 14,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_EMPTY__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_006 => array(
                self::PARAM_NEED_VA_ID              => 14,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_007 => array(
                self::PARAM_NEED_VA_ID              => 20,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_ADV1_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_008 => array(
                self::PARAM_NEED_VA_ID              => 20,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_009 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_010 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_021 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_022 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_CRRCT__OLD_EMPTY__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_029 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_030 => array(
                self::PARAM_NEED_VA_ID              => 52,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_032 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_034 => array(
                self::PARAM_NEED_VA_ID              => 56,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_CRRCT__OLD_CAM_VA__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_035 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_036 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_100 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_101 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_102 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_ADV1_CAM1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_103 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_CAM,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_105 => array(
                self::PARAM_NEED_VA_ID              => 108,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_ADV1_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_106 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_107 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_109 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_113 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_114 => array(
                self::PARAM_NEED_VA_ID              => 120,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_115 => array(
                self::PARAM_NEED_VA_ID              => 126,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
            ), 
        ),
        self::TIPN_ID_03 => array(
            self::CALL_TIMESTAMP_003 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_004 => array(
                self::PARAM_NEED_VA_ID              => 9,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_006 => array(
                self::PARAM_NEED_VA_ID              => 15,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_CRRCT__OLD_CAM_VA__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_008 => array(
                self::PARAM_NEED_VA_ID              => 21,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_009 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_010 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_021 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_022 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_043 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_044 => array(
                self::PARAM_NEED_VA_ID              => 63,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_TRIPS,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_TRIPS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_045 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_TRIPS,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_TRIPS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_046 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_TRIPS,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_TRIPS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_ADV1_CAM1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_047 => array(
                self::PARAM_NEED_VA_ID              => 67,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_TRIPS,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_TRIPS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_EMPTY__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
            ), 
            self::CALL_TIMESTAMP_048 => array(
                self::PARAM_NEED_VA_ID              => 67,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_TRIPS,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_TRIPS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
            ), 
            self::CALL_TIMESTAMP_049 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_TRIPS,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_EMPTY__NEW_ADV,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_052 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_TRIPS,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_CRRCT__OLD_ADV__NEW_ADV,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_051 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_TRIPS,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_ADV,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_052 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_TRIPS,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_ADV1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_053 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_TRIPS,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_ADV,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_054 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_TRIPS,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_ADV1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_055 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_CRRCT__OLD_EMPTY__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_056 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_100 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_101 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_102 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_CRRCT__OLD_CAM__NEW_CAM,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_103 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_ADV1_CAM1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_104 => array(
                self::PARAM_NEED_VA_ID              => 109,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
            ), 
            self::CALL_TIMESTAMP_105 => array(
                self::PARAM_NEED_VA_ID              => 109,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_ADV1_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
            ), 
            self::CALL_TIMESTAMP_106 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_107 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_109 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_113 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_114 => array(
                self::PARAM_NEED_VA_ID              => 121,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_TRIPS,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_TRIPS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_ADV1_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_115 => array(
                self::PARAM_NEED_VA_ID              => 127,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_TRIPS,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_TRIPS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_ADV1_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
            ), 
        ),
        self::TIPN_ID_04 => array(
            self::CALL_TIMESTAMP_003 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_004 => array(
                self::PARAM_NEED_VA_ID              => 10,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_006 => array(
                self::PARAM_NEED_VA_ID              => 16,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_ADV1_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
            ), 
            self::CALL_TIMESTAMP_008 => array(
                self::PARAM_NEED_VA_ID              => 22,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
            ), 
            self::CALL_TIMESTAMP_009 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_010 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_021 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_022 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_062 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_CRRCT__OLD_EMPTY__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_063 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_IPS,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_EMPTY__NEW_ADV,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_065 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_IPS,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_CRRCT__OLD_ADV__NEW_ADV,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_066 => array(
                self::PARAM_NEED_VA_ID              => 76,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_IPS,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_IPS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_CRRCT__OLD_CAM_VA__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_067 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_IPS,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_IPS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_EMPTY__NEW_CAM,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_068 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_IPS,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_IPS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_CAM,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_069 => array(
                self::PARAM_NEED_VA_ID              => 80,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_IPS,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_IPS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_ADV1_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
            ), 
            self::CALL_TIMESTAMP_070 => array(
                self::PARAM_NEED_VA_ID              => 80,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_IPS,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_IPS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_ADV1_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
            ), 
            self::CALL_TIMESTAMP_071 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_IPS,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_ADV1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_072 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_IPS,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_ADV,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_074 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_IPS,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_075 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_IPS,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_ADV,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_076 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_IPS,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_ADV1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_077 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_078 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_100 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_101 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_102 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_ADV1_CAM1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_103 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_CRRCT__OLD_CAM__NEW_CAM,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_105 => array(
                self::PARAM_NEED_VA_ID              => 110,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
            ), 
            self::CALL_TIMESTAMP_106 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_107 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_109 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_113 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_114 => array(
                self::PARAM_NEED_VA_ID              => 122,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_IPS,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_IPS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_115 => array(
                self::PARAM_NEED_VA_ID              => 128,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_IPS,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_IPS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_EMPTY__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
            ), 
        ),
        self::TIPN_ID_05 => array(
            self::CALL_TIMESTAMP_003 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_004 => array(
                self::PARAM_NEED_VA_ID              => 11,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_ADV1_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_006 => array(
                self::PARAM_NEED_VA_ID              => 17,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
            ), 
            self::CALL_TIMESTAMP_008 => array(
                self::PARAM_NEED_VA_ID              => 23,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_ADV1_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
            ), 
            self::CALL_TIMESTAMP_009 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_010 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_021 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_022 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_085 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_CRRCT__OLD_EMPTY__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_086 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_ADV1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_087 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_ADV1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_088 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_ADV1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_089 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_CRRCT__OLD_ADV__NEW_ADV,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_090 => array(
                self::PARAM_NEED_VA_ID              => 91,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_091 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_EMPTY__NEW_CAM,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_092 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_ADV1_CAM1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_093 => array(
                self::PARAM_NEED_VA_ID              => 95,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
            ), 
            self::CALL_TIMESTAMP_094 => array(
                self::PARAM_NEED_VA_ID              => 95,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_ADV1_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
            ), 
            self::CALL_TIMESTAMP_095 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_096 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_100 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_101 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_102 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_CRRCT__OLD_CAM__NEW_CAM,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_103 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_ADV1_CAM1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_105 => array(
                self::PARAM_NEED_VA_ID              => 111,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
            ), 
            self::CALL_TIMESTAMP_106 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_107 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_109 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_113 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_114 => array(
                self::PARAM_NEED_VA_ID              => 123,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_CRRCT__OLD_CAM_VA__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_115 => array(
                self::PARAM_NEED_VA_ID              => 129,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_EMPTY__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
            ), 
        ),
        self::TIPN_ID_06 => array(
            self::CALL_TIMESTAMP_003 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_004 => array(
                self::PARAM_NEED_VA_ID              => 12,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM_VA_TIME,
                self::PARAM_DESCRIPTION             => '',
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_006 => array(
                self::PARAM_NEED_VA_ID              => 18,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM_VA_TIPN,
                self::PARAM_DESCRIPTION             => '',
            ), 
            self::CALL_TIMESTAMP_008 => array(
                self::PARAM_NEED_VA_ID              => 24,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM_VA_TIME,
                self::PARAM_DESCRIPTION             => '',
            ), 
            self::CALL_TIMESTAMP_009 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_010 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_013 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_015 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_016 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_018 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_ADV1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_019 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_020 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_ADV,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_021 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_022 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_026 => array(
                self::PARAM_NEED_VA_ID              => 51,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM_VA_TIPN,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_029 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_030 => array(
                self::PARAM_NEED_VA_ID              => 53,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_031 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_EMPTY__NEW_CAM,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_032 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_CAM,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_034 => array(
                self::PARAM_NEED_VA_ID              => 57,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM_VA_TIME,
                self::PARAM_DESCRIPTION             => '',
            ), 
            self::CALL_TIMESTAMP_035 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_036 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_040 => array(
                self::PARAM_NEED_VA_ID              => 62,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM_VA_TIPN,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_043 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_044 => array(
                self::PARAM_NEED_VA_ID              => 64,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_EMPTY__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_046 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_ADV1_CAM1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_048 => array(
                self::PARAM_NEED_VA_ID              => 68,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_ADV1_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_049 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_052 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_ADV1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_054 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_055 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_056 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_059 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_062 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_063 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_EMPTY__NEW_ADV,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_065 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_066 => array(
                self::PARAM_NEED_VA_ID              => 77,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_ADV1_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_067 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_CAM1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_068 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_CRRCT__OLD_CAM__NEW_CAM,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_070 => array(
                self::PARAM_NEED_VA_ID              => 81,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM_VA_TIME,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_071 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_074 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_076 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_ADV1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_077 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_ADV__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_078 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_082 => array(
                self::PARAM_NEED_VA_ID              => 88,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_CRRCT__OLD_CAM_VA__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_085 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_086 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_ADV,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_088 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_089 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_CRRCT__OLD_ADV__NEW_ADV,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_090 => array(
                self::PARAM_NEED_VA_ID              => 92,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM_VA_TIPN,
                self::PARAM_DESCRIPTION             => '',
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_091 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_ADV1_CAM1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_092 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_ADV1_CAM1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_094 => array(
                self::PARAM_NEED_VA_ID              => 96,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_ADV1_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_095 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_096 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_CRRCT__OLD_EMPTY__NEW_EMPTY,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_100 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_101 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_102 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_103 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_105 => array(
                self::PARAM_NEED_VA_ID              => 112,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_SNOW,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_106 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_107 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),

            self::CALL_TIMESTAMP_109 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_113 => array(
                self::PARAM_NEED_VA_ID              => null,
                self::PARAM_NEED_ADVERTISER_ID      => null,
                self::PARAM_NEED_CAMPAIGN_ID        => null,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE,
                self::PARAM_DESCRIPTION             => '',
            ),
            self::CALL_TIMESTAMP_114 => array(
                self::PARAM_NEED_VA_ID              => 124,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM_VA__NEW_CAM1_VA1,
                self::PARAM_DESCRIPTION             => '',
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_115 => array(
                self::PARAM_NEED_VA_ID              => 130,
                self::PARAM_NEED_ADVERTISER_ID      => self::ADVERTISER_ID_BIONIC,
                self::PARAM_NEED_CAMPAIGN_ID        => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_CASE_TYPE               => self::CASE_TYPE_WRONG__OLD_CAM__NEW_CAM_VA,
                self::PARAM_DESCRIPTION             => '',
            ),
        ),
    );


    /**
     * @dataProvider providerCases
     * 
     */
    public function testCase(
        $tipnId, 
        $timestamp, 
        $expectedVaId, 
        $expectedAdvId, 
        $expectedCamId, 
        $caseType, 
        $description, 
        $notes
    ) {

        $notes = empty($notes) ? '' : "[Note, that $notes]. ";
        $description = empty($description) ? '' : "$description. ";

        $specificAdditionalMessage = "$caseType. {$description}{$notes}";

        $tic = TwilioIncomingCallTable::findOneByTipnAndTime($tipnId, $timestamp);
        if (empty($tic) || !$tic->exists()) {
            throw new Exception("$specificAdditionalMessage\n    Cannot find TwilioIncomingCall for TwilioiIncomingPhoneNumber<$tipnId> timestamp<$timestamp>.");
        }

        $id = $tic->getId();
        $description = "$specificAdditionalMessage\n   PhoneCall is for TwilioiIncomingCall<$id> TwilioiIncomingPhoneNumber<$tipnId> timestamp<$timestamp>.\n   ";

        $oldPhoneCall = PhoneCallTable::getInstance()->find($id);
        if ($caseType ==  self::CASE_TYPE_WRONG__OLD_DOES_NOT_EXIST__NEW_SHOULD_BE) {
            $this->assertTrue(
                empty($oldPhoneCall) || !$oldPhoneCall->exists(),
                "$description Old PhoneCall exists."
            );
        } else {
            $this->assertTrue(
                !empty($oldPhoneCall) && $oldPhoneCall->exists(),
                "$description Old PhoneCall does not exist."
            );
        }

        $info = PhoneCallTable::createOrUpdateByActualTwilioIncomingCall($tic);

        $newPhoneCall = PhoneCallTable::getInstance()->find($id);
        $this->assertTrue(
            !empty($newPhoneCall) && $newPhoneCall->exists(),
            "$description PhoneCall has not been created."
        );        

        $vaId = $newPhoneCall->getVisitorAnalyticsId();

        $this->assertEquals(
            $expectedVaId,
            $vaId,
            "$description Result Related VisitorAnalytics is wrong."
        );

        $advId = $newPhoneCall->getAdvertiserId();
        $this->assertEquals(
            $expectedAdvId,
            $advId,
            "$description Result Related Advertiser is wrong."
        );

        $camId = $newPhoneCall->getCampaignId();
        $this->assertEquals(
            $expectedCamId,
            $camId,
            "$description Result Related Campaign is wrong."
        );
    }

    public function providerCases() {

        $cases = array();
        foreach (self::$EXPECTED as $tipnId => $expectedForTipn) {
            foreach ($expectedForTipn as $timestamp => $expected) {
                $vaId               = array_key_exists(self::PARAM_NEED_VA_ID,              $expected) ? $expected[self::PARAM_NEED_VA_ID] :            null;
                $advId              = array_key_exists(self::PARAM_NEED_ADVERTISER_ID,      $expected) ? $expected[self::PARAM_NEED_ADVERTISER_ID] :    null;
                $camId              = array_key_exists(self::PARAM_NEED_CAMPAIGN_ID,        $expected) ? $expected[self::PARAM_NEED_CAMPAIGN_ID] :      null;
                $caseType           = array_key_exists(self::PARAM_CASE_TYPE,               $expected) ? $expected[self::PARAM_CASE_TYPE] :             '';
                $description        = array_key_exists(self::PARAM_DESCRIPTION,             $expected) ? $expected[self::PARAM_DESCRIPTION] :           '';
                $notes              = array_key_exists(self::PARAM_NOTES,                   $expected) ? $expected[self::PARAM_NOTES] :                 '';
                $name = "case tipn<$tipnId> timestamp<$timestamp>";
                $cases[$name] = array(
                    $tipnId, 
                    $timestamp, 
                    $vaId, 
                    $advId,
                    $camId,
                    $caseType, 
                    $description, 
                    $notes
                );
            }
        }
        return $cases;
    }
}
