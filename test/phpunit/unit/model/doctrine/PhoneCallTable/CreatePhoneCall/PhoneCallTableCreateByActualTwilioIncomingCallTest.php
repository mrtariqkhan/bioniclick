<?php

/**
 * Description of PhoneCallTableCreateByActualTwilioIncomingCallTest
 * 
 * @author fairdev
 */
class PhoneCallTableCreateByActualTwilioIncomingCallTest extends sfBasePhpunitTestCase
    implements sfPhpunitFixtureDoctrineAggregator {

    const CALL_TIMESTAMP_003 = 1323000003;
    const CALL_TIMESTAMP_004 = 1323000004;
    const CALL_TIMESTAMP_006 = 1323000006;
    const CALL_TIMESTAMP_008 = 1323000008;
    const CALL_TIMESTAMP_009 = 1323000009;
    const CALL_TIMESTAMP_010 = 1323000010;
    const CALL_TIMESTAMP_013 = 1323000013;
    const CALL_TIMESTAMP_015 = 1323000015;
    const CALL_TIMESTAMP_016 = 1323000016;
    const CALL_TIMESTAMP_018 = 1323000018;
    const CALL_TIMESTAMP_020 = 1323000020;
    const CALL_TIMESTAMP_021 = 1323000021;
    const CALL_TIMESTAMP_022 = 1323000022;
    const CALL_TIMESTAMP_026 = 1323000026;
    const CALL_TIMESTAMP_029 = 1323000029;
    const CALL_TIMESTAMP_030 = 1323000030;
    const CALL_TIMESTAMP_032 = 1323000032;
    const CALL_TIMESTAMP_034 = 1323000034;
    const CALL_TIMESTAMP_035 = 1323000035;
    const CALL_TIMESTAMP_036 = 1323000036;
    const CALL_TIMESTAMP_040 = 1323000040;
    const CALL_TIMESTAMP_043 = 1323000043;
    const CALL_TIMESTAMP_044 = 1323000044;
    const CALL_TIMESTAMP_046 = 1323000046;
    const CALL_TIMESTAMP_048 = 1323000048;
    const CALL_TIMESTAMP_049 = 1323000049;
    const CALL_TIMESTAMP_052 = 1323000052;
    const CALL_TIMESTAMP_054 = 1323000054;
    const CALL_TIMESTAMP_055 = 1323000055;
    const CALL_TIMESTAMP_056 = 1323000056;
    const CALL_TIMESTAMP_059 = 1323000059;
    const CALL_TIMESTAMP_062 = 1323000062;
    const CALL_TIMESTAMP_063 = 1323000063;
    const CALL_TIMESTAMP_065 = 1323000065;
    const CALL_TIMESTAMP_066 = 1323000066;
    const CALL_TIMESTAMP_068 = 1323000068;
    const CALL_TIMESTAMP_070 = 1323000070;
    const CALL_TIMESTAMP_071 = 1323000071;
    const CALL_TIMESTAMP_074 = 1323000074;
    const CALL_TIMESTAMP_076 = 1323000076;
    const CALL_TIMESTAMP_077 = 1323000077;
    const CALL_TIMESTAMP_078 = 1323000078;
    const CALL_TIMESTAMP_082 = 1323000082;
    const CALL_TIMESTAMP_085 = 1323000085;
    const CALL_TIMESTAMP_086 = 1323000086;
    const CALL_TIMESTAMP_088 = 1323000088;
    const CALL_TIMESTAMP_089 = 1323000089;
    const CALL_TIMESTAMP_090 = 1323000090;
    const CALL_TIMESTAMP_092 = 1323000092;
    const CALL_TIMESTAMP_094 = 1323000094;
    const CALL_TIMESTAMP_095 = 1323000095;
    const CALL_TIMESTAMP_096 = 1323000096;
    const CALL_TIMESTAMP_100 = 1323000100;
    const CALL_TIMESTAMP_101 = 1323000101;
    const CALL_TIMESTAMP_103 = 1323000103;
    const CALL_TIMESTAMP_105 = 1323000105;
    const CALL_TIMESTAMP_106 = 1323000106;
    const CALL_TIMESTAMP_107 = 1323000107;
    const CALL_TIMESTAMP_109 = 1323000109;
    const CALL_TIMESTAMP_113 = 1323000113;
    const CALL_TIMESTAMP_114 = 1323000114;
    const CALL_TIMESTAMP_115 = 1323000115;
//    const CALL_TIMESTAMP_XXX = 1323000XXX;


    const VA_TIMESTAMP_002 = 1323000002;
    const VA_TIMESTAMP_003 = 1323000003;
    const VA_TIMESTAMP_004 = 1323000004;
    const VA_TIMESTAMP_006 = 1323000006;
    const VA_TIMESTAMP_008 = 1323000008;
    const VA_TIMESTAMP_009 = 1323000009;
    const VA_TIMESTAMP_013 = 1323000013;
    const VA_TIMESTAMP_015 = 1323000015;
    const VA_TIMESTAMP_018 = 1323000018;
    const VA_TIMESTAMP_021 = 1323000021;
    const VA_TIMESTAMP_022 = 1323000022;
    const VA_TIMESTAMP_025 = 1323000025;
    const VA_TIMESTAMP_028 = 1323000028;
    const VA_TIMESTAMP_030 = 1323000030;
    const VA_TIMESTAMP_032 = 1323000032;
    const VA_TIMESTAMP_034 = 1323000034;
    const VA_TIMESTAMP_035 = 1323000035;
    const VA_TIMESTAMP_039 = 1323000039;
    const VA_TIMESTAMP_042 = 1323000042;
    const VA_TIMESTAMP_044 = 1323000044;
    const VA_TIMESTAMP_046 = 1323000046;
    const VA_TIMESTAMP_048 = 1323000048;
    const VA_TIMESTAMP_051 = 1323000051;
    const VA_TIMESTAMP_057 = 1323000057;
    const VA_TIMESTAMP_059 = 1323000059;
    const VA_TIMESTAMP_064 = 1323000064;
    const VA_TIMESTAMP_066 = 1323000066;
    const VA_TIMESTAMP_068 = 1323000068;
    const VA_TIMESTAMP_070 = 1323000070;
    const VA_TIMESTAMP_075 = 1323000075;
    const VA_TIMESTAMP_079 = 1323000079;
    const VA_TIMESTAMP_081 = 1323000081;
    const VA_TIMESTAMP_084 = 1323000084;
    const VA_TIMESTAMP_087 = 1323000087;
    const VA_TIMESTAMP_090 = 1323000090;
    const VA_TIMESTAMP_092 = 1323000092;
    const VA_TIMESTAMP_094 = 1323000094;
    const VA_TIMESTAMP_098 = 1323000098;
    const VA_TIMESTAMP_101 = 1323000101;
    const VA_TIMESTAMP_103 = 1323000103;
    const VA_TIMESTAMP_105 = 1323000105;
    const VA_TIMESTAMP_108 = 1323000108;
    const VA_TIMESTAMP_114 = 1323000114;
//    const VA_TIMESTAMP_XXX = 1323000XXX;



    const TIPN_ID_01 = 1;
    const TIPN_ID_02 = 2;
    const TIPN_ID_03 = 5;
    const TIPN_ID_04 = 6;
    const TIPN_ID_05 = 9;
    const TIPN_ID_06 = 10;

    const TYPE_VA_CORRECT                   = 'Visitor Analytics is correct';
    const TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN = 'Visitor Analytics is wrong. It Belongs to Another Campaign, but phone number belonged to Need Campaign in that time.';
    const TYPE_VA_WRONG_TO_NEED_CAMPAIGN    = 'Visitor Analytics is wrong. It Belongs to Need Campaign, but phone number belonged to Bionic in that time.';
    protected static $VA_ID_TYPE = array(
        null => 'No Visitor Analytics',

        1 => self::TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN,
        2 => self::TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN,
        3 => self::TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN,
        4 => self::TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN,
        5 => self::TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN,
        6 => self::TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN,
        7 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        8 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        9 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        10 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        11 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        12 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        13 => self::TYPE_VA_CORRECT,
        14 => self::TYPE_VA_CORRECT,
        15 => self::TYPE_VA_CORRECT,
        16 => self::TYPE_VA_CORRECT,
        17 => self::TYPE_VA_CORRECT,
        18 => self::TYPE_VA_CORRECT,
        19 => self::TYPE_VA_CORRECT,
        20 => self::TYPE_VA_CORRECT,
        21 => self::TYPE_VA_CORRECT,
        22 => self::TYPE_VA_CORRECT,
        23 => self::TYPE_VA_CORRECT,
        24 => self::TYPE_VA_CORRECT,
        25 => self::TYPE_VA_CORRECT,
        26 => self::TYPE_VA_CORRECT,
        27 => self::TYPE_VA_CORRECT,
        28 => self::TYPE_VA_CORRECT,
        29 => self::TYPE_VA_CORRECT,
        30 => self::TYPE_VA_CORRECT,
        31 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        32 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        33 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        34 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        35 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        36 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,

        37 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        38 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        39 => self::TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN,
        40 => self::TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN,
        41 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        42 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        43 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        44 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        45 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        46 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        47 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        48 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        49 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        50 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,

        51 => self::TYPE_VA_CORRECT,

        52 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        53 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        54 => self::TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN,
        55 => self::TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN,
        56 => self::TYPE_VA_CORRECT,
        57 => self::TYPE_VA_CORRECT,
        58 => self::TYPE_VA_CORRECT,
        59 => self::TYPE_VA_CORRECT,
        60 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        61 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,

        62 => self::TYPE_VA_CORRECT,

        63 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        64 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        65 => self::TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN,
        66 => self::TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN,
        67 => self::TYPE_VA_CORRECT,
        68 => self::TYPE_VA_CORRECT,
        69 => self::TYPE_VA_CORRECT,
        70 => self::TYPE_VA_CORRECT,
        71 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        72 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        73 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        74 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,

        75 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,

        76 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        77 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        78 => self::TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN,
        79 => self::TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN,
        80 => self::TYPE_VA_CORRECT,
        81 => self::TYPE_VA_CORRECT,
        82 => self::TYPE_VA_CORRECT,
        83 => self::TYPE_VA_CORRECT,
        84 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        85 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        86 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        87 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,

        88 => self::TYPE_VA_CORRECT,

        89 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        90 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        91 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        92 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        93 => self::TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN,
        94 => self::TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN,
        95 => self::TYPE_VA_CORRECT,
        96 => self::TYPE_VA_CORRECT,
        97 => self::TYPE_VA_CORRECT,
        98 => self::TYPE_VA_CORRECT,
        99 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        100 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,

        101 => self::TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN,
        102 => self::TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN,
        103 => self::TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN,
        104 => self::TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN,
        105 => self::TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN,
        106 => self::TYPE_VA_WRONG_TO_ANOTHER_CAMPAIGN,
        107 => self::TYPE_VA_CORRECT,
        108 => self::TYPE_VA_CORRECT,
        109 => self::TYPE_VA_CORRECT,
        110 => self::TYPE_VA_CORRECT,
        111 => self::TYPE_VA_CORRECT,
        112 => self::TYPE_VA_CORRECT,
        113 => self::TYPE_VA_CORRECT,
        114 => self::TYPE_VA_CORRECT,
        115 => self::TYPE_VA_CORRECT,
        116 => self::TYPE_VA_CORRECT,
        117 => self::TYPE_VA_CORRECT,
        118 => self::TYPE_VA_CORRECT,
        119 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        120 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        121 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        122 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        123 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,
        124 => self::TYPE_VA_WRONG_TO_NEED_CAMPAIGN,

        125 => self::TYPE_VA_CORRECT,
        126 => self::TYPE_VA_CORRECT,
        127 => self::TYPE_VA_CORRECT,
        128 => self::TYPE_VA_CORRECT,
        129 => self::TYPE_VA_CORRECT,
        130 => self::TYPE_VA_CORRECT,
    );

    const PARAM_VA_ID                   = 'va_id';
    const PARAM_ADVERTISER_ID           = 'advertiser_id';
    const PARAM_CAMPAIGN_ID             = 'campaign_id';
    const PARAM_BELONGING_WHILE_CALL    = 'belonging while call';
    const PARAM_CALL_TYPE               = 'call type';
    const PARAM_DESCRIPTION             = 'description';
    const PARAM_NOTES                   = 'notes';
    const PARAM_VA_TYPE                 = 'va type';

    const CALL_TYPE_DIRTY           = 'Dirty';
    const CALL_TYPE_CORRECT         = 'Correct';
    const CALL_TYPE_CORRECT_ALMOST  = 'Almost Correct';

    const BELONGING_TYPE_BIONIC     = 'Bionic';
    const BELONGING_TYPE_ADVERTISER = 'Advertiser';
    const BELONGING_TYPE_CAMPAIGN   = 'Campaign';

    const CALL_BELONGING_TYPE_BEFORE= 'before belonging';
    const CALL_BELONGING_TYPE_WHILE = 'while belonging';
    const CALL_BELONGING_TYPE_AFTER = 'after belonging';

    const NOTES_UNREAL_VA = 'It is impossible to have such VA in DB in relity, so method does not check it now';

    const ADVERTISER_ID_BIONIC      = 1;
    const ADVERTISER_ID_TRIPS       = 3;
    const ADVERTISER_ID_IPS         = 4;
    const ADVERTISER_ID_SNOW        = 5;

    const CAMPAIGN_ID_4ALLPROMOS    = 1;
    const CAMPAIGN_ID_TRIPS         = 3;
    const CAMPAIGN_ID_IPS           = 4;
    const CAMPAIGN_ID_SNOWING       = 5;


    protected static $EXPECTED = array(
//        self::TIPN_ID_* => array(
//            self::CALL_TIMESTAMP_* =>  array(
//                self::PARAM_VA_ID                   => null or id,
//                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_*,
//                self::PARAM_CALL_TYPE               => self::CALL_TYPE_*,
//                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_*,
//                self::PARAM_NOTES                   => self::NOTES_*,
//            ),
//        ),
        self::TIPN_ID_01 => array(
            self::CALL_TIMESTAMP_003 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_004 => array(
                self::PARAM_VA_ID                   => 7,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT_ALMOST,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_006 => array(
                self::PARAM_VA_ID                   => 13,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_008 => array(
                self::PARAM_VA_ID                   => 19,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_009 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_010 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_013 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_015 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_016 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_018 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_020 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_021 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_022 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_100 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_101 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_103 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_105 => array(
                self::PARAM_VA_ID                   => 107,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_106 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_107 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_109 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_113 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_114 => array(
                self::PARAM_VA_ID                   => 119,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT_ALMOST,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_115 => array(
                self::PARAM_VA_ID                   => 125,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
        ),
        self::TIPN_ID_02 => array(
            self::CALL_TIMESTAMP_003 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_004 => array(
                self::PARAM_VA_ID                   => 8,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT_ALMOST,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_006 => array(
                self::PARAM_VA_ID                   => 14,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_008 => array(
                self::PARAM_VA_ID                   => 20,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_009 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_010 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_022 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),

            self::CALL_TIMESTAMP_029 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_030 => array(
                self::PARAM_VA_ID                   => 52,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT_ALMOST,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_032 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_034 => array(
                self::PARAM_VA_ID                   => 56,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_035 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_036 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_100 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_101 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_103 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_105 => array(
                self::PARAM_VA_ID                   => 108,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_106 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_107 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_109 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_113 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_114 => array(
                self::PARAM_VA_ID                   => 120,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT_ALMOST,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_115 => array(
                self::PARAM_VA_ID                   => 126,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ), 
        ),
        self::TIPN_ID_03 => array(
            self::CALL_TIMESTAMP_003 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_004 => array(
                self::PARAM_VA_ID                   => 9,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT_ALMOST,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_006 => array(
                self::PARAM_VA_ID                   => 15,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_008 => array(
                self::PARAM_VA_ID                   => 21,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_009 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_010 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_022 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),

            self::CALL_TIMESTAMP_043 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_044 => array(
                self::PARAM_VA_ID                   => 63,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_TRIPS,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_TRIPS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT_ALMOST,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_046 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_TRIPS,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_TRIPS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_048 => array(
                self::PARAM_VA_ID                   => 67,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_TRIPS,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_TRIPS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ), 
            self::CALL_TIMESTAMP_049 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_TRIPS,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_052 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_TRIPS,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_054 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_TRIPS,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_055 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_056 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_100 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_101 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_103 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_105 => array(
                self::PARAM_VA_ID                   => 109,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ), 
            self::CALL_TIMESTAMP_106 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_107 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_109 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_113 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_114 => array(
                self::PARAM_VA_ID                   => 121,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_TRIPS,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_TRIPS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT_ALMOST,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_115 => array(
                self::PARAM_VA_ID                   => 127,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_TRIPS,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_TRIPS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ), 
        ),
        self::TIPN_ID_04 => array(
            self::CALL_TIMESTAMP_003 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_004 => array(
                self::PARAM_VA_ID                   => 10,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT_ALMOST,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_006 => array(
                self::PARAM_VA_ID                   => 16,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ), 
            self::CALL_TIMESTAMP_008 => array(
                self::PARAM_VA_ID                   => 22,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ), 
            self::CALL_TIMESTAMP_009 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_010 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_022 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),

            self::CALL_TIMESTAMP_062 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_063 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_IPS,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_065 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_IPS,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_066 => array(
                self::PARAM_VA_ID                   => 76,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_IPS,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_IPS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT_ALMOST,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_068 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_IPS,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_IPS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_070 => array(
                self::PARAM_VA_ID                   => 80,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_IPS,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_IPS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ), 
            self::CALL_TIMESTAMP_071 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_IPS,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_074 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_IPS,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_076 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_IPS,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_077 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_078 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_100 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_101 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_103 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_105 => array(
                self::PARAM_VA_ID                   => 110,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ), 
            self::CALL_TIMESTAMP_106 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_107 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_109 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_113 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_114 => array(
                self::PARAM_VA_ID                   => 122,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_IPS,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_IPS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT_ALMOST,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_115 => array(
                self::PARAM_VA_ID                   => 128,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_IPS,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_IPS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ), 
        ),
        self::TIPN_ID_05 => array(
            self::CALL_TIMESTAMP_003 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_004 => array(
                self::PARAM_VA_ID                   => 11,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT_ALMOST,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_006 => array(
                self::PARAM_VA_ID                   => 17,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ), 
            self::CALL_TIMESTAMP_008 => array(
                self::PARAM_VA_ID                   => 23,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ), 
            self::CALL_TIMESTAMP_009 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_010 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_022 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),

            self::CALL_TIMESTAMP_085 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_086 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_088 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_089 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_090 => array(
                self::PARAM_VA_ID                   => 91,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT_ALMOST,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_092 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_094 => array(
                self::PARAM_VA_ID                   => 95,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ), 
            self::CALL_TIMESTAMP_095 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_096 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_100 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_101 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_103 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_105 => array(
                self::PARAM_VA_ID                   => 111,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ), 
            self::CALL_TIMESTAMP_106 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_107 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_109 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_113 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_114 => array(
                self::PARAM_VA_ID                   => 123,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT_ALMOST,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_115 => array(
                self::PARAM_VA_ID                   => 129,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ), 
        ),
        self::TIPN_ID_06 => array(
            self::CALL_TIMESTAMP_003 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_004 => array(
                self::PARAM_VA_ID                   => 12,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT_ALMOST,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_006 => array(
                self::PARAM_VA_ID                   => 18,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ), 
            self::CALL_TIMESTAMP_008 => array(
                self::PARAM_VA_ID                   => 24,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ), 
            self::CALL_TIMESTAMP_009 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_010 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_013 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_015 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_016 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_018 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_020 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_021 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_022 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_026 => array(
                self::PARAM_VA_ID                   => 51,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),

            self::CALL_TIMESTAMP_029 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_030 => array(
                self::PARAM_VA_ID                   => 53,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT_ALMOST,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),     
            self::CALL_TIMESTAMP_032 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_034 => array(
                self::PARAM_VA_ID                   => 57,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ), 
            self::CALL_TIMESTAMP_035 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_036 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_040 => array(
                self::PARAM_VA_ID                   => 62,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),

            self::CALL_TIMESTAMP_043 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_044 => array(
                self::PARAM_VA_ID                   => 64,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT_ALMOST,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_046 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_048 => array(
                self::PARAM_VA_ID                   => 68,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_049 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_052 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_054 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_055 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_056 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_059 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_062 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_063 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_065 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_066 => array(
                self::PARAM_VA_ID                   => 77,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT_ALMOST,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_068 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_070 => array(
                self::PARAM_VA_ID                   => 81,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_071 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_074 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_076 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_077 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_078 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_082 => array(
                self::PARAM_VA_ID                   => 88,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),

            self::CALL_TIMESTAMP_085 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_086 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_088 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_089 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_ADVERTISER,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_090 => array(
                self::PARAM_VA_ID                   => 92,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT_ALMOST,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_092 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_094 => array(
                self::PARAM_VA_ID                   => 96,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_095 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_096 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_100 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_101 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_103 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_105 => array(
                self::PARAM_VA_ID                   => 112,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_SNOW,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_SNOWING,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
            self::CALL_TIMESTAMP_106 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),
            self::CALL_TIMESTAMP_107 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_AFTER,
            ),

            self::CALL_TIMESTAMP_109 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_113 => array(
                self::PARAM_VA_ID                   => null,
                self::PARAM_ADVERTISER_ID           => null,
                self::PARAM_CAMPAIGN_ID             => null,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_BIONIC,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_DIRTY,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_BEFORE,
            ),
            self::CALL_TIMESTAMP_114 => array(
                self::PARAM_VA_ID                   => 124,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT_ALMOST,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
                self::PARAM_NOTES                   => self::NOTES_UNREAL_VA,
            ),
            self::CALL_TIMESTAMP_115 => array(
                self::PARAM_VA_ID                   => 130,
                self::PARAM_ADVERTISER_ID           => self::ADVERTISER_ID_BIONIC,
                self::PARAM_CAMPAIGN_ID             => self::CAMPAIGN_ID_4ALLPROMOS,
                self::PARAM_BELONGING_WHILE_CALL    => self::BELONGING_TYPE_CAMPAIGN,
                self::PARAM_CALL_TYPE               => self::CALL_TYPE_CORRECT,
                self::PARAM_DESCRIPTION             => self::CALL_BELONGING_TYPE_WHILE,
            ),
        ),
    );


    /**
     * @dataProvider providerCases
     * 
     */
    public function testCase(
        $tipnId, 
        $timestamp, 
        $expectedVaId, 
        $expectedAdvId, 
        $expectedCamId, 
        $ownerWhileCall, 
        $callType, 
        $belongingWhileCall, 
        $notes
    ) {

        $notes = empty($notes) ? '' : "[Note, that $notes].";
        $specificAdditionalMessage = "Call is $callType. Call was $belongingWhileCall. Phone number belonged to $ownerWhileCall while call. $notes";

        $tic = TwilioIncomingCallTable::findOneByTipnAndTime($tipnId, $timestamp);
        if (empty($tic) || !$tic->exists()) {
            throw new Exception("$specificAdditionalMessage\n    Cannot find TwilioIncomingCall for TwilioiIncomingPhoneNumber<$tipnId> timestamp<$timestamp>.");
        }

        $id = $tic->getId();
        $description = "$specificAdditionalMessage\n   PhoneCall for TwilioiIncomingCall<$id> TwilioiIncomingPhoneNumber<$tipnId> timestamp<$timestamp>.\n   ";

        $oldPhoneCall = PhoneCallTable::getInstance()->find($id);

        $this->assertTrue(
            empty($oldPhoneCall) || !$oldPhoneCall->exists(),
            "$description PhoneCall exists already."
        );

        $info = PhoneCallTable::createOrUpdateByActualTwilioIncomingCall($tic);

        $newPhoneCall = PhoneCallTable::getInstance()->find($id);
        $this->assertTrue(
            !empty($newPhoneCall) && $newPhoneCall->exists(),
            "$description PhoneCall has not been created."
        );
        

        $vaId = $newPhoneCall->getVisitorAnalyticsId();
        $type = array_key_exists($vaId, self::$VA_ID_TYPE) ? self::$VA_ID_TYPE[$vaId] : '';

        $this->assertEquals(
            $expectedVaId,
            $vaId,
            "$description Related VisitorAnalytics is wrong(Type of Visitor analytics <$vaId> is : '$type')."
        );

        $advId = $newPhoneCall->getAdvertiserId();
        $this->assertEquals(
            $expectedAdvId,
            $advId,
            "$description Related Advertiser is wrong."
        );

        $camId = $newPhoneCall->getCampaignId();
        $this->assertEquals(
            $expectedCamId,
            $camId,
            "$description Related Campaign is wrong."
        );
    }

    public function providerCases() {

        $cases = array();
        foreach (self::$EXPECTED as $tipnId => $expectedForTipn) {
            foreach ($expectedForTipn as $timestamp => $expected) {
                $vaId               = array_key_exists(self::PARAM_VA_ID,                   $expected) ? $expected[self::PARAM_VA_ID] :                 null;
                $advId              = array_key_exists(self::PARAM_ADVERTISER_ID,           $expected) ? $expected[self::PARAM_ADVERTISER_ID] :         null;
                $camId              = array_key_exists(self::PARAM_CAMPAIGN_ID,             $expected) ? $expected[self::PARAM_CAMPAIGN_ID] :           null;
                $ownerWhileCall     = array_key_exists(self::PARAM_BELONGING_WHILE_CALL,    $expected) ? $expected[self::PARAM_BELONGING_WHILE_CALL] :  '';
                $callType           = array_key_exists(self::PARAM_CALL_TYPE,               $expected) ? $expected[self::PARAM_CALL_TYPE] :             '';
                $belongingWhileCall = array_key_exists(self::PARAM_DESCRIPTION,             $expected) ? $expected[self::PARAM_DESCRIPTION] :           '';
                $notes              = array_key_exists(self::PARAM_NOTES,                   $expected) ? $expected[self::PARAM_NOTES] :                 '';
                $name = "case tipn<$tipnId> timestamp<$timestamp>";
                $cases[$name] = array(
                    $tipnId, 
                    $timestamp, 
                    $vaId, 
                    $advId,
                    $camId,
                    $ownerWhileCall, 
                    $callType, 
                    $belongingWhileCall, 
                    $notes
                );
            }
        }
        return $cases;
    }
}
