<?php

/**
 * Description of PackageTestBase
 * 
 * @author fairdev
 */
abstract class PackageTestBase extends sfBasePhpunitTestCase
    implements sfPhpunitFixtureDoctrineAggregator {

    const COMPANY_ID_BIONIC         = 1;
    const COMPANY_ID_RESELLER       = 2;
    const COMPANY_ID_AGENCY         = 6;

    const ADVERTISER_ID_BIONIC      = 1;
    const ADVERTISER_ID_RESELLER    = 3;
    const ADVERTISER_ID_AGENCY      = 4;

    const COMPANY_ID_BIONIC_WITH_DELETED_SUBTREE    = 1;
    const COMPANY_ID_RESELLER_WITH_DELETED_SUBTREE  = 3;
    const COMPANY_ID_AGENCY_WITH_DELETED_SUBTREE    = 8;

    const COMPANY_ID_RESELLER_DELETED  = 5;
    const COMPANY_ID_AGENCY_DELETED    = 7;


    const PACKAGE_ID_FOR_RESELLER               = 1;
    const PACKAGE_ID_FOR_AGENCY                 = 4;
    const PACKAGE_ID_FOR_ADVERTISER_BIONIC      = 1;
    const PACKAGE_ID_FOR_ADVERTISER_RESELLER    = 4;
    const PACKAGE_ID_FOR_ADVERTISER_AGENCY      = 10;

    const PACKAGE_ID_FROM_BIONIC_NOT_IN_USE     = 2;
    const PACKAGE_ID_FROM_RESELLER_NOT_IN_USE   = 9;
    const PACKAGE_ID_FROM_AGENCY_NOT_IN_USE     = 11;

    const PACKAGE_ID_FROM_BIONIC_IN_USE_BY_DELETED     = 3;
    const PACKAGE_ID_FROM_RESELLER_IN_USE_BY_DELETED   = 6;
    const PACKAGE_ID_FROM_AGENCY_IN_USE_BY_DELETED     = 13;

//    const PACKAGE_ID_FROM_BIONIC_WILL_BE_LAST     = 0;
    const PACKAGE_ID_FROM_RESELLER_WILL_BE_LAST   = 5;
    const PACKAGE_ID_FROM_AGENCY_WILL_BE_LAST     = 12;


    /**
     *
     * @param int $packageId one of self::PACKAGE_ID_*
     * @return Package 
     */
    protected function findPackage($packageId) {

        $object = null;
        if (!empty($packageId)) {
            $object = PackageTable::getInstance()->find($packageId);
        }
        if (empty($object) || !$object->exists()) {
            throw new Exception('Cannot find such Package.');
        }

        return $object;
    }

    /**
     *
     * @param int $packageId one of self::PACKAGE_ID_*
     * @return Package 
     */
    protected function findCheckedPackage($packageId, $companyId) {

        $package = $this->findPackage($packageId);
        $company = $this->findPackage($companyId);

        if ($package->getCompanyId() != $companyId) {
            throw new Exception("Package '$package' is not for company '$company'.");
        }

        return $package;
    }

    /**
     *
     * @param int $companyId one of self::COMPANY_ID_*
     * @return Company 
     */
    protected function findCompany($companyId) {

        $object = null;
        if (!empty($companyId)) {
            $object = CompanyTable::getInstance()->find($companyId);
        }
        if (empty($object) || !$object->exists()) {
            throw new Exception('Cannot find such Company.');
        }

        return $object;
    }
}
