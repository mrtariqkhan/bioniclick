<?php

require_once dirname(__FILE__) . '/PackageTestBase.php';

/**
 * Description of PackageSafelyDeletingTest
 * 
 * @author fairdev
 */
class PackageSafelyDeletingTest extends PackageTestBase {

    /**
     * @dataProvider providerCases
     * 
     */
    public function testCase(
        $comId, 
        $packageId, 
        $canBeDeleted, 
        $specificAdditionalMessage = ''
    ) {

        $package = $this->findCheckedPackage($packageId, $comId);

        if ($canBeDeleted) {
            $this->checkWillBeDeleted(
                $packageId,
                $package, 
                $specificAdditionalMessage
            );
        } else {
            $this->checkWillNotBeDeleted(
                $package, 
                $specificAdditionalMessage
            );
        }
    }

    protected function checkWillBeDeleted($packageId, $package, $specificAdditionalMessage = '') {

        $package->safeDelete();
        $wasDeleted = $this->checkPackageIsDeleted($packageId);

        $this->assertTrue(
            $wasDeleted,  
            "$specificAdditionalMessage Package has not been deleted."
        );
    }
    
    protected function checkWillNotBeDeleted($package, $specificAdditionalMessage = '') {

        try {
            $package->safeDelete();
        } catch (Exception $e) {
            return;
        }
        $this->fail("$specificAdditionalMessage An expected exception has not been raised.");
    }

    /**
     *
     * @param int $packageId one of self::PACKAGE_ID_*
     * @return boolean 
     */
    protected function checkPackageIsDeleted($packageId) {

        $object = null;
        if (!empty($packageId)) {
            $object = PackageTable::getInstance()->find($packageId);
        }
        if (empty($object) || !$object->exists()) {
            throw new Exception('Cannot find such Package.');
        }

        return $object->getWasDeleted();
    }

    public function providerCases() {
        return array(
            "case Package from Bionic for existent Reseller" => array(
                self::COMPANY_ID_BIONIC,
                self::PACKAGE_ID_FOR_RESELLER,
                false,
                "Bionic's Package is in use by existent Reseller. Package MUST NOT be deleted."
            ),
            "case Package from Bionic for existent Advertiser" => array(
                self::COMPANY_ID_BIONIC,
                self::PACKAGE_ID_FOR_ADVERTISER_BIONIC,
                false,
                "Bionic's Package is in use by existent Advertiser. Package MUST NOT be deleted."
            ),
            "case Package from Reseller for existent Agency" => array(
                self::COMPANY_ID_RESELLER,
                self::PACKAGE_ID_FOR_AGENCY,
                false,
                "Reseller's Package is in use by existent Agency. Package MUST NOT be deleted."
            ),
            "case Package from Reseller for existent Advertiser" => array(
                self::COMPANY_ID_RESELLER,
                self::PACKAGE_ID_FOR_ADVERTISER_RESELLER,
                false,
                "Reseller's Package is in use by existent Advertiser. Package MUST NOT be deleted."
            ),
            "case Package from Agency for existent Advertiser" => array(
                self::COMPANY_ID_AGENCY,
                self::PACKAGE_ID_FOR_ADVERTISER_AGENCY,
                false,
                "Agency's Package is in use by existent Advertiser. Package MUST NOT be deleted."
            ),


            "case Package from Bionic for nothing" => array(
                self::COMPANY_ID_BIONIC,
                self::PACKAGE_ID_FROM_BIONIC_NOT_IN_USE,
                true,
                "Bionic's Package is not in use. Package MUST be deleted."
            ),
            "case Package from Reseller for nothing" => array(
                self::COMPANY_ID_RESELLER,
                self::PACKAGE_ID_FROM_RESELLER_NOT_IN_USE,
                true,
                "Reseller's Package is not in use. Package MUST be deleted."
            ),
            "case Package from Agency for nothing" => array(
                self::COMPANY_ID_AGENCY,
                self::PACKAGE_ID_FROM_AGENCY_NOT_IN_USE,
                true,
                "Agency's Package is not in use. Package MUST be deleted."
            ),


            "case Package from Bionic for nothing" => array(
                self::COMPANY_ID_BIONIC_WITH_DELETED_SUBTREE,
                self::PACKAGE_ID_FROM_BIONIC_IN_USE_BY_DELETED,
                true,
                "Bionic's Package is in use by deleted company and deleted advertiser only. It is not the last package. Package MUST be deleted."
            ),
            "case Package from Reseller for nothing" => array(
                self::COMPANY_ID_RESELLER_WITH_DELETED_SUBTREE,
                self::PACKAGE_ID_FROM_RESELLER_IN_USE_BY_DELETED,
                true,
                "Reseller's Package is in use by deleted company only. It is not the last package. Package MUST be deleted."
            ),
            "case Package from Agency for nothing" => array(
                self::COMPANY_ID_AGENCY_WITH_DELETED_SUBTREE,
                self::PACKAGE_ID_FROM_AGENCY_IN_USE_BY_DELETED,
                true,
                "Agency's Package is in use by deleted advertiser only. It is not the last package. Package MUST be deleted."
            ),


//            "case Package from Bionic for nothing" => array(
//                self::COMPANY_ID_BIONIC_WITH_DELETED_SUBTREE,
//                self::PACKAGE_ID_FROM_BIONIC_IN_USE_BY_DELETED,
//                true,
//                "Bionic's Package is in use by deleted company and deleted advertiser only. Package MUST be deleted."
//            ),
            "case Package from Reseller last" => array(
                self::COMPANY_ID_RESELLER_WITH_DELETED_SUBTREE,
                self::PACKAGE_ID_FROM_RESELLER_WILL_BE_LAST,
                false,
                "Reseller's LAST Package is in use by deleted company only. Package MUST NOT be deleted."
            ),
            "case Package from Agency last" => array(
                self::COMPANY_ID_AGENCY_WITH_DELETED_SUBTREE,
                self::PACKAGE_ID_FROM_AGENCY_WILL_BE_LAST,
                false,
                "Agency's LAST Package is in use by deleted advertiser only. Package MUST NOT be deleted."
            ),
        );
    }
}
