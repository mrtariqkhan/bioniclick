<?php

/**
 * Description of ModelTestsSuite
 *
 * @author fairdev
 */
class ModelTestSuite extends sfBasePhpunitTestSuite
    implements sfPhpunitContextInitilizerInterface, sfPhpunitFixtureDoctrineAggregator {

    /**
     * Dev hook for custom "setUp" stuff
     */
    protected function _start() {

        $this->_initFilters();
        $this
            ->fixture()
            ->clean()
            ->loadCommon('06_CompanyTree.yml')
            ->loadCommon('07_Timezone.yml')
            ->loadCommon('08_sfGuard.yml')
            ->loadCommon('24_PaymentProfile.yml')
            ->loadCommon('26_RecurrentPayment.yml')
        ;
    }

    protected function _initFilters() {

        $filters = sfConfig::get('app_sfPhpunitPlugin_filter', array());
        foreach ($filters as $filter) {
            PHPUnit_Util_Filter::addDirectoryToFilter($filter['path'], $filter['ext']);
        }
    }

    public function getApplication() {
        return 'backend';
    }

    protected function _initFixture(array $options = array()) {

        $options = array(
            'fixture_ext' => ''
        );
        return parent::_initFixture($options);
    }
}

