<?php

/**
 * Description of IncomingCallsActionsTestSuite
 *
 * @author fairdev
 */
class IncomingCallsActionsTestSuite extends sfBasePhpunitTestSuite
    implements sfPhpunitContextInitilizerInterface, sfPhpunitFixtureDoctrineAggregator {

    /**
     * Dev hook for custom "setUp" stuff
     */
    protected function _start() {

        $this->_initFilters();
        $this
            ->fixture()
            ->clean()
            ->loadOwn('01_01_CompanyTree.yml')
            ->loadOwn('01_02_Campaign.yml')
            ->loadOwn('02_01_Timezone.yml')
            ->loadOwn('02_02_sfGuard.yml')
            ->loadOwn('03_Pool_01_TwilioAccount.yml')
            ->loadOwn('03_Pool_02_TwilioLocalAreaCode.yml')
            ->loadOwn('03_Pool_03_TwilioIncomingPhoneNumber.yml')
            ->loadOwn('03_Pool_04_BionicIncomingNumberPool.yml')
            ->loadOwn('03_Pool_05_AdvertiserIncomingNumberPool.yml')
            ->loadOwn('03_Pool_06_IncomingNumbersQueue.yml')
            ->loadOwn('03_Pool_07_AdvertiserIncomingNumberPoolLog.yml')
            ->loadOwn('04_Calls_01_TwilioCity.yml')
            ->loadOwn('04_Calls_10_ReferrerSource.yml')
            ->loadOwn('04_Calls_11_AnalyticsIp.yml')
            ->loadOwn('04_Calls_12_Page.yml')
            ->loadOwn('04_Calls_13_Combo.yml')
            ->loadOwn('04_Calls_14_PhysicalPhoneNumber.yml')
            ->loadOwn('04_Calls_15_PhoneNumberAssignment.yml')
            ->loadOwn('04_Calls_16_VisitorAnalytics.yml')
            ->loadOwn('04_Calls_17_AnalyticsPhoneNumber.yml')
        ;
    }

    protected function _initFilters() {

        $filters = sfConfig::get('app_sfPhpunitPlugin_filter', array());
        foreach ($filters as $filter) {
            PHPUnit_Util_Filter::addDirectoryToFilter($filter['path'], $filter['ext']);
        }
    }

    public function getApplication() {
        return 'backend';
    }

    public function getOwnFixtureDir() {

        $sep = DIRECTORY_SEPARATOR;
        $reflection = new ReflectionClass($this);
        $path = str_replace($sep . get_class($this) . '.php', '', $reflection->getFileName());

        return str_replace(
            "{$sep}test{$sep}phpunit",
            "{$sep}test{$sep}phpunit{$sep}fixtures",
            $path
        );
    }

    protected function _initFixture(array $options = array()) {

        $options = array(
            'fixture_ext' => ''
        );
        return parent::_initFixture($options);
    }
    
}

