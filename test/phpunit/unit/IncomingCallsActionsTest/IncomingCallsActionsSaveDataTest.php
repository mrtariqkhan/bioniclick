<?php

require_once dirname(__FILE__) . '/../../../../lib/model/doctrine/sfTwilioAPIPlugin/TwilioIncomingCallTable.class.php';
require_once dirname(__FILE__) . '/../../../../plugins/sfTwilioAPIPlugin/lib/task/CallInfoTask.class.php';

/**
 * Description of IncomingCallsActionsTest
 *
 * @author fairdev
 */
class IncomingCallsActionsSaveDataTest extends sfBasePhpunitTestCase {

    const URL_CREATE_INCOMING_CALL = '/incomingCalls/create';

    const CALL_TYPE_IN_PROGRESS_WITHOUT_RECORDING   = 'Call in-progress Without Recording';
    const CALL_TYPE_IN_PROGRESS_WITH_RECORDING      = 'Call in-progress With Recording';
    const CALL_TYPE_IN_PROGRESS_WITH_ONE_SEGMENT    = 'Call in-progress With One Segment';

    const CALL_TYPE_COMPLETED_WITHOUT_RECORDING     = 'Call completed Without Recording';
    const CALL_TYPE_COMPLETED_WITH_RECORDING        = 'Call completed With Recording';
    const CALL_TYPE_COMPLETED_WITH_ONE_SEGMENT      = 'Call completed With One Segment';

    const PARAMETER_ACCOUNT_SID             = 'AC3657210a30ba1b3272221bffbd6d8ad6';
    const PARAMETER_CALLER                  = '8663740610';
    const PARAMETER_TIPN_NUMBER_01_CAM      = '1111111011';
    const PARAMETER_TIPN_NUMBER_02_ADV      = '1111111012';
    const PARAMETER_TIPN_NUMBER_03_BIONIC   = '1111111013';
    const PARAMETER_TIPN_NUMBER_04_CAM      = '1111111021';
    const PARAMETER_TIPN_NUMBER_05_ADV      = '1111111022';
    const PARAMETER_TIPN_NUMBER_06_BIONIC   = '1111111023';

    protected static $CALL_POST_PARAMS = array(
        self::CALL_TYPE_IN_PROGRESS_WITHOUT_RECORDING => array(
            'CallSid'       => '0000000000000000000000000000000001',
            'Caller'        => self::PARAMETER_CALLER,
            'Called'        => self::PARAMETER_TIPN_NUMBER_01_CAM,
            'AccountSid'    => self::PARAMETER_ACCOUNT_SID,
            'CallStatus'    => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
        ),
        self::CALL_TYPE_IN_PROGRESS_WITH_RECORDING => array(
            'CallSid'       => '0000000000000000000000000000000002',
            'Caller'        => self::PARAMETER_CALLER,
            'Called'        => self::PARAMETER_TIPN_NUMBER_02_ADV,
            'AccountSid'    => self::PARAMETER_ACCOUNT_SID,
            'CallStatus'    => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
        ),
        self::CALL_TYPE_IN_PROGRESS_WITH_ONE_SEGMENT => array(

            'CallSid'       => '0000000000000000000000000000000003',
            'Caller'        => self::PARAMETER_CALLER,
            'Called'        => self::PARAMETER_TIPN_NUMBER_03_BIONIC,
            'AccountSid'    => self::PARAMETER_ACCOUNT_SID,
            'CallStatus'    => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
        ),
        
        self::CALL_TYPE_COMPLETED_WITHOUT_RECORDING => array(
            'CallSid'       => '0000000000000000000000000000000004',
            'Caller'        => self::PARAMETER_CALLER,
            'Called'        => self::PARAMETER_TIPN_NUMBER_04_CAM,
            'AccountSid'    => self::PARAMETER_ACCOUNT_SID,
            'CallStatus'    => TwilioIncomingCallTable::STATUS_COMPLETED,
        ),
        self::CALL_TYPE_COMPLETED_WITH_RECORDING => array(
            'CallSid'       => '0000000000000000000000000000000005',
            'Caller'        => self::PARAMETER_CALLER,
            'Called'        => self::PARAMETER_TIPN_NUMBER_05_ADV,
            'AccountSid'    => self::PARAMETER_ACCOUNT_SID,
            'CallStatus'    => TwilioIncomingCallTable::STATUS_COMPLETED,
        ),
        self::CALL_TYPE_COMPLETED_WITH_ONE_SEGMENT => array(
            'CallSid'       => '0000000000000000000000000000000006',
            'Caller'        => self::PARAMETER_CALLER,
            'Called'        => self::PARAMETER_TIPN_NUMBER_06_BIONIC,
            'AccountSid'    => self::PARAMETER_ACCOUNT_SID,
            'CallStatus'    => TwilioIncomingCallTable::STATUS_COMPLETED,
        ),
    );
    protected static $CALL_SEGMENTS = array(
        self::CALL_TYPE_IN_PROGRESS_WITHOUT_RECORDING => array(
            array(
                'CallSegmentSid'=> '0000000000000000000000000000000011',
                'StartTime'     => 'Tue, 25 Oct 2011 12:28:58 -0700',
                'EndTime'       => 'Tue, 25 Oct 2011 12:28:59 -0700',
                'Duration'      => '1',
                'Price'         => '',
                'Status'        => TwilioIncomingCallTable::STATUS_COMPLETED,
                'Called'        => '111111111',
            ),
        ),
        self::CALL_TYPE_IN_PROGRESS_WITH_RECORDING => array(
            array(
                'CallSegmentSid'=> '0000000000000000000000000000000012',
                'StartTime'     => 'Tue, 25 Oct 2011 13:20:34 -0700',
                'EndTime'       => 'Tue, 25 Oct 2011 13:20:36 -0700',
                'Duration'      => '2',
                'Price'         => '',
                'Status'        => TwilioIncomingCallTable::STATUS_COMPLETED,
                'Called'        => '222222222',
            ),
        ),
        self::CALL_TYPE_IN_PROGRESS_WITH_ONE_SEGMENT => array(
            array(
                'CallSegmentSid'=> '',
                'StartTime'     => 'Tue, 25 Oct 2011 14:18:14 -0700',
                'EndTime'       => 'Tue, 25 Oct 2011 14:18:17 -0700',
                'Duration'      => '3',
                'Price'         => '0.03',
                'Status'        => TwilioIncomingCallTable::STATUS_COMPLETED,
                'Called'        => '',
            ),
        ),

        self::CALL_TYPE_COMPLETED_WITHOUT_RECORDING => array(
            array(
                'CallSegmentSid'=> '0000000000000000000000000000000014',
                'StartTime'     => 'Tue, 25 Oct 2011 14:00:01 -0700',
                'EndTime'       => 'Tue, 25 Oct 2011 14:00:03 -0700',
                'Duration'      => '2',
                'Price'         => '',
                'Status'        => TwilioIncomingCallTable::STATUS_COMPLETED,
                'Called'        => '444444444',
            ),
        ),
        self::CALL_TYPE_COMPLETED_WITH_RECORDING => array(
            array(
                'CallSegmentSid'=> '0000000000000000000000000000000015',
                'StartTime'     => 'Tue, 25 Oct 2011 15:17:00 -0700',
                'EndTime'       => 'Tue, 25 Oct 2011 15:18:00 -0700',
                'Duration'      => '60',
                'Price'         => '',
                'Status'        => TwilioIncomingCallTable::STATUS_COMPLETED,
                'Called'        => '555555555',
            ),
        ),
        self::CALL_TYPE_COMPLETED_WITH_ONE_SEGMENT => array(
            array(
                'CallSegmentSid'=> '',
                'StartTime'     => 'Tue, 25 Oct 2011 16:26:11 -0700',
                'EndTime'       => 'Tue, 25 Oct 2011 16:26:19 -0700',
                'Duration'      => '8',
                'Price'         => '0.03',
                'Status'        => TwilioIncomingCallTable::STATUS_COMPLETED,
                'Called'        => '',
            ),
        ),
    );
    protected static $CALL_RECORDINGS = array(
        self::CALL_TYPE_IN_PROGRESS_WITHOUT_RECORDING => array(),
        self::CALL_TYPE_IN_PROGRESS_WITH_RECORDING => array(
            array(
                'Sid'           => 'RECORDING0000000000000000000000022',
                'AccountSid'    => self::PARAMETER_ACCOUNT_SID,
                'CallSid'       => '0000000000000000000000000000000002',
                'Duration'      => '2',
                'DateCreated'   => 'Tue, 25 Oct 2011 13:20:35 -0700',
                'DateUpdated'   => 'Tue, 25 Oct 2011 13:20:35 -0700',
            ),
        ),
        self::CALL_TYPE_IN_PROGRESS_WITH_ONE_SEGMENT => array(),

        self::CALL_TYPE_COMPLETED_WITHOUT_RECORDING => array(),
        self::CALL_TYPE_COMPLETED_WITH_RECORDING => array(
            array(
                'Sid'           => 'RECORDING0000000000000000000000024',
                'AccountSid'    => self::PARAMETER_ACCOUNT_SID,
                'CallSid'       => '0000000000000000000000000000000004',
                'Duration'      => '2',
                'DateCreated'   => 'Tue, 25 Oct 2011 15:17:00 -0700',
                'DateUpdated'   => 'Tue, 25 Oct 2011 15:17:00 -0700',
            ),
        ),
        self::CALL_TYPE_COMPLETED_WITH_ONE_SEGMENT => array(),
    );


    const EXPECTED_PARAMETER_TYPE_CALLS         = 'Incoming calls amount';
    const EXPECTED_PARAMETER_TYPE_REDIALED_CALLS= 'Redialed calls amount';
    const EXPECTED_PARAMETER_TYPE_RECORDINGS    = 'Recordings amount';
    const EXPECTED_PARAMETER_TYPE_QUEUES        = 'Queues amount';
    const EXPECTED_PARAMETER_TYPE_PHONE_CALLS   = 'Phone calls amount';
    const EXPECTED_PARAMETER_TYPE_PHONE_CALL    = 'Phone call';
    const EXPECTED_PARAMETER_ADV                = 'adv';
    const EXPECTED_PARAMETER_CAM                = 'cam';
    const EXPECTED_PARAMETER_VA                 = 'va';

    const ADV_ID    = 1;
    const CAM_ID    = 1;
    const VA_ID_01  = 1;
    const VA_ID_02  = 2;

    protected static $CALL_IDS = array(
        self::CALL_TYPE_IN_PROGRESS_WITHOUT_RECORDING   => 1,
        self::CALL_TYPE_IN_PROGRESS_WITH_RECORDING      => 2,
        self::CALL_TYPE_IN_PROGRESS_WITH_ONE_SEGMENT    => 3,
        self::CALL_TYPE_COMPLETED_WITHOUT_RECORDING     => 4,
        self::CALL_TYPE_COMPLETED_WITH_RECORDING        => 5,
        self::CALL_TYPE_COMPLETED_WITH_ONE_SEGMENT      => 6,
    );

    protected static $EXPECTED_FIRST_CALL_DIFFERENCES_BEFORE_TASK = array(
        self::CALL_TYPE_IN_PROGRESS_WITHOUT_RECORDING => array(
            self::EXPECTED_PARAMETER_TYPE_CALLS          => 1,
            self::EXPECTED_PARAMETER_TYPE_REDIALED_CALLS => 0,
            self::EXPECTED_PARAMETER_TYPE_RECORDINGS     => 0,
            self::EXPECTED_PARAMETER_TYPE_QUEUES         => 0,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALLS    => 1,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALL     => array(
                self::EXPECTED_PARAMETER_ADV => self::ADV_ID,
                self::EXPECTED_PARAMETER_CAM => self::CAM_ID,
                self::EXPECTED_PARAMETER_VA  => null,
            ),
        ),
        self::CALL_TYPE_IN_PROGRESS_WITH_RECORDING => array(
            self::EXPECTED_PARAMETER_TYPE_CALLS          => 1,
            self::EXPECTED_PARAMETER_TYPE_REDIALED_CALLS => 0,
            self::EXPECTED_PARAMETER_TYPE_RECORDINGS     => 0,
            self::EXPECTED_PARAMETER_TYPE_QUEUES         => 0,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALLS    => 1,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALL     => array(
                self::EXPECTED_PARAMETER_ADV => self::ADV_ID,
                self::EXPECTED_PARAMETER_CAM => null,
                self::EXPECTED_PARAMETER_VA  => null,
            ),
        ),
        self::CALL_TYPE_IN_PROGRESS_WITH_ONE_SEGMENT => array(
            self::EXPECTED_PARAMETER_TYPE_CALLS          => 1,
            self::EXPECTED_PARAMETER_TYPE_REDIALED_CALLS => 0,
            self::EXPECTED_PARAMETER_TYPE_RECORDINGS     => 0,
            self::EXPECTED_PARAMETER_TYPE_QUEUES         => 0,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALLS    => 1,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALL     => array(
                self::EXPECTED_PARAMETER_ADV => null,
                self::EXPECTED_PARAMETER_CAM => null,
                self::EXPECTED_PARAMETER_VA  => null,
            ),
        ),

        self::CALL_TYPE_COMPLETED_WITHOUT_RECORDING => array(
            self::EXPECTED_PARAMETER_TYPE_CALLS          => 1,
            self::EXPECTED_PARAMETER_TYPE_REDIALED_CALLS => 0,
            self::EXPECTED_PARAMETER_TYPE_RECORDINGS     => 0,
            self::EXPECTED_PARAMETER_TYPE_QUEUES         => 1,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALLS    => 1,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALL     => array(
                self::EXPECTED_PARAMETER_ADV => self::ADV_ID,
                self::EXPECTED_PARAMETER_CAM => self::CAM_ID,
                self::EXPECTED_PARAMETER_VA  => null,
            ),
        ),
        self::CALL_TYPE_COMPLETED_WITH_RECORDING => array(
            self::EXPECTED_PARAMETER_TYPE_CALLS          => 1,
            self::EXPECTED_PARAMETER_TYPE_REDIALED_CALLS => 0,
            self::EXPECTED_PARAMETER_TYPE_RECORDINGS     => 0,
            self::EXPECTED_PARAMETER_TYPE_QUEUES         => 1,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALLS    => 1,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALL     => array(
                self::EXPECTED_PARAMETER_ADV => self::ADV_ID,
                self::EXPECTED_PARAMETER_CAM => null,
                self::EXPECTED_PARAMETER_VA  => null,
            ),
        ),
        self::CALL_TYPE_COMPLETED_WITH_ONE_SEGMENT => array(
            self::EXPECTED_PARAMETER_TYPE_CALLS          => 1,
            self::EXPECTED_PARAMETER_TYPE_REDIALED_CALLS => 0,
            self::EXPECTED_PARAMETER_TYPE_RECORDINGS     => 0,
            self::EXPECTED_PARAMETER_TYPE_QUEUES         => 1,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALLS    => 1,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALL     => array(
                self::EXPECTED_PARAMETER_ADV => null,
                self::EXPECTED_PARAMETER_CAM => null,
                self::EXPECTED_PARAMETER_VA  => null,
            ),
        ),
    );

    protected static $EXPECTED_FIRST_CALL_DIFFERENCES_AFTER_TASK = array(
        self::CALL_TYPE_IN_PROGRESS_WITHOUT_RECORDING => array(
            self::EXPECTED_PARAMETER_TYPE_CALLS          => 1,
            self::EXPECTED_PARAMETER_TYPE_REDIALED_CALLS => 0,
            self::EXPECTED_PARAMETER_TYPE_RECORDINGS     => 0,
            self::EXPECTED_PARAMETER_TYPE_QUEUES         => 0,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALLS    => 1,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALL     => array(
                self::EXPECTED_PARAMETER_ADV => self::ADV_ID,
                self::EXPECTED_PARAMETER_CAM => self::CAM_ID,
                self::EXPECTED_PARAMETER_VA  => null,
            ),
        ),
        self::CALL_TYPE_IN_PROGRESS_WITH_RECORDING => array(
            self::EXPECTED_PARAMETER_TYPE_CALLS          => 1,
            self::EXPECTED_PARAMETER_TYPE_REDIALED_CALLS => 0,
            self::EXPECTED_PARAMETER_TYPE_RECORDINGS     => 0,
            self::EXPECTED_PARAMETER_TYPE_QUEUES         => 0,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALLS    => 1,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALL     => array(
                self::EXPECTED_PARAMETER_ADV => self::ADV_ID,
                self::EXPECTED_PARAMETER_CAM => null,
                self::EXPECTED_PARAMETER_VA  => null,
            ),
        ),
        self::CALL_TYPE_IN_PROGRESS_WITH_ONE_SEGMENT => array(
            self::EXPECTED_PARAMETER_TYPE_CALLS          => 1,
            self::EXPECTED_PARAMETER_TYPE_REDIALED_CALLS => 0,
            self::EXPECTED_PARAMETER_TYPE_RECORDINGS     => 0,
            self::EXPECTED_PARAMETER_TYPE_QUEUES         => 0,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALLS    => 1,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALL     => array(
                self::EXPECTED_PARAMETER_ADV => null,
                self::EXPECTED_PARAMETER_CAM => null,
                self::EXPECTED_PARAMETER_VA  => null,
            ),
        ),

        self::CALL_TYPE_COMPLETED_WITHOUT_RECORDING => array(
            self::EXPECTED_PARAMETER_TYPE_CALLS          => 1,
            self::EXPECTED_PARAMETER_TYPE_REDIALED_CALLS => 1,
            self::EXPECTED_PARAMETER_TYPE_RECORDINGS     => 0,
            self::EXPECTED_PARAMETER_TYPE_QUEUES         => 0,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALLS    => 1,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALL     => array(
                self::EXPECTED_PARAMETER_ADV => self::ADV_ID,
                self::EXPECTED_PARAMETER_CAM => self::CAM_ID,
                self::EXPECTED_PARAMETER_VA  => self::VA_ID_02,
            ),
        ),
        self::CALL_TYPE_COMPLETED_WITH_RECORDING => array(
            self::EXPECTED_PARAMETER_TYPE_CALLS          => 1,
            self::EXPECTED_PARAMETER_TYPE_REDIALED_CALLS => 1,
            self::EXPECTED_PARAMETER_TYPE_RECORDINGS     => 1,
            self::EXPECTED_PARAMETER_TYPE_QUEUES         => 0,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALLS    => 1,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALL     => array(
                self::EXPECTED_PARAMETER_ADV => self::ADV_ID,
                self::EXPECTED_PARAMETER_CAM => null,
                self::EXPECTED_PARAMETER_VA  => null,
            ),
        ),
        self::CALL_TYPE_COMPLETED_WITH_ONE_SEGMENT => array(
            self::EXPECTED_PARAMETER_TYPE_CALLS          => 1,
            self::EXPECTED_PARAMETER_TYPE_REDIALED_CALLS => 0,
            self::EXPECTED_PARAMETER_TYPE_RECORDINGS     => 0,
            self::EXPECTED_PARAMETER_TYPE_QUEUES         => 0,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALLS    => 1,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALL     => array(
                self::EXPECTED_PARAMETER_ADV => null,
                self::EXPECTED_PARAMETER_CAM => null,
                self::EXPECTED_PARAMETER_VA  => null,
            ),
        ),
    );

    protected static $EXPECTED_SECOND_CALL_DIFFERENCES_BEFORE_TASK = array(
        self::CALL_TYPE_IN_PROGRESS_WITHOUT_RECORDING => array(
            self::EXPECTED_PARAMETER_TYPE_CALLS          => 0,
            self::EXPECTED_PARAMETER_TYPE_REDIALED_CALLS => 0,
            self::EXPECTED_PARAMETER_TYPE_RECORDINGS     => 0,
            self::EXPECTED_PARAMETER_TYPE_QUEUES         => 1,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALLS    => 0,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALL     => array(
                self::EXPECTED_PARAMETER_ADV => self::ADV_ID,
                self::EXPECTED_PARAMETER_CAM => self::CAM_ID,
                self::EXPECTED_PARAMETER_VA  => null,
            ),
        ),
        self::CALL_TYPE_IN_PROGRESS_WITH_RECORDING => array(
            self::EXPECTED_PARAMETER_TYPE_CALLS          => 0,
            self::EXPECTED_PARAMETER_TYPE_REDIALED_CALLS => 0,
            self::EXPECTED_PARAMETER_TYPE_RECORDINGS     => 0,
            self::EXPECTED_PARAMETER_TYPE_QUEUES         => 1,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALLS    => 0,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALL     => array(
                self::EXPECTED_PARAMETER_ADV => self::ADV_ID,
                self::EXPECTED_PARAMETER_CAM => null,
                self::EXPECTED_PARAMETER_VA  => null,
            ),
        ),
        self::CALL_TYPE_IN_PROGRESS_WITH_ONE_SEGMENT => array(
            self::EXPECTED_PARAMETER_TYPE_CALLS          => 0,
            self::EXPECTED_PARAMETER_TYPE_REDIALED_CALLS => 0,
            self::EXPECTED_PARAMETER_TYPE_RECORDINGS     => 0,
            self::EXPECTED_PARAMETER_TYPE_QUEUES         => 1,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALLS    => 0,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALL     => array(
                self::EXPECTED_PARAMETER_ADV => null,
                self::EXPECTED_PARAMETER_CAM => null,
                self::EXPECTED_PARAMETER_VA  => null,
            ),
        ),

        self::CALL_TYPE_COMPLETED_WITHOUT_RECORDING => array(),  //second request will not appear
        self::CALL_TYPE_COMPLETED_WITH_RECORDING    => array(),  //second request will not appear
        self::CALL_TYPE_COMPLETED_WITH_ONE_SEGMENT  => array(),  //second request will not appear
    );

    protected static $EXPECTED_SECOND_CALL_DIFFERENCES_AFTER_TASK = array(
        self::CALL_TYPE_IN_PROGRESS_WITHOUT_RECORDING => array(
            self::EXPECTED_PARAMETER_TYPE_CALLS          => 0,
            self::EXPECTED_PARAMETER_TYPE_REDIALED_CALLS => 1,
            self::EXPECTED_PARAMETER_TYPE_RECORDINGS     => 0,
            self::EXPECTED_PARAMETER_TYPE_QUEUES         => 0,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALLS    => 0,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALL     => array(
                self::EXPECTED_PARAMETER_ADV => self::ADV_ID,
                self::EXPECTED_PARAMETER_CAM => self::CAM_ID,
                self::EXPECTED_PARAMETER_VA  => self::VA_ID_01,
            ),
        ),
        self::CALL_TYPE_IN_PROGRESS_WITH_RECORDING => array(
            self::EXPECTED_PARAMETER_TYPE_CALLS          => 0,
            self::EXPECTED_PARAMETER_TYPE_REDIALED_CALLS => 1,
            self::EXPECTED_PARAMETER_TYPE_RECORDINGS     => 1,
            self::EXPECTED_PARAMETER_TYPE_QUEUES         => 0,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALLS    => 0,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALL     => array(
                self::EXPECTED_PARAMETER_ADV => self::ADV_ID,
                self::EXPECTED_PARAMETER_CAM => null,
                self::EXPECTED_PARAMETER_VA  => null,
            ),
        ),
        self::CALL_TYPE_IN_PROGRESS_WITH_ONE_SEGMENT => array(
            self::EXPECTED_PARAMETER_TYPE_CALLS          => 0,
            self::EXPECTED_PARAMETER_TYPE_REDIALED_CALLS => 0,
            self::EXPECTED_PARAMETER_TYPE_RECORDINGS     => 0,
            self::EXPECTED_PARAMETER_TYPE_QUEUES         => 0,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALLS    => 0,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALL     => array(
                self::EXPECTED_PARAMETER_ADV => null,
                self::EXPECTED_PARAMETER_CAM => null,
                self::EXPECTED_PARAMETER_VA  => null,
            ),
        ),

        self::CALL_TYPE_COMPLETED_WITHOUT_RECORDING => array(),  //second request will not appear
        self::CALL_TYPE_COMPLETED_WITH_RECORDING    => array(),  //second request will not appear
        self::CALL_TYPE_COMPLETED_WITH_ONE_SEGMENT  => array(),  //second request will not appear
    );

    /**
     *
     * @dataProvider providerFirstCallCases
     * 
     * @param string $typeCode one of self::CALL_TYPE_*
     */
    public function testFirstCallCase($typeCode) {

        $expectedDiffBefore = self::$EXPECTED_FIRST_CALL_DIFFERENCES_BEFORE_TASK[$typeCode];
        $expectedDiffAfter  = self::$EXPECTED_FIRST_CALL_DIFFERENCES_AFTER_TASK[$typeCode];
        $id                 = self::$CALL_IDS[$typeCode];
        $phoneCallInfoBefore= $expectedDiffBefore[self::EXPECTED_PARAMETER_TYPE_PHONE_CALL];
        $phoneCallInfoAfter = $expectedDiffAfter[self::EXPECTED_PARAMETER_TYPE_PHONE_CALL];


        $oldAmounts = $this->getTablesRawsAmounts();
        $this->checkTicExists($id, false, 'before everything');
        $this->checkPhoneCall($id, false, 'before everything');

        $this->createIncomingCall($typeCode);

        $this->checkTicExists($id, true, 'before task');
        $this->checkPhoneCall($id, true, 'before task', $phoneCallInfoBefore);

        $newAmountsBefore = $this->getTablesRawsAmounts();
        $this->checkAmounts($oldAmounts, $newAmountsBefore, $expectedDiffBefore, ' before task');

        $this->executeCallInfoTask($typeCode);

        $this->checkTicExists($id, true, 'after task');
        $this->checkPhoneCall($id, true, 'after task', $phoneCallInfoAfter);

        $newAmountsAfter = $this->getTablesRawsAmounts();
        $this->checkAmounts($oldAmounts, $newAmountsAfter, $expectedDiffAfter, ' after task');
    }

    /**
     * @depends testFirstCallCase
     * 
     * @dataProvider providerSecondCallCases
     * 
     * @param string $typeCode one of self::CALL_TYPE_*
     */
    public function testSecondCallCase($typeCode) {

        $expectedDiffBefore = self::$EXPECTED_SECOND_CALL_DIFFERENCES_BEFORE_TASK[$typeCode];
        $expectedDiffAfter  = self::$EXPECTED_SECOND_CALL_DIFFERENCES_AFTER_TASK[$typeCode];
        $id                 = self::$CALL_IDS[$typeCode];
        $phoneCallInfoBefore= $expectedDiffBefore[self::EXPECTED_PARAMETER_TYPE_PHONE_CALL];
        $phoneCallInfoAfter = $expectedDiffAfter[self::EXPECTED_PARAMETER_TYPE_PHONE_CALL];

        $oldAmounts = $this->getTablesRawsAmounts();
        $this->checkTicExists($id, true, 'before everything (2nd)');
        $this->checkPhoneCall($id, true, 'before everything (2nd)', $phoneCallInfoBefore);

        $this->createIncomingCall($typeCode);

        $this->checkTicExists($id, true, 'before task');
        $this->checkPhoneCall($id, true, 'before task', $phoneCallInfoBefore);

        $newAmountsBefore = $this->getTablesRawsAmounts();
        $this->checkAmounts($oldAmounts, $newAmountsBefore, $expectedDiffBefore, ' before task');

        $this->executeCallInfoTask($typeCode);

        $this->checkTicExists($id, true, 'after task');
        $this->checkPhoneCall($id, true, 'after task', $phoneCallInfoAfter);

        $newAmountsAfter = $this->getTablesRawsAmounts();
        $this->checkAmounts($oldAmounts, $newAmountsAfter, $expectedDiffAfter, ' after task');
    }

    public function providerFirstCallCases() {
        return array(
            'case 01' => array(self::CALL_TYPE_IN_PROGRESS_WITHOUT_RECORDING),
            'case 02' => array(self::CALL_TYPE_IN_PROGRESS_WITH_RECORDING),
            'case 03' => array(self::CALL_TYPE_IN_PROGRESS_WITH_ONE_SEGMENT),

            'case 04' => array(self::CALL_TYPE_COMPLETED_WITHOUT_RECORDING),
            'case 05' => array(self::CALL_TYPE_COMPLETED_WITH_RECORDING),
            'case 06' => array(self::CALL_TYPE_COMPLETED_WITH_ONE_SEGMENT),
        );
    }

    public function providerSecondCallCases() {
        return array(
            'case 07' => array(self::CALL_TYPE_IN_PROGRESS_WITHOUT_RECORDING),
            'case 08' => array(self::CALL_TYPE_IN_PROGRESS_WITH_RECORDING),
            'case 09' => array(self::CALL_TYPE_IN_PROGRESS_WITH_ONE_SEGMENT),
        );
    }

    protected function checkTicExists($id, $exists, $msg = '') {

        $incomingCall = TwilioIncomingCallTable::getInstance()->find($id);
        if ($exists) {
            $this->assertTrue(
                !empty($incomingCall) && $incomingCall->exists(),
                "$msg. There is no TwilioIncomingCall with id <$id>."
            );
        } else {
            $this->assertFalse(
                !empty($incomingCall) && $incomingCall->exists(),
                "$msg. There is TwilioIncomingCall with id <$id>."
            );
        }
    }

    protected function checkPhoneCall($id, $exists, $msg = '', $phoneCallInfo = array()) {

        $phoneCall = PhoneCallTable::getInstance()->find($id);
        if ($exists) {
            $this->assertTrue(
                !empty($phoneCall) && $phoneCall->exists(),
                "$msg. There is no PhoneCall with id <$id>."
            );
        } else {
            $this->assertFalse(
                !empty($phoneCall) && $phoneCall->exists(),
                "$msg. There is no PhoneCall with id <$id>."
            );
        }

        if (empty($phoneCallInfo)) {
            return;
        }

        $adv    = $phoneCallInfo[self::EXPECTED_PARAMETER_ADV];
        $cam    = $phoneCallInfo[self::EXPECTED_PARAMETER_CAM];
        $va     = $phoneCallInfo[self::EXPECTED_PARAMETER_VA];

        $advAdctual = $phoneCall->getAdvertiserId();
        $camAdctual = $phoneCall->getCampaignId();
        $vaAdctual  = $phoneCall->getVisitorAnalyticsId();

        if (empty($adv)) {
            $this->assertTrue(empty($advAdctual), "$msg. Phone call <$id> is wrong. Advertiser should be empty.");
        } else {
            $this->assertEquals(
                $adv,
                $advAdctual,
                "$msg. Phone call <$id> is wrong. Advertiser is wrong."
            );
        }
        if (empty($cam)) {
            $this->assertTrue(empty($camAdctual), "$msg. Phone call <$id> is wrong. Campaign should be empty.");
        } else {
            $this->assertEquals(
                $cam,
                $camAdctual,
                "$msg. Phone call <$id> is wrong. Campaign is wrong."
            );
        }
        if (empty($va)) {
            $this->assertTrue(empty($vaAdctual), "$msg. Phone call <$id> is wrong. VisitorAnalytics should be empty.");
        } else {
            $this->assertEquals(
                $va,
                $vaAdctual,
                "$msg. Phone call <$id> is wrong. VisitorAnalytics is wrong."
            );
        }
    }


    /**
     * Counts amount of raws in all need tables (TwilioIncomingCallTable, TwilioRedialedCallTable, TwilioCallRecordingTable)
     * 
     * @return array 
     */
    protected function getTablesRawsAmounts() {
        
        $incomingCallCount = TwilioIncomingCallTable::getInstance()->createQuery()->count();
        $redialedCallCount = TwilioRedialedCallTable::getInstance()->createQuery()->count();
        $recordingCount = TwilioCallRecordingTable::getInstance()->createQuery()->count();
        $queueCount = TwilioCallQueueTable::getInstance()->createQuery()->count();
        $phoneCallCount = PhoneCallTable::getInstance()->createQuery()->count();

        return array(
            self::EXPECTED_PARAMETER_TYPE_CALLS             => $incomingCallCount,
            self::EXPECTED_PARAMETER_TYPE_REDIALED_CALLS    => $redialedCallCount,
            self::EXPECTED_PARAMETER_TYPE_RECORDINGS        => $recordingCount,
            self::EXPECTED_PARAMETER_TYPE_QUEUES            => $queueCount,
            self::EXPECTED_PARAMETER_TYPE_PHONE_CALLS       => $phoneCallCount,
        );
    }

    /**
     * Asserts if (oldValue + expectedDifference != newValue) for all postitions
     *
     * @param array $oldAmounts
     * @param array $newAmounts
     * @param array $expectedDiff
     * @param array $additionalType
     */
    protected function checkAmounts($oldAmounts, $newAmounts, $expectedDiff, $additionalType = '') {

        $this->checkAmount($oldAmounts, $newAmounts, $expectedDiff, self::EXPECTED_PARAMETER_TYPE_CALLS, $additionalType);
        $this->checkAmount($oldAmounts, $newAmounts, $expectedDiff, self::EXPECTED_PARAMETER_TYPE_REDIALED_CALLS, $additionalType);
        $this->checkAmount($oldAmounts, $newAmounts, $expectedDiff, self::EXPECTED_PARAMETER_TYPE_RECORDINGS, $additionalType);
        $this->checkAmount($oldAmounts, $newAmounts, $expectedDiff, self::EXPECTED_PARAMETER_TYPE_QUEUES, $additionalType);
        $this->checkAmount($oldAmounts, $newAmounts, $expectedDiff, self::EXPECTED_PARAMETER_TYPE_PHONE_CALLS, $additionalType);
    }

    /**
     * Asserts if (oldValue + expectedDifference != newValue)
     *
     * @param array $oldAmounts
     * @param array $newAmounts
     * @param array $expectedDiff
     * @param string $expectedParameterType one of self::self::EXPECTED_PARAMETER_TYPE_*
     * @param array $additionalType
     */
    protected function checkAmount(
        $oldAmounts, 
        $newAmounts, 
        $expectedDiff, 
        $expectedParameterType,
        $additionalType = ''
    ) {
        $this->assertEquals(
            $oldAmounts[$expectedParameterType] + $expectedDiff[$expectedParameterType],
            $newAmounts[$expectedParameterType],
            "{$expectedParameterType}{$additionalType} is wrong."
        );
    }



    /**
     * Creates incoming call only
     * 
     * @param string $typeCode one of self::CALL_TYPE_*
     */
    protected function createIncomingCall($typeCode) {

        $browser = new sfBrowser();
        $browser->post(self::URL_CREATE_INCOMING_CALL, self::$CALL_POST_PARAMS[$typeCode]);
    }

    /**
     * Emuates work of real task (This mock object does not use Twilio, 
     * but it uses fixture info about recordings and redialed calls)
     * 
     * @param string $typeCode one of self::CALL_TYPE_*
     */
    protected function executeCallInfoTask($typeCode) {

        $dispatcher = sfContext::getInstance()->getEventDispatcher();
        $formatter = new sfFormatter();

        // NOTE:: Create a Mock Object for the CallInfoTask class mocking need methods.
        $taskMock = $this->getMock(
            'CallInfoTask',
            array(
                'findCallSegments',
                'findCallRecordingList',
            ), 
            array(
                $dispatcher,
                $formatter,
            )
        );
 
        // NOTE:: Set up the expectations for need methods
        $taskMock
            ->expects($this->any())
            ->method('findCallSegments')
            ->will($this->returnValue(self::$CALL_SEGMENTS[$typeCode]))
        ;
        $taskMock
            ->expects($this->any())
            ->method('findCallRecordingList')
            ->will($this->returnValue(self::$CALL_RECORDINGS[$typeCode]))
        ;

        TaskHelper::runTask($taskMock, array(), array(), 'test');
    }
}
