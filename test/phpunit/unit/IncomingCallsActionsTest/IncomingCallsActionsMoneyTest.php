<?php

/**
 * Description of IncomingCallsActionsMoneyTest
 *
 * @author fairdev
 */
//class IncomingCallsActionsMoneyTest extends sfBasePhpunitTestCase {
//
//    //TODO:: what should test this class? Correct it if it is need.
//
//    protected static $username = 'bionic-admin-MOORE@bionicclick.com';
//
//    public function testCreateTwoSegmentFirstRequest() {
//
//        $postParams = array(
//            'CallSid' => 'CAc529f83c52b188ccac901a5263f5a3af',
//            'Caller' => '2819759960',
//            'Called' => '8322611242',
//            'AccountSid' => 'AC3657210a30ba1b3272221bffbd6d8ad6',
//            'CallStatus' => 'completed'
//        );
//        $user = sfGuardUserTable::findByUsername(self::$username);
//        $credit = $user->getProfile()->getCredit();
//        $browser = new sfBrowser();
//        $browser->post('/incomingCalls/create', $postParams);
//        $newCredit = $user->getProfile()->getCredit();
//        $this->assertTrue($credit === $newCredit);
//    }
//
//    /**
//     * @depends testCreateTwoSegmentFirstRequest
//     */
//    public function testCreateTwoSegmentSecondRequest() {
//
//        $postParams = array(
//            'CallSid' => 'CAc529f83c52b188ccac901a5263f5a3af',
//            'Caller' => '2819759960',
//            'Called' => '8322611242',
//            'AccountSid' => 'AC3657210a30ba1b3272221bffbd6d8ad6',
//            'CallStatus' => 'completed'
//        );
//        $user = sfGuardUserTable::findByUsername(self::$username);
//        $credit = $user->getProfile()->getCredit();
//        $browser = new sfBrowser();
//        $browser->post('/incomingCalls/create', $postParams);
//        $newCredit = $user->getProfile()->getCredit();
//        $this->assertTrue($credit > $newCredit);
//    }
//
//
//
//    public function testCreateOneSegmentFirstRequest() {
//
//        $postParams = array(
//            'CallSid' => 'CA0a7b62a3f5d9c52ae04690b9647fe0e6',
//            'Caller' => '7135286817',
//            'Called' => '2819144460',
//            'AccountSid' => 'AC3657210a30ba1b3272221bffbd6d8ad6',
//            'CallStatus' => 'completed'
//        );
//        $user = sfGuardUserTable::findByUsername(self::$username);
//        $credit = $user->getProfile()->getCredit();
//        $browser = new sfBrowser();
//        $browser->post('/incomingCalls/create', $postParams);
//        $newCredit = $user->getProfile()->getCredit();
//        $this->assertTrue($credit === $newCredit);
//    }
//
//    /*
//     * @depends testCreateOneSegmentFirstRequest
//     */
//    public function testCreateOneSegmentSecondRequest() {
//
//        $postParams = array(
//            'CallSid' => 'CA0a7b62a3f5d9c52ae04690b9647fe0e6',
//            'Caller' => '7135286817',
//            'Called' => '2819144460',
//            'AccountSid' => 'AC3657210a30ba1b3272221bffbd6d8ad6',
//            'CallStatus' => 'completed'
//        );
//        $user = sfGuardUserTable::findByUsername(self::$username);
//        $credit = $user->getProfile()->getCredit();
//        $browser = new sfBrowser();
//        $browser->post('/incomingCalls/create', $postParams);
//        $newCredit = $user->getProfile()->getCredit();
//        $this->assertTrue($credit > $newCredit);
//    }
//
//}
