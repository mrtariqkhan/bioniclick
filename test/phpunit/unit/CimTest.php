<?php

/**
 * Description of CimTest
 *
 * @author fairdev
 */
class CimTest extends sfBasePhpunitTestCase
    implements sfPhpunitFixtureDoctrineAggregator {

    public function testCreateCustomerProfile() {

        $cim = AuthorizeNet::getCIM();
        $uniquePrefix = sfConfig::get('my_custom_unique_profile_prefix');
        $customerProfile = new FairDev_Payment_AuthorizeNet_CustomerProfile();
        $customerProfile->setMerchantCustomerId($uniquePrefix + 'TM');
        try {
            $cim->createCustomerProfile($customerProfile);
        } catch (FairDev_Payment_AuthorizeNet_CIMAPIException $e) {
            if ($e->getCode() == FairDev_Payment_AuthorizeNet_CIMAPIException::DUPLICATE_ENTRY) {
                $cim->deleteCustomerProfile($e->getDuplicateEntry());
                $cim->createCustomerProfile($customerProfile);
            } else {
                $this->fail('Unexpected exception: ' . $e->getMessage());
            }
        }
        $customerProfileId = $customerProfile->getId();
        $this->assertTrue(is_numeric($customerProfileId));
        return $customerProfileId;
    }

     /**
     * @depends testCreateCustomerProfile
     */
    public function testGetCustomerProfile($customerProfileId) {

        $cim = AuthorizeNet::getCIM();
        try {
            $profile = $cim->getCustomerProfile($customerProfileId);
        } catch (FairDev_Payment_AuthorizeNet_CIMAPIException $e) {
            $this->fail('Unexpected exception: ' . $e->getMessage());
        }
        $this->assertTrue(!empty($profile));
    }

    /**
     * @depends testCreateCustomerProfile
     */
    public function testCreateCustomerPaymentProfile($customerProfileId) {

        $paymentProfile = $this->preparePaymentProfile();
        $cim = AuthorizeNet::getCIM();
        try {
            $cim->createCustomerPaymentProfile($customerProfileId, $paymentProfile);
        } catch (FairDev_Payment_AuthorizeNet_CIMAPIException $e) {
            $this->fail('Unexpected exception: ' . $e->getMessage());
        }
        $paymentProfileId = $paymentProfile->getId();
        $this->assertTrue(is_numeric($customerProfileId));
        return array($customerProfileId, $paymentProfileId);
    }

    /**
     * @depends testCreateCustomerPaymentProfile
     */
    public function testGetCustomerPaymentProfile(array $data) {

        $cim = AuthorizeNet::getCIM();
        try {
            $customerPaymentProfile = $cim->getCustomerPaymentProfile($data[0], $data[1]);
        } catch (FairDev_Payment_AuthorizeNet_CIMAPIException $e) {
            $this->fail('Unexpected exception: ' . $e->getMessage());
        }
        $this->assertTrue(!empty($customerPaymentProfile));
        $billTo = $customerPaymentProfile->getBillTo();
        $this->assertEquals($billTo->getFirstName(), 'Matt');
        $this->assertEquals($billTo->getLastName(), 'Becker');
        $this->assertEquals($billTo->getAddress(), '12 Abbey Road');
        $this->assertEquals($billTo->getZip(), '12345');
        $creditCard = $customerPaymentProfile->getPayment()->getCreditCard();
        $this->assertEquals($creditCard->getCardNumber(), 'XXXX0027');
        $this->assertEquals($creditCard->getExpirationDate(), 'XXXX');
        return $data;
    }

    /**
     * @depends testGetCustomerPaymentProfile
     */
    public function testUpdateCustomerPaymentProfile(array $data) {

        $cim = AuthorizeNet::getCIM();
        $paymentProfile = $this->preparePaymentProfile($data[1], 'Matthew');
        try {
            $cim->updateCustomerPaymentProfile($data[0], $paymentProfile);
        } catch (FairDev_Payment_AuthorizeNet_CIMAPIException $e) {
            $this->fail('Unexpected exception: ' . $e->getMessage());
        }
        $customerPaymentProfile = $cim->getCustomerPaymentProfile($data[0], $data[1]);
        $this->assertTrue(!empty($customerPaymentProfile));
        $billTo = $customerPaymentProfile->getBillTo();
        $this->assertEquals($billTo->getFirstName(), 'Matthew');
    }

    /**
     * @depends testCreateCustomerPaymentProfile
     */
    public function testCreateCustomerProfileTransaction(array $data) {

        $cim = AuthorizeNet::getCIM();
        $transaction = new FairDev_Payment_AuthorizeNet_ProfileTransaction();
        $transaction
            ->setAmount(0.05)
            ->setCardCode('123')
            ->setCustomerPaymentProfileId($data[1])
            ->setCustomerProfileId($data[0])
        ;
        try {
            $cim->createCustomerProfileTransaction($transaction);
        } catch (FairDev_Payment_AuthorizeNet_CIMAPIException $e) {
            $this->fail('Unexpected exception: ' . $e->getMessage());
        }
    }

    /**
     * @depends testCreateCustomerPaymentProfile
     */
    public function testDeleteCustomerPaymentProfile(array $data) {

        $cim = AuthorizeNet::getCIM();
        try {
            $cim->deleteCustomerPaymentProfile($data[0], $data[1]);
        } catch (FairDev_Payment_AuthorizeNet_CIMAPIException $e) {
            $this->fail('Unexpected exception: ' . $e->getMessage());
        }
    }

    /**
     * @depends testCreateCustomerProfile
     */
    public function testDeletePaymentProfile($customerProfileId) {

        $cim = AuthorizeNet::getCIM();
        try {
            $cim->deleteCustomerProfile($customerProfileId);
        } catch (FairDev_Payment_AuthorizeNet_CIMAPIException $e) {
            $this->fail('Unexpected exception');
        }
    }

    protected function preparePaymentProfile(
        $paymentProfileId = 0,
        $firstName = 'Matt',
        $lastName = 'Becker',
        $address = '12 Abbey Road',
        $zip = '12345',
        $cardNumber = '4007000000027',
        $expirationDate = '2012-12'
    ) {

        $billTo = new FairDev_Payment_AuthorizeNet_CustomerAddress();
        $billTo
            ->setFirstName($firstName)
            ->setLastName($lastName)
            ->setAddress($address)
            ->setZip($zip);

        $creditCard = new FairDev_Payment_AuthorizeNet_CreditCard();
        $creditCard
            ->setCardNumber($cardNumber)
            ->setExpirationDate($expirationDate)
        ;

        $payment = new FairDev_Payment_AuthorizeNet_Payment();
        $payment->setCreditCard($creditCard);

        $paymentProfile = new FairDev_Payment_AuthorizeNet_CustomerPaymentProfile();
        $paymentProfile
            ->setBillTo($billTo)
            ->setPayment($payment)
            ->setId($paymentProfileId)
        ;
        return $paymentProfile;
    }

}
