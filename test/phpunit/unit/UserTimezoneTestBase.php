<?php

/**
 * Description of UserTimezoneTestBase
 * 
 * @author fairdev
 */
abstract class UserTimezoneTestBase extends sfBasePhpunitTestCase
    implements sfPhpunitFixtureDoctrineAggregator {

    const TIMEZONE_OFFSET_MINUS_5           = '-0500';
    const TIMEZONE_OFFSET_MINUS_2_AND_A_HALF= '-0230';
    const TIMEZONE_OFFSET_PLUS_6            = '+0600';
    const TIMEZONE_OFFSET_GMT               = '+0000';

    const TIMEZONE_ID_MINUS_5               = 1;
    const TIMEZONE_ID_MINUS_2_AND_A_HALF    = 2;
    const TIMEZONE_ID_PLUS_6                = 3;
    const TIMEZONE_ID_GMT                   = 4;

    protected $user         = null;
    protected $timezoneName = '';
    protected $userKind     = '';
    protected $gmtDifference= '';


    protected function _start() {

        parent::_start();

        $this->configureUserWithTz();
    }

    abstract protected function configureUserWithTz();

    protected function findUserSuperAdmin() {

        $user = sfGuardUserTable::getInstance()->findOneBy('is_super_admin', true);
        if (empty($user) || !$user->exists()) {
            throw new Exception('There is no need sfGuardUser in DB');
        }

        return $user;
    }

    /**
     *
     * @param type $tz one of self::TIMEZONE_ID_*
     * @return string offset
     */
    public static function getTzOffset($tzId) {

        switch ($tzId) {
            case self::TIMEZONE_ID_MINUS_5:
                return self::TIMEZONE_OFFSET_MINUS_5;
                break;
            case self::TIMEZONE_ID_MINUS_2_AND_A_HALF:
                return self::TIMEZONE_OFFSET_MINUS_2_AND_A_HALF;
                break;
            case self::TIMEZONE_ID_PLUS_6:
                return self::TIMEZONE_OFFSET_PLUS_6;
                break;
            default:
                return self::TIMEZONE_OFFSET_GMT;
        }
    }

    /**
     *
     * @param int $companyId one of self::ID_COMPANY_* or null if Advertiser
     * @param int $companyId one of self::ID_ADVERTISER_* or null if Company
     * @param int $tzId one of self::TIMEZONE_ID_* or null to use user's tz as is in DB
     */
    protected function configureCompanyTreeUserWithTz($companyId, $advertiserId, $tzId = null) {

        $companyKind = '';
        $company = null;
        if (!is_null($companyId)) {
            $company = CompanyTable::getInstance()->find($companyId);
            $companyKind = 'company';
        } elseif (!is_null($advertiserId)) {
            $company = AdvertiserTable::getInstance()->find($advertiserId);
            $companyKind = 'advertiser';
        }
        if (empty($company) || !$company->exists()) {
            throw new Exception('There is no need Company(Advertiser) in DB');
        }

        $user = $company->getAdmin();
        if (empty($user) || !$user->exists()) {
            throw new Exception('There is no need Admin in DB');
        }


        $gmtDifference = self::getTzOffset($tzId);
        $this->setGmtDifference($gmtDifference);

        $this->setUser($user);
        $this->setUserKind("Admin of '$company' $companyKind");
        $this->setTimezoneName($gmtDifference);
    }

    protected function configureSuperAdminAsCompanyTreeUserWithTz($tzId = null) {

        $user = $this->findUserSuperAdmin();

        $gmtDifference = self::getTzOffset($tzId);
        $this->setGmtDifference($gmtDifference);

        $this->setUser($user);
        $this->setUserKind("Super Admin");
        $this->setTimezoneName($gmtDifference);
        
    }

//    // NOTE:: is unnecessary now because classes to test have been modified 
//    //  not to use user, but timezone instead of it
//
//    /**
//     *
//     * @param sfGuardUser $userMock
//     * @param type $tz one of self::TIMEZONE_ID_*
//     * @return sfGuardUser mock object 
//     */
//    protected function createSfGuardUserMock($user, $tzId) {
//
//        $offset = self::getTzOffset($tzId);
//
//        // NOTE:: Create a Mock Object for the sfGuardUser class mocking need methods.
//        $userMock = $this->getMock(
//            'sfGuardUser', 
//            array(
//                'findGmtDifference',
//                'findTimezoneName',
//                'getFirstCompany',
//                'getFirstCompanyId',
//                'exists',
//            )
//        );
// 
//        // NOTE:: Set up the expectations for need methods
//        $userMock
//            ->expects($this->any())
//            ->method('findGmtDifference')
//            ->will($this->returnValue($offset))
//        ;
//        $userMock
//            ->expects($this->any())
//            ->method('findTimezoneName')
//            ->will($this->returnValue($offset))
//        ;
//        $userMock
//            ->expects($this->any())
//            ->method('getFirstCompany')
//            ->will($this->returnValue($user->getFirstCompany()))
//        ;
//        $userMock
//            ->expects($this->any())
//            ->method('getFirstCompanyId')
//            ->will($this->returnValue($user->getFirstCompanyId()))
//        ;
//        $userMock
//            ->expects($this->any())
//            ->method('exists')
//            ->will($this->returnValue($user->exists()))
//        ;
//
//        // NOTE:: make "copy" of need sfGuardUser object
//        $userMock->setIsSuperAdmin(     $user->getIsSuperAdmin());
//        $userMock->setIsActive(         $user->getIsActive());
//        $userMock->setFirstName(        $user->getFirstName());
//        $userMock->setLastName(         $user->getLastName());
//        $userMock->setAlgorithm(        $user->getAlgorithm());
//        $userMock->setPassword(         $user->getPassword());
//        $userMock->setUsername(         $user->getUsername());
//        $userMock->setEmailAddress(     $user->getEmailAddress());
//        $userMock->setForgotPassword(   $user->getForgotPassword());
//        $userMock->setWasDeleted(       $user->getWasDeleted());
//        $userMock->setSalt(             $user->getSalt());
//        $userMock->setProfile(          $user->getProfile());
//        $userMock->setLastLogin(        $user->getLastLogin());
//        $userMock->setGroups(           $user->getGroups());
//        $userMock->setPermissions(      $user->getPermissions());
//        $userMock->setRememberKeys(     $user->getRememberKeys());
//
//        return $userMock;
//    }
//
//    // NOTE:: is unnecessary now because classes to test have been modified 
//    //  not to use user, but timezone instead of it
//
//    /**
//     *
//     * @param sfGuardUser $user
//     * @param int $tzId one of self::TIMEZONE_ID_*
//     * @return MOCK object of sfGuardUser with need timezone's offset
//     */
//    protected function setTimezoneToUser($user, $tzId) {
//
//        $profile = $user->getProfile();
//        if (empty($profile) || !$profile->exists()) {
//            throw new Exception('There is no need sfGuardUserProfile in DB');
//        }
//
//        return $this->createSfGuardUserMock($user, $tzId);
//    }

//    //NOTE:: is unnecessary because Mock object is in use. Timezone's offset
//    // is important, timezone's name is not important
//    protected function findTimezone($id) {
//
//        $tz = TimezoneTable::getInstance()->find($id);
//        if (empty($tz) || !$tz->exists()) {
//            throw new Exception('There is no need Timezone in DB');
//        }
//
//        return $tz;
//    }
//
//    //NOTE:: is unnecessary because Mock object is in use. Timezone's offset
//    // is important, timezone's name is not important
//    protected function setTimezoneToUser($user, $tz) {
//
//        $profile = $user->getProfile();
//        if (empty($profile) || !$profile->exists()) {
//            throw new Exception('There is no need sfGuardUserProfile in DB');
//        }
//
//        $profile->setTimezone($tz);
//        $user->save();
//
//        return $user;
//        
//    }


    public function setUser($user) {
        $this->user = $user;
    }

    public function setUserKind($userKind) {
        $this->userKind = $userKind;
    }

    public function setTimezoneName($timezoneName) {
        $this->timezoneName = $timezoneName;
    }

    public function setGmtDifference($gmtDifference) {
        $this->gmtDifference = $gmtDifference;
    }


    public function getUser() {
        return $this->user;
    }

    public function getUserKind() {
        return $this->userKind;
    }

    public function getTimezoneName() {
        return $this->timezoneName;
    }

    public function getGmtDifference() {
        return $this->gmtDifference;
    }
}
