<?php

/**
 * Description of IncomingCallHandlerTest
 *
 * @author konstantin
 */
//class IncomingCallHandlerTest extends sfBasePhpunitTestCase
//    implements sfPhpunitFixtureDoctrineAggregator {
//
//    //TODO:: Rewrite this test to make it actual
//
//    protected static $campaignApiKey = '5ae1ea640a985ada6e4654a001350d78';
//    protected static $sid1 = 'PN8c28e516087f2c5d19ff4d32765707c2';
//    protected static $sid2 = 'PN3fd77db4e955f19157e2c2a24127a3c9';
//    protected static $sid3 = 'PN8b06d776d7f5460e7e0ec1aff33eb304';
//
//    public function testGenerateTwiMlByCampaignWithoutCampaign() {
//
//        //$incomingNumber without campaign
//        $incomingNumber = TwilioIncomingPhoneNumberTable::getInstance()->findBySid(self::$sid1);
//        $result = IncomingCallHandler::generateTwiMlByCampaign($incomingNumber->getId());
//        $this->assertEquals(
//            '',
//            $result
//        );
//    }
//
//    public function testGenerateTwiMlByCampaignForNotDefault() {
//
//        //$incomingNumber with campaign but isn't default
//        $incomingNumber = TwilioIncomingPhoneNumberTable::getInstance()->findBySid(self::$sid2);
//        $result = IncomingCallHandler::generateTwiMlByCampaign($incomingNumber->getId());
//        $this->assertEquals(
//            '<Dial timeout="120"><Number>8323278675</Number></Dial>',
//            $result
//        );
//    }
//
//    public function testGenerateTwiMlByCampaignWithRedirectMessage() {
//
//        //with redirect message
//        $incomingNumber = TwilioIncomingPhoneNumberTable::getInstance()->findBySid(self::$sid2);
//        $campaign = CampaignTable::findByApiKey(self::$campaignApiKey);
//        $campaign->setRedirectMessage(true);
//        $campaign->save();
//        $result = IncomingCallHandler::generateTwiMlByCampaign($incomingNumber->getId());
//        $this->assertEquals(
//            '<Play>/audio/redirect_message.mp3</Play><Dial timeout="120"><Number>8323278675</Number></Dial>',
//            $result
//        );
//        $campaign->setRedirectMessage(false);
//        $campaign->save();
//    }
//
//    public function testGenerateTwiMlByCampaignWithRecord() {
//
//        //with record
//        $incomingNumber = TwilioIncomingPhoneNumberTable::getInstance()->findBySid(self::$sid2);
//        $campaign = CampaignTable::findByApiKey(self::$campaignApiKey);
//        $campaign->setIsRecordingsEnabled(true);
//        $campaign->save();
//        $result = IncomingCallHandler::generateTwiMlByCampaign($incomingNumber->getId());
//        $this->assertEquals(
//            '<Dial timeout="120" record="true"><Number>8323278675</Number></Dial>',
//            $result
//        );
//    }
//
//    public function testGenerateTwiMlByCampaignForDefaultNumberWithRecord() {
//
//        //default $incomingNumber with campaign and recording
//        $incomingNumber = TwilioIncomingPhoneNumberTable::getInstance()->findBySid(self::$sid3);
//        $campaign = CampaignTable::findByApiKey(self::$campaignApiKey);
//        $campaign->setIsRecordingsEnabled(true);
//        $campaign->save();
//        $result = IncomingCallHandler::generateTwiMlByCampaign($incomingNumber->getId());
//        $this->assertEquals(
//            '<Say>Please leave your name, phone number and a brief message after the beep, and we will contact you as soon as possible</Say><Record/>',
//            $result
//        );
//    }
//
//    public function testGenerateTwiMlByCampaignForDefaultNumberWithoutRecord() {
//
//        //default $incomingNumber with campaign, but without recording
//        $incomingNumber = TwilioIncomingPhoneNumberTable::getInstance()->findBySid(self::$sid3);
//        $campaign = CampaignTable::findByApiKey(self::$campaignApiKey);
//        $campaign->setIsRecordingsEnabled(false);
//        $campaign->save();
//        $result = IncomingCallHandler::generateTwiMlByCampaign($incomingNumber->getId());
//        $this->assertEquals(
//            '<Say>We are sorry, called number is unavailable. We will contact you as soon as possible</Say>',
//            $result
//        );
//    }
//
//    public function testGenerateTwiMlByUserWithoutProfile() {
//
//        $incomingNumber = TwilioIncomingPhoneNumberTable::getInstance()->findBySid(self::$sid1);
//        //FIXME: Use advertiser's and company's default numbers
//        /*$result = IncomingCallHandler::generateTwiMlByUser($incomingNumber->getId());
//        $phoneNumber = sfConfig::get('app_default_phone_number');
//        $this->assertEquals(
//            "<Say>We are sorry, called number is unavailable. You are calling to the support staff now</Say><Dial timeout=\"120\" record=\"true\"><Number>$phoneNumber</Number></Dial>",
//            $result
//        );*/
//    }
//
//    public function testGenerateTwiMlByUser() {
//
//        $incomingNumber = TwilioIncomingPhoneNumberTable::getInstance()->findBySid(self::$sid2);
//        //FIXME: Use advertiser's and company's default numbers
//        /*$result = IncomingCallHandler::generateTwiMlByUser($incomingNumber->getId());
//        $this->assertEquals(
//            "<Say>We are sorry, called number is unavailable. You are calling to the support staff now</Say><Dial timeout=\"120\" record=\"true\"><Number>+18770000000</Number></Dial>",
//            $result
//        );*/
//    }
//}

