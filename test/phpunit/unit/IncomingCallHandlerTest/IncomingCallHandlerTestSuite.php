<?php

/**
 * Description of MyPhpunitTestSuite
 *
 * @author fairdev
 */
class IncomingCallHandlerTestSuite extends sfBasePhpunitTestSuite
    implements sfPhpunitContextInitilizerInterface, sfPhpunitFixtureDoctrineAggregator {

    /**
     * Dev hook for custom "setUp" stuff
     */
    protected function _start() {

        $this->_initFilters();
        $this
            ->fixture()
            ->clean()
            ->loadOwn('01_TwilioAccount.yml')
            ->loadOwn('03_TwilioLocalAreaCode.yml')
            ->loadOwn('04_TwilioIncomingPhoneNumber.yml')
            ->loadOwn('05_TwilioCity.yml')
            ->loadOwn('06_CompanyTree.yml')
            ->loadOwn('07_Timezone.yml')
            ->loadOwn('08_sfGuard.yml')
            ->loadOwn('09_Campaign.yml')
            ->loadOwn('10_AdvertiserIncomingNumberPool.yml')
            ->loadOwn('11_Page.yml')
            ->loadOwn('15_PhysicalPhoneNumber.yml')
            ->loadOwn('19_PhoneNumberAssignment.yml')
            ->loadOwn('20_PhoneNumberNoncompleteAssignment.yml')
        ;
    }

    protected function _initFilters() {

        $filters = sfConfig::get('app_sfPhpunitPlugin_filter', array());
        foreach ($filters as $filter) {
            PHPUnit_Util_Filter::addDirectoryToFilter($filter['path'], $filter['ext']);
        }
    }

    public function getApplication() {
        return 'backend';
    }

    public function getOwnFixtureDir() {

        $sep = DIRECTORY_SEPARATOR;
        $reflection = new ReflectionClass($this);
        $path = str_replace($sep . get_class($this) . '.php', '', $reflection->getFileName());

        $ownDir = str_replace(
            "{$sep}test{$sep}phpunit",
            "{$sep}test{$sep}phpunit{$sep}fixtures",
            $path
        );

        return $ownDir;
    }

    protected function _initFixture(array $options = array()) {

        $options = array(
            'fixture_ext' => ''
        );
        return parent::_initFixture($options);
    }

}

