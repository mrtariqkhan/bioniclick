<?php
//TODO:: write it later. It needs to test CallLogs for every type of users.
//
//require_once dirname(__FILE__) . '/TicIndexHelperCreateIndexQueryBaseCompanyTree.php';
//
///**
// * Description of TicIndexHelperCreateIndexQueryCompanyTreeAdvertiserOfBionicTest
// * 
// * @author fairdev
// */
//class TicIndexHelperCreateIndexQueryCompanyTreeAdvertiserOfBionicTest
//    extends TicIndexHelperCreateIndexQueryBaseCompanyTree {
//
//    protected function configureFilterValuesAndResults() {
//        
//        $this->setCampaignIds(                              self::$CAMPAIGN_IDS);
//        $this->setAvailableSecondsBetweenAnalyticsAndCalls( self::$AVAILABLE_SECONDS_BETWEEN_ANALYTICS_AND_CALL_DAYS);
//        $this->setFilterValues(                             self::$FILTER_VALUES);
//        $this->setExpectedResults(                          self::$EXPECTED_RESULTS);
//    }
//
//    protected function configureUserWithTz() {
//        $this->configureCompanyTreeUserWithTz(
//            null,
//            self::ID_ADVERTISER_BIONIC_ADVERTISER,
//            self::TIMEZONE_ID_GMT
//        );
//    }
//
//
//    protected static $CAMPAIGN_IDS = array (
//        self::TYPE__ALL_TIME__AVAILABLE_DAY__FOR_ALL_CAMPAIGNS  => self::ID_CAMPAIGN_NO,
//        self::TYPE__ALL_TIME__AVAILABLE_DAY__FOR_CAMPAIGN       => self::ID_CAMPAIGN_OF_ADVERTISER_BIONIC_ADVERTISER,
//        self::TYPE__ALL_TIME__AVAILABLE_HOUR__FOR_ALL_CAMPAIGNS => self::ID_CAMPAIGN_NO,
//        self::TYPE__ALL_TIME__AVAILABLE_HOUR__FOR_CAMPAIGN      => self::ID_CAMPAIGN_OF_ADVERTISER_BIONIC_ADVERTISER,
//        self::TYPE__ALL_TIME__AVAILABLE_YEAR__FOR_ALL_CAMPAIGNS => self::ID_CAMPAIGN_NO,
//        self::TYPE__ALL_TIME__AVAILABLE_YEAR__FOR_CAMPAIGN      => self::ID_CAMPAIGN_OF_ADVERTISER_BIONIC_ADVERTISER,
//    );
//
//
//    protected static $AVAILABLE_SECONDS_BETWEEN_ANALYTICS_AND_CALL_DAYS = array (
//        self::TYPE__ALL_TIME__AVAILABLE_DAY__FOR_ALL_CAMPAIGNS  => self::AVAILABLE_SECONDS_BETWEEN_ANALYTICS_AND_CALL_DAY,
//        self::TYPE__ALL_TIME__AVAILABLE_DAY__FOR_CAMPAIGN       => self::AVAILABLE_SECONDS_BETWEEN_ANALYTICS_AND_CALL_DAY,
//        self::TYPE__ALL_TIME__AVAILABLE_HOUR__FOR_ALL_CAMPAIGNS => self::AVAILABLE_SECONDS_BETWEEN_ANALYTICS_AND_CALL_HOUR,
//        self::TYPE__ALL_TIME__AVAILABLE_HOUR__FOR_CAMPAIGN      => self::AVAILABLE_SECONDS_BETWEEN_ANALYTICS_AND_CALL_HOUR,
//        self::TYPE__ALL_TIME__AVAILABLE_YEAR__FOR_ALL_CAMPAIGNS => self::AVAILABLE_SECONDS_BETWEEN_ANALYTICS_AND_CALL_YEAR,
//        self::TYPE__ALL_TIME__AVAILABLE_YEAR__FOR_CAMPAIGN      => self::AVAILABLE_SECONDS_BETWEEN_ANALYTICS_AND_CALL_YEAR,
//    );
//
//
//    protected static $FILTER_VALUES = array(
//        self::TYPE__ALL_TIME__AVAILABLE_DAY__FOR_ALL_CAMPAIGNS  => array(
//        ),
//        self::TYPE__ALL_TIME__AVAILABLE_DAY__FOR_CAMPAIGN       => array(
//        ),
//        self::TYPE__ALL_TIME__AVAILABLE_HOUR__FOR_ALL_CAMPAIGNS => array(
//        ),
//        self::TYPE__ALL_TIME__AVAILABLE_HOUR__FOR_CAMPAIGN      => array(
//        ),
//        self::TYPE__ALL_TIME__AVAILABLE_YEAR__FOR_ALL_CAMPAIGNS => array(
//        ),
//        self::TYPE__ALL_TIME__AVAILABLE_YEAR__FOR_CAMPAIGN      => array(
//        ),
//    );
//
//
//    protected static $EXPECTED_RESULTS = array(
//        self::TYPE__ALL_TIME__AVAILABLE_DAY__FOR_ALL_CAMPAIGNS  => array(
//        ),
//        self::TYPE__ALL_TIME__AVAILABLE_DAY__FOR_CAMPAIGN       => array(
//        ),
//        self::TYPE__ALL_TIME__AVAILABLE_HOUR__FOR_ALL_CAMPAIGNS => array(
//        ),
//        self::TYPE__ALL_TIME__AVAILABLE_HOUR__FOR_CAMPAIGN      => array(
//        ),
//        self::TYPE__ALL_TIME__AVAILABLE_YEAR__FOR_ALL_CAMPAIGNS => array(
//        ),
//        self::TYPE__ALL_TIME__AVAILABLE_YEAR__FOR_CAMPAIGN      => array(
//        ),
//    );
//}
