<?php

require_once dirname(__FILE__) . '/TicIndexHelperCreateIndexQueryBaseProvided.php';

/**
 * Description of TicIndexHelperCreateIndexQueryBaseCompanyTree
 * 
 * @author fairdev
 */
abstract class TicIndexHelperCreateIndexQueryBaseCompanyTree extends TicIndexHelperCreateIndexQueryBaseProvided {


    //TODO:: write tests later

    //TODO:: add other types
    const TYPE__ALL_TIME__AVAILABLE_DAY__FOR_ALL_CAMPAIGNS  = 'All time  Available Day   For All Campaigns';
    const TYPE__ALL_TIME__AVAILABLE_DAY__FOR_CAMPAIGN       = 'All time  Available Day   For One Campaign';
    const TYPE__ALL_TIME__AVAILABLE_HOUR__FOR_ALL_CAMPAIGNS = 'All time  Available Hour  For All Campaigns';
    const TYPE__ALL_TIME__AVAILABLE_HOUR__FOR_CAMPAIGN      = 'All time  Available Hour  For One Campaign';
    const TYPE__ALL_TIME__AVAILABLE_YEAR__FOR_ALL_CAMPAIGNS = 'All time  Available Year  For All Campaigns';
    const TYPE__ALL_TIME__AVAILABLE_YEAR__FOR_CAMPAIGN      = 'All time  Available Year  For One Campaign';


    const ID_COMPANY_BIONIC                     = 1;
    const ID_COMPANY_RESELLER_ACQUISIO          = 2;
    const ID_COMPANY_AGENCY_REALSEARCH          = 4;

    const ID_ADVERTISER_BIONIC_ADVERTISER       = 1;
    const ID_ADVERTISER_RESELLER_ACQUISIO_TRIPS = 3;
    const ID_ADVERTISER_AGENCY_REALSEARCH_IPS   = 4;


    const ID_CAMPAIGN_NO                                    = NULL;
    const ID_CAMPAIGN_OF_ADVERTISER_BIONIC_ADVERTISER       = 0;
    const ID_CAMPAIGN_OF_RESELLER_ACQUISIO_TRIPS            = 0;
    const ID_CAMPAIGN_OF_ADVERTISER_AGENCY_REALSEARCH_IPS   = 0;


    const AVAILABLE_SECONDS_BETWEEN_ANALYTICS_AND_CALL_YEAR = 31536000;
    const AVAILABLE_SECONDS_BETWEEN_ANALYTICS_AND_CALL_DAY  = 86400;
    const AVAILABLE_SECONDS_BETWEEN_ANALYTICS_AND_CALL_HOUR = 3600;


//    /**
//     *
//     * @dataProvider providerCases
//     * 
//     * @param string $testKey one of QueryBuilderCallStatusTz4Test::TYPE_*
//     * @param string $specificAdditionalMessage
//     */
//    public function testCase($testKey, $specificAdditionalMessage = '') {
//
//        $campaignId                                 = $this->getCampaignId($testKey);
//        $availableSecondsBetweenAnalyticsAndCall    = $this->getAvailableSecondsBetweenAnalyticsAndCall($testKey);
//        $filterValues                               = $this->getFilterValue($testKey);
//        $expectedResult                             = $this->getExpectedResult($testKey);
//
//        $actualResult = $this->getActualResult(
//            $campaignId, 
//            $availableSecondsBetweenAnalyticsAndCall, 
//            $filterValues
//        );
//
//        $this->assertArrayAreSame(
//            $expectedResult, 
//            $actualResult, 
//<<<EOD
//    Error in Test ($testKey)
//        (an attempt to find CallLogs for '{$this->userKind}' user with timezone '{$this->timezoneName}').
//        $specificAdditionalMessage
//EOD
//        );
//    }
//
//    public function providerCases() {
//        return array(
//            'case All 01' => array(self::TYPE__ALL_TIME__AVAILABLE_DAY__FOR_ALL_CAMPAIGNS),
//            'case All 02' => array(self::TYPE__ALL_TIME__AVAILABLE_DAY__FOR_CAMPAIGN),
//            'case All 03' => array(self::TYPE__ALL_TIME__AVAILABLE_HOUR__FOR_ALL_CAMPAIGNS),
//            'case All 04' => array(self::TYPE__ALL_TIME__AVAILABLE_HOUR__FOR_CAMPAIGN),
//            'case All 05' => array(self::TYPE__ALL_TIME__AVAILABLE_YEAR__FOR_ALL_CAMPAIGNS),
//            'case All 06' => array(self::TYPE__ALL_TIME__AVAILABLE_YEAR__FOR_CAMPAIGN),
//        );
//    }
}
