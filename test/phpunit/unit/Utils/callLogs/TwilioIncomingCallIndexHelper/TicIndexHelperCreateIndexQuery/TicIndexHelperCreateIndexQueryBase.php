<?php

require_once dirname(__FILE__) . '/../../../../UserTimezoneTestBase.php';

/**
 * Description of TicIndexHelperCreateIndexQueryBase
 * 
 * @author fairdev
 */
abstract class TicIndexHelperCreateIndexQueryBase extends UserTimezoneTestBase {


    public function assertArrayAreSame($exceptedArray, array $actualArray, $message) {

        $exceptedArray = empty($exceptedArray) ? array() : $exceptedArray;

        var_dump($exceptedArray);
        var_dump($actualArray);

////        var_dump($resultArray);
//        foreach ($exceptedArray as $needValueArray) {
//
//            $ids = $needValueArray['id'];
//            if (!is_array($ids)) {
//                $ids = array($ids);
//            }
//            $needValueArray['id'] = $ids;
//
//            $needle = $needValueArray;
//            $contains = false;
//            foreach ($ids as $id) {
//                $needle['id'] = $id;
//                $contains = !(array_search($needle, $actualArray) === false);
//                if ($contains) {
//                    break;
//                }
//            }
//            if (!$contains) {
//                $needValueArrayStr = $this->getNeedValueArrayString($needValueArray);
//                $this->fail("$message Actual array has no: $needValueArrayStr.");
//            }
//        }
//
//        $this->assertEquals(
//            count($exceptedArray), 
//            count($actualArray), 
//            $message
//        );
    }

    protected function getActualResult(
        $campaignId,
        $availableSecondsBetweenAnalyticsAndCall,
        $filterValues, 
        $maxPerPage = 'all',
        $firstIndex = 1
    ) {
//var_dump($campaignId);
//var_dump($availableSecondsBetweenAnalyticsAndCall);
//var_dump($filterValues);
//var_dump($maxPerPage);
//var_dump($firstIndex);
        $user = $this->getUser();

        $twilioIncomingCallIndexHelper = new TwilioIncomingCallIndexHelper(
            $campaignId, 
            $user, 
            $availableSecondsBetweenAnalyticsAndCall
        );
        $query = $twilioIncomingCallIndexHelper->createIndexQuery(
            $filterValues,
            $maxPerPage,
            $firstIndex
        );
//echoln('-------------------');
//echo $query->getSqlQuery();
//echoln('-------------------');
        $actualResult = $query->execute()->toArray();
//var_dump($actualResult);
//echoln('-------------------');
//echoln('-------------------');
//die;
        return $actualResult;
    }
}
