<?php

require_once dirname(__FILE__) . '/TicIndexHelperCreateIndexQueryBase.php';

/**
 * Description of QueryBuilderCallStatusBaseTz
 * 
 * @author fairdev
 */
abstract class TicIndexHelperCreateIndexQueryBaseProvided extends TicIndexHelperCreateIndexQueryBase {

    /**
     * All of them are: array($testType => value)
     */
    protected $campaignIds                              = null;
    protected $availableSecondsBetweenAnalyticsAndCalls = null;
    protected $filterValues                             = array();
    protected $expectedResults                          = array();


    protected function _start() {

        parent::_start();

        $this->configureFilterValuesAndResults();
    }

    abstract protected function configureFilterValuesAndResults();


    public function getCampaignId($testKey) {

        $values = $this->getCampaignIds();
        return self::getArrayValue($values, $testKey);
    }

    public function getAvailableSecondsBetweenAnalyticsAndCall($testKey) {

        $values = $this->getAvailableSecondsBetweenAnalyticsAndCalls();
        return self::getArrayValue($values, $testKey);
    }

    public function getFilterValue($testKey) {

        $values = $this->getFilterValues();
        return self::getArrayValue($values, $testKey);
    }

    public function getExpectedResult($testKey) {

        $values = $this->getExpectedResults();
        return self::getArrayValue($values, $testKey);
    }


    protected static function getArrayValue($searchArray, $key) {

        if (!is_array($searchArray)) {
            throw new Excpetion('Array has been  expected.');
        }

        if (!array_key_exists($key, $searchArray)) {
            throw new Excpetion('Array has no such key.');
        }

        return $searchArray[$key];
    }


    public function setCampaignIds($campaignIds) {
        $this->campaignIds = $campaignIds;
    }

    public function setAvailableSecondsBetweenAnalyticsAndCalls($availableSecondsBetweenAnalyticsAndCalls) {
        $this->availableSecondsBetweenAnalyticsAndCalls = $availableSecondsBetweenAnalyticsAndCalls;
    }

    public function setFilterValues(array $filterValues) {
        $this->filterValues = $filterValues;
    }

    public function setExpectedResults(array $expectedResults) {
        $this->expectedResults = $expectedResults;
    }



    public function getCampaignIds() {
        return $this->campaignIds;
    }

    public function getAvailableSecondsBetweenAnalyticsAndCalls() {
        return $this->availableSecondsBetweenAnalyticsAndCalls;
    }

    public function getFilterValues() {
        return $this->filterValues;
    }

    public function getExpectedResults() {
        return $this->expectedResults;
    }
}
