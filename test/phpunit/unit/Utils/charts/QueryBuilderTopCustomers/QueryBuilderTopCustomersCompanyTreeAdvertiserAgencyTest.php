<?php

require_once dirname(__FILE__) . '/QueryBuilderTopCustomersBaseCompanyTree.php';

/**
 * Description of QueryBuilderTopCustomersCompanyTreeAdvertiserAgencyTest
 * 
 * @author fairdev
 */
class QueryBuilderTopCustomersCompanyTreeAdvertiserAgencyTest extends QueryBuilderTopCustomersBaseCompanyTree {

    
    protected function configureFilterValuesAndResults() {

        $this->setFilterValues(self::$FILTER_VALUES);
        $this->setExpectedResults(self::$RESULTS);
    }

    protected function configureUserWithTz() {
        $this->configureCompanyTreeUserWithTz(
            null,
            self::ID_ADVERTISER_AGENCY_REALSEARCH_IPS,
            self::TIMEZONE_ID_GMT
        );
    }

    protected static $FILTER_VALUES = array(
        self::TYPE__TIME_RANGE_00__KIND_ALL => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_ALL,
            self::FILTER_PARAMETER_FROM => '1323000001',
            self::FILTER_PARAMETER_TO   => '1323000015',
        ),
        self::TYPE__TIME_RANGE_00__KIND_WEB_PAGES => array(
            self::FILTER_PARAMETER_FROM => '1323000033',
            self::FILTER_PARAMETER_TO   => '1323000055',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_WEB_PAGES,
        ),
        self::TYPE__TIME_RANGE_00__KIND_YELLOW_PAGES => array(
            self::FILTER_PARAMETER_FROM => '1323000099',
            self::FILTER_PARAMETER_TO   => '1323000111',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_YELLOW_PAGES,
        ),
        self::TYPE__TIME_RANGE_00__KIND_BILLBOARDS => array(
            self::FILTER_PARAMETER_FROM => '1323000018',
            self::FILTER_PARAMETER_TO   => '1323000034',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_BILLBOARDS,
        ),
        self::TYPE__TIME_RANGE_00__KIND_EMAIL => array(
            self::FILTER_PARAMETER_FROM => '1323000084',
            self::FILTER_PARAMETER_TO   => '1323000097',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_EMAIL,
        ),
        self::TYPE__TIME_RANGE_00__KIND_OTHER => array(
            self::FILTER_PARAMETER_FROM => '1323000118',
            self::FILTER_PARAMETER_TO   => '1323000119',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_OTHER,
        ),

        self::TYPE__TIME_RANGE_01__KIND_ALL => array(
            self::FILTER_PARAMETER_FROM => '1323000118',
            self::FILTER_PARAMETER_TO   => '1323000129',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_ALL,
        ),
//        self::TYPE__TIME_RANGE_01__KIND_WEB_PAGES => array(),
        self::TYPE__TIME_RANGE_01__KIND_YELLOW_PAGES => array(
            self::FILTER_PARAMETER_FROM => '1323000121',
            self::FILTER_PARAMETER_TO   => '1323000129',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_YELLOW_PAGES,
        ),
        self::TYPE__TIME_RANGE_01__KIND_BILLBOARDS => array(
            self::FILTER_PARAMETER_FROM => '1323000061',
            self::FILTER_PARAMETER_TO   => '1323000073',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_BILLBOARDS,
        ),
//        self::TYPE__TIME_RANGE_01__KIND_EMAIL => array(),
//        self::TYPE__TIME_RANGE_01__KIND_OTHER => array(),

        self::TYPE__TIME_RANGE_02__KIND_ALL => array(
            self::FILTER_PARAMETER_FROM => '1323000070',
            self::FILTER_PARAMETER_TO   => '1323000122',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_ALL,
        ),
//        self::TYPE__TIME_RANGE_02__KIND_WEB_PAGES => array(),
//        self::TYPE__TIME_RANGE_02__KIND_YELLOW_PAGES => array(),
//        self::TYPE__TIME_RANGE_02__KIND_BILLBOARDS => array(),
//        self::TYPE__TIME_RANGE_02__KIND_EMAIL => array(),
//        self::TYPE__TIME_RANGE_02__KIND_OTHER => array(),

        self::TYPE__TIME_ALL__KIND_ALL => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_ALL,
        ),
        self::TYPE__TIME_ALL__KIND_WEB_PAGES => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_WEB_PAGES,
        ),
        self::TYPE__TIME_ALL__KIND_YELLOW_PAGES => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_YELLOW_PAGES,
        ),
        self::TYPE__TIME_ALL__KIND_BILLBOARDS => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_BILLBOARDS,
        ),
        self::TYPE__TIME_ALL__KIND_EMAIL => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_EMAIL,
        ),
        self::TYPE__TIME_ALL__KIND_OTHER => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_OTHER,
        ),
    );


    protected static $RESULTS = array(
        self::TYPE__TIME_RANGE_00__KIND_ALL => array(),
        self::TYPE__TIME_RANGE_00__KIND_WEB_PAGES => array(),
        self::TYPE__TIME_RANGE_00__KIND_YELLOW_PAGES => array(),
        self::TYPE__TIME_RANGE_00__KIND_BILLBOARDS => array(),
        self::TYPE__TIME_RANGE_00__KIND_EMAIL => array(),
        self::TYPE__TIME_RANGE_00__KIND_OTHER => array(),

        self::TYPE__TIME_RANGE_01__KIND_ALL => array(
            array(
                self::PARAMETER_COUNT           => 2,
                self::PARAMETER_NAME            => self::CAMPAIGN_NAME__ADVERTISER_AGENCY_REALSEARCH_IPS_02,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
        ),
//        self::TYPE__TIME_RANGE_01__KIND_WEB_PAGES => array(),
        self::TYPE__TIME_RANGE_01__KIND_YELLOW_PAGES => array(
            array(
                self::PARAMETER_COUNT           => 2,
                self::PARAMETER_NAME            => self::CAMPAIGN_NAME__ADVERTISER_AGENCY_REALSEARCH_IPS_02,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
        ),
        self::TYPE__TIME_RANGE_01__KIND_BILLBOARDS => array(
            array(
                self::PARAMETER_COUNT           => 5,
                self::PARAMETER_NAME            => self::CAMPAIGN_NAME__ADVERTISER_AGENCY_REALSEARCH_IPS_01,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
        ),
//        self::TYPE__TIME_RANGE_01__KIND_EMAIL => array(),
//        self::TYPE__TIME_RANGE_01__KIND_OTHER => array(),


        self::TYPE__TIME_RANGE_02__KIND_ALL => array(
            array(
                self::PARAMETER_COUNT           => 5,
                self::PARAMETER_NAME            => self::CUSTOMER_NAME__NONE,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
            array(
                self::PARAMETER_COUNT           => 3,
                self::PARAMETER_NAME            => self::CAMPAIGN_NAME__ADVERTISER_AGENCY_REALSEARCH_IPS_01,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
            array(
                self::PARAMETER_COUNT           => 1,
                self::PARAMETER_NAME            => self::CAMPAIGN_NAME__ADVERTISER_AGENCY_REALSEARCH_IPS_02,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
        ),
//        self::TYPE__TIME_RANGE_02__KIND_WEB_PAGES => array(),
//        self::TYPE__TIME_RANGE_02__KIND_YELLOW_PAGES => array(),
//        self::TYPE__TIME_RANGE_02__KIND_BILLBOARDS => array(),
//        self::TYPE__TIME_RANGE_02__KIND_EMAIL => array(),
//        self::TYPE__TIME_RANGE_02__KIND_OTHER => array(),


        self::TYPE__TIME_ALL__KIND_ALL => array(
            array(
                self::PARAMETER_COUNT           => 7,
                self::PARAMETER_NAME            => self::CUSTOMER_NAME__NONE,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
            array(
                self::PARAMETER_COUNT           => 7,
                self::PARAMETER_NAME            => self::CAMPAIGN_NAME__ADVERTISER_AGENCY_REALSEARCH_IPS_01,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
            array(
                self::PARAMETER_COUNT           => 2,
                self::PARAMETER_NAME            => self::CAMPAIGN_NAME__ADVERTISER_AGENCY_REALSEARCH_IPS_02,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
        ),
        self::TYPE__TIME_ALL__KIND_WEB_PAGES => array(),
        self::TYPE__TIME_ALL__KIND_YELLOW_PAGES => array(
            array(
                self::PARAMETER_COUNT           => 2,
                self::PARAMETER_NAME            => self::CAMPAIGN_NAME__ADVERTISER_AGENCY_REALSEARCH_IPS_02,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
        ),
        self::TYPE__TIME_ALL__KIND_BILLBOARDS => array(
            array(
                self::PARAMETER_COUNT           => 7,
                self::PARAMETER_NAME            => self::CAMPAIGN_NAME__ADVERTISER_AGENCY_REALSEARCH_IPS_01,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
        ),
        self::TYPE__TIME_ALL__KIND_EMAIL => array(),
        self::TYPE__TIME_ALL__KIND_OTHER => array(),
    );
}
