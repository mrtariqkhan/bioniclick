<?php

require_once dirname(__FILE__) . '/QueryBuilderTopCustomersBaseCompanyTree.php';

/**
 * Description of QueryBuilderTopCustomersCompanyTreeAgencyTest
 * 
 * @author fairdev
 */
class QueryBuilderTopCustomersCompanyTreeAgencyTest extends QueryBuilderTopCustomersBaseCompanyTree {

    
    protected function configureFilterValuesAndResults() {

        $this->setFilterValues(self::$FILTER_VALUES);
        $this->setExpectedResults(self::$RESULTS);
    }

    protected function configureUserWithTz() {
        $this->configureCompanyTreeUserWithTz(
            self::ID_COMPANY_AGENCY_REALSEARCH,
            null,
            self::TIMEZONE_ID_GMT
        );
    }

    protected static $FILTER_VALUES = array(
        self::TYPE__TIME_RANGE_00__KIND_ALL => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_ALL,
            self::FILTER_PARAMETER_FROM => '1323000016',
            self::FILTER_PARAMETER_TO   => '1323000023',
        ),
        self::TYPE__TIME_RANGE_00__KIND_WEB_PAGES => array(
            self::FILTER_PARAMETER_FROM => '1323000113',
            self::FILTER_PARAMETER_TO   => '1323000117',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_WEB_PAGES,
        ),
        self::TYPE__TIME_RANGE_00__KIND_YELLOW_PAGES => array(
            self::FILTER_PARAMETER_FROM => '1323000100',
            self::FILTER_PARAMETER_TO   => '1323000106',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_YELLOW_PAGES,
        ),
        self::TYPE__TIME_RANGE_00__KIND_BILLBOARDS => array(
            self::FILTER_PARAMETER_FROM => '1323000116',
            self::FILTER_PARAMETER_TO   => '1323000125',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_BILLBOARDS,
        ),
        self::TYPE__TIME_RANGE_00__KIND_EMAIL => array(
            self::FILTER_PARAMETER_FROM => '1323000004',
            self::FILTER_PARAMETER_TO   => '1323000007',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_EMAIL,
        ),
        self::TYPE__TIME_RANGE_00__KIND_OTHER => array(
            self::FILTER_PARAMETER_FROM => '1323000006',
            self::FILTER_PARAMETER_TO   => '1323000121',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_OTHER,
        ),

        self::TYPE__TIME_RANGE_01__KIND_ALL => array(
            self::FILTER_PARAMETER_FROM => '1323000060',
            self::FILTER_PARAMETER_TO   => '1323000066',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_ALL,
        ),
//        self::TYPE__TIME_RANGE_01__KIND_WEB_PAGES => array(),
        self::TYPE__TIME_RANGE_01__KIND_YELLOW_PAGES => array(
            self::FILTER_PARAMETER_FROM => '1323000123',
            self::FILTER_PARAMETER_TO   => '1323000126',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_YELLOW_PAGES,
        ),
        self::TYPE__TIME_RANGE_01__KIND_BILLBOARDS => array(
            self::FILTER_PARAMETER_FROM => '1323000114',
            self::FILTER_PARAMETER_TO   => '1323000114',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_BILLBOARDS,
        ),
//        self::TYPE__TIME_RANGE_01__KIND_EMAIL => array(),
//        self::TYPE__TIME_RANGE_01__KIND_OTHER => array(),

        self::TYPE__TIME_RANGE_02__KIND_ALL => array(
            self::FILTER_PARAMETER_FROM => '1323000115',
            self::FILTER_PARAMETER_TO   => '1323000121',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_ALL,
        ),
//        self::TYPE__TIME_RANGE_02__KIND_WEB_PAGES => array(),
//        self::TYPE__TIME_RANGE_02__KIND_YELLOW_PAGES => array(),
//        self::TYPE__TIME_RANGE_02__KIND_BILLBOARDS => array(),
//        self::TYPE__TIME_RANGE_02__KIND_EMAIL => array(),
//        self::TYPE__TIME_RANGE_02__KIND_OTHER => array(),

        self::TYPE__TIME_ALL__KIND_ALL => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_ALL,
        ),
        self::TYPE__TIME_ALL__KIND_WEB_PAGES => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_WEB_PAGES,
        ),
        self::TYPE__TIME_ALL__KIND_YELLOW_PAGES => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_YELLOW_PAGES,
        ),
        self::TYPE__TIME_ALL__KIND_BILLBOARDS => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_BILLBOARDS,
        ),
        self::TYPE__TIME_ALL__KIND_EMAIL => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_EMAIL,
        ),
        self::TYPE__TIME_ALL__KIND_OTHER => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_OTHER,
        ),
    );


    protected static $RESULTS = array(
        self::TYPE__TIME_RANGE_00__KIND_ALL => array(),
        self::TYPE__TIME_RANGE_00__KIND_WEB_PAGES => array(),
        self::TYPE__TIME_RANGE_00__KIND_YELLOW_PAGES => array(),
        self::TYPE__TIME_RANGE_00__KIND_BILLBOARDS => array(),
        self::TYPE__TIME_RANGE_00__KIND_EMAIL => array(),
        self::TYPE__TIME_RANGE_00__KIND_OTHER => array(),

        self::TYPE__TIME_RANGE_01__KIND_ALL => array(
            array(
                self::PARAMETER_COUNT           => 3,
                self::PARAMETER_NAME            => self::ADVERTISER_NAME__ADVERTISER_AGENCY_REALSEARCH_IPS,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_ADVERTISER,
            ),
        ),
//        self::TYPE__TIME_RANGE_01__KIND_WEB_PAGES => array(),
        self::TYPE__TIME_RANGE_01__KIND_YELLOW_PAGES => array(
            array(
                self::PARAMETER_COUNT           => 1,
                self::PARAMETER_NAME            => self::ADVERTISER_NAME__ADVERTISER_AGENCY_REALSEARCH_IPS,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_ADVERTISER,
            ),
        ),
        self::TYPE__TIME_RANGE_01__KIND_BILLBOARDS => array(
            array(
                self::PARAMETER_COUNT           => 1,
                self::PARAMETER_NAME            => self::ADVERTISER_NAME__ADVERTISER_AGENCY_REALSEARCH_IPS,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_ADVERTISER,
            ),
        ),
//        self::TYPE__TIME_RANGE_01__KIND_EMAIL => array(),
//        self::TYPE__TIME_RANGE_01__KIND_OTHER => array(),


        self::TYPE__TIME_RANGE_02__KIND_ALL => array(
            array(
                self::PARAMETER_COUNT           => 2,
                self::PARAMETER_NAME            => self::ADVERTISER_NAME__ADVERTISER_AGENCY_REALSEARCH_IPS,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_ADVERTISER,
            ),
        ),
//        self::TYPE__TIME_RANGE_02__KIND_WEB_PAGES => array(),
//        self::TYPE__TIME_RANGE_02__KIND_YELLOW_PAGES => array(),
//        self::TYPE__TIME_RANGE_02__KIND_BILLBOARDS => array(),
//        self::TYPE__TIME_RANGE_02__KIND_EMAIL => array(),
//        self::TYPE__TIME_RANGE_02__KIND_OTHER => array(),


        self::TYPE__TIME_ALL__KIND_ALL => array(
            array(
                self::PARAMETER_COUNT           => 16,
                self::PARAMETER_NAME            => self::ADVERTISER_NAME__ADVERTISER_AGENCY_REALSEARCH_IPS,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_ADVERTISER,
            ),
        ),
        self::TYPE__TIME_ALL__KIND_WEB_PAGES => array(),
        self::TYPE__TIME_ALL__KIND_YELLOW_PAGES => array(
            array(
                self::PARAMETER_COUNT           => 2,
                self::PARAMETER_NAME            => self::ADVERTISER_NAME__ADVERTISER_AGENCY_REALSEARCH_IPS,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_ADVERTISER,
            ),
        ),
        self::TYPE__TIME_ALL__KIND_BILLBOARDS => array(
            array(
                self::PARAMETER_COUNT           => 7,
                self::PARAMETER_NAME            => self::ADVERTISER_NAME__ADVERTISER_AGENCY_REALSEARCH_IPS,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_ADVERTISER,
            ),
        ),
        self::TYPE__TIME_ALL__KIND_EMAIL => array(),
        self::TYPE__TIME_ALL__KIND_OTHER => array(),
    );
}
