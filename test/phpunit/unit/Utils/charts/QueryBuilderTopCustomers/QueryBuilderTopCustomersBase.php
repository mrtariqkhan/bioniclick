<?php

require_once dirname(__FILE__) . '/../../../UserTimezoneTestBase.php';

/**
 * Description of QueryBuilderTopCustomersBase
 * 
 * @author fairdev
 */
abstract class QueryBuilderTopCustomersBase extends UserTimezoneTestBase {

    const PARAMETER_COUNT           = 'calls_count';
    const PARAMETER_NAME            = 'name';
    const PARAMETER_CUSTOMER_KIND   = 'customer_kind';


    public function assertArrayAreSame($expectedArray, array $actualArray, $message) {

        $expectedArray = empty($expectedArray) 
            ? array() 
            : $expectedArray
        ;

        foreach ($expectedArray as $expectedArr) {

            $expectedName           = $expectedArr[self::PARAMETER_NAME];
            $expectedCustomerKind   = $expectedArr[self::PARAMETER_CUSTOMER_KIND];
            $expectedCount          = intval($expectedArr[self::PARAMETER_COUNT]);

            $actualName         = '';
            $actualCustomerKind = '';
            $actualCount        = -1;
            foreach ($actualArray as $i => $actualArr) {
                if (
                        $actualArr[self::PARAMETER_CUSTOMER_KIND] === $expectedCustomerKind
                        && (
                                is_null($expectedName) && is_null($actualArr[self::PARAMETER_NAME])
                            || !is_null($expectedName) && $actualArr[self::PARAMETER_NAME] === $expectedName
                        )
                ) {
                    $actualName         = $actualArr[self::PARAMETER_NAME];
                    $actualCustomerKind = $actualArr[self::PARAMETER_CUSTOMER_KIND];
                    $actualCount        = intval($actualArr[self::PARAMETER_COUNT]);
                    unset($actualArr[$i]);
                    break;
                }
            }

            $this->assertEquals(
                $expectedCustomerKind, 
                $actualCustomerKind, 
                "$message (actual results has no info about '$expectedCustomerKind \"$expectedName\"'. Actual CutomerKind is wrong.)"
            );

            $this->assertEquals(
                $expectedName, 
                $actualName, 
                "$message (actual results has no info about '$expectedCustomerKind \"$expectedName\"'. Actual Name is wrong.)"
            );
            $this->assertEquals(
                $expectedCount, 
                $actualCount, 
                "$message (actual count of '$expectedCustomerKind \"$expectedName\"' is wrong)"
            );
        }

        $this->assertEquals(
            count($expectedArray), 
            count($actualArray), 
            "$message (size of actual array is wrong)"
        );
    }

    /**
     *
     * @param array $filterValues
     * @return type 
     */
    protected function getResultArray(
        $filterValues
    ) {

        $user = $this->getUser();

        $advertiser = null;
        $company    = null;
        if ($user->isAdvertiserUser()) {
            $advertiser = $user->getAdvertiser();
        } else {
            $company = $user->getFirstCompany();
        }
        $queryBuilder = new QueryBuilderTopCustomers(
            $advertiser,
            $company,
            $filterValues
        );

        $sqlQueryString = $queryBuilder->getSqlQueryString();
//echoln($sqlQueryString);
        unset($queryBuilder);

        $queryResult = array();
        if (!empty($sqlQueryString)) {
            $conn = Doctrine_Manager::getInstance()->getConnection('doctrine');
            $queryResult = $conn->fetchAssoc($sqlQueryString, array());
        }

        return $queryResult;
    }
}
