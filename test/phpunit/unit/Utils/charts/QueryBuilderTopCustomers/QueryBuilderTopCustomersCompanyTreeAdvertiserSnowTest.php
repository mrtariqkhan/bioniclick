<?php

require_once dirname(__FILE__) . '/QueryBuilderTopCustomersBaseCompanyTree.php';

/**
 * Description of QueryBuilderTopCustomersCompanyTreeAdvertiserSnowTest
 * 
 * @author fairdev
 */
class QueryBuilderTopCustomersCompanyTreeAdvertiserSnowTest extends QueryBuilderTopCustomersBaseCompanyTree {

    
    protected function configureFilterValuesAndResults() {

        $this->setFilterValues(self::$FILTER_VALUES);
        $this->setExpectedResults(self::$RESULTS);
    }

    protected function configureUserWithTz() {
        $this->configureCompanyTreeUserWithTz(
            null,
            self::ID_ADVERTISER_AGENCY_SNOW,
            self::TIMEZONE_ID_GMT
        );
    }

    protected static $FILTER_VALUES = array(
        self::TYPE__TIME_RANGE_00__KIND_ALL => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_ALL,
            self::FILTER_PARAMETER_FROM => '1323000043',
            self::FILTER_PARAMETER_TO   => '1323000078',
        ),
        self::TYPE__TIME_RANGE_00__KIND_WEB_PAGES => array(
            self::FILTER_PARAMETER_FROM => '1323000028',
            self::FILTER_PARAMETER_TO   => '1323000046',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_WEB_PAGES,
        ),
        self::TYPE__TIME_RANGE_00__KIND_YELLOW_PAGES => array(
            self::FILTER_PARAMETER_FROM => '1323000107',
            self::FILTER_PARAMETER_TO   => '1323000126',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_YELLOW_PAGES,
        ),
        self::TYPE__TIME_RANGE_00__KIND_BILLBOARDS => array(
            self::FILTER_PARAMETER_FROM => '1323000062',
            self::FILTER_PARAMETER_TO   => '1323000093',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_BILLBOARDS,
        ),
        self::TYPE__TIME_RANGE_00__KIND_EMAIL => array(
            self::FILTER_PARAMETER_FROM => '1323000009',
            self::FILTER_PARAMETER_TO   => '1323000025',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_EMAIL,
        ),
        self::TYPE__TIME_RANGE_00__KIND_OTHER => array(
            self::FILTER_PARAMETER_FROM => '1323000117',
            self::FILTER_PARAMETER_TO   => '1323000123',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_OTHER,
        ),

        self::TYPE__TIME_RANGE_01__KIND_ALL => array(
            self::FILTER_PARAMETER_FROM => '1323000007',
            self::FILTER_PARAMETER_TO   => '1323000022',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_ALL,
        ),
        self::TYPE__TIME_RANGE_01__KIND_WEB_PAGES => array(
            self::FILTER_PARAMETER_FROM => '1323000117',
            self::FILTER_PARAMETER_TO   => '1323000133',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_WEB_PAGES
        ),
//        self::TYPE__TIME_RANGE_01__KIND_YELLOW_PAGES => array(),
        self::TYPE__TIME_RANGE_01__KIND_BILLBOARDS => array(
            self::FILTER_PARAMETER_FROM => '1323000118',
            self::FILTER_PARAMETER_TO   => '1323000134',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_BILLBOARDS,
        ),
        self::TYPE__TIME_RANGE_01__KIND_EMAIL => array(
            self::FILTER_PARAMETER_FROM => '1323000026',
            self::FILTER_PARAMETER_TO   => '1323000083',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_EMAIL,
        ),
        self::TYPE__TIME_RANGE_01__KIND_OTHER => array(
            self::FILTER_PARAMETER_FROM => '1323000119',
            self::FILTER_PARAMETER_TO   => '1323000135',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_OTHER,
        ),

//        self::TYPE__TIME_RANGE_02__KIND_ALL => array(),
//        self::TYPE__TIME_RANGE_02__KIND_WEB_PAGES => array(),
//        self::TYPE__TIME_RANGE_02__KIND_YELLOW_PAGES => array(),
//        self::TYPE__TIME_RANGE_02__KIND_BILLBOARDS => array(),
//        self::TYPE__TIME_RANGE_02__KIND_EMAIL => array(),
//        self::TYPE__TIME_RANGE_02__KIND_OTHER => array(),

        self::TYPE__TIME_ALL__KIND_ALL => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_ALL,
        ),
        self::TYPE__TIME_ALL__KIND_WEB_PAGES => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_WEB_PAGES,
        ),
        self::TYPE__TIME_ALL__KIND_YELLOW_PAGES => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_YELLOW_PAGES,
        ),
        self::TYPE__TIME_ALL__KIND_BILLBOARDS => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_BILLBOARDS,
        ),
        self::TYPE__TIME_ALL__KIND_EMAIL => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_EMAIL,
        ),
        self::TYPE__TIME_ALL__KIND_OTHER => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_OTHER,
        ),
    );


    protected static $RESULTS = array(
        self::TYPE__TIME_RANGE_00__KIND_ALL => array(),
        self::TYPE__TIME_RANGE_00__KIND_WEB_PAGES => array(),
        self::TYPE__TIME_RANGE_00__KIND_YELLOW_PAGES => array(),
        self::TYPE__TIME_RANGE_00__KIND_BILLBOARDS => array(),
        self::TYPE__TIME_RANGE_00__KIND_EMAIL => array(),
        self::TYPE__TIME_RANGE_00__KIND_OTHER => array(),

        self::TYPE__TIME_RANGE_01__KIND_ALL => array(
            array(
                self::PARAMETER_COUNT           => 8,
                self::PARAMETER_NAME            => self::CAMPAIGN_NAME__ADVERTISER_AGENCY_SNOW_01,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
        ),
        self::TYPE__TIME_RANGE_01__KIND_WEB_PAGES => array(
            array(
                self::PARAMETER_COUNT           => 1,
                self::PARAMETER_NAME            => self::CAMPAIGN_NAME__ADVERTISER_AGENCY_SNOW_03,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
        ),
//        self::TYPE__TIME_RANGE_01__KIND_YELLOW_PAGES => array(),
        self::TYPE__TIME_RANGE_01__KIND_BILLBOARDS => array(
            array(
                self::PARAMETER_COUNT           => 1,
                self::PARAMETER_NAME            => self::CAMPAIGN_NAME__ADVERTISER_AGENCY_SNOW_04,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
        ),
        self::TYPE__TIME_RANGE_01__KIND_EMAIL => array(
            array(
                self::PARAMETER_COUNT           => 3,
                self::PARAMETER_NAME            => self::CAMPAIGN_NAME__ADVERTISER_AGENCY_SNOW_01,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
        ),
        self::TYPE__TIME_RANGE_01__KIND_OTHER => array(
            array(
                self::PARAMETER_COUNT           => 1,
                self::PARAMETER_NAME            => self::CAMPAIGN_NAME__ADVERTISER_AGENCY_SNOW_05,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
        ),


//        self::TYPE__TIME_RANGE_02__KIND_ALL => array(),
//        self::TYPE__TIME_RANGE_02__KIND_WEB_PAGES => array(),
//        self::TYPE__TIME_RANGE_02__KIND_YELLOW_PAGES => array(),
//        self::TYPE__TIME_RANGE_02__KIND_BILLBOARDS => array(),
//        self::TYPE__TIME_RANGE_02__KIND_EMAIL => array(),
//        self::TYPE__TIME_RANGE_02__KIND_OTHER => array(),


        self::TYPE__TIME_ALL__KIND_ALL => array(
            array(
                self::PARAMETER_COUNT           => 51,
                self::PARAMETER_NAME            => self::CAMPAIGN_NAME__ADVERTISER_AGENCY_SNOW_01,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
            array(
                self::PARAMETER_COUNT           => 1,
                self::PARAMETER_NAME            => self::CAMPAIGN_NAME__ADVERTISER_AGENCY_SNOW_03,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
            array(
                self::PARAMETER_COUNT           => 1,
                self::PARAMETER_NAME            => self::CAMPAIGN_NAME__ADVERTISER_AGENCY_SNOW_04,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
            array(
                self::PARAMETER_COUNT           => 1,
                self::PARAMETER_NAME            => self::CAMPAIGN_NAME__ADVERTISER_AGENCY_SNOW_05,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
        ),
        self::TYPE__TIME_ALL__KIND_WEB_PAGES => array(
            array(
                self::PARAMETER_COUNT           => 1,
                self::PARAMETER_NAME            => self::CAMPAIGN_NAME__ADVERTISER_AGENCY_SNOW_03,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
        ),
        self::TYPE__TIME_ALL__KIND_YELLOW_PAGES => array(),
        self::TYPE__TIME_ALL__KIND_BILLBOARDS => array(
            array(
                self::PARAMETER_COUNT           => 1,
                self::PARAMETER_NAME            => self::CAMPAIGN_NAME__ADVERTISER_AGENCY_SNOW_04,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
        ),
        self::TYPE__TIME_ALL__KIND_EMAIL => array(
            array(
                self::PARAMETER_COUNT           => 51,
                self::PARAMETER_NAME            => self::CAMPAIGN_NAME__ADVERTISER_AGENCY_SNOW_01,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
        ),
        self::TYPE__TIME_ALL__KIND_OTHER => array(
            array(
                self::PARAMETER_COUNT           => 1,
                self::PARAMETER_NAME            => self::CAMPAIGN_NAME__ADVERTISER_AGENCY_SNOW_05,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_CAMPAIGN,
            ),
        ),
    );
}
