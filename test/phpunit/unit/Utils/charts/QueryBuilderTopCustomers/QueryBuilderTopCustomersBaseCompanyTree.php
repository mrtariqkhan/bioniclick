<?php

require_once dirname(__FILE__) . '/QueryBuilderTopCustomersBaseProvided.php';

/**
 * Description of QueryBuilderCallsByParameterBaseCompanyTree
 * 
 * @author fairdev
 */
abstract class QueryBuilderTopCustomersBaseCompanyTree extends QueryBuilderTopCustomersBaseProvided {

    const FILTER_PARAMETER_FROM = 'from';
    const FILTER_PARAMETER_TO   = 'to';
    const FILTER_PARAMETER_KIND = 'kind';

    const KIND_CODE_ALL            = CampaignTable::KIND_CODE_ALL;
    const KIND_CODE_WEB_PAGES      = CampaignTable::KIND_CODE_WEB_PAGES;
    const KIND_CODE_YELLOW_PAGES   = CampaignTable::KIND_CODE_YELLOW_PAGES;
    const KIND_CODE_BILLBOARDS     = CampaignTable::KIND_CODE_BILLBOARDS;
    const KIND_CODE_EMAIL          = CampaignTable::KIND_CODE_EMAIL;
    const KIND_CODE_OTHER          = CampaignTable::KIND_CODE_OTHER;

    const TYPE__TIME_RANGE_00__KIND_ALL            = 'time range, where there is noone call; all kind of campaigns';
    const TYPE__TIME_RANGE_00__KIND_WEB_PAGES      = 'time range, where there is noone call; web pages kind of campaigns';
    const TYPE__TIME_RANGE_00__KIND_YELLOW_PAGES   = 'time range, where there is noone call; yellow pages kind of campaigns';
    const TYPE__TIME_RANGE_00__KIND_BILLBOARDS     = 'time range, where there is noone call; billboards kind of campaigns';
    const TYPE__TIME_RANGE_00__KIND_EMAIL          = 'time range, where there is noone call; email kind of campaigns';
    const TYPE__TIME_RANGE_00__KIND_OTHER          = 'time range, where there is noone call; other  kind of campaigns';

    const TYPE__TIME_RANGE_01__KIND_ALL            = 'time range, where there are calls to ONE campaign; all kind of campaigns';
    const TYPE__TIME_RANGE_01__KIND_WEB_PAGES      = 'time range, where there are calls to ONE campaign; web pages kind of campaigns';
    const TYPE__TIME_RANGE_01__KIND_YELLOW_PAGES   = 'time range, where there are calls to ONE campaign; yellow pages kind of campaigns';
    const TYPE__TIME_RANGE_01__KIND_BILLBOARDS     = 'time range, where there are calls to ONE campaign; billboards kind of campaigns';
    const TYPE__TIME_RANGE_01__KIND_EMAIL          = 'time range, where there are calls to ONE campaign; email kind of campaigns';
    const TYPE__TIME_RANGE_01__KIND_OTHER          = 'time range, where there are calls to ONE campaign; other  kind of campaigns';

    const TYPE__TIME_RANGE_02__KIND_ALL            = 'time range, where there are calls to SEVERAL campaigns; all kind of campaigns';
    const TYPE__TIME_RANGE_02__KIND_WEB_PAGES      = 'time range, where there are calls to SEVERAL campaigns; web pages kind of campaigns';
    const TYPE__TIME_RANGE_02__KIND_YELLOW_PAGES   = 'time range, where there are calls to SEVERAL campaigns; yellow pages kind of campaigns';
    const TYPE__TIME_RANGE_02__KIND_BILLBOARDS     = 'time range, where there are calls to SEVERAL campaigns; billboards kind of campaigns';
    const TYPE__TIME_RANGE_02__KIND_EMAIL          = 'time range, where there are calls to SEVERAL campaigns; email kind of campaigns';
    const TYPE__TIME_RANGE_02__KIND_OTHER          = 'time range, where there are calls to SEVERAL campaigns; other  kind of campaigns';

    const TYPE__TIME_ALL__KIND_ALL              = 'all time; all kind of campaigns';
    const TYPE__TIME_ALL__KIND_WEB_PAGES        = 'all time; web pages kind of campaigns';
    const TYPE__TIME_ALL__KIND_YELLOW_PAGES     = 'all time; yellow pages kind of campaigns';
    const TYPE__TIME_ALL__KIND_BILLBOARDS       = 'all time; billboards kind of campaigns';
    const TYPE__TIME_ALL__KIND_EMAIL            = 'all time; email kind of campaigns';
    const TYPE__TIME_ALL__KIND_OTHER            = 'all time; other kind of campaigns';


    const ID_COMPANY_BIONIC                     = 1;
    const ID_COMPANY_RESELLER_ACQUISIO          = 2;
    const ID_COMPANY_AGENCY_REALSEARCH          = 4;
    const ID_ADVERTISER_BIONIC_ADVERTISER       = 1;
    const ID_ADVERTISER_RESELLER_ACQUISIO_TRIPS = 3;
    const ID_ADVERTISER_AGENCY_REALSEARCH_IPS   = 4;
    const ID_ADVERTISER_AGENCY_SNOW             = 5;

    const CUSTOMER_NAME__NONE       = NULL;

    const CAMPAIGN_NAME__ADVERTISER_BIONIC_ADVERTISER_01        = '4allpromos.com';
    const CAMPAIGN_NAME__ADVERTISER_BIONIC_ADVERTISER_02        = '4allpromos_another.com';
    const CAMPAIGN_NAME__ADVERTISER_BIONIC_ADVERTISER_03        = 'houstonairconditioners.net';
    const CAMPAIGN_NAME__ADVERTISER_RESELLER_ACQUISIO_TRIPS_01  = 'trips.com';
    const CAMPAIGN_NAME__ADVERTISER_RESELLER_ACQUISIO_TRIPS_02  = 'trips_another.com';
    const CAMPAIGN_NAME__ADVERTISER_RESELLER_ACQUISIO_TRIPS_03  = 'trips_another_email.com';
    const CAMPAIGN_NAME__ADVERTISER_AGENCY_REALSEARCH_IPS_01    = 'ips.com';
    const CAMPAIGN_NAME__ADVERTISER_AGENCY_REALSEARCH_IPS_02    = 'ips_another.com';
    const CAMPAIGN_NAME__ADVERTISER_AGENCY_SNOW_01              = 'snowing.com';
    const CAMPAIGN_NAME__ADVERTISER_AGENCY_SNOW_02              = 'snowing_another.com';
    const CAMPAIGN_NAME__ADVERTISER_AGENCY_SNOW_03              = 'snowing_another_web.com';
    const CAMPAIGN_NAME__ADVERTISER_AGENCY_SNOW_04              = 'snowing_another_billboards.com';
    const CAMPAIGN_NAME__ADVERTISER_AGENCY_SNOW_05              = 'snowing_another_other.com';

    const ADVERTISER_NAME__ADVERTISER_BIONIC_ADVERTISER         = 'Bionic Advertiser';
    const ADVERTISER_NAME__ADVERTISER_RESELLER_ACQUISIO_TRIPS   = 'Trips';
    const ADVERTISER_NAME__ADVERTISER_AGENCY_REALSEARCH_IPS     = 'iPS Packaging';
    const ADVERTISER_NAME__ADVERTISER_AGENCY_SNOW               = 'Snow';

    const AGENCY_NAME__REAL_SEARCH  = 'Real Search';
    const AGENCY_NAME__YOUR_PPC     = 'Your PPC';

    const RESELLER_NAME__IT_DOORS   = 'IT Doors';
    const RESELLER_NAME__ACQUISIO   = 'Acquisio';

    const CUSTOMER_KIND_CAMPAIGN    = 'campaign';
    const CUSTOMER_KIND_ADVERTISER  = 'advertiser';
    const CUSTOMER_KIND_AGENCY      = 'company';
    const CUSTOMER_KIND_RESELLER    = 'company';
    const CUSTOMER_KIND_NONE        = NULL;

    /**
     *
     * @dataProvider providerCases
     * 
     * @param string $caseType one of self::TYPE__*
     * @param int $kindCode one of self::KIND_CODE_*
     * @param string $kindName
     * @param string $specificAdditionalMessage
     */
    public function testCase(
        $caseType,
        $specificAdditionalMessage = ''
    ) {

        $expectedResultArray = $this->getExpectedResults();


        $filterValues = $this->getFilterValues();
        if (!array_key_exists($caseType, $filterValues)) {
            //NOTE:: test case is unnecessary
//            $this->markTestSkipped();
            return;
        }
        $filterValues = $filterValues[$caseType];

        $queryResult = $this->getResultArray($filterValues);
        $needResult = array_key_exists($caseType, $expectedResultArray) ? $expectedResultArray[$caseType] : array();

        $chartKind = "TopCustomers chart";
//var_dump($needResult);
//var_dump('=++++++++++++++++++++');
//var_dump($queryResult);
//var_dump('------------------------------------------------------');
        $this->assertArrayAreSame(
            $needResult, 
            $queryResult, 
<<<EOD
    Error in attempt to find $chartKind for '{$this->userKind}' user with timezone '{$this->timezoneName}' in '$caseType' case.
        $specificAdditionalMessage
EOD
        );
    }

    public function providerCases() {

        $rObj = new ReflectionObject($this);
        $caseTypes = array();
        $consts = $rObj->getConstants();
        foreach ($consts as $constName => $constValue) {
            if (substr_count($constName, 'TYPE__TIME_', 0) > 0) {
                $caseTypes[$constName] = $constValue;
            }
        }

        $cases = array();
        $i = 0;
        foreach ($caseTypes as $caseType) {
            $cases['case ' . ++$i . ": $caseType"] = array(
                $caseType
            );
        }

        return $cases;
    }
}
