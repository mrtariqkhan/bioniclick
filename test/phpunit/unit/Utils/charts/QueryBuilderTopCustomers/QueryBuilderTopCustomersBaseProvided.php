<?php

require_once dirname(__FILE__) . '/QueryBuilderTopCustomersBase.php';

/**
 * Description of QueryBuilderCallsByParameterBaseTz
 * 
 * @author fairdev
 */
abstract class QueryBuilderTopCustomersBaseProvided extends QueryBuilderTopCustomersBase {

    protected $filterValues = array();
    protected $expectedResults = array();


    protected function _start() {

        parent::_start();

        $this->configureFilterValuesAndResults();
    }

    abstract protected function configureFilterValuesAndResults();


    public function setFilterValues(array $filterValues) {
        $this->filterValues = $filterValues;
    }

    public function setExpectedResults(array $expectedResults) {
        $this->expectedResults = $expectedResults;
    }


    public function getFilterValues() {
        return $this->filterValues;
    }

    public function getExpectedResults() {
        return $this->expectedResults;
    }
}
