<?php

require_once dirname(__FILE__) . '/QueryBuilderTopCustomersBaseCompanyTree.php';

/**
 * Description of QueryBuilderTopCustomersCompanyTreeBionicTest
 * 
 * @author fairdev
 */
class QueryBuilderTopCustomersCompanyTreeBionicTest extends QueryBuilderTopCustomersBaseCompanyTree {

    
    protected function configureFilterValuesAndResults() {

        $this->setFilterValues(self::$FILTER_VALUES);
        $this->setExpectedResults(self::$RESULTS);
    }

    protected function configureUserWithTz() {
        $this->configureCompanyTreeUserWithTz(
            self::ID_COMPANY_BIONIC,
            null,
            self::TIMEZONE_ID_GMT
        );
    }

    protected static $FILTER_VALUES = array(
        self::TYPE__TIME_RANGE_00__KIND_ALL => array(
            self::FILTER_PARAMETER_FROM => '1323000060',
            self::FILTER_PARAMETER_TO   => '1323000061',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_ALL,
        ),
        self::TYPE__TIME_RANGE_00__KIND_WEB_PAGES => array(
            self::FILTER_PARAMETER_FROM => '1323000021',
            self::FILTER_PARAMETER_TO   => '1323000023',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_WEB_PAGES,
        ),
        self::TYPE__TIME_RANGE_00__KIND_YELLOW_PAGES => array(
            self::FILTER_PARAMETER_FROM => '1323000098',
            self::FILTER_PARAMETER_TO   => '1323000113',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_YELLOW_PAGES,
        ),
        self::TYPE__TIME_RANGE_00__KIND_BILLBOARDS => array(
            self::FILTER_PARAMETER_FROM => '1323000042',
            self::FILTER_PARAMETER_TO   => '1323000056',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_BILLBOARDS,
        ),
        self::TYPE__TIME_RANGE_00__KIND_EMAIL => array(
            self::FILTER_PARAMETER_FROM => '1323000062',
            self::FILTER_PARAMETER_TO   => '1323000081',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_EMAIL,
        ),
        self::TYPE__TIME_RANGE_00__KIND_OTHER => array(
            self::FILTER_PARAMETER_FROM => '1323000001',
            self::FILTER_PARAMETER_TO   => '1323000022',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_OTHER,
        ),

        self::TYPE__TIME_RANGE_01__KIND_ALL => array(
            self::FILTER_PARAMETER_FROM => '1323000083',
            self::FILTER_PARAMETER_TO   => '1323000100',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_ALL,
        ),
        self::TYPE__TIME_RANGE_01__KIND_WEB_PAGES => array(
            self::FILTER_PARAMETER_FROM => '1323000014',
            self::FILTER_PARAMETER_TO   => '1323000033',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_WEB_PAGES,
        ),
        self::TYPE__TIME_RANGE_01__KIND_YELLOW_PAGES => array(
            self::FILTER_PARAMETER_FROM => '1323000126',
            self::FILTER_PARAMETER_TO   => '1323000134',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_YELLOW_PAGES,
        ),
        self::TYPE__TIME_RANGE_01__KIND_BILLBOARDS => array(
            self::FILTER_PARAMETER_FROM => '1323000127',
            self::FILTER_PARAMETER_TO   => '1323000136',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_BILLBOARDS,
        ),
        self::TYPE__TIME_RANGE_01__KIND_EMAIL => array(
            self::FILTER_PARAMETER_FROM => '1323000007',
            self::FILTER_PARAMETER_TO   => '1323000040',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_EMAIL,
        ),
        self::TYPE__TIME_RANGE_01__KIND_OTHER => array(
            self::FILTER_PARAMETER_FROM => '1323000123',
            self::FILTER_PARAMETER_TO   => '1323000137',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_OTHER,
        ),

        self::TYPE__TIME_RANGE_02__KIND_ALL => array(
            self::FILTER_PARAMETER_FROM => '1323000105',
            self::FILTER_PARAMETER_TO   => '1323000122',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_ALL,
        ),
        self::TYPE__TIME_RANGE_02__KIND_WEB_PAGES => array(
            self::FILTER_PARAMETER_FROM => '1323000122',
            self::FILTER_PARAMETER_TO   => '1323000133',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_WEB_PAGES,
        ),
        self::TYPE__TIME_RANGE_02__KIND_YELLOW_PAGES => array(
            self::FILTER_PARAMETER_FROM => '1323000122',
            self::FILTER_PARAMETER_TO   => '1323000137',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_YELLOW_PAGES,
        ),
        self::TYPE__TIME_RANGE_02__KIND_BILLBOARDS => array(
            self::FILTER_PARAMETER_FROM => '1323000111',
            self::FILTER_PARAMETER_TO   => '1323000134',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_BILLBOARDS,
        ),
        self::TYPE__TIME_RANGE_02__KIND_EMAIL => array(
            self::FILTER_PARAMETER_FROM => '1323000104',
            self::FILTER_PARAMETER_TO   => '1323000133',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_EMAIL,
        ),
        self::TYPE__TIME_RANGE_02__KIND_OTHER => array(
            self::FILTER_PARAMETER_FROM => '1323000119',
            self::FILTER_PARAMETER_TO   => '1323000134',
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_OTHER,
        ),

        self::TYPE__TIME_ALL__KIND_ALL => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_ALL,
        ),
        self::TYPE__TIME_ALL__KIND_WEB_PAGES => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_WEB_PAGES,
        ),
        self::TYPE__TIME_ALL__KIND_YELLOW_PAGES => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_YELLOW_PAGES,
        ),
        self::TYPE__TIME_ALL__KIND_BILLBOARDS => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_BILLBOARDS,
        ),
        self::TYPE__TIME_ALL__KIND_EMAIL => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_EMAIL,
        ),
        self::TYPE__TIME_ALL__KIND_OTHER => array(
            self::FILTER_PARAMETER_KIND => self::KIND_CODE_OTHER,
        ),
    );


    protected static $RESULTS = array(
        self::TYPE__TIME_RANGE_00__KIND_ALL => array(),
        self::TYPE__TIME_RANGE_00__KIND_WEB_PAGES => array(),
        self::TYPE__TIME_RANGE_00__KIND_YELLOW_PAGES => array(),
        self::TYPE__TIME_RANGE_00__KIND_BILLBOARDS => array(),
        self::TYPE__TIME_RANGE_00__KIND_EMAIL => array(),
        self::TYPE__TIME_RANGE_00__KIND_OTHER => array(),

        self::TYPE__TIME_RANGE_01__KIND_ALL => array(
            array(
                self::PARAMETER_COUNT           => 12,
                self::PARAMETER_NAME            => self::CUSTOMER_NAME__NONE,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_NONE,
            ),
            array(
                self::PARAMETER_COUNT           => 16,
                self::PARAMETER_NAME            => self::ADVERTISER_NAME__ADVERTISER_BIONIC_ADVERTISER,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_ADVERTISER,
            ),
        ),
        self::TYPE__TIME_RANGE_01__KIND_WEB_PAGES => array(
            array(
                self::PARAMETER_COUNT           => 5,
                self::PARAMETER_NAME            => self::ADVERTISER_NAME__ADVERTISER_BIONIC_ADVERTISER,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_ADVERTISER,
            ),
        ),
        self::TYPE__TIME_RANGE_01__KIND_YELLOW_PAGES => array(
            array(
                self::PARAMETER_COUNT           => 2,   
                self::PARAMETER_NAME            => self::ADVERTISER_NAME__ADVERTISER_BIONIC_ADVERTISER,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_ADVERTISER,
            ),
        ),
        self::TYPE__TIME_RANGE_01__KIND_BILLBOARDS => array(
            array(
                self::PARAMETER_COUNT           => 1,   
                self::PARAMETER_NAME            => self::RESELLER_NAME__IT_DOORS,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
        ),
        self::TYPE__TIME_RANGE_01__KIND_EMAIL => array(
            array(
                self::PARAMETER_COUNT           => 10,
                self::PARAMETER_NAME            => self::RESELLER_NAME__IT_DOORS,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
        ),
        self::TYPE__TIME_RANGE_01__KIND_OTHER => array(
            array(
                self::PARAMETER_COUNT           => 1,   
                self::PARAMETER_NAME            => self::RESELLER_NAME__IT_DOORS,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
        ),


        self::TYPE__TIME_RANGE_02__KIND_ALL => array(
            // [.] - x,x,x,x - [.,.] - x - [.,.]
            array(
                self::PARAMETER_COUNT           => 30,  // [.] - 6,6,6,6 - [.,.] - 6 - [.,.]
                self::PARAMETER_NAME            => self::CUSTOMER_NAME__NONE,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_NONE,
            ),
            array(
                self::PARAMETER_COUNT           => 11,  // [.] - x,x,x,x - [4,4] - x - [3,.]
                self::PARAMETER_NAME            => self::ADVERTISER_NAME__ADVERTISER_BIONIC_ADVERTISER,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_ADVERTISER,
            ),
            array(
                self::PARAMETER_COUNT           => 7,   // [.] - x,x,x,x - [2,2] - x - [2,1]
                self::PARAMETER_NAME            => self::RESELLER_NAME__ACQUISIO,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
            array(
                self::PARAMETER_COUNT           => 6,   // [6] - x,x,x,x - [.,.] - x - [.,.]
                self::PARAMETER_NAME            => self::RESELLER_NAME__IT_DOORS,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
        ),
        self::TYPE__TIME_RANGE_02__KIND_WEB_PAGES => array(
            // [.,.,.] - [.]
            array(
                self::PARAMETER_COUNT           => 10,  // [3,4,3] - [.]
                self::PARAMETER_NAME            => self::ADVERTISER_NAME__ADVERTISER_BIONIC_ADVERTISER,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_ADVERTISER,
            ),
            array(
                self::PARAMETER_COUNT           => 1,   // [.,.,.] - [1]
                self::PARAMETER_NAME            => self::RESELLER_NAME__IT_DOORS,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
        ),
        self::TYPE__TIME_RANGE_02__KIND_YELLOW_PAGES => array(
            // [.,.,.,.] - [.]
            array(
                self::PARAMETER_COUNT           => 2,   // [.,.,.,.] - [2]
                self::PARAMETER_NAME            => self::ADVERTISER_NAME__ADVERTISER_BIONIC_ADVERTISER,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_ADVERTISER,
            ),
            array(
                self::PARAMETER_COUNT           => 1,   // [.,.,.,1] - [.]
                self::PARAMETER_NAME            => self::RESELLER_NAME__ACQUISIO,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
        ),
        self::TYPE__TIME_RANGE_02__KIND_BILLBOARDS => array(
            // x - [.,.] - x - [.,.,.,.,.] - [.]
            array(
                self::PARAMETER_COUNT           => 2,   // x - [1,1] - x - [.,.,.,.,.] - [.]
                self::PARAMETER_NAME            => self::RESELLER_NAME__ACQUISIO,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
            array(
                self::PARAMETER_COUNT           => 1,   // x - [.,.] - x - [.,.,.,.,.] - [1]
                self::PARAMETER_NAME            => self::RESELLER_NAME__IT_DOORS,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
        ),
        self::TYPE__TIME_RANGE_02__KIND_EMAIL => array(
            // [.,.] - x,x,x,x - [.,.] - x - [.,.,.,.,.] - [.]
            array(
                self::PARAMETER_COUNT           => 1,   // [.,.] - x,x,x,x - [.,.] - x - [.,.,.,.,.] - [1]
                self::PARAMETER_NAME            => self::RESELLER_NAME__ACQUISIO,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
            array(
                self::PARAMETER_COUNT           => 8,   // [2,6] - x,x,x,x - [.,.] - x - [.,.,.,.,.] - [.]
                self::PARAMETER_NAME            => self::RESELLER_NAME__IT_DOORS,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
        ),
        self::TYPE__TIME_RANGE_02__KIND_OTHER => array(
            // x - [.,.,.,.,.] - [.]
            array(
                self::PARAMETER_COUNT           => 2,   // x - [1,1,.,.,.] - [.]
                self::PARAMETER_NAME            => self::RESELLER_NAME__ACQUISIO,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
            array(
                self::PARAMETER_COUNT           => 1,   // x - [.,.,.,.,.] - [1]
                self::PARAMETER_NAME            => self::RESELLER_NAME__IT_DOORS,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
        ),


// x - [.,.,.,.,.] - x,x,x,x - [y,y,y,y,y] - x,x - [.] - x - [.,.,.,.] - x,x - [.] - x - [.,.,.,.,.,y,y,y,y,y,y] - x,x,x,x - [y,y,.,.,.,.,.,y,y,y,y,y] - x,x - [.] - x - [y,y,y,y,.,.,.,.,.] - x,x,x - [.,.,.,.,.] - x,x,x,x - [.,.] - x - [.,.,.,.,.] - [.]
        self::TYPE__TIME_ALL__KIND_ALL => array(
            array(
                self::PARAMETER_COUNT           => 95,  // 6 - [.,.,.,.,.] - 6,6,2,2 - [y,y,y,y,y] - 6,6 - [.] - 2 - [.,.,.,.] - 2,2 - [.] - 2 - [.,.,.,.,.,y,y,y,y,y,y] - 2,2,1,2 - [y,y,.,.,.,.,.,y,y,y,y,y] - 2,2 - [.] - 2 - [y,y,y,y,.,.,.,.,.] - 2,2,6 - [.,.,.,.,.] - 6,6,6,6 - [.,.] - 6 - [.,.,.,.,.] - [.]
                self::PARAMETER_NAME            => self::CUSTOMER_NAME__NONE,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_NONE,
            ),
            array(
                self::PARAMETER_COUNT           => 70,  // x - [.,.,.,.,.] - x,x,x,x - [2,1,2,2,2] - x,x - [.] - x - [2,1,2,2] - x,x - [.] - x - [1,.,1,.,1,1,y,y,1,y,1] - x,x,x,x - [1,1,1,1,1,.,1,1,y,1,y,1] - x,x - [.] - x - [2,1,2,2,2,2,2,1,2] - x,x,x - [.,.,.,.,.] - x,x,x,x - [4,4] - x - [3,.,3,4,3] - [2]
                self::PARAMETER_NAME            => self::ADVERTISER_NAME__ADVERTISER_BIONIC_ADVERTISER,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_ADVERTISER,
            ),
            array(
                self::PARAMETER_COUNT           => 32,  // x - [.,.,.,.,.] - x,x,x,x - [y,y,y,y,y] - x,x - [.] - x - [.,.,.,.] - x,x - [.] - x - [1,1,1,1,1,1,1,1,1,1,1] - x,x,x,x - [1,1,1,1,1,1,1,1,1,1,1,1] - x,x - [.] - x - [y,y,y,y,.,.,.,.,.] - x,x,x - [.,.,.,.,.] - x,x,x,x - [2,2] - x - [2,1,.,.,1] - [1]
                self::PARAMETER_NAME            => self::RESELLER_NAME__ACQUISIO,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
            array(
                self::PARAMETER_COUNT           => 54,  // x - [6,2,6,2,6] - x,x,x,x - [y,y,y,y,y] - x,x - [1] - x - [.,.,.,.] - x,x - [1] - x - [.,.,.,.,.,y,y,y,y,y,y] - x,x,x,x - [y,y,.,.,.,.,.,y,y,y,y,y] - x,x - [1] - x - [y,y,y,y,.,.,.,.,.] - x,x,x - [6,6,6,2,6] - x,x,x,x - [.,.] - x - [.,.,.,.,.] - [3]
                self::PARAMETER_NAME            => self::RESELLER_NAME__IT_DOORS,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
        ),
        self::TYPE__TIME_ALL__KIND_WEB_PAGES => array(
            array(
                self::PARAMETER_COUNT           => 44,  // x - [.,.,.,.,.] - x,x,x,x - [y,y,y,y,y] - x,x - [.] - x - [2,1,2,2] - x,x - [.] - x - [1,.,1,.,1,y,y,y,y,y,y] - x,x,x,x - [y,y,1,1,1,.,1,y,y,y,y,y] - x,x - [.] - x - [y,y,y,y,2,2,2,1,2] - x,x,x - [.,.,.,.,.] - x,x,x,x - [4,4] - x - [3,.,3,4,3] - [.]
                self::PARAMETER_NAME            => self::ADVERTISER_NAME__ADVERTISER_BIONIC_ADVERTISER,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_ADVERTISER,
            ),
            array(
                self::PARAMETER_COUNT           => 1,   // x - [.,.,.,.,.] - x,x,x,x - [y,y,y,y,y] - x,x - [.] - x - [.,.,.,.] - x,x - [.] - x - [.,.,.,.,.,y,y,y,y,y,y] - x,x,x,x - [y,y,.,.,.,.,.,y,y,y,y,y] - x,x - [.] - x - [y,y,y,y,.,.,.,.,.] - x,x,x - [.,.,.,.,.] - x,x,x,x - [.,.] - x - [.,.,.,.,.] - [1]
                self::PARAMETER_NAME            => self::RESELLER_NAME__IT_DOORS,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
        ),
        self::TYPE__TIME_ALL__KIND_YELLOW_PAGES => array(
            array(
                self::PARAMETER_COUNT           => 2,   // x - [.,.,.,.,.] - x,x,x,x - [y,y,y,y,y] - x,x - [.] - x - [.,.,.,.] - x,x - [.] - x - [.,.,.,.,.,y,y,y,y,y,y] - x,x,x,x - [y,y,.,.,.,.,.,y,y,y,y,y] - x,x - [.] - x - [y,y,y,y,.,.,.,.,.] - x,x,x - [.,.,.,.,.] - x,x,x,x - [.,.] - x - [.,.,.,.,.] - [2]
                self::PARAMETER_NAME            => self::ADVERTISER_NAME__ADVERTISER_BIONIC_ADVERTISER,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_ADVERTISER,
            ),
            array(
                self::PARAMETER_COUNT           => 9,   // x - [.,.,.,.,.] - x,x,x,x - [y,y,y,y,y] - x,x - [.] - x - [.,.,.,.] - x,x - [.] - x - [1,1,1,1,1,y,y,y,y,y,y] - x,x,x,x - [y,y,.,.,.,.,.,y,y,y,y,y] - x,x - [.] - x - [y,y,y,y,.,.,.,.,.] - x,x,x - [.,.,.,.,.] - x,x,x,x - [1,1] - x - [1,.,.,.,1] - [.]
                self::PARAMETER_NAME            => self::RESELLER_NAME__ACQUISIO,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
        ),
        self::TYPE__TIME_ALL__KIND_BILLBOARDS => array(
            array(
                self::PARAMETER_COUNT           => 7,   // x - [.,.,.,.,.] - x,x,x,x - [y,y,y,y,y] - x,x - [.] - x - [.,.,.,.] - x,x - [.] - x - [.,.,.,.,.,y,y,y,y,y,y] - x,x,x,x - [y,y,1,1,1,1,1,y,y,y,y,y] - x,x - [.] - x - [y,y,y,y,.,.,.,.,.] - x,x,x - [.,.,.,.,.] - x,x,x,x - [1,1] - x - [.,.,.,.,.] - [.]
                self::PARAMETER_NAME            => self::RESELLER_NAME__ACQUISIO,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
            array(
                self::PARAMETER_COUNT           => 1,   // x - [.,.,.,.,.] - x,x,x,x - [y,y,y,y,y] - x,x - [.] - x - [.,.,.,.] - x,x - [.] - x - [.,.,.,.,.,y,y,y,y,y,y] - x,x,x,x - [y,y,.,.,.,.,.,y,y,y,y,y] - x,x - [.] - x - [y,y,y,y,.,.,.,.,.] - x,x,x - [.,.,.,.,.] - x,x,x,x - [.,.] - x - [.,.,.,.,.] - [1]
                self::PARAMETER_NAME            => self::RESELLER_NAME__IT_DOORS,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
        ),
        self::TYPE__TIME_ALL__KIND_EMAIL => array(
            array(
                self::PARAMETER_COUNT           => 1,   // x - [.,.,.,.,.] - x,x,x,x - [y,y,y,y,y] - x,x - [.] - x - [.,.,.,.] - x,x - [.] - x - [.,.,.,.,.,y,y,y,y,y,y] - x,x,x,x - [y,y,.,.,.,.,.,y,y,y,y,y] - x,x - [.] - x - [y,y,y,y,.,.,.,.,.] - x,x,x - [.,.,.,.,.] - x,x,x,x - [.,.] - x - [.,.,.,.,.] - [1]
                self::PARAMETER_NAME            => self::RESELLER_NAME__ACQUISIO,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
            array(
                self::PARAMETER_COUNT           => 51,  // x - [6,2,6,2,6] - x,x,x,x - [y,y,y,y,y] - x,x - [1] - x - [.,.,.,.] - x,x - [1] - x - [.,.,.,.,.,y,y,y,y,y,y] - x,x,x,x - [y,y,.,.,.,.,.,y,y,y,y,y] - x,x - [1] - x - [y,y,y,y,.,.,.,.,.] - x,x,x - [6,6,6,2,6] - x,x,x,x - [.,.] - x - [.,.,.,.,.] - [.]
                self::PARAMETER_NAME            => self::RESELLER_NAME__IT_DOORS,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
        ),
        self::TYPE__TIME_ALL__KIND_OTHER => array(
            array(
                self::PARAMETER_COUNT           => 2,   // x - [.,.,.,.,.] - x,x,x,x - [y,y,y,y,y] - x,x - [.] - x - [.,.,.,.] - x,x - [.] - x - [.,.,.,.,.,y,y,y,y,y,y] - x,x,x,x - [y,y,.,.,.,.,.,y,y,y,y,y] - x,x - [.] - x - [y,y,y,y,.,.,.,.,.] - x,x,x - [.,.,.,.,.] - x,x,x,x - [.,.] - x - [1,1,.,.,.] - [.]
                self::PARAMETER_NAME            => self::RESELLER_NAME__ACQUISIO,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
            array(
                self::PARAMETER_COUNT           => 1,   // x - [.,.,.,.,.] - x,x,x,x - [y,y,y,y,y] - x,x - [.] - x - [.,.,.,.] - x,x - [.] - x - [.,.,.,.,.,y,y,y,y,y,y] - x,x,x,x - [y,y,.,.,.,.,.,y,y,y,y,y] - x,x - [.] - x - [y,y,y,y,.,.,.,.,.] - x,x,x - [.,.,.,.,.] - x,x,x,x - [.,.] - x - [.,.,.,.,.] - [1]
                self::PARAMETER_NAME            => self::RESELLER_NAME__IT_DOORS,
                self::PARAMETER_CUSTOMER_KIND   => self::CUSTOMER_KIND_RESELLER,
            ),
        ),
    );
}
