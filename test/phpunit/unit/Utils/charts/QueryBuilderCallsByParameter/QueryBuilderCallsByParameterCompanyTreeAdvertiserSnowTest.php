<?php

require_once dirname(__FILE__) . '/QueryBuilderCallsByParameterBaseCompanyTree.php';

/**
 * Description of QueryBuilderCallsByParameterCompanyTreeAdvertiserSnowTest
 * 
 * @author fairdev
 */
class QueryBuilderCallsByParameterCompanyTreeAdvertiserSnowTest extends QueryBuilderCallsByParameterBaseCompanyTree {


    const VALUE_URL_AUTO  = 'http://snowing.com/index.html?cmbid=00000000000000000000000000000004';


    protected function configureFilterValuesAndResults() {

        $this->setFilterValues(self::$FILTER_VALUES);

        $this->setExpectedResultsByStatus(          self::$RESULTS_BY_STATUS);
        $this->setExpectedResultsByReferrerSource(  self::$RESULTS_BY_REFERRER_SOURCE);
        $this->setExpectedResultsBySearchType(      self::$RESULTS_BY_SEARCH_TYPE);
        $this->setExpectedResultsByUrl(             self::$RESULTS_BY_URL);
        $this->setExpectedResultsByKeyword(         self::$RESULTS_BY_KEYWORD);
    }

    protected function configureUserWithTz() {
        $this->configureCompanyTreeUserWithTz(
            null,
            self::ID_ADVERTISER_AGENCY_SNOW,
            self::TIMEZONE_ID_GMT
        );
    }

    protected static $FILTER_VALUES = array(
        self::TYPE_RANGE => array(
            'from'  => '1323000003',
            'to'    => '1323000019',
        ),
        self::TYPE_ALL => array(
        ),
    );


    // NOTE:: Does not depend on type of calls
    protected static $RESULTS_BY_STATUS = array(
        self::TYPE_RANGE => array(
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 12,    // - [4,2,1,1,4] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 5,     // - [1,.,3,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 2,     // - [.,.,1,1,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 2,     // - [.,.,1,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 12,    // - [4,2,1,1,4] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 5,     // - [1,.,3,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 2,     // - [.,.,1,1,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 2,     // - [.,.,1,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 12,    // - [4,2,1,1,4] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 5,     // - [1,.,3,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 2,     // - [.,.,1,1,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 2,     // - [.,.,1,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 12,    // - [4,2,1,1,4] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 5,     // - [1,.,3,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 2,     // - [.,.,1,1,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 2,     // - [.,.,1,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 12,    // - [4,2,1,1,4] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 5,     // - [1,.,3,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 2,     // - [.,.,1,1,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 2,     // - [.,.,1,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.] - 
                ),
            ),
        ),
        self::TYPE_ALL => array(
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 24,    // - [4,2,1,1,4] - [.] - [.] - [1] - [4,1,1,2,3]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 12,    // - [1,.,3,.,1] - [.] - [.] - [.] - [.,3,4,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 6,     // - [.,.,1,1,.] - [1] - [.] - [.] - [1,.,1,.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 5,     // - [.,.,1,.,1] - [.] - [1] - [.] - [1,.,.,.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 4,     // - [1,.,.,.,.] - [.] - [.] - [.] - [.,2,.,.,1]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 24,    // - [4,2,1,1,4] - [.] - [.] - [1] - [4,1,1,2,3]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 12,    // - [1,.,3,.,1] - [.] - [.] - [.] - [.,3,4,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 6,     // - [.,.,1,1,.] - [1] - [.] - [.] - [1,.,1,.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 5,     // - [.,.,1,.,1] - [.] - [1] - [.] - [1,.,.,.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 4,     // - [1,.,.,.,.] - [.] - [.] - [.] - [.,2,.,.,1]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 24,    // - [4,2,1,1,4] - [.] - [.] - [1] - [4,1,1,2,3]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 12,    // - [1,.,3,.,1] - [.] - [.] - [.] - [.,3,4,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 6,     // - [.,.,1,1,.] - [1] - [.] - [.] - [1,.,1,.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 5,     // - [.,.,1,.,1] - [.] - [1] - [.] - [1,.,.,.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 4,     // - [1,.,.,.,.] - [.] - [.] - [.] - [.,2,.,.,1]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 24,    // - [4,2,1,1,4] - [.] - [.] - [1] - [4,1,1,2,3]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 12,    // - [1,.,3,.,1] - [.] - [.] - [.] - [.,3,4,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 6,     // - [.,.,1,1,.] - [1] - [.] - [.] - [1,.,1,.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 5,     // - [.,.,1,.,1] - [.] - [1] - [.] - [1,.,.,.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 4,     // - [1,.,.,.,.] - [.] - [.] - [.] - [.,2,.,.,1]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 24,    // - [4,2,1,1,4] - [.] - [.] - [1] - [4,1,1,2,3]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 12,    // - [1,.,3,.,1] - [.] - [.] - [.] - [.,3,4,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 6,     // - [.,.,1,1,.] - [1] - [.] - [.] - [1,.,1,.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 5,     // - [.,.,1,.,1] - [.] - [1] - [.] - [1,.,.,.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 4,     // - [1,.,.,.,.] - [.] - [.] - [.] - [.,2,.,.,1]
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_REFERRER_SOURCE = array(
        self::TYPE_RANGE => array(
            // - [.,.,.,.,.] - 
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_GOOGLE,
                    self::PARAMETER_COUNT => 2,     // - [2,.,.,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_YAHOO,
                    self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_BING,
                    self::PARAMETER_COUNT => 3,     // - [2,.,.,1,.] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_EMPTY,
                    self::PARAMETER_COUNT => 1,     // - [.,.,1,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_DIRECT,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_GOOGLE,
                    self::PARAMETER_COUNT => 1,     // - [.,.,1,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_YAHOO,
                    self::PARAMETER_COUNT => 4,     // - [.,.,2,.,2] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_BING,
                    self::PARAMETER_COUNT => 3,     // - [.,.,1,.,2] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_GOOGLE,
                    self::PARAMETER_COUNT => 2,     // - [.,.,.,1,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_YAHOO,
                    self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_BING,
                    self::PARAMETER_COUNT => 2,     // - [.,1,1,.,.] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_EMPTY,
                    self::PARAMETER_COUNT => 1,     // - [.,.,1,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_DIRECT,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_GOOGLE,
                    self::PARAMETER_COUNT => 5,     // - [2,.,1,1,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_YAHOO,
                    self::PARAMETER_COUNT => 7,     // - [2,1,2,.,2] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_BING,
                    self::PARAMETER_COUNT => 8,     // - [2,1,2,1,2] - 
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [.,.,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.] - 
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_GOOGLE,
                    self::PARAMETER_COUNT => 3,     // - [2,.,.,.,.] - [.] - [.] - [.] - [x,x,x,1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_YAHOO,
                    self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_BING,
                    self::PARAMETER_COUNT => 3,     // - [2,.,.,1,.] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_EMPTY,
                    self::PARAMETER_COUNT => 1,     // - [.,.,1,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_DIRECT,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_GOOGLE,
                    self::PARAMETER_COUNT => 3,     // - [.,.,1,.,.] - [.] - [.] - [.] - [x,x,x,.,2]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_YAHOO,
                    self::PARAMETER_COUNT => 6,     // - [.,.,2,.,2] - [.] - [.] - [.] - [x,x,x,.,2]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_BING,
                    self::PARAMETER_COUNT => 4,     // - [.,.,1,.,2] - [.] - [.] - [.] - [x,x,x,.,1]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_EMPTY,
                    self::PARAMETER_COUNT => 15,    // - [.,.,.,.,.] - [.] - [.] - [.] - [5,5,5,.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_EMPTY,
                    self::PARAMETER_COUNT => 3,     // - [.,.,.,.,.] - [.] - [.] - [.] - [1,1,1,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_GOOGLE,
                    self::PARAMETER_COUNT => 6,     // - [.,.,.,1,1] - [1] - [1] - [.] - [x,x,x,1,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_YAHOO,
                    self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_BING,
                    self::PARAMETER_COUNT => 3,     // - [.,1,1,.,.] - [.] - [.] - [1] - [x,x,x,.,.] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_EMPTY,
                    self::PARAMETER_COUNT => 19,    // - [.,.,1,.,.] - [.] - [.] - [.] - [6,6,6,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_DIRECT,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_GOOGLE,
                    self::PARAMETER_COUNT => 12,    // - [2,.,1,1,1] - [1] - [1] - [.] - [x,x,x,2,3]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_YAHOO,
                    self::PARAMETER_COUNT => 9,     // - [2,1,2,.,2] - [.] - [.] - [.] - [x,x,x,.,2]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_BING,
                    self::PARAMETER_COUNT => 10,    // - [2,1,2,1,2] - [.] - [.] - [1] - [x,x,x,.,1]
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_SEARCH_TYPE = array(
        self::TYPE_RANGE => array(
            // - [.,.,.,.,.] - 
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_PPC,
                    self::PARAMETER_COUNT => 4,     // - [3,1,.,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_ORGANIC,
                    self::PARAMETER_COUNT => 3,     // - [2,.,.,1,.] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_PPC,
                    self::PARAMETER_COUNT => 5,     // - [.,.,5,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_ORGANIC,
                    self::PARAMETER_COUNT => 4,     // - [.,.,.,.,4] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_DIRECT,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_PPC,
                    self::PARAMETER_COUNT => 2,     // - [.,1,1,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_ORGANIC,
                    self::PARAMETER_COUNT => 3,     // - [1,.,.,1,1] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_PPC,
                    self::PARAMETER_COUNT => 11,    // - [3,2,6,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_ORGANIC,
                    self::PARAMETER_COUNT => 10,    // - [3,.,.,2,5] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_DIRECT,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - 
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [.,.,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.] - 
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_PPC,
                    self::PARAMETER_COUNT => 5,     // - [3,1,.,.,.] - [.] - [.] - [.] - [x,x,x,1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_ORGANIC,
                    self::PARAMETER_COUNT => 3,     // - [2,.,.,1,.] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_PPC,
                    self::PARAMETER_COUNT => 8,     // - [.,.,5,.,.] - [.] - [.] - [.] - [x,x,x,.,3]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_ORGANIC,
                    self::PARAMETER_COUNT => 6,     // - [.,.,.,.,4] - [.] - [.] - [.] - [x,x,x,.,2]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_DIRECT,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_EMPTY,
                    self::PARAMETER_COUNT => 15,    // - [.,.,.,.,.] - [.] - [.] - [.] - [5,5,5,.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_EMPTY,
                    self::PARAMETER_COUNT => 3,     // - [.,.,.,.,.] - [.] - [.] - [.] - [1,1,1,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_PPC,
                    self::PARAMETER_COUNT => 6,     // - [.,1,1,.,.] - [.] - [1] - [1] - [x,x,x,1,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_ORGANIC,
                    self::PARAMETER_COUNT => 4,     // - [1,.,.,1,1] - [1] - [.] - [.] - [x,x,x,.,.] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_EMPTY,
                    self::PARAMETER_COUNT => 18,    // - [.,.,.,.,.] - [.] - [.] - [.] - [6,6,6,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_PPC,
                    self::PARAMETER_COUNT => 19,    // - [3,2,6,.,.] - [.] - [1] - [1] - [x,x,x,2,4]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_ORGANIC,
                    self::PARAMETER_COUNT => 13,    // - [3,.,.,2,5] - [1] - [.] - [.] - [x,x,x,.,2]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_DIRECT,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_URL = array(
        self::TYPE_RANGE => array(
            // - [.,.,.,.,.] - 
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_1,
                    self::PARAMETER_COUNT => 4,     // - [3,.,.,1,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_2,
                    self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_3,
                    self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_EMPTY_STR,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_AUTO,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_1,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_2,
                    self::PARAMETER_COUNT => 4,     // - [.,.,3,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_3,
                    self::PARAMETER_COUNT => 3,     // - [.,.,1,.,2] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_1,
                    self::PARAMETER_COUNT => 3,     // - [1,.,.,1,1] -  
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_2,
                    self::PARAMETER_COUNT => 2,     // - [.,1,1,.,.] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_EMPTY_STR,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_AUTO,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_1,
                    self::PARAMETER_COUNT => 8,     // - [4,.,.,2,2] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_2,
                    self::PARAMETER_COUNT => 7,     // - [1,1,4,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_3,
                    self::PARAMETER_COUNT => 5,     // - [1,1,1,.,2] - 
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [.,.,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.] - 
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_1,
                    self::PARAMETER_COUNT => 4,     // - [3,.,.,1,.] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_2,
                    self::PARAMETER_COUNT => 2,     // - [1,.,.,.,.] - [.] - [.] - [.] - [x,x,x,1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_3,
                    self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_EMPTY_STR,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_AUTO,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_1,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_2,
                    self::PARAMETER_COUNT => 9,     // - [.,.,3,.,1] - [.] - [.] - [.] - [x,x,x,.,5]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_3,
                    self::PARAMETER_COUNT => 3,     // - [.,.,1,.,2] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_EMPTY,
                    self::PARAMETER_COUNT => 15,    // - [.,.,.,.,.] - [.] - [.] - [.] - [5,5,5,.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_EMPTY,
                    self::PARAMETER_COUNT => 3,     // - [.,.,.,.,.] - [.] - [.] - [.] - [1,1,1,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_1,
                    self::PARAMETER_COUNT => 3,     // - [1,.,.,1,1] - [.] - [.] - [.] - [x,x,x,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_2,
                    self::PARAMETER_COUNT => 5,     // - [.,1,1,.,.] - [.] - [1] - [.] - [x,x,x,1,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_3,
                    self::PARAMETER_COUNT => 2,     // - [.,.,.,.,.] - [1] - [.] - [1] - [x,x,x,.,.] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_EMPTY,
                    self::PARAMETER_COUNT => 18,    // - [.,.,.,.,.] - [.] - [.] - [.] - [6,6,6,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_EMPTY_STR,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_AUTO,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_1,
                    self::PARAMETER_COUNT => 8,     // - [4,.,.,2,2] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_2,
                    self::PARAMETER_COUNT => 16,    // - [1,1,4,.,1] - [.] - [1] - [.] - [x,x,x,2,6]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_3,
                    self::PARAMETER_COUNT => 7,     // - [1,1,1,.,2] - [1] - [.] - [1] - [x,x,x,.,.]
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_KEYWORD = array(
        self::TYPE_RANGE => array(
            // - [.,.,.,.,.] - 
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_2,
                    self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_3,
                    self::PARAMETER_COUNT => 4,     // - [3,.,.,1,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_4,
                    self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_EMPTY,
                    self::PARAMETER_COUNT => 1,     // - [.,.,1,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_DIRECT,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_1,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_2,
                    self::PARAMETER_COUNT => 4,     // - [.,.,2,.,2] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_3,
                    self::PARAMETER_COUNT => 2,     // - [.,.,1,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_4,
                    self::PARAMETER_COUNT => 1,     // - [.,.,1,.,.] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_2,
                    self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_3,
                    self::PARAMETER_COUNT => 2,     // - [.,1,1,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_4,
                    self::PARAMETER_COUNT => 2,     // - [.,.,.,1,1] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_EMPTY,
                    self::PARAMETER_COUNT => 1,     // - [.,.,1,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_DIRECT,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_1,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_2,
                    self::PARAMETER_COUNT => 6,     // - [2,.,2,.,2] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_3,
                    self::PARAMETER_COUNT => 8,     // - [3,1,2,1,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_4,
                    self::PARAMETER_COUNT => 5,     // - [1,1,1,1,1] - 
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [.,.,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.] - 
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_2,
                    self::PARAMETER_COUNT => 2,     // - [1,.,.,.,.] - [.] - [.] - [.] - [x,x,x,1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_3,
                    self::PARAMETER_COUNT => 4,     // - [3,1,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_4,
                    self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_EMPTY,
                    self::PARAMETER_COUNT => 1,     // - [.,.,1,.,.] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_DIRECT,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_1,
                    self::PARAMETER_COUNT => 2,     // - [.,.,.,.,1] - [.] - [.] - [.] - [x,x,x,.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_2,
                    self::PARAMETER_COUNT => 6,     // - [.,.,2,.,2] - [.] - [.] - [.] - [x,x,x,.,2]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_3,
                    self::PARAMETER_COUNT => 4,     // - [.,.,1,.,1] - [.] - [.] - [.] - [x,x,x,.,2]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_4,
                    self::PARAMETER_COUNT => 1,     // - [.,.,1,.,.] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_EMPTY,
                    self::PARAMETER_COUNT => 15,    // - [.,.,.,.,.] - [.] - [.] - [.] - [5,5,5,.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_EMPTY,
                    self::PARAMETER_COUNT => 3,     // - [.,.,.,.,.] - [.] - [.] - [.] - [1,1,1,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_2,
                    self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_3,
                    self::PARAMETER_COUNT => 7,     // - [.,1,1,.,.] - [1] - [1] - [1] - [x,x,x,1,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_4,
                    self::PARAMETER_COUNT => 2,     // - [.,.,.,1,1] - [.] - [.] - [.] - [x,x,x,.,.] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_EMPTY,
                    self::PARAMETER_COUNT => 19,    // - [.,.,1,.,.] - [.] - [.] - [.] - [6,6,6,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_DIRECT,
                    self::PARAMETER_COUNT => 1,     // - [.,.,.,.,1] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_1,
                    self::PARAMETER_COUNT => 2,     // - [.,.,.,.,1] - [.] - [.] - [.] - [x,x,x,.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_2,
                    self::PARAMETER_COUNT => 9,     // - [2,.,2,.,2] - [.] - [.] - [.] - [x,x,x,1,2]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_3,
                    self::PARAMETER_COUNT => 15,    // - [3,1,2,1,1] - [1] - [1] - [1] - [x,x,x,1,3]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_4,
                    self::PARAMETER_COUNT => 5,     // - [1,1,1,1,1] - [.] - [.] - [.] - [x,x,x,.,.]
                ),
            ),
        ),
    );
}
