<?php

require_once dirname(__FILE__) . '/QueryBuilderCallsByParameterBaseCompanyTree.php';

/**
 * Description of QueryBuilderCallsByParameterCompanyTreeBionicTest
 * 
 * @author fairdev
 */
class QueryBuilderCallsByParameterCompanyTreeAdvertiserBionicTest extends QueryBuilderCallsByParameterBaseCompanyTree {


    protected function configureFilterValuesAndResults() {

        $this->setFilterValues(self::$FILTER_VALUES);

        $this->setExpectedResultsByStatus(          self::$RESULTS_BY_STATUS);
        $this->setExpectedResultsByReferrerSource(  self::$RESULTS_BY_REFERRER_SOURCE);
        $this->setExpectedResultsBySearchType(      self::$RESULTS_BY_SEARCH_TYPE);
        $this->setExpectedResultsByUrl(             self::$RESULTS_BY_URL);
        $this->setExpectedResultsByKeyword(         self::$RESULTS_BY_KEYWORD);
    }

    protected function configureUserWithTz() {
        $this->configureCompanyTreeUserWithTz(
            null,
            self::ID_ADVERTISER_BIONIC_ADVERTISER,
            self::TIMEZONE_ID_GMT
        );
    }

    protected static $FILTER_VALUES = array(
        self::TYPE_RANGE => array(
            'from'  => '1323000085',
            'to'    => '1323000098',
        ),
        self::TYPE_ALL => array(
        ),
    );


    // NOTE:: Does not depend on type of calls
    protected static $RESULTS_BY_STATUS = array(
        self::TYPE_RANGE => array(
            // - [x,x,x,x,.,y,y,.,.] - 
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 5,     // - [1,.,1,.,1,.,2,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 3,     // - [.,.,.,1,.,1,.,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 3,     // - [.,.,1,1,.,.,.,1,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 2,     // - [1,.,.,.,1,.,.,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 3,     // - [.,1,.,.,.,1,.,.,1] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 5,     // - [1,.,1,.,1,.,2,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 3,     // - [.,.,.,1,.,1,.,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 3,     // - [.,.,1,1,.,.,.,1,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 2,     // - [1,.,.,.,1,.,.,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 3,     // - [.,1,.,.,.,1,.,.,1] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 5,     // - [1,.,1,.,1,.,2,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 3,     // - [.,.,.,1,.,1,.,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 3,     // - [.,.,1,1,.,.,.,1,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 2,     // - [1,.,.,.,1,.,.,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 3,     // - [.,1,.,.,.,1,.,.,1] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 5,     // - [1,.,1,.,1,.,2,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 3,     // - [.,.,.,1,.,1,.,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 3,     // - [.,.,1,1,.,.,.,1,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 2,     // - [1,.,.,.,1,.,.,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 3,     // - [.,1,.,.,.,1,.,.,1] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 5,     // - [1,.,1,.,1,.,2,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 3,     // - [.,.,.,1,.,1,.,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 3,     // - [.,.,1,1,.,.,.,1,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 2,     // - [1,.,.,.,1,.,.,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 3,     // - [.,1,.,.,.,1,.,.,1] - 
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [x,x,x,x,x] - [.,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,y,y,.,.] - [.,.]
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 17,    // - [.,.,1,.,1] - [1,.,1,1] - [.,.,1,1,1,.] - [1,.,.,.,1,.,.,1,.] - [1,.,1,.,1,.,2,.,.] - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 9,     // - [.,.,.,1,.] - [1,.,.,.] - [.,1,.,.,.,.] - [.,.,1,.,.,.,1,.,.] - [.,.,.,1,.,1,.,.,1] - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 10,    // - [1,.,1,.,1] - [.,.,.,1] - [.,.,.,.,.,1] - [.,1,.,.,.,.,.,.,1] - [.,.,1,1,.,.,.,1,.] - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 6,     // - [.,1,.,.,.] - [.,.,1,.] - [.,.,.,.,.,.] - [.,.,.,.,.,1,.,.,.] - [1,.,.,.,1,.,.,.,.] - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 9,     // - [1,.,.,1,.] - [.,1,.,.] - [1,.,.,.,.,.] - [.,.,.,1,.,.,.,.,.] - [.,1,.,.,.,1,.,.,1] - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_IN_PROGRESS,
                    self::PARAMETER_COUNT => 4,     // - [.,.,.,.,.] - [.,.,.,.] - [.,.,.,.,.,.] - [.,.,.,.,.,.,.,.,.] - [.,.,.,.,.,.,.,.,.] - [3,1]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 17,    // - [.,.,1,.,1] - [1,.,1,1] - [.,.,1,1,1,.] - [1,.,.,.,1,.,.,1,.] - [1,.,1,.,1,.,2,.,.] - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 9,     // - [.,.,.,1,.] - [1,.,.,.] - [.,1,.,.,.,.] - [.,.,1,.,.,.,1,.,.] - [.,.,.,1,.,1,.,.,1] - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 10,    // - [1,.,1,.,1] - [.,.,.,1] - [.,.,.,.,.,1] - [.,1,.,.,.,.,.,.,1] - [.,.,1,1,.,.,.,1,.] - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 6,     // - [.,1,.,.,.] - [.,.,1,.] - [.,.,.,.,.,.] - [.,.,.,.,.,1,.,.,.] - [1,.,.,.,1,.,.,.,.] - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 9,     // - [1,.,.,1,.] - [.,1,.,.] - [1,.,.,.,.,.] - [.,.,.,1,.,.,.,.,.] - [.,1,.,.,.,1,.,.,1] - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_IN_PROGRESS,
                    self::PARAMETER_COUNT => 4,     // - [.,.,.,.,.] - [.,.,.,.] - [.,.,.,.,.,.] - [.,.,.,.,.,.,.,.,.] - [.,.,.,.,.,.,.,.,.] - [3,1]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 17,    // - [.,.,1,.,1] - [1,.,1,1] - [.,.,1,1,1,.] - [1,.,.,.,1,.,.,1,.] - [1,.,1,.,1,.,2,.,.] - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 9,     // - [.,.,.,1,.] - [1,.,.,.] - [.,1,.,.,.,.] - [.,.,1,.,.,.,1,.,.] - [.,.,.,1,.,1,.,.,1] - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 10,    // - [1,.,1,.,1] - [.,.,.,1] - [.,.,.,.,.,1] - [.,1,.,.,.,.,.,.,1] - [.,.,1,1,.,.,.,1,.] - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 6,     // - [.,1,.,.,.] - [.,.,1,.] - [.,.,.,.,.,.] - [.,.,.,.,.,1,.,.,.] - [1,.,.,.,1,.,.,.,.] - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 9,     // - [1,.,.,1,.] - [.,1,.,.] - [1,.,.,.,.,.] - [.,.,.,1,.,.,.,.,.] - [.,1,.,.,.,1,.,.,1] - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_IN_PROGRESS,
                    self::PARAMETER_COUNT => 4,     // - [.,.,.,.,.] - [.,.,.,.] - [.,.,.,.,.,.] - [.,.,.,.,.,.,.,.,.] - [.,.,.,.,.,.,.,.,.] - [3,1]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 17,    // - [.,.,1,.,1] - [1,.,1,1] - [.,.,1,1,1,.] - [1,.,.,.,1,.,.,1,.] - [1,.,1,.,1,.,2,.,.] - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 9,     // - [.,.,.,1,.] - [1,.,.,.] - [.,1,.,.,.,.] - [.,.,1,.,.,.,1,.,.] - [.,.,.,1,.,1,.,.,1] - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 10,    // - [1,.,1,.,1] - [.,.,.,1] - [.,.,.,.,.,1] - [.,1,.,.,.,.,.,.,1] - [.,.,1,1,.,.,.,1,.] - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 6,     // - [.,1,.,.,.] - [.,.,1,.] - [.,.,.,.,.,.] - [.,.,.,.,.,1,.,.,.] - [1,.,.,.,1,.,.,.,.] - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 9,     // - [1,.,.,1,.] - [.,1,.,.] - [1,.,.,.,.,.] - [.,.,.,1,.,.,.,.,.] - [.,1,.,.,.,1,.,.,1] - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_IN_PROGRESS,
                    self::PARAMETER_COUNT => 4,     // - [.,.,.,.,.] - [.,.,.,.] - [.,.,.,.,.,.] - [.,.,.,.,.,.,.,.,.] - [.,.,.,.,.,.,.,.,.] - [3,1]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 17,    // - [.,.,1,.,1] - [1,.,1,1] - [.,.,1,1,1,.] - [1,.,.,.,1,.,.,1,.] - [1,.,1,.,1,.,2,.,.] - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 9,     // - [.,.,.,1,.] - [1,.,.,.] - [.,1,.,.,.,.] - [.,.,1,.,.,.,1,.,.] - [.,.,.,1,.,1,.,.,1] - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 10,    // - [1,.,1,.,1] - [.,.,.,1] - [.,.,.,.,.,1] - [.,1,.,.,.,.,.,.,1] - [.,.,1,1,.,.,.,1,.] - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 6,     // - [.,1,.,.,.] - [.,.,1,.] - [.,.,.,.,.,.] - [.,.,.,.,.,1,.,.,.] - [1,.,.,.,1,.,.,.,.] - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 9,     // - [1,.,.,1,.] - [.,1,.,.] - [1,.,.,.,.,.] - [.,.,.,1,.,.,.,.,.] - [.,1,.,.,.,1,.,.,1] - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_IN_PROGRESS,
                    self::PARAMETER_COUNT => 4,     // - [.,.,.,.,.] - [.,.,.,.] - [.,.,.,.,.,.] - [.,.,.,.,.,.,.,.,.] - [.,.,.,.,.,.,.,.,.] - [3,1]
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_REFERRER_SOURCE = array(
        self::TYPE_RANGE => array(
            // - [x,x,x,x,.,y,y,.,.] - 
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_GOOGLE,
                    self::PARAMETER_COUNT => 1,     // - [x,x,x,x,.,y,y,.,1] -  
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_YAHOO,
                    self::PARAMETER_COUNT => 1,     // - [x,x,x,x,1,y,y,.,.] -  
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_EMPTY,
                    self::PARAMETER_COUNT => 2,     // - [x,x,x,x,.,1,1,.,.] -  
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_EMPTY,
                    self::PARAMETER_COUNT => 2,     // - [x,x,x,x,.,1,1,.,.] -  
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_GOOGLE,
                    self::PARAMETER_COUNT => 2,     // - [x,x,x,x,.,y,y,1,1] -  
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_YAHOO,
                    self::PARAMETER_COUNT => 1,     // - [x,x,x,x,1,y,y,.,.] -  
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_EMPTY,
                    self::PARAMETER_COUNT => 4,     // - [x,x,x,x,.,2,2,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_GOOGLE,
                    self::PARAMETER_COUNT => 3,     // - [x,x,x,x,.,y,y,1,2] -  
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_YAHOO,
                    self::PARAMETER_COUNT => 2,     // - [x,x,x,x,2,y,y,.,.] -  
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [x,x,x,x,x] - [.,y,y,.] - [.,y,,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,y,y,.,.] - [.,.]
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_GOOGLE,
                    self::PARAMETER_COUNT => 1,     // - [x,x,x,x,x] - [.,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,y,y,.,.] - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_YAHOO,
                    self::PARAMETER_COUNT => 2,     // - [x,x,x,x,x] - [.,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,y,y,.,.] - [.,2]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_GOOGLE,
                    self::PARAMETER_COUNT => 4,     // - [x,x,x,x,x] - [.,y,y,1] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,y,y,.,1] - [2,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_YAHOO,
                    self::PARAMETER_COUNT => 3,     // - [x,x,x,x,x] - [1,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,1,x,x,x] - [x,x,x,x,1,y,y,.,.] - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_BING,
                    self::PARAMETER_COUNT => 1,     // - [x,x,x,x,x] - [.,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,y,y,.,.] - [1,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_EMPTY,
                    self::PARAMETER_COUNT => 4,     // - [x,x,x,x,x] - [.,1,1,.] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,1,1,.,.] - [.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_EMPTY,
                    self::PARAMETER_COUNT => 6,     // - [x,x,x,x,x] - [.,y,1,.] - [.,1,.,x,x,x] - [x,x,.,1,1,.,x,x,x] - [x,x,x,x,.,1,1,.,.] - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_GOOGLE,
                    self::PARAMETER_COUNT => 5,     // - [x,x,x,x,x] - [1,y,y,.] - [.,y,1,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,y,y,1,1] - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_YAHOO,
                    self::PARAMETER_COUNT => 2,     // - [x,x,x,x,x] - [.,y,y,.] - [1,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,1,y,y,.,.] - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_BING,
                    self::PARAMETER_COUNT => 3,     // - [x,x,x,x,x] - [.,y,y,1] - [.,y,.,x,x,x] - [x,x,1,y,y,.,x,x,x] - [x,x,x,x,.,y,y,.,.] - [1,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_EMPTY,
                    self::PARAMETER_COUNT => 10,    // - [x,x,x,x,x] - [.,1,2,.] - [.,1,.,x,x,x] - [x,x,.,1,1,.,x,x,x] - [x,x,x,x,.,2,2,.,.] - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_GOOGLE,
                    self::PARAMETER_COUNT => 10,    // - [x,x,x,x,x] - [1,y,y,1] - [.,1,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,y,y,1,2] - [2,2]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_YAHOO,
                    self::PARAMETER_COUNT => 7,     // - [x,x,x,x,x] - [1,y,y,.] - [1,.,x,x,x] - [x,x,.,y,y,1,x,x,x] - [x,x,x,x,2,y,y,.,.] - [.,2]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_BING,
                    self::PARAMETER_COUNT => 4,     // - [x,x,x,x,x] - [.,y,y,1] - [.,.,x,x,x] - [x,x,1,y,y,.,x,x,x] - [x,x,x,x,.,y,y,.,.] - [2,.]
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_SEARCH_TYPE = array(
        self::TYPE_RANGE => array(
            // - [x,x,x,x,.,y,y,.,.] - 
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_PPC,
                    self::PARAMETER_COUNT => 2,     // - [x,x,x,x,1,y,y,.,1] -
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_EMPTY,
                    self::PARAMETER_COUNT => 2,     // - [x,x,x,x,.,1,1,.,.] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_EMPTY,
                    self::PARAMETER_COUNT => 2,     // - [x,x,x,x,.,1,1,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_PPC,
                    self::PARAMETER_COUNT => 2,     // - [x,x,x,x,1,y,y,1,.] -
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_ORGANIC,
                    self::PARAMETER_COUNT => 1,     // - [x,x,x,x,.,y,y,.,1] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_EMPTY,
                    self::PARAMETER_COUNT => 4,     // - [x,x,x,x,.,2,2,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_PPC,
                    self::PARAMETER_COUNT => 4,     // - [x,x,x,x,2,y,y,1,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_ORGANIC,
                    self::PARAMETER_COUNT => 1,     // - [x,x,x,x,.,y,y,.,1] - 
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [x,x,x,x,x] - [.,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,y,y,.,.] - [.,.]
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_ORGANIC,
                    self::PARAMETER_COUNT => 3,     // - [x,x,x,x,x] - [.,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,y,y,.,.] - [.,3]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_PPC,
                    self::PARAMETER_COUNT => 6,     // - [x,x,x,x,x] - [1,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,1,y,y,.,1] - [3,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_ORGANIC,
                    self::PARAMETER_COUNT => 2,     // - [x,x,x,x,x] - [.,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,1,x,x,x] - [x,x,x,x,.,y,y,.,.] - [1,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_EMPTY,
                    self::PARAMETER_COUNT => 4,     // - [x,x,x,x,x] - [.,1,1,.] - [.,.,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,1,1,.,.] - [.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_EMPTY,
                    self::PARAMETER_COUNT => 6,     // - [x,x,x,x,x] - [.,y,1,.] - [.,1,.,x,x,x] - [x,x,.,1,1,.,x,x,x] - [x,x,x,x,.,1,1,.,.] - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_PPC,
                    self::PARAMETER_COUNT => 5,     // - [x,x,x,x,x] - [1,y,y,1] - [.,y,1,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,1,y,y,1,.] - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_ORGANIC,
                    self::PARAMETER_COUNT => 5,     // - [x,x,x,x,x] - [.,y,y,1] - [1,y,.,x,x,x] - [x,x,1,y,y,.,x,x,x] - [x,x,x,x,.,y,y,.,1] - [1,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_EMPTY,
                    self::PARAMETER_COUNT => 10,    // - [x,x,x,x,x] - [.,1,2,.] - [.,1,.,x,x,x] - [x,x,.,1,1,.,x,x,x] - [x,x,x,x,.,2,2,.,.] - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_PPC,
                    self::PARAMETER_COUNT => 11,    // - [x,x,x,x,x] - [2,y,y,1] - [.,y,1,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,2,y,y,1,1] - [3,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_ORGANIC,
                    self::PARAMETER_COUNT => 10,    // - [x,x,x,x,x] - [.,y,y,1] - [1,y,.,x,x,x] - [x,x,1,y,y,1,x,x,x] - [x,x,x,x,.,y,y,.,1] - [1,4]
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_URL = array(
        self::TYPE_RANGE => array(
            // - [x,x,x,x,.,y,y,.,.] - 
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_2,
                    self::PARAMETER_COUNT => 1,     // - [x,x,x,x,.,y,y,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_3,
                    self::PARAMETER_COUNT => 1,     // - [x,x,x,x,1,y,y,.,.] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_EMPTY,
                    self::PARAMETER_COUNT => 9,     // - [2,1,2,2,.,1,1,.,.] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_EMPTY,
                    self::PARAMETER_COUNT => 2,     // - [x,x,x,x,.,1,1,.,.] -  
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_2,
                    self::PARAMETER_COUNT => 2,     // - [x,x,x,x,1,y,y,1,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_1,
                    self::PARAMETER_COUNT => 1,     // - [x,x,x,x,.,y,y,.,1] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_EMPTY,
                    self::PARAMETER_COUNT => 11,    // - [2,1,2,2,.,2,2,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_1,
                    self::PARAMETER_COUNT => 1,     // - [x,x,x,x,.,y,y,.,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_2,
                    self::PARAMETER_COUNT => 3,     // - [x,x,x,x,1,y,y,1,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_3,
                    self::PARAMETER_COUNT => 1,     // - [x,x,x,x,1,y,y,.,.] - 
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [x,x,x,x,x] - [.,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,y,y,.,.] - [.,.]
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_1,
                    self::PARAMETER_COUNT => 1,     // - [x,x,x,x,x] - [.,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,y,y,.,.] - [.,1] 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_2,
                    self::PARAMETER_COUNT => 2,     // - [x,x,x,x,x] - [.,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,y,y,.,.] - [.,2] 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_1,
                    self::PARAMETER_COUNT => 5,     // - [x,x,x,x,x] - [1,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,1,x,x,x] - [x,x,x,x,.,y,y,.,.] - [3,.] 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_2,
                    self::PARAMETER_COUNT => 2,     // - [x,x,x,x,x] - [.,y,y,1] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,y,y,.,1] - [.,.] 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_3,
                    self::PARAMETER_COUNT => 1,     // - [x,x,x,x,x] - [.,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,1,y,y,.,.] - [.,.] 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_EMPTY,
                    self::PARAMETER_COUNT => 28,    // - [2,1,2,2,2] - [.,1,1,.] - [.,y,.,1,1,1] - [1,1,.,y,y,.,1,1,1] - [2,1,2,2,.,1,1,.,.] - [.,.] 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_EMPTY,
                    self::PARAMETER_COUNT => 6,     // - [x,x,x,x,x] - [.,y,1,.] - [.,1,.,x,x,x] - [x,x,.,1,1,.,x,x,x] - [x,x,x,x,.,1,1,.,.] - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_1,
                    self::PARAMETER_COUNT => 3,     // - [x,x,x,x,x] - [.,y,y,1] - [1,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,y,y,.,1] - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_2,
                    self::PARAMETER_COUNT => 4,     // - [x,x,x,x,x] - [.,y,y,.] - [.,y,1,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,1,y,y,1,.] - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_3,
                    self::PARAMETER_COUNT => 3,     // - [x,x,x,x,x] - [1,y,y,.] - [.,y,.,x,x,x] - [x,x,1,y,y,.,x,x,x] - [x,x,x,x,.,y,y,.,.] - [.,1]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_EMPTY,
                    self::PARAMETER_COUNT => 34,    // - [2,1,2,2,2] - [.,1,2,.] - [.,1,.,1,1,1] - [1,1,.,1,1,.,1,1,1] - [2,1,2,2,.,2,2,.,.] - [.,.] 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_1,
                    self::PARAMETER_COUNT => 9,     // - [x,x,x,x,x] - [1,y,y,1] - [1,y,.,x,x,x] - [x,x,.,y,y,1,x,x,x] - [x,x,x,x,.,y,y,.,1] - [3,1] 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_2,
                    self::PARAMETER_COUNT => 8,     // - [x,x,x,x,x] - [.,y,y,1] - [.,y,1,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,1,y,y,1,1] - [1,2] 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_3,
                    self::PARAMETER_COUNT => 4,     // - [x,x,x,x,x] - [1,y,y,.] - [.,y,.,x,x,x] - [x,x,1,y,y,.,x,x,x] - [x,x,x,x,1,y,y,.,.] - [.,1] 
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_KEYWORD = array(
        self::TYPE_RANGE => array(
            // - [x,x,x,x,.,y,y,.,.] - 
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_1,
                    self::PARAMETER_COUNT => 2,     // - [x,x,x,x,1,y,y,.,1] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_EMPTY,
                    self::PARAMETER_COUNT => 2,     // - [x,x,x,x,.,1,1,.,.] -
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_EMPTY,
                    self::PARAMETER_COUNT => 2,     // - [x,x,x,x,.,1,1,.,.] -
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_1,
                    self::PARAMETER_COUNT => 1,     // - [x,x,x,x,.,y,y,1,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_3,
                    self::PARAMETER_COUNT => 2,     // - [x,x,x,x,1,y,y,.,1] - 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_EMPTY,
                    self::PARAMETER_COUNT => 4,     // - [x,x,x,x,.,2,2,.,.] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_1,
                    self::PARAMETER_COUNT => 3,     // - [x,x,x,x,1,y,y,1,1] - 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_3,
                    self::PARAMETER_COUNT => 2,     // - [x,x,x,x,1,y,y,.,1] - 
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [x,x,x,x,x] - [.,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,y,y,.,.] - [.,.]
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_2,
                    self::PARAMETER_COUNT => 1,     // - [x,x,x,x,x] - [.,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [.,1] 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_3,
                    self::PARAMETER_COUNT => 1,     // - [x,x,x,x,x] - [.,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [.,1] 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_4,
                    self::PARAMETER_COUNT => 1,     // - [x,x,x,x,x] - [.,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [.,1] 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_1,
                    self::PARAMETER_COUNT => 4,     // - [x,x,x,x,x] - [.,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,1,x,x,x] - [x,x,x,x,1,y,y,.,1] - [1,.] 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_2,
                    self::PARAMETER_COUNT => 2,     // - [x,x,x,x,x] - [.,y,y,1] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [1,.] 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_3,
                    self::PARAMETER_COUNT => 1,     // - [x,x,x,x,x] - [.,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,y,y,.,.] - [1,.] 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_4,
                    self::PARAMETER_COUNT => 1,     // - [x,x,x,x,x] - [.,y,y,.] - [1,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [.,.] 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_EMPTY,
                    self::PARAMETER_COUNT => 4,     // - [x,x,x,x,x] - [.,y,2,.] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,1,1,.,.] - [.,.] 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_EMPTY,
                    self::PARAMETER_COUNT => 6,     // - [x,x,x,x,x] - [.,1,y,.] - [.,1,.,x,x,x] - [x,x,.,1,1,.,x,x,x] - [x,x,x,x,.,1,1,.,.] - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_1, 
                    self::PARAMETER_COUNT => 1,     // - [x,x,x,x,x] - [.,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,y,y,1,.] - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_2,
                    self::PARAMETER_COUNT => 2,     // - [x,x,x,x,x] - [.,y,y,.] - [.,y,1,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,y,y,.,.] - [.,1] 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_3,
                    self::PARAMETER_COUNT => 6,     // - [x,x,x,x,x] - [1,y,y,1] - [.,y,.,x,x,x] - [x,x,1,y,y,.,x,x,x] - [x,x,x,x,1,y,y,.,1] - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_4,
                    self::PARAMETER_COUNT => 1,     // - [x,x,x,x,x] - [1,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,x,x,.,y,y,.,.] - [.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_EMPTY,
                    self::PARAMETER_COUNT => 10,    // - [x,x,x,x,x] - [.,1,2,.] - [.,1,.,x,x,x] - [x,x,.,1,1,.,x,x,x] - [x,x,x,x,.,2,2,.,.] - [.,.] 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_1,
                    self::PARAMETER_COUNT => 5,     // - [x,x,x,x,x] - [.,y,y,.] - [.,y,.,x,x,x] - [x,x,.,y,y,1,x,x,x] - [x,x,x,x,1,y,y,1,1] - [1,.] 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_2,
                    self::PARAMETER_COUNT => 5,     // - [x,x,x,x,x] - [.,y,y,1] - [.,y,1,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [1,2] 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_3,
                    self::PARAMETER_COUNT => 8,     // - [x,x,x,x,x] - [1,y,y,1] - [.,y,.,x,x,x] - [x,x,1,y,y,.,x,x,x] - [x,x,x,x,1,y,y,.,1] - [2,1] 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_4,
                    self::PARAMETER_COUNT => 3,     // - [x,x,x,x,x] - [1,y,y,.] - [1,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [x,x,.,y,y,.,x,x,x] - [.,1] 
                ),
            ),
        ),
    );
}
