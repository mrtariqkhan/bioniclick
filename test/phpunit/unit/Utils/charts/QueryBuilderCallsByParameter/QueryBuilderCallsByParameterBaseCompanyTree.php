<?php

require_once dirname(__FILE__) . '/QueryBuilderCallsByParameterBaseProvided.php';

/**
 * Description of QueryBuilderCallsByParameterBaseCompanyTree
 * 
 * @author fairdev
 */
abstract class QueryBuilderCallsByParameterBaseCompanyTree extends QueryBuilderCallsByParameterBaseProvided {

    const TYPE_RANGE    = 'time range';
    const TYPE_ALL      = 'all time';

    protected static $PARAMETER_TYPES = array(
        QueryBuilderCallsByParameter::TYPE_BY_STATUS,
        QueryBuilderCallsByParameter::TYPE_BY_REFERRER_SOURCE,
        QueryBuilderCallsByParameter::TYPE_BY_SEARCH_TYPE,
        QueryBuilderCallsByParameter::TYPE_BY_URL,
        QueryBuilderCallsByParameter::TYPE_BY_KEYWORD,
    );
    protected static $TIME_RANGES = array(
        self::TYPE_RANGE,
        self::TYPE_ALL,
    );

    const VALUE_REFERRER_SOURCE_GOOGLE  = 'www.google.com';
    const VALUE_REFERRER_SOURCE_YAHOO   = 'www.yahoo.com';
    const VALUE_REFERRER_SOURCE_BING    = 'www.bing.com';
    const VALUE_REFERRER_SOURCE_DIRECT  = 'Direct';
    const VALUE_REFERRER_SOURCE_EMPTY   = null;

    const VALUE_KEYWORD_1       = 'keyword 1';
    const VALUE_KEYWORD_2       = 'keyword 2';
    const VALUE_KEYWORD_3       = 'keyword 3';
    const VALUE_KEYWORD_4       = 'keyword 4';
    const VALUE_KEYWORD_55      = 'keyword 55';
    const VALUE_KEYWORD_77      = 'keyword 77';
    const VALUE_KEYWORD_99      = 'keyword 99';
    const VALUE_KEYWORD_DIRECT  = 'Direct';
    const VALUE_KEYWORD_EMPTY   = null;

    const VALUE_SEARCH_TYPE_PPC     = '0';
    const VALUE_SEARCH_TYPE_ORGANIC = '1';
    const VALUE_SEARCH_TYPE_DIRECT  = '2';
    const VALUE_SEARCH_TYPE_EMPTY   = null;

    const VALUE_URL_1           = 'http://first-site.html';
    const VALUE_URL_2           = 'www.second-site.html';
    const VALUE_URL_3           = 'https://third-site.php';
    const VALUE_URL_EMPTY_STR   = '';
    const VALUE_URL_EMPTY       = null;

    const VALUE_STATUS_NOT_YET_DIALED   = TwilioIncomingCallTable::STATUS_NOT_YET_DIALED;
    const VALUE_STATUS_IN_PROGRESS      = TwilioIncomingCallTable::STATUS_IN_PROGRESS ;
    const VALUE_STATUS_COMPLETED        = TwilioIncomingCallTable::STATUS_COMPLETED;
    const VALUE_STATUS_BUSY             = TwilioIncomingCallTable::STATUS_BUSY;
    const VALUE_STATUS_FAILED           = TwilioIncomingCallTable::STATUS_FAILED;
    const VALUE_STATUS_NO_ANSWER        = TwilioIncomingCallTable::STATUS_NO_ANSWER;


    const ID_COMPANY_BIONIC                     = 1;
    const ID_COMPANY_RESELLER_ACQUISIO          = 2;
    const ID_COMPANY_AGENCY_REALSEARCH          = 4;
    const ID_ADVERTISER_BIONIC_ADVERTISER       = 1;
    const ID_ADVERTISER_RESELLER_ACQUISIO_TRIPS = 3;
    const ID_ADVERTISER_AGENCY_REALSEARCH_IPS   = 4;
    const ID_ADVERTISER_AGENCY_SNOW             = 5;

    /**
     *
     * @dataProvider providerCases
     * 
     * @param string $timeRangeType one of self::TYPE_*
     * @param string $parameterType one of QueryBuilderCallsByParameter::TYPE_BY_*
     * @param string $testKey one of QueryBuilderCallsByParameterTz4Test::TYPE_*
     * @param string $specificAdditionalMessage
     */
    public function testCase(
        $timeRangeType,
        $parameterType, 
        $visitorAnalyticsTypeCode,
        $vaTypeDescription,
        $specificAdditionalMessage = ''
    ) {

        $expectedResultArray = array();
        switch ($parameterType) {
            case QueryBuilderCallsByParameter::TYPE_BY_STATUS:
                $builderClassName = 'QueryBuilderCallsByStatus';
                $expectedResultArray = $this->getExpectedResultsByStatus();
                break;
            case QueryBuilderCallsByParameter::TYPE_BY_REFERRER_SOURCE:
                $builderClassName = 'QueryBuilderCallsByReferrerSource';
                $expectedResultArray = $this->getExpectedResultsByReferrerSource();
                break;
            case QueryBuilderCallsByParameter::TYPE_BY_SEARCH_TYPE:
                $builderClassName = 'QueryBuilderCallsBySearchType';
                $expectedResultArray = $this->getExpectedResultsBySearchType();
                break;
            case QueryBuilderCallsByParameter::TYPE_BY_URL:
                $builderClassName = 'QueryBuilderCallsByUrl';
                $expectedResultArray = $this->getExpectedResultsByUrl();
                break;
            case QueryBuilderCallsByParameter::TYPE_BY_KEYWORD:
                $builderClassName = 'QueryBuilderCallsByKeyword';
                $expectedResultArray = $this->getExpectedResultsByKeyword();
                break;
            default:
                throw new Exception("Invalid QueryBuilderCallsByParameter type");
        }

        $filterValues = $this->getFilterValues();
        $filterValues = $filterValues[$timeRangeType];

        $queryResult = $this->getResultArray(
            $filterValues, 
            $builderClassName, 
            $visitorAnalyticsTypeCode
        );
        $needResult = $expectedResultArray[$timeRangeType][$visitorAnalyticsTypeCode];

        $chartKind = "'$parameterType' chart";
//var_dump($needResult);
//var_dump('=++++++++++++++++++++');
//var_dump($queryResult);
//var_dump('------------------------------------------------------');
        $this->assertArrayAreSame(
            $needResult, 
            $queryResult, 
<<<EOD
    Error in Test ($vaTypeDescription)
        (an attempt to find $chartKind for '{$this->userKind}' user with timezone '{$this->timezoneName}' in '$timeRangeType' time range).
        $specificAdditionalMessage
EOD
        );
    }

    public function providerCases() {

        $cases = array();
        $vaTypes = VisitorAnalyticsTable::getTypeDescriptions();
        $i = 0;
        foreach (self::$PARAMETER_TYPES as $parameterType) {
            foreach (self::$TIME_RANGES as $timeRangeType) {
                foreach ($vaTypes as $vaTypeCode => $vaTypeDescription) {
                    $cases['case ' . ++$i . ": $timeRangeType $parameterType $vaTypeDescription"] = array(
                        $timeRangeType, 
                        $parameterType, 
                        $vaTypeCode, 
                        $vaTypeDescription
                    );
                }
            }
        }

        return $cases;
    }
}
