<?php

require_once dirname(__FILE__) . '/QueryBuilderCallsByParameterBase.php';

/**
 * Description of QueryBuilderCallsByParameterBaseTz
 * 
 * @author fairdev
 */
abstract class QueryBuilderCallsByParameterBaseProvided extends QueryBuilderCallsByParameterBase {

    protected $filterValues         = array();
    protected $expectedResultsByStatus          = array();
    protected $expectedResultsByReferrerSource  = array();
    protected $expectedResultsBySearchType      = array();
    protected $expectedResultsByUrl             = array();
    protected $expectedResultsByKeyword         = array();


    protected function _start() {

        parent::_start();

        $this->configureFilterValuesAndResults();
    }

    abstract protected function configureFilterValuesAndResults();


    public function setFilterValues(array $filterValues) {
        $this->filterValues = $filterValues;
    }

    public function setExpectedResultsByStatus(array $expectedResultsByStatus) {
        $this->expectedResultsByStatus = $expectedResultsByStatus;
    }

    public function setExpectedResultsByReferrerSource(array $expectedResultsByReferrerSource) {
        $this->expectedResultsByReferrerSource = $expectedResultsByReferrerSource;
    }

    public function setExpectedResultsBySearchType(array $expectedResultsBySearchType) {
        $this->expectedResultsBySearchType = $expectedResultsBySearchType;
    }

    public function setExpectedResultsByUrl(array $expectedResultsByUrl) {
        $this->expectedResultsByUrl = $expectedResultsByUrl;
    }

    public function setExpectedResultsByKeyword(array $expectedResultsByKeyword) {
        $this->expectedResultsByKeyword = $expectedResultsByKeyword;
    }


    public function getFilterValues() {
        return $this->filterValues;
    }

    public function getExpectedResultsByStatus() {
        return $this->expectedResultsByStatus;
    }

    public function getExpectedResultsByReferrerSource() {
        return $this->expectedResultsByReferrerSource;
    }

    public function getExpectedResultsBySearchType() {
        return $this->expectedResultsBySearchType;
    }

    public function getExpectedResultsByUrl() {
        return $this->expectedResultsByUrl;
    }

    public function getExpectedResultsByKeyword() {
        return $this->expectedResultsByKeyword;
    }
}
