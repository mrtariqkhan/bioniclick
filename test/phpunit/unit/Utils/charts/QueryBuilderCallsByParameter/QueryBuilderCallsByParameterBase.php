<?php

require_once dirname(__FILE__) . '/../../../UserTimezoneTestBase.php';

/**
 * Description of QueryBuilderCallsByParameterBase
 * 
 * @author fairdev
 */
abstract class QueryBuilderCallsByParameterBase extends UserTimezoneTestBase {

    const PARAMETER_COUNT = QueryBuilderCallsByParameter::RESULT_VALUE_CALLS_COUNT;
    const PARAMETER_VALUE = QueryBuilderCallsByParameter::RESULT_VALUE_BY;


    public function assertArrayAreSame($expectedArray, array $actualArray, $message) {

        $expectedArray = empty($expectedArray) 
            ? array() 
            : $expectedArray
        ;

        foreach ($expectedArray as $expectedArr) {

            $expectedParameter = $expectedArr[self::PARAMETER_VALUE];
            $expectedCount = intval($expectedArr[self::PARAMETER_COUNT]);

            $actualParameter = '';
            $actualCount = -1;
            foreach ($actualArray as $i => $actualArr) {
                if (
                            is_null($expectedParameter) && is_null($actualArr[self::PARAMETER_VALUE])
                        || !is_null($expectedParameter) && $actualArr[self::PARAMETER_VALUE] === $expectedParameter
                ) {
                    $actualParameter = $actualArr[self::PARAMETER_VALUE];
                    $actualCount = $actualArr[self::PARAMETER_COUNT];
                    unset($actualArr[$i]);
                    break;
                }
            }

            $this->assertEquals(
                $expectedParameter, 
                $actualParameter, 
                "$message (actual results has no info about '$expectedParameter')"
            );
            $this->assertEquals(
                $expectedCount, 
                $actualCount, 
                "$message (actual count of '$expectedParameter' is wrong)"
            );
        }

        $this->assertEquals(
            count($expectedArray), 
            count($actualArray), 
            "$message (size of actual array is wrong)"
        );
    }


    /**
     *
     * @param array $filterValues
     * @param string $builderClassName
     * @param type $visitorAnalyticsTypeCode
     * @return type 
     */
    protected function getResultArray(
        $filterValues,
        $builderClassName,
        $visitorAnalyticsTypeCode
    ) {

        $user = $this->getUser();

        $advertiser = null;
        $company    = null;
        if ($user->isAdvertiserUser()) {
            $advertiser = $user->getAdvertiser();
        } else {
            $company = $user->getFirstCompany();
        }
        $queryBuilder = new $builderClassName(
            $advertiser,
            $company,
            $this->getGmtDifference(),
            $filterValues,
            $visitorAnalyticsTypeCode
        );

        $sqlQueryString = $queryBuilder->getSqlQueryString();
        unset($queryBuilder);
//echoln ($sqlQueryString);
        $queryResult = array();
        if (!empty($sqlQueryString)) {
            $conn = Doctrine_Manager::getInstance()->getConnection('doctrine');
            $queryResult = $conn->fetchAssoc($sqlQueryString, array());
        }

        return $queryResult;
    }
}
