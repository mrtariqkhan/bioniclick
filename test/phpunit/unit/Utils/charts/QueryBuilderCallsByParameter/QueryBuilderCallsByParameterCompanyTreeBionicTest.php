<?php

require_once dirname(__FILE__) . '/QueryBuilderCallsByParameterBaseCompanyTree.php';

/**
 * Description of QueryBuilderCallsByParameterCompanyTreeBionicTest
 * 
 * @author fairdev
 */
class QueryBuilderCallsByParameterCompanyTreeBionicTest extends QueryBuilderCallsByParameterBaseCompanyTree {

    const VALUE_URL_AUTO = 'http://snowing.com/index.html?cmbid=00000000000000000000000000000004';

    protected function configureFilterValuesAndResults() {

        $this->setFilterValues(self::$FILTER_VALUES);

        $this->setExpectedResultsByStatus(          self::$RESULTS_BY_STATUS);
        $this->setExpectedResultsByReferrerSource(  self::$RESULTS_BY_REFERRER_SOURCE);
        $this->setExpectedResultsBySearchType(      self::$RESULTS_BY_SEARCH_TYPE);
        $this->setExpectedResultsByUrl(             self::$RESULTS_BY_URL);
        $this->setExpectedResultsByKeyword(         self::$RESULTS_BY_KEYWORD);
    }

    protected function configureUserWithTz() {
        $this->configureCompanyTreeUserWithTz(
            self::ID_COMPANY_BIONIC,
            null,
            self::TIMEZONE_ID_GMT
        );
    }

    protected static $FILTER_VALUES = array(
        self::TYPE_RANGE => array(
            'from'  => '1323000031',
            'to'    => '1323000050',
        ),
        self::TYPE_ALL => array(
        ),
    );


    // NOTE:: Does not depend on type of calls
    protected static $RESULTS_BY_STATUS = array(
        self::TYPE_RANGE => array(
            // - [y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x]
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,                  
                    self::PARAMETER_COUNT => 13,    // [.,1,1] - 2,2 - [.] - 2 - [.,1,.,1,1,2,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 2,     // [.,.,.] - .,. - [.] - . - [.,.,1,.,.,.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 2,     // [.,.,1] - .,. - [.] - . - [.,.,1,.,.,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 3,     // [.,1,.] - .,. - [1] - . - [1,.,.,.,.,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 3,     // [1,.,.] - .,. - [.] - . - [1,.,.,.,1,.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 13,    // [.,1,1] - 2,2 - [.] - 2 - [.,1,.,1,1,2,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 2,     // [.,.,.] - .,. - [.] - . - [.,.,1,.,.,.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 2,     // [.,.,1] - .,. - [.] - . - [.,.,1,.,.,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 3,     // [.,1,.] - .,. - [1] - . - [1,.,.,.,.,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 3,     // [1,.,.] - .,. - [.] - . - [1,.,.,.,1,.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 13,    // [.,1,1] - 2,2 - [.] - 2 - [.,1,.,1,1,2,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 2,     // [.,.,.] - .,. - [.] - . - [.,.,1,.,.,.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 2,     // [.,.,1] - .,. - [.] - . - [.,.,1,.,.,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 3,     // [.,1,.] - .,. - [1] - . - [1,.,.,.,.,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 3,     // [1,.,.] - .,. - [.] - . - [1,.,.,.,1,.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 13,    // [.,1,1] - 2,2 - [.] - 2 - [.,1,.,1,1,2,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 2,     // [.,.,.] - .,. - [.] - . - [.,.,1,.,.,.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 2,     // [.,.,1] - .,. - [.] - . - [.,.,1,.,.,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 3,     // [.,1,.] - .,. - [1] - . - [1,.,.,.,.,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 3,     // [1,.,.] - .,. - [.] - . - [1,.,.,.,1,.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 13,    // [.,1,1] - 2,2 - [.] - 2 - [.,1,.,1,1,2,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 2,     // [.,.,.] - .,. - [.] - . - [.,.,1,.,.,.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 2,     // [.,.,1] - .,. - [.] - . - [.,.,1,.,.,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 3,     // [.,1,.] - .,. - [1] - . - [1,.,.,.,.,.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 3,     // [1,.,.] - .,. - [.] - . - [1,.,.,.,1,.,.]
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - x - [.,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 99,    // 2 - [4,2,1,1,4] - 2,4,1,2 - [.,.,1,.,1] - 5,4 - [.] - 1 - [1,.,1,1] - 2,2 - [.] - 2 - [.,1,.,1,1,2,.,1,2,.,.] - .,2,1,1 - [1,.,.,.,2,.,.,1,1,1,1,.] - 1,2 - [1] - . - [1,.,1,.,1,.,2,.,.] - 1,.,3 - [4,1,1,2,3] - 3,2,5,1 - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 33,    // 1 - [1,.,3,.,1] - 1,1,.,. - [.,.,.,1,.] - .,1 - [.] - . - [1,.,.,.] - .,. - [.] - . - [.,.,1,.,.,.,1,.,.,.,.] - 1,.,.,. - [.,1,1,.,.,.,1,1,.,.,.,.] - .,. - [.] - 1 - [.,.,.,1,.,1,.,.,1] - .,1,. - [.,3,4,.,.] - .,1,.,1 - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 34,    // 1 - [.,.,1,1,.] - 2,.,1,. - [1,.,1,.,1] - .,1 - [1] - . - [.,.,.,1] - .,. - [.] - . - [.,.,1,.,.,.,.,.,.,1,1] - .,.,.,. - [.,1,1,1,.,.,.,.,.,.,.,2] - .,. - [.] - 1 - [.,.,1,1,.,.,.,1,.] - .,.,1 - [1,.,1,.,1] - 1,2,.,2 - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 22,    // 1 - [.,.,1,.,1] - .,.,.,. - [.,1,.,.,.] - 1,. - [.] - . - [.,.,1,.] - .,. - [1] - . - [1,.,.,.,.,.,.,.,.,.,.] - 1,.,.,. - [1,.,.,.,.,1,1,.,.,.,.,.] - .,. - [.] - . - [1,.,.,.,1,.,.,.,.] - 1,1,1 - [1,.,.,.,1] - .,.,.,1 - [1,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 28,    // 1 - [1,.,.,.,.] - 1,1,.,. - [1,.,.,1,.] - .,. - [.] - 1 - [.,1,.,.] - .,. - [.] - . - [1,.,.,.,1,.,.,.,.,.,1] - .,.,.,1 - [.,.,.,1,.,.,.,.,..1,.,.] - 1,. - [.] - . - [.,1,.,.,.,1,.,.,1] - .,.,1 - [.,2,.,.,1] - 2,1,1,1 - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_IN_PROGRESS,
                    self::PARAMETER_COUNT => 6,     // . - [.,.,.,.,.] - .,.,.,. - [.,.,.,.,.] - .,. - [.] - . - [.,.,.,.] - .,. - [.] - . - [.,.,.,.,.,.,.,.,.,.,.] - .,.,.,. - [.,.,.,.,.,.,.,.,..1,.,.] - .,. - [.] - . - [.,.,.,.,.,.,.,.,.] - .,.,. - [.,.,.,.,.] - .,.,.,. - [3,3]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 99,    // 2 - [4,2,1,1,4] - 2,4,1 - [2,.,.,1,.,1] - 5,4 - [.] - [1,1,.,1,1] - 2,2 - [.] - 2 - [.,1,.,1,1,2,.,1,2,.,.] - .,2,1,1 - [1,.,.,.,2,.,.,1,1,1,1,.] - 1,2,1,. - [1,.,1,.,1,.,2,.,.] - 1,.,3 - [4,1,1,2,3] - 3,2,5,1 - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 33,    // 1 - [1,.,3,.,1] - 1,1,. - [.,.,.,.,1,.] - .,1 - [.] - [.,1,.,.,.] - .,. - [.] - . - [.,.,1,.,.,.,1,.,.,.,.] - 1,.,.,. - [.,1,1,.,.,.,1,1,.,.,.,.] - .,.,.,1 - [.,.,.,1,.,1,.,.,1] - .,1,. - [.,3,4,.,.] - .,1,.,1 - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 34,    // 1 - [.,.,1,1,.] - 2,.,1 - [.,1,.,1,.,1] - .,1 - [1] - [.,.,.,.,1] - .,. - [.] - . - [.,.,1,.,.,.,.,.,.,1,1] - .,.,.,. - [.,1,1,1,.,.,.,.,.,.,.,2] - .,.,.,1 - [.,.,1,1,.,.,.,1,.] - .,.,1 - [1,.,1,.,1] - 1,2,.,2 - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 22,    // 1 - [.,.,1,.,1] - .,.,. - [.,.,1,.,.,.] - 1,. - [.] - [.,.,.,1,.] - .,. - [1] - . - [1,.,.,.,.,.,.,.,.,.,.] - 1,.,.,. - [1,.,.,.,.,1,1,.,.,.,.,.] - .,.,.,. - [1,.,.,.,1,.,.,.,.] - 1,1,1 - [1,.,.,.,1] - .,.,.,1 - [1,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 28,    // 1 - [1,.,.,.,.] - 1,1,. - [.,1,.,.,1,.] - .,. - [.] - [1,.,1,.,.] - .,. - [.] - . - [1,.,.,.,1,.,.,.,.,.,1] - .,.,.,1 - [.,.,.,1,.,.,.,.,..1,.,.] - 1,.,.,. - [.,1,.,.,.,1,.,.,1] - .,.,1 - [.,2,.,.,1] - 2,1,1,1 - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_IN_PROGRESS,
                    self::PARAMETER_COUNT => 6,     // . - [.,.,.,.,.] - .,.,. - [.,.,.,.,.,.] - .,. - [.] - [.,.,.,.,.] - .,. - [.] - . - [.,.,.,.,.,.,.,.,.,.,.] - .,.,.,. - [.,.,.,.,.,.,.,.,..1,.,.] - .,.,.,. - [.,.,.,.,.,.,.,.,.] - .,.,. - [.,.,.,.,.] - .,.,.,. - [3,3]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 99,    // 2 - [4,2,1,1,4] - 2,4,1 - [2,.,.,1,.,1] - 5,4 - [.] - [1,1,.,1,1] - 2,2 - [.] - 2 - [.,1,.,1,1,2,.,1,2,.,.] - .,2,1,1 - [1,.,.,.,2,.,.,1,1,1,1,.] - 1,2,1,. - [1,.,1,.,1,.,2,.,.] - 1,.,3 - [4,1,1,2,3] - 3,2,5,1 - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 33,    // 1 - [1,.,3,.,1] - 1,1,. - [.,.,.,.,1,.] - .,1 - [.] - [.,1,.,.,.] - .,. - [.] - . - [.,.,1,.,.,.,1,.,.,.,.] - 1,.,.,. - [.,1,1,.,.,.,1,1,.,.,.,.] - .,.,.,1 - [.,.,.,1,.,1,.,.,1] - .,1,. - [.,3,4,.,.] - .,1,.,1 - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 34,    // 1 - [.,.,1,1,.] - 2,.,1 - [.,1,.,1,.,1] - .,1 - [1] - [.,.,.,.,1] - .,. - [.] - . - [.,.,1,.,.,.,.,.,.,1,1] - .,.,.,. - [.,1,1,1,.,.,.,.,.,.,.,2] - .,.,.,1 - [.,.,1,1,.,.,.,1,.] - .,.,1 - [1,.,1,.,1] - 1,2,.,2 - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 22,    // 1 - [.,.,1,.,1] - .,.,. - [.,.,1,.,.,.] - 1,. - [.] - [.,.,.,1,.] - .,. - [1] - . - [1,.,.,.,.,.,.,.,.,.,.] - 1,.,.,. - [1,.,.,.,.,1,1,.,.,.,.,.] - .,.,.,. - [1,.,.,.,1,.,.,.,.] - 1,1,1 - [1,.,.,.,1] - .,.,.,1 - [1,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 28,    // 1 - [1,.,.,.,.] - 1,1,. - [.,1,.,.,1,.] - .,. - [.] - [1,.,1,.,.] - .,. - [.] - . - [1,.,.,.,1,.,.,.,.,.,1] - .,.,.,1 - [.,.,.,1,.,.,.,.,..1,.,.] - 1,.,.,. - [.,1,.,.,.,1,.,.,1] - .,.,1 - [.,2,.,.,1] - 2,1,1,1 - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_IN_PROGRESS,
                    self::PARAMETER_COUNT => 6,     // . - [.,.,.,.,.] - .,.,. - [.,.,.,.,.,.] - .,. - [.] - [.,.,.,.,.] - .,. - [.] - . - [.,.,.,.,.,.,.,.,.,.,.] - .,.,.,. - [.,.,.,.,.,.,.,.,..1,.,.] - .,.,.,. - [.,.,.,.,.,.,.,.,.] - .,.,. - [.,.,.,.,.] - .,.,.,. - [3,3]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 99,    // 2 - [4,2,1,1,4] - 2,4,1 - [2,.,.,1,.,1] - 5,4 - [.] - [1,1,.,1,1] - 2,2 - [.] - 2 - [.,1,.,1,1,2,.,1,2,.,.] - .,2,1,1 - [1,.,.,.,2,.,.,1,1,1,1,.] - 1,2,1,. - [1,.,1,.,1,.,2,.,.] - 1,.,3 - [4,1,1,2,3] - 3,2,5,1 - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 33,    // 1 - [1,.,3,.,1] - 1,1,. - [.,.,.,.,1,.] - .,1 - [.] - [.,1,.,.,.] - .,. - [.] - . - [.,.,1,.,.,.,1,.,.,.,.] - 1,.,.,. - [.,1,1,.,.,.,1,1,.,.,.,.] - .,.,.,1 - [.,.,.,1,.,1,.,.,1] - .,1,. - [.,3,4,.,.] - .,1,.,1 - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 34,    // 1 - [.,.,1,1,.] - 2,.,1 - [.,1,.,1,.,1] - .,1 - [1] - [.,.,.,.,1] - .,. - [.] - . - [.,.,1,.,.,.,.,.,.,1,1] - .,.,.,. - [.,1,1,1,.,.,.,.,.,.,.,2] - .,.,.,1 - [.,.,1,1,.,.,.,1,.] - .,.,1 - [1,.,1,.,1] - 1,2,.,2 - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 22,    // 1 - [.,.,1,.,1] - .,.,. - [.,.,1,.,.,.] - 1,. - [.] - [.,.,.,1,.] - .,. - [1] - . - [1,.,.,.,.,.,.,.,.,.,.] - 1,.,.,. - [1,.,.,.,.,1,1,.,.,.,.,.] - .,.,.,. - [1,.,.,.,1,.,.,.,.] - 1,1,1 - [1,.,.,.,1] - .,.,.,1 - [1,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 28,    // 1 - [1,.,.,.,.] - 1,1,. - [.,1,.,.,1,.] - .,. - [.] - [1,.,1,.,.] - .,. - [.] - . - [1,.,.,.,1,.,.,.,.,.,1] - .,.,.,1 - [.,.,.,1,.,.,.,.,..1,.,.] - 1,.,.,. - [.,1,.,.,.,1,.,.,1] - .,.,1 - [.,2,.,.,1] - 2,1,1,1 - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_IN_PROGRESS,
                    self::PARAMETER_COUNT => 6,     // . - [.,.,.,.,.] - .,.,. - [.,.,.,.,.,.] - .,. - [.] - [.,.,.,.,.] - .,. - [.] - . - [.,.,.,.,.,.,.,.,.,.,.] - .,.,.,. - [.,.,.,.,.,.,.,.,..1,.,.] - .,.,.,. - [.,.,.,.,.,.,.,.,.] - .,.,. - [.,.,.,.,.] - .,.,.,. - [3,3]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_COMPLETED,
                    self::PARAMETER_COUNT => 99,    // 2 - [4,2,1,1,4] - 2,4,1 - [2,.,.,1,.,1] - 5,4 - [.] - [1,1,.,1,1] - 2,2 - [.] - 2 - [.,1,.,1,1,2,.,1,2,.,.] - .,2,1,1 - [1,.,.,.,2,.,.,1,1,1,1,.] - 1,2,1,. - [1,.,1,.,1,.,2,.,.] - 1,.,3 - [4,1,1,2,3] - 3,2,5,1 - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NOT_YET_DIALED,
                    self::PARAMETER_COUNT => 33,    // 1 - [1,.,3,.,1] - 1,1,. - [.,.,.,.,1,.] - .,1 - [.] - [.,1,.,.,.] - .,. - [.] - . - [.,.,1,.,.,.,1,.,.,.,.] - 1,.,.,. - [.,1,1,.,.,.,1,1,.,.,.,.] - .,.,.,1 - [.,.,.,1,.,1,.,.,1] - .,1,. - [.,3,4,.,.] - .,1,.,1 - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_BUSY,
                    self::PARAMETER_COUNT => 34,    // 1 - [.,.,1,1,.] - 2,.,1 - [.,1,.,1,.,1] - .,1 - [1] - [.,.,.,.,1] - .,. - [.] - . - [.,.,1,.,.,.,.,.,.,1,1] - .,.,.,. - [.,1,1,1,.,.,.,.,.,.,.,2] - .,.,.,1 - [.,.,1,1,.,.,.,1,.] - .,.,1 - [1,.,1,.,1] - 1,2,.,2 - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_FAILED,
                    self::PARAMETER_COUNT => 22,    // 1 - [.,.,1,.,1] - .,.,. - [.,.,1,.,.,.] - 1,. - [.] - [.,.,.,1,.] - .,. - [1] - . - [1,.,.,.,.,.,.,.,.,.,.] - 1,.,.,. - [1,.,.,.,.,1,1,.,.,.,.,.] - .,.,.,. - [1,.,.,.,1,.,.,.,.] - 1,1,1 - [1,.,.,.,1] - .,.,.,1 - [1,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_NO_ANSWER,
                    self::PARAMETER_COUNT => 28,    // 1 - [1,.,.,.,.] - 1,1,. - [.,1,.,.,1,.] - .,. - [.] - [1,.,1,.,.] - .,. - [.] - . - [1,.,.,.,1,.,.,.,.,.,1] - .,.,.,1 - [.,.,.,1,.,.,.,.,..1,.,.] - 1,.,.,. - [.,1,.,.,.,1,.,.,1] - .,.,1 - [.,2,.,.,1] - 2,1,1,1 - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_STATUS_IN_PROGRESS,
                    self::PARAMETER_COUNT => 6,     // . - [.,.,.,.,.] - .,.,. - [.,.,.,.,.,.] - .,. - [.] - [.,.,.,.,.] - .,. - [.] - . - [.,.,.,.,.,.,.,.,.,.,.] - .,.,.,. - [.,.,.,.,.,.,.,.,..1,.,.] - .,.,.,. - [.,.,.,.,.,.,.,.,.] - .,.,. - [.,.,.,.,.] - .,.,.,. - [3,3]
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_REFERRER_SOURCE = array(
        self::TYPE_RANGE => array(
            // - [y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x]
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_GOOGLE,
                    self::PARAMETER_COUNT => 1,     // - [y,y,1] - x,x - [.] - x - [.,y,y,.,.,x,x]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_BING,
                    self::PARAMETER_COUNT => 1,     // - [y,y,1] - x,x - [.] - x - [.,y,y,.,.,x,x]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_EMPTY,
                    self::PARAMETER_COUNT => 12,    // - [1,2,.] - 2,2 - [.] - 2 - [.,y,y,.,.,2,1]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_EMPTY,
                    self::PARAMETER_COUNT => 3,     // - [y,y,.] - x,x - [.] - x - [.,1,2,.,.,x,x]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_GOOGLE,
                    self::PARAMETER_COUNT => 2,     // - [y,y,.] - x,x - [1] - x - [.,y,y,.,1,x,x]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_YAHOO,
                    self::PARAMETER_COUNT => 3,     // - [y,y,.] - x,x - [.] - x - [1,y,y,1,1,x,x]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_BING,
                    self::PARAMETER_COUNT => 1,     // - [y,y,.] - x,x - [.] - x - [1,y,y,.,.,x,x]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_EMPTY,
                    self::PARAMETER_COUNT => 15,    // - [1,2,.] - x,x - [.] - 2 - [.,1,2,.,.,2,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_GOOGLE,
                    self::PARAMETER_COUNT => 3,     // - [y,y,1] - x,x - [1] - x - [.,y,y,.,1,x,x]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_YAHOO,
                    self::PARAMETER_COUNT => 3,     // - [y,y,.] - x,x - [.] - x - [1,y,y,1,1,x,x]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_BING,
                    self::PARAMETER_COUNT => 2,     // - [y,y,1] - x,x - [.] - x - [1,y,y,.,.,x,x]
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - x - [.,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]

            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_GOOGLE,
                    self::PARAMETER_COUNT => 5,     // - x - [2,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,1,.] - x,x,x,x - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_YAHOO,
                    self::PARAMETER_COUNT => 4,     // - x - [1,1,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,2]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_BING,
                    self::PARAMETER_COUNT => 4,     // - x - [2,1,.,...] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,1]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_EMPTY,
                    self::PARAMETER_COUNT => 1,     // - x - [.,.,1,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_DIRECT,
                    self::PARAMETER_COUNT => 2,     // - x - [.,.,.,.,1] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_GOOGLE,
                    self::PARAMETER_COUNT => 8,     // - x - [.,.,1,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,1] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,1,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,1] - x,x,x - [y,y,y,.,2] - x,x,x,x - [2,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_YAHOO,
                    self::PARAMETER_COUNT => 9,     // - x - [.,.,2,.,2] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [1,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,1,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,1,y,y,.,.] - x,x,x - [y,y,y,.,2] - x,x,x,x - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_BING,
                    self::PARAMETER_COUNT => 6,     // - x - [.,.,2,.,1] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [1,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,1] - x,x,x,x - [1,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_EMPTY,
                    self::PARAMETER_COUNT => 147,   // - 6 - [.,.,.,.,.] - 6,6,2,2 - [2,1,2,2,2] - 6,6 - [.] - 2 - [.,1,1,.] - 2,2 - [.] - 2 - [.,1,.,.,.,2,1,1,2,1,2] - 2,2,1,2 - [2,2,.,.,1,.,.,2,1,2,1,2] - 2,2 - [.] - 2 - [2,1,2,2,.,1,1,.,.] - 2,2,6 - [5,5,5,.,.] - 6,6,6,6 - [.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_EMPTY,
                    self::PARAMETER_COUNT => 11,    // - x - [.,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,1,.] - x,x - [.] - x - [.,y,2,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,2,1,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,1,1,.,.] - x,x,x - [1,1,1,.,.] - x,x,x,x - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_GOOGLE,
                    self::PARAMETER_COUNT => 12,    // - x - [.,.,.,1,1] - x,x,x,x - [x,x,x,x,x] - x,x - [1] - x - [1,y,y,.] - x,x - [1] - x - [.,y,y,.,1,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,1,1] - x,x,x - [y,y,y,1,1] - x,x,x,x - [2,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_YAHOO,
                    self::PARAMETER_COUNT => 6,     // - x - [1,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [1,y,y,1,1,x,x,x,x,x,x] - x,x,x,x - [x,x,1,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,1,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_BING,
                    self::PARAMETER_COUNT => 7,     // - x - [.,1,1,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,1] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,1,y,y,.,.,x,x,x,x,x] - x,x - [1] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [2,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_EMPTY,
                    self::PARAMETER_COUNT => 159,   // - 6 - [.,.,1,.,.] - 6,6,2,2 - [2,1,2,2,2] - 6,6 - [.] - 2 - [.,1,2,.] - 2,2 - [.] - 2 - [.,1,2,.,.,2,1,1,2,1,2] - 2,2,1,2 - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [2,1,2,2,.,2,2,.,.] - 2,2,6 - [6,6,6,.,.] - 6,6,6,6 - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_DIRECT,
                    self::PARAMETER_COUNT => 2,     // - x - [.,.,.,.,1] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_GOOGLE,
                    self::PARAMETER_COUNT => 25,    // - x - [2,.,1,1,1] - x,x,x,x - [x,x,x,x,x] - x,x - [1] - x - [1,y,y,1] - x,x - [1] - x - [.,y,y,.,1,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,1,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,1,2] - x,x,x - [y,y,y,2,3] - x,x,x,x - [2,3]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_YAHOO,
                    self::PARAMETER_COUNT => 19,    // - x - [2,1,2,.,2] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [1,y,y,.] - x,x - [.] - x - [1,y,y,1,1,x,x,x,x,x,x] - x,x,x,x - [x,x,1,y,y,.,1,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,2,y,y,.,.] - x,x,x - [y,y,y,.,2] - x,x,x,x - [.,2]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_REFERRER_SOURCE_BING,
                    self::PARAMETER_COUNT => 17,    // - x - [2,1,2,1,2] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,1] - x,x - [.] - x - [1,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,1,y,y,.,.,x,x,x,x,x] - x,x - [1] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,1] - x,x,x,x - [3,1]
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_SEARCH_TYPE = array(
        self::TYPE_RANGE => array(
            // - [y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x]
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_PPC,
                    self::PARAMETER_COUNT => 2,      // -[y,y,1] - x,x - [.] - x - [1,y,y,.,.,x,x]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_EMPTY,
                    self::PARAMETER_COUNT => 12,    // - [1,1,.] - 2,2 - [.] - 2 - [.,1,.,.,.,2,1]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_EMPTY,
                    self::PARAMETER_COUNT => 3,     // - [y,1,.] - x,x - [.] - x - [.,.,2,.,.,x,x]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_PPC,
                    self::PARAMETER_COUNT => 2,     // - [y,y,.] - x,x - [1] - x - [.,y,y,.,1,x,x]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_ORGANIC,
                    self::PARAMETER_COUNT => 4,     // - [y,y,1] - x,x - [.] - x - [1,y,y,1,1,x,x]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_EMPTY,
                    self::PARAMETER_COUNT => 15,    // - [1,2,.] - 2,2 - [.] - 2 - [.,1,2,.,.,2,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_PPC,
                    self::PARAMETER_COUNT => 4,     // - [y,y,1] - x,x - [1] - x - [1,y,y,.,1,x,x]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_ORGANIC,
                    self::PARAMETER_COUNT => 4,     // - [y,y,1] - x,x - [.] - x - [1,y,y,1,1,x,x]
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - x - [.,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_PPC,
                    self::PARAMETER_COUNT => 6,     // - x - [3,1,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,1,.] - x,x,x,x - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_ORGANIC,
                    self::PARAMETER_COUNT => 7,     // - x - [2,.,.,1,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,3]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_PPC,
                    self::PARAMETER_COUNT => 15,    // - x - [.,.,5,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [1,y,y,.] - x,x - [.] - x - [1,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,1,y,y,.,.] - x,x,x - [y,y,y,.,3] - x,x,x,x - [4,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_ORGANIC,
                    self::PARAMETER_COUNT => 9,     // - x - [.,.,.,.,4] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,2,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,1] - x,x,x - [y,y,y,.,2] - x,x,x,x - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_DIRECT,
                    self::PARAMETER_COUNT => 2,     // - x - [.,.,.,.,1] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [1,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_EMPTY,
                    self::PARAMETER_COUNT => 147,   // - 6 - [.,.,.,.,.] - 6,6,2,2 - [2,1,2,2,2] - 6,6 - [.] - 2 - [.,1,1,.] - 2,2 - [.] - 2 - [.,1,y,.,.,2,1,1,2,1,2] - 2,2,1,2 - [2,2,.,y,1,.,.,2,1,2,1,2] - 2,2 - [.] - 2 - [2,1,2,2,.,1,1,.,.] - 2,2,6 - [5,5,5,.,.] - 6,6,6,6 - [.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_EMPTY,
                    self::PARAMETER_COUNT => 11,    // - x - [.,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,1,.] - x,x - [.] - x - [.,y,2,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,2,1,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,1,1,.,.] - x,x,x - [1,1,1,.,.] - x,x,x,x - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_PPC,
                    self::PARAMETER_COUNT => 14,    // - x - [.,1,1,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [1,y,y,.] - x,x - [1] - x - [.,y,y,.,1,x,x,x,x,x,x] - x,x,x,x - [x,x,1,y,y,.,.,x,x,x,x,x] - x,x - [1] - x - [x,x,x,x,1,y,y,1,1] - x,x,x - [y,y,y,1,1] - x,x,x,x - [1,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_ORGANIC,
                    self::PARAMETER_COUNT => 11,    // - x - [1,.,.,1,1] - x,x,x,x - [x,x,x,x,x] - x,x - [1] - x - [.,y,y,1] - x,x - [.] - x - [1,y,y,1,1,x,x,x,x,x,x] - x,x,x,x - [x,x,1,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [1,1]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_EMPTY,
                    self::PARAMETER_COUNT => 158,   // - 6 - [.,.,.,.,.] - 6,6,2,2 - [2,1,2,2,2] - 6,6 - [.] - 2 - [.,1,2,.] - 2,2 - [.] - 2 - [.,1,2,.,.,2,1,1,2,1,2] - 2,2,1,2 - [2,2,.,2,2,.,.,2,1,2,1,2] - 2,2 - [.] - 2 - [2,1,2,2,.,2,2,.,.] - 2,2,6 - [6,6,6,.,.] - 6,6,6,6 - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_PPC,
                    self::PARAMETER_COUNT => 35,    // - x - [3,2,6,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [2,y,y,1] - x,x - [1] - x - [1,y,y,.,1,x,x,x,x,x,x] - x,x,x,x - [x,x,1,y,y,.,.,x,x,x,x,x] - x,x - [1] - x - [x,x,x,x,2,y,y,1,1] - x,x,x - [y,y,y,2,4] - x,x,x,x - [4,2]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_ORGANIC,
                    self::PARAMETER_COUNT => 27,    // - x - [3,.,.,2,5] - x,x,x,x - [x,x,x,x,x] - x,x - [1] - x - [.,y,y,1] - x,x - [.] - x - [1,y,y,1,1,x,x,x,x,x,x] - x,x,x,x - [x,x,1,y,y,1,2,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,1] - x,x,x - [y,y,y,.,2] - x,x,x,x - [1,4]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_SEARCH_TYPE_DIRECT,
                    self::PARAMETER_COUNT => 2,     // - x - [.,.,.,.,1] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [1,.]
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_URL = array(
        self::TYPE_RANGE => array(
            // - [y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x]
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_1,
                    self::PARAMETER_COUNT => 1,     // - [y,y,.] - x,x - [.] - x - [1,y,y,.,.,x,x]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_2,
                    self::PARAMETER_COUNT => 1,     // - [y,y,1] - x,x - [.] - x - [.,y,y,.,.,x,x]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_EMPTY,
                    self::PARAMETER_COUNT => 12,    // - [1,1,.] - 2,2 - [.] - 2 - [.,1,.,.,.,2,1]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_EMPTY,
                    self::PARAMETER_COUNT => 3,     // - [y,1,.] - x,x - [.] - x - [.,y,2,.,.,x,x]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_1,
                    self::PARAMETER_COUNT => 2,     // - [y,y,1] - x,x - [.] - x - [1,y,y,.,.,x,x]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_2,
                    self::PARAMETER_COUNT => 4,     // - [y,y,.] - x,x - [1] - x - [.,y,y,1,2,x,x]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_EMPTY,
                    self::PARAMETER_COUNT => 15,    // - [1,2,.] - 2,2 - [.] - 2 - [.,1,2,.,.,2,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_1,
                    self::PARAMETER_COUNT => 3,     // - [y,y,1] - x,x - [.] - x - [2,y,y,.,.,x,x]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_2,
                    self::PARAMETER_COUNT => 5,     // - [y,y,1] - x,x - [1] - x - [.,y,y,1,2,x,x]
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - x - [.,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_1,
                    self::PARAMETER_COUNT => 5,     // - x - [3,.,.,1,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_2,
                    self::PARAMETER_COUNT => 5,     // - x - [1,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,1,.] - x,x,x,x - [.,2]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_3,
                    self::PARAMETER_COUNT => 3,     // - x - [1,1,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,1]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_EMPTY_STR,
                    self::PARAMETER_COUNT => 1,     // - x - [.,.,.,.,1] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_1,
                    self::PARAMETER_COUNT => 7,     // - x - [.,.,.,.,1] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [1,y,y,.] - x,x - [.] - x - [1,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,1,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [3,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_2,
                    self::PARAMETER_COUNT => 12,    // - x - [.,.,3,.,1] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,1] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,1,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,1] - x,x,x - [y,y,y,.,5] - x,x,x,x - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_3,
                    self::PARAMETER_COUNT => 5,     // - x - [.,.,1,.,2] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,1,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_AUTO,
                    self::PARAMETER_COUNT => 1,     // - x - [.,.,1,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_EMPTY,
                    self::PARAMETER_COUNT => 147,   // - 6 - [.,.,.,.,.] - 6,6,2,2 - [2,1,2,2,2] - 6,6 - [.] - 2 - [.,1,1,.] - 2,2 - [.] - 2 - [.,1,y,.,.,2,1,1,2,1,2] - 2,2,1,2 - [2,2,.,y,1,.,.,2,1,2,1,2] - 2,2 - [.] - 2 - [2,1,2,2,.,1,1,.,.] - 2,2,6 - [5,5,5,.,.] - 6,6,6,6 - [.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_EMPTY,
                    self::PARAMETER_COUNT => 11,    // - x - [.,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,1,.] - x,x - [.] - x - [.,y,2,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,2,1,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,1,1,.,.] - x,x,x - [1,1,1,.,.] - x,x,x,x - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_1,
                    self::PARAMETER_COUNT => 9,     // - x - [1,.,.,1,1] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,1] - x,x - [.] - x - [1,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,1,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,1] - x,x,x - [y,y,y,.,.] - x,x,x,x - [1,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_2,
                    self::PARAMETER_COUNT => 11,    // - x - [.,1,1,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [1] - x - [.,y,y,1,2,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,1,y,y,1,.] - x,x,x - [y,y,y,1,1] - x,x,x,x - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_3,
                    self::PARAMETER_COUNT => 5,     // - x - [.,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [1] - x - [1,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,1,y,y,.,.,x,x,x,x,x] - x,x - [1] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,1]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_EMPTY,
                    self::PARAMETER_COUNT => 158,   // - 6 - [.,.,.,.,.] - 6,6,2,2 - [2,1,2,2,2] - 6,6 - [.] - 2 - [.,1,2,.] - 2,2 - [.] - 2 - [.,1,2,.,.,2,1,1,2,2,2] - 2,2,1,2 - [2,2,.,2,2,.,.,2,1,2,1,2] - 2,2 - [.] - 2 - [2,1,2,2,.,2,2,.,.] - 2,2,6 - [6,6,6,.,.] - 6,6,6,6 - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_EMPTY_STR,
                    self::PARAMETER_COUNT => 1,     // - x - [.,.,.,.,1] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_1,
                    self::PARAMETER_COUNT => 21,    // - x - [4,.,2,.,2] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [1,y,y,1] - x,x - [.] - x - [2,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,1,y,y,.,1,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,1] - x,x,x - [y,y,y,.,.] - x,x,x,x - [4,2]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_2,
                    self::PARAMETER_COUNT => 28,    // - x - [1,1,4,.,1] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,1] - x,x - [1] - x - [.,y,y,1,2,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,1,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,1,y,y,1,1] - x,x,x - [y,y,y,2,6] - x,x,x,x - [1,2]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_3,
                    self::PARAMETER_COUNT => 13,    // - x - [1,1,1,.,2] - x,x,x,x - [x,x,x,x,x] - x,x - [1] - x - [1,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,1,y,y,.,.,x,x,x,x,x] - x,x - [1] - x - [x,x,x,x,1,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [1,2]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_URL_AUTO,
                    self::PARAMETER_COUNT => 1,     // - x - [.,.,1,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_KEYWORD = array(
        self::TYPE_RANGE => array(
            // - [y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x]
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_2,
                    self::PARAMETER_COUNT => 1,     // - [y,y,1] - x,x - [.] - x - [.,y,y,.,.,x,x] 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_4,
                    self::PARAMETER_COUNT => 1,     // - [y,y,.] - x,x - [.] - x - [1,y,y,.,.,x,x] 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_EMPTY,
                    self::PARAMETER_COUNT => 12,    // - [1,1,.] - 2,2 - [.] - 2 - [.,1,y,.,.,2,1] 
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_EMPTY,
                    self::PARAMETER_COUNT => 3,     // - [y,1,.] - x,x - [.] - x - [.,y,2,.,.,x,x]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_2,
                    self::PARAMETER_COUNT => 3,     // - [y,y,.] - x,x - [.] - x - [.,y,y,1,2,x,x]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_3,
                    self::PARAMETER_COUNT => 2,     // - [y,y,1] - x,x - [1] - x - [.,y,y,.,.,x,x]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_4,
                    self::PARAMETER_COUNT => 1,     // - [y,y,.] - x,x - [.] - x - [1,y,y,.,.,x,x]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_EMPTY,
                    self::PARAMETER_COUNT => 15,    // - [1,2,.] - 2,2 - [.] - 2 - [.,1,2,.,.,2,1] 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_2,
                    self::PARAMETER_COUNT => 4,     // - [y,y,1] - x,x - [.] - x - [.,y,y,1,2,x,x] 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_3,
                    self::PARAMETER_COUNT => 2,     // - [y,y,1] - x,x - [1] - x - [.,y,y,.,.,x,x] 
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_4,
                    self::PARAMETER_COUNT => 2,     // - [y,y,.] - x,x - [.] - x - [2,y,y,.,.,x,x] 
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - x - [.,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_1,
                    self::PARAMETER_COUNT => 1,     // - x - [.,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_2,
                    self::PARAMETER_COUNT => 3,     // - x - [1,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,1,.] - x,x,x,x - [.,1]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_3,
                    self::PARAMETER_COUNT => 6,     // - x - [3,.,.,1,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,2]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_4,
                    self::PARAMETER_COUNT => 3,     // - x - [1,1,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,1]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_EMPTY,
                    self::PARAMETER_COUNT => 1,     // - x - [.,.,1,.,1] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_DIRECT,
                    self::PARAMETER_COUNT => 2,     // - x - [.,.,.,.,1] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_1,
                    self::PARAMETER_COUNT => 7,     // - x - [.,.,.,.,1] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,2,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,1,y,y,.,1] - x,x,x - [y,y,y,.,1] - x,x,x,x - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_2,
                    self::PARAMETER_COUNT => 8,     // - x - [.,.,2,.,2] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,1] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,2] - x,x,x,x - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_3,
                    self::PARAMETER_COUNT => 5,     // - x - [.,.,1,.,1] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,2] - x,x,x,x - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_4,
                    self::PARAMETER_COUNT => 3,     // - x - [.,.,1,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [1,y,y,.] - x,x - [.] - x - [1,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_EMPTY,
                    self::PARAMETER_COUNT => 147,   // - 6 - [.,.,.,.,.] - 6,6,2,2 - [2,1,2,2,2] - 6,6 - [.] - 2 - [.,1,1,.] - 2,2 - [.] - 2 - [.,1,y,.,.,2,1,1,2,1,2] - 2,2,1,2 - [2,2,.,y,1,.,.,2,1,2,1,2] - 2,2 - [.] - 2 - [2,1,2,2,.,1,1,.,.] - 2,2,6 - [5,5,5,.,.] - 6,6,6,6 - [.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_EMPTY,
                    self::PARAMETER_COUNT => 11,    // - x - [.,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,1,.] - x,x - [.] - x - [.,y,2,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,2,1,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,1,1,.,.] - x,x,x - [1,1,1,.,.] - x,x,x,x - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_1,
                    self::PARAMETER_COUNT => 2,     // - x - [.,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,1,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_2,
                    self::PARAMETER_COUNT => 6,     // - x - [1,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,1,2,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,2]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_3,
                    self::PARAMETER_COUNT => 14,    // - x - [.,1,1,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [1] - x - [1,y,y,1] - x,x - [1] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,2,y,y,.,.,x,x,x,x,x] - x,x - [1] - x - [x,x,x,x,1,y,y,.,1] - x,x,x - [y,y,y,1,1] - x,x,x,x - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_4,
                    self::PARAMETER_COUNT => 3,     // - x - [.,.,.,1,1] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [1,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
                ),
            ),
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED => array (
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_EMPTY,
                    self::PARAMETER_COUNT => 159,   // - 6 - [.,.,1,.,.] - 6,6,2,2 - [2,1,2,2,2] - 6,6 - [.] - 2 - [.,1,2,.] - 2,2 - [.] - 2 - [.,1,2,.,.,2,1,1,2,1,2] - 2,2,1,2 - [2,2,.,2,2,.,.,2,1,2,1,2] - 2,2 - [.] - 2 - [2,1,2,2,.,2,2,.,.] - 2,2,6 - [6,6,6,.,.] - 6,6,6,6 - [.,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_DIRECT,
                    self::PARAMETER_COUNT => 2,     // - x - [.,.,.,.,1] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [1,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_1,
                    self::PARAMETER_COUNT => 10,    // - x - [.,.,.,.,1] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,2,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,1,y,y,1,1] - x,x,x - [y,y,y,.,1] - x,x,x,x - [2,.]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_2,
                    self::PARAMETER_COUNT => 17,    // - x - [2,.,2,.,2] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,1] - x,x - [.] - x - [.,y,y,1,2,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,1,2] - x,x,x,x - [1,3]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_3,
                    self::PARAMETER_COUNT => 25,    // - x - [3,1,2,1,1] - x,x,x,x - [x,x,x,x,x] - x,x - [1] - x - [1,y,y,1] - x,x - [1] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,2,y,y,.,.,x,x,x,x,x] - x,x - [1] - x - [x,x,x,x,1,y,y,.,1] - x,x,x - [y,y,y,1,3] - x,x,x,x - [2,2]
                ),
                array(
                    self::PARAMETER_VALUE => self::VALUE_KEYWORD_4,
                    self::PARAMETER_COUNT => 9,     // - x - [1,1,1,1,1] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [1,y,y,.] - x,x - [.] - x - [2,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,1]
                ),
            ),
        ),
    );
}
