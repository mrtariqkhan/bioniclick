<?php

/**
 * Description of DataBaseTimezoneHelperTest
 * 
 * @author fairdev
 */
class DataBaseTimezoneHelperTest extends sfBasePhpunitTestCase {

    protected static $TIMEZONE_CST_OFFSET= '-0500';
    protected static $TIMEZONE_NST_OFFSET= '-0230';

    protected static $TIMEZONE_CST_OFFSET_CONVERTED= '-05:00';
    protected static $TIMEZONE_NST_OFFSET_CONVERTED= '-02:30';

    protected static $DB_TIMEZONE_OFFSET_HOURS      = '-05:00';
    protected static $DB_TIMEZONE_OFFSET_MINUTES    = '+02:30';


    public function testConvertOffsetHours() {

        $offsetStringConverted = DataBaseTimezoneHelper::convertOffset(self::$TIMEZONE_CST_OFFSET);
        $this->assertEquals(
            self::$TIMEZONE_CST_OFFSET_CONVERTED,
            $offsetStringConverted
        );
    }

    public function testConvertOffsetMinutes() {

        $offsetStringConverted = DataBaseTimezoneHelper::convertOffset(self::$TIMEZONE_NST_OFFSET);
        $this->assertEquals(
            self::$TIMEZONE_NST_OFFSET_CONVERTED,
            $offsetStringConverted
        );
    }

    public function testFindOffsetHours() {

        $this->setDatabaseTimezoneOffset(self::$DB_TIMEZONE_OFFSET_HOURS);

        $offsetStringConverted = DataBaseTimezoneHelper::findOffset();
        $this->assertEquals(
            self::$DB_TIMEZONE_OFFSET_HOURS,
            $offsetStringConverted
        );
    }

    public function testFindOffsetMinutes() {

        $this->setDatabaseTimezoneOffset(self::$DB_TIMEZONE_OFFSET_MINUTES);

        $offsetStringConverted = DataBaseTimezoneHelper::findOffset();
        $this->assertEquals(
            self::$DB_TIMEZONE_OFFSET_MINUTES,
            $offsetStringConverted
        );
    }

    /**
     *
     * @param string $offset in format "+02:30"
     */
    protected function setDatabaseTimezoneOffset($offset) {

        $sqlQueryString = "SET @@session.time_zone = '$offset'";

        $conn = Doctrine_Manager::getInstance()->getConnection('doctrine');
        $conn->execute($sqlQueryString, array());
    }

}

