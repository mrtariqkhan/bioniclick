<?php

require_once dirname(__FILE__) . '/QueryBuilderCallStatusBase.php';

/**
 * Description of QueryBuilderCallStatusBaseTz
 * 
 * @author fairdev
 */
abstract class QueryBuilderCallStatusBaseProvided extends QueryBuilderCallStatusBase {

    protected $filterValues         = array();
    protected $resultsPerMonthDay   = array();
    protected $resultsPerWeekDay    = array();
    protected $resultsPerHour       = array();


    protected function _start() {

        parent::_start();

        $this->configureFilterValuesAndResults();
    }

    abstract protected function configureFilterValuesAndResults();


    public function setFilterValues(array $filterValues) {
        $this->filterValues = $filterValues;
    }

    public function setResultsPerMonthDay(array $resultsPerMonthDay) {
        $this->resultsPerMonthDay = $resultsPerMonthDay;
    }

    public function setResultsPerWeekDay(array $resultsPerWeekDay) {
        $this->resultsPerWeekDay = $resultsPerWeekDay;
    }

    public function setResultsPerHour(array $resultsPerHour) {
        $this->resultsPerHour = $resultsPerHour;
    }
}
