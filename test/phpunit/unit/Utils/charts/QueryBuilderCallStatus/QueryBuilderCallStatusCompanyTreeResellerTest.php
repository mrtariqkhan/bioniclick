<?php

require_once dirname(__FILE__) . '/QueryBuilderCallStatusBaseCompanyTree.php';

/**
 * Description of QueryBuilderCallStatusCompanyTreeResellerTest
 * 
 * @author fairdev
 */
class QueryBuilderCallStatusCompanyTreeResellerTest extends QueryBuilderCallStatusBaseCompanyTree {

    protected function configureFilterValuesAndResults() {

        $this->setFilterValues(         self::$FILTER_VALUES);
        $this->setResultsPerMonthDay(   self::$RESULTS_PER_MONTH_DAY);
        $this->setResultsPerWeekDay(    self::$RESULTS_PER_WEEK_DAY);
        $this->setResultsPerHour(       self::$RESULTS_PER_HOUR);
    }

    protected function configureUserWithTz() {
        $this->configureCompanyTreeUserWithTz(
            self::ID_COMPANY_RESELLER_ACQUISIO,
            null,
            self::TIMEZONE_ID_MINUS_2_AND_A_HALF
        );
    }

    protected static $FILTER_VALUES = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            'from'  => '1317525300', //2011-10-02 00:45:00 -0230
            'to'    => '1317528900', //2011-10-02 01:45:00 -0230
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            'from'  => '1319929261', //2011-10-29 20:31:01 -0230
            'to'    => '1319932860', //2011-10-29 21:31:00 -0230
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            'from'  => '1316827801', //2011-09-23 23:00:01 -0230
            'to'    => '1317432600', //2011-09-30 23:00:00 -0230
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            'from'  => '1314925462', //2011-09-01 22:34:22 -0230
            'to'    => '1317517461', //2011-10-01 22:34:21 -0230
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            'from'  => '1283391176', //2010-09-01 23:02:56 -0230
            'to'    => '1314927175', //2011-09-01 23:02:55 -0230
        ),

        self::TYPE_ALL => array(
        ),

        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'from'  => '1319955217', //2011-10-30 03:43:37 -0230
            'to'    => '1319958818', //2011-10-30 04:43:38 -0230
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
            'from'  => '1317607201', //2011-10-02 23:30:01 -0230
            'to'    => '1317693600', //2011-10-03 23:30:00 -0230
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
            'from'  => '1315881000', //2011-09-13 00:00:00 -0230
            'to'    => '1316478579', //2011-09-19 21:59:39 -0230
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
            'from'  => '1276542732', //2010-06-14 16:42:12 -0230
            'to'    => '1279134731', //2010-07-14 16:42:11 -0230
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
            'from'  => '1282265646', //2010-08-19 22:24:06 -0230
            'to'    => '1313801645', //2011-08-19 22:24:05 -0230
        ),
    );


    protected static $RESULTS_PER_MONTH_DAY = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '17',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            array(
                'id'            => '29',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
            array(
                'id'            => '30',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            array(
                'id'            => '11',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '6',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '8',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '11',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
        ),
        self::TYPE_ALL => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '17',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '6',
                'period_name'   => '6',
            ),
            array(
                'id'            => array('24', '26'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '11',
                'period_name'   => '11',
            ),
            array(
                'id'            => '6',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => array('5', '27'),
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '2',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '8',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => array('11', '28'),
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '2',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
            array(
                'id'            => '29',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
            array(
                'id'            => '30',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
        ),
    );

    protected static $RESULTS_PER_WEEK_DAY = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '17',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            array(
                'id'            => '29',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
            array(
                'id'            => '30',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            array(
                'id'            => '11',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
            array(
                'id'            => array('6', '8'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
            array(
                'id'            => '11',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
        ),
        self::TYPE_ALL => array(
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '17',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
            array(
                'id'            => array('6', '8'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
            array(
                'id'            => array('24', '26'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_TUESDAY,
                'period_name'   => self::WEEK_DAY_TUESDAY,
            ),
            array(
                'id'            => '27',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_WEDNESDAY,
                'period_name'   => self::WEEK_DAY_WEDNESDAY,
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
            array(
                'id'            => array('11', '22'),
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '28',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
            array(
                'id'            => '29',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
            array(
                'id'            => '30',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
        ),
    );

    protected static $RESULTS_PER_HOUR = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '0',
                'period_name'   => '0',
            ),
            array(
                'id'            => '17',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '0',
                'period_name'   => '0',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            array(
                'id'            => '29',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '20',
                'period_name'   => '20',
            ),
            array(
                'id'            => '30',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            array(
                'id'            => '11',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '14',
                'period_name'   => '14',
            ),
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '4',
                'period_name'   => '4',
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => array('6', '8'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => array('9', '15'),
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '2',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '11',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '14',
                'period_name'   => '14',
            ),
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '4',
                'period_name'   => '4',
            ),
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '4',
                'period_name'   => '4',
            ),
        ),
        self::TYPE_ALL => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '0',
                'period_name'   => '0',
            ),
            array(
                'id'            => '17',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '0',
                'period_name'   => '0',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => array('4', '13'),
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '2',
                'period_num'    => '4',
                'period_name'   => '4',
            ),
            array(
                'id'            => '27',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '5',
                'period_name'   => '5',
            ),
            array(
                'id'            => array('11', '28'),
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '2',
                'period_num'    => '14',
                'period_name'   => '14',
            ),
            array(
                'id'            => '29',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '20',
                'period_name'   => '20',
            ),
            array(
                'id'            => array('9', '15'),
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '2',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '24',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '30',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => array('6', '8'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '26',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '23',
                'period_name'   => '23',
            ),
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
        ),
    );
}
