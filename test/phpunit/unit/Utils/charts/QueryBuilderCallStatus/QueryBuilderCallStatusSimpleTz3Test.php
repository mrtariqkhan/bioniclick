<?php

require_once dirname(__FILE__) . '/QueryBuilderCallStatusBaseSimpleTz.php';

/**
 * Description of QueryBuilderCallStatusSimpleTz3Test
 * 
 * @author fairdev
 */
class QueryBuilderCallStatusSimpleTz3Test extends QueryBuilderCallStatusBaseSimpleTz {

    protected function configureFilterValuesAndResults() {

        $this->setFilterValues(         self::$FILTER_VALUES);
        $this->setResultsPerMonthDay(   self::$RESULTS_PER_MONTH_DAY);
        $this->setResultsPerWeekDay(    self::$RESULTS_PER_WEEK_DAY);
        $this->setResultsPerHour(       self::$RESULTS_PER_HOUR);
    }

    protected function configureUserWithTz() {
        $this->configureSuperAdminUserWithTz(self::TIMEZONE_ID_PLUS_6);
    }

    protected static $FILTER_VALUES = array(
        self::TYPE_CALL_ID_21 => array(//1317690000
            'from'  => '1317690000',
            'to'    => '1317690000',
        ),
        self::TYPE_CALL_ID_22 => array(//1317944400
            'from'  => '1317944400',
            'to'    => '1317944401',
        ),
        self::TYPE_CALL_ID_23 => array(//1318377485
            'from'  => '1318377484',
            'to'    => '1318377486',
        ),
        self::TYPE_CALL_ID_24 => array(//1318377727
            'from'  => '1318377720',
            'to'    => '1318377727',
        ),
        self::TYPE_CALL_ID_25 => array(//1318380237
            'from'  => '1318380234',
            'to'    => '1318380239',
        ),
//        self::TYPE_CALL_ID_26 => array(//1318386327
//            'from'  => '1318386324',
//            'to'    => '1318386328',
//        ),
//        self::TYPE_CALL_ID_27 => array(//1318406566
//            'from'  => '1318406560',
//            'to'    => '1318406567',
//        ),
    );

    protected static $RESULTS_PER_MONTH_DAY = array(
        self::TYPE_CALL_ID_21 => array(//2011-10-04 07:00:00 +0600
            'id'            => '21',
            'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
            'calls_amount'  => '1',
            'period_num'    => '4',
            'period_name'   => '4',
        ),
        self::TYPE_CALL_ID_22 => array(//2011-10-07 05:40:00 +0600
            'id'            => '22',
            'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
            'calls_amount'  => '1',
            'period_num'    => '7',
            'period_name'   => '7',
        ),
        self::TYPE_CALL_ID_23 => array(//2011-10-12 05:58:05 +0600
        ),
        self::TYPE_CALL_ID_24 => array(//2011-10-12 06:02:07 +0600
            'id'            => '24',
            'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
            'calls_amount'  => '1',
            'period_num'    => '12',
            'period_name'   => '12',
        ),
        self::TYPE_CALL_ID_25 => array(//2011-10-12 06:43:57 +0600
            'id'            => '25',
            'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
            'calls_amount'  => '1',
            'period_num'    => '12',
            'period_name'   => '12',
        ),
//        self::TYPE_CALL_ID_26 => array(//011-10-12 08:25:27 +0600
//            'id'            => '26',
//            'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
//            'calls_amount'  => '1',
//            'period_num'    => '12',
//            'period_name'   => '12',
//        ),
//        self::TYPE_CALL_ID_27 => array(//2011-10-12 14:02:46 +0600
//            'id'            => '27',
//            'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
//            'calls_amount'  => '1',
//            'period_num'    => '12',
//            'period_name'   => '12',
//        ),
    );

    protected static $RESULTS_PER_WEEK_DAY = array(
        self::TYPE_CALL_ID_21 => array(//2011-10-04 07:00:00 +0600
            'id'            => '21',
            'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
            'calls_amount'  => '1',
            'period_num'    => self::WEEK_DAY_NUM_TUESDAY,
            'period_name'   => self::WEEK_DAY_TUESDAY,
        ),
        self::TYPE_CALL_ID_22 => array(//2011-10-07 05:40:00 +0600
            'id'            => '22',
            'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
            'calls_amount'  => '1',
            'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
            'period_name'   => self::WEEK_DAY_FRIDAY,
        ),
        self::TYPE_CALL_ID_23 => array(//2011-10-12 05:58:05 +0600
        ),
        self::TYPE_CALL_ID_24 => array(//2011-10-12 06:02:07 +0600
            'id'            => '24',
            'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
            'calls_amount'  => '1',
            'period_num'    => self::WEEK_DAY_NUM_WEDNESDAY,
            'period_name'   => self::WEEK_DAY_WEDNESDAY,
        ),
        self::TYPE_CALL_ID_25 => array(//2011-10-12 06:43:57 +0600
            'id'            => '25',
            'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
            'calls_amount'  => '1',
            'period_num'    => self::WEEK_DAY_NUM_WEDNESDAY,
            'period_name'   => self::WEEK_DAY_WEDNESDAY,
        ),
//        self::TYPE_CALL_ID_26 => array(//011-10-12 08:25:27 +0600
//            'id'            => '26',
//            'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
//            'calls_amount'  => '1',
//            'period_num'    => self::WEEK_DAY_NUM_WEDNESDAY,
//            'period_name'   => self::WEEK_DAY_WEDNESDAY,
//        ),
//        self::TYPE_CALL_ID_27 => array(//2011-10-12 14:02:46 +0600
//            'id'            => '27',
//            'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
//            'calls_amount'  => '1',
//            'period_num'    => self::WEEK_DAY_NUM_WEDNESDAY,
//            'period_name'   => self::WEEK_DAY_WEDNESDAY,
//        ),
    );

    protected static $RESULTS_PER_HOUR = array(
        self::TYPE_CALL_ID_21 => array(//2011-10-04 07:00:00 +0600
            'id'            => '21',
            'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
            'calls_amount'  => '1',
            'period_num'    => '7',
            'period_name'   => '7',
        ),
        self::TYPE_CALL_ID_22 => array(//2011-10-07 05:40:00 +0600
            'id'            => '22',
            'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
            'calls_amount'  => '1',
            'period_num'    => '5',
            'period_name'   => '5',
        ),
        self::TYPE_CALL_ID_23 => array(//2011-10-12 05:58:05 +0600
        ),
        self::TYPE_CALL_ID_24 => array(//2011-10-12 06:02:07 +0600
            'id'            => '24',
            'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
            'calls_amount'  => '1',
            'period_num'    => '6',
            'period_name'   => '6',
        ),
        self::TYPE_CALL_ID_25 => array(//2011-10-12 06:43:57 +0600
            'id'            => '25',
            'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
            'calls_amount'  => '1',
            'period_num'    => '6',
            'period_name'   => '6',
        ),
//        self::TYPE_CALL_ID_26 => array(//011-10-12 08:25:27 +0600
//            'id'            => '26',
//            'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
//            'calls_amount'  => '1',
//            'period_num'    => '8',
//            'period_name'   => '8',
//        ),
//        self::TYPE_CALL_ID_27 => array(//2011-10-12 14:02:46 +0600
//            'id'            => '27',
//            'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
//            'calls_amount'  => '1',
//            'period_num'    => '14',
//            'period_name'   => '14',
//        ),
    );
}
