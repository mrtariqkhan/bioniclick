<?php

require_once dirname(__FILE__) . '/QueryBuilderCallStatusBaseCompanyTree.php';

/**
 * Description of QueryBuilderCallStatusCompanyTreeAdvertiserOfResellerTest
 * 
 * @author fairdev
 */
class QueryBuilderCallStatusCompanyTreeAdvertiserOfResellerTest extends QueryBuilderCallStatusBaseCompanyTree {

    //TODO:: write all need constants

    protected function configureFilterValuesAndResults() {

        $this->setFilterValues(         self::$FILTER_VALUES);
        $this->setResultsPerMonthDay(   self::$RESULTS_PER_MONTH_DAY);
        $this->setResultsPerWeekDay(    self::$RESULTS_PER_WEEK_DAY);
        $this->setResultsPerHour(       self::$RESULTS_PER_HOUR);
    }

    protected function configureUserWithTz() {
        $this->configureCompanyTreeUserWithTz(
            null,
            self::ID_ADVERTISER_RESELLER_ACQUISIO_TRIPS,
            self::TIMEZONE_ID_MINUS_2_AND_A_HALF
        );
    }


    protected static $FILTER_VALUES = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            'from'  => '1317524401', //2011-10-02 00:30:01 -0230
            'to'    => '1317528000', //2011-10-02 01:30:00 -0230
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            'from'  => '1317347881', //2011-09-29 23:28:01 -0230
            'to'    => '1317434280', //2011-09-30 23:28:00 -0230
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            'from'  => '1317425404', //2011-09-30 21:00:04 -0230
            'to'    => '1318030203', //2011-10-07 21:00:03 -0230
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            'from'  => '1313807399', //2011-08-19 23:59:59 -0230
            'to'    => '1316485798', //2011-09-19 23:59:58 -0230
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            'from'  => '1286843460', //2010-10-11 22:01:00 -0230
            'to'    => '1318379461', //2011-10-11 22:01:01 -0230
        ),

        self::TYPE_ALL => array(
        ),

        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'from'  => '1315875601', //2011-09-12 22:30:01 -0230
            'to'    => '1315875600', //2011-09-12 22:30:00 -0230
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
            'from'  => '1317173403', //2011-09-27 23:00:03 -0230
            'to'    => '1317259802', //2011-09-28 23:00:02 -0230
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
            'from'  => '1319325000', //2011-10-22 20:40:00 -0230
            'to'    => '1319929800', //2011-10-29 20:40:00 -0230
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
            'from'  => '1311222601', //2011-07-21 02:00:01  -0230
            'to'    => '1313901000', //2011-08-21 02:00:00 -0230
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
            'from'  => '1282357801', //2010-08-21 00:00:01 -0230
            'to'    => '1313893800', //2011-08-21 00:00:00 -0230
        ),
    );


    protected static $RESULTS_PER_MONTH_DAY = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            array(
                'id'            => '17',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            array(
                'id'            => '17',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            array(
                'id'            => '6',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '8',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            array(
                'id'            => '17',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '24',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '11',
                'period_name'   => '11',
            ),
            array(
                'id'            => '6',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '8',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
        ),
        self::TYPE_ALL => array(
            array(
                'id'            => '17',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '24',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '11',
                'period_name'   => '11',
            ),
            array(
                'id'            => '6',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '27',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '8',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '30',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
        ),
    );

    protected static $RESULTS_PER_WEEK_DAY = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            array(
                'id'            => '17',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            array(
                'id'            => '17',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => array('6', '8'), 
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '17',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => array('6', '8'), 
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
            array(
                'id'            => '24',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_TUESDAY,
                'period_name'   => self::WEEK_DAY_TUESDAY,
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
        ),
        self::TYPE_ALL => array(
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '17',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => array('6', '8'), 
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
            array(
                'id'            => '24',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_TUESDAY,
                'period_name'   => self::WEEK_DAY_TUESDAY,
            ),
            array(
                'id'            => '27',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_WEDNESDAY,
                'period_name'   => self::WEEK_DAY_WEDNESDAY,
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '30',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
        ),
    );

    protected static $RESULTS_PER_HOUR = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            array(
                'id'            => '17',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '0',
                'period_name'   => '0',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            array(
                'id'            => '17',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '0',
                'period_name'   => '0',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '4',
                'period_name'   => '4',
            ),
            array(
                'id'            => array('6', '8'), 
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            array(
                'id'            => '17',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '0',
                'period_name'   => '0',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '4',
                'period_name'   => '4',
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '24',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => array('6', '8'), 
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
        ),
        self::TYPE_ALL => array(
            array(
                'id'            => '17',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '0',
                'period_name'   => '0',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '4',
                'period_name'   => '4',
            ),
            array(
                'id'            => '27',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '5',
                'period_name'   => '5',
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '24',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '30',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => array('6', '8'), 
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
        ),
    );
}


