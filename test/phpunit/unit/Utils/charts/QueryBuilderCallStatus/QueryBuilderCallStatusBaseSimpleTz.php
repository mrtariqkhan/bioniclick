<?php

require_once dirname(__FILE__) . '/QueryBuilderCallStatusBaseProvided.php';

/**
 * Description of QueryBuilderCallStatusBaseSimpleTz
 * 
 * @author fairdev
 */
abstract class QueryBuilderCallStatusBaseSimpleTz extends QueryBuilderCallStatusBaseProvided {

    const TYPE_CALL_ID_21 = 21;
    const TYPE_CALL_ID_22 = 22;
    const TYPE_CALL_ID_23 = 23;
    const TYPE_CALL_ID_24 = 24;
    const TYPE_CALL_ID_25 = 25;
//    const TYPE_CALL_ID_26 = 26;
//    const TYPE_CALL_ID_27 = 27;


    /**
     *
     * @param int $tzId one of self::TIMEZONE_ID_*
     */
    protected function configureSuperAdminUserWithTz($tzId) {

        $user = $this->findUserSuperAdmin();

        $gmtDifference = self::getTzOffset($tzId);
        $this->setGmtDifference($gmtDifference);

        $this->setUser($user);
        $this->setUserKind('SuperAdmin');
        $this->setTimezoneName($gmtDifference);
    }

    /**
     * 
     * @dataProvider providerCasesFound
     *
     * @param string $type one of QueryBuilderCallStatus::TYPE_PER_*
     * @param string $callId id of need call
     * @param string $specificAdditionalMessage
     */
    public function testOneCallFound($type, $callId, $specificAdditionalMessage = '') {

        $needResultArray = array();
        switch ($type) {
            case QueryBuilderCallStatus::TYPE_PER_DAY:
                $needResultArray = $this->resultsPerMonthDay;
                break;
            case QueryBuilderCallStatus::TYPE_PER_WEEK_DAY:
                $needResultArray = $this->resultsPerWeekDay;
                break;
            case QueryBuilderCallStatus::TYPE_PER_HOUR:
                $needResultArray = $this->resultsPerHour;
                break;
            default:
                throw new Exception("Invalid QueryBuilderCallStatus type");
        }

        $filterValues = $this->filterValues[$callId];

        $queryResult = self::getResultArray($filterValues, $type);
        $needResult = array($needResultArray[$callId]);
        $this->assertEquals(
            $needResult,
            $queryResult,
<<<EOD
    Call (id=$callId) has been found with mistakes 
        (an attempt to find $this->chartKind by $type for '{$this->userKind}' user with timezone '{$this->timezoneName}').
        $specificAdditionalMessage
EOD
        );
    }

    /**
     * 
     * @dataProvider providerCasesNotFound
     *
     * @param string $type one of QueryBuilderCallStatus::TYPE_PER_*
     * @param string $callId id of need call
     * @param string $specificAdditionalMessage
     */
    public function testNooneCallFound($type, $callId, $specificAdditionalMessage = '') {

        $needResultArray = array();
        switch ($type) {
            case QueryBuilderCallStatus::TYPE_PER_DAY:
                $needResultArray = $this->resultsPerMonthDay;
                break;
            case QueryBuilderCallStatus::TYPE_PER_WEEK_DAY:
                $needResultArray = $this->resultsPerWeekDay;
                break;
            case QueryBuilderCallStatus::TYPE_PER_HOUR:
                $needResultArray = $this->resultsPerHour;
                break;
            default:
                throw new Exception("Invalid QueryBuilderCallStatus type");
        }

        $filterValues = $this->filterValues[$callId];

        $queryResult = self::getResultArray($filterValues, $type);

        $this->assertEquals(
            0, 
            count($queryResult),
<<<EOD
    Call (id=$callId) has been found.
        (an attempt to find $this->chartKind by $type for '{$this->userKind}' user with timezone '{$this->timezoneName}').
        $specificAdditionalMessage
EOD
        );
    }

    public function providerCasesFound() {
        return array(
            'case 01' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_CALL_ID_21, 'filterValues["from"] = tic.start_time = filterValues["to"].'),
            'case 02' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_CALL_ID_21, 'filterValues["from"] = tic.start_time = filterValues["to"].'),
            'case 03' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_CALL_ID_21, 'filterValues["from"] = tic.start_time = filterValues["to"].'),
            'case 04' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_CALL_ID_22, 'filterValues["from"] = tic.start_time < filterValues["to"].'),
            'case 05' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_CALL_ID_22, 'filterValues["from"] = tic.start_time < filterValues["to"].'),
            'case 06' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_CALL_ID_22, 'filterValues["from"] = tic.start_time < filterValues["to"].'),
            'case 07' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_CALL_ID_24, 'filterValues["from"] < tic.start_time = filterValues["to"].'),
            'case 08' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_CALL_ID_24, 'filterValues["from"] < tic.start_time = filterValues["to"].'),
            'case 09' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_CALL_ID_24, 'filterValues["from"] < tic.start_time = filterValues["to"].'),
            'case 10' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_CALL_ID_25, 'filterValues["from"] < tic.start_time < filterValues["to"].'),
            'case 11' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_CALL_ID_25, 'filterValues["from"] < tic.start_time < filterValues["to"].'),
            'case 12' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_CALL_ID_25, 'filterValues["from"] < tic.start_time < filterValues["to"].'),
        );
    }

    public function providerCasesNotFound() {
        return array(
            'case 13' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_CALL_ID_23, 'filterValues["from"] < tic.start_time < filterValues["to"]. It is mistake because it is phone number from Bionic pool.'),
            'case 14' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_CALL_ID_23, 'filterValues["from"] < tic.start_time < filterValues["to"]. It is mistake because it is phone number from Bionic pool.'),
            'case 15' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_CALL_ID_23, 'filterValues["from"] < tic.start_time < filterValues["to"]. It is mistake because it is phone number from Bionic pool.'),
        );
    }
}
