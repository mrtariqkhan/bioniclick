<?php

require_once dirname(__FILE__) . '/QueryBuilderCallStatusBaseTz.php';

/**
 * Description of QueryBuilderCallStatusTz2Test
 * 
 * @author fairdev
 */
class QueryBuilderCallStatusTz2Test extends QueryBuilderCallStatusBaseTz {

    protected function configureFilterValuesAndResults() {

        $this->setFilterValues(         self::$FILTER_VALUES);
        $this->setResultsPerMonthDay(   self::$RESULTS_PER_MONTH_DAY);
        $this->setResultsPerWeekDay(    self::$RESULTS_PER_WEEK_DAY);
        $this->setResultsPerHour(       self::$RESULTS_PER_HOUR);
    }

    protected function configureUserWithTz() {
        $this->configureSuperAdminUserWithTz(self::TIMEZONE_ID_MINUS_2_AND_A_HALF);
    }

    protected static $FILTER_VALUES = array(
        self::TYPE_ONE_HOUR_ONE_CALL => array(
            'from'  => '1318403000', //2011-10-12 04:33:20 -0230
            'to'    => '1318406600', //2011-10-12 05:33:20 -0230
        ),
        self::TYPE_ONE_HOUR_ONE_TYPE_SEVERAL_CALLS => array(
            'from'  => '1318377665', //2011-10-11 21:31:05 -0230
            'to'    => '1318381265', //2011-10-11 22:31:05 -0230
        ),
        self::TYPE_ONE_HOUR_SEVERAL_TYPES => array(
            'from'  => '1317524400', //2011-10-02 00:30:00 -0230
            'to'    => '1317527999', //2011-10-02 01:29:59 -0230
        ),
        self::TYPE_ONE_DAY_ONE_CALL => array(
            'from'  => '1316473669', //2011-09-19 20:37:49 -0230
            'to'    => '1316560069', //2011-09-20 20:37:49 -0230
        ),
        self::TYPE_ONE_DAY_ONE_TYPE_SEVERAL_CALLS => array(
            'from'  => '1318316400', //2011-10-11 04:30:00 -0230
            'to'    => '1318402799', //2011-10-12 04:29:59 -0230
        ),
        self::TYPE_ONE_DAY_SEVERAL_TYPES => array(
            'from'  => '1318320961', //2011-10-11 05:46:01 -0230
            'to'    => '1318407300', //2011-10-12 05:45:00 -0230
        ),
        self::TYPE_ONE_WEEK_ONE_CALL => array(
            'from'  => '1316007017', //2011-09-14 11:00:17 -0230
            'to'    => '1316611756', //2011-09-21 10:59:16 -0230
        ),
        self::TYPE_ONE_WEEK_ONE_TYPE_SEVERAL_CALLS => array(
            'from'  => '1315875481', //2011-09-12 22:28:01 -0230
            'to'    => '1316480220', //2011-09-19 22:27:00 -0230
        ),
        self::TYPE_ONE_WEEK_SEVERAL_TYPES => array(
            'from'  => '1317525420', //2011-10-02 00:47:00 -0230
            'to'    => '1318043819', //2011-10-08 00:46:59 -0230
        ),
        self::TYPE_ONE_MONTH_ONE_CALL => array(
            'from'  => '1311206400', //2011-07-20 21:30:00 -0230
            'to'    => '1313884799', //2011-08-20 21:29:59 -0230
        ),
        self::TYPE_ONE_MONTH_ONE_TYPE_SEVERAL_CALLS => array(
            'from'  => '1311638399', //2011-07-25 21:29:59 -0230
            'to'    => '1314316798', //2011-08-25 21:29:58 -0230
        ),
        self::TYPE_ONE_MONTH_SEVERAL_TYPES => array(
            'from'  => '1313197201', //2011-08-12 22:30:01 -0230
            'to'    => '1315875600', //2011-09-12 22:30:00 -0230
        ),
        self::TYPE_ONE_YEAR_ONE_CALL => array(
            'from'  => '1282282468', //2010-08-20 03:04:28 -0230
            'to'    => '1313818467', //2011-08-20 03:04:27 -0230
        ),
        self::TYPE_ONE_YEAR_ONE_TYPE_SEVERAL_CALLS => array(
            'from'  => '1283313660', //2010-09-01 01:31:00 -0230
            'to'    => '1314849659', //2011-09-01 01:30:59 -0230
        ),
        self::TYPE_ONE_YEAR_SEVERAL_TYPES => array(
            'from'  => '1284944281', //2010-09-19 22:28:01 -0230
            'to'    => '1316480280', //2011-09-19 22:28:00 -0230
        ),
        self::TYPE_ALL => array(
        ),
    );

    protected static $RESULTS_PER_MONTH_DAY = array(
        self::TYPE_ONE_HOUR_ONE_CALL => array(
            array(
                'id'            => '27',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
        ),
        self::TYPE_ONE_HOUR_ONE_TYPE_SEVERAL_CALLS => array(
            array(
                'id'            => array('24', '25'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '11',
                'period_name'   => '11',
            ),
        ),
        self::TYPE_ONE_HOUR_SEVERAL_TYPES => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => array('17', '19'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
        ),
        self::TYPE_ONE_DAY_ONE_CALL => array(
            array(
                'id'            => '8',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
        ),
        self::TYPE_ONE_DAY_ONE_TYPE_SEVERAL_CALLS => array(
            array(
                'id'            => array('24', '25', '26'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '3',
                'period_num'    => '11',
                'period_name'   => '11',
            ),
        ),
        self::TYPE_ONE_DAY_SEVERAL_TYPES => array(
            array(
                'id'            => array('24', '25', '26'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '3',
                'period_num'    => '11',
                'period_name'   => '11',
            ),
            array(
                'id'            => '27',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
        ),
        self::TYPE_ONE_WEEK_ONE_CALL => array(
            array(
                'id'            => '8',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
        ),
        self::TYPE_ONE_WEEK_ONE_TYPE_SEVERAL_CALLS => array(
            array(
                'id'            => '6',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '8',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
        ),
        self::TYPE_ONE_WEEK_SEVERAL_TYPES => array(
            array(
                'id'            => array('17', '19'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '21',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '3',
                'period_name'   => '3',
            ),
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '6',
                'period_name'   => '6',
            ),
        ),
        self::TYPE_ONE_MONTH_ONE_CALL => array(
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
        ),
        self::TYPE_ONE_MONTH_ONE_TYPE_SEVERAL_CALLS => array(
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
        ),
        self::TYPE_ONE_MONTH_SEVERAL_TYPES => array(
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
        ),
        self::TYPE_ONE_YEAR_ONE_CALL => array(
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
        ),
        self::TYPE_ONE_YEAR_ONE_TYPE_SEVERAL_CALLS => array(
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
        ),
        self::TYPE_ONE_YEAR_SEVERAL_TYPES => array(
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '6',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '8',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
        ),
        self::TYPE_ALL => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => array('17', '19'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '21',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '3',
                'period_name'   => '3',
            ),
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '6',
                'period_name'   => '6',
            ),
            array(
                'id'            => array('24', '25', '26'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '3',
                'period_num'    => '11',
                'period_name'   => '11',
            ),
            array(
                'id'            => array('5', '27'),
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '2',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => array('1', '2', '6'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '3',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '8',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '10',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '28',
                'period_name'   => '28',
            ),
            array(
                'id'            => array('11', '12', '28'),
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '3',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
            array(
                'id'            => '29',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
            array(
                'id'            => '30',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
            array(
                'id'            => array('13', '31'),
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '2',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
            array(
                'id'            => '14',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
        ),
    );

    protected static $RESULTS_PER_WEEK_DAY = array(
        self::TYPE_ONE_HOUR_ONE_CALL => array(
            array(
                'id'            => '27',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_WEDNESDAY,
                'period_name'   => self::WEEK_DAY_WEDNESDAY,
            ),
        ),
        self::TYPE_ONE_HOUR_ONE_TYPE_SEVERAL_CALLS => array(
            array(
                'id'            => array('24', '25'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_TUESDAY,
                'period_name'   => self::WEEK_DAY_TUESDAY,
            ),
        ),
        self::TYPE_ONE_HOUR_SEVERAL_TYPES => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => array('17', '19'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
        ),
        self::TYPE_ONE_DAY_ONE_CALL => array(
            array(
                'id'            => '8',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
        ),
        self::TYPE_ONE_DAY_ONE_TYPE_SEVERAL_CALLS => array(
            array(
                'id'            => array('24', '25', '26'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '3',
                'period_num'    => self::WEEK_DAY_NUM_TUESDAY,
                'period_name'   => self::WEEK_DAY_TUESDAY,
            ),
        ),
        self::TYPE_ONE_DAY_SEVERAL_TYPES => array(
            array(
                'id'            => array('24', '25', '26'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '3',
                'period_num'    => self::WEEK_DAY_NUM_TUESDAY,
                'period_name'   => self::WEEK_DAY_TUESDAY,
            ),
            array(
                'id'            => '27',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_WEDNESDAY,
                'period_name'   => self::WEEK_DAY_WEDNESDAY,
            ),
        ),
        self::TYPE_ONE_WEEK_ONE_CALL => array(
            array(
                'id'            => '8',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
        ),
        self::TYPE_ONE_WEEK_ONE_TYPE_SEVERAL_CALLS => array(
            array(
                'id'            => array('6', '8'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
        ),
        self::TYPE_ONE_WEEK_SEVERAL_TYPES => array(
            array(
                'id'            => array('17', '19'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '21',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
        ),
        self::TYPE_ONE_MONTH_ONE_CALL => array(
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
        ),
        self::TYPE_ONE_MONTH_ONE_TYPE_SEVERAL_CALLS => array(
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
        ),
        self::TYPE_ONE_MONTH_SEVERAL_TYPES => array(
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
        ),
        self::TYPE_ONE_YEAR_ONE_CALL => array(
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
        ),
        self::TYPE_ONE_YEAR_ONE_TYPE_SEVERAL_CALLS => array(
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
        ),
        self::TYPE_ONE_YEAR_SEVERAL_TYPES => array(
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
            array(
                'id'            => array('6', '8'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
        ),
        self::TYPE_ALL => array(
            array(
                'id'            => array('4', '31'),
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => array('17', '19'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => array('1', '2', '6', '8'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '4',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
            array(
                'id'            => array('5', '21'),
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
            array(
                'id'            => array('24', '25', '26'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '3',
                'period_num'    => self::WEEK_DAY_NUM_TUESDAY,
                'period_name'   => self::WEEK_DAY_TUESDAY,
            ),
            array(
                'id'            => '10',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_WEDNESDAY,
                'period_name'   => self::WEEK_DAY_WEDNESDAY,
            ),
            array(
                'id'            => '27',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_WEDNESDAY,
                'period_name'   => self::WEEK_DAY_WEDNESDAY,
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
            array(
                'id'            => array('11', '12', '22'),
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '3',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
            array(
                'id'            => array('3', '13'),
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '14',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '28',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
            array(
                'id'            => '29',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
            array(
                'id'            => '30',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
        ),
    );

    protected static $RESULTS_PER_HOUR = array(
        self::TYPE_ONE_HOUR_ONE_CALL => array(
            array(
                'id'            => '27',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '5',
                'period_name'   => '5',
            ),
        ),
        self::TYPE_ONE_HOUR_ONE_TYPE_SEVERAL_CALLS => array(
            array(
                'id'            => '24',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '25',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
        ),
        self::TYPE_ONE_HOUR_SEVERAL_TYPES => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '0',
                'period_name'   => '0',
            ),
            array(
                'id'            => '17',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '0',
                'period_name'   => '0',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '19',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
        ),
        self::TYPE_ONE_DAY_ONE_CALL => array(
            array(
                'id'            => '8',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
        ),
        self::TYPE_ONE_DAY_ONE_TYPE_SEVERAL_CALLS => array(
            array(
                'id'            => '24',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '25',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '26',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '23',
                'period_name'   => '23',
            ),
        ),
        self::TYPE_ONE_DAY_SEVERAL_TYPES => array(
            array(
                'id'            => '24',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '25',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '26',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '23',
                'period_name'   => '23',
            ),
            array(
                'id'            => '27',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '5',
                'period_name'   => '5',
            ),
        ),
        self::TYPE_ONE_WEEK_ONE_CALL => array(
            array(
                'id'            => '8',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
        ),
        self::TYPE_ONE_WEEK_ONE_TYPE_SEVERAL_CALLS => array(
            array(
                'id'            => array('6', '8'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
        ),
        self::TYPE_ONE_WEEK_SEVERAL_TYPES => array(
            array(
                'id'            => '17',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '0',
                'period_name'   => '0',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '19',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '21',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
        ),
        self::TYPE_ONE_MONTH_ONE_CALL => array(
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
        ),
        self::TYPE_ONE_MONTH_ONE_TYPE_SEVERAL_CALLS => array(
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '4',
                'period_name'   => '4',
            ),
        ),
        self::TYPE_ONE_MONTH_SEVERAL_TYPES => array(
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '4',
                'period_name'   => '4',
            ),
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
        ),
        self::TYPE_ONE_YEAR_ONE_CALL => array(
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
        ),
        self::TYPE_ONE_YEAR_ONE_TYPE_SEVERAL_CALLS => array(
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '4',
                'period_name'   => '4',
            ),
        ),
        self::TYPE_ONE_YEAR_SEVERAL_TYPES => array(
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '4',
                'period_name'   => '4',
            ),
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => array('6', '8'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
        ),
        self::TYPE_ALL => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '0',
                'period_name'   => '0',
            ),
            array(
                'id'            => '17',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '0',
                'period_name'   => '0',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '19',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => array('4', '13', '31'),
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '3',
                'period_num'    => '4',
                'period_name'   => '4',
            ),
            array(
                'id'            => '14',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '5',
                'period_name'   => '5',
            ),
            array(
                'id'            => '27',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '5',
                'period_name'   => '5',
            ),
            array(
                'id'            => array('11', '28'),
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '2',
                'period_num'    => '14',
                'period_name'   => '14',
            ),
            array(
                'id'            => '12',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '29',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '20',
                'period_name'   => '20',
            ),
            array(
                'id'            => array('9', '15'),
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '2',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '24',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '30',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => array('1', '2', '6', '8', '25'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '5',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => array('5', '21'),
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '2',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '10',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '26',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '23',
                'period_name'   => '23',
            ),
        ),
    );
}
