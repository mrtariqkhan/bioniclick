<?php

require_once dirname(__FILE__) . '/QueryBuilderCallStatusBaseProvided.php';

/**
 * Description of QueryBuilderCallStatusBaseCompanyTree
 * 
 * @author fairdev
 */
abstract class QueryBuilderCallStatusBaseCompanyTree extends QueryBuilderCallStatusBaseProvided {

    const TYPE_FOUND_ONE_HOUR       = '(Found) One Hour';
    const TYPE_FOUND_ONE_DAY        = '(Found) One Day';
    const TYPE_FOUND_ONE_WEEK       = '(Found) One Week';
    const TYPE_FOUND_ONE_MONTH      = '(Found) One Month';
    const TYPE_FOUND_ONE_YEAR       = '(Found) One Year';

    const TYPE_NOT_FOUND_ONE_HOUR   = '(Not Found) One Hour';
    const TYPE_NOT_FOUND_ONE_DAY    = '(Not Found) One Day';
    const TYPE_NOT_FOUND_ONE_WEEK   = '(Not Found) One Week';
    const TYPE_NOT_FOUND_ONE_MONTH  = '(Not Found) One Month';
    const TYPE_NOT_FOUND_ONE_YEAR   = '(Not Found) One Year';

    const TYPE_ALL                  = 'All time';


    const ID_COMPANY_BIONIC                     = 1;
    const ID_COMPANY_RESELLER_ACQUISIO          = 2;
    const ID_COMPANY_AGENCY_REALSEARCH          = 4;
    const ID_ADVERTISER_BIONIC_ADVERTISER       = 1;
    const ID_ADVERTISER_RESELLER_ACQUISIO_TRIPS = 3;
    const ID_ADVERTISER_AGENCY_REALSEARCH_IPS   = 4;

    /**
     *
     * @dataProvider providerCases
     * 
     * @param string $type one of QueryBuilderCallStatus::TYPE_PER_*
     * @param string $testKey one of QueryBuilderCallStatusTz4Test::TYPE_*
     * @param string $specificAdditionalMessage
     */
    public function testCase($type, $testKey, $specificAdditionalMessage = '') {

        $needResultArray = array();
        switch ($type) {
            case QueryBuilderCallStatus::TYPE_PER_DAY:
                $needResultArray = $this->resultsPerMonthDay;
                break;
            case QueryBuilderCallStatus::TYPE_PER_WEEK_DAY:
                $needResultArray = $this->resultsPerWeekDay;
                break;
            case QueryBuilderCallStatus::TYPE_PER_HOUR:
                $needResultArray = $this->resultsPerHour;
                break;
            default:
                throw new Exception("Invalid QueryBuilderCallStatus type");
        }

        $filterValues = $this->filterValues[$testKey];

        $queryResult = $this->getResultArray($filterValues, $type);
        $needResult = $needResultArray[$testKey];

        $this->assertArrayAreSame(
            $needResult, 
            $queryResult, 
<<<EOD
    Error in Test ($testKey)
        (an attempt to find $this->chartKind by $type for '{$this->userKind}' user with timezone '{$this->timezoneName}').
        $specificAdditionalMessage
EOD
        );
    }

    public function providerCases() {
        return array(
            'case 01' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_FOUND_ONE_HOUR),
            'case 02' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_FOUND_ONE_HOUR),
            'case 03' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_FOUND_ONE_HOUR),
            'case 04' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_FOUND_ONE_DAY),
            'case 05' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_FOUND_ONE_DAY),
            'case 06' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_FOUND_ONE_DAY),
            'case 07' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_FOUND_ONE_WEEK),
            'case 08' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_FOUND_ONE_WEEK),
            'case 09' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_FOUND_ONE_WEEK),
            'case 10' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_FOUND_ONE_MONTH),
            'case 11' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_FOUND_ONE_MONTH),
            'case 12' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_FOUND_ONE_MONTH),
            'case 13' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_FOUND_ONE_YEAR),
            'case 14' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_FOUND_ONE_YEAR),
            'case 15' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_FOUND_ONE_YEAR),

            'case 21' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_NOT_FOUND_ONE_HOUR),
            'case 22' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_NOT_FOUND_ONE_HOUR),
            'case 23' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_NOT_FOUND_ONE_HOUR),
            'case 24' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_NOT_FOUND_ONE_DAY),
            'case 25' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_NOT_FOUND_ONE_DAY),
            'case 26' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_NOT_FOUND_ONE_DAY),
            'case 27' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_NOT_FOUND_ONE_WEEK),
            'case 28' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_NOT_FOUND_ONE_WEEK),
            'case 29' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_NOT_FOUND_ONE_WEEK),
            'case 30' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_NOT_FOUND_ONE_MONTH),
            'case 31' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_NOT_FOUND_ONE_MONTH),
            'case 32' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_NOT_FOUND_ONE_MONTH),
            'case 33' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_NOT_FOUND_ONE_YEAR),
            'case 34' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_NOT_FOUND_ONE_YEAR),
            'case 35' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_NOT_FOUND_ONE_YEAR),

            'case 41' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_ALL),
            'case 42' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_ALL),
            'case 43' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_ALL),
        );
    }
}
