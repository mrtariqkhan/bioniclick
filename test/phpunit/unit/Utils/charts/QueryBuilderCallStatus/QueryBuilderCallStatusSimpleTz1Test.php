<?php

require_once dirname(__FILE__) . '/QueryBuilderCallStatusBaseSimpleTz.php';

/**
 * Description of QueryBuilderCallStatusSimpleTz1Test
 * 
 * @author fairdev
 */
class QueryBuilderCallStatusSimpleTz1Test extends QueryBuilderCallStatusBaseSimpleTz {

    protected function configureFilterValuesAndResults() {

        $this->setFilterValues(         self::$FILTER_VALUES);
        $this->setResultsPerMonthDay(   self::$RESULTS_PER_MONTH_DAY);
        $this->setResultsPerWeekDay(    self::$RESULTS_PER_WEEK_DAY);
        $this->setResultsPerHour(       self::$RESULTS_PER_HOUR);
    }

    protected function configureUserWithTz() {
        $this->configureSuperAdminUserWithTz(self::TIMEZONE_ID_MINUS_5);
    }

    protected static $FILTER_VALUES = array(
        self::TYPE_CALL_ID_21 => array(//1317690000
            'from'  => '1317690000',
            'to'    => '1317690000',
        ),
        self::TYPE_CALL_ID_22 => array(//1317944400
            'from'  => '1317944400',
            'to'    => '1317944401',
        ),
        self::TYPE_CALL_ID_23 => array(//1318377485
            'from'  => '1318377484',
            'to'    => '1318377486',
        ),
        self::TYPE_CALL_ID_24 => array(//1318377727
            'from'  => '1318377720',
            'to'    => '1318377727',
        ),
        self::TYPE_CALL_ID_25 => array(//1318380237
            'from'  => '1318380234',
            'to'    => '1318380239',
        ),
//        self::TYPE_CALL_ID_26 => array(//1318386327
//            'from'  => '1318386324',
//            'to'    => '1318386328',
//        ),
//        self::TYPE_CALL_ID_27 => array(//1318406566
//            'from'  => '1318406560',
//            'to'    => '1318406567',
//        ),
    );

    protected static $RESULTS_PER_MONTH_DAY = array(
        self::TYPE_CALL_ID_21 => array(//2011-10-03 20:00:00 -0500
            'id'            => '21',
            'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
            'calls_amount'  => '1',
            'period_num'    => '3',
            'period_name'   => '3',
        ),
        self::TYPE_CALL_ID_22 => array(//2011-10-06 18:40:00 -0500
            'id'            => '22',
            'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
            'calls_amount'  => '1',
            'period_num'    => '6',
            'period_name'   => '6',
        ),
        self::TYPE_CALL_ID_23 => array(//2011-10-11 18:58:05 -0500
        ),
        self::TYPE_CALL_ID_24 => array(//2011-10-11 19:02:07 -0500
            'id'            => '24',
            'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
            'calls_amount'  => '1',
            'period_num'    => '11',
            'period_name'   => '11',
        ),
        self::TYPE_CALL_ID_25 => array(//2011-10-11 19:43:57 -0500
            'id'            => '25',
            'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
            'calls_amount'  => '1',
            'period_num'    => '11',
            'period_name'   => '11',
        ),
//        self::TYPE_CALL_ID_26 => array(//2011-10-11 21:25:27 -0500
//            'id'            => '26',
//            'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
//            'calls_amount'  => '1',
//            'period_num'    => '11',
//            'period_name'   => '11',
//        ),
//        self::TYPE_CALL_ID_27 => array(//2011-10-12 03:02:46 -0500
//            'id'            => '27',
//            'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
//            'calls_amount'  => '1',
//            'period_num'    => '12',
//            'period_name'   => '12',
//        ),
    );

    protected static $RESULTS_PER_WEEK_DAY = array(
        self::TYPE_CALL_ID_21 => array(//2011-10-03 20:00:00 -0500
            'id'            => '21',
            'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
            'calls_amount'  => '1',
            'period_num'    => self::WEEK_DAY_NUM_MONDAY,
            'period_name'   => self::WEEK_DAY_MONDAY,
        ),
        self::TYPE_CALL_ID_22 => array(//2011-10-06 18:40:00 -0500
            'id'            => '22',
            'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
            'calls_amount'  => '1',
            'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
            'period_name'   => self::WEEK_DAY_THURSDAY,
        ),
        self::TYPE_CALL_ID_23 => array(//2011-10-11 18:58:05 -0500
        ),
        self::TYPE_CALL_ID_24 => array(//2011-10-11 19:02:07 -0500
            'id'            => '24',
            'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
            'calls_amount'  => '1',
            'period_num'    => self::WEEK_DAY_NUM_TUESDAY,
            'period_name'   => self::WEEK_DAY_TUESDAY,
        ),
        self::TYPE_CALL_ID_25 => array(//2011-10-11 19:43:57 -0500
            'id'            => '25',
            'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
            'calls_amount'  => '1',
            'period_num'    => self::WEEK_DAY_NUM_TUESDAY,
            'period_name'   => self::WEEK_DAY_TUESDAY,
        ),
//        self::TYPE_CALL_ID_26 => array(//2011-10-11 21:25:27 -0500
//            'id'            => '26',
//            'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
//            'calls_amount'  => '1',
//            'period_num'    => self::WEEK_DAY_NUM_TUESDAY,
//            'period_name'   => self::WEEK_DAY_TUESDAY,
//        ),
//        self::TYPE_CALL_ID_27 => array(//2011-10-12 03:02:46 -0500
//            'id'            => '27',
//            'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
//            'calls_amount'  => '1',
//            'period_num'    => self::WEEK_DAY_NUM_WEDNESDAY,
//            'period_name'   => self::WEEK_DAY_WEDNESDAY,
//        ),
    );

    protected static $RESULTS_PER_HOUR = array(
        self::TYPE_CALL_ID_21 => array(//2011-10-03 20:00:00 -0500
            'id'            => '21',
            'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
            'calls_amount'  => '1',
            'period_num'    => '20',
            'period_name'   => '20',
        ),
        self::TYPE_CALL_ID_22 => array(//2011-10-06 18:40:00 -0500
            'id'            => '22',
            'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
            'calls_amount'  => '1',
            'period_num'    => '18',
            'period_name'   => '18',
        ),
        self::TYPE_CALL_ID_23 => array(//2011-10-11 18:58:05 -0500
        ),
        self::TYPE_CALL_ID_24 => array(//2011-10-11 19:02:07 -0500
            'id'            => '24',
            'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
            'calls_amount'  => '1',
            'period_num'    => '19',
            'period_name'   => '19',
        ),
        self::TYPE_CALL_ID_25 => array(//2011-10-11 19:43:57 -0500
            'id'            => '25',
            'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
            'calls_amount'  => '1',
            'period_num'    => '19',
            'period_name'   => '19',
        ),
//        self::TYPE_CALL_ID_26 => array(//2011-10-11 21:25:27 -0500
//            'id'            => '26',
//            'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
//            'calls_amount'  => '1',
//            'period_num'    => '21',
//            'period_name'   => '21',
//        ),
//        self::TYPE_CALL_ID_27 => array(//2011-10-12 03:02:46 -0500
//            'id'            => '27',
//            'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
//            'calls_amount'  => '1',
//            'period_num'    => '3',
//            'period_name'   => '3',
//        ),
    );
}
