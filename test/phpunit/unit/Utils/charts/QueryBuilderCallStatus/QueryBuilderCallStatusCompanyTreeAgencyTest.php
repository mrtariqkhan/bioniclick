<?php

require_once dirname(__FILE__) . '/QueryBuilderCallStatusBaseCompanyTree.php';

/**
 * Description of QueryBuilderCallStatusCompanyTreeAgencyTest
 * 
 * @author fairdev
 */
class QueryBuilderCallStatusCompanyTreeAgencyTest extends QueryBuilderCallStatusBaseCompanyTree {

    protected function configureFilterValuesAndResults() {

        $this->setFilterValues(         self::$FILTER_VALUES);
        $this->setResultsPerMonthDay(   self::$RESULTS_PER_MONTH_DAY);
        $this->setResultsPerWeekDay(    self::$RESULTS_PER_WEEK_DAY);
        $this->setResultsPerHour(       self::$RESULTS_PER_HOUR);
    }

    protected function configureUserWithTz() {
        $this->configureCompanyTreeUserWithTz(
            self::ID_COMPANY_AGENCY_REALSEARCH,
            null,
            self::TIMEZONE_ID_PLUS_6
        );
    }


    protected static $FILTER_VALUES = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            'from'  => '1319929201', //2011-10-30 05:00:01 +0600
            'to'    => '1319932800', //2011-10-30 06:00:00 +0600
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            'from'  => '1317295209', //2011-09-29 17:20:09 +0600
            'to'    => '1317381608', //2011-09-30 17:20:08 +0600
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            'from'  => '1317496722', //2011-10-02 01:18:42 +0600
            'to'    => '1318101521', //2011-10-09 01:18:41 +0600
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            'from'  => '1315316128', //2011-09-06 19:35:28 +0600
            'to'    => '1317908127', //2011-10-06 19:35:27 +0600
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            'from'  => '1285779540', //2010-09-29 22:59:00 +0600
            'to'    => '1317315539', //2011-09-29 22:58:59 +0600
        ),

        self::TYPE_ALL => array(
        ),

        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'from'  => '1313776985', //2011-08-20 00:03:05 +0600
            'to'    => '1313863384', //2011-08-21 00:03:04 +0600
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
            'from'  => '1316476801', //2011-09-20 06:00:01 +0600
            'to'    => '1316563200', //2011-09-21 06:00:00 +0600
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
            'from'  => '1313341388', //2011-08-14 23:03:08 +0600
            'to'    => '1313946187', //2011-08-21 23:03:07 +0600
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
            'from'  => '1311162133', //2011-07-20 17:42:13 +0600
            'to'    => '1313840532', //2011-08-20 17:42:12 +0600
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
            'from'  => '1278067942', //2010-07-02 16:52:22 +0600
            'to'    => '1309603941', //2011-07-02 16:52:21 +0600
        ),
    );


    protected static $RESULTS_PER_MONTH_DAY = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            array(
                'id'            => '29',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            array(
                'id'            => '11',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '7',
                'period_name'   => '7',
            ),
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '13',
                'period_name'   => '13',
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '23',
                'period_name'   => '23',
            ),
            array(
                'id'            => '11',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '13',
                'period_name'   => '13',
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '23',
                'period_name'   => '23',
            ),
        ),
        self::TYPE_ALL => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '7',
                'period_name'   => '7',
            ),
            array(
                'id'            => '26',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '13',
                'period_name'   => '13',
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '23',
                'period_name'   => '23',
            ),
            array(
                'id'            => array('11', '28'),
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '2',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
            array(
                'id'            => '29',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
        ),
    );

    protected static $RESULTS_PER_WEEK_DAY = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            array(
                'id'            => '29',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            array(
                'id'            => '11',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_TUESDAY,
                'period_name'   => self::WEEK_DAY_TUESDAY,
            ),
            array(
                'id'            => '11',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_TUESDAY,
                'period_name'   => self::WEEK_DAY_TUESDAY,
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
        ),
        self::TYPE_ALL => array(
            array(
                'id'            => array('16', '29'),
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_TUESDAY,
                'period_name'   => self::WEEK_DAY_TUESDAY,
            ),
            array(
                'id'            => '26',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_WEDNESDAY,
                'period_name'   => self::WEEK_DAY_WEDNESDAY,
            ),
            array(
                'id'            => '11',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '28',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
        ),
    );

    protected static $RESULTS_PER_HOUR = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            array(
                'id'            => '29',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '5',
                'period_name'   => '5',
            ),
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '13',
                'period_name'   => '13',
            ),
            array(
                'id'            => '11',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '23',
                'period_name'   => '23',
            ),
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '9',
                'period_name'   => '9',
            ),
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '5',
                'period_name'   => '5',
            ),
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '6',
                'period_name'   => '6',
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '6',
                'period_name'   => '6',
            ),
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '9',
                'period_name'   => '9',
            ),
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '13',
                'period_name'   => '13',
            ),
            array(
                'id'            => '11',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '23',
                'period_name'   => '23',
            ),
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '6',
                'period_name'   => '6',
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '6',
                'period_name'   => '6',
            ),
        ),
        self::TYPE_ALL => array(
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '5',
                'period_name'   => '5',
            ),
            array(
                'id'            => '29',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '5',
                'period_name'   => '5',
            ),
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '6',
                'period_name'   => '6',
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '6',
                'period_name'   => '6',
            ),
            array(
                'id'            => '26',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '8',
                'period_name'   => '8',
            ),
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '9',
                'period_name'   => '9',
            ),
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '13',
                'period_name'   => '13',
            ),
            array(
                'id'            => array('11', '28'),
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '2',
                'period_num'    => '23',
                'period_name'   => '23',
            ),
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
        ),
    );
}
