<?php

require_once dirname(__FILE__) . '/QueryBuilderCallStatusBaseCompanyTree.php';

/**
 * Description of QueryBuilderCallStatusCompanyTreeBionicTest
 * 
 * @author fairdev
 */
class QueryBuilderCallStatusCompanyTreeBionicTest extends QueryBuilderCallStatusBaseCompanyTree {

    protected function configureFilterValuesAndResults() {

        $this->setFilterValues(         self::$FILTER_VALUES);
        $this->setResultsPerMonthDay(   self::$RESULTS_PER_MONTH_DAY);
        $this->setResultsPerWeekDay(    self::$RESULTS_PER_WEEK_DAY);
        $this->setResultsPerHour(       self::$RESULTS_PER_HOUR);
    }

    protected function configureUserWithTz() {
        $this->configureCompanyTreeUserWithTz(
            self::ID_COMPANY_BIONIC,
            null,
            self::TIMEZONE_ID_MINUS_5
        );
    }

    protected static $FILTER_VALUES = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            'from'  => '1316476894', //2011-09-19 19:01:34 -0500 
            'to'    => '1316480493', //2011-09-19 20:01:33 -0500
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            'from'  => '1317446104', //2011-10-01 00:15:04 -0500
            'to'    => '1317532503', //2011-10-02 00:15:03 -0500
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            'from'  => '1317944400', //2011-10-06 18:40:00 -0500
            'to'    => '1318462799', //2011-10-12 18:39:59 -0500
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            'from'  => '1315803721', //2011-09-12 00:02:01 -0500
            'to'    => '1318395720', //2011-10-12 00:02:00 -0500
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            'from'  => '1286843401', //2010-10-11 19:30:01 -0500
            'to'    => '1318379400', //2011-10-11 19:30:00 -0500
        ),

        self::TYPE_ALL => array(
        ),

        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'from'  => '1316480738', //2011-09-19 20:05:38 -0500
            'to'    => '1316477137', //2011-09-19 19:05:37 -0500
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
            'from'  => '1318291091', //2011-10-10 18:58:11 -0500
            'to'    => '1318377490', //2011-10-11 18:58:10 -0500
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
            'from'  => '1315270561', //2011-09-05 19:56:01 -0500
            'to'    => '1315875360', //2011-09-12 19:56:00 -0500
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
            'from'  => '1278982207', //2010-07-12 19:50:07 -0500
            'to'    => '1281660606', //2010-08-12 19:50:06 -0500
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
            'from'  => '1230789601', //2009-01-01 00:00:01 -0500
            'to'    => '1262325601', //2010-01-01 00:00:01 -0500
        ),
    );


    protected static $RESULTS_PER_MONTH_DAY = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            array(
                'id'            => '8',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => array('17', '19'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '6',
                'period_name'   => '6',
            ),
            array(
                'id'            => array('24', '25', '26'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '3',
                'period_num'    => '11',
                'period_name'   => '11',
            ),
            array(
                'id'            => '27',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '6',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '8',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '10',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '28',
                'period_name'   => '28',
            ),
            array(
                'id'            => array('11', '12'),
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '2',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
            array(
                'id'            => '14',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => array('17', '19'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '21',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '3',
                'period_name'   => '3',
            ),
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '6',
                'period_name'   => '6',
            ),
            array(
                'id'            => array('24', '25', '26'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '3',
                'period_num'    => '11',
                'period_name'   => '11',
            ),
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '6',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '8',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '10',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '28',
                'period_name'   => '28',
            ),
            array(
                'id'            => array('11', '12'),
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '2',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
            array(
                'id'            => '14',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => array('17', '19'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '21',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '3',
                'period_name'   => '3',
            ),
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '6',
                'period_name'   => '6',
            ),
            array(
                'id'            => '24',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '11',
                'period_name'   => '11',
            ),
        ),
        self::TYPE_ALL => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => array('17', '19'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '21',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '3',
                'period_name'   => '3',
            ),
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '6',
                'period_name'   => '6',
            ),
            array(
                'id'            => array('24', '25', '26'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '3',
                'period_num'    => '11',
                'period_name'   => '11',
            ),
            array(
                'id'            => array('1', '2', '6'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '3',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => array('5', '27'),
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '2',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '8',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '10',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '28',
                'period_name'   => '28',
            ),
            array(
                'id'            => array('11', '12', '28'),
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '3',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
            array(
                'id'            => '29',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
            array(
                'id'            => '30',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
            array(
                'id'            => array('13', '31'),
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '2',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
            array(
                'id'            => '14',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
        ),
    );

    protected static $RESULTS_PER_WEEK_DAY = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            array(
                'id'            => '8',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
            array(
                'id'            => array('17', '19'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
            array(
                'id'            => array('24', '25', '26'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '3',
                'period_num'    => self::WEEK_DAY_NUM_TUESDAY,
                'period_name'   => self::WEEK_DAY_TUESDAY,
            ),
            array(
                'id'            => '27',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_WEDNESDAY,
                'period_name'   => self::WEEK_DAY_WEDNESDAY,
            ),
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            array(
                'id'            => array('5', '21'),
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
            array(
                'id'            => array('6', '8'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
            array(
                'id'            => array('24', '25', '26'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '3',
                'period_num'    => self::WEEK_DAY_NUM_TUESDAY,
                'period_name'   => self::WEEK_DAY_TUESDAY,
            ),
            array(
                'id'            => '10',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_WEDNESDAY,
                'period_name'   => self::WEEK_DAY_WEDNESDAY,
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
            array(
                'id'            => array('11', '12'),
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '3',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '14',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
            array(
                'id'            => array('17', '19'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            array(
                'id'            => '4',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => array('5', '21'),
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
            array(
                'id'            => array('6', '8'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
            array(
                'id'            => '24',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_TUESDAY,
                'period_name'   => self::WEEK_DAY_TUESDAY,
            ),
            array(
                'id'            => '10',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_WEDNESDAY,
                'period_name'   => self::WEEK_DAY_WEDNESDAY,
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
            array(
                'id'            => array('11', '12'),
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '3',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
            array(
                'id'            => array('3', '13'),
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '14',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
            array(
                'id'            => array('17', '19'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
        ),
        self::TYPE_ALL => array(
            array(
                'id'            => array('4', '31'),
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_SUNDAY,
                'period_name'   => self::WEEK_DAY_SUNDAY,
            ),
            array(
                'id'            => array('1', '2', '6', '8'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '4',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
            array(
                'id'            => array('5', '21'),
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
            array(
                'id'            => array('24', '25', '26'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '3',
                'period_num'    => self::WEEK_DAY_NUM_TUESDAY,
                'period_name'   => self::WEEK_DAY_TUESDAY,
            ),
            array(
                'id'            => '10',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_WEDNESDAY,
                'period_name'   => self::WEEK_DAY_WEDNESDAY,
            ),
            array(
                'id'            => '27',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_WEDNESDAY,
                'period_name'   => self::WEEK_DAY_WEDNESDAY,
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
            array(
                'id'            => array('11', '12', '22'),
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '3',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
            array(
                'id'            => array('3', '13'),
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '14',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => array('16', '29'),
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
            array(
                'id'            => array('17', '19'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
            array(
                'id'            => array('18', '30'),
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
            array(
                'id'            => '28',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
        ),
    );

    protected static $RESULTS_PER_HOUR = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            array(
                'id'            => '8',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => array('17', '19'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '18',
                'period_name'   => '18',
            ),
            array(
                'id'            => array('24', '25'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '26',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '27',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '3',
                'period_name'   => '3',
            ),
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '14',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '3',
                'period_name'   => '3',
            ),
            array(
                'id'            => '11',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '12',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '17',
                'period_name'   => '17',
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '18',
                'period_name'   => '18',
            ),
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '18',
                'period_name'   => '18',
            ),
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => array('8', '24', '25'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '3',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '10',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '6',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '20',
                'period_name'   => '20',
            ),
            array(
                'id'            => '21',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '20',
                'period_name'   => '20',
            ),
            array(
                'id'            => '26',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => array('17', '19'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '14',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '3',
                'period_name'   => '3',
            ),
            array(
                'id'            => '11',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '12',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '17',
                'period_name'   => '17',
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '18',
                'period_name'   => '18',
            ),
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '18',
                'period_name'   => '18',
            ),
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => array('8', '24', '25'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '3',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '10',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '6',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '20',
                'period_name'   => '20',
            ),
            array(
                'id'            => '21',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '20',
                'period_name'   => '20',
            ),
            array(
                'id'            => '26',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => array('17', '19'), 
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            array(
                'id'            => array('4', '13'),
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '2',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '14',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '3',
                'period_name'   => '3',
            ),
            array(
                'id'            => '11',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '12',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '17',
                'period_name'   => '17',
            ),
            array(
                'id'            => '15',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '18',
                'period_name'   => '18',
            ),
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '18',
                'period_name'   => '18',
            ),
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => array('8', '24'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '10',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '20',
                'period_name'   => '20',
            ),
            array(
                'id'            => '6',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '20',
                'period_name'   => '20',
            ),
            array(
                'id'            => '21',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '20',
                'period_name'   => '20',
            ),
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => array('17', '19'), 
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
        ),
        self::TYPE_ALL => array(
            array(
                'id'            => array('4', '13', '31'), 
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '3',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => '14',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '3',
                'period_name'   => '3',
            ),
            array(
                'id'            => '27',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '3',
                'period_name'   => '3',
            ),
            array(
                'id'            => array('11', '28'),
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '2',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '12',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '17',
                'period_name'   => '17',
            ),
            array(
                'id'            => array('15', '29'),
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '2',
                'period_num'    => '18',
                'period_name'   => '18',
            ),
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '18',
                'period_name'   => '18',
            ),
            array(
                'id'            => array('1', '2', '8', '24', '25'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '5',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => array('10', '30'),
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '2',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '3',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '20',
                'period_name'   => '20',
            ),
            array(
                'id'            => '6',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '20',
                'period_name'   => '20',
            ),
            array(
                'id'            => '21',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '20',
                'period_name'   => '20',
            ),
            array(
                'id'            => '26',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => array('17', '19'),
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '2',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '18',
                'call_status'   => TwilioIncomingCallTable::STATUS_BUSY,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '20',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
        ),
    );
}
