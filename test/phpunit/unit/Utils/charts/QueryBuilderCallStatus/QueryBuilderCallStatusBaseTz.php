<?php

require_once dirname(__FILE__) . '/QueryBuilderCallStatusBaseProvided.php';

/**
 * Description of QueryBuilderCallStatusBaseTz
 * 
 * @author fairdev
 */
abstract class QueryBuilderCallStatusBaseTz extends QueryBuilderCallStatusBaseProvided {

    const TYPE_ONE_HOUR_ONE_CALL                    = 'One Hour - One Type - One Call';
    const TYPE_ONE_HOUR_ONE_TYPE_SEVERAL_CALLS      = 'One Hour - One Type - Several Calls';
    const TYPE_ONE_HOUR_SEVERAL_TYPES               = 'One Hour - Several Types - Several Calls';
    const TYPE_ONE_DAY_ONE_CALL                     = 'One Day - One Type - One Call';
    const TYPE_ONE_DAY_ONE_TYPE_SEVERAL_CALLS       = 'One Day - One Type - Several Calls';
    const TYPE_ONE_DAY_SEVERAL_TYPES                = 'One Day - Several Types - Several Calls';
    const TYPE_ONE_WEEK_ONE_CALL                    = 'One Week - One Type - One Call';
    const TYPE_ONE_WEEK_ONE_TYPE_SEVERAL_CALLS      = 'One Week - One Type - Several Calls';
    const TYPE_ONE_WEEK_SEVERAL_TYPES               = 'One Week - Several Types - Several Calls';
    const TYPE_ONE_MONTH_ONE_CALL                   = 'One Month - One Type - One Call';
    const TYPE_ONE_MONTH_ONE_TYPE_SEVERAL_CALLS     = 'One Month - One Type - Several Calls';
    const TYPE_ONE_MONTH_SEVERAL_TYPES              = 'One Month - Several Types - Several Calls';
    const TYPE_ONE_YEAR_ONE_CALL                    = 'One Year - One Type - One Call';
    const TYPE_ONE_YEAR_ONE_TYPE_SEVERAL_CALLS      = 'One Year - One Type - Several Calls';
    const TYPE_ONE_YEAR_SEVERAL_TYPES               = 'One Year - Several Types - Several Calls';
    const TYPE_ALL                                  = 'All';

    /**
     *
     * @param int $tzId one of self::TIMEZONE_ID_*
     */
    protected function configureSuperAdminUserWithTz($tzId) {

        $user = $this->findUserSuperAdmin();

        $gmtDifference = self::getTzOffset($tzId);
        $this->setGmtDifference($gmtDifference);

        $this->setUser($user);
        $this->setUserKind('SuperAdmin');
        $this->setTimezoneName($gmtDifference);
    }

    /**
     *
     * @dataProvider providerCases
     *
     * @param string $type one of QueryBuilderCallStatus::TYPE_PER_*
     * @param string $testKey one of QueryBuilderCallStatusTz4Test::TYPE_*
     * @param string $specificAdditionalMessage
     */
    public function testCase($type, $testKey, $specificAdditionalMessage = '') {

        $needResultArray = array();
        switch ($type) {
            case QueryBuilderCallStatus::TYPE_PER_DAY:
                $needResultArray = $this->resultsPerMonthDay;
                break;
            case QueryBuilderCallStatus::TYPE_PER_WEEK_DAY:
                $needResultArray = $this->resultsPerWeekDay;
                break;
            case QueryBuilderCallStatus::TYPE_PER_HOUR:
                $needResultArray = $this->resultsPerHour;
                break;
            default:
                throw new Exception("Invalid QueryBuilderCallStatus type");
        }

        $filterValues = $this->filterValues[$testKey];

        $queryResult = $this->getResultArray($filterValues, $type);
        $needResult = $needResultArray[$testKey];

        $this->assertArrayAreSame(
            $needResult, 
            $queryResult, 
<<<EOD
    Error in Test ($testKey)
        (an attempt to find $this->chartKind by $type for '{$this->userKind}' user with timezone '{$this->timezoneName}').
        $specificAdditionalMessage
EOD
        );
    }

    public function providerCases() {
        return array(
            'case 01' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_ONE_HOUR_ONE_CALL),
            'case 02' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_ONE_HOUR_ONE_CALL),
            'case 03' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_ONE_HOUR_ONE_CALL),
            'case 04' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_ONE_HOUR_ONE_TYPE_SEVERAL_CALLS),
            'case 05' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_ONE_HOUR_ONE_TYPE_SEVERAL_CALLS),
            'case 06' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_ONE_HOUR_ONE_TYPE_SEVERAL_CALLS),
            'case 07' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_ONE_HOUR_SEVERAL_TYPES),
            'case 08' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_ONE_HOUR_SEVERAL_TYPES),
            'case 09' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_ONE_HOUR_SEVERAL_TYPES),
            'case 11' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_ONE_DAY_ONE_CALL),
            'case 12' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_ONE_DAY_ONE_CALL),
            'case 13' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_ONE_DAY_ONE_CALL),
            'case 14' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_ONE_DAY_ONE_TYPE_SEVERAL_CALLS),
            'case 15' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_ONE_DAY_ONE_TYPE_SEVERAL_CALLS),
            'case 16' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_ONE_DAY_ONE_TYPE_SEVERAL_CALLS),
            'case 17' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_ONE_DAY_SEVERAL_TYPES),
            'case 18' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_ONE_DAY_SEVERAL_TYPES),
            'case 19' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_ONE_DAY_SEVERAL_TYPES),
            'case 20' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_ONE_WEEK_ONE_CALL),
            'case 21' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_ONE_WEEK_ONE_CALL),
            'case 22' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_ONE_WEEK_ONE_CALL),
            'case 23' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_ONE_WEEK_ONE_TYPE_SEVERAL_CALLS),
            'case 24' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_ONE_WEEK_ONE_TYPE_SEVERAL_CALLS),
            'case 25' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_ONE_WEEK_ONE_TYPE_SEVERAL_CALLS),
            'case 26' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_ONE_WEEK_SEVERAL_TYPES),
            'case 27' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_ONE_WEEK_SEVERAL_TYPES),
            'case 28' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_ONE_WEEK_SEVERAL_TYPES),
            'case 29' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_ONE_MONTH_ONE_CALL),
            'case 30' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_ONE_MONTH_ONE_CALL),
            'case 31' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_ONE_MONTH_ONE_CALL),
            'case 32' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_ONE_MONTH_ONE_TYPE_SEVERAL_CALLS),
            'case 33' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_ONE_MONTH_ONE_TYPE_SEVERAL_CALLS),
            'case 34' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_ONE_MONTH_ONE_TYPE_SEVERAL_CALLS),
            'case 35' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_ONE_MONTH_SEVERAL_TYPES),
            'case 36' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_ONE_MONTH_SEVERAL_TYPES),
            'case 37' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_ONE_MONTH_SEVERAL_TYPES),
            'case 38' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_ONE_YEAR_ONE_CALL),
            'case 39' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_ONE_YEAR_ONE_CALL),
            'case 41' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_ONE_YEAR_ONE_CALL),
            'case 42' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_ONE_YEAR_ONE_TYPE_SEVERAL_CALLS),
            'case 43' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_ONE_YEAR_ONE_TYPE_SEVERAL_CALLS),
            'case 44' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_ONE_YEAR_ONE_TYPE_SEVERAL_CALLS),
            'case 45' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_ONE_YEAR_SEVERAL_TYPES),
            'case 46' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_ONE_YEAR_SEVERAL_TYPES),
            'case 47' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_ONE_YEAR_SEVERAL_TYPES),
            'case 48' => array(QueryBuilderCallStatus::TYPE_PER_DAY,     self::TYPE_ALL),
            'case 49' => array(QueryBuilderCallStatus::TYPE_PER_WEEK_DAY,self::TYPE_ALL),
            'case 50' => array(QueryBuilderCallStatus::TYPE_PER_HOUR,    self::TYPE_ALL),
        );
    }
}
