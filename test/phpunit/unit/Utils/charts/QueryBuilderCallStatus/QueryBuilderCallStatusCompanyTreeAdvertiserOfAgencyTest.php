<?php

require_once dirname(__FILE__) . '/QueryBuilderCallStatusBaseCompanyTree.php';

/**
 * Description of QueryBuilderCallStatusCompanyTreeAdvertiserOfAgencyTest
 * 
 * @author fairdev
 */
class QueryBuilderCallStatusCompanyTreeAdvertiserOfAgencyTest extends QueryBuilderCallStatusBaseCompanyTree {

    protected function configureFilterValuesAndResults() {

        $this->setFilterValues(         self::$FILTER_VALUES);
        $this->setResultsPerMonthDay(   self::$RESULTS_PER_MONTH_DAY);
        $this->setResultsPerWeekDay(    self::$RESULTS_PER_WEEK_DAY);
        $this->setResultsPerHour(       self::$RESULTS_PER_HOUR);
    }

    protected function configureUserWithTz() {
        $this->configureCompanyTreeUserWithTz(
            null,
            self::ID_ADVERTISER_AGENCY_REALSEARCH_IPS,
            self::TIMEZONE_ID_MINUS_5
        );
    }


    protected static $FILTER_VALUES = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            'from'  => '1317524401', //2011-10-01 22:00:01 -0500
            'to'    => '1317528000', //2011-10-01 23:00:00 -0500
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            'from'  => '1319843401', //2011-10-28 18:10:01 -0500
            'to'    => '1319929800', //2011-10-29 18:10:00 -0500
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            'from'  => '1317877214', //2011-10-06 00:00:14 -0500
            'to'    => '1318482013', //2011-10-13 00:00:13 -0500
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            'from'  => '1313910069', //2011-08-21 02:01:09 -0500
            'to'    => '1316588468', //2011-09-21 02:01:08 -0500
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            'from'  => '1285779603', //2010-09-29 12:00:03 -0500
            'to'    => '1317315602', //2011-09-29 12:00:02 -0500
        ),

        self::TYPE_ALL => array(
        ),

        self::TYPE_NOT_FOUND_ONE_HOUR => array(
            'from'  => '1315875429', //2011-09-12 19:57:09 -0500
            'to'    => '1315879028', //2011-09-12 20:57:08 -0500
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
            'from'  => '1317171603', //2011-09-27 20:00:03 -0500
            'to'    => '1317258002', //2011-09-28 20:00:02 -0500
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
            'from'  => '1318403101', //2011-10-12 02:05:01 -0500
            'to'    => '1319007900', //2011-10-19 02:05:00 -0500
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
            'from'  => '1311224351', //2011-07-20 23:59:11 -0500
            'to'    => '1313902750', //2011-08-20 23:59:10 -0500
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
            'from'  => '1282366801', //2010-08-21 00:00:01 -0500
            'to'    => '1313902800', //2011-08-21 00:00:00 -0500
        ),
    );


    protected static $RESULTS_PER_MONTH_DAY = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            array(
                'id'            => '28',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
            array(
                'id'            => '29',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '6',
                'period_name'   => '6',
            ),
            array(
                'id'            => '26',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '11',
                'period_name'   => '11',
            ),
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '11',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
        ),
        self::TYPE_ALL => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '1',
                'period_name'   => '1',
            ),
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '6',
                'period_name'   => '6',
            ),
            array(
                'id'            => '26',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '11',
                'period_name'   => '11',
            ),
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
            array(
                'id'            => '29',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
            array(
                'id'            => array('11', '28'),
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '2',
                'period_num'    => '29',
                'period_name'   => '29',
            ),
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '30',
                'period_name'   => '30',
            ),
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
        ),
    );

    protected static $RESULTS_PER_WEEK_DAY = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            array(
                'id'            => '28',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
            array(
                'id'            => '29',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            array(
                'id'            => '26',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_TUESDAY,
                'period_name'   => self::WEEK_DAY_TUESDAY,
            ),
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
            array(
                'id'            => '11',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
        ),
        self::TYPE_ALL => array(
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_MONDAY,
                'period_name'   => self::WEEK_DAY_MONDAY,
            ),
            array(
                'id'            => '26',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_TUESDAY,
                'period_name'   => self::WEEK_DAY_TUESDAY,
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
            array(
                'id'            => array('11', '22'),
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_THURSDAY,
                'period_name'   => self::WEEK_DAY_THURSDAY,
            ),
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_FRIDAY,
                'period_name'   => self::WEEK_DAY_FRIDAY,
            ),
            array(
                'id'            => array('16', '29'),
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '2',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
            array(
                'id'            => '28',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => self::WEEK_DAY_NUM_SATURDAY,
                'period_name'   => self::WEEK_DAY_SATURDAY,
            ),
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
        ),
    );

    protected static $RESULTS_PER_HOUR = array(
        self::TYPE_FOUND_ONE_HOUR => array(
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
        ),
        self::TYPE_FOUND_ONE_DAY => array(
            array(
                'id'            => '28',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '29',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '18',
                'period_name'   => '18',
            ),
        ),
        self::TYPE_FOUND_ONE_WEEK => array(
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '18',
                'period_name'   => '18',
            ),
            array(
                'id'            => '26',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
        ),
        self::TYPE_FOUND_ONE_MONTH => array(
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
        ),
        self::TYPE_FOUND_ONE_YEAR => array(
            array(
                'id'            => '11',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
        ),
        self::TYPE_ALL => array(
            array(
                'id'            => '13',
                'call_status'   => TwilioIncomingCallTable::STATUS_IN_PROGRESS,
                'calls_amount'  => '1',
                'period_num'    => '2',
                'period_name'   => '2',
            ),
            array(
                'id'            => array('11', '28'),
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '2',
                'period_num'    => '12',
                'period_name'   => '12',
            ),
            array(
                'id'            => '22',
                'call_status'   => TwilioIncomingCallTable::STATUS_FAILED,
                'calls_amount'  => '1',
                'period_num'    => '18',
                'period_name'   => '18',
            ),
            array(
                'id'            => '29',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '18',
                'period_name'   => '18',
            ),
            array(
                'id'            => '5',
                'call_status'   => TwilioIncomingCallTable::STATUS_NO_ANSWER,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '9',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '19',
                'period_name'   => '19',
            ),
            array(
                'id'            => '26',
                'call_status'   => TwilioIncomingCallTable::STATUS_COMPLETED,
                'calls_amount'  => '1',
                'period_num'    => '21',
                'period_name'   => '21',
            ),
            array(
                'id'            => '16',
                'call_status'   => TwilioIncomingCallTable::STATUS_NOT_YET_DIALED,
                'calls_amount'  => '1',
                'period_num'    => '22',
                'period_name'   => '22',
            ),
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_DAY => array(
        ),
        self::TYPE_NOT_FOUND_ONE_WEEK => array(
        ),
        self::TYPE_NOT_FOUND_ONE_HOUR => array(
        ),
        self::TYPE_NOT_FOUND_ONE_MONTH => array(
        ),
        self::TYPE_NOT_FOUND_ONE_YEAR => array(
        ),
    );
}

