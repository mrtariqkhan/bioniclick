<?php

require_once dirname(__FILE__) . '/../../../UserTimezoneTestBase.php';

/**
 * Description of QueryBuilderCallStatusBase
 * 
 * @author fairdev
 */
abstract class QueryBuilderCallStatusBase extends UserTimezoneTestBase {
    //TODO:: add fixtures for TwilioRedialedCall table. Because we need to test statuses
    // IF(trc.id IS NOT NULL, trc.call_status, $alias.call_status) AS call_status

    const WEEK_DAY_SUNDAY       = 'Sunday';
    const WEEK_DAY_MONDAY       = 'Monday';
    const WEEK_DAY_TUESDAY      = 'Tuesday';
    const WEEK_DAY_WEDNESDAY    = 'Wednesday';
    const WEEK_DAY_THURSDAY     = 'Thursday';
    const WEEK_DAY_FRIDAY       = 'Friday';
    const WEEK_DAY_SATURDAY     = 'Saturday';

    const WEEK_DAY_NUM_SUNDAY       = 1;
    const WEEK_DAY_NUM_MONDAY       = 2;
    const WEEK_DAY_NUM_TUESDAY      = 3;
    const WEEK_DAY_NUM_WEDNESDAY    = 4;
    const WEEK_DAY_NUM_THURSDAY     = 5;
    const WEEK_DAY_NUM_FRIDAY       = 6;
    const WEEK_DAY_NUM_SATURDAY     = 7;

    const OUTPUT_ARRAY_GLUE             = ', ';
    const OUTPUT_ARRAY_GLUE_KEY_VALUE   = ': ';


    protected $chartKind    = '';


    protected function _start() {

        parent::_start();

        $this->chartKind    = 'call statuses';
    }

    public function assertArrayAreSame($needArray, array $resultArray, $message) {

        $needArray = empty($needArray) ? array() : $needArray;

//        var_dump($resultArray);
        foreach ($needArray as $needValueArray) {

            $ids = $needValueArray['id'];
            if (!is_array($ids)) {
                $ids = array($ids);
            }
            $needValueArray['id'] = $ids;

            $needle = $needValueArray;
            $contains = false;
            foreach ($ids as $id) {
                $needle['id'] = $id;
                $contains = !(array_search($needle, $resultArray) === false);
                if ($contains) {
                    break;
                }
            }
            if (!$contains) {
                $needValueArrayStr = $this->getNeedValueArrayString($needValueArray);
                $this->fail("$message Actual array has no: $needValueArrayStr.");
            }
        }

        $this->assertEquals(
            count($needArray), 
            count($resultArray), 
            $message
        );
    }

    protected function getNeedValueArrayString($needValueArray) {

        $needValueArray['id'] = implode(' or ', $needValueArray['id']);

        $arrayOfStrings = array_map(
            create_function(
                '$key, $value',
                'return $key . "' . self::OUTPUT_ARRAY_GLUE_KEY_VALUE . '". $value;'
            ),
            array_keys($needValueArray), 
            array_values($needValueArray)
        );

        return '(' . implode(self::OUTPUT_ARRAY_GLUE, $arrayOfStrings) . ')';
    }

    /**
     *
     * @param array $filterValues
     * @param string $type
     * @return array
     */
    protected function getResultArray($filterValues, $type) {

        $user = $this->getUser();

        $advertiser = null;
        $company    = null;
        if ($user->isAdvertiserUser()) {
            $advertiser = $user->getAdvertiser();
        } else {
            $company = $user->getFirstCompany();
        }
        $queryBuilder = new QueryBuilderCallStatus(
            $advertiser,
            $company,
            $this->getGmtDifference(),
            $filterValues
        );
//        $query = $queryBuilder->getQuery($type);
//
//        return $query->fetchArray();

        $sqlQueryString = $queryBuilder->getSqlQueryString($type);
        unset($queryBuilder);

        $queryResult = array();
        if (!empty($sqlQueryString)) {
            $conn = Doctrine_Manager::getInstance()->getConnection('doctrine');
            $queryResult = $conn->fetchAssoc($sqlQueryString, array());
        }

        return $queryResult;
    }
}
