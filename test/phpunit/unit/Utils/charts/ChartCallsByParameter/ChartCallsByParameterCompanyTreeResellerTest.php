<?php

require_once dirname(__FILE__) . '/ChartCallsByParameterBaseCompanyTree.php';

/**
 * Description of ChartCallsByParameterCompanyTreeResellerTest
 * 
 * @author fairdev
 */
class ChartCallsByParameterCompanyTreeResellerTest extends ChartCallsByParameterBaseCompanyTree {


    protected function configureFilterValuesAndResults() {

        $this->setFilterValues(self::$FILTER_VALUES);

        $this->setExpectedResultsByStatus(          self::$RESULTS_BY_STATUS);
        $this->setExpectedResultsByReferrerSource(  self::$RESULTS_BY_REFERRER_SOURCE);
        $this->setExpectedResultsBySearchType(      self::$RESULTS_BY_SEARCH_TYPE);
        $this->setExpectedResultsByUrl(             self::$RESULTS_BY_URL);
        $this->setExpectedResultsByKeyword(         self::$RESULTS_BY_KEYWORD);
    }

    protected function configureUserWithTz() {
        $this->configureCompanyTreeUserWithTz(
            self::ID_COMPANY_RESELLER_ACQUISIO,
            null,
            self::TIMEZONE_ID_GMT
        );
    }

    protected static $FILTER_VALUES = array(
        self::TYPE_RANGE => array(
            'from'  => '1323000020',
            'to'    => '1323000070',
        ),
        self::TYPE_ALL => array(
        ),
    );


    // NOTE:: Does not depend on type of calls
    protected static $RESULTS_BY_STATUS = array(
        self::TYPE_RANGE => array(
            // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.] 
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 6,     // - [.,1,.,1,.,1,.,1,1,.,.] - [.,.,.,.,1,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. busy',
                        self::PARAMETER_COUNT => 4,     // - [.,.,1,.,.,.,.,.,.,1,.] - [.,.,1,1,.,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. failed',
                        self::PARAMETER_COUNT => 3,     // - [1,.,.,.,.,.,.,.,.,.,.] - [1,.,.,.,.,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. not yet dialed',
                        self::PARAMETER_COUNT => 3,     // - [.,.,.,.,.,.,1,.,.,.,.] - [.,1,.,.,.,.,1] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '4. no-answer',
                        self::PARAMETER_COUNT => 2,     // - [.,.,.,.,1,.,.,.,.,.,1] - [.,.,.,.,.,.,.] - 
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 6,     // - [.,1,.,1,.,1,.,1,1,.,.] - [.,.,.,.,1,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. busy',
                        self::PARAMETER_COUNT => 4,     // - [.,.,1,.,.,.,.,.,.,1,.] - [.,.,1,1,.,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. failed',
                        self::PARAMETER_COUNT => 3,     // - [1,.,.,.,.,.,.,.,.,.,.] - [1,.,.,.,.,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => self::VALUE_STATUS_OTHERS,
                        self::PARAMETER_COUNT => 5,     // - [.,.,.,.,1,.,1,.,.,.,1] - [.,1,.,.,.,.,1] - 
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 6,     // - [.,1,.,1,.,1,.,1,1,.,.] - [.,.,.,.,1,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. busy',
                        self::PARAMETER_COUNT => 4,     // - [.,.,1,.,.,.,.,.,.,1,.] - [.,.,1,1,.,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. failed',
                        self::PARAMETER_COUNT => 3,     // - [1,.,.,.,.,.,.,.,.,.,.] - [1,.,.,.,.,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. not yet dialed',
                        self::PARAMETER_COUNT => 3,     // - [.,.,.,.,.,.,1,.,.,.,.] - [.,1,.,.,.,.,1] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '4. no-answer',
                        self::PARAMETER_COUNT => 2,     // - [.,.,.,.,1,.,.,.,.,.,1] - [.,.,.,.,.,.,.] - 
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 6,     // - [.,1,.,1,.,1,.,1,1,.,.] - [.,.,.,.,1,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. busy',
                        self::PARAMETER_COUNT => 4,     // - [.,.,1,.,.,.,.,.,.,1,.] - [.,.,1,1,.,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. failed',
                        self::PARAMETER_COUNT => 3,     // - [1,.,.,.,.,.,.,.,.,.,.] - [1,.,.,.,.,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => self::VALUE_STATUS_OTHERS,
                        self::PARAMETER_COUNT => 5,     // - [.,.,.,.,1,.,1,.,.,.,1] - [.,1,.,.,.,.,1] - 
                    ),
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.,x,x,x,x,x] - [.,.]
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 9,     // - [.,1,.,1,.,1,.,1,1,.,.] - [.,.,.,.,1,.,.,1,1,.,1,.] - [.,.] 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. busy',
                        self::PARAMETER_COUNT => 6,     // - [.,.,1,.,.,.,.,.,.,1,.] - [.,.,1,1,.,.,.,.,.,.,.,1] - [1,.] 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. failed',
                        self::PARAMETER_COUNT => 4,     // - [1,.,.,.,.,.,.,.,.,.,.] - [1,.,.,.,.,1,.,.,.,.,.,.] - [1,.]  
                    ),
                    array(
                        self::PARAMETER_TICK  => '4. no-answer',
                        self::PARAMETER_COUNT => 3,     // - [.,.,.,.,1,.,.,.,.,.,1] - [.,.,.,.,.,.,.,.,.,1,.,.] - [.,.]  
                    ),
                    array(
                        self::PARAMETER_TICK  => '4. not yet dialed',
                        self::PARAMETER_COUNT => 3,     // - [.,.,.,.,.,.,1,.,.,.,.] - [.,1,.,.,.,.,1,.,.,.,.,.] - [.,.] 
                    ),
                    array(
                        self::PARAMETER_TICK  => '5. in-progress',
                        self::PARAMETER_COUNT => 2,     // - [.,.,.,.,.,.,.,.,.,.,.] - [.,.,.,.,.,.,.,.,.,.,.,.] - [.,2]  
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 9,     // - [.,1,.,1,.,1,.,1,1,.,.] - [.,.,.,.,1,.,.,1,1,.,1,.] - [.,.] 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. busy',
                        self::PARAMETER_COUNT => 6,     // - [.,.,1,.,.,.,.,.,.,1,.] - [.,.,1,1,.,.,.,.,.,.,.,1] - [1,.] 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. failed',
                        self::PARAMETER_COUNT => 4,     // - [1,.,.,.,.,.,.,.,.,.,.] - [1,.,.,.,.,1,.,.,.,.,.,.] - [1,.]  
                    ),
                    array(
                        self::PARAMETER_TICK  => self::VALUE_STATUS_OTHERS,
                        self::PARAMETER_COUNT => 8,     // - [.,.,.,.,1,.,1,.,.,.,1] - [.,1,.,.,.,.,1,.,.,1,.,.] - [.,2]   
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 9,     // - [.,1,.,1,.,1,.,1,1,.,.] - [.,.,.,.,1,.,.,1,1,.,1,.] - [.,.] 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. busy',
                        self::PARAMETER_COUNT => 6,     // - [.,.,1,.,.,.,.,.,.,1,.] - [.,.,1,1,.,.,.,.,.,.,.,1] - [1,.] 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. failed',
                        self::PARAMETER_COUNT => 4,     // - [1,.,.,.,.,.,.,.,.,.,.] - [1,.,.,.,.,1,.,.,.,.,.,.] - [1,.]  
                    ),
                    array(
                        self::PARAMETER_TICK  => '4. no-answer',
                        self::PARAMETER_COUNT => 3,     // - [.,.,.,.,1,.,.,.,.,.,1] - [.,.,.,.,.,.,.,.,.,1,.,.] - [.,.]  
                    ),
                    array(
                        self::PARAMETER_TICK  => '4. not yet dialed',
                        self::PARAMETER_COUNT => 3,     // - [.,.,.,.,.,.,1,.,.,.,.] - [.,1,.,.,.,.,1,.,.,.,.,.] - [.,.] 
                    ),
                    array(
                        self::PARAMETER_TICK  => '5. in-progress',
                        self::PARAMETER_COUNT => 2,     // - [.,.,.,.,.,.,.,.,.,.,.] - [.,.,.,.,.,.,.,.,.,.,.,.] - [.,2]  
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 9,     // - [.,1,.,1,.,1,.,1,1,.,.] - [.,.,.,.,1,.,.,1,1,.,1,.] - [.,.] 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. busy',
                        self::PARAMETER_COUNT => 6,     // - [.,.,1,.,.,.,.,.,.,1,.] - [.,.,1,1,.,.,.,.,.,.,.,1] - [1,.] 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. failed',
                        self::PARAMETER_COUNT => 4,     // - [1,.,.,.,.,.,.,.,.,.,.] - [1,.,.,.,.,1,.,.,.,.,.,.] - [1,.]  
                    ),
                    array(
                        self::PARAMETER_TICK  => self::VALUE_STATUS_OTHERS,
                        self::PARAMETER_COUNT => 8,     // - [.,.,.,.,1,.,1,.,.,.,1] - [.,1,.,.,.,.,1,.,.,1,.,.] - [.,2] 
                    ),
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_REFERRER_SOURCE = array(
        self::TYPE_RANGE => array(
            // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.] 
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.google.com',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.] 
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.google.com',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.] 
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.google.com',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.] 
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.google.com',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.] 
                    ),
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.,x,x,x,x,x] - [.,.]
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.bing.com',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.,x,x,x,x,x] - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '1. www.google.com',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.,x,x,x,x,x] - [.,.]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.bing.com',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.,x,x,x,x,x] - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '1. www.google.com',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.,x,x,x,x,x] - [.,.]
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.bing.com',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.,x,x,x,x,x] - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '1. www.google.com',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.,x,x,x,x,x] - [.,.]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.bing.com',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.,x,x,x,x,x] - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '1. www.google.com',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.,x,x,x,x,x] - [.,.]
                    ),
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_SEARCH_TYPE = array(
        self::TYPE_RANGE => array(
            // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.] 
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. Organic',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. Organic',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.]
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. Organic',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. Organic',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.]
                    ),
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.,x,x,x,x,x] - [.,.]
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. Organic',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.,x,x,x,x,x] - [.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '1. PPC',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.,x,x,x,x,x] - [.,1]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. Organic',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.,x,x,x,x,x] - [.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '1. PPC',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.,x,x,x,x,x] - [.,1]
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. Organic',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.,x,x,x,x,x] - [.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '1. PPC',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.,x,x,x,x,x] - [.,1]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. Organic',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.,x,x,x,x,x] - [.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '1. PPC',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.,x,x,x,x,x] - [.,1]
                    ),
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_URL = array(
        self::TYPE_RANGE => array(
            // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.] 
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.second-site.html',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.] 
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.second-site.html',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.] 
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.second-site.html',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.] 
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.second-site.html',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.] 
                    ),
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.,x,x,x,x,x] - [.,.]
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. https://third-site.php',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.,x,x,x,x,x] - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '1. www.second-site.html',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.,x,x,x,x,x] - [.,.]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. https://third-site.php',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.,x,x,x,x,x] - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '1. www.second-site.html',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.,x,x,x,x,x] - [.,.]
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. https://third-site.php',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.,x,x,x,x,x] - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '1. www.second-site.html',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.,x,x,x,x,x] - [.,.]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. https://third-site.php',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.,x,x,x,x,x] - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '1. www.second-site.html',
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.,x,x,x,x,x] - [.,.]
                    ),
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_KEYWORD = array(
        self::TYPE_RANGE => array(
            // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.] 
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 1'",
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 1'",
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.]
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 1'",
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 1'",
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.]
                    ),
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.,x,x,x,x,x] - [.,.]
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 1'",
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.,x,x,x,x,x] - [.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 3'",
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.,x,x,x,x,x] - [.,1]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 1'",
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.,x,x,x,x,x] - [.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 3'",
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.,x,x,x,x,x] - [.,1]
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 1'",
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.,x,x,x,x,x] - [.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 3'",
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.,x,x,x,x,x] - [.,1]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 1'",
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,1,.,x,x,x,x,x] - [.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 3'",
                        self::PARAMETER_COUNT => 1,     // - [.,y,y,.,.,x,x,x,x,x,x] - [x,x,.,y,y,.,.,x,x,x,x,x] - [.,1]
                    ),
                ),
            ),
        ),
    );
}
