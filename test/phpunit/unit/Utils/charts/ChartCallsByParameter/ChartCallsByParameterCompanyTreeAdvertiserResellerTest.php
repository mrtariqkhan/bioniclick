<?php

require_once dirname(__FILE__) . '/ChartCallsByParameterBaseCompanyTree.php';

/**
 * Description of ChartCallsByParameterCompanyTreeAdvertiserResellerTest
 * 
 * @author fairdev
 */
class ChartCallsByParameterCompanyTreeAdvertiserResellerTest extends ChartCallsByParameterBaseCompanyTree {


    protected function configureFilterValuesAndResults() {

        $this->setFilterValues(self::$FILTER_VALUES);

        $this->setExpectedResultsByStatus(          self::$RESULTS_BY_STATUS);
        $this->setExpectedResultsByReferrerSource(  self::$RESULTS_BY_REFERRER_SOURCE);
        $this->setExpectedResultsBySearchType(      self::$RESULTS_BY_SEARCH_TYPE);
        $this->setExpectedResultsByUrl(             self::$RESULTS_BY_URL);
        $this->setExpectedResultsByKeyword(         self::$RESULTS_BY_KEYWORD);
    }

    protected function configureUserWithTz() {
        $this->configureCompanyTreeUserWithTz(
            null,
            self::ID_ADVERTISER_RESELLER_ACQUISIO_TRIPS,
            self::TIMEZONE_ID_GMT
        );
    }

    protected static $FILTER_VALUES = array(
        self::TYPE_RANGE => array(
            'from'  => '1323000042',
            'to'    => '1323000058',
        ),
        self::TYPE_ALL => array(
        ),
    );


    // NOTE:: Does not depend on type of calls
    protected static $RESULTS_BY_STATUS = array(
        self::TYPE_RANGE => array(
            // - [.,y,y,.,.,x,x,x,x,x,x] - 
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 5,     // - [.,1,.,1,.,1,.,1,1,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. busy',
                        self::PARAMETER_COUNT => 2,     // - [.,.,1,.,.,.,.,.,.,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. no-answer',
                        self::PARAMETER_COUNT => 2,     // - [.,.,.,.,1,.,.,.,.,.,1] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. failed',
                        self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.,.,.,.,.,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. not yet dialed',
                        self::PARAMETER_COUNT => 1,     // - [.,.,.,.,.,.,1,.,.,.,.] - 
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 5,     // - [.,1,.,1,.,1,.,1,1,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. busy',
                        self::PARAMETER_COUNT => 2,     // - [.,.,1,.,.,.,.,.,.,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. no-answer',
                        self::PARAMETER_COUNT => 2,     // - [.,.,.,.,1,.,.,.,.,.,1] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => self::VALUE_STATUS_OTHERS,
                        self::PARAMETER_COUNT => 2,     // - [1,.,.,.,.,.,1,.,.,.,.] - 
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 5,     // - [.,1,.,1,.,1,.,1,1,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. busy',
                        self::PARAMETER_COUNT => 2,     // - [.,.,1,.,.,.,.,.,.,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. no-answer',
                        self::PARAMETER_COUNT => 2,     // - [.,.,.,.,1,.,.,.,.,.,1] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. failed',
                        self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.,.,.,.,.,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. not yet dialed',
                        self::PARAMETER_COUNT => 1,     // - [.,.,.,.,.,.,1,.,.,.,.] - 
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 5,     // - [.,1,.,1,.,1,.,1,1,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. busy',
                        self::PARAMETER_COUNT => 2,     // - [.,.,1,.,.,.,.,.,.,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. no-answer',
                        self::PARAMETER_COUNT => 2,     // - [.,.,.,.,1,.,.,.,.,.,1] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => self::VALUE_STATUS_OTHERS,
                        self::PARAMETER_COUNT => 2,     // - [1,.,.,.,.,.,1,.,.,.,.] - 
                    ),
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [.,y,y,.,.,x,x,x,x,x,x] - [.,.]
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 5,     // - [.,1,.,1,.,1,.,1,1,.,.] - [.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. busy',
                        self::PARAMETER_COUNT => 3,     // - [.,.,1,.,.,.,.,.,.,1,.] - [1,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. no-answer',
                        self::PARAMETER_COUNT => 2,     // - [.,.,.,.,1,.,.,.,.,.,1] - [.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '4. failed',
                        self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.,.,.,.,.,.,.] - [.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '4. in-progress',
                        self::PARAMETER_COUNT => 1,     // - [.,.,.,.,.,.,.,.,.,.,.] - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '4. not yet dialed',
                        self::PARAMETER_COUNT => 1,     // - [.,.,.,.,.,.,1,.,.,.,.] - [.,.]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 5,     // - [.,1,.,1,.,1,.,1,1,.,.] - [.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. busy',
                        self::PARAMETER_COUNT => 3,     // - [.,.,1,.,.,.,.,.,.,1,.] - [1,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. no-answer',
                        self::PARAMETER_COUNT => 2,     // - [.,.,.,.,1,.,.,.,.,.,1] - [.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => self::VALUE_STATUS_OTHERS,
                        self::PARAMETER_COUNT => 3,     // - [1,.,.,.,.,.,1,.,.,.,.] - [.,1]
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 5,     // - [.,1,.,1,.,1,.,1,1,.,.] - [.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. busy',
                        self::PARAMETER_COUNT => 3,     // - [.,.,1,.,.,.,.,.,.,1,.] - [1,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. no-answer',
                        self::PARAMETER_COUNT => 2,     // - [.,.,.,.,1,.,.,.,.,.,1] - [.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '4. failed',
                        self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.,.,.,.,.,.,.] - [.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '4. in-progress',
                        self::PARAMETER_COUNT => 1,     // - [.,.,.,.,.,.,.,.,.,.,.] - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '4. not yet dialed',
                        self::PARAMETER_COUNT => 1,     // - [.,.,.,.,.,.,1,.,.,.,.] - [.,.]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 5,     // - [.,1,.,1,.,1,.,1,1,.,.] - [.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. busy',
                        self::PARAMETER_COUNT => 3,     // - [.,.,1,.,.,.,.,.,.,1,.] - [1,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. no-answer',
                        self::PARAMETER_COUNT => 2,     // - [.,.,.,.,1,.,.,.,.,.,1] - [.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => self::VALUE_STATUS_OTHERS,
                        self::PARAMETER_COUNT => 3,     // - [1,.,.,.,.,.,1,.,.,.,.] - [.,1]
                    ),
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_REFERRER_SOURCE = array(
        self::TYPE_RANGE => array(
            // - [.,y,y,.,.,x,x,x,x,x,x] - 
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [.,y,y,.,.,x,x,x,x,x,x] - [.,.]
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_SEARCH_TYPE = array(
        self::TYPE_RANGE => array(
            // - [.,y,y,.,.,x,x,x,x,x,x] - 
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [.,y,y,.,.,x,x,x,x,x,x] - [.,.]
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_URL = array(
        self::TYPE_RANGE => array(
            // - [.,y,y,.,.,x,x,x,x,x,x] - 
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [.,y,y,.,.,x,x,x,x,x,x] - [.,.]
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_KEYWORD = array(
        self::TYPE_RANGE => array(
            // - [.,y,y,.,.,x,x,x,x,x,x] - 
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [.,y,y,.,.,x,x,x,x,x,x] - [.,.]
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
        ),
    );
}
