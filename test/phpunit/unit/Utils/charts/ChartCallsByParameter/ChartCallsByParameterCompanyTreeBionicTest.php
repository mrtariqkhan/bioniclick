<?php

require_once dirname(__FILE__) . '/ChartCallsByParameterBaseCompanyTree.php';

/**
 * Description of ChartCallsByParameterCompanyTreeBionicTest
 * 
 * @author fairdev
 */
class ChartCallsByParameterCompanyTreeBionicTest extends ChartCallsByParameterBaseCompanyTree {


    protected function configureFilterValuesAndResults() {

        $this->setFilterValues(self::$FILTER_VALUES);

        $this->setExpectedResultsByStatus(          self::$RESULTS_BY_STATUS);
        $this->setExpectedResultsByReferrerSource(  self::$RESULTS_BY_REFERRER_SOURCE);
        $this->setExpectedResultsBySearchType(      self::$RESULTS_BY_SEARCH_TYPE);
        $this->setExpectedResultsByUrl(             self::$RESULTS_BY_URL);
        $this->setExpectedResultsByKeyword(         self::$RESULTS_BY_KEYWORD);
    }

    protected function configureUserWithTz() {
        $this->configureCompanyTreeUserWithTz(
            self::ID_COMPANY_BIONIC,
            null,
            self::TIMEZONE_ID_GMT
        );
    }

    protected static $FILTER_VALUES = array(
        self::TYPE_RANGE => array(
            'from'  => '1323000031',
            'to'    => '1323000050',
        ),
        self::TYPE_ALL => array(
        ),
    );


    // NOTE:: Does not depend on type of calls
    protected static $RESULTS_BY_STATUS = array(
        self::TYPE_RANGE => array(
            // - [y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x]
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',                  
                        self::PARAMETER_COUNT => 13,    // [.,1,1] - 2,2 - [.] - 2 - [.,1,.,1,1,2,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. failed',
                        self::PARAMETER_COUNT => 3,     // [.,1,.] - .,. - [1] - . - [1,.,.,.,.,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. no-answer',
                        self::PARAMETER_COUNT => 3,     // [1,.,.] - .,. - [.] - . - [1,.,.,.,1,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. busy',
                        self::PARAMETER_COUNT => 2,     // [.,.,1] - .,. - [.] - . - [.,.,1,.,.,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. not yet dialed',
                        self::PARAMETER_COUNT => 2,     // [.,.,.] - .,. - [.] - . - [.,.,1,.,.,.,1]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',                  
                        self::PARAMETER_COUNT => 13,    // [.,1,1] - 2,2 - [.] - 2 - [.,1,.,1,1,2,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. failed',
                        self::PARAMETER_COUNT => 3,     // [.,1,.] - .,. - [1] - . - [1,.,.,.,.,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. no-answer',
                        self::PARAMETER_COUNT => 3,     // [1,.,.] - .,. - [.] - . - [1,.,.,.,1,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => self::VALUE_STATUS_OTHERS,
                        self::PARAMETER_COUNT => 4,     // [.,.,1] - .,. - [.] - . - [.,.,2,.,.,.,1]
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',                  
                        self::PARAMETER_COUNT => 13,    // [.,1,1] - 2,2 - [.] - 2 - [.,1,.,1,1,2,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. failed',
                        self::PARAMETER_COUNT => 3,     // [.,1,.] - .,. - [1] - . - [1,.,.,.,.,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. no-answer',
                        self::PARAMETER_COUNT => 3,     // [1,.,.] - .,. - [.] - . - [1,.,.,.,1,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. busy',
                        self::PARAMETER_COUNT => 2,     // [.,.,1] - .,. - [.] - . - [.,.,1,.,.,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. not yet dialed',
                        self::PARAMETER_COUNT => 2,     // [.,.,.] - .,. - [.] - . - [.,.,1,.,.,.,1]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',                  
                        self::PARAMETER_COUNT => 13,    // [.,1,1] - 2,2 - [.] - 2 - [.,1,.,1,1,2,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. failed',
                        self::PARAMETER_COUNT => 3,     // [.,1,.] - .,. - [1] - . - [1,.,.,.,.,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. no-answer',
                        self::PARAMETER_COUNT => 3,     // [1,.,.] - .,. - [.] - . - [1,.,.,.,1,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => self::VALUE_STATUS_OTHERS,
                        self::PARAMETER_COUNT => 4,     // [.,.,1] - .,. - [.] - . - [.,.,2,.,.,.,1]
                    ),
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - x - [.,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 99,    // 2 - [4,2,1,1,4] - 2,4,1,2 - [.,.,1,.,1] - 5,4 - [.] - 1 - [1,.,1,1] - 2,2 - [.] - 2 - [.,1,.,1,1,2,.,1,2,.,.] - .,2,1,1 - [1,.,.,.,2,.,.,1,1,1,1,.] - 1,2 - [1] - . - [1,.,1,.,1,.,2,.,.] - 1,.,3 - [4,1,1,2,3] - 3,2,5,1 - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. busy',
                        self::PARAMETER_COUNT => 34,    // 1 - [.,.,1,1,.] - 2,.,1,. - [1,.,1,.,1] - .,1 - [1] - . - [.,.,.,1] - .,. - [.] - . - [.,.,1,.,.,.,.,.,.,1,1] - .,.,.,. - [.,1,1,1,.,.,.,.,.,.,.,2] - .,. - [.] - 1 - [.,.,1,1,.,.,.,1,.] - .,.,1 - [1,.,1,.,1] - 1,2,.,2 - [1,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. not yet dialed',
                        self::PARAMETER_COUNT => 33,    // 1 - [1,.,3,.,1] - 1,1,.,. - [.,.,.,1,.] - .,1 - [.] - . - [1,.,.,.] - .,. - [.] - . - [.,.,1,.,.,.,1,.,.,.,.] - 1,.,.,. - [.,1,1,.,.,.,1,1,.,.,.,.] - .,. - [.] - 1 - [.,.,.,1,.,1,.,.,1] - .,1,. - [.,3,4,.,.] - .,1,.,1 - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '4. no-answer',
                        self::PARAMETER_COUNT => 28,    // 1 - [1,.,.,.,.] - 1,1,.,. - [1,.,.,1,.] - .,. - [.] - 1 - [.,1,.,.] - .,. - [.] - . - [1,.,.,.,1,.,.,.,.,.,1] - .,.,.,1 - [.,.,.,1,.,.,.,.,..1,.,.] - 1,. - [.] - . - [.,1,.,.,.,1,.,.,1] - .,.,1 - [.,2,.,.,1] - 2,1,1,1 - [1,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '5. failed',
                        self::PARAMETER_COUNT => 22,    // 1 - [.,.,1,.,1] - .,.,.,. - [.,1,.,.,.] - 1,. - [.] - . - [.,.,1,.] - .,. - [1] - . - [1,.,.,.,.,.,.,.,.,.,.] - 1,.,.,. - [1,.,.,.,.,1,1,.,.,.,.,.] - .,. - [.] - . - [1,.,.,.,1,.,.,.,.] - 1,1,1 - [1,.,.,.,1] - .,.,.,1 - [1,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '6. in-progress',
                        self::PARAMETER_COUNT => 6,     // . - [.,.,.,.,.] - .,.,.,. - [.,.,.,.,.] - .,. - [.] - . - [.,.,.,.] - .,. - [.] - . - [.,.,.,.,.,.,.,.,.,.,.] - .,.,.,. - [.,.,.,.,.,.,.,.,..1,.,.] - .,. - [.] - . - [.,.,.,.,.,.,.,.,.] - .,.,. - [.,.,.,.,.] - .,.,.,. - [3,3]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 99,    // 2 - [4,2,1,1,4] - 2,4,1,2 - [.,.,1,.,1] - 5,4 - [.] - 1 - [1,.,1,1] - 2,2 - [.] - 2 - [.,1,.,1,1,2,.,1,2,.,.] - .,2,1,1 - [1,.,.,.,2,.,.,1,1,1,1,.] - 1,2 - [1] - . - [1,.,1,.,1,.,2,.,.] - 1,.,3 - [4,1,1,2,3] - 3,2,5,1 - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. busy',
                        self::PARAMETER_COUNT => 34,    // 1 - [.,.,1,1,.] - 2,.,1,. - [1,.,1,.,1] - .,1 - [1] - . - [.,.,.,1] - .,. - [.] - . - [.,.,1,.,.,.,.,.,.,1,1] - .,.,.,. - [.,1,1,1,.,.,.,.,.,.,.,2] - .,. - [.] - 1 - [.,.,1,1,.,.,.,1,.] - .,.,1 - [1,.,1,.,1] - 1,2,.,2 - [1,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. not yet dialed',
                        self::PARAMETER_COUNT => 33,    // 1 - [1,.,3,.,1] - 1,1,.,. - [.,.,.,1,.] - .,1 - [.] - . - [1,.,.,.] - .,. - [.] - . - [.,.,1,.,.,.,1,.,.,.,.] - 1,.,.,. - [.,1,1,.,.,.,1,1,.,.,.,.] - .,. - [.] - 1 - [.,.,.,1,.,1,.,.,1] - .,1,. - [.,3,4,.,.] - .,1,.,1 - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => self::VALUE_STATUS_OTHERS,
                        self::PARAMETER_COUNT => 56,    // 2 - [1,.,1,.,1] - 1,1,.,. - [1,1,.,1,.] - 1,. - [.] - 1 - [.,1,1,.] - .,. - [1] - . - [2,.,.,.,1,.,.,.,.,.,1] - 1,.,.,1 - [1,.,.,1,.,1,1,.,..2,.,.] - 1,. - [.] - . - [1,1,.,.,1,1,.,.,1] - 1,1,2 - [1,2,.,.,2] - 2,1,1,2 - [5,4]
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 99,    // 2 - [4,2,1,1,4] - 2,4,1,2 - [.,.,1,.,1] - 5,4 - [.] - 1 - [1,.,1,1] - 2,2 - [.] - 2 - [.,1,.,1,1,2,.,1,2,.,.] - .,2,1,1 - [1,.,.,.,2,.,.,1,1,1,1,.] - 1,2 - [1] - . - [1,.,1,.,1,.,2,.,.] - 1,.,3 - [4,1,1,2,3] - 3,2,5,1 - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. busy',
                        self::PARAMETER_COUNT => 34,    // 1 - [.,.,1,1,.] - 2,.,1,. - [1,.,1,.,1] - .,1 - [1] - . - [.,.,.,1] - .,. - [.] - . - [.,.,1,.,.,.,.,.,.,1,1] - .,.,.,. - [.,1,1,1,.,.,.,.,.,.,.,2] - .,. - [.] - 1 - [.,.,1,1,.,.,.,1,.] - .,.,1 - [1,.,1,.,1] - 1,2,.,2 - [1,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. not yet dialed',
                        self::PARAMETER_COUNT => 33,    // 1 - [1,.,3,.,1] - 1,1,.,. - [.,.,.,1,.] - .,1 - [.] - . - [1,.,.,.] - .,. - [.] - . - [.,.,1,.,.,.,1,.,.,.,.] - 1,.,.,. - [.,1,1,.,.,.,1,1,.,.,.,.] - .,. - [.] - 1 - [.,.,.,1,.,1,.,.,1] - .,1,. - [.,3,4,.,.] - .,1,.,1 - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '4. no-answer',
                        self::PARAMETER_COUNT => 28,    // 1 - [1,.,.,.,.] - 1,1,.,. - [1,.,.,1,.] - .,. - [.] - 1 - [.,1,.,.] - .,. - [.] - . - [1,.,.,.,1,.,.,.,.,.,1] - .,.,.,1 - [.,.,.,1,.,.,.,.,..1,.,.] - 1,. - [.] - . - [.,1,.,.,.,1,.,.,1] - .,.,1 - [.,2,.,.,1] - 2,1,1,1 - [1,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '5. failed',
                        self::PARAMETER_COUNT => 22,    // 1 - [.,.,1,.,1] - .,.,.,. - [.,1,.,.,.] - 1,. - [.] - . - [.,.,1,.] - .,. - [1] - . - [1,.,.,.,.,.,.,.,.,.,.] - 1,.,.,. - [1,.,.,.,.,1,1,.,.,.,.,.] - .,. - [.] - . - [1,.,.,.,1,.,.,.,.] - 1,1,1 - [1,.,.,.,1] - .,.,.,1 - [1,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '6. in-progress',
                        self::PARAMETER_COUNT => 6,     // . - [.,.,.,.,.] - .,.,.,. - [.,.,.,.,.] - .,. - [.] - . - [.,.,.,.] - .,. - [.] - . - [.,.,.,.,.,.,.,.,.,.,.] - .,.,.,. - [.,.,.,.,.,.,.,.,..1,.,.] - .,. - [.] - . - [.,.,.,.,.,.,.,.,.] - .,.,. - [.,.,.,.,.] - .,.,.,. - [3,3]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 99,    // 2 - [4,2,1,1,4] - 2,4,1,2 - [.,.,1,.,1] - 5,4 - [.] - 1 - [1,.,1,1] - 2,2 - [.] - 2 - [.,1,.,1,1,2,.,1,2,.,.] - .,2,1,1 - [1,.,.,.,2,.,.,1,1,1,1,.] - 1,2 - [1] - . - [1,.,1,.,1,.,2,.,.] - 1,.,3 - [4,1,1,2,3] - 3,2,5,1 - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. busy',
                        self::PARAMETER_COUNT => 34,    // 1 - [.,.,1,1,.] - 2,.,1,. - [1,.,1,.,1] - .,1 - [1] - . - [.,.,.,1] - .,. - [.] - . - [.,.,1,.,.,.,.,.,.,1,1] - .,.,.,. - [.,1,1,1,.,.,.,.,.,.,.,2] - .,. - [.] - 1 - [.,.,1,1,.,.,.,1,.] - .,.,1 - [1,.,1,.,1] - 1,2,.,2 - [1,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. not yet dialed',
                        self::PARAMETER_COUNT => 33,    // 1 - [1,.,3,.,1] - 1,1,.,. - [.,.,.,1,.] - .,1 - [.] - . - [1,.,.,.] - .,. - [.] - . - [.,.,1,.,.,.,1,.,.,.,.] - 1,.,.,. - [.,1,1,.,.,.,1,1,.,.,.,.] - .,. - [.] - 1 - [.,.,.,1,.,1,.,.,1] - .,1,. - [.,3,4,.,.] - .,1,.,1 - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => self::VALUE_STATUS_OTHERS,
                        self::PARAMETER_COUNT => 56,    // 2 - [1,.,1,.,1] - 1,1,.,. - [1,1,.,1,.] - 1,. - [.] - 1 - [.,1,1,.] - .,. - [1] - . - [2,.,.,.,1,.,.,.,.,.,1] - 1,.,.,1 - [1,.,.,1,.,1,1,.,..2,.,.] - 1,. - [.] - . - [1,1,.,.,1,1,.,.,1] - 1,1,2 - [1,2,.,.,2] - 2,1,1,2 - [5,4]
                    ),
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_REFERRER_SOURCE = array(
        self::TYPE_RANGE => array(
            // - [y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x]
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - x - [.,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.google.com',
                        self::PARAMETER_COUNT => 5,     // - x - [2,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,1,.] - x,x,x,x - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.bing.com',
                        self::PARAMETER_COUNT => 4,     // - x - [2,1,.,...] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.yahoo.com',
                        self::PARAMETER_COUNT => 4,     // - x - [1,1,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,2]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.google.com',
                        self::PARAMETER_COUNT => 5,     // - x - [2,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,1,.] - x,x,x,x - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.bing.com',
                        self::PARAMETER_COUNT => 4,     // - x - [2,1,.,...] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.yahoo.com',
                        self::PARAMETER_COUNT => 4,     // - x - [1,1,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,2]
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.google.com',
                        self::PARAMETER_COUNT => 5,     // - x - [2,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,1,.] - x,x,x,x - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.bing.com',
                        self::PARAMETER_COUNT => 4,     // - x - [2,1,.,...] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.yahoo.com',
                        self::PARAMETER_COUNT => 4,     // - x - [1,1,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,2]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.google.com',
                        self::PARAMETER_COUNT => 5,     // - x - [2,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,1,.] - x,x,x,x - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.bing.com',
                        self::PARAMETER_COUNT => 4,     // - x - [2,1,.,...] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.yahoo.com',
                        self::PARAMETER_COUNT => 4,     // - x - [1,1,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,2]
                    ),
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_SEARCH_TYPE = array(
        self::TYPE_RANGE => array(
            // - [y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x]
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - x - [.,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. Organic',
                        self::PARAMETER_COUNT => 7,     // - x - [2,.,.,1,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,3]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. PPC',
                        self::PARAMETER_COUNT => 6,     // - x - [3,1,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,1,.] - x,x,x,x - [.,1]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. Organic',
                        self::PARAMETER_COUNT => 7,     // - x - [2,.,.,1,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,3]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. PPC',
                        self::PARAMETER_COUNT => 6,     // - x - [3,1,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,1,.] - x,x,x,x - [.,1]
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. Organic',
                        self::PARAMETER_COUNT => 7,     // - x - [2,.,.,1,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,3]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. PPC',
                        self::PARAMETER_COUNT => 6,     // - x - [3,1,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,1,.] - x,x,x,x - [.,1]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. Organic',
                        self::PARAMETER_COUNT => 7,     // - x - [2,.,.,1,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,3]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. PPC',
                        self::PARAMETER_COUNT => 6,     // - x - [3,1,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,1,.] - x,x,x,x - [.,1]
                    ),
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_URL = array(
        self::TYPE_RANGE => array(
            // - [y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x]
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - x - [.,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. http://first-site.html',
                        self::PARAMETER_COUNT => 5,     // - x - [3,.,.,1,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '1. www.second-site.html',
                        self::PARAMETER_COUNT => 5,     // - x - [1,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,1,.] - x,x,x,x - [.,2]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. https://third-site.php',
                        self::PARAMETER_COUNT => 3,     // - x - [1,1,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,1]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. http://first-site.html',
                        self::PARAMETER_COUNT => 5,     // - x - [3,.,.,1,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '1. www.second-site.html',
                        self::PARAMETER_COUNT => 5,     // - x - [1,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,1,.] - x,x,x,x - [.,2]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. https://third-site.php',
                        self::PARAMETER_COUNT => 3,     // - x - [1,1,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,1]
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. http://first-site.html',
                        self::PARAMETER_COUNT => 5,     // - x - [3,.,.,1,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '1. www.second-site.html',
                        self::PARAMETER_COUNT => 5,     // - x - [1,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,1,.] - x,x,x,x - [.,2]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. https://third-site.php',
                        self::PARAMETER_COUNT => 3,     // - x - [1,1,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,1]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. http://first-site.html',
                        self::PARAMETER_COUNT => 5,     // - x - [3,.,.,1,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '1. www.second-site.html',
                        self::PARAMETER_COUNT => 5,     // - x - [1,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,1,.] - x,x,x,x - [.,2]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. https://third-site.php',
                        self::PARAMETER_COUNT => 3,     // - x - [1,1,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,1]
                    ),
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_KEYWORD = array(
        self::TYPE_RANGE => array(
            // - [y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x]
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                ),
                self::MAX_ROW_COUNT => array(
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - x - [.,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 3'",
                        self::PARAMETER_COUNT => 6,     // - x - [3,.,.,1,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,2]
                    ),
                    array(
                        self::PARAMETER_TICK  => "2. 'keyword 2'",
                        self::PARAMETER_COUNT => 3,     // - x - [1,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,1,.] - x,x,x,x - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => "2. 'keyword 4'",
                        self::PARAMETER_COUNT => 3,     // - x - [1,1,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => "3. 'keyword 1'",
                        self::PARAMETER_COUNT => 1,     // - x - [.,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 3'",
                        self::PARAMETER_COUNT => 6,     // - x - [3,.,.,1,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,2]
                    ),
                    array(
                        self::PARAMETER_TICK  => "2. 'keyword 2'",
                        self::PARAMETER_COUNT => 3,     // - x - [1,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,1,.] - x,x,x,x - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => "2. 'keyword 4'",
                        self::PARAMETER_COUNT => 3,     // - x - [1,1,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => self::VALUE_KEYWORD_OTHERS,
                        self::PARAMETER_COUNT => 1,     // - x - [.,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 3'",
                        self::PARAMETER_COUNT => 6,     // - x - [3,.,.,1,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,2]
                    ),
                    array(
                        self::PARAMETER_TICK  => "2. 'keyword 2'",
                        self::PARAMETER_COUNT => 3,     // - x - [1,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,1,.] - x,x,x,x - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => "2. 'keyword 4'",
                        self::PARAMETER_COUNT => 3,     // - x - [1,1,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => "3. 'keyword 1'",
                        self::PARAMETER_COUNT => 1,     // - x - [.,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 3'",
                        self::PARAMETER_COUNT => 6,     // - x - [3,.,.,1,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,2]
                    ),
                    array(
                        self::PARAMETER_TICK  => "2. 'keyword 2'",
                        self::PARAMETER_COUNT => 3,     // - x - [1,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,1,.] - x,x,x,x - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => "2. 'keyword 4'",
                        self::PARAMETER_COUNT => 3,     // - x - [1,1,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,.,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => self::VALUE_KEYWORD_OTHERS,
                        self::PARAMETER_COUNT => 1,     // - x - [.,.,.,.,.] - x,x,x,x - [x,x,x,x,x] - x,x - [.] - x - [.,y,y,.] - x,x - [.] - x - [.,y,y,.,.,x,x,x,x,x,x] - x,x,x,x - [x,x,.,y,y,1,.,x,x,x,x,x] - x,x - [.] - x - [x,x,x,x,.,y,y,.,.] - x,x,x - [y,y,y,.,.] - x,x,x,x - [.,.]
                    ),
                ),
            ),
        ),
    );
}
