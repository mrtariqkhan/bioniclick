<?php

require_once dirname(__FILE__) . '/ChartCallsByParameterBaseCompanyTree.php';

/**
 * Description of ChartCallsByParameterCompanyTreeAdvertiserSnowTest
 * 
 * @author fairdev
 */
class ChartCallsByParameterCompanyTreeAdvertiserSnowTest extends ChartCallsByParameterBaseCompanyTree {


    protected function configureFilterValuesAndResults() {

        $this->setFilterValues(self::$FILTER_VALUES);

        $this->setExpectedResultsByStatus(          self::$RESULTS_BY_STATUS);
        $this->setExpectedResultsByReferrerSource(  self::$RESULTS_BY_REFERRER_SOURCE);
        $this->setExpectedResultsBySearchType(      self::$RESULTS_BY_SEARCH_TYPE);
        $this->setExpectedResultsByUrl(             self::$RESULTS_BY_URL);
        $this->setExpectedResultsByKeyword(         self::$RESULTS_BY_KEYWORD);
    }

    protected function configureUserWithTz() {
        $this->configureCompanyTreeUserWithTz(
            null,
            self::ID_ADVERTISER_AGENCY_SNOW,
            self::TIMEZONE_ID_GMT
        );
    }

    protected static $FILTER_VALUES = array(
        self::TYPE_RANGE => array(
            'from'  => '1323000003',
            'to'    => '1323000019',
        ),
        self::TYPE_ALL => array(
        ),
    );


    // NOTE:: Does not depend on type of calls
    protected static $RESULTS_BY_STATUS = array(
        self::TYPE_RANGE => array(
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 12,    // - [4,2,1,1,4] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. not yet dialed',
                        self::PARAMETER_COUNT => 5,     // - [1,.,3,.,1] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. busy',
                        self::PARAMETER_COUNT => 2,     // - [.,.,1,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. failed',
                        self::PARAMETER_COUNT => 2,     // - [.,.,1,.,1] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '4. no-answer',
                        self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.] - 
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 12,    // - [4,2,1,1,4] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. not yet dialed',
                        self::PARAMETER_COUNT => 5,     // - [1,.,3,.,1] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. busy',
                        self::PARAMETER_COUNT => 2,     // - [.,.,1,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => self::VALUE_STATUS_OTHERS,
                        self::PARAMETER_COUNT => 3,     // - [1,.,1,.,1] - 
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 12,    // - [4,2,1,1,4] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. not yet dialed',
                        self::PARAMETER_COUNT => 5,     // - [1,.,3,.,1] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. busy',
                        self::PARAMETER_COUNT => 2,     // - [.,.,1,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. failed',
                        self::PARAMETER_COUNT => 2,     // - [.,.,1,.,1] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '4. no-answer',
                        self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.] - 
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 12,    // - [4,2,1,1,4] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. not yet dialed',
                        self::PARAMETER_COUNT => 5,     // - [1,.,3,.,1] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. busy',
                        self::PARAMETER_COUNT => 2,     // - [.,.,1,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => self::VALUE_STATUS_OTHERS,
                        self::PARAMETER_COUNT => 3,     // - [1,.,1,.,1] - 
                    ),
                ),
            ),
        ),
        self::TYPE_ALL => array(
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 24,    // - [4,2,1,1,4] - [.] - [.] - [1] - [4,1,1,2,3]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. not yet dialed',
                        self::PARAMETER_COUNT => 12,    // - [1,.,3,.,1] - [.] - [.] - [.] - [.,3,4,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. busy',
                        self::PARAMETER_COUNT => 6,     // - [.,.,1,1,.] - [1] - [.] - [.] - [1,.,1,.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '4. failed',
                        self::PARAMETER_COUNT => 5,     // - [.,.,1,.,1] - [.] - [.] - [1] - [1,.,.,.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '5. no-answer',
                        self::PARAMETER_COUNT => 4,     // - [1,.,.,.,.] - [.] - [.] - [.] - [.,2,.,.,1]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 24,    // - [4,2,1,1,4] - [.] - [.] - [1] - [4,1,1,2,3]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. not yet dialed',
                        self::PARAMETER_COUNT => 12,    // - [1,.,3,.,1] - [.] - [.] - [.] - [.,3,4,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. busy',
                        self::PARAMETER_COUNT => 6,     // - [.,.,1,1,.] - [1] - [.] - [.] - [1,.,1,.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => self::VALUE_STATUS_OTHERS,
                        self::PARAMETER_COUNT => 9,     // - [1,.,1,.,1] - [.] - [1] - [.] - [1,2,.,.,2]
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 24,    // - [4,2,1,1,4] - [.] - [.] - [1] - [4,1,1,2,3]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. not yet dialed',
                        self::PARAMETER_COUNT => 12,    // - [1,.,3,.,1] - [.] - [.] - [.] - [.,3,4,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. busy',
                        self::PARAMETER_COUNT => 6,     // - [.,.,1,1,.] - [1] - [.] - [.] - [1,.,1,.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '4. failed',
                        self::PARAMETER_COUNT => 5,     // - [.,.,1,.,1] - [.] - [.] - [1] - [1,.,.,.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => '5. no-answer',
                        self::PARAMETER_COUNT => 4,     // - [1,.,.,.,.] - [.] - [.] - [.] - [.,2,.,.,1]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. completed',
                        self::PARAMETER_COUNT => 24,    // - [4,2,1,1,4] - [.] - [.] - [1] - [4,1,1,2,3]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. not yet dialed',
                        self::PARAMETER_COUNT => 12,    // - [1,.,3,.,1] - [.] - [.] - [.] - [.,3,4,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. busy',
                        self::PARAMETER_COUNT => 6,     // - [.,.,1,1,.] - [1] - [.] - [.] - [1,.,1,.,1]
                    ),
                    array(
                        self::PARAMETER_TICK  => self::VALUE_STATUS_OTHERS,
                        self::PARAMETER_COUNT => 9,     // - [1,.,1,.,1] - [.] - [1] - [.] - [1,2,.,.,2]
                    ),
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_REFERRER_SOURCE = array(
        self::TYPE_RANGE => array(
            // - [.,.,.,.,.] - 
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.bing.com',
                        self::PARAMETER_COUNT => 3,     // - [2,.,.,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.google.com',
                        self::PARAMETER_COUNT => 2,     // - [2,.,.,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.yahoo.com',
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - 
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.bing.com',
                        self::PARAMETER_COUNT => 3,     // - [2,.,.,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.google.com',
                        self::PARAMETER_COUNT => 2,     // - [2,.,.,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.yahoo.com',
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - 
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.bing.com',
                        self::PARAMETER_COUNT => 3,     // - [2,.,.,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.google.com',
                        self::PARAMETER_COUNT => 2,     // - [2,.,.,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.yahoo.com',
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - 
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.bing.com',
                        self::PARAMETER_COUNT => 3,     // - [2,.,.,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.google.com',
                        self::PARAMETER_COUNT => 2,     // - [2,.,.,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.yahoo.com',
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - 
                    ),
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [.,.,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.] - 
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.bing.com',
                        self::PARAMETER_COUNT => 3,     // - [2,.,.,1,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '1. www.google.com',
                        self::PARAMETER_COUNT => 3,     // - [2,.,.,.,.] - [.] - [.] - [.] - [x,x,x,1,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.yahoo.com',
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.bing.com',
                        self::PARAMETER_COUNT => 3,     // - [2,.,.,1,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '1. www.google.com',
                        self::PARAMETER_COUNT => 3,     // - [2,.,.,.,.] - [.] - [.] - [.] - [x,x,x,1,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.yahoo.com',
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.bing.com',
                        self::PARAMETER_COUNT => 3,     // - [2,.,.,1,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '1. www.google.com',
                        self::PARAMETER_COUNT => 3,     // - [2,.,.,.,.] - [.] - [.] - [.] - [x,x,x,1,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.yahoo.com',
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. www.bing.com',
                        self::PARAMETER_COUNT => 3,     // - [2,.,.,1,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '1. www.google.com',
                        self::PARAMETER_COUNT => 3,     // - [2,.,.,.,.] - [.] - [.] - [.] - [x,x,x,1,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.yahoo.com',
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_SEARCH_TYPE = array(
        self::TYPE_RANGE => array(
            // - [.,.,.,.,.] - 
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. PPC',
                        self::PARAMETER_COUNT => 4,     // - [3,1,.,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. Organic',
                        self::PARAMETER_COUNT => 3,     // - [2,.,.,1,.] - 
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. PPC',
                        self::PARAMETER_COUNT => 4,     // - [3,1,.,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. Organic',
                        self::PARAMETER_COUNT => 3,     // - [2,.,.,1,.] - 
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. PPC',
                        self::PARAMETER_COUNT => 4,     // - [3,1,.,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. Organic',
                        self::PARAMETER_COUNT => 3,     // - [2,.,.,1,.] - 
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. PPC',
                        self::PARAMETER_COUNT => 4,     // - [3,1,.,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. Organic',
                        self::PARAMETER_COUNT => 3,     // - [2,.,.,1,.] - 
                    ),
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [.,.,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.] - 
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. PPC',
                        self::PARAMETER_COUNT => 5,     // - [3,1,.,.,.] - [.] - [.] - [.] - [x,x,x,1,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. Organic',
                        self::PARAMETER_COUNT => 3,     // - [2,.,.,1,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. PPC',
                        self::PARAMETER_COUNT => 5,     // - [3,1,.,.,.] - [.] - [.] - [.] - [x,x,x,1,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. Organic',
                        self::PARAMETER_COUNT => 3,     // - [2,.,.,1,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. PPC',
                        self::PARAMETER_COUNT => 5,     // - [3,1,.,.,.] - [.] - [.] - [.] - [x,x,x,1,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. Organic',
                        self::PARAMETER_COUNT => 3,     // - [2,.,.,1,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. PPC',
                        self::PARAMETER_COUNT => 5,     // - [3,1,.,.,.] - [.] - [.] - [.] - [x,x,x,1,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. Organic',
                        self::PARAMETER_COUNT => 3,     // - [2,.,.,1,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_URL = array(
        self::TYPE_RANGE => array(
            // - [.,.,.,.,.] - 
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. http://first-site.html',
                        self::PARAMETER_COUNT => 4,     // - [3,.,.,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. https://third-site.php',
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. www.second-site.html',
                        self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.] - 
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. http://first-site.html',
                        self::PARAMETER_COUNT => 4,     // - [3,.,.,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. https://third-site.php',
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. www.second-site.html',
                        self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.] - 
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. http://first-site.html',
                        self::PARAMETER_COUNT => 4,     // - [3,.,.,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. https://third-site.php',
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. www.second-site.html',
                        self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.] - 
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. http://first-site.html',
                        self::PARAMETER_COUNT => 4,     // - [3,.,.,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. https://third-site.php',
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => '3. www.second-site.html',
                        self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.] - 
                    ),
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [.,.,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.] - 
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. http://first-site.html',
                        self::PARAMETER_COUNT => 4,     // - [3,.,.,1,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. https://third-site.php',
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.second-site.html',
                        self::PARAMETER_COUNT => 2,     // - [1,.,.,.,.] - [.] - [.] - [.] - [x,x,x,1,.]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. http://first-site.html',
                        self::PARAMETER_COUNT => 4,     // - [3,.,.,1,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. https://third-site.php',
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.second-site.html',
                        self::PARAMETER_COUNT => 2,     // - [1,.,.,.,.] - [.] - [.] - [.] - [x,x,x,1,.]
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => '1. http://first-site.html',
                        self::PARAMETER_COUNT => 4,     // - [3,.,.,1,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. https://third-site.php',
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.second-site.html',
                        self::PARAMETER_COUNT => 2,     // - [1,.,.,.,.] - [.] - [.] - [.] - [x,x,x,1,.]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => '1. http://first-site.html',
                        self::PARAMETER_COUNT => 4,     // - [3,.,.,1,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. https://third-site.php',
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => '2. www.second-site.html',
                        self::PARAMETER_COUNT => 2,     // - [1,.,.,.,.] - [.] - [.] - [.] - [x,x,x,1,.]
                    ),
                ),
            ),
        ),
    );


    protected static $RESULTS_BY_KEYWORD = array(
        self::TYPE_RANGE => array(
            // - [.,.,.,.,.] - 
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 3'",
                        self::PARAMETER_COUNT => 4,     // - [3,.,.,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => "2. 'keyword 4'",
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => "3. 'keyword 2'",
                        self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.] - 
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 3'",
                        self::PARAMETER_COUNT => 4,     // - [3,.,.,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => "2. 'keyword 4'",
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => "3. 'keyword 2'",
                        self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.] - 
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 3'",
                        self::PARAMETER_COUNT => 4,     // - [3,.,.,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => "2. 'keyword 4'",
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => "3. 'keyword 2'",
                        self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.] - 
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 3'",
                        self::PARAMETER_COUNT => 4,     // - [3,.,.,1,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => "2. 'keyword 4'",
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - 
                    ),
                    array(
                        self::PARAMETER_TICK  => "3. 'keyword 2'",
                        self::PARAMETER_COUNT => 1,     // - [1,.,.,.,.] - 
                    ),
                ),
            ),
        ),
        self::TYPE_ALL => array(
            // - [.,.,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.] - 
            self::UNDEFINED_IS_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 3'",
                        self::PARAMETER_COUNT => 4,     // - [3,1,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => "2. 'keyword 2'",
                        self::PARAMETER_COUNT => 2,     // - [1,.,.,.,.] - [.] - [.] - [.] - [x,x,x,1,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => "2. 'keyword 4'",
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 3'",
                        self::PARAMETER_COUNT => 4,     // - [3,1,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => "2. 'keyword 2'",
                        self::PARAMETER_COUNT => 2,     // - [1,.,.,.,.] - [.] - [.] - [.] - [x,x,x,1,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => "2. 'keyword 4'",
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                ),
            ),
            self::UNDEFINED_IS_NOT_NEED => array(
                self::MAX_ROW_COUNT_UNDEFINED => array(
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 3'",
                        self::PARAMETER_COUNT => 4,     // - [3,1,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => "2. 'keyword 2'",
                        self::PARAMETER_COUNT => 2,     // - [1,.,.,.,.] - [.] - [.] - [.] - [x,x,x,1,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => "2. 'keyword 4'",
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                ),
                self::MAX_ROW_COUNT => array(
                    array(
                        self::PARAMETER_TICK  => "1. 'keyword 3'",
                        self::PARAMETER_COUNT => 4,     // - [3,1,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => "2. 'keyword 2'",
                        self::PARAMETER_COUNT => 2,     // - [1,.,.,.,.] - [.] - [.] - [.] - [x,x,x,1,.]
                    ),
                    array(
                        self::PARAMETER_TICK  => "2. 'keyword 4'",
                        self::PARAMETER_COUNT => 2,     // - [1,1,.,.,.] - [.] - [.] - [.] - [x,x,x,.,.]
                    ),
                ),
            ),
        ),
    );
}
