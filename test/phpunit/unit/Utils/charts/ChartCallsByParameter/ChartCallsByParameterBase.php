<?php

require_once dirname(__FILE__) . '/../../../UserTimezoneTestBase.php';

/**
 * Description of ChartCallsByParameterBase
 * 
 * @author fairdev
 */
abstract class ChartCallsByParameterBase extends UserTimezoneTestBase {

    const PARAMETER_COUNT   = 'calls_count';
    const PARAMETER_TICK    = 'tick';


    public function assertArrayAreSame($expectedArray, array $actualArray, $message) {

        $expectedArray = empty($expectedArray) 
            ? array() 
            : $expectedArray
        ;

        foreach ($expectedArray as $expectedArr) {

            $expectedTick = $expectedArr[self::PARAMETER_TICK];
            $expectedCount = intval($expectedArr[self::PARAMETER_COUNT]);

            $actualTick = array_key_exists($expectedTick, $actualArray) ? $expectedTick : '';
            $actualCount = array_key_exists($expectedTick, $actualArray) ? $actualArray[$expectedTick]: -1;

            $this->assertEquals(
                $expectedTick, 
                $actualTick, 
                "$message (actual results has no info about '$expectedTick')"
            );
            $this->assertEquals(
                $expectedCount, 
                $actualCount, 
                "$message (actual count of '$expectedTick' is wrong)"
            );
        }

        $this->assertEquals(
            count($expectedArray), 
            count($actualArray), 
            "$message (size of actual array is wrong)"
        );
    }


    /**
     *
     * @param array $filterValues
     * @param string $chartHelperClassName
     * @param bool $undefinedIsNeed
     * @param int $maxRawsCount
     * @return type 
     */
    protected function getResultArray(
        $filterValues,
        $chartHelperClassName,
        $undefinedIsNeed = false, 
        $maxRawsCount = 3
    ) {

        $user = $this->getUser();

        $advertiser = null;
        $company    = null;
        if ($user->isAdvertiserUser()) {
            $advertiser = $user->getAdvertiser();
        } else {
            $company = $user->getFirstCompany();
        }
        $chartHelper = new $chartHelperClassName(
            $advertiser,
            $company,
            $this->getGmtDifference(),
            $filterValues,
            $user
        );
        $resultArray = $chartHelper->findData($undefinedIsNeed, $maxRawsCount);
//var_dump($result);
        return $resultArray;
    }
}
