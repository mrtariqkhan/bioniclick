<?php

    include dirname(__FILE__) . '/Doctrine.php';

    $testsCount = 4;
    $limeTest = new lime_test($testsCount, new lime_output_color());


    $bionicCompany = CompanyTable::getBionic();

    $limeTest->ok(!empty($bionicCompany), 'Bionic has been found');
        
    if (!empty($bionicCompany)) {
        $limeTest->isa_ok($bionicCompany, 'Company', 'Bionic is company');
        $limeTest->is($bionicCompany->getLevel(), CompanyTable::$LEVEL_BIONIC, 'Bionic has need level');

        $limeTest->todo('Check if Bionic has admin (add fixtures for sfGuard-tables)');
//        $admin = $bionicCompany->getAdmin();
//        $limeTest->ok(!empty($admin), 'Bionic has admin');
    } else {
        $limeTest->skip('Because there is no Bionic company', 3);
    }

