<?php
    require_once dirname(__FILE__) . '/../bootstrap/unit.php';
    require_once dirname(__FILE__) . '/../../plugins/sfTwilioAPIPlugin/lib/TwilioPhoneNumberAPI.php';

    $testsCount = 6;
    $limeTest = new lime_test($testsCount, new lime_output_color());

    /* Set our AccountSid and AuthToken */
    $accountSid = 'AC3657210a30ba1b3272221bffbd6d8ad6';
    $authToken = '2c5963d6a97d08d979a0e0a57fcb60ad';
    $sid = 'PN296cd6347cd7bf74808b9aecd7c0c1e5';
    $url = 'http://localhost';
    $friendlyName = 'Annie test changed';
    $apiVersion = '2008-08-01';
    $numberType = null;

//    $limeTest->comment("TwilioPhoneNumberAPITest: getTollFreeIncomingPhoneNumbers");
//    
//    $resultArray1 = TwilioPhoneNumberAPI::getTollFreeIncomingPhoneNumbers(
//        $accountSid,
//        $authToken
//    );
//    //var_dump($resultArray1);

    $limeTest->comment("TwilioPhoneNumberAPITest: createPathString");

    $pathString = TwilioPhoneNumberAPI::createPathString(
        $apiVersion,
        $accountSid,
        $numberType,
        $sid
    );

    $limeTest->is(
        $pathString,
        'https://api.twilio.com/2008-08-01/Accounts/AC3657210a30ba1b3272221bffbd6d8ad6/IncomingPhoneNumbers/PN296cd6347cd7bf74808b9aecd7c0c1e5',
        'Path string creation test'
    );

    $limeTest->comment("TwilioPhoneNumberAPITest: sendIncomingPhoneNumber");

    $resultArray2 = TwilioPhoneNumberAPI::sendIncomingPhoneNumber(
        $accountSid,
        $authToken,
        $sid,
        $url,
        $friendlyName
    );
//    var_dump($resultArray2);

    $limeTest->ok(is_array($resultArray2), 'Is result array');
    $limeTest->is(count($resultArray2), 3, 'Result has 3 fields');
    $limeTest->ok(!isset($resultArray2['error']), 'Error is empty');
    $limeTest->is($resultArray2['sid'], $sid, 'Sid field comparing');
    $limeTest->is($resultArray2['phone_number'], '8663740610', 'Phone number field comparing');
