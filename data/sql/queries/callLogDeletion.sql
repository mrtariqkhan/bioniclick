-- NOTE:: script removes twilio_incoming_call records related to concrete ADVERTISER (id = 18)
-- NOTE:: phone_call table is in use.




--    select
--        trc.id
--        , tic.id
--    from
--        twilio_redialed_call trc
--        inner join twilio_incoming_call tic on tic.id = trc.twilio_incoming_call_id
--        inner join phone_call pc on pc.id = tic.id
--    where
--        pc.advertiser_id = 18
--    order by
--        tic.id ASC
--    ;

delete from 
    twilio_redialed_call
where
    twilio_incoming_call_id in (
        select 
            tic.id
        from 
            twilio_incoming_call tic
            inner join phone_call pc on pc.id = tic.id
        where
            pc.advertiser_id = 18
    )
;




--    select
--          cr.id
--        , tic.id
--    from
--        call_result cr
--        inner join twilio_incoming_call tic on tic.id = cr.twilio_incoming_call_id
--        inner join phone_call pc on pc.id = tic.id
--    where
--        pc.advertiser_id = 18
--    order by
--        tic.id ASC
--    ;

delete from 
    call_result 
where
    twilio_incoming_call_id in (
        select 
            tic.id
        from 
            twilio_incoming_call tic
            inner join phone_call pc on pc.id = tic.id
        where
            pc.advertiser_id = 18
    )
;




--    select
--        tcq.id
--        , tic.id
--    from
--        twilio_call_queue tcq
--        inner join twilio_incoming_call tic on tic.id = tcq.twilio_incoming_call_id
--        inner join phone_call pc on pc.id = tic.id
--    where
--        pc.advertiser_id = 18
--    order by
--        tic.id ASC
--    ;

delete from 
    twilio_call_queue
where
    twilio_incoming_call_id in (
        select 
            tic.id
        from 
            twilio_incoming_call tic
            inner join phone_call pc on pc.id = tic.id
        where
            pc.advertiser_id = 18
    )
;



--    select
--        tcr.id
--        , tic.id
--    from
--        twilio_call_recording tcr
--        inner join twilio_incoming_call tic on tic.id = tcr.twilio_incoming_call_id
--        inner join phone_call pc on pc.id = tic.id
--    where
--        pc.advertiser_id = 18
--    order by
--        tic.id ASC
--    ;

delete from 
    twilio_call_recording
where
    twilio_incoming_call_id in (
        select 
            tic.id
        from 
            twilio_incoming_call tic
            inner join phone_call pc on pc.id = tic.id
        where
            pc.advertiser_id = 18
    )
;




--    select
--          tic.id
--        , tic.start_time
--        , FROM_UNIXTIME(tic.start_time)
--    from
--        twilio_incoming_call tic
--        inner join phone_call pc on pc.id = tic.id
--    where
--        pc.advertiser_id = 18
--    order by
--        tic.id ASC
--    ;

delete from
    twilio_incoming_call
where
    id in (
        select 
            pc.id
        from 
            phone_call pc
        where
            pc.advertiser_id = 18
    )
;



--    select
--          pc.id
--        , tic.id
--        , tic.twilio_incoming_phone_number_id
--    from
--        phone_call pc
--        inner join twilio_incoming_call tic on tic.id = pc.id
--    where
--        advertiser_id = 18
--    order by
--        tic.id ASC
--    ;
--
--    select
--          pc.id
--    from
--        phone_call pc
--    where
--        advertiser_id = 18
--    order by
--        pc.id ASC
--    ;

delete from
    phone_call
where
    advertiser_id = 18
;

