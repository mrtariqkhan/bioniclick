# Скрипт выдает список логинов, под которымими можно зайти, для проверки
# permissions, методов заданных в поле LIKE.
# Пример: %crea% выдаст список всех логинов, у которых есть права на
# выполнение метода create. Итоговый список содержит пару
# догин + название разрешенного метода.


SELECT
      guardUser.username
    , tmpGroup.name
FROM
    sf_guard_user AS guardUser
    INNER JOIN (
        SELECT
              gug.user_id
            , tmpGGP.group_id
            , tmpGGP.name
        FROM
            sf_guard_user_group AS gug
            INNER JOIN (
                SELECT DISTINCT
                      ggp.group_id
                    , gp.id
                    , gp.name
                FROM
                    sf_guard_group_permission AS ggp
                    INNER JOIN sf_guard_permission AS gp ON (ggp.permission_id = gp.id AND gp.name LIKE '%crea%')
            ) AS tmpGGP ON (gug.group_id = tmpGGP.group_id)
    ) AS tmpGroup ON (guardUser.id = tmpGroup.user_id)
;
