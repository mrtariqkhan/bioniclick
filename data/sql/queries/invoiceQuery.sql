

SELECT
    SUM(CEIL(t.duration / 60.0)) AS t__0 
FROM
    twilio_incoming_call t 
    INNER JOIN twilio_incoming_phone_number t2 ON t.twilio_incoming_phone_number_id = t2.id 
    INNER JOIN twilio_local_area_code t3 ON t2.twilio_local_area_code_id = t3.id 
    INNER JOIN advertiser_incoming_number_pool a ON t2.id = a.twilio_incoming_phone_number_id 
WHERE (
        a.campaign_id = 94 
    AND t3.phone_number_type = 'toll free' 
    AND t.start_time >= 1309478400 
    AND t.start_time <= 1311321887
);

SELECT
    SUM(CEIL(tic.duration / 60.0)) AS tic__duration
FROM
    twilio_incoming_call tic 
    INNER JOIN twilio_incoming_phone_number tipn ON tic.twilio_incoming_phone_number_id = tipn.id 
    INNER JOIN twilio_local_area_code tlac ON tipn.twilio_local_area_code_id = tlac.id 
    INNER JOIN advertiser_incoming_number_pool ainp ON tipn.id = ainp.twilio_incoming_phone_number_id 
WHERE (
        ainp.campaign_id = 94 
    AND tlac.phone_number_type = 'toll free' 
    AND tic.start_time >= 1309478400 
    AND tic.start_time <= 1311321887
);




SELECT
      ainp.campaign_id AS campaign_id
    , tipn.phone_number AS tipn__phone_number
    , tic.duration AS tic__duration
FROM
    twilio_incoming_call tic 
    INNER JOIN twilio_incoming_phone_number tipn ON tic.twilio_incoming_phone_number_id = tipn.id 
    INNER JOIN twilio_local_area_code tlac ON tipn.twilio_local_area_code_id = tlac.id 
    INNER JOIN advertiser_incoming_number_pool ainp ON tipn.id = ainp.twilio_incoming_phone_number_id 
WHERE (
        ainp.campaign_id = 94 
    AND tlac.phone_number_type = 'toll free' 
    AND tic.start_time >= 1309478400 
    AND tic.start_time <= 1311321887
)
ORDER BY tic.start_time
;