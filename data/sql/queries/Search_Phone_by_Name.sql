SELECT c.id, c.advertiser_id, c.name, a.id, a.company_id, ainp.advertiser_id, ainp.twilio_incoming_phone_number_id, ainp.campaign_id, tipn.id, tipn.phone_number
   FROM campaign c 
    INNER JOIN advertiser a ON (c.advertiser_id = a.id) 
    INNER JOIN advertiser_incoming_number_pool ainp ON (a.id = ainp.advertiser_id AND c.id = ainp.campaign_id) 
    INNER JOIN twilio_incoming_phone_number tipn ON (tipn.id = ainp.twilio_incoming_phone_number_id) WHERE c.name LIKE ('%Winnipeg%');