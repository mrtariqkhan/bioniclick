SELECT c.id, c.name, count(uinp.id)
FROM test1.campaign c
LEFT JOIN test1.user_incoming_number_pool uinp ON (c.id = uinp.campaign_id)
WHERE (uinp.is_default is null or uinp.is_default=1)
GROUP BY c.id, c.name
HAVING count(uinp.id) = 1
order by 3