
select
    count(tmp.caller) callers_count, sum(calls_count) all_calls_count
from (
    select
        count(tic.id) calls_count, tic.twilio_caller_phone_number_id caller
    from
        twilio_incoming_call tic
    where
        tic.start_time between 1301172414 and 1301233695
    group by
        tic.twilio_caller_phone_number_id
) as tmp