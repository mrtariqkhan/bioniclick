SELECT
    COUNT(tmp.calls_count) unique_callers,
    calls_count
FROM
    (SELECT
        COUNT(tic.twilio_caller_phone_number_id) calls_count
        FROM
            twilio_incoming_call tic
        WHERE
            start_time >= 1301979600
            AND start_time <= 1302065999

            AND
            (
                EXISTS (
                    SELECT
                        null
                    FROM
                        company_incoming_number_pool cinp
                        INNER JOIN company com ON cinp.company_id = com.id
                    WHERE
                        cinp.twilio_incoming_phone_number_id = tic.twilio_incoming_phone_number_id
                        AND (com.lft BETWEEN 1 AND 16)
                )
                OR EXISTS (
                    SELECT
                        null
                    FROM
                        advertiser_incoming_number_pool ainp
                        INNER JOIN advertiser adv ON ainp.advertiser_id = adv.id
                        INNER JOIN company com
                            ON
                                com.id = adv.company_id
                                AND (com.lft BETWEEN 1 AND 16)
                    WHERE
                        ainp.twilio_incoming_phone_number_id = tic.twilio_incoming_phone_number_id
                )
            )

    GROUP BY
        twilio_caller_phone_number_id) as tmp
GROUP BY calls_count
ORDER BY calls_count