/* Advertisers */
SELECT
    tmp.calls_count,
    tmp.name
FROM (
    SELECT
        count(tic.id) as calls_count,
        c.id as id,
        c.name as name
    FROM
        twilio_incoming_call tic
        INNER JOIN twilio_incoming_phone_number tipn ON tic.twilio_incoming_phone_number_id = tipn.id
        INNER JOIN advertiser_incoming_number_pool ainp ON ainp.twilio_incoming_phone_number_id = tipn.id
        LEFT JOIN campaign c ON ainp.campaign_id = c.id
    WHERE
        ainp.twilio_incoming_phone_number_id = tic.twilio_incoming_phone_number_id
        AND ainp.advertiser_id = {$user->getAdvertiserId()}
    GROUP BY
        c.id, c.name
    HAVING
        count(tic.id) > 0
) as tmp
ORDER BY tmp.calls_count DESC
;

/* Companies */
SELECT
    tmp.calls_count,
    tmp.name
FROM (
    SELECT
        count(tic.id) as calls_count,
        c.id as company_id,
        c.name as company_name
    FROM
        twilio_incoming_call tic
        INNER JOIN twilio_incoming_phone_number tipn ON tic.twilio_incoming_phone_number_id = tipn.id
        INNER JOIN advertiser_incoming_number_pool ainp ON ainp.twilio_incoming_phone_number_id = tipn.id
        INNER JOIN advertiser a ON ainp.advertiser_id = a.id
        INNER JOIN company c ON a.company_id = c.id
    WHERE
        ainp.twilio_incoming_phone_number_id = tic.twilio_incoming_phone_number_id
        AND (c.lft BETWEEN 1 AND 16)
        AND (c.level = 1)
    GROUP BY
        c.id, c.name
    HAVING
        count(tic.id) > 0
) as tmp
ORDER BY tmp.calls_count DESC
;