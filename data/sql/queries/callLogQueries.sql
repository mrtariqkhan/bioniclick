/* actual */

SELECT 
      t.id              AS t__id
    , t.caller_country  AS t__caller_country
    , t.caller_zip      AS t__caller_zip
    , t.caller_state    AS t__caller_state
    , t.price           AS t__price
    , t.sid             AS t__sid
    , p.id              AS p__id
    , a.id              AS a__id
    , a.name            AS a__name
    , c.id              AS c__id
    , c.name            AS c__name
    , v.id              AS v__id
    , v.is_organic      AS v__is_organic
    , combo.id          AS combo__id
    , combo.name        AS combo__name
    , page.id           AS page__id
    , page.page_url     AS page__url
    , cr.id             AS cr__id
    , cr.revenue        AS cr__revenue
    , cr.first_name     AS cr__first_name
    , cr.last_name      AS cr__last_name
    , cr.sex            AS cr__sex
    , cr.address        AS cr__address
    , cr.zip            AS cr__zip
    , cr.phone          AS cr__phone
    , cr.rating         AS cr__rating
    , cr.note           AS cr__note
    , tcpn.id           AS tcpn__id
    , tcpn.phone_number AS tcpn_phone_number
    , tipn.id           AS tipn__id
    , tipn.phone_number AS tipn__phone_number
    , tcr.id            AS tcr__id
    , tcr.file_url      AS tcr__file_url
    , tc.id             AS tc__id
    , tc.name           AS tc__name
    , IF(v.is_organic = "2", "Direct", r.url)               AS r_url
    , IF(trc.id IS NOT NULL, trc.start_time, t.start_time)  AS start_time
    , IF(trc.id IS NOT NULL, trc.end_time, t.end_time)      AS end_time
    , IF(trc.id IS NOT NULL, trc.duration, t.duration)      AS duration
    , IF(
        v.is_organic = "2"
        , "Direct"
        , IF(
            v.keyword = "0" OR v.keyword = "undefined" OR v.keyword = "" OR v.keyword LIKE "N/A" OR v.keyword LIKE "&esrc"
            , NULL
            , v.keyword
        )
    ) AS v__keyword
    , IF(
        v.traffic_source IS NULL
        , CONCAT("http://", c.domain, "/", page.page_url, "?cmbid=", combo.api_key)
        , v.traffic_source
    ) AS v_URL
    , IF(tcpn.is_blocked, 1, 0)                                               AS tcpn__is_blocked
    , IF(trc.id IS NOT NULL, trc.called_number, "Undefined")  AS called_number
    , IF(trc.id IS NOT NULL, trc.call_status, t.call_status)  AS call_status
    , IF(
        v.id IS NULL
        , 3
        , IF(
            v.analytics_time < t.start_time AND v.analytics_time >= (t.start_time - 3600)
            , 1
            , 2
        )
    ) AS v__analytics_code
    , IF(
        v.id IS NULL
        , 'Call has no suitable Visitor Analytics'
        , IF(
            v.analytics_time < t.start_time AND v.analytics_time >= (t.start_time - 3600)
            , 'Call has suitable Visitor Analytics'
            , 'Call has Old suitable Visitor Analytics'
        )
    ) AS v__analytics_type

FROM 
    twilio_incoming_call t 
    INNER JOIN phone_call p ON t.id = p.id 
    LEFT JOIN advertiser a ON p.advertiser_id = a.id 
    LEFT JOIN campaign c ON p.campaign_id = c.id 
    LEFT JOIN visitor_analytics v ON p.visitor_analytics_id = v.id 
    LEFT JOIN combo combo ON v.combo_id = combo.id 
    LEFT JOIN page page ON combo.page_id = page.id 
    LEFT JOIN referrer_source r ON v.referrer_source_id = r.id 
    LEFT JOIN call_result cr ON t.id = cr.twilio_incoming_call_id 
    LEFT JOIN twilio_redialed_call trc ON t.id = trc.twilio_incoming_call_id 
    LEFT JOIN twilio_caller_phone_number tcpn ON t.twilio_caller_phone_number_id = tcpn.id 
    LEFT JOIN twilio_incoming_phone_number tipn ON t.twilio_incoming_phone_number_id = tipn.id 
    LEFT JOIN twilio_call_recording tcr ON t.id = tcr.twilio_incoming_call_id 
    LEFT JOIN twilio_city tc ON t.twilio_city_id = tc.id 

WHERE (
    p.advertiser_id IN (3, 4, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 27, 28, 29)
    AND t.start_time >= 1322719200 
    AND t.start_time <= 1323237599
)
ORDER BY
    t.start_time desc 
LIMIT 10











/*1 Without time filter*/
SELECT SQL_CALC_FOUND_ROWS
      twilioIncomingCall.id twilioIncomingCall_id
    , twilioIncomingCall.caller_country twilioIncomingCall_caller_country
    , twilioIncomingCall.caller_zip twilioIncomingCall_caller_zip
    , twilioIncomingCall.caller_state twilioIncomingCall_caller_state
    , twilioIncomingCall.call_status twilioIncomingCall_call_status
    , twilioIncomingCall.price twilioIncomingCall_price

    , twilioIncomingPhoneNumber.id twilioIncomingPhoneNumber_id
    , twilioIncomingPhoneNumber.phone_number twilioIncomingPhoneNumber_phone_number

    , advertiserIncomingNumberPool.id advertiserIncomingNumberPool_id

    , campaign.id campaign_id
    , campaign.name campaign_name

    , twilioCallerPhoneNumber.id twilioCallerPhoneNumber_id
    , twilioCallerPhoneNumber.phone_number twilioCallerPhoneNumber_phone_number
    , twilioCallerPhoneNumber.is_blocked twilioCallerPhoneNumber_is_blocked

    , twilioCity.id twilioCity_id
    , twilioCity.name twilioCity_name

    , callResult.id callResult_id
    , callResult.first_name callResult_first_name
    , callResult.last_name callResult_last_name
    , callResult.sex callResult_sex
    , callResult.address callResult_address
    , callResult.zip callResult_zip
    , callResult.phone callResult_phone
    , callResult.revenue callResult_revenue
    , callResult.rating callResult_rating

    , twilioCallRecording.id twilioCallRecording_id
    , twilioCallRecording.file_url twilioCallRecording_file_url

    , tmpAnalytics.id tmpAnalytics_id
    , tmpAnalytics.traffic_source tmpAnalytics_traffic_source
    , tmpAnalytics.keyword tmpAnalytics_keyword
    , tmpAnalytics.is_organic tmpAnalytics_is_organic
    , tmpAnalytics.combo_id tmpAnalytics_combo_id
    , tmpAnalytics.combo_name tmpAnalytics_combo_name
    , tmpAnalytics.page_id tmpAnalytics_page_id
    , tmpAnalytics.page_url tmpAnalytics_page_url
    , tmpAnalytics.referrer_source tmpAnalytics_referrer_source

    , tmpUnionCall.id tmpUnionCall_id
    , tmpUnionCall.called_number tmpUnionCall_called_number
    , tmpUnionCall.start_time tmpUnionCall_start_time
    , tmpUnionCall.end_time tmpUnionCall_end_time
    , tmpUnionCall.duration tmpUnionCall_duration
    , tmpUnionCall.is_redialed_call tmpUnionCall_is_redialed_call

FROM
    twilio_incoming_call twilioIncomingCall

    INNER JOIN twilio_incoming_phone_number twilioIncomingPhoneNumber ON twilioIncomingCall.twilio_incoming_phone_number_id = twilioIncomingPhoneNumber.id
    INNER JOIN advertiser_incoming_number_pool advertiserIncomingNumberPool ON twilioIncomingPhoneNumber.id = advertiserIncomingNumberPool.twilio_incoming_phone_number_id
    INNER JOIN campaign campaign ON advertiserIncomingNumberPool.campaign_id = campaign.id
    INNER JOIN advertiser advertiser ON advertiserIncomingNumberPool.advertiser_id = advertiser.id
    INNER JOIN sf_guard_user_profile sfGuardUserProfile ON advertiser.id = sfGuardUserProfile.advertiser_id
    INNER JOIN sf_guard_user sfGuardUser ON sfGuardUserProfile.user_id = sfGuardUser.id
    INNER JOIN twilio_caller_phone_number twilioCallerPhoneNumber ON twilioIncomingCall.twilio_caller_phone_number_id = twilioCallerPhoneNumber.id

    LEFT JOIN twilio_city twilioCity ON twilioIncomingCall.twilio_city_id = twilioCity.id
    LEFT JOIN call_result callResult ON twilioIncomingCall.id = callResult.twilio_incoming_call_id
    LEFT JOIN twilio_call_recording twilioCallRecording ON twilioIncomingCall.id = twilioCallRecording.twilio_incoming_call_id

    LEFT JOIN (
        SELECT
              visitorAnalytics.id id
            , visitorAnalytics.traffic_source traffic_source
            , visitorAnalytics.keyword keyword
            , visitorAnalytics.is_organic is_organic

            , analyticsPhoneNumber.id analyticsPhoneNumber_id
            , analyticsPhoneNumber.twilio_incoming_phone_number_id twilio_incoming_phone_number_id

            , analyticsTime.id analyticsTime_id
            , analyticsTime.time time
            , MAX(analyticsTime.time) max_analytics_time

            , combo.id combo_id
            , combo.name combo_name

            , page.id page_id
            , page.page_url page_url

            , referrerSource.id referrerSource_id
            , referrerSource.url referrer_source

        FROM
            visitor_analytics visitorAnalytics

            INNER JOIN analytics_time analyticsTime ON visitorAnalytics.analytics_time_id = analyticsTime.id
            INNER JOIN analytics_phone_number analyticsPhoneNumber ON visitorAnalytics.id = analyticsPhoneNumber.visitor_analytics_id
            INNER JOIN combo combo ON visitorAnalytics.combo_id = combo.id
            INNER JOIN page page ON combo.page_id = page.id

            LEFT JOIN referrer_source referrerSource ON visitorAnalytics.referrer_source_id = referrerSource.id

        GROUP BY analyticsPhoneNumber.twilio_incoming_phone_number_id
        HAVING time = max_analytics_time

    ) tmpAnalytics ON
            tmpAnalytics.twilio_incoming_phone_number_id = twilioIncomingCall.twilio_incoming_phone_number_id
        AND tmpAnalytics.time <= twilioIncomingCall.start_time

    LEFT JOIN (
        SELECT *
        FROM
        (
            (
                    SELECT
                          twilioIncomingCall.id id
                        , NULL called_number
                        , twilioIncomingCall.start_time start_time
                        , twilioIncomingCall.end_time end_time
                        , twilioIncomingCall.duration duration
                        , 0 is_redialed_call
                    FROM
                        twilio_incoming_call twilioIncomingCall
            )
            UNION
            (
                    SELECT
                          twilioRedialedCall.twilio_incoming_call_id id
                        , twilioRedialedCall.called_number called_number
                        , twilioRedialedCall.start_time start_time
                        , twilioRedialedCall.end_time end_time
                        , twilioRedialedCall.duration duration
                        , 1 is_redialed_call
                    FROM
                        twilio_redialed_call twilioRedialedCall
            )
            ORDER BY id, is_redialed_call DESC
        ) tmpUnionCallGrouped
        GROUP BY id
    ) tmpUnionCall ON
        tmpUnionCall.id = twilioIncomingCall.id

WHERE

    advertiserIncomingNumberPool.campaign_id IS NOT NULL
    AND sfGuardUser.id = 8
ORDER BY twilioIncomingCall.start_time DESC
;







/*2 With time filter*/
SELECT SQL_CALC_FOUND_ROWS
      twilioIncomingCall.id twilioIncomingCall_id
    , twilioIncomingCall.caller_country twilioIncomingCall_caller_country
    , twilioIncomingCall.caller_zip twilioIncomingCall_caller_zip
    , twilioIncomingCall.caller_state twilioIncomingCall_caller_state
    , twilioIncomingCall.call_status twilioIncomingCall_call_status
    , twilioIncomingCall.price twilioIncomingCall_price

    , twilioIncomingPhoneNumber.id twilioIncomingPhoneNumber_id
    , twilioIncomingPhoneNumber.phone_number twilioIncomingPhoneNumber_phone_number

    , advertiserIncomingNumberPool.id advertiserIncomingNumberPool_id

    , campaign.id campaign_id
    , campaign.name campaign_name

    , twilioCallerPhoneNumber.id twilioCallerPhoneNumber_id
    , twilioCallerPhoneNumber.phone_number twilioCallerPhoneNumber_phone_number
    , twilioCallerPhoneNumber.is_blocked twilioCallerPhoneNumber_is_blocked

    , twilioCity.id twilioCity_id
    , twilioCity.name twilioCity_name

    , callResult.id callResult_id
    , callResult.first_name callResult_first_name
    , callResult.last_name callResult_last_name
    , callResult.sex callResult_sex
    , callResult.address callResult_address
    , callResult.zip callResult_zip
    , callResult.phone callResult_phone
    , callResult.revenue callResult_revenue
    , callResult.rating callResult_rating

    , twilioCallRecording.id twilioCallRecording_id
    , twilioCallRecording.file_url twilioCallRecording_file_url

    , tmpAnalytics.id tmpAnalytics_id
    , tmpAnalytics.traffic_source tmpAnalytics_traffic_source
    , tmpAnalytics.keyword tmpAnalytics_keyword
    , tmpAnalytics.is_organic tmpAnalytics_is_organic
    , tmpAnalytics.combo_id tmpAnalytics_combo_id
    , tmpAnalytics.combo_name tmpAnalytics_combo_name
    , tmpAnalytics.page_id tmpAnalytics_page_id
    , tmpAnalytics.page_url tmpAnalytics_page_url
    , tmpAnalytics.referrer_source tmpAnalytics_referrer_source

    , tmpUnionCall.id tmpUnionCall_id
    , tmpUnionCall.called_number tmpUnionCall_called_number
    , tmpUnionCall.start_time tmpUnionCall_start_time
    , tmpUnionCall.end_time tmpUnionCall_end_time
    , tmpUnionCall.duration tmpUnionCall_duration
    , tmpUnionCall.is_redialed_call tmpUnionCall_is_redialed_call

FROM
    twilio_incoming_call twilioIncomingCall

    INNER JOIN twilio_incoming_phone_number twilioIncomingPhoneNumber ON twilioIncomingCall.twilio_incoming_phone_number_id = twilioIncomingPhoneNumber.id
    INNER JOIN advertiser_incoming_number_pool advertiserIncomingNumberPool ON twilioIncomingPhoneNumber.id = advertiserIncomingNumberPool.twilio_incoming_phone_number_id
    INNER JOIN campaign campaign ON advertiserIncomingNumberPool.campaign_id = campaign.id
    INNER JOIN advertiser advertiser ON advertiserIncomingNumberPool.advertiser_id = advertiser.id
    INNER JOIN sf_guard_user_profile sfGuardUserProfile ON advertiser.id = sfGuardUserProfile.advertiser_id
    INNER JOIN sf_guard_user sfGuardUser ON sfGuardUserProfile.user_id = sfGuardUser.id
    INNER JOIN twilio_caller_phone_number twilioCallerPhoneNumber ON twilioIncomingCall.twilio_caller_phone_number_id = twilioCallerPhoneNumber.id

    LEFT JOIN twilio_city twilioCity ON twilioIncomingCall.twilio_city_id = twilioCity.id
    LEFT JOIN call_result callResult ON twilioIncomingCall.id = callResult.twilio_incoming_call_id
    LEFT JOIN twilio_call_recording twilioCallRecording ON twilioIncomingCall.id = twilioCallRecording.twilio_incoming_call_id

    LEFT JOIN (
        SELECT
              visitorAnalytics.id id
            , visitorAnalytics.traffic_source traffic_source
            , visitorAnalytics.keyword keyword
            , visitorAnalytics.is_organic is_organic

            , analyticsPhoneNumber.id analyticsPhoneNumber_id
            , analyticsPhoneNumber.twilio_incoming_phone_number_id twilio_incoming_phone_number_id

            , analyticsTime.id analyticsTime_id
            , UNIX_TIMESTAMP(DATE_FORMAT(analyticsTime.time,'%Y-%m-%d %T')) time
            , MAX(UNIX_TIMESTAMP(DATE_FORMAT(analyticsTime.time,'%Y-%m-%d %T'))) max_analytics_time

            , combo.id combo_id
            , combo.name combo_name

            , page.id page_id
            , page.page_url page_url

            , referrerSource.id referrerSource_id
            , referrerSource.url referrer_source

        FROM
            visitor_analytics visitorAnalytics

            INNER JOIN analytics_time analyticsTime ON visitorAnalytics.analytics_time_id = analyticsTime.id
            INNER JOIN analytics_phone_number analyticsPhoneNumber ON visitorAnalytics.id = analyticsPhoneNumber.visitor_analytics_id
            INNER JOIN combo combo ON visitorAnalytics.combo_id = combo.id
            INNER JOIN page page ON combo.page_id = page.id

            LEFT JOIN referrer_source referrerSource ON visitorAnalytics.referrer_source_id = referrerSource.id

        GROUP BY analyticsPhoneNumber.twilio_incoming_phone_number_id
        HAVING time = max_analytics_time

    ) tmpAnalytics ON
            tmpAnalytics.twilio_incoming_phone_number_id = twilioIncomingCall.twilio_incoming_phone_number_id
        AND tmpAnalytics.time <= twilioIncomingCall.start_time

    LEFT JOIN (
        SELECT *
        FROM
        (
            (
                    SELECT
                          twilioIncomingCall.id id
                        , NULL called_number
                        , twilioIncomingCall.start_time start_time
                        , twilioIncomingCall.end_time end_time
                        , twilioIncomingCall.duration duration
                        , 0 is_redialed_call
                    FROM
                        twilio_incoming_call twilioIncomingCall
            )
            UNION
            (
                    SELECT
                          twilioRedialedCall.twilio_incoming_call_id id
                        , twilioRedialedCall.called_number called_number
                        , twilioRedialedCall.start_time start_time
                        , twilioRedialedCall.end_time end_time
                        , twilioRedialedCall.duration duration
                        , 1 is_redialed_call
                    FROM
                        twilio_redialed_call twilioRedialedCall
            )
            ORDER BY id, is_redialed_call DESC
        ) tmpUnionCallGrouped
        GROUP BY id
    ) tmpUnionCall ON
        tmpUnionCall.id = twilioIncomingCall.id

WHERE
    twilioIncomingCall.start_time>=1235887200 AND twilioIncomingCall.start_time<=1301633999
    AND advertiserIncomingNumberPool.campaign_id IS NOT NULL
    AND sfGuardUser.id = 19
ORDER BY twilioIncomingCall.start_time DESC
;






/* 3 VisitorAnalytics Part of (4) query */
select * from (
            SELECT
                  visitorAnalytics.id id
                , visitorAnalytics.traffic_source traffic_source
                , visitorAnalytics.keyword keyword
                , visitorAnalytics.is_organic is_organic

                , analyticsPhoneNumber.id analyticsPhoneNumber_id
                , analyticsPhoneNumber.twilio_incoming_phone_number_id twilio_incoming_phone_number_id

                , analyticsTime.id analyticsTime_id
                , MAX(analyticsTime.time) max_analytics_time

                , combo.id combo_id
                , combo.name combo_name

                , page.id page_id
                , page.page_url page_url

                , referrerSource.id referrerSource_id
                , referrerSource.url referrer_source


                , campaign.id campaign_id
                , campaign.name campaign_name

                , advertiser.id advertiser_id
                , advertiser.name advertiser_name



            FROM
                visitor_analytics visitorAnalytics

                INNER JOIN analytics_time analyticsTime ON visitorAnalytics.analytics_time_id = analyticsTime.id
                INNER JOIN analytics_phone_number analyticsPhoneNumber ON visitorAnalytics.id = analyticsPhoneNumber.visitor_analytics_id
                INNER JOIN combo combo ON visitorAnalytics.combo_id = combo.id
                INNER JOIN page page ON combo.page_id = page.id

                INNER JOIN campaign campaign ON page.campaign_id = campaign.id
                LEFT JOIN advertiser advertiser ON campaign.advertiser_id = advertiser.id
                LEFT JOIN company company ON advertiser.company_id = company.id


                LEFT JOIN referrer_source referrerSource ON visitorAnalytics.referrer_source_id = referrerSource.id

            WHERE campaign.id IS NOT NULL
                AND advertiser.id = 1

            GROUP BY analyticsPhoneNumber.twilio_incoming_phone_number_id
     ) tmpAnalytics
      where
            tmpAnalytics.twilio_incoming_phone_number_id = 479
        AND tmpAnalytics.max_analytics_time <= 1301592865;
;



/* 4 */
    SELECT SQL_CALC_FOUND_ROWS
          twilioIncomingCall.id twilioIncomingCall_id
        , twilioIncomingCall.caller_country twilioIncomingCall_caller_country
        , twilioIncomingCall.caller_zip twilioIncomingCall_caller_zip
        , twilioIncomingCall.caller_state twilioIncomingCall_caller_state
        , twilioIncomingCall.call_status twilioIncomingCall_call_status
        , twilioIncomingCall.price twilioIncomingCall_price

        , twilioIncomingPhoneNumber.id twilioIncomingPhoneNumber_id
        , twilioIncomingPhoneNumber.phone_number twilioIncomingPhoneNumber_phone_number

        , twilioCallerPhoneNumber.id twilioCallerPhoneNumber_id
        , twilioCallerPhoneNumber.phone_number twilioCallerPhoneNumber_phone_number
        , twilioCallerPhoneNumber.is_blocked twilioCallerPhoneNumber_is_blocked

        , twilioCity.id twilioCity_id
        , twilioCity.name twilioCity_name

        , callResult.id callResult_id
        , callResult.first_name callResult_first_name
        , callResult.last_name callResult_last_name
        , callResult.sex callResult_sex
        , callResult.address callResult_address
        , callResult.zip callResult_zip
        , callResult.phone callResult_phone
        , callResult.revenue callResult_revenue
        , callResult.rating callResult_rating

        , twilioCallRecording.id twilioCallRecording_id
        , twilioCallRecording.file_url twilioCallRecording_file_url

        , tmpAnalytics.id tmpAnalytics_id
        , tmpAnalytics.traffic_source tmpAnalytics_traffic_source
        , tmpAnalytics.keyword tmpAnalytics_keyword
        , tmpAnalytics.is_organic tmpAnalytics_is_organic
        , tmpAnalytics.combo_id tmpAnalytics_combo_id
        , tmpAnalytics.combo_name tmpAnalytics_combo_name
        , tmpAnalytics.page_id tmpAnalytics_page_id
        , tmpAnalytics.page_url tmpAnalytics_page_url
        , tmpAnalytics.referrer_source tmpAnalytics_referrer_source

        , tmpUnionCall.id tmpUnionCall_id
        , tmpUnionCall.called_number tmpUnionCall_called_number
        , tmpUnionCall.start_time tmpUnionCall_start_time
        , tmpUnionCall.end_time tmpUnionCall_end_time
        , tmpUnionCall.duration tmpUnionCall_duration
        , tmpUnionCall.is_redialed_call tmpUnionCall_is_redialed_call

        , tmpAnalytics.max_analytics_time tmpAnalytics_max_analytics_time
        , twilioIncomingCall.start_time twilioIncomingCall_start_time

        , tmpAnalytics.campaign_id tmpAnalytics_campaign_id
        , tmpAnalytics.campaign_name tmpAnalytics_campaign_name
        , tmpAnalytics.advertiser_id tmpAnalytics_advertiser_id
        , tmpAnalytics.advertiser_name tmpAnalytics_advertiser_name



    FROM
        twilio_incoming_call twilioIncomingCall

        INNER JOIN twilio_incoming_phone_number twilioIncomingPhoneNumber ON twilioIncomingCall.twilio_incoming_phone_number_id = twilioIncomingPhoneNumber.id
        INNER JOIN twilio_caller_phone_number twilioCallerPhoneNumber ON twilioIncomingCall.twilio_caller_phone_number_id = twilioCallerPhoneNumber.id

        LEFT JOIN twilio_city twilioCity ON twilioIncomingCall.twilio_city_id = twilioCity.id
        LEFT JOIN call_result callResult ON twilioIncomingCall.id = callResult.twilio_incoming_call_id
        LEFT JOIN twilio_call_recording twilioCallRecording ON twilioIncomingCall.id = twilioCallRecording.twilio_incoming_call_id

        LEFT JOIN (
            SELECT
                  visitorAnalytics.id id
                , visitorAnalytics.traffic_source traffic_source
                , visitorAnalytics.keyword keyword
                , visitorAnalytics.is_organic is_organic

                , analyticsPhoneNumber.id analyticsPhoneNumber_id
                , analyticsPhoneNumber.twilio_incoming_phone_number_id twilio_incoming_phone_number_id

                , analyticsTime.id analyticsTime_id
                , MAX(analyticsTime.time) max_analytics_time

                , combo.id combo_id
                , combo.name combo_name

                , page.id page_id
                , page.page_url page_url

                , referrerSource.id referrerSource_id
                , referrerSource.url referrer_source


                , campaign.id campaign_id
                , campaign.name campaign_name

                , advertiser.id advertiser_id
                , advertiser.name advertiser_name



            FROM
                visitor_analytics visitorAnalytics

                INNER JOIN analytics_time analyticsTime ON visitorAnalytics.analytics_time_id = analyticsTime.id
                INNER JOIN analytics_phone_number analyticsPhoneNumber ON visitorAnalytics.id = analyticsPhoneNumber.visitor_analytics_id
                INNER JOIN combo combo ON visitorAnalytics.combo_id = combo.id
                INNER JOIN page page ON combo.page_id = page.id

                INNER JOIN campaign campaign ON page.campaign_id = campaign.id
                LEFT JOIN advertiser advertiser ON campaign.advertiser_id = advertiser.id
                LEFT JOIN company company ON advertiser.company_id = company.id

                LEFT JOIN referrer_source referrerSource ON visitorAnalytics.referrer_source_id = referrerSource.id

            WHERE campaign.id IS NOT NULL
                AND advertiser.id = 1

            GROUP BY analyticsPhoneNumber.twilio_incoming_phone_number_id

        ) tmpAnalytics ON
                tmpAnalytics.twilio_incoming_phone_number_id = twilioIncomingCall.twilio_incoming_phone_number_id
            AND tmpAnalytics.max_analytics_time <= twilioIncomingCall.start_time

        LEFT JOIN (
            SELECT *
            FROM
            (
                (
                        SELECT
                              twilioIncomingCall.id id
                            , NULL called_number
                            , twilioIncomingCall.start_time start_time
                            , twilioIncomingCall.end_time end_time
                            , twilioIncomingCall.duration duration
                            , 0 is_redialed_call
                        FROM
                            twilio_incoming_call twilioIncomingCall
                )
                UNION
                (
                        SELECT
                              twilioRedialedCall.twilio_incoming_call_id id
                            , twilioRedialedCall.called_number called_number
                            , twilioRedialedCall.start_time start_time
                            , twilioRedialedCall.end_time end_time
                            , twilioRedialedCall.duration duration
                            , 1 is_redialed_call
                        FROM
                            twilio_redialed_call twilioRedialedCall
                )
                ORDER BY id, is_redialed_call DESC
            ) tmpUnionCallGrouped
            GROUP BY id
        ) tmpUnionCall ON
            tmpUnionCall.id = twilioIncomingCall.id

    WHERE
        (
            twilioIncomingCall.start_time BETWEEN
                     tmpAnalytics.max_analytics_time
                AND (tmpAnalytics.max_analytics_time + 7200)
            OR tmpAnalytics.id IS NULL
        )

    ORDER BY twilioIncomingCall.start_time DESC
;

select distinct
    tic.id as call_id, tic.twilio_incoming_phone_number_id, tic.start_time as call_start,
    va.id as visitor_id, /*va.keyword, va.combo_id,*/
    max(ant.time) as max_analytics_time,
    ani.ip
from
    twilio_incoming_call tic, visitor_analytics va
    inner join analytics_phone_number apn on va.id = apn.visitor_analytics_id
    inner join analytics_time ant on ant.id = va.analytics_time_id
    inner join analytics_ip ani on ani.id = va.analytics_ip_id
where
    apn.twilio_incoming_phone_number_id = tic.twilio_incoming_phone_number_id
  and
    tic.start_time > ant.time
  and
    tic.start_time - 7200 <= ant.time
group by
    ani.ip
order by
    tic.start_time desc, max_analytics_time desc
limit 20

select distinct
    tic.id, tic.start_time,
    analytics.*,
    (tic.start_time - analytics.max_analytics_time) / 60 as minutes,
    trc.id
from
    twilio_incoming_call tic
    left join (
        select distinct
            atic.id as call_id,
            va.id as visitor_id, /*va.keyword, va.combo_id,*/
            max(ant.time) as max_analytics_time,
            ani.ip
        from
            twilio_incoming_call atic, visitor_analytics va
            inner join analytics_phone_number apn on va.id = apn.visitor_analytics_id
            inner join analytics_time ant on ant.id = va.analytics_time_id
            inner join analytics_ip ani on ani.id = va.analytics_ip_id
        where
            apn.twilio_incoming_phone_number_id = atic.twilio_incoming_phone_number_id
          and
            atic.start_time > ant.time
          and
            atic.start_time - 7200 <= ant.time
          and
            atic.id in (4859, 4858, 4857, 4856, 4855, 4854, 4853, 4852, 4851, 4850)
        group by
            ani.ip
    ) analytics on tic.id = analytics.call_id
    left join twilio_redialed_call trc on trc.twilio_incoming_call_id = tic.id
order by
    tic.start_time desc, analytics.max_analytics_time desc
limit 20

select distinct
    tic.*,
    analytics.*,
    trc.*,
    cr.*
from
    twilio_incoming_call tic
    left join (
        select distinct
            atic.id as call_id,
            ad.name as advertiser_name,
            cp.name as campaign_name,
            pg.page_url,
            cm.name as combo_name,
            va.*,
            rs.url,
            max(ant.time) as max_analytics_time
        from
            twilio_incoming_call atic, visitor_analytics va
            inner join analytics_phone_number apn on va.id = apn.visitor_analytics_id
            inner join analytics_time ant on ant.id = va.analytics_time_id
            inner join combo cm on cm.id = va.combo_id
            inner join page pg on pg.id = cm.page_id
            inner join campaign cp on cp.id = pg.campaign_id
            inner join advertiser ad on ad.id = cp.advertiser_id
            left join referrer_source rs on rs.id = va.referrer_source_id
        where
            apn.twilio_incoming_phone_number_id = atic.twilio_incoming_phone_number_id
          and
            atic.start_time > ant.time
          and
            atic.start_time - 7200 <= ant.time
          and
            atic.id in (
                4859, 4858, 4857, 4856, 4855, 4854, 4853, 4852, 4851, 4850,
                4849, 4848, 4847, 4846, 4845, 4844, 4843, 4842, 4841, 4840
            )
        group by
            atic.id
    ) analytics on tic.id = analytics.call_id
    left join twilio_redialed_call trc on trc.twilio_incoming_call_id = tic.id
    left join call_result cr on cr.twilio_incoming_call_id = tic.id
where
    tic.id in (
        4859, 4858, 4857, 4856, 4855, 4854, 4853, 4852, 4851, 4850,
        4849, 4848, 4847, 4846, 4845, 4844, 4843, 4842, 4841, 4840
    )
order by
    tic.start_time desc

select distinct
    tic.id,
    tic.caller_country,
    tic.caller_zip,
    tic.caller_state,
    tic.call_status,
    tic.price,
    tipn.id as twilioIncomingPhoneNumber_id,
    tipn.phone_number as twilioIncomingPhoneNumber_phone_number,
    tcpn.id as twilioCallerPhoneNumber_id,
    tcpn.phone_number as twilioCallerPhoneNumber_phone_number,
    tcpn.is_blocked as twilioCallerPhoneNumber_is_blocked,
    tc.id as twilioCity_id,
    tc.name as twilioCity_name,
    cr.id as callResult_id,
    cr.first_name as callResult_first_name,
    cr.last_name as callResult_last_name,
    cr.sex as callResult_sex,
    cr.address as callResult_address,
    cr.zip as callResult_zip,
    cr.phone as callResult_phone,
    cr.revenue as callResult_revenue,
    cr.rating as callResult_rating,
    crd.id as twilioCallRecording_id,
    crd.file_url as twilioCallRecording_file_url,
    analytics.id as tmpAnalytics_id,
    analytics.traffic_source as tmpAnalytics_traffic_source,
    analytics.keyword as tmpAnalytics_keyword,
    analytics.is_organic as tmpAnalytics_is_organic,
    analytics.combo_id as tmpAnalytics_combo_id,
    analytics.combo_name as tmpAnalytics_combo_name,
    analytics.page_id as tmpAnalytics_page_id,
    analytics.page_url as tmpAnalytics_page_url,
    analytics.referrer_source as tmpAnalytics_referrer_source,
    trc.id  as tmpUnionCall_id,
    if(trc.id is not null, trc.called_number, 'Undefined') as tmpUnionCall_called_number,
    if(trc.id is not null, trc.start_time, tic.start_time) as tmpUnionCall_start_time,
    if(trc.id is not null, trc.end_time, tic.end_time) as tmpUnionCall_end_time,
    if(trc.id is not null, trc.duration, tic.duration) as tmpUnionCall_duration,
    if(trc.id is not null, 1, 0) as tmpUnionCall_is_redialed_call,
    analytics.max_analytics_time as tmpAnalytics_max_analytics_time,
    tic.start_time,
    analytics.campaign_id as tmpAnalytics_campaign_id,
    analytics.campaign_name as tmpAnalytics_campaign_name,
    analytics.advertiser_id as tmpAnalytics_advertiser_id,
    analytics.advertiser_name as tmpAnalytics_advertiser_name
from
    twilio_incoming_call tic
    inner join twilio_incoming_phone_number tipn on tic.twilio_incoming_phone_number_id = tipn.id
    left join twilio_caller_phone_number tcpn on tic.twilio_caller_phone_number_id = tcpn.id
    left join twilio_city tc on tic.twilio_city_id = tc.id
    left join twilio_redialed_call trc on trc.twilio_incoming_call_id = tic.id
    left join call_result cr on cr.twilio_incoming_call_id = tic.id
    left join twilio_call_recording crd on crd.twilio_incoming_call_id = tic.id
    left join (
        select distinct
            atic.id as call_id,
            ad.id as advertiser_id,
            ad.name as advertiser_name,
            cp.id as campaign_id,
            cp.name as campaign_name,
            pg.id as page_id,
            pg.page_url as page_url,
            cm.name as combo_name,
            va.*,
            rs.url as referrer_source,
            max(ant.time) as max_analytics_time
        from
            twilio_incoming_call atic, visitor_analytics va
            inner join analytics_phone_number apn on va.id = apn.visitor_analytics_id
            inner join analytics_time ant on ant.id = va.analytics_time_id
            inner join combo cm on cm.id = va.combo_id
            inner join page pg on pg.id = cm.page_id
            inner join campaign cp on cp.id = pg.campaign_id
            inner join advertiser ad on ad.id = cp.advertiser_id
            left join referrer_source rs on rs.id = va.referrer_source_id
        where
            apn.twilio_incoming_phone_number_id = atic.twilio_incoming_phone_number_id
          and
            atic.start_time > ant.time
          and
            atic.start_time - 7200 <= ant.time
          and
            atic.id in (
                4859, 4858, 4857, 4856, 4855, 4854, 4853, 4852, 4851, 4850,
                4849, 4848, 4847, 4846, 4845, 4844, 4843, 4842, 4841, 4840
            )
        group by
            atic.id
    ) analytics on tic.id = analytics.call_id
where
    tic.id in (
        4859, 4858, 4857, 4856, 4855, 4854, 4853, 4852, 4851, 4850,
        4849, 4848, 4847, 4846, 4845, 4844, 4843, 4842, 4841, 4840
    )
order by
    tic.start_time desc


        SELECT DISTINCT
            max_analytics.call_id,
            ad.id AS advertiser_id,
            ad.name AS advertiser_name,
            cp.id AS campaign_id,
            cp.name AS campaign_name,
            pg.id AS page_id,
            pg.page_url AS page_url,
            va.id AS tmpAnalytics_id,
            va.traffic_source AS tmpAnalytics_traffic_source,
            va.keyword AS tmpAnalytics_keyword,
            va.is_organic AS tmpAnalytics_is_organic,
            va.combo_id AS tmpAnalytics_combo_id,
            cm.name AS combo_name,
            rs.url AS referrer_source,
            va.analytics_time AS max_analytics_time
        FROM (
            SELECT DISTINCT
                atic.id AS call_id,
                max(mva.analytics_time) AS max_analytics_time
            FROM
                twilio_incoming_call atic,
                visitor_analytics mva
                INNER JOIN analytics_phone_number apn ON mva.id = apn.visitor_analytics_id
            WHERE
                    apn.twilio_incoming_phone_number_id = atic.twilio_incoming_phone_number_id
                AND atic.start_time > mva.analytics_time
                AND atic.start_time - 7200 <= mva.analytics_time
            GROUP BY
                atic.id
        ) max_analytics INNER JOIN visitor_analytics va ON va.analytics_time = max_analytics.max_analytics_time
            INNER JOIN combo cm ON cm.id = va.combo_id
            INNER JOIN page pg ON pg.id = cm.page_id
            INNER JOIN campaign cp ON cp.id = pg.campaign_id
            INNER JOIN advertiser ad ON ad.id = cp.advertiser_id
            LEFT JOIN referrer_source rs ON rs.id = va.referrer_source_id
        ORDER BY
            max_analytics.call_id desc
        LIMIT 30
