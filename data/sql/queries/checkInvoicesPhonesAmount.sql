/*
Scripts to show amounts of phoneNumbers of each type:
1). for each advertiser,
2). for each company : owner/noOwner will be shown separately.
3). for each company : where company is owner or noOwner together.
4). for each company : where company is owner only.
*/


/*1*/
SELECT
     com.level      AS parent_company_level
    , com.id        AS parent_company_name
    , com.name      AS parent_company_name
    , adv.id        AS advertiser_id
    , adv.name      AS advertiser_name
    , IF (t2.phone_number_type IS NOT NULL, t2.phone_number_type, 'local, toll free') AS phone_number_type
    , COUNT(t.id)   AS phones_amount
FROM
    advertiser adv
    LEFT JOIN company com ON com.id = adv.company_id
    LEFT JOIN advertiser_incoming_number_pool a ON a.advertiser_id = adv.id
    LEFT JOIN twilio_incoming_phone_number t ON a.twilio_incoming_phone_number_id = t.id
    LEFT JOIN twilio_local_area_code t2 ON t.twilio_local_area_code_id = t2.id
GROUP BY adv.id, t2.phone_number_type
ORDER BY com.level, adv.id




/*2*/
SELECT
      com.level     AS company_level
    , com.id        AS company_id
    , com.name      AS company_name
    , IF (t2.phone_number_type IS NOT NULL, t2.phone_number_type, 'local, toll free') AS phone_number_type
    , IF (c.is_owner = 1, 'is owner', '') AS is_owner
    , IF (c.is_owner = 0, 'is NOT owner', '') AS is_not_owner
    , COUNT(t.id)   AS phones_amount
FROM
    company com
    LEFT JOIN company_incoming_number_pool c ON c.company_id = com.id
    LEFT JOIN twilio_incoming_phone_number t ON c.twilio_incoming_phone_number_id = t.id
    LEFT JOIN twilio_local_area_code t2 ON t.twilio_local_area_code_id = t2.id
GROUP BY com.id, t2.phone_number_type, c.is_owner
ORDER BY com.level, com.id




/*3*/
SELECT
      com.level     AS company_level
    , com.id        AS company_id
    , com.name      AS company_name
    , IF (t2.phone_number_type IS NOT NULL, t2.phone_number_type, 'local, toll free') AS phone_number_type
    , COUNT(t.id)   AS phones_amount
FROM
    company com
    LEFT JOIN company_incoming_number_pool c ON c.company_id = com.id
    LEFT JOIN twilio_incoming_phone_number t ON c.twilio_incoming_phone_number_id = t.id
    LEFT JOIN twilio_local_area_code t2 ON t.twilio_local_area_code_id = t2.id
GROUP BY com.id, t2.phone_number_type
ORDER BY com.level, com.id




/*4*/
SELECT
      com.level     AS company_level
    , com.id        AS company_id
    , com.name      AS company_name
    , IF (t2.phone_number_type IS NOT NULL, t2.phone_number_type, 'local, toll free') AS phone_number_type
    , COUNT(t.id)   AS phones_amount
FROM
    company com
    LEFT JOIN company_incoming_number_pool c ON c.company_id = com.id
    LEFT JOIN twilio_incoming_phone_number t ON c.twilio_incoming_phone_number_id = t.id
    LEFT JOIN twilio_local_area_code t2 ON t.twilio_local_area_code_id = t2.id
WHERE
    c.id IS NULL
    OR c.is_owner = 1
GROUP BY com.id, t2.phone_number_type
ORDER BY com.level, com.id