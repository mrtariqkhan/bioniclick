-- company filtering by agency for bionic
-- old
SELECT 
    c.id AS c__id, c.name AS c__name, c.url AS c__url, c.default_phone_number AS c__default_phone_number, c.merchant_payment_profile_id AS c__merchant_payment_profile_id, c.customer_payment_profile_id AS c__customer_payment_profile_id, c.credit AS c__credit, c.skin AS c__skin, c.package_id AS c__package_id, c.was_deleted AS c__was_deleted, c.lft AS c__lft, c.rgt AS c__rgt, c.level AS c__level
FROM company c 
    WHERE (
        EXISTS (
            SELECT 
                c2.id AS c2__id 
            FROM 
                company c2, company c3 
            WHERE (
                c2.level = 1 
                AND c3.level = 2 
                AND c3.name LIKE '%Ag%' 
                AND c3.lft BETWEEN c2.lft AND c2.rgt 
                AND c.id = c3.id
            )
        )
    ) 
ORDER BY c.level, c.name ASC

--new
SELECT 
    c.id AS c__id, c.name AS c__name, c.url AS c__url, c.default_phone_number AS c__default_phone_number, c.merchant_payment_profile_id AS c__merchant_payment_profile_id, c.customer_payment_profile_id AS c__customer_payment_profile_id, c.credit AS c__credit, c.skin AS c__skin, c.package_id AS c__package_id, c.was_deleted AS c__was_deleted, c.lft AS c__lft, c.rgt AS c__rgt, c.level AS c__level
FROM company c 
    WHERE (
        c.level = 2 
        AND c.name LIKE '%Ag%'                
    )
ORDER BY c.level, c.name ASC


-- company filtering by reseller for bionic. right query:
--new
SELECT 
    c.id AS c__id, c.name AS c__name, c.url AS c__url, c.default_phone_number AS c__default_phone_number, c.merchant_payment_profile_id AS c__merchant_payment_profile_id, c.customer_payment_profile_id AS c__customer_payment_profile_id, c.credit AS c__credit, c.skin AS c__skin, c.package_id AS c__package_id, c.was_deleted AS c__was_deleted, c.lft AS c__lft, c.rgt AS c__rgt, c.level AS c__level
FROM 
    company c 
WHERE (
    EXISTS (
        SELECT 
            c2.id AS c2__id 
        FROM 
            company c2, company c3 
        WHERE (
            c2.level = 1
            AND c3.level = 2
            AND c2.name LIKE '%Re%'
            AND (
                c3.lft BETWEEN c2.lft AND c2.rgt                
            )
            AND (c.id = c3.id OR c.id = c2.id)
        )
    )
    OR EXISTS (
        SELECT 
            c2.id AS c2__id 
        FROM 
            company c2
        WHERE (
            c2.level = 1            
            AND c2.name LIKE '%Re%'
            AND c2.lft + 1 = c2.rgt
            AND c.id = c2.id
        )
    )
) ORDER BY c.level, c.name ASC LIMIT 10

-- company filtering by reseller for bionic. right query:

SELECT 
    c.id AS c__id, c.name AS c__name, c.url AS c__url, c.default_phone_number AS c__default_phone_number, c.merchant_payment_profile_id AS c__merchant_payment_profile_id, c.customer_payment_profile_id AS c__customer_payment_profile_id, c.credit AS c__credit, c.skin AS c__skin, c.package_id AS c__package_id, c.was_deleted AS c__was_deleted, c.lft AS c__lft, c.rgt AS c__rgt, c.level AS c__level 
FROM 
    company c 
WHERE (
    EXISTS (
        SELECT 
            c2.id AS c2__id 
        FROM 
            company c2, company c3 
        WHERE (
            c2.level = 1 
            AND c3.level = 2 
            AND c2.name LIKE '%Re%' 
            AND c3.name LIKE '%Ag%' 
            AND c3.lft BETWEEN c2.lft AND c2.rgt 
            AND c.id = c3.id
        )
    )
) ORDER BY c.level, c.name ASC