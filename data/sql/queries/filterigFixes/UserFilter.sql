-- users filtering by agency (for super admin). old query:
SELECT 
    s.id AS s__id, s.first_name AS s__first_name, s.last_name AS s__last_name, s.email_address AS s__email_address, s.username AS s__username, s.algorithm AS s__algorithm, s.salt AS s__salt, s.password AS s__password, s.is_active AS s__is_active, s.is_super_admin AS s__is_super_admin, s.last_login AS s__last_login, s.was_deleted AS s__was_deleted, s.created_at AS s__created_at, s.updated_at AS s__updated_at,
    s2.id AS s2__id, s2.user_id AS s2__user_id, s2.email AS s2__email, s2.validate AS s2__validate, s2.timezone_id AS s2__timezone_id, s2.advertiser_id AS s2__advertiser_id, s2.company_id AS s2__company_id, s2.customer_profile_id AS s2__customer_profile_id, s2.created_at AS s2__created_at, s2.updated_at AS s2__updated_at,
    a.id AS a__id, a.name AS a__name, a.url AS a__url, a.company_id AS a__company_id, a.default_phone_number AS a__default_phone_number, a.customer_payment_profile_id AS a__customer_payment_profile_id, a.credit AS a__credit, a.package_id AS a__package_id, a.was_deleted AS a__was_deleted, a.created_at AS a__created_at, a.updated_at AS a__updated_at,
    c.id AS c__id, c.name AS c__name, c.url AS c__url, c.default_phone_number AS c__default_phone_number, c.merchant_payment_profile_id AS c__merchant_payment_profile_id, c.customer_payment_profile_id AS c__customer_payment_profile_id, c.credit AS c__credit, c.skin AS c__skin, c.package_id AS c__package_id, c.was_deleted AS c__was_deleted, c.lft AS c__lft, c.rgt AS c__rgt, c.level AS c__level,
    c2.id AS c2__id, c2.name AS c2__name, c2.url AS c2__url, c2.default_phone_number AS c2__default_phone_number, c2.merchant_payment_profile_id AS c2__merchant_payment_profile_id, c2.customer_payment_profile_id AS c2__customer_payment_profile_id, c2.credit AS c2__credit, c2.skin AS c2__skin, c2.package_id AS c2__package_id, c2.was_deleted AS c2__was_deleted, c2.lft AS c2__lft, c2.rgt AS c2__rgt, c2.level AS c2__level
FROM 
    sf_guard_user s 
LEFT JOIN 
    sf_guard_user_profile s2 ON s.id = s2.user_id 
LEFT JOIN 
    advertiser a ON s2.advertiser_id = a.id 
LEFT JOIN 
    company c ON s2.company_id = c.id 
LEFT JOIN 
    company c2 ON a.company_id = c2.id 
WHERE (
    EXISTS (
        SELECT 
            c3.id AS c3__id 
        FROM 
            company c3, company c4 
        WHERE (
            c3.level = 1 
            AND c4.level = 2 
            AND c4.name LIKE '%Ag%' 
            AND c4.lft BETWEEN c3.lft AND c3.rgt 
            AND (c.id IS NULL OR (c.id IS NOT NULL AND c.id = c4.id)) 
            AND (c2.id IS NULL OR (c2.id IS NOT NULL AND c2.id = c4.id))
        )
    )
) 
ORDER BY s.first_name ASC, s.last_name ASC

-- users filtering by reseller (for super admin). old query:

SELECT 
    s.id AS s__id, s.first_name AS s__first_name, s.last_name AS s__last_name, s.email_address AS s__email_address, s.username AS s__username, s.algorithm AS s__algorithm, s.salt AS s__salt, s.password AS s__password, s.is_active AS s__is_active, s.is_super_admin AS s__is_super_admin, s.last_login AS s__last_login, s.was_deleted AS s__was_deleted, s.created_at AS s__created_at, s.updated_at AS s__updated_at,
    s2.id AS s2__id, s2.user_id AS s2__user_id, s2.email AS s2__email, s2.validate AS s2__validate, s2.timezone_id AS s2__timezone_id, s2.advertiser_id AS s2__advertiser_id, s2.company_id AS s2__company_id, s2.customer_profile_id AS s2__customer_profile_id, s2.created_at AS s2__created_at, s2.updated_at AS s2__updated_at,
    a.id AS a__id, a.name AS a__name, a.url AS a__url, a.company_id AS a__company_id, a.default_phone_number AS a__default_phone_number, a.customer_payment_profile_id AS a__customer_payment_profile_id, a.credit AS a__credit, a.package_id AS a__package_id, a.was_deleted AS a__was_deleted, a.created_at AS a__created_at, a.updated_at AS a__updated_at,
    c.id AS c__id, c.name AS c__name, c.url AS c__url, c.default_phone_number AS c__default_phone_number, c.merchant_payment_profile_id AS c__merchant_payment_profile_id, c.customer_payment_profile_id AS c__customer_payment_profile_id, c.credit AS c__credit, c.skin AS c__skin, c.package_id AS c__package_id, c.was_deleted AS c__was_deleted, c.lft AS c__lft, c.rgt AS c__rgt, c.level AS c__level,
    c2.id AS c2__id, c2.name AS c2__name, c2.url AS c2__url, c2.default_phone_number AS c2__default_phone_number, c2.merchant_payment_profile_id AS c2__merchant_payment_profile_id, c2.customer_payment_profile_id AS c2__customer_payment_profile_id, c2.credit AS c2__credit, c2.skin AS c2__skin, c2.package_id AS c2__package_id, c2.was_deleted AS c2__was_deleted, c2.lft AS c2__lft, c2.rgt AS c2__rgt, c2.level AS c2__level
FROM 
    sf_guard_user s
LEFT JOIN 
    sf_guard_user_profile s2 ON s.id = s2.user_id
LEFT JOIN 
    advertiser a ON s2.advertiser_id = a.id
LEFT JOIN 
    company c ON s2.company_id = c.id
LEFT JOIN
    company c2 ON a.company_id = c2.id 
WHERE (
    EXISTS (
        SELECT 
            c3.id AS c3__id
        FROM 
            company c3, company c4 
        WHERE (
            c3.level = 1 
            AND c4.level = 2 
            AND c3.name LIKE '%Re%' 
            AND (c4.lft BETWEEN c3.lft AND c3.rgt OR c3.lft + 1 = c3.rgt) 
            AND (c.id IS NULL OR (c.id IS NOT NULL AND (c.id = c4.id OR c.id = c3.id))) 
            AND (c2.id IS NULL OR (c2.id IS NOT NULL AND (c2.id = c4.id OR c2.id = c3.id)))
        )
    )
) ORDER BY s.first_name ASC, s.last_name ASC

-- fixed query:

SELECT 
    s.id AS s__id, s.first_name AS s__first_name, s.last_name AS s__last_name, s.email_address AS s__email_address, s.username AS s__username, s.algorithm AS s__algorithm, s.salt AS s__salt, s.password AS s__password, s.is_active AS s__is_active, s.is_super_admin AS s__is_super_admin, s.last_login AS s__last_login, s.was_deleted AS s__was_deleted, s.created_at AS s__created_at, s.updated_at AS s__updated_at,
    s2.id AS s2__id, s2.user_id AS s2__user_id, s2.email AS s2__email, s2.validate AS s2__validate, s2.timezone_id AS s2__timezone_id, s2.advertiser_id AS s2__advertiser_id, s2.company_id AS s2__company_id, s2.customer_profile_id AS s2__customer_profile_id, s2.created_at AS s2__created_at, s2.updated_at AS s2__updated_at,
    a.id AS a__id, a.name AS a__name, a.url AS a__url, a.company_id AS a__company_id, a.default_phone_number AS a__default_phone_number, a.customer_payment_profile_id AS a__customer_payment_profile_id, a.credit AS a__credit, a.package_id AS a__package_id, a.was_deleted AS a__was_deleted, a.created_at AS a__created_at, a.updated_at AS a__updated_at,
    c.id AS c__id, c.name AS c__name, c.url AS c__url, c.default_phone_number AS c__default_phone_number, c.merchant_payment_profile_id AS c__merchant_payment_profile_id, c.customer_payment_profile_id AS c__customer_payment_profile_id, c.credit AS c__credit, c.skin AS c__skin, c.package_id AS c__package_id, c.was_deleted AS c__was_deleted, c.lft AS c__lft, c.rgt AS c__rgt, c.level AS c__level,
    c2.id AS c2__id, c2.name AS c2__name, c2.url AS c2__url, c2.default_phone_number AS c2__default_phone_number, c2.merchant_payment_profile_id AS c2__merchant_payment_profile_id, c2.customer_payment_profile_id AS c2__customer_payment_profile_id, c2.credit AS c2__credit, c2.skin AS c2__skin, c2.package_id AS c2__package_id, c2.was_deleted AS c2__was_deleted, c2.lft AS c2__lft, c2.rgt AS c2__rgt, c2.level AS c2__level
FROM 
    sf_guard_user s
LEFT JOIN 
    sf_guard_user_profile s2 ON s.id = s2.user_id
LEFT JOIN 
    advertiser a ON s2.advertiser_id = a.id
LEFT JOIN 
    company c ON s2.company_id = c.id
LEFT JOIN
    company c2 ON a.company_id = c2.id 
WHERE (
    EXISTS (
        SELECT 
            c3.id AS c3__id
        FROM 
            company c3, company c4 
        WHERE (
            c3.level = 1 
            AND c4.level = 2 
            AND c3.name LIKE '%Re%' 
            AND (c4.lft BETWEEN c3.lft AND c3.rgt) 
            AND (c.id IS NULL OR (c.id IS NOT NULL AND (c.id = c4.id OR c.id = c3.id))) 
            AND (c2.id IS NULL OR (c2.id IS NOT NULL AND (c2.id = c4.id OR c2.id = c3.id)))
        )
    )
    OR EXISTS (
        SELECT 
            c3.id AS c3__id
        FROM 
            company c3
        WHERE (
            c3.level = 1
            AND c3.name LIKE '%Re%' 
            AND (c3.lft + 1 = c3.rgt) 
            AND (c.id IS NULL OR (c.id IS NOT NULL AND c.id = c3.id)) 
            AND (c2.id IS NULL OR (c2.id IS NOT NULL AND c2.id = c3.id))
        )
    )
) ORDER BY s.first_name ASC, s.last_name ASC

SELECT 
    s.id AS s__id, s.first_name AS s__first_name, s.last_name AS s__last_name, s.email_address AS s__email_address, s.username AS s__username, s.algorithm AS s__algorithm, s.salt AS s__salt, s.password AS s__password, s.is_active AS s__is_active, s.is_super_admin AS s__is_super_admin, s.last_login AS s__last_login, s.was_deleted AS s__was_deleted, s.created_at AS s__created_at, s.updated_at AS s__updated_at, s2.id AS s2__id, s2.user_id AS s2__user_id, s2.email AS s2__email, s2.validate AS s2__validate, s2.timezone_id AS s2__timezone_id, s2.advertiser_id AS s2__advertiser_id, s2.company_id AS s2__company_id, s2.customer_profile_id AS s2__customer_profile_id, s2.created_at AS s2__created_at, s2.updated_at AS s2__updated_at, a.id AS a__id, a.name AS a__name, a.url AS a__url, a.company_id AS a__company_id, a.default_phone_number AS a__default_phone_number, a.customer_payment_profile_id AS a__customer_payment_profile_id, a.credit AS a__credit, a.package_id AS a__package_id, a.was_deleted AS a__was_deleted, a.created_at AS a__created_at, a.updated_at AS a__updated_at, c.id AS c__id, c.name AS c__name, c.url AS c__url, c.default_phone_number AS c__default_phone_number, c.merchant_payment_profile_id AS c__merchant_payment_profile_id, c.customer_payment_profile_id AS c__customer_payment_profile_id, c.credit AS c__credit, c.skin AS c__skin, c.package_id AS c__package_id, c.was_deleted AS c__was_deleted, c.lft AS c__lft, c.rgt AS c__rgt, c.level AS c__level, c2.id AS c2__id, c2.name AS c2__name, c2.url AS c2__url, c2.default_phone_number AS c2__default_phone_number, c2.merchant_payment_profile_id AS c2__merchant_payment_profile_id, c2.customer_payment_profile_id AS c2__customer_payment_profile_id, c2.credit AS c2__credit, c2.skin AS c2__skin, c2.package_id AS c2__package_id, c2.was_deleted AS c2__was_deleted, c2.lft AS c2__lft, c2.rgt AS c2__rgt, c2.level AS c2__level 
FROM 
    sf_guard_user s LEFT JOIN sf_guard_user_profile s2 ON s.id = s2.user_id LEFT JOIN advertiser a ON s2.advertiser_id = a.id LEFT JOIN company c ON s2.company_id = c.id LEFT JOIN company c2 ON a.company_id = c2.id 
WHERE (
    EXISTS (
        SELECT 
            c3.id AS c3__id 
        FROM 
            company c3, company c4 
        WHERE (
            c3.level = 1 
            AND c4.level = 2 
            AND c3.name LIKE '%Re%' 
            AND (c.id IS NULL OR (c.id IS NOT NULL AND (c.id = c4.id OR c.id = c3.id)))
            AND (c2.id IS NULL OR (c2.id IS NOT NULL AND (c2.id = c4.id OR c2.id = c3.id)))
        )
    ) OR EXISTS (
        SELECT 
            c5.id AS c5__id 
        FROM 
            company c5 
        WHERE (
            c5.lft + 1 = c5.rgt 
            AND c5.level = 1 
            AND c5.name LIKE '%Re%' 
            AND (c.id IS NULL OR (c.id IS NOT NULL AND c.id = c5.id))
            AND (c2.id IS NULL OR (c2.id IS NOT NULL AND c2.id = c5.id))
        )
    )
) ORDER BY s.first_name ASC, s.last_name ASC