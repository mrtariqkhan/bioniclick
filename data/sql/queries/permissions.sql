SELECT
      ggp.group_id             AS group_id
    , gg.name           AS group_name
    , ggp.permission_id  AS permission_id
    , gp.name           AS permission_name
FROM
    sf_guard_group_permission AS ggp
    INNER JOIN sf_guard_group AS gg ON gg.id = ggp.group_id
    INNER JOIN sf_guard_permission AS gp ON ggp.permission_id = gp.id
ORDER BY
      ggp.group_id
    , ggp.permission_id