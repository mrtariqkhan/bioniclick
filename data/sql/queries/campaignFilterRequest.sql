-- for Agency and Reseller names filter
select cam.name cam_name, adv.name adv_name, com.name com_name
from
    campaign cam
inner join
    advertiser adv on cam.advertiser_id = adv.id
inner join
    company com on adv.company_id = com.id
where exists (
    select
        null
    from
        company resellers, company agencies
    where
        (resellers.level = 1)
        and (agencies.level = 2)
        and (agencies.lft BETWEEN resellers.lft and resellers.rgt)
        and (agencies.name LIKE '%Ag%')
        and (resellers.name LIKE '%Re%')
        and (com.id = agencies.id)
)

--for Agency name filter
select cam.name cam_name, adv.name adv_name, com.name com_name
from
    campaign cam
inner join
    advertiser adv on cam.advertiser_id = adv.id
inner join
    company com on adv.company_id = com.id
where exists (
    select
        null
    from
        company resellers, company agencies
    where
        (resellers.level = 1)
        and (agencies.level = 2)
        and (agencies.lft BETWEEN resellers.lft and resellers.rgt)
        and (agencies.name LIKE '%Ag%')
        and (com.id = agencies.id)
)

--for Reseller name filter
select cam.name cam_name, adv.name adv_name, com.name com_name
from
    campaign cam
inner join
    advertiser adv on cam.advertiser_id = adv.id
inner join
    company com on adv.company_id = com.id
where exists (
    select
        null
    from
        company resellers, company agencies
    where
        (resellers.level = 1)
        and (agencies.level = 2)
        and (agencies.lft BETWEEN resellers.lft and resellers.rgt or resellers.lft + 1 = resellers.rgt)
        and (resellers.name LIKE '%Re%')
        and (com.id = agencies.id or com.id = resellers.id)
)

/// New version of query:

select cam.name cam_name, adv.name adv_name, com.name com_name
from
    campaign cam
inner join
    advertiser adv on cam.advertiser_id = adv.id
inner join
    company com on adv.company_id = com.id
where exists (
    select
        null
    from
        company resellers, company agencies
    where
        (resellers.level = 1)
        and (agencies.level = 2)
        and (
            agencies.lft BETWEEN resellers.lft and resellers.rgt
            or (
                resellers.lft + 1 = resellers.rgt
                and exists (
                    select
                        null
                    from
                        campaign cam1
                    inner join
                        advertiser adv1 on cam1.advertiser_id = adv1.id
                    where
                        adv1.company_id = resellers.id
                )
            )
        )
        and (resellers.name LIKE '%Reseller2%')
        and (com.id = agencies.id or com.id = resellers.id)
)
