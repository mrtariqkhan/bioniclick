
INSERT INTO `twilio_account` (`id`, `sid`, `auth_token`, `friendly_name`, `status`, `created_at`, `updated_at`) VALUES
    (1, 'AC3657210a30ba1b3272221bffbd6d8ad6', '2c5963d6a97d08d979a0e0a57fcb60ad', 'Full account', 2, '2009-10-19 14:16:01', '2009-10-19 14:16:01');

INSERT INTO `twilio_country` (`id`, `name`, `created_at`, `updated_at`) VALUES
    (1, 'USA', '2011-05-19 08:10:57', '2011-05-19 08:10:57');

INSERT INTO `twilio_state` (`id`, `code`, `name`, `twilio_country_id`, `created_at`, `updated_at`) VALUES
    (54, 'TX', 'Texas', 1, '2011-04-04 16:46:32', '2011-04-04 16:46:22');

INSERT INTO `twilio_local_area_code` (`id`, `code`, `phone_number_type`, `twilio_state_id`, `created_at`, `updated_at`) VALUES
    (1, 832, 'local', 54, '2011-03-11 11:53:56', '2011-03-11 11:53:56'),
    (2, 281, 'local', 54, '2011-03-11 11:53:56', '2011-03-11 11:53:56');

INSERT INTO `twilio_incoming_phone_number` (`id`, `sid`, `twilio_account_id`, `url`, `http_method_type`, `phone_number`, `friendly_name`, `twilio_local_area_code_id`, `was_deleted`, `created_at`, `updated_at`) VALUES
-- campaign's number
    (1, 'PN8b06d776d7f5460e7e0ec1aff33eb304', 1, 'http://bionicclick.com/calls_forwarding/', 'POST', '8322624801', '832 number 1', 1, 1, '2009-10-19 14:16:02', '2011-07-15 06:43:21'),
-- campaign's number
    (2, 'PN8c28e516087f2c5d19ff4d32765707c2', 1, 'http://project3.bionicclick.com/backend_dev.php/incomingCalls/create', 'POST', '8322614585', '832 number 2', 1, 0, '2009-10-19 14:16:02', '2011-04-08 14:51:14'),
-- advertiser's number
    (3, 'PN3fd77db4e955f19157e2c2a24127a3c9', 1, 'http://project2.bionicclick.com/incomingCalls/create', 'POST', '8327353073', '832 number 3', 1, 1, '2009-10-19 14:16:02', '2011-07-15 06:43:25'),
-- Bionic's number    
    (4, 'PN35f8299a6c80a9b674800794d92a302b', 1, 'http://bionicclick.com/calls_forwarding/', 'POST', '2818165704', '281 number 1', 2, 1, '2009-10-19 14:16:02', '2011-07-15 06:43:21'),
-- Bionic's number    
    (5, 'PN70c309231d16f299101eb6869300786c', 1, 'http://project3.bionicclick.com/backend_dev.php/incomingCalls/create', 'POST', '2813050785', '281 number 2', 2, 0, '2009-10-19 14:16:02', '2011-04-19 07:00:00'),
-- Bionic's number    
    (6, 'PN207bae57484b0c93e5cb9c02d08815a4', 1, 'http://project2.bionicclick.com/incomingCalls/create', 'POST', '2813050784', '281 number 3', 2, 1, '2009-10-19 14:16:02', '2011-07-15 06:43:25'),
-- Bionic's number
    (7, 'PN7c4c46a9f471d6917867ec61e4a9e45e', 1, 'http://project2.bionicclick.com/incomingCalls/create', 'POST', '8323849264', '832 number 5', 1, 1, '2009-10-19 14:16:02', '2011-07-15 06:43:25'),
-- Bionic's number
    (8, 'PN04e2d9605d2c92d1257887c9fdbb392e', 1, 'http://project2.bionicclick.com/incomingCalls/create', 'POST', '8323849267', '832 number 6', 1, 1, '2009-10-19 14:16:02', '2011-07-15 06:43:25');

INSERT INTO `bionic_incoming_number_pool` (`id`, `twilio_incoming_phone_number_id`, `is_owner`, `created_at`, `updated_at`) VALUES
    (1, 1, 0, '2011-04-25 14:00:00', '2011-04-25 14:00:00'),
    (2, 2, 0, '2011-04-25 14:00:00', '2011-04-25 14:00:00'),
    (3, 3, 0, '2011-04-25 14:00:00', '2011-04-25 14:00:00'),
    (4, 4, 1, '2011-04-25 14:00:00', '2011-04-25 14:00:00'),
    (5, 5, 1, '2011-04-25 14:00:00', '2011-04-25 14:00:00'),
    (6, 6, 1, '2011-04-25 14:00:00', '2011-04-25 14:00:00'),
    (7, 7, 1, '2011-04-25 14:00:00', '2011-04-25 14:00:00'),
    (8, 8, 1, '2011-04-25 14:00:00', '2011-07-15 08:31:51');

INSERT INTO `company` (`id`, `name`, `url`, `default_phone_number`, `authorize_customer_profile_id`, `is_debtor`, `skin`, `package_id`, `was_deleted`, `lft`, `rgt`, `level`, `created_at`, `updated_at`) VALUES
    (1, 'Bionic Click', 'www.bionicclick.com', '7136232667', NULL, 0, '', NULL, 0, 1, 40, 0, '2011-03-27 21:41:11', '2011-08-25 01:49:18');

INSERT INTO `package` (`id`, `company_id`, `name`, `monthly_fee`, `toll_free_number_monthly_fee`, `local_number_monthly_fee`, `toll_free_number_per_minute`, `local_number_per_minute`, `was_deleted`, `created_at`, `updated_at`) VALUES
    (1, 1, 'Default Bionic package', '0.000', '4.000', '2.000', '0.600', '0.200', 0, '2011-04-28 17:52:11', '2011-04-28 17:52:11');

INSERT INTO `advertiser` (`id`, `name`, `url`, `company_id`, `default_phone_number`, `authorize_customer_profile_id`, `is_debtor`, `package_id`, `was_deleted`, `created_at`, `updated_at`) VALUES
    (15, 'Arvell Moore', 'www.arvellmoore.com', 1, '8770000000', NULL, 0, 1, 0, '2011-07-14 02:35:33', '2011-07-14 02:35:33');

INSERT INTO `campaign` (`id`, `name`, `domain`, `api_key`, `conversion_url`, `is_enabled`, `always_convert`, `is_recordings_enabled`, `dynamic_number_expiry`, `advertiser_id`, `was_deleted`, `created_at`, `updated_at`) VALUES
    (1, 'airsmartusa', 'www.airsmartusa.com', '0421038fe2beb93e29fb7d309e92a4c0', 'convert.html', 1, 0, 1, 60, 15, 0, '2009-10-19 14:17:09', '2011-05-11 11:37:28');
        
INSERT INTO `advertiser_incoming_number_pool` (`id`, `advertiser_id`, `twilio_incoming_phone_number_id`, `campaign_id`, `is_default`, `purchased_at`, `created_at`, `updated_at`) VALUES
    (368, 15, 1, 1, 1, NULL, '2011-03-31 21:14:45', '2011-07-15 09:35:15'), 
    (369, 15, 2, 1, 0, NULL, '2011-03-31 21:14:45', '2011-07-15 09:35:15'),
    (370, 15, 3, NULL, 0, NULL, '2011-03-31 21:14:45', '2011-07-15 09:35:15');
