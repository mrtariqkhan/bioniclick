CREATE TABLE IF NOT EXISTS zipcode_full(
    ZipCode char(5) NOT NULL,
    City varchar(35) NOT NULL,
    State char(2) NOT NULL,
    County varchar(45) NOT NULL,
    AreaCode varchar(55) NOT NULL,
    CityType char(1) NOT NULL,
    CityAliasAbbreviation varchar(13) NOT NULL,
    CityAliasName varchar(35) NOT NULL,
    Latitude decimal(12, 6) NOT NULL,
    Longitude decimal(12, 6) NOT NULL,
    TimeZone char(2) NOT NULL,
    Elevation integer NOT NULL,
    CountyFIPS char(3) NOT NULL,
    DayLightSaving char(1) NOT NULL,
    PreferredLastLineKey varchar(10) NOT NULL,
    ClassificationCode char(1) NOT NULL,
    MultiCounty char(1) NOT NULL,
    StateFIPS char(2) NOT NULL,
    CityStateKey char(6) NOT NULL,
    CityAliasCode varchar(5) NOT NULL,
    PrimaryRecord char(1) NOT NULL,
    CityMixedCase varchar(35) NOT NULL,
    CityAliasMixedCase varchar(35) NOT NULL,
    StateANSI varchar(2) NOT NULL,
    CountyANSI varchar(3) NOT NULL,
    FacilityCode varchar(1) NOT NULL,
    CityDeliveryIndicator varchar(1) NOT NULL,
    CarrierRouteRateSortation varchar(1) NOT NULL,
    FinanceNumber varchar(6) NOT NULL,
    UniqueZIPName varchar(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;


LOAD DATA LOCAL INFILE '/home/fairdev/projects/BionicDoctrine/data/sql/geography - BoughtDB/zip-codes-database-STANDARD.csv'
INTO TABLE zipcode_full
CHARACTER SET latin1
FIELDS
    TERMINATED BY ','
    ENCLOSED BY '"'
LINES TERMINATED BY '\n'
(
    ZipCode,
    City,
    State,
    County,
    AreaCode,
    CityType,
    CityAliasAbbreviation,
    CityAliasName,
    Latitude,
    Longitude,
    TimeZone,
    Elevation,
    CountyFIPS,
    DayLightSaving,
    PreferredLastLineKey,
    ClassificationCode,
    MultiCounty,
    StateFIPS,
    CityStateKey,
    CityAliasCode,
    PrimaryRecord,
    CityMixedCase,
    CityAliasMixedCase,
    StateANSI,
    CountyANSI,
    FacilityCode,
    CityDeliveryIndicator,
    CarrierRouteRateSortation,
    FinanceNumber,
    UniqueZIPName
);


-- Remove repeated zip codes and take usefull columns using new table
CREATE TABLE IF NOT EXISTS new_zipcode_full(
    ZipCode char(5) NOT NULL,
    City varchar(35) NOT NULL,
    State char(2) NOT NULL,
    County varchar(45) NOT NULL,
    AreaCode varchar(55) NOT NULL    
) ENGINE=InnoDB  DEFAULT CHARSET=utf8;

INSERT INTO 
    new_zipcode_full (
        ZipCode,
        City,
        `State`,
        County,
        AreaCode    
    )
SELECT 
    zf.ZipCode, zf.City, zf.`State`, zf.County, zf.AreaCode
FROM
    zipcode_full zf
WHERE primaryrecord = 'P';

INSERT INTO
    administrative_unit (
        pattern,
        name,
        country_id
    )
SELECT
    'state/county',
    concat(state,'/' ,county),
    1
FROM
    new_zipcode_full
GROUP BY
    state, county

INSERT INTO
    city (
        name,
        administrative_unit_id
    )
SELECT
    city, au.id as au_id
FROM
    new_zipcode_full nzf, administrative_unit au
WHERE
    au.name = concat(nzf.state,'/' , nzf.county)
group by
    city, state, county

INSERT INTO
    zip_code (
        code,
        city_id
    )
SELECT
    zipcode, c.id
FROM
    new_zipcode_full nzf,
    city c INNER JOIN administrative_unit au ON c.administrative_unit_id = au.id
WHERE
    nzf.city = c.name
    AND au.name = concat(nzf.state, '/', nzf.county)
