
# показывает в area_codes записи с одинаковыими code_number и разными id

SELECT ac.state_code, ac.code_number, ac.id, ca.state_code, ca.code_number, ca.id
FROM area_codes ac INNER JOIN area_codes ca ON ( ac.code_number = ca.code_number AND ac.id <> ca.id )


# Скрипт для конвертации нашей старой таблицы в новый формат, в соотвествии с кодами для имен штатов:

UPDATE
	test1.twilio_local_area_code as tlac
LEFT JOIN Book.area_codes as ac ON ( tlac.code = ac.code_number )
SET tlac.state_code=ac.state_code
;



# скрипт проверяет есть ли в area_codes номера, которых нет в нашей таблице twilio_local_area_code  и если есть - копирует их.

set foreign_key_checks=0;
INSERT INTO twilio_local_area_code (id, state_code, code)
SELECT id, state_code, code_number
        FROM area_codes
WHERE NOT EXISTS (
                  SELECT code
                    FROM twilio_local_area_code
                    WHERE code = area_codes.code_number
                 );
set foreign_key_checks=1;

