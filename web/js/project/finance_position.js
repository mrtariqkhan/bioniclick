
var filterEventCallbacksFinance = {
    filteredSuccess: function(filter, filterContainer) {
        var url = $('.ajax-filterable').attr('data_url');
        $('.ajax-filterable').load(url);
    }
}
var ajaxFilterElementFinance = '.ajax-filter-finance';

$(document).ready(function() {

    processDateRangeFinance('finance_filter');

    ajaxFiltersProcessing(filterEventCallbacksFinance, ajaxFilterElementFinance);

});

function processDateRangeFinance(formName) {
    var dateRangeElement    = '#' + formName + '_dating_date_range';
    var typeElement         = '#' + formName + '_dating_type';
    var datePickerElement   = 'input.date-picker';

     $(dateRangeElement).livequery(function() {
         $(datePickerElement).daterangepicker({
             presetRanges: [],
             presets: {
                 specificDate: 'Specific Month'
             },
             latestDate: Date.parse('+1years'), //latest date allowed
             earliestDate: Date.parse('-10years'), //earliest date allowed
             datepickerOptions: {
                 showButtonPanel: true,
                 changeYear: true,
                 changeMonth: true,
                 currentText: 'Reset to today'
             },
             dateFormat: 'mm/dd/yy',
             onClose: function () {

                var inputDates = '';
                var val = $(datePickerElement).val();

                var values = val.split(this.rangeSplitter);

                var specificDate = Date.parse(values[0]);
                if (specificDate) {
                    var year = specificDate.getFullYear();
                    var month = specificDate.getMonth();

                    var inputDateAtemp = getDateFirstDayOfMonth(year, month);
                    var inputDateBtemp = getDateLastDayOfMonth(year, month);

                    var inputDateA = inputDateAtemp.toString('MM/dd/yyyy');
                    var inputDateB = inputDateBtemp.toString('MM/dd/yyyy');

                    if (inputDateA.length) {
                        inputDates = inputDateA;
                        if (inputDateB.length && inputDateA != inputDateB) {
                            inputDates = inputDateA + ' ' + this.rangeSplitter + ' ' + inputDateB;
                        }
                    } else if (inputDateB.length) {
                        inputDates = inputDateB;
                    }
                }

                $(datePickerElement).val(inputDates);
             }
         });
    });

    function getDateFirstDayOfMonth(year, month) {
        var firstDayOfMonth = new Date(year, month, 1);
        return firstDayOfMonth;
    }

    function getDateLastDayOfMonth(year, month) {
        var lastDayOfMonth = (new Date(year, month, 1)).addMonths(1).addSeconds(-1);
        return lastDayOfMonth;
    }

    $(typeElement).livequery(function() {
        dateRangeHider();
        $(typeElement).live('change', function(event) {
            dateRangeHider();
        });
    });

    function dateRangeHider() {

        var type = $(typeElement + ' option:selected').val();
        var elem = $(dateRangeElement).parent('div:first');

        if (type == 'date_range') {
            elem.show();
        } else {
            elem.hide();
        }
    }
}