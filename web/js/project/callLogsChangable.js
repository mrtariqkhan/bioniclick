var changeableUrlElement = '.changeable-url';
var tableElement = 'table.log-table';


$(function() {

    // NOTE:: $(tableElement).live('load', function(){});
    $(tableElement).livequery(function() {

        var changeableUrlElements = $(this).find(changeableUrlElement);
        changeableUrlElements.each(function() {
            setChangableUrl(this, true);
        });
    });


    $(changeableUrlElement).live('click', function() {

        var currentUrl  = $(this).html();
        var shortUrl    = $(this).attr('data-url-short');

        setChangableUrl(this, (currentUrl != shortUrl));
    });
});

/**
 *
 *@param elem element with changableUrl
 *@param isShort true if should be set 'short', otherwise - 'long'
 */
function setChangableUrl(elem, isShort) {
    
    var attrName = isShort ? 'data-url-short' : 'data-url-long';
    var oppositeTitle = isShort ? 'full' : 'short';

    var url = $(elem).attr(attrName);

    $(elem).html(url);
    $(elem).attr('title', 'Click to make ' + oppositeTitle);
}
