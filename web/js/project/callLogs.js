var checkboxesElements = '.togCol';
var allCheckboxesElements = '.togCol';
var headerElements = '.hDivBox table tr th';
var tableElements = '#callLogsContainer.log-data div.bDiv td';
var filterElementBox = '#callLogsContainer.log-data div.cDrag div';


/* --- Flexgird functions ---*/
function confgureFlexigrid(passedHeaderElements, passedCheckboxesElements, passedTableElements
    , passedFilterElementBox){

    if(passedHeaderElements != null)
        headerElements = passedHeaderElements;

    if(passedCheckboxesElements != null)
        checkboxesElements = passedCheckboxesElements;

    if(passedTableElements != null)
        tableElements = passedTableElements;

    if(passedFilterElementBox != null)
        filterElementBox = passedFilterElementBox;


    configureFlexigridCheckboxesAndColumns();

    $(allCheckboxesElements).die('click');
    $(checkboxesElements).live('click', function() {
        saveFlexigridSelectedFields();
    });
}
function configureFlexigridCheckboxesAndColumns() {
    $(headerElements).parent().parent().addClass('log-table call-log-table table');
   
    hideUnneededColumns();

    //hide first two columns from filters
    $(checkboxesElements).eq(0).parent().parent().hide();
    $(checkboxesElements).eq(1).parent().parent().hide();

    adjustColumnFiltersPosition();
}

function hideUnneededColumns(){
    var selectedFields = getFlexigridSelectedFields();
    if(selectedFields['has_fields'] == false)
        return;

     $(checkboxesElements).each(function(i) {
        if (selectedFields[getFlexigridColumnCode(this)] == null){
            hideFlexigridColumn(this);
        }
    });
}
function hideFlexigridColumn(checkboxElement){
    $(checkboxElement).selected(false);
    $(checkboxElement).removeAttr('checked');
    
    var fieldTitle = getFlexigridColumnCode(checkboxElement);
    //hide columns headers
    $($(headerElements).selector + '[abbr="' + fieldTitle + '"]').attr('hidden', 'hidden');
    //hide columns
    $($(tableElements).selector + '[column_code="' + fieldTitle + '"]').hide();
}

function adjustColumnFiltersPosition(){
    //fix filters boxes positions by resizing first column
    var firstFilterBox = $(filterElementBox).eq(0);
    firstFilterBox.mousedown();
    firstFilterBox.mousemove();
    firstFilterBox.mouseup();
}
function checkElement(checkboxElement){
    var doShowColumn = $(checkboxElement).is(':checked');
    if (doShowColumn) {
        $(checkboxElement).selected(false);
        $(checkboxElement).removeAttr('checked');
    } else {
        $(checkboxElement).selected(true);
    }
}
/* --- Helpers to store info --- */
function saveFlexigridSelectedFields() {

    var selectedFields = Array();
    $(checkboxesElements).each(function(i) {
        if($(this).is(':checked') == true)
            selectedFields.push(getFlexigridColumnCode(this));
    });

    var selectedFieldsStr = '';
    selectedFieldsStr = selectedFields.join(',');

    $.cookie('selected-fields', selectedFieldsStr, {
        expires: 365
    });
}

function getFlexigridSelectedFields() {

    var fields = Array();
    var hashedFields = Array();
    hashedFields['has_fields'] = false;

    var cookieFields = $.cookie('selected-fields');
    fields = cookieFields != null ? cookieFields.split(',') : Array("");
    $(fields).each(function(i){
        if(fields[i] != ""){
            hashedFields[fields[i]] = fields[i];
            hashedFields['has_fields'] = true;
        }
    });
    return hashedFields;
}
function getFlexigridColumnCode(checkboxElement) {
    var columnTitle = $(checkboxElement).parent().parent().find("td.ndcol2").html();
    var columnCode = '';
    $(headerElements).find('div').each(function(i) {
        if($(this).html() == columnTitle){
            columnCode = $(this).parent().attr('abbr');
        }
    });
    return columnCode;
}
function getFlexigridColumnFieldTitle(checkboxElement) {
    return $(checkboxElement).attr('field_title');
}
