//$(document).ready(function() {
//
//    processCampaignSelect(
//        'user_incoming_number_pool_filters',
//        'fillUserPhoneNumbersByCampaign'
//    );
//});
//
///**
// *@param formName name of form to find elements on it.
// *@param url1 url where we should get options for local area code select.
// *@param url2 url where we should get options phone number select.
// */
//function processCampaignSelect(formName, url) {
//
//    var campaignSelectElement   = '#' + formName + '_campaign_id';
//    var phoneNumberSelectElement = '#' + formName + '_twilio_incoming_phone_number_id';
//
//    // NOTE:: it is like $(phoneTypeSelectElement).live('load', function(){});
//    //$(campaignSelectElement).livequery(function() {
//    //    showPhoneNumbersOnCampaign();
//    //});
//
//    $(campaignSelectElement).live(
//        'change',
//        function(event) {
//            showPhoneNumbersOnCampaign();
//        }
//    );
//
//     function showPhoneNumbersOnCampaign() {
//
//        var campaignId = $(campaignSelectElement).val();
//
//        $.ajax(
//            {
//                type: 'GET',
//                url: url,
//                dataType: 'json',
//                cache: false,
//                data: {
//                    campaign_id: campaignId
//                },
//                error: globalAjaxErrorHandler,
//                success: function(response, statusText, xhr) {
//                    globalAjaxSuccessHandler();
//
//                    $(phoneNumberSelectElement).empty();
//                    $(phoneNumberSelectElement).append(new Option('', ''));
//                    $.each(response, function() {
//                         $(phoneNumberSelectElement).append(new Option(this['phone_number'], this['id']));
//                    });
//                    //$(phoneNumberSelectElement).val('20');
//                }
//            }
//        );
//    }
//
//
//}
