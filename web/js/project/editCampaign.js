
$('#redirectMessage').livequery(function() {
    
    $(this).find('input[name=typeMessage]').change(function(){
        $('#campaign_is_recordings_enabled').val( $(this).val() );
    });
    
    $('#upload-container').each(function(){
        uploader = new plupload.Uploader({
            runtimes : 'gears,flash, html5, html4,silverlight,browserplus',
            flash_swf_url : '/js/plupload/js/plupload.flash.swf',
            silverlight_xap_url : '/js/plupload/js/plupload.silverlight.xap',
            browse_button : 'upload-selectfiles',
            container : 'upload-container',
            max_file_size : '3mb',
            chunk_size: '1500kb',
            dragdrop : true,
            url : $('#redirectMessage').attr('name'),
            filters : [{title : "Audio files", extensions : "mp3,wav"}],
            headers: {'X-Requested-With': 'XMLHttpRequest'}
        });

        uploader.bind('Init', function(up, params) {
            $('#upload-info').html('');
            $('#upload-selectfiles').show();
        });
        
        uploader.bind('FilesAdded', function(up, files) {
            
            setTimeout(function(){
                if (uploader.files.length < 1) {
                    return false;
                }
                
                // uploader.splice(1);

                $('#error-custom').hide();

                $('#download-custom').hide();
                // $('#player-custom').hide();
                $('#custom-message-player').hide();

                $('#upload-info').show().html('File uploading in progress');
                
                uploader.start();
                
                // uploader.splice(0);
            }, 0);
            
            up.refresh(); // Reposition Flash/Silverlight
        });

        uploader.bind('UploadProgress', function(up, file) {
            
            $('#upload-info').html('File uploading ' + file.percent + "%");
        });
        
        uploader.bind('Error', function(up, err) {
            if (err.status == 401) {
                return redirectToLogin();
            }

            if (err.code == -600) {
                err.message = "Maximum file size 3mb.";
            } else
            if (err.code == -601) {
                err.message = "Only mp3 or wav audio files.";
            }

            $('#error-custom').show().html(err.message);

            setTimeout(function(){
                $('#error-custom').hide(222);
            }, 2222);

            //console.log(err.code + ' ' + err.message);

            up.refresh(); // Reposition Flash/Silverlight
        });

        uploader.bind('FileUploaded', function(up, file) {
            $('#upload-info').html('File uploading completed');
            
            var path = '/audio/custom-redirect-msg/' + $('#campaign_id').val() + file.name.substring( file.name.lastIndexOf('.') );
            setTimeout(function(){
                $('#upload-info').hide(222);

                $('#download-custom').attr('href', path).show(222);
                $('#custom-message-player').load('/api/audioPlayer?path=' + path);
                $('#custom-message-player').show(222);

                // $('#player-custom').html('').show(222);
                // miniAudioPlayer('#player-custom', path );

            }, 999);
        });
        
        uploader.init();
        
        
        // Stiling "Upload new file" button
        $('div.plupload').hover(function(){
            $('#upload-selectfiles').addClass('hover');
        }, function(){
            $('#upload-selectfiles').removeClass('hover');
        });
    });
    
    
    
    path = $('#download-custom').attr('href');
    if (path != '') {
        $('#download-custom').show();
        $('#custom-message-player').show();
        // miniAudioPlayer('#player-custom' , path );
    }
});