var ajaxFilterElement = '.ajax-filter';
var ajaxFilterContainer = '.ajax-filter-container';

/**
Example:

var filterEventCallbacks = {
    filteredSuccess: function(filter, filterContainer) {
        ...
    }
}
$(function() {
    ajaxFiltersProcessing(filterEventCallbacks);
});
*/
function ajaxFiltersProcessing(filterEventCallbacks, ajaxFilterElementCustom) {

    ajaxFilterElementCustom = ajaxFilterElementCustom ? ajaxFilterElementCustom : ajaxFilterElement;
    $(ajaxFilterElementCustom).live('submit', function() {
        return submitAjaxFilter($(this), filterEventCallbacks);
    });
}

function submitAjaxFilter(filter, filterEventCallbacks) {
    var options = {
        success:    ajaxFilterSuccessHandler,
        error:      globalAjaxErrorHandler,
        dataType:   'json',
        customFilterEventCallbacks:       filterEventCallbacks

        //NOTE:: we do not use iframe flag because if we use it then request will be
        // not XHR. But we need XHR. So we cannot upload files in such forms.
//        iframe:     true
    };
    filter.ajaxSubmit(options);
    return false;
}

function ajaxFilterSuccessHandler(responseText, statusText, xhr, filter) {

    globalAjaxSuccessHandler();

    if (!responseText) {
        return;
    }

    if (responseText['success']) {
        if (!invokeFilteredSuccess(filter, this.customFilterEventCallbacks)) {
            return;
        }

    } else if (responseText['error']) {
        myAjaxGlobalError(responseText['error']);
    }
}




/* ------ Helpers to make usage of filterEventCallbacks more simple. ------ */
/**
 * returns true if need filterEventCallbacks exists
 */
function invokeFilteredSuccess(filter, filterEventCallbacks) {
    if (filterEventCallbacks && filterEventCallbacks['filteredSuccess']) {
        var filterContainer = filter.parents(ajaxFilterContainer);
        filterEventCallbacks['filteredSuccess'](filter, filterContainer);
        return true;
    }
    return false;
}
