
var reloadableChartElement  = '.reloadable-chart';
var chartElement            = '.reloadable-chart #chart-call-summary';
var chartTimeElementFrom    = '.reloadable-chart-time-range .chart-time-range-from-value';
var chartTimeElementTo      = '.reloadable-chart-time-range .chart-time-range-to-value';

var classChartCallSummary       = 'ajax-chart-call-summary-filter';
var classChartCallsByParameter  = 'ajax-chart-calls-by-parameter-filter';
var classChartCallStatus        = 'ajax-chart-call-status-filter';
var classChartTopClients        = 'ajax-chart-top-clients-filter';

var filterEventCallbacks = {
    filteredSuccess: function(filter, filterContainer) {

        if (filter.hasClass(classChartCallSummary)) {
            doReloadChartCallSummary(filterContainer);
        } else if (filter.hasClass(classChartCallsByParameter)) {
            loadChartCallsByParamater();
        } else if (filter.hasClass(classChartCallStatus)) {
            doReloadChartCallStatus(filterContainer);
        } else if (filter.hasClass(classChartTopClients)) {
            loadChartTopClients();
        } else {
            location.reload();
        }
    }
}

$(document).ready(function() {
    processDateRange('graph_filter');

    ajaxFiltersProcessing(filterEventCallbacks);
});

function doReloadChartCallSummary(filterContainer) {

    var elemToReload = filterContainer.find(reloadableChartElement);
    var url = elemToReload.attr('data_load_url');

    setChartIsLoading(elemToReload);

    $.get(url, {}, function(response) {
        setChartIsNotLoading(elemToReload);
        
        elemToReload.html(response);

        var chart = filterContainer.find(chartElement);
        var rangeFrom = chart.attr('range_data_from');
        var rangeTo = chart.attr('range_data_to');

        filterContainer.find(chartTimeElementFrom).html(rangeFrom);
        filterContainer.find(chartTimeElementTo).html(rangeTo);
    });
}

function doReloadChartCallStatus(filterContainer) {

    var elemToReload = filterContainer.find(reloadableChartElement);
    var url = elemToReload.attr('data_load_url');

    setChartIsLoading(elemToReload);

    $.get(url, {}, function(response) {
        setChartIsNotLoading(elemToReload);

        elemToReload.html(response);
    });
}