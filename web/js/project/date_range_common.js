
function processDateRange(formName) {
    var dateRangeElement    = '#' + formName + '_dating_date_range';
    var typeElement         = '#' + formName + '_dating_type';
    var datePickerElement   = 'input.date-picker';

     $(dateRangeElement).livequery(function() {
         $(datePickerElement).daterangepicker({
             presetRanges: [],
             presets: {
//                 specificDate: 'Specific Date',
//                 allDatesBefore: 'All Dates Before',
//                 allDatesAfter: 'All Dates After',
                 dateRange: 'Date Range'
             },
             latestDate: Date.parse('+1years'), //latest date allowed
             earliestDate: Date.parse('-10years'), //earliest date allowed
             datepickerOptions: {
                 //minDate: Date.parse('-10years'),
                 //maxDate: Date.parse('+1years'),
                 showButtonPanel: true,
                 changeYear: true,
                 currentText: 'Reset to today'
             },
             dateFormat: 'mm/dd/yy',
             onClose: function () {

                var val = $(datePickerElement).val();

                var values = val.split(this.rangeSplitter);

                var inputDateAtemp = Date.parse(values[0]);
                var inputDateBtemp = Date.parse(values[1]);

                var inputDateA = $.datepicker.formatDate(this.dateFormat, inputDateAtemp);
                var inputDateB = $.datepicker.formatDate(this.dateFormat, inputDateBtemp);

                var inputDates = '';
                if (inputDateA.length) {
                    inputDates = inputDateA;
                    if (inputDateB.length && inputDateA != inputDateB) {
                        inputDates = inputDateA + ' ' + this.rangeSplitter + ' ' + inputDateB;
                    }
                } else if (inputDateB.length) {
                    inputDates = inputDateB;
                }

                $(datePickerElement).val(inputDates);
             }
         });
    });

    $(typeElement).livequery(function() {
        dateRangeHider();
        $(typeElement).live('change', function(event) {
            dateRangeHider();
        });
    });

    function dateRangeHider() {

        var type = $(typeElement + ' option:selected').val();
        var elem = $(dateRangeElement).parent('div:first');

        if (type == 'date_range') {
            elem.show();
        } else {
            elem.hide();
        }
    }
}