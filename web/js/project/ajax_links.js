var ajaxLinksElements = [
      'a.ajax-form-load'
    , '.ajax-form-cancel'
    , '.ajax-delete'
    , 'a.ajax-load'
    , '#snippetgetter'
//    , '.ajax-pager a'
//    , '.ajax-pager .max-per-page'
];
var dataAttrName = 'href';
var defaultHref = '/';


$(ajaxLinksProcessing);


function ajaxLinksProcessing() {

    $.each(ajaxLinksElements, function (i, ajaxLinksElement) {
        $(ajaxLinksElement).livequery(function() {
            setAjaxRealHref(this);
        });
    });
}

function getAjaxRealHref(element) {
    return $(element).data(dataAttrName);
}

function setAjaxRealHref(element) {
    $(element).data(dataAttrName, $(element).attr('href'));
    if($(element).attr('href'))
        $(element).attr('href', $(element).data('href'));
    else
        $(element).attr('href', defaultHref);
}