
function reloadPagable(element, page, maxPerPage) {
    var pagablePanel = getPagablePanelByElement(element);
    if (pagablePanel) {
        if (!maxPerPage) {
            var selectedOption = pagablePanel.find('.ajax-pager .max-per-page option:selected');
            maxPerPage = selectedOption.length ? selectedOption.val() : '';
        }

        var params = {
            'pager': {
                'page'          : page,
                'max_per_page'  : maxPerPage
            }
        };
        reloadPagableWithParams(pagablePanel, params);
    }
}

$('.ajax-pager a').live('click', function() {
    var page = $(this).attr('pager_data');
    reloadPagable(this, page);
    return false;
});


$('.ajax-pager .max-per-page').live('change', function() {
    var selectedOption = $(this).find('option:selected');
    var maxPerPage = selectedOption.length ? selectedOption.val() : '';
    reloadPagable(this, 1, maxPerPage);
});

function reloadPagableWithParams(pagablePanel, params) {
    var url = getPagableUrl(pagablePanel);
    if (typeof(window.saveTabParamsByElement) == 'function') {
        saveTabParamsByElement(pagablePanel, params, url, 'GET');
    }

    $.get(url, params, function(responseText) {
        pagablePanel.html(responseText);
    });
}

function getPagablePanelByElement(element) {
    var pagablePanel = $(element).parents('div.ajax-pagable:first');
    pagablePanel = (pagablePanel.length > 0) ? pagablePanel : null;

    return pagablePanel;
}

function getPagableUrl(pagablePanel) {
    var pagerPanel = pagablePanel ? pagablePanel.find('div.ajax-pager:first') : null;
    pagerPanel = (pagerPanel.length > 0) ? pagerPanel : null;

    var url = pagerPanel ? pagerPanel.attr('paging_table_url') : '';
    return url;
}