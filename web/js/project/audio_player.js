
function processAudioPlayer() {

    $('.audio-player a').live('click', function(){
	$('.audio-player a').show();
        $('.audio-player span').hide();
        $(this).hide();
        $(this).next('span').show();

	//remove audio player and add it again to force reloading(solve hanging issue of audio player)
        var html = $(this).next('span').html();
	$(this).next('span').children().remove();
	$(this).next('span').append(html);
    });

}

function miniAudioPlayer(obj, file) {
    
    if( !obj && !file )
        return false;
    
    var $obj = $(obj);
    
    $obj.append(
        '<span id="' + obj.substring(1) + '-interface"><a href="#" class="jp-play">Play</a><a href="#" class="jp-pause">Pause</a>'+
        '<span class="jp-video-play"></span><span class="jp-stop"></span>'+
        '<span class="jp-seek-bar"></span><span class="jp-play-bar"></span><span class="jp-mute"></span><span class="jp-unmute">'+
        '</span><span class="jp-volume-bar-value"></span>'+
        '</span><span class="jp-volume-bar"></span>'+
        '</span>'
    );
    
    var player = $('<span class="player" id="pr333"></span>');
    
    $obj.append(player);
    
    var extension = file.substring( file.lastIndexOf('.') + 1 );
    var solution = 'flash, html';
    if(extension == 'wav')
    	solution = 'html, flash';
    $('#pr333').jPlayer({
        ready: function () {
            
            var media = {};
            media[ file.substring( file.lastIndexOf('.') + 1 ) ] = file + '?v=' + Math.random();
            
            $('#pr333').jPlayer("setMedia", media);

            //$('#pr333').jPlayer("setMedia", {mp3: file});

            $(obj+'-interface .jp-play').click(function(){

                //console.log($obj);

                $('#pr333').jPlayer('play');

                return false;
            });

            $(obj+'-interface .jp-pause').click(function(){

                $('#pr333').jPlayer('pause');

                return false;
            });
        },
        swfPath: '/js/jquery/jPlayer', //'/jPlayer/js',
        solution: solution, //'html, flash',
        supplied: 'wav, mp3', //'mp3,m4a,oga,wav,webma',
        volume: 0.7,
        cssSelectorAncestor: obj + '-interface' //obj.children('interface').get(0)
        /*,errorAlerts: true
        ,warningAlerts: false*/
    });
    
}
