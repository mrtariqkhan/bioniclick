//NOTE:: whenever You use tabs.js You must define tabEventCallbacks:
//var tabEventCallbacks = {
//    needTabCode1: {
//        loaded: function(ui) {
//        },
//        selected: function(ui) {
//        }
//    },
//    needTabCode2: {
//        loaded: function(ui) {
//        },
//        selected: function(ui) {
//        }
//    }
//}

var ajaxOptionsCommon = {
    error: globalAjaxErrorHandler,
    success: globalAjaxSuccessHandler
};
var tabElement = 'div#tabs';
var formCallbacks = {
    reloadFormContainer: function(elementFromFormPanel, url, methodType) {
        var tabIndex = getTabIndexByElement($(elementFromFormPanel));
        reloadTabWithParams(tabIndex, {}, url, methodType);
    },
    getFormContainerPanel: function(form) {
        return getTabPanelByElement(form);
    }
}
var formEventCallbacks = {
    formSavedSuccess: function(form, ajaxFormCancelElem) {
        cancelAjaxForm(ajaxFormCancelElem);
    }
}
var deleteEventCallbacks = {
    itemDeletedSuccess: function(deleteForm) {
        var url = deleteForm.attr('after_url');
        var tabIndex = getTabIndexByElement(deleteForm);
        reloadTab(tabIndex, url);
    }
}
var filterEventCallbacks = {
    filteredSuccess: function(filter, filterContainer) {
        var tableElement = filterContainer.find('.table');
        reloadPagable(tableElement, 1);
    }
}
//var tabCallbacks = {};
//var eventCallbacks = {};

$(function() {

    ajaxFiltersProcessing(filterEventCallbacks);

    buildTabs();
});


/* ------ Tab's configuring ------ */
function buildTabs() {

    var selectedTab = $('.selected-tab');
    var selectedIndex = (selectedTab.length)
        ? $(tabElement + ' li').index(selectedTab)
        : 0
    ;

    $(tabElement).tabs({
        selected: selectedIndex,
        remove: function(event, ui) {
            $(ui.panel).remove();
        },
        load: function(event, ui) {
            globalAjaxSuccessHandler();
            
            if (!invokeTabLoaded(ui)) {
                return;
            }
        },
        select: function(event, ui) {
            invokeTabSelected(ui);

            var tabIndex = getTabIndex(ui.tab);
            updateTabParams(tabIndex);
        },
        ajaxOptions: ajaxOptionsCommon,
        //TODO:: think how to use cache.
        cache: false,
        cookie: {},
        spinner: '' //'<img src="/images/new_design/ajax_loader.gif" class="ajax-loader-small"/><span class="ajax-loader-small-message">Loading...</span>'
    });
}




/* ------ Helpers to find tab, tabPanel, tabIndex ect. ------ */

function getTabPanelByIndex(tabIndex) {
    tabIndex = tabIndex + 1;
    var tabPanel = $(tabElement).find('div#ui-tabs-' + tabIndex);
    return tabPanel;
}

function getTabByIndex(tabIndex) {
    var tab = null;

    var tabsElements = $(tabElement + ' ul.ui-tabs-nav a');
    $.each(tabsElements, function(i, tabsElement) {
        if (i == tabIndex) {
            tab = $(tabsElement);
        }
    });

    return tab;
}

function getTabIndexByElement(objectInTab) {
    objectInTab = $(objectInTab);
    var tabPanel = objectInTab.parents('.ui-tabs-panel:first');
    var tabId = tabPanel.attr('id');

    var index = null;
    var allLinksInTabs = $('div#tabs a');
    $.each(allLinksInTabs, function(i, linkInTab) {
        if ($(linkInTab).attr('href') == '#' + tabId) {
            index = i;
        }
    });

    return index;
}

function getTabIndex(tab) {
    var tabIndexString = $(tab).attr('href');
    var tabIndex = getIdByString(tabIndexString);
    tabIndex = tabIndex ? tabIndex -1 : null;
    return tabIndex;
}

function getTabUrl(index) {
    var tab = getTabByIndex(index);
    var url = tab ? tab.data('href.tabs') : '';
    return url;
}

function getTabFirstMethodType(index) {
    var tab = getTabByIndex(index);
    var method = tab ? tab.attr('first_method') : 'POST';
    return method;
}

function getTabPanelByElement(objectInTab) {
    var tabIndex = getTabIndexByElement(objectInTab);
    var tabPanel = getTabPanelByIndex(tabIndex);

    return tabPanel;
}

/* ------ Tab's reloading ------ */

/**
 *@param tabIndex - index of need tab.
 *@param url - (optional) url
 *@param methodType - (optional) method type
 */
function updateTabParams(tabIndex, url, methodType) {
    var ajaxOptions = getTabCookie(tabIndex, url, methodType);
    setTabAjaxOptions(ajaxOptions);
    
    if (url) {
        $(tabElement).tabs('url', tabIndex , url);
    }
}

/**
 *@param tabIndex - index of need tab.
 *@param url - (optional) url
 *@param methodType - (optional) method type
 */
function reloadTab(tabIndex, url, methodType) {
    updateTabParams(tabIndex, url, methodType);
    $(tabElement).tabs('load', tabIndex);
}

/**
 *@param tabIndex - index of need tab.
 *@param paramsForTabUpdated - params
 *@param url - (optional) url
 *@param methodType - (optional) method type
 */
function reloadTabWithParams(tabIndex, paramsForTabUpdated, url, methodType) {
    var doReload = setTabCookie(tabIndex, paramsForTabUpdated, url, methodType);
    if (doReload) {
        reloadTab(tabIndex, url, methodType);
    }
}





/* ------ Work with cookies to make possible to use SEPARATE params for EACH TAB ------ */

function saveTabParamsByElement(element, paramsForTabUpdated, url, methodType) {
    var tabIndex = getTabIndexByElement(element);
    setTabCookie(tabIndex, paramsForTabUpdated, url, methodType);
}

/**
 *Merges old params for need tab with new. New params have priority.
 *@param tabIndex - index of need tab.
 *@param paramsForTabUpdated - new params to need tab
 *@param url - (optional) new url
 *@param methodType - (optional) new method type
 *@return True if parameters have been changed for need tab. False othrwise.
 */
function setTabCookie(tabIndex, paramsForTabUpdated, url, methodType) {
    var curUrl = getTabUrl(tabIndex);
    url = url ? url : curUrl;
    methodType = methodType ? methodType : null;

    var tabsCookie = getTabsCookie();
    var tabCookieAll = tabsCookie[tabIndex] ? tabsCookie[tabIndex] : null;
    var tabCookie = tabCookieAll && tabCookieAll[url] ? tabCookieAll[url] : null;

    var params = paramsForTabUpdated;
    if (tabCookie) {
        var oldParamsForTab = tabCookie['data'];

        params = $.extend(oldParamsForTab, paramsForTabUpdated);

        if ($(params).equals(oldParamsForTab)) {
            return false;
        }
    } else if (!paramsForTabUpdated) {
        return true;
    }

    if (!tabCookieAll) {
        tabsCookie[tabIndex] = {};
    }
    tabsCookie[tabIndex][url] = {'data': params, 'type': methodType};
    tabsCookie[tabIndex]['last'] = {'url': url, 'data': params, 'type': methodType};
    $(tabElement).tabs('option', 'cookie', tabsCookie);
    return true;
}

function getTabsCookie() {
    var tabsCookie = $(tabElement).tabs('option', 'cookie');
    return tabsCookie ? tabsCookie : {};
}

/**
 *@param tabIndex - index of need tab.
 *@param url - (optional) url
 *@param methodType - (optional) method type
 *@return all ajax options to tab with url.
 */
function getTabCookie(tabIndex, url, methodType) {
    var tabsCookie = getTabsCookie();
    var tabCookieAll = tabsCookie[tabIndex] ? tabsCookie[tabIndex] : {};
    var tabsCookieLast = tabCookieAll['last'];

    if (!tabsCookieLast && !url && ! methodType) {
        url         = getTabUrl(tabIndex);
        methodType  = getTabFirstMethodType(tabIndex);
        setTabCookie(tabIndex, {}, url, methodType);
        return getTabCookie(tabIndex);
    }

    tabsCookieLast = tabsCookieLast ? tabsCookieLast : {};

    url = url ? url : tabsCookieLast['url'];
    url = url ? url : getTabUrl(tabIndex);
    var tabCookie =  tabCookieAll[url] ? tabCookieAll[url] : {};

    methodType = methodType ? methodType : null;
    if (!methodType) {
        methodType = tabCookie['type'] ? tabCookie['type'] : 'POST';
    }
    tabCookie['type'] = methodType;

    return tabCookie;
}

function setTabAjaxOptions(ajaxOptionsAdditional) {
    var ajaxOptions = {};
    $.each(ajaxOptionsAdditional, function(i, item) {
        ajaxOptions[i] = item;
    });
    $.each(ajaxOptionsCommon, function(i, item) {
        ajaxOptions[i] = item;
    });

    $(tabElement).tabs('option', 'ajaxOptions', ajaxOptions);
}





/* ------ Helpers ------ */

/**
 * @returns true if this-object and compareTo-object are equals.
 */
$.fn.equals = function(compareTo) {
    if (!compareTo || !compareTo.length || this.length != compareTo.length) {
        return false;
    }

    for (var i = 0; i < this.length; i++) {
        if (this[i] !== compareTo[i]) {
            return false;
        }
    }

    return true;
}

/**
 * @returns first parsed number in string.
 */
function getIdByString(stringWithNumber) {
    var number = /(\d+)/.exec(stringWithNumber);
    number = (number) ? number[0] : null;
    return number;
}



/**
 * returns true if need formEventCallbacks exists
 */
function invokeTabLoaded(ui) {
    var tabCode = $(ui.tab).attr('tab_code').toLowerCase();
    if (window.tabEventCallbacks && tabEventCallbacks[tabCode] && tabEventCallbacks[tabCode]['loaded']) {
        tabEventCallbacks[tabCode]['loaded'](ui);
        return true;
    }
    return false;
}

function invokeTabSelected(ui) {
    var tabCode = $(ui.tab).attr('tab_code').toLowerCase();
    if (window.tabEventCallbacks && tabEventCallbacks[tabCode] && tabEventCallbacks[tabCode]['selected']) {
        tabEventCallbacks[tabCode]['selected'](ui);
        return true;
    }
    return false;
}
