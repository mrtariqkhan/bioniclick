$('#widget_html_id')
    .die( 'widget_editor:init')
    .live('widget_editor:init',function() {
        if ($('#widget_html_content1').size() > 0) {

            var ckeditorConfig = {
                toolbar : [[
                    'Source', '-', 'Bold', 'Italic', '-', 'NumberedList',
                    'BulletedList', '-', 'Link', 'Unlink'
                ]],
                width: 500,
                height: 200,
                enterMode: 2
            };
            CKEDITOR.replace('widget_html_content1', ckeditorConfig);
        }
        $('#widget_html_id').trigger('change');
    });

$('#widget_html_id')
    .die( 'widget_editor:destroy')
    .live('widget_editor:destroy', function() {
        if (CKEDITOR.instances['widget_html_content1']) {
            CKEDITOR.instances['widget_html_content1'].destroy();
        }
    });

//$('#widget_html_id').live('change', function(){
//    if ($('#widget_html_content1').size() > 0) {
//        url = document.location;
//        re = new RegExp('.*combo_id/([0-9]+).*');
//
//        html_id = $(this).val();
//        combo_id = re.exec(url)[1];
////        $.post('/backend_dev.php/widget/getTagContent', {html_id: html_id, combo_id: combo_id}, function(data){
////            CKEDITOR.instances['widget_html_content1'].setData(data);
////        });
//    }
//    // $('#widget_html_content1').load('/backend_dev.php/widget/getTagContent', {html_id: html_id, combo_id: combo_id});
//});

$(function() {
    $('#widget_html_id').trigger('widget_editor:init');
});