
/**
 *@param formName name of form to find elements on it.
 *@param urlLocalAreCode url where we should get options for local area code select.
 *@param urlPhoneNumbersCount url where we should get options phone number select.
 */
function processLocalAreaCodeSelect(formName, urlLocalAreCode, urlPhoneNumbersCount) {
    
    var phoneTypeSelectElement   = '#' + formName + '_phone_number_type';
    var areaSelectElement        = '#' + formName + '_twilio_local_area_code_id';
    var phoneNumberSelectElement = '#' + formName + '_twilio_incoming_phone_number_id';
    var countInfoElement = '.field-help';

    $(phoneTypeSelectElement).live('change', showAreasOfPhoneType);

//    $(areaSelectElement).live('change', showPhoneNumbersCountOnArea);

    function showAreasOfPhoneType() {

        var phoneNumberType = $(phoneTypeSelectElement).val();
        
        if (!urlLocalAreCode) {
            return;
        }

        var params = {
            phone_number_type: phoneNumberType
        };

        var elem = $('#company_id_handler');
        if (elem.length > 0) {
            params['company_id'] = elem.attr('company_id');
        }

        elem = $('#advertiser_id_handler');
        if (elem.length > 0) {
            params['advertiser_id'] = elem.attr('advertiser_id');
        }
        
        $.ajax(
            {
                type: 'GET',
                url: urlLocalAreCode,
                dataType: 'json',
                cache: false,
                data: params,
                error: globalAjaxErrorHandler,
                success: function(response, statusText, xhr) {
                    globalAjaxSuccessHandler();
                    
                    $(areaSelectElement).empty();
                    $.each(response, function() {
                        text = this['code'];
                        locationInfo = '';
                        if (this['state_name']) {
                            locationInfo = ' (' + this['country_name'] + ' - ' + this['state_name'] + ')';
                        }
                        
                        $(areaSelectElement).append(
                            $('<option>').text(text + locationInfo).val(this['id'])
                        );
                    });
                    
//                    showPhoneNumbersCountOnArea();
                }
            }
        );
    }

//    function showPhoneNumbersCountOnArea() {
//
//        var localAreaCodeId = $(areaSelectElement).val();
//
//        if (!urlPhoneNumbersCount) {
//            return;
//        }
//
//        var params = {
//            local_area_code_id: localAreaCodeId
//        };
//
//        var elem = $('#advertiser_id_handler');
//        if (elem.length > 0) {
//            params['advertiser_id'] = elem.attr('advertiser_id');
//        }
//
//        $.ajax(
//            {
//                type: 'GET',
//                url: urlPhoneNumbersCount,
//                dataType: 'json',
//                cache: false,
//                data: params,
//                error: globalAjaxErrorHandler,
//                success: function(response, statusText, xhr) {
//                    globalAjaxSuccessHandler();
//                    var count = 0;
//                    if (response.count) {
//                        count = response.count;
//                    }
//                    $(countInfoElement).html('(' + count + ' available number(s) with selected code)');                    
//                }
//            }
//        );
//    }
    
}
