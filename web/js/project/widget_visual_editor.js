/*
 * NOTE:: Reverts the $ alias and then creates and executes a function to
 *  provide the $ as a jQuery alias inside the functions scope. Inside the
 *  function the original $ object is not available.
 *  This works well for most plugins that don't rely on any other library.
 */
jQuery.noConflict();
(function($) {
    // NOTE:: more code using $ as alias to jQuery


    $.ajaxSetup({
        error: wveGlobalAjaxErrorHandler,
        success: wveGlobalAjaxSuccessHandler
    });


    /**
     * NOTE:: wve_field_name, wve_function_name need to make sure that main page
     *  has no fields and functions with the same names.
     * wve prefix means widget_visual_editor
     */
    var wveEditorData = {};

    var wveOldHtmlId = '';
    var wveOldHtmlTag = ''

    var WVE_STYLE_SELECTED  = {cursor: 'pointer', border: '5px solid red'};
    var WVE_STYLE_NORMAL    = {cursor: 'pointer', border: '5px solid blue'};
    var wveOriginalStyles   = {};

    var WVE_FORM_TYPE_UPDATE_HTML    = 1;
    var WVE_FORM_TYPE_UPDATE_PHONE   = 2;
    var WVE_FORM_TYPE_CREATE_HTML    = 3;
    var WVE_FORM_TYPE_CREATE_PHONE   = 4;


    var wveWidgetDialogElement          = '#wve-bionic-dialog';
    var wveWidgetDialogCancelElement    = '#wve-bionic-dialog-cancel';
    var wveOpenWidgetPhoneNewFormElement= '#wve-bionic-dialog-new-phone';
    var wveOpenWidgetNewFormElement     = '#wve-bionic-dialog-new-html';
    var wveWidgetListElement            = '#wve-bionic-widget-list';

    var wveFormElements        = '.WVE-form';
    var wveFormWidgetPhoneEdit = 'WVE_widget_phone_edit';
    var wveFormWidgetEdit      = 'WVE_widget_edit';
    var wveFormWidgetPhoneNew  = 'WVE_widget_phone_new';
    var wveFormWidgetNew       = 'WVE_widget_new';
    var wveFormContainers      = '.WVE-each-form-container';

    var showPreviewElements = '.WVE-do-preview';
    var saveElements        = '.WVE-do-save';
//    var cancelElements      = '.WVE-do-cancel';


    var wveErrorGlobalElement       = '.bionic-ajax-field-error-global';
    var wveErrorGlobalHiddenClass   = 'bionic-ajax-field-error-global bionic-ajax-field-error-global-hidden';
    var wveErrorGlobalShownClass    = 'bionic-ajax-field-error-global bionic-ajax-field-error-global-shown';

    var wveErrorFieldElement        = '.bionic-ajax-field-error';
    var wveErrorFieldHiddenClass    = 'bionic-ajax-field-error bionic-ajax-field-error-hidden';
    var wveErrorFieldShownClass     = 'bionic-ajax-field-error bionic-ajax-field-error-shown';

    var WVE_DURATION_ACTIVATION_SHOW = 1000;
    var wveBlockingElement = '.WVE-ajax-blocked';



    $(document).ready(function() {
        var wveEditorDataTmp = $(wveWidgetDialogElement).attr('editor_data');
        eval('wveEditorData = ' + wveEditorDataTmp);

        if (wveEditorData.objects.length == 0) {
            wveEditorData.objects = {};
        }


        var wveWidgetHtmlContentElementEdit = wveGetEditWidgetContentName();
        var wveWidgetHtmlContentElementNew = wveGetNewWidgetContentName();

        var ckeditorConfig = {
            toolbar : [[
                'Source', '-', 'Bold', 'Italic', '-', 'NumberedList',
                'BulletedList', '-', 'Link', 'Unlink'
            ]],
            width: $('#' + wveWidgetHtmlContentElementEdit).width(),
            height: $('#' + wveWidgetHtmlContentElementEdit).height(),
            resize_enabled: false,
            enterMode: 2
        };
        CKEDITOR.replace(wveWidgetHtmlContentElementEdit, ckeditorConfig);
        CKEDITOR.replace(wveWidgetHtmlContentElementNew,  ckeditorConfig);



        $('#' + wveFormWidgetEdit + ' ' + showPreviewElements + ', ' +
          '#' + wveFormWidgetNew + ' ' + showPreviewElements).click(function() {
            var form = $(this).parents('form:first');

            var htmlId = wveGetField(form, 'html_id').val();
            var htmlTag = wveGetField(form, 'html_type').val();
            var elemOriginal = wveGetOriginalElement(htmlId, htmlTag);

            wveClearFormErrors(form);

            wveNormaliseOldOriginalElement();

            if (elemOriginal.size() > 0) {
                wveSaveOldOriginalElement(htmlId, htmlTag);

                var contentElement = wveGetFieldName(form, 'html_content');
                elemOriginal.html(CKEDITOR.instances[contentElement].getData());

                wveMoveDialogTo(elemOriginal);

                wveChangeStyle(elemOriginal, WVE_STYLE_SELECTED);
            }

            wveResizeDialogContent(form);

            return false;
        });

        $('#' + wveFormWidgetPhoneEdit + ' ' + showPreviewElements + ', ' +
          '#' + wveFormWidgetPhoneNew + ' ' + showPreviewElements).click(function() {
            var form = $(this).parents('form:first');

            var htmlId = wveGetField(form, 'html_id').val();
            var htmlTag = wveGetField(form, 'html_type').val();
            var elemOriginal = wveGetOriginalElement(htmlId, htmlTag);

            wveClearFormErrors(form);

            wveNormaliseOldOriginalElement();

            if (elemOriginal.size() > 0) {
                wveSaveOldOriginalElement(htmlId, htmlTag);

                var hc1 = wveGetField(form, 'html_content_1').val();
                var hc2 = wveGetField(form, 'html_content_2').val();

                var numberId = wveGetField(form, 'physical_phone_number_id').val();
                var number = wveEditorData.physical_phone_numbers[numberId];

                var formatId = wveGetField(form, 'number_format_id').val();
                var format = wveEditorData.number_formats[formatId];

                var hcPhone = wveFormatNumber(number, format);
                elemOriginal.html(hc1 + hcPhone + hc2);

                wveMoveDialogTo(elemOriginal);

                wveChangeStyle(elemOriginal, WVE_STYLE_SELECTED);
            }

            wveResizeDialogContent(form);

            return false;
        });

        $(wveWidgetDialogCancelElement).click(function() {
            var baseUrl = /((http|https):\/\/)?(.+?)\/.*?/.exec(document.location)[3];
            var url = 'http://' + baseUrl + wveEditorData.back_url;
            document.location = url;
        });

//        $(cancelElements).click(function() {
//            var firstWidget = $(wveWidgetListElement + ' option:first');
//            if (!firstWidget || firstWidget.length <= 0) {
//                return false;
//            }
//
//            var firstWidgetValue = firstWidget.attr('value');
//
//            $(wveWidgetListElement)
//                .val(firstWidgetValue)
//                .trigger('change');
//            return false;
//        });


        $(wveFormElements).live('submit', function() {
            return wveSubmitAjaxForm($(this));
        });

        $(wveOpenWidgetNewFormElement).click(function() {
            $(wveWidgetListElement).val('');
            wveActivateForm(WVE_FORM_TYPE_CREATE_HTML);

            wveNormaliseOldOriginalElement();
        });

        $(wveOpenWidgetPhoneNewFormElement).click(function() {
            $(wveWidgetListElement).val('');
            wveActivateForm(WVE_FORM_TYPE_CREATE_PHONE);

            wveNormaliseOldOriginalElement();
        });

        $(wveWidgetListElement).unbind().change(function() {
            var htmlId = $(this).val();
            var obj = wveEditorData.objects[htmlId];
            var htmlTag = obj.html_type;

            wveNormaliseOldOriginalElement();

            var elemOriginal = wveGetOriginalElement(htmlId, htmlTag);
            if (elemOriginal.size() > 0) {
                wveSaveOldOriginalElement(htmlId, htmlTag);
                wveChangeStyle(elemOriginal, WVE_STYLE_SELECTED);
                wveMoveDialogTo(elemOriginal);
            }

            wveLoadEditData(htmlId);
        });



        $(wveWidgetDialogElement).dialog(
            {
                title: 'Editor',
                width: 'auto',
                height: 'auto',
                resizable: false,
                close: function() {
                    $(wveWidgetDialogCancelElement).trigger('click');
                }
            }
        );
        $(wveWidgetDialogElement).show();

        wveInitHTML();

        wveUpdateHTMLContent();

        wveActivateForm(WVE_FORM_TYPE_CREATE_HTML);
    });




    function wveMoveDialogTo(object) {
        object = $(object);
        var xy = $(object).offset();
        // xy.top = object.get()[0].offsetTop;//, left: object.get()[0].offsetLeft};

        var otop = xy.top;
        var owb = parseInt(object.css('margin-left')) + parseInt(object.css('margin-right')) + parseInt(object.css('padding-left')) + parseInt(object.css('padding-right'));
        var ohb = parseInt(object.css('margin-top')) + parseInt(object.css('margin-bottom')) + parseInt(object.css('padding-top')) + parseInt(object.css('padding-bottom'));
        var ow = $(object).width() + 20 + owb;
        var oh = $(object).height() + 20 + ohb;
        var ww = $('.ui-dialog').width();
        var wh = $('.ui-dialog').height();
        var dw = document.width;
        var dh = document.height;

        if (xy.left > ww && xy.left - ww - 20 > 0) {
            xy.left -= ww + 20;
            xy.top -= (wh / 2) - (oh / 2);
            if (xy.top < 0) {
                xy.top = 0;
            }
            if (xy.top > dh - wh) {
                xy.top = dh - wh;
            }
        } else if (xy.top > wh && xy.top - wh - oh > 0) {
            xy.top -= wh + 20;
            xy.left -= ww / 2 - ow / 2;
            if (xy.left < 0) {
                xy.left = 0;
            }
            if (xy.left > dh - wh) {
                xy.left = dw - ww;
            }
        } else if (xy.top + oh < dh - wh) {
            xy.left -= ww / 2 - ow / 2;
            xy.top += oh;
        } else if (xy.left + ow < dw - ww) {
            xy.left += ow;
            xy.top -= (wh / 2) - (oh / 2);
            if (xy.top < 0) {
                xy.top = 0;
            }
            if (xy.top > dh - wh) {
                xy.top = dh - wh;
            }
        }

        $('.ui-dialog').animate(xy, 200);
        if (otop < xy.top) {
            $.scrollTo(otop, 200);
        } else {
            $.scrollTo(xy.top, 200);
        }
    }


    function wveInitHTML() {
        $(wveWidgetListElement).empty();

        wveResetStyles();

        $.each(wveEditorData.objects, function(htmlId, obj) {
            var htmlTag = obj.html_type;
            var elemOriginal = wveGetOriginalElement(htmlId, htmlTag);
            if (elemOriginal.size() > 0) {

                wveInitStyle(elemOriginal);
                wveChangeStyle(elemOriginal, WVE_STYLE_NORMAL);

                elemOriginal.unbind().click(function() {
                    var htmlId = $(this).attr('id');
                    $(wveWidgetListElement)
                        .val(htmlId)
                        .trigger('change');
                    //wveMoveDialogTo(this);
                });

                elemOriginal.hover(
                    function() {
                        wveChangeStyle($(this), WVE_STYLE_SELECTED);
                    },
                    function() {
                        if (!wveIsOldOriginalElement($(this))) {
                            wveChangeStyle($(this), WVE_STYLE_NORMAL);
                        }
                    }
                );
            }

            var option = '<option data-id="' + obj['id'] + '" value="' + htmlId + '">' + htmlId + '</option>';
            $(wveWidgetListElement).append(option);
        });
    }

    function wveUpdateAllEditorInstances() {
        for (var i in CKEDITOR.instances) {
            $(CKEDITOR.instances[i].element.$).val(CKEDITOR.instances[i].getData());
        }
    }

    function wveUpdateHTMLContent() {

        $.each(wveEditorData.objects, function(htmlId, obj) {

            var htmlTag = obj.html_type;
            var elemOriginal = wveGetOriginalElement(htmlId, htmlTag);

            if (elemOriginal.size() > 0) {
                if (obj.widget_type == wveEditorData.widget_types.WIDGET) {

                    elemOriginal.html(obj.html_content);

                } else if (obj.widget_type == wveEditorData.widget_types.WIDGET_PHONE) {

                    var hc1 = obj.html_content_1;
                    var hc2 = obj.html_content_2;

                    var numberId = obj.physical_phone_number_id;
                    var number = wveEditorData.physical_phone_numbers[numberId];

                    var formatId = obj.number_format_id;
                    var format = wveEditorData.number_formats[formatId];

                    var hcPhone = wveFormatNumber(number, format);
                    elemOriginal.html(hc1 + hcPhone + hc2);
                }
            }
        });
    }


/* -------------- Form Helpers ------------ */
    function wveSubmitAjaxForm(wveForm) {
        wveUpdateAllEditorInstances();

        var requestParams = wveGetFormRequestParams(wveForm);

        var options = {
            url:        requestParams.url,
            type:       requestParams.method,
            dataType:   'json',

            success:    wveFormSubmitSuccessHandler,
            error:      wveGlobalAjaxErrorHandler

            //NOTE:: we do not use iframe flag because if we use it then request will be
            // not XHR. But we need XHR. So we cannot upload files in such forms.
//            iframe:     true
        };
        wveForm.ajaxSubmit(options);
        return false;
    }

    function wveFormSubmitSuccessHandler(responseText, statusText, xhr, form) {
        wveGlobalAjaxSuccessHandler();
        wveClearFormErrors(form);

        var visibleForm = form;

        if (!responseText) {
            wveMyAjaxGlobalError('Error! Object has not been saved.');

        } else if (responseText['success']) {

            var obj = responseText['object'];
            var objHtmlId = obj['html_id'];

            wveEditorData.objects[objHtmlId] = obj;

            wveClearForm(form);

            wveInitHTML();

            $(wveWidgetListElement).val(objHtmlId).trigger('change');

            wveUpdateHTMLContent();

            var formType = wveGetFormTypeForObject(objHtmlId);
            visibleForm = wveGetFormByType(formType);

        } else if (responseText['error']) {
            wveFormSubmitErrorHandler(responseText, statusText, xhr, form);
        }

        wveResizeDialogContent(visibleForm);
    }

    function wveFormSubmitErrorHandler(responseText, statusText, xhr, form) {
        var formContainerPanel = wveDoGetFormContainerPanel(form)

        var errorsAll = responseText['error'];

        var errorsGlobal = errorsAll ? errorsAll['global'] : {};
        var errorsFormGlobal = errorsAll ? errorsAll['form_global'] : {};
        var errorsGlobalAll = $.merge(errorsGlobal, errorsFormGlobal);
        var errorGlobalElem = formContainerPanel.find(wveErrorGlobalElement);
        if (errorGlobalElem.length > 0) {
            var errGlobalMsg = 'Error! Form has not been saved.';
            $.each(errorsGlobalAll, function(i, errorText) {
                errGlobalMsg += '</br>' + errorText;
            });

            errorGlobalElem.attr('class', wveErrorGlobalShownClass);
            errorGlobalElem.html(errGlobalMsg);
        }


        var errorsLocal = errorsAll ? errorsAll['form_local'] : {};
        var fieldElement = null;
        var fieldContainerElement = null;
        var errorElement = null;
        $.each(errorsLocal, function(errorField, errorText) {
           fieldElement = formContainerPanel.find('#' + errorField);
           fieldContainerElement = fieldElement.parent();
           errorElement = fieldContainerElement.find(wveErrorFieldElement);

           errorElement.html(errorText);
           errorElement.attr('class', wveErrorFieldShownClass);
        });
    }

    function wveGetFormRequestParams(wveForm) {
        var wveFormId = $(wveForm).attr('id');

        var url = '';
        var method = '';

        var create = wveEditorData.urls.create;
        var update = wveEditorData.urls.update;

        switch (wveFormId) {
            case wveFormWidgetNew:
                url = create.url.widget;
                method = create.method;
                break;
            case wveFormWidgetPhoneNew:
                url = create.url.widget_phone;
                method = create.method;
                break;

            case wveFormWidgetEdit:
                var widgetId = wveGetField(wveForm, 'id').val();
                url = wveMakeUrlWithId(update.url.widget, widgetId);
                method = update.method;
                break;
            case wveFormWidgetPhoneEdit:
                var widgetPhoneId = wveGetField(wveForm, 'id').val();
                url = wveMakeUrlWithId(update.url.widget_phone, widgetPhoneId);
                method = update.method;
                break;

            default:
        }

        var baseUrl = /((http|https):\/\/)?(.+?)\/.*?/.exec(document.location)[3];
        var absoluteUrl = 'http://' + baseUrl + url;

        var requestParams = {
            'url':      absoluteUrl,
            'method':   method
        };
        return requestParams;
    }

    function wveGetFormTypeForObject(htmlId) {

        var obj = wveEditorData.objects[htmlId];
        var type = obj ? obj.widget_type : null;

        var formType = null;
        if (type == wveEditorData.widget_types.WIDGET) {
            formType = WVE_FORM_TYPE_UPDATE_HTML;
        } else if (type == wveEditorData.widget_types.WIDGET_PHONE) {
            formType = WVE_FORM_TYPE_UPDATE_PHONE;
        }

        return formType;
    }

    function wveGetFormByType(formType) {

        var form = null;
        switch (formType) {
            case WVE_FORM_TYPE_UPDATE_HTML:
                form = $('#' + wveFormWidgetEdit);
                break;
            case WVE_FORM_TYPE_UPDATE_PHONE:
                form = $('#' + wveFormWidgetPhoneEdit);
                break;
            case WVE_FORM_TYPE_CREATE_HTML:
                form = $('#' + wveFormWidgetNew);
                break;
            case WVE_FORM_TYPE_CREATE_PHONE:
                form = $('#' + wveFormWidgetPhoneNew);
                break;
            default:
        }

        return form;
    }

    function wveActivateForm(formType) {

        wveDoBlockElement();

        $(wveFormContainers).hide();

        var form = wveGetFormByType(formType);

        wveClearFormErrors(form);

        wveClearForm(form);

        var formContainer = form.parents(wveFormContainers + ':first');
        formContainer.show(WVE_DURATION_ACTIVATION_SHOW, function() {
            wveResizeDialogContent($(this));

            wveDoUnBlockElement();
        });
    }

    function wveLoadEditData(htmlId) {
        var formType = wveGetFormTypeForObject(htmlId);
        var form = wveGetFormByType(formType);
        wveActivateForm(formType);

        var obj = wveEditorData.objects[htmlId];
        var type = obj ? obj.widget_type : null;


        $.each(obj, function(fieldName, fieldValue) {
            wveGetField(form, fieldName).val(fieldValue);
        });

        if (type == wveEditorData.widget_types.WIDGET) {
            var wveWidgetHtmlContentElementEdit = wveGetEditWidgetContentName();
            CKEDITOR.instances[wveWidgetHtmlContentElementEdit].setData(obj.html_content);
        }
    }

    function wveClearFormErrors(form) {
        var formContainerPanel = wveDoGetFormContainerPanel(form);

        var errorGlobalElem = formContainerPanel.find(wveErrorGlobalElement);
        if (errorGlobalElem.length > 0) {
            errorGlobalElem.attr('class', wveErrorGlobalHiddenClass);
            errorGlobalElem.html('');
        }

        var errorElements = formContainerPanel.find(wveErrorFieldElement);
        if (errorElements.length > 0) {
            $.each(errorElements, function(i, errorElement){
                $(errorElement).attr('class', wveErrorFieldHiddenClass);
                $(errorElement).html('');
            });
        }
    }

    function wveClearForm(form) {
//        form.find('select, input[type=text], textarea').val('');

        var elems = form.find('li:not(.wve-bionic-form-buttons):not(.wve-bionic-input-hidden)');
        $.each(elems, function(i, elem) {
            var field = $(elem).find(':not(label):not(div):not(input[type=hidden]):first');
            if (field.size()) {
                field.val('');
            }
        });

        for (var i in CKEDITOR.instances) {
            CKEDITOR.instances[i].setData('');
        }
    }

    function wveDoGetFormContainerPanel(form) {
        var formContainerPanel = form.parents('div:first');
        return formContainerPanel;
    }

    function wveGetField(form, fieldName) {
        return $('#' + wveGetFieldName(form, fieldName), form);
    }

    function wveGetFieldName(form, fieldName) {
        var formId = form.attr('id');
        return formId + '_' + fieldName;
    }


/* -------------- Resize Helpers ------------ */
    function wveResizeDialogContent(initiatedForm) {
        var height = $(initiatedForm).height();
        var width  = $(initiatedForm).width();

        wveResizeElement($('#wve-bionic-widget-list'), null, height - 20);
        wveResizeElement($('#wve-bionic-widget-select'), null, height);
    }

    function wveResizeElement(elem, width, height) {

        width  = width  ? width  : elem.width();
        height = height ? height : elem.height();

        width  = (width  != elem.width())  ? width  : elem.width();
        height = (height != elem.height()) ? height : elem.height();

        var options = {
            'width':  width + 'px',
            'height': height + 'px'
        };

        $(elem).animate(options);
    }




/* -------------- Helpers to work with Original site ------------ */
    function wveSaveOldOriginalElement(htmlId, htmlTag) {
        wveOldHtmlId = htmlId;
        wveOldHtmlTag = htmlTag;
    }

    function wveIsOldOriginalElement(elem) {
        var htmlId  = elem.attr('id');
        var htmlTag = elem.attr('tagName');

        if (wveOldHtmlId != htmlId || wveOldHtmlTag.toUpperCase() != htmlTag.toUpperCase()) {
            return false;
        }
        return true;
    }

    function wveGetOriginalElement(htmlId, htmlTag) {
        if (!htmlId || htmlId == '' || !htmlTag || htmlTag == '') {
            return $('');
        }

        var obj = wveEditorData.objects[htmlId];
        htmlTag = htmlTag ? htmlTag : obj.html_type;

        var elemOriginal = $(htmlTag + '#' + htmlId);
        return elemOriginal;
    }

    function wveNormaliseOldOriginalElement() {
        if (wveOldHtmlId) {
            var elemOriginalOld = wveGetOriginalElement(wveOldHtmlId, wveOldHtmlTag);
            wveChangeStyle(elemOriginalOld, WVE_STYLE_NORMAL);
            wveUpdateHTMLContent();
        }
    }

    function wveInitStyle(elemOriginal) {
        elemOriginal = $(elemOriginal);

        wveOriginalStyles[elemOriginal.attr('id')] = {style: elemOriginal.attr('style'), html: elemOriginal.html()};

        var mt = parseInt(elemOriginal.css('margin-top'));
        var mb = parseInt(elemOriginal.css('margin-bottom'));
        var ml = parseInt(elemOriginal.css('margin-left'));
        var mr = parseInt(elemOriginal.css('margin-right'));

        var newCSS = {
           marginTop:       mt - 5,
           marginBottom:    mb - 5,
           marginLeft:      ml - 5,
           marginRight:     mr - 5
        };

        elemOriginal.css(newCSS);
    }

    function wveResetStyles() {
        wveSaveOldOriginalElement('', '');

        $.each(wveOriginalStyles, function(htmlId, wveOriginalStyle) {

            var obj = wveEditorData.objects[htmlId];
            var htmlTag = obj.html_type;
            var elemOriginal = wveGetOriginalElement(htmlId, htmlTag);
            if (elemOriginal.size() > 0) {
                if (wveOriginalStyle.style != undefined) {
                    elemOriginal.attr('style', wveOriginalStyle.style);
                } else {
                    elemOriginal.removeAttr('style');
                }

                elemOriginal.unbind().html(wveOriginalStyle.html);
            }
        });
    }

    function wveChangeStyle(elemOriginal, newCSS) {
        elemOriginal.css(newCSS);
    }



/* -------------- Ckeditor Helpers ------------ */

    function wveGetEditWidgetContentName() {
        var formEditWidget = wveGetFormByType(WVE_FORM_TYPE_UPDATE_HTML);
        var contentEditName = wveGetFieldName(formEditWidget, 'html_content');
        return contentEditName;
    }

    function wveGetNewWidgetContentName() {
        var formNewWidget = wveGetFormByType(WVE_FORM_TYPE_CREATE_HTML);
        var contentNewName = wveGetFieldName(formNewWidget, 'html_content');
        return contentNewName;
    }



/* -------------- Other Helpers ------------ */
    function wveFormatNumber(number, format) {
        var result = '';

        if (!number || !format) {
            return result;
        }

        var j = 0;
        for (var i = 0; (i < format.length && j < number.length); i++) {
            if (format[i] == 'x') {
                result += number[j];
                j++;
            } else {
                result += format[i];
            }
        }
        return result;
    }

    /**
     *
     * @param url url with '.../id/0' part. 0 means that this is place where we should place needId.
     * @param needId
     * @return '.../id/needId'
     */
    function wveMakeUrlWithId(url, needId) {
        var newUrl = url.replace(/(0)/, needId);
        return newUrl;
    }



/* -------------- Errors Helpers ------------ */
/**
 * NOTE:: ajax_global_error.js has been partly used to write this.
 */
    function wveGlobalAjaxErrorHandler(xmlHttpRequest) {
        if (xmlHttpRequest.status == 401) {
            wveRedirectToLogin();
            return;
        }

        if (xmlHttpRequest.statusText) {
            wveMyAjaxGlobalError("Error! Status: "+ xmlHttpRequest.statusText);
        } else {
            wveMyAjaxGlobalError("Unknown error!");
        }
    }

    function wveGlobalAjaxSuccessHandler() {
        wveMyAjaxGlobalError(false);
    }

    function wveMyAjaxGlobalError(message) {
        var errorDiv = $("#ajax_errors");
        if (errorDiv.length) {
            if (message) {
                errorDiv.get(0).innerHTML = message;
                errorDiv.show();
            }
            else {
                errorDiv.hide();
                errorDiv.get(0).innerHTML = "";
            }
        }
        return errorDiv;
    }


    function wveRedirectToLogin() {
        window.location.href = '/';
    }





/* -------------- Blocking Helpers ------------ */
/**
 * NOTE:: is based on ajax_loader.js
 */
    function wveDoBlockElement() {

        var blockingElement = $(wveBlockingElement);

        var blockedParams = {
            message: '<div class="WVE-ajax-loader-message">Loading...</div>',
            theme: true,
            themedCSS: {
                opacity: 0.75,
                backgroundColor: '#F6FFEF',
                width: blockingElement.width() + 'px',
                height: blockingElement.height() + 'px'
            }
        };

        blockingElement.block(blockedParams);
    }

    function wveDoUnBlockElement() {

        var blockingElement = $(wveBlockingElement);
        blockingElement.unblock();
    }

})(jQuery);

//NOTE:: other code using $ as an alias to the other library
