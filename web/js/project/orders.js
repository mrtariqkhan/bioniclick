
var hideOrshowOrderDetailsElement = '.fulfill-order-details-toggle';
var hideOrderDetailsElement = 'fulfill-order-details-hide';
var showOrderDetailsElement = 'fulfill-order-details-show';
var orderDetailsElement     = '.fulfill-order-details-stowable';
var parentElement           = '.fulfill-order-details';
var orderHideSpeed          = 300;

$(function() {

    $('.' + hideOrderDetailsElement).live('click', function() {
        hideDetails(this, orderHideSpeed);
    });

    $('.' + showOrderDetailsElement).live('click', function() {
        showDetails(this, orderHideSpeed);
    });
});

function hideDetails(elem, speed) {
    showOrHideDetails(elem, speed, false);
}

function showDetails(elem, speed) {
    showOrHideDetails(elem, speed, true);
}

function showOrHideDetails(elem, speed, doShow) {

    var orderDetailsElem            = $(elem).parents(parentElement).find(orderDetailsElement);
    var hideOrShowOrderDetailsElem  = $(elem).parents(parentElement).find(hideOrshowOrderDetailsElement);

    if (orderDetailsElem.length > 0) {
        if (doShow) {
            $(orderDetailsElem).show(speed);
            if (hideOrShowOrderDetailsElem.length > 0) {
                $(hideOrShowOrderDetailsElem).removeClass(showOrderDetailsElement);
                $(hideOrShowOrderDetailsElem).addClass(hideOrderDetailsElement);
                $(hideOrShowOrderDetailsElem).attr('title', 'Hide details');
            }
        } else {
            $(orderDetailsElem).hide(speed);
            if (hideOrShowOrderDetailsElem.length > 0) {
                $(hideOrShowOrderDetailsElem).removeClass(hideOrderDetailsElement);
                $(hideOrShowOrderDetailsElem).addClass(showOrderDetailsElement);
                $(hideOrShowOrderDetailsElem).attr('title', 'Show details');
            }
        }
    }
}