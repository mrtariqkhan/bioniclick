var ajaxFormDeleteElement = '.ajax-delete-form';


//NOTE:: whenever You use ajax_delete.js You must define deleteEventCallbacks:
//var deleteEventCallbacks = {
//    itemDeletedSuccess: function(deleteForm) {
//        ...
//    }
//}

$(document).ready(function() {
    deleteProcessing();
});

function deleteProcessing() {

    $(ajaxFormDeleteElement).live('submit', function() {
        return submitAjaxFormDelete($(this));
    });

    $('.ajax-delete').live('click', function(event) {
        if (confirm('Are you sure?')) {
            var crsf_field_name     = $(this).attr('ajax_delete_csrf_name');
            var crsf_field_value    = $(this).attr('ajax_delete_csrf_value');
            var special_form_data   = $(this).attr('ajax_delete_special_form_data');
            var after_url           = $(this).attr('after_url');
            var method              = $(this).attr('ajax_delete_method');
            var url                 = getAjaxRealHref(this);

            var form =
'<form\n\
    class="ajax-delete-form"\n\
    method="post"\n\
    action="' + url + '"\n\
    special_data="' + special_form_data + '"\n\
    after_url="' + after_url + '"\n\
>\n\
    <input\n\
        type="hidden"\n\
        name="sf_method"\n\
        value="' + method + '"\n\
    />\n\
    <input\n\
        type="hidden" \n\
        name="' + crsf_field_name + '"\n\
        value="' + crsf_field_value + '"\n\
    />\n\
</form>';
            $(this).append(form);
            $(ajaxFormDeleteElement).submit();
        }
        return false;
    });
}


function submitAjaxFormDelete(form) {
    var options = {
        success:    ajaxFormDeleteSuccessHandler,
        error:      globalAjaxErrorHandler,
        dataType:   'json'

        //NOTE:: we do not use iframe flag because if we use it then request will be
        // not XHR. But we need XHR. So we cannot upload files in such forms.
//        iframe:     true
    };
    form.ajaxSubmit(options);
    return false;
}

function ajaxFormDeleteSuccessHandler(responseText, statusText, xhr, form) {
    globalAjaxSuccessHandler();

    if (!responseText) {
        return;
    }

    if (responseText['success']) {
        if (!invokeItemDeletedSuccess(form)) {
            return;
        }

    } else if (responseText['error']) {
        myAjaxGlobalError('Error! Object has not been deleted. ' + responseText['error']);
    }
}

/**
 * returns true if need deleteEventCallbacks exists
 */
function invokeItemDeletedSuccess(deleteForm) {
    if (window.deleteEventCallbacks && deleteEventCallbacks['itemDeletedSuccess']) {
        deleteEventCallbacks['itemDeletedSuccess'](deleteForm);
        return true;
    }
    return false
}