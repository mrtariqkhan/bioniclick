
function reloadTableOrTabByPager(element, page, maxPerPage) {
    var tabIndex = getTabIndexByElement(element);
    if (tabIndex !== null) {
        var tabPanel = getTabPanelByIndex(tabIndex);

        if (!maxPerPage && tabPanel.length) {
            var selectedOption = tabPanel.find('.ajax-pager .max-per-page option:selected');
            maxPerPage = selectedOption.length ? selectedOption.val() : '';
        }

        var params = {
            'pager': {
                'page'          : page,
                'max_per_page'  : maxPerPage
            }
        };

//        if (eventCallbacks && eventCallbacks.reloadTableDiv) {
//            //NOTE:: if need to reload only table, not full page.
//            eventCallbacks.reloadTableDiv(params);
//        } else {
            reloadTabWithParams(tabIndex, params);
//        }
    }
}

$('.ajax-pager a').live('click', function() {
    var page = $(this).attr('pager_data');
    reloadTableOrTabByPager(this, page);
    return false;
});


$('.ajax-pager .max-per-page').live('change', function() {
    var selectedOption = $(this).find('option:selected');
    var maxPerPage = selectedOption.length ? selectedOption.val() : '';
    reloadTableOrTabByPager(this, 1, maxPerPage);
});