$.ajaxSetup({
    error: globalAjaxErrorHandler,
    success: globalAjaxSuccessHandler
});

function globalAjaxErrorHandler(event, jqXHR) {

    if (!event) {
        return myAjaxGlobalError("Error!");
    }

    if (event.status == 401) {
        return redirectToLogin();
    }

    if ($.type(event.responseText) === "string") {
        return myAjaxGlobalError("Unknown error!");
    }

    var responseJSON = $.parseJSON(event.responseText)
    if (responseJSON &&
        responseJSON.jsonrpc == '2.0' &&
        responseJSON.error
    ) {
        return myAjaxGlobalError('Error ' + responseJSON.error.code + ': ' + responseJSON.error.message);
    }
    
    return myAjaxGlobalError("Unknown error!");
}

function globalAjaxSuccessHandler(event, XMLHttpRequest, ajaxOption) {
    
    myAjaxGlobalError(false);
}

function myAjaxGlobalError(message) {
    var errorDiv = $("#ajax_errors");
    if (errorDiv.length) {
        if (message) {
            errorDiv.get(0).innerHTML = message;
            errorDiv.show();
        }
        else {
            errorDiv.hide();
            errorDiv.get(0).innerHTML = "";
        }
    }
    return errorDiv;
}


function redirectToLogin() {
    location.reload();
}