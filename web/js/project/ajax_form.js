var ajaxFormElement = '.ajax-form';
var ajaxFormCancelElement = '.ajax-form-cancel';

var errorGlobalElement = '.bionic-ajax-field-error-global';
var errorGlobalHiddenClass = 'bionic-ajax-field-error-global bionic-ajax-field-error-global-hidden';
var errorGlobalShownClass  = 'bionic-ajax-field-error-global bionic-ajax-field-error-global-shown';

var errorFieldElement = '.bionic-ajax-field-error';
var errorFieldHiddenClass = 'bionic-ajax-field-error bionic-ajax-field-error-hidden';
var errorFieldShownClass  = 'bionic-ajax-field-error bionic-ajax-field-error-shown';



//NOTE:: whenever You use ajax_form.js You must define formCallbacks, formEventCallbacks:
//var formCallbacks = {
//    reloadFormContainer: function(elementFromFormPanel, url, methodType) {
//        ...
//    },
//    getFormContainerPanel: function(form) {
//        ....
//    }
//}
//var formEventCallbacks = {
//    formSavedSuccess: function(form, ajaxFormCancelElem) {
//        ...
//    }
//}

$(ajaxFormsProcessing);

function ajaxFormsProcessing() {
    $('a.ajax-form-load').live('click', function() {
        return loadAjaxForm($(this));
    });

    $(ajaxFormCancelElement).live('click', function() {
        return cancelAjaxForm($(this));
    });

    $(ajaxFormElement).live('submit', function() {
        return submitAjaxForm($(this));
    });
}



function loadAjaxForm(element) {
    var formUrl = getAjaxRealHref(element);
    if (!doReloadFormContainer(element, formUrl, 'GET')) {
        return false;
    }
    return false;
}

function cancelAjaxForm(cancelElement) {
    var previousUrl = getAjaxRealHref(cancelElement);

    var previousUrlMethod = $(cancelElement).attr('method_type');
    previousUrlMethod = previousUrlMethod ? previousUrlMethod : 'POST';

    var backUrlType = $(cancelElement).attr('back_url_type');
    backUrlType = backUrlType ? backUrlType : 'in-tab';

    if (backUrlType == 'in-tab') {
        if (!doReloadFormContainer(cancelElement, previousUrl, previousUrlMethod)) {
            return false;
        }
    } else {
        document.location = previousUrl;
        return false;
    }

    return false;
}

function submitAjaxForm(form) {
    globalAjaxSuccessHandler();
    clearFormErrors(form);
    var options = {
        success:    ajaxFormSuccessHandler,
        error:      globalAjaxErrorHandler,
        dataType:   'json'

        //NOTE:: we do not use iframe flag because if we use it then request will be
        // not XHR. But we need XHR. So we cannot upload files in such forms.
//        iframe:     true
    };
    form.ajaxSubmit(options);
    return false;
}




function ajaxFormSuccessHandler(responseText, statusText, xhr, form) {
    globalAjaxSuccessHandler();
    clearFormErrors(form);

    if (!responseText) {
        return;
    }

    if (responseText['success']) {
        var formContainerPanel = doGetFormContainerPanel(form);
        var ajaxFormCancelElem = formContainerPanel.find(ajaxFormCancelElement);

        if (!invokeFormSavedSuccess(form, ajaxFormCancelElem)) {
            return;
        }

    } else if (responseText['error']) {
        ajaxFormErrorHandler(responseText, statusText, xhr, form);
    }
}

function ajaxFormErrorHandler(responseText, statusText, xhr, form) {
    var formContainerPanel = doGetFormContainerPanel(form);

    var errorsAll = responseText['error'];

    var errorsGlobal = errorsAll ? errorsAll['global'] : {};
    var errorsFormGlobal = errorsAll ? errorsAll['form_global'] : {};
    var errorsGlobalAll = $.merge(errorsGlobal, errorsFormGlobal);
    var errorGlobalElem = formContainerPanel.find(errorGlobalElement);
    if (errorGlobalElem.length > 0) {
        var errGlobalMsg = 'Error! Form has not been saved.';
        $.each(errorsGlobalAll, function(i, errorText) {
            errGlobalMsg += '</br>' + errorText;
        });

        errorGlobalElem.attr('class', errorGlobalShownClass);
        errorGlobalElem.html(errGlobalMsg);
    }


    var errorsLocal = errorsAll ? errorsAll['form_local'] : {};
    var fieldElement = null;
    var fieldContainerElement = null;
    var errorElement = null;
    $.each(errorsLocal, function(errorField, errorText) {
       fieldElement = formContainerPanel.find('#' + errorField);
       fieldContainerElement = fieldElement.parents('div:first');
       errorElement = fieldContainerElement.find(errorFieldElement);

       errorElement.html(errorText);
       errorElement.attr('class', errorFieldShownClass);
    });
}

function clearFormErrors(form) {
    var formContainerPanel = doGetFormContainerPanel(form);

    var errorGlobalElem = formContainerPanel.find(errorGlobalElement);
    if (errorGlobalElem.length > 0) {
        errorGlobalElem.attr('class', errorGlobalHiddenClass);
        errorGlobalElem.html('');
    }

    var errorElements = formContainerPanel.find(errorFieldElement);
    if (errorElements.length > 0) {
        $.each(errorElements, function(i, errorElement){
            $(errorElement).attr('class', errorFieldHiddenClass);
            $(errorElement).html('');
        });
    }
}







/* ------ Helpers to make usage of formCallbacks, formEventCallbacks more simple. ------ */

/**
 * returns true if need formCallback exists
 */
function doReloadFormContainer(elementFromFormPanel, url, methodType) {
    if (window.formCallbacks && formCallbacks['reloadFormContainer']) {
        formCallbacks['reloadFormContainer'](elementFromFormPanel, url, methodType);
        return true;
    }
    return false;
}

/**
 * returns formContainerPanel if need formCallback exists,
 *          otherwise returns empty element.
 */
function doGetFormContainerPanel(form) {
    var formContainerPanel = $('');

    if (window.formCallbacks && formCallbacks['getFormContainerPanel']) {
        formContainerPanel = formCallbacks['getFormContainerPanel'](form);
    }

    return formContainerPanel;
}

/**
 * returns true if need formEventCallbacks exists
 */
function invokeFormSavedSuccess(form, ajaxFormCancelElem) {
    if (window.formEventCallbacks && formEventCallbacks['formSavedSuccess']) {
        formEventCallbacks['formSavedSuccess'](form, ajaxFormCancelElem);
        return true;
    }
    return false
}
