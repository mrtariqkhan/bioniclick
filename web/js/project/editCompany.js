
$('#company_export_date_range').livequery(function() {
    $('#company_export_date_range').daterangepicker();
});

$('#company_export').livequery(function() {
    $('#company_export').change(function () {
        updateExportBox();
    });
    updateExportBox();
});

$('#company_date_range_export').livequery(function() {
    $('#company_date_range_export').change(function () {
        updateDateRangeBox();
    });
    updateDateRangeBox();
});

function updateExportBox(){
    var exportChecked = $('#company_export').is(':checked');
    if(exportChecked)
        $('#company_date_range_export').parent().show();
    else
        $('#company_date_range_export').parent().hide();
    updateDateRangeBox();
}

function updateDateRangeBox(){
    var exportChecked = $('#company_export').is(':checked');
    var dateRangeExportchecked = $('#company_date_range_export').is(':checked');

    if(dateRangeExportchecked && exportChecked)
        $('#company_export_date_range').parent().show();
    else
        $('#company_export_date_range').parent().hide();
}