var chartTopClientsElement      = '#chart-top-clients';
var chartTopClientsHoverElement = "#chart-top-clients-hover";
var chartTopClientsLegendElement= "#chart-top-clients-legend";

var chartCallsByParameterElement        = '#chart-calls-by-parameter';
var chartCallsByParameterHoverElement   = "#chart-calls-by-parameter-hover";
var chartCallsByParameterLegendElement  = "#chart-calls-by-parameter-legend";

$(document).ready(function() {

    loadChartTopClients();
    loadChartCallsByParamater();
});

function loadChartTopClients() {
    loadChart(
        chartTopClientsElement,
        chartTopClientsHoverElement, 
        chartTopClientsLegendElement
    );
}

function loadChartCallsByParamater() {
    loadChart(
        chartCallsByParameterElement, 
        chartCallsByParameterHoverElement, 
        chartCallsByParameterLegendElement
    );
}

function loadChart(chartElement, chartHoverElement, chartLegendContainerElement) {

    $(chartHoverElement).html('');
    $(chartLegendContainerElement).html('');
    $(chartElement).html('');

    setChartIsLoading(chartElement);

    var chartLegendContainerElem = $(chartLegendContainerElement);

    var url = $(chartElement).attr('data_load_url');
    if (!url) {
        return;
    }

    $.post(url, function(response) {

        var jsonObject = response;
        if (response && (typeof jsonObject == 'object')) {
            var data        = jsonObject['data'];
            var needLinks   = jsonObject['need_links'];
            var escapeLinks = jsonObject['escape_links'];

            var dataArr = [];
            var j = 0;
            for (var i in data) {
            	j++;
            	var name = i.substring( i.indexOf('.') + 1 );
                dataArr.push({
                    label   : j + '. ' + name,
                    data    : parseInt(data[i])
                });
            }
            j = 0;
            
            setChartIsNotLoading(chartElement);

            if (dataArr.length > 0) {
                $.plot(
                    $(chartElement),
                    dataArr,
                    {
                        series: {
                            pie: {
                                show: true,
                                radius: 1,  
                                label: {
                                    show: false,
                                    formatter: function(label, series) {
                                        var newLabel = series.label;
                                        newLabel = newLabel.replace(/(\s)/, '<br/>');

                                        if (isNaN(series.percent)) {
                                            series.percent = 0;
                                        }
                                        return '<span class="chart-small-label">'
                                                + '<b>' + newLabel +'</b>'
                                                + '<br/>'
                                                + '(' + Math.round(series.percent) + '%)'
                                                + '</span>'
                                        ;
                                    },
                                    radius: 0.6,
    //                                background: {
    //                                    color: '#000',
    //                                    opacity: 0.1
    //                                },
                                    threshold: 0.1
                                }
                            }
                        },
                        legend: {
                            show: true,
                            position: "ne",
//                            margin: [-200, 0],
                            container: chartLegendContainerElem,
                            labelFormatter: function(label, series) {
                                if (isNaN(series.percent)) {
                                    series.percent = 0;
                                }

                                var escaped = false;
                                $.each(escapeLinks, function(i, escapeLink) {
                                    if (escapeLink == series.label) {
                                        escaped = true;
                                    }
                                });

                                var newLabel = '';
                                if (needLinks && !escaped) {
                                    var re = /^([0-9]+\s*.\s*)(\w+.*)/ig;
                                    var parts = re.exec(series.label);
                                    var prefix  = parts[1];
                                    var href    = parts[2];
                                    newLabel = 
                                        '<b>' + prefix + '</b>'
                                        + '<a class="chart-label-link" target="_blank" href="' + href + '" title="' + href + '">'
                                            + href
                                        + '</a>'
                                    ;
                                } else {
                                    newLabel = series.label;
                                }

                                newLabel = 
                                    '<span class="chart-label">'
                                        + '<b>' 
                                            + newLabel
                                        + '</b>'
                                        + '<br/>'
                                        + series.data[0][1] + ' (' + Math.round(series.percent) + '%)'
                                    + '</span>'
                                ;

                                return newLabel;
                            }
                        },
                        grid: {
                            hoverable: true
                        }
                    }
                );
            }
        } else {
            setChartIsNotLoading(chartElement);
        }
    });
    $(chartElement).bind("plothover", pieHover);

    function pieHover(event, pos, obj) {
        if (!obj) {
            return;
        }
        var percent = parseFloat(obj.series.percent).toFixed(2);
        $(chartHoverElement).html(
            '<span style="font-weight: bold; color: '
            + obj.series.color
            + '">'
            + obj.series.label
            + ' (' + percent + '%)</span>');
    }
}

