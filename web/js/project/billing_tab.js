var creditCardsTableContainer = '.credit-cards';
var upLinksElements = creditCardsTableContainer + ' .priority-up';
var downLinksElements = creditCardsTableContainer + ' .priority-down';

$(document).ready(function() {

    processUpDown();
});


/* ------ Other ------ */
function processUpDown() {

    $(upLinksElements).click(function() {
        return reloadCreditCardsTableContainer(this);
    });

    $(downLinksElements).click(function() {
        return reloadCreditCardsTableContainer(this);
    });
}

function reloadCreditCardsTableContainer(elem) {

    var url = getAjaxRealHref(elem);
    $(creditCardsTableContainer).load(url);

    return false;
}