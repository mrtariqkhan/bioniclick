 var Advertiser = {
 	
 	allNames: Array(),
 	
 	assignTextboxesFilteringMenu : function(){
 		if(Advertiser.allNames.length == 0)
	 		$.get("/api/agencyAdvertisers", function(data) {
		        var availableTags = $.parseJSON(data);
		        Advertiser.allNames = availableTags;

		        //autocomplete text
		        $("#call_log_filter_branching_advertiser").livequery(function(){
		        	$("#call_log_filter_branching_advertiser").autocomplete({
			            source: Advertiser.allNames, minLength:0,
			        });
		        });
		        $("#call_log_filter_branching_advertiser").live('focus', function(){
					$("#call_log_filter_branching_advertiser").autocomplete( "search", $("#call_log_filter_branching_advertiser").val());
			    });
		    });
 	},
 };
