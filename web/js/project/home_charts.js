var chartCallSummarySelectElement       = '.ajax-chart-call-summary-filter select';
var chartCallsByParameterSelectElement  = '.ajax-chart-calls-by-parameter-filter select';
var chartCallStatusSelectElement        = '.ajax-chart-call-status-filter select';
var chartTopClientsSelectElement        = '.ajax-chart-top-clients-filter select';

var classChartIsLoading                 = 'chart-is-loading';

$(document).ready(function() {

    formSubmitOnChangedSelect(chartCallSummarySelectElement);
    formSubmitOnChangedSelect(chartCallsByParameterSelectElement);
    formSubmitOnChangedSelect(chartCallStatusSelectElement);
    formSubmitOnChangedSelect(chartTopClientsSelectElement);
});

function formSubmitOnChangedSelect(selectElement) {
    $(selectElement).live('change', function() {
        var form = $(this).parents('form:first');
        form.submit();
    });
}

function setChartIsLoading(elem) {
    if ($(elem).length > 0) {
        $(elem).html('');
        $(elem).addClass(classChartIsLoading);
    }
}

function setChartIsNotLoading(elem) {
    if ($(elem).length > 0) {
        $(elem).removeClass(classChartIsLoading);
    }
}
