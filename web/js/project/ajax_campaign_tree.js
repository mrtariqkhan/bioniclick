var treeElement = 'div.campaign-tree';
var loadableContainer = 'div.campaign-ajax-loadable';
var treeScrollableContainerElement = '.sidebar-block-content-first';
var headerElement = '#header';
var footerElement = '#footer';

var formCallbacks = {
    reloadFormContainer: function(elementFromFormPanel, url, methodType) {
        getData(url);
    },
    getFormContainerPanel: function(form) {
        var formContainerPanel = form.parents(loadableContainer + ':first');
        return formContainerPanel;
    }
}
var formEventCallbacks = {
    formSavedSuccess: function(form, ajaxFormCancelElem) {

        if (ifNeedToReloadTreeNodeParent(form)) {
            reloadCampaignTree(true, false, false);
            return;
        } else if (ifNeedToReloadTreeNodeSelf(form)) {
            reloadCampaignTree(false, true, false);
            return;
        } else if (ifNeedToReloadTree(form)) {
            reloadCampaignTree(false, false, false);
            return;
        }

        var key = getReloadKey(form);
        if (key != false) {
            reloadCampaignTree(false, false, key);
            return;
        }

        cancelAjaxForm(ajaxFormCancelElem);
    }
}
var deleteEventCallbacks = {
    itemDeletedSuccess: function(deleteForm) {
        if (ifNeedToReloadTreeNodeParent(deleteForm)) {
            reloadCampaignTree(true, false, false);
            return;
        } else if (ifNeedToReloadTreeNodeSelf(deleteForm)) {
            reloadCampaignTree(false, true, false);
            return;
        } else if (ifNeedToReloadTree(deleteForm)) {
            reloadCampaignTree(false, false, false);
            return;
        }
        
        var key = getReloadKey(deleteForm);
        if (key != false) {
            reloadCampaignTree(false, false, key);
            return;
        }

        var url = deleteForm.attr('after_url');
        getData(url);
    }
}
var filterEventCallbacks = {
    filteredSuccess: function(filter, filterContainer) {
        var tableElement = filterContainer.find('.table');
        reloadPagable(tableElement, 1);
    }
}


$(function() {

    ajaxFiltersProcessing(filterEventCallbacks);

    $('a.ajax-load').die().live('click',function() {
        var url = getAjaxRealHref(this);
        getData(url);

        if ($(this).hasClass('edit') || $(this).hasClass('new')) {
            if (typeof window.formOnLoad == 'function') {
                var form = $(loadableContainer).find('form:first');
                formOnLoad($(form));
            }
        }

        return false;
    });

    reloadCampaignTree(false, false, false);

    $(window).resize(function() {
        resizeTreeContainer();
    });
});

function reloadCampaignTree(doReloadNodeParent, doReloadNodeSelf, key) {
    
    resizeTreeContainer();

    setLastLoadedUrl(location.href);

    var url = $(treeElement).attr('data_url');

    var ajaxDefaultOptions = {
        cache: false,
        dataType: "json"
    };
    var ajaxOptions = {
        'url': url,
        'data': {},

        addActiveKey: true,         // NOTE:: add &activeKey= parameter to URL
        addExpandedKeyList: true    // NOTE:: add &expandedKeyList= parameter to URL
//        addFocusedKey: true,        // NOTE:: add &focusedKey= parameter to URL
//        success: reloadTreeSuccessHandler,// NOTE:: initAjax.success callback is ignored when onPostInit was specified.
//        error: reloadTreeErrorHandler     // NOTE:: initAjax.error callback is ignored when onPostInit was specified.
    };

    var treeRoot = $(treeElement).dynatree('getRoot');
    var isInitialisedDynatree = /DynaTreeNode/.exec(treeRoot.toString());

    if (isInitialisedDynatree) {
        //FIXME:: 1). merges ajax options (new options have more high prioprity),
        //        2). reloads the existing tree with merged options.

        var dtnodeToReactivate = null;
        var dtnode = getNeedActiveDTNode();
        if (doReloadNodeParent) {
            dtnodeToReactivate = dtnode.getParent();
        } else if (doReloadNodeSelf) {
            dtnodeToReactivate = dtnode;
        } else if (key) {
            var tree = $(treeElement).dynatree("getTree");
            dtnodeToReactivate = tree.getNodeByKey(key);
        }

        reloadChildrenOfNode(dtnodeToReactivate);
        reloadTreeSuccessHandler();

    } else {
        $(treeElement).dynatree({
            
            ajaxDefaults: ajaxDefaultOptions,
            initAjax: ajaxOptions,

            persist: true,
            cookieId: 'campaign-dynatree',
            cookie: {
                expires: null
//                path: location.href
            },
            
            //NOTE:: for lazy tree
            onPostInit: function(isReloading, isError) {
                // In lazy mode, this will be called *after* the initAjax request returned.
                // 'this' is the current tree
                // @param isReloading is set, if status was read from existing cookies
                if (isError) {
                    //NOTE:: Ajax is failed
                    reloadTreeErrorHandler();
                } else {
                    reloadTreeSuccessHandler();
                }
            },
            onLazyRead: reloadChildrenOfNode,

            onQueryActivate: onQueryActivateHandler,
            //onActivate: loadNodeContent,
            
            onRender: function(node, nodeSpan) {
                
                // if active node = "default-load", changed on first children node.
                if (node.tree.activeNode) {
                    if (node.tree.activeNode.data.key == 'default-load'){
                        
                        node.tree.activeNode.childList[0].activate();
                    }
                    
                    // remove calback onRender
                    node.tree.options.onRender = null;
                }
            },
            
            onKeydown: function(node, e){
                //console.log(['down', node, e]);
                
                node.tree.lastEvent = 'down';
            },
            
            onKeypress: function(node, e){
                
                // normal keypress
                if ( node.tree.lastEvent == 'pres' ) {
                    var handling = node._onKeydown(
                        jQuery.Event("keydown", {which: e.keyCode})
                    );
                } else {
                    var handling = true;
                }
                
                node.tree.lastEvent = 'pres';
                
                return handling;
            },
            
            activeVisible: true,
            //autoFocus: true,
            autoCollapse: false,

            keyboard: true,

            minExpandLevel: 2,
            clickFolderMode: 1,
            imagePath: '/images/dynatree/',
            classNames: {
                nodeWait: 'dynatree-statusnode-wait-special'
            }
            //, debugLevel: 2 //0:quiet, 1:normal, 2:debug
        })
        .dynatree("getTree");
    }
}

function getNeedActiveDTNode() {

    //FIXME:: load content for active node if exist, for default node otherwise.
    var tree = $(treeElement).dynatree("getTree");
    var activeNode = $(treeElement).dynatree("getActiveNode");

    var selectedKey = $(treeElement).attr('selected_key');
    var forcedActiveNode = tree.getNodeByKey(selectedKey);

    if (forcedActiveNode) {
        if (activeNode && forcedActiveNode == activeNode) {
            return activeNode;
        } else {
            return forcedActiveNode;
        }
    } else {
        if (activeNode) {
            return activeNode;
        } else {
            var rootChild = tree.getRoot().childList;
            return rootChild[rootChild.length - 1];
        }
    }
}

function reloadTreeSuccessHandler() {
    globalAjaxSuccessHandler();

    //FIXME:: load content for active node if exist, for default node otherwise.
    var tree = $(treeElement).dynatree("getTree");

    var key = null;
    var dtnode = getNeedActiveDTNode();
    if (dtnode) {
        key = dtnode.data.key;
    }

    var activeNode = $(treeElement).dynatree("getActiveNode");
    var activeNodeKey = (activeNode) ? activeNode.data.key : null;
    
    if (activeNodeKey && activeNodeKey == key) {
        tree.reactivate();
    } else {
        tree.activateKey(key);
    }
}

function reloadTreeErrorHandler(data, XMLHttpRequest, textStatus, errorThrown) {
    globalAjaxErrorHandler(XMLHttpRequest);
}

function reloadChildrenOfNode(dtnode) {
    
    var tree = $(treeElement).dynatree('getTree');
    
    if (dtnode) {
        var url = $(treeElement).attr('data_lazy_url');
    
        var pers = tree.getPersistData();
        var activeKey       = pers.activeKey;
        var expandedKeyList = pers.expandedKeyList.join(",");

        var params = {
            'type': dtnode.data.lazy_params.type,
            'parameter': dtnode.data.lazy_params.parameter,
            'activeKey': activeKey,
            'expandedKeyList': expandedKeyList
        };

        dtnode.appendAjax(
            {
                url: url,
                data: params,
                type: 'post',
                success: lazyLoadTreeSuccessHandler,
                error: lazyLoadTreeErrorHandler
            }
        );
        
    } else {
        
        tree.reload();
    }
}

function lazyLoadTreeSuccessHandler(dtnode) {
    
    // Automatically deactivate type "folder" the node if no child elements
    if (!dtnode.hasChildren()) {
        
        if (dtnode.isFocused()) {
            dtnode.data.focus = true;
        }
        
        setTimeout(function(){
            dtnode.data.isLazy = false;
            dtnode.removeChildren();
            
            dtnode.expand(false);
            
            if (dtnode.data.focus) {
                dtnode.focus();
            }
        }, 0);
    }
    
    globalAjaxSuccessHandler();
}


function lazyLoadTreeErrorHandler(data, XMLHttpRequest, textStatus, errorThrown) {
    globalAjaxErrorHandler(XMLHttpRequest);
}


function onQueryActivateHandler(flag, dtnode) {
    
    if (flag) {
        if (dtnode.data.url) {
            getData(dtnode.data.url);   
        } else {
            return false;
        }
    }
}


function getData(url) {
//    if (getLastLoadedUrl() == url) {
//        return;
//    }
    if (!url) {
        $(loadableContainer).html('');
        return;
    }
    setLastLoadedUrl(url);

    $.ajax({
        url: url,
        success: function(response, statusText, jqXHR) {
            globalAjaxSuccessHandler();
            
            if ($.type(response) === "string") {
                $(loadableContainer).html(response);
            } else {
                var jsonObject = response;
                if (jsonObject['error']) {
                    myAjaxGlobalError(jsonObject['error']);
                } else {
                    getData(jsonObject['url']);
                    return;
                }
            }
        },
        error: function(XMLHttpRequest, textStatus, errorThrown) {
            globalAjaxErrorHandler(XMLHttpRequest);
        }
    });
}


function setLastLoadedUrl(url) {
    $(treeElement).data('lastLoadedUrl', url);
}


function getLastLoadedUrl() {
    //TODO:: think if we can use cookies from dynatree to save url.
//    var tree = $(treeElement).dynatree("getTree");
//    var treeCookies = tree.getPersistData();
//    var lastLoadedUrl = treeCookies['lastLoadedUrl'];

    return $(treeElement).data('lastLoadedUrl');
}


function ifNeedToReloadTree(elementWithSpecialData) {
    var ifNeedToReloadFlag = elementWithSpecialData.attr('special_data');
    return (ifNeedToReloadFlag == 'do-reload-tree');
}
function ifNeedToReloadTreeNodeParent(elementWithSpecialData) {
    var ifNeedToReloadFlag = elementWithSpecialData.attr('special_data');
    return (ifNeedToReloadFlag == 'do-reload-tree-node-parent');
}
function ifNeedToReloadTreeNodeSelf(elementWithSpecialData) {
    var ifNeedToReloadFlag = elementWithSpecialData.attr('special_data');
    return (ifNeedToReloadFlag == 'do-reload-tree-node-self');
}
function ifNeedToReloadContent(elementWithSpecialData) {
    var ifNeedToReloadContent = elementWithSpecialData.attr('special_data');
    return (ifNeedToReloadContent == 'do-reload-content');
}
function getReloadKey(elementWithSpecialData) {
    var ifNeedToReloadFlag = elementWithSpecialData.attr('special_data');
    return (ifNeedToReloadFlag != '') ? ifNeedToReloadFlag : false;
}

function resizeTreeContainer() {

    var newHeight = $(window).height() - $(headerElement).height() - $(footerElement).height();

    //TODO:: think about padding, do not use 100
    newHeight = newHeight - 100;

    $(treeScrollableContainerElement).height(newHeight + 'px');
}