var ajaxLoaderHandlerElements   = '.ajax-loader-handler';
var ajaxBlockingElement         = '.ajax-blocked';

$(startLoader);


function startLoader() {

    $(ajaxLoaderHandlerElements).unbind();

    $(ajaxLoaderHandlerElements).ajaxSend(function() {
        var blockingElement = $(ajaxBlockingElement);
        
        var blockedParams = {
            message: '<div class="ajax-loader-message">Loading...</div>',
            theme: true,
            themedCSS: {
                opacity: 0.75,
                backgroundColor: '#F6FFEF',
                width: blockingElement.width() + 'px',
                height: blockingElement.height() + 'px'
            }
        };
        
        doBlock(blockingElement, blockedParams);
    });

    $(ajaxLoaderHandlerElements).ajaxComplete(function() {
        var blockingElement = $(ajaxBlockingElement);
        doUnBlock(blockingElement);
    });
}

/* ------ Blocking ------ */

/**
 *Blocks element. It continue blocking element if this element is blocked.
 *@param element - blockingElement.
 *@param blockedParams - params for blocking
 *@see jquery.blockUI.js
 */
function doBlock(element, blockedParams) {
    if (!isBlocked(element)) {
        element.block(blockedParams);
    }
    addBlocker(element);
}

/**
 *UnBlocks element. It continue blocking element if this element is blocked by
 *some another request.
 *@param element - blockingElement.
 *@see jquery.blockUI.js
 */
function doUnBlock(element) {
    deleteBlocker(element);
    if (!isBlocked(element)) {
        element.unblock();
    }
}


/* ------ Blocking Helpers ------ */

/**
 *@param element - blockingElement.
 *@return amount of blockers.
 */
function getBlockersCount(element) {
    var ajaxBlockersCount = element.attr('ajax_blockers_count');
    if (!ajaxBlockersCount) {
        setBlockersCount(element, 0);
        ajaxBlockersCount = element.attr('ajax_blockers_count');
    }
    return ajaxBlockersCount;
}

/**
 *@param element - blockingElement.
 *@param ajaxBlockersCount - amount of blockers.
 */
function setBlockersCount(element, ajaxBlockersCount) {
    ajaxBlockersCount = (ajaxBlockersCount < 0) ? 0 : ajaxBlockersCount;
    element.attr('ajax_blockers_count', ajaxBlockersCount);
}

/**
 *@param element - blockingElement.
 *@return true if element has blockers.
 */
function isBlocked(element) {
    var ajaxBlockersCount = getBlockersCount(element);
    return (ajaxBlockersCount > 0);
}


/**
 *Adds one blocker.
 *@param element - blockingElement.
 */
function addBlocker(element) {
    var ajaxBlockersCount = getBlockersCount(element);
    setBlockersCount(element, ++ajaxBlockersCount);
}

/**
 *Deletes one blocker.
 *@param element - blockingElement.
 */
function deleteBlocker(element) {
    var ajaxBlockersCount = getBlockersCount(element);
    setBlockersCount(element, --ajaxBlockersCount);
}