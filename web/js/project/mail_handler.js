/*** autocomplete for mail text**/
$("#mail-addresses").livequery(function(){
  resetMailsAutoCompleteText();
});

function resetMailsAutoCompleteText(){
  var mails = getMails();
  var obj = actb($("#mail-addresses")[0],mails);  

  this.actb_bgColor = '#FFFFFF';
  this.actb_textColor = '#000000';
  this.actb_hColor = $('body').css('background-color');
  this.actb_fFamily = 'Verdana';
  this.actb_fSize = '14px';
  this.actb_hStyle = 'text-decoration:underline;font-weight="bold"';
}
function getMails(){
  var mails = Array();
  var cookieMailsString = $.cookie('saved-mails');
  mails = cookieMailsString != null ? cookieMailsString.split(',') : Array();
  return mails;
}
function saveMails(newMailsStr) {
  var mails = Array();
  var unreapeatedMailsStr = '';
  newMails = (newMailsStr + '').split(',');
  var oldMails = getMails();

  $.each(oldMails, function(index, value){
    if(mails[value] != ""){
      if(mails[value] == null)
        mails.push(value);
      mails[value] = value;
    }
  });
  $.each(newMails, function(index, value){
    if(mails[value] != ""){
      if(mails[value] == null)
        mails.push(value);
      mails[value] = value;  
    }
  });
  unreapeatedMailsStr = mails.join(',');

  //remove white spaces
  unreapeatedMailsStr = unreapeatedMailsStr.split(" ").join('');
  $.cookie('saved-mails', unreapeatedMailsStr, {
      expires: 365
  });
}

/*** sending mail handling code***/
$('#send-mail-button').live('click', function(){
	$('body').css('cursor', 'progress');

	var formData = $('#send-mail-form').serializeArray();
	var data = Array();
	$.each(formData, function(index, value){
		data[formData[index]['name']] = formData[index]['value'];
	});

	$.ajax({
         url: '/mail/sendWithSnapshotAttached' , 
         type: 'POST',
         data: {'to-email': data['to-email'], 'subject': data['subject'], 'body': data['body']},
         success: function(response){     
           $('body').css('cursor', 'auto');
           
           if($.parseJSON(response)['status'] == 'success')
              alert('mail was sent successfully');
           else 
              alert('mail sending failed');

            saveMails(Array(data['to-email']));
            resetMailsAutoCompleteText();
         }
      });   
	return false;
});