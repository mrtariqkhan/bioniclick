
function formOnLoad(form) {
    var isSnippetForm = form.hasClass('form-with-snippet');
    if (!isSnippetForm) {
        return;
    }

    //TODO:: think if it needs to call getSnippet after form loaded.
    //getSnippet(url);
}

$('#snippetgetter').live('click', function() {
    var url = getAjaxRealHref(this);
    getSnippet(url);
    return false;
});

function getSnippet(url) {
    var googleAccountPassword = $("#campaign_adwords_account_password").attr("value");
    var googleAccountEmail = $("#campaign_adwords_account_email").attr("value");

    var actionType = $("#campaign_google_action_type").attr("value");
    var actionName = $("#campaign_google_action").attr("value");

    var options = {
        'email':        googleAccountEmail,
        'password':     googleAccountPassword,
        'action_type':  actionType,
        'action_name':  actionName
    }
    $.ajax(
        {
            type:    "POST",
            url:     url,
            cache:   false,
            data:    options,
            error: globalAjaxErrorHandler,
            success: function(response, statusText, xhr) {
                globalAjaxSuccessHandler();

                var isDataCorrect = !( /[^,:{}\[\]0-9.\-+Eaeflnr-u \n\r\t]/.test(response.replace(/"(\\.|[^"\\])*"/g, '')) );
                var jsonObject = isDataCorrect && eval('(' + response + ')');

                if (jsonObject['success']) {
                    var output = jsonObject['output'];
                    $("#campaign_google_snippet").val(output);
                } else {
                    var error = 'Error: ' + jsonObject['error'];
                    myAjaxGlobalError(error);
                }
            }
        }
    );
}
