var progressTableElement    = '.log-table-interactive-progress';
var progressDateFromElement = '.interactive-date-info-from';
var progressDateToElement   = '.interactive-date-info-to';
var fieldsElement           = '.filter-call-log-fields .field_list';
var interactivePauseElement = '.pause';
var interactiveStartElement = '.start';
var tdIndexClass            = 'ind';

var tabEventCallbacks = {
    call_log: {
        loaded: function(ui) {
        },
        selected: function(ui) {
            stopSecondLogsTimer();

            hideAudioPalyers();
            hideDatePicker();
        }
    },
    call_log_interactive: {
        loaded: function(ui) {
            if (isProgressListContent($(ui.panel))) {
                startSecondLogsTimer();

                $(interactiveStartElement).hide();
                $(interactivePauseElement).show();

                $(interactivePauseElement).click(function() {
                    $(this).hide();
                    $(interactiveStartElement).show();
                    stopSecondLogsTimer();
                });
                $(interactiveStartElement).click(function() {
                    $(this).hide();
                    $(interactivePauseElement).show();
                    logTimerTimeoutHandler();
                });

            } else {
                stopSecondLogsTimer();
            }
        },
        selected: function(ui) {
        }
    },
    callers: {
        loaded: function(ui) {
        },
        selected: function(ui) {
            stopSecondLogsTimer();
        }
    }
}

$(function() {

    processDateRange('call_log_filter');

    processAudioPlayer();
});


/* ------ Other ------ */
function hideAudioPalyers() {
    $('.audio-player span').hide();
}

function hideDatePicker() {

    //TODO:: rewrite this
    //$('input.date-picker').daterangepicker.hideRP();
    //$('.ui-daterangepickercontain').hide();
    //$('input.date-picker').daterangepicker('hideRP');
    $('#call_log_filter_type').click();
}


/* ------ SecondLogs timers ------ */
var logTimer = null;
var logTimerInterval = 15 * 1000;
var cellClassStatus = 'status';

function startSecondLogsTimer() {
    if (logTimer) {
        clearTimeout(logTimer);
    }
    logTimer = setTimeout(logTimerTimeoutHandler, logTimerInterval);
}

function stopSecondLogsTimer() {
    if (logTimer) {
        clearTimeout(logTimer);
    }
}

function logTimerTimeoutHandler() {

    var params = {
        begin_timestamp: $(progressTableElement).attr('begin_timestamp'),
        campaign_id: $(progressTableElement).attr('data_campaign_id')
    };

    $.ajax({
        type: 'POST',
        url: 'callLogInProgress',
        dataType: 'json',
        cache: false,
        data: params,
        error: function () {
            startSecondLogsTimer();
        },
        success: function(response, statusText, xhr) {
            globalAjaxSuccessHandler();

            var beginTimestamp  = getArrayElement(response, 'begin_timestamp');
            var calls           = getArrayElement(response, 'calls');

            var dateTo          = getArrayElement(response, 'date_to');

            deleteDisappearedCalls(calls);
            addOrUpdateCalls(calls);
            updateDateInfo(dateTo);

            $(progressTableElement).attr('begin_timestamp', beginTimestamp);
            startSecondLogsTimer();

            //reload flexigrid table
            // $('.flexme').flexReload();
            $('#interactiveContainer.log-data').append($('#callslogInteractiveTable'));
            $('.flexigrid').remove();
            $('#callslogInteractiveTable').find('tbody').find('tr').each(function(i){
                $(this).find('td').each(function(i){
                    var contents = $(this).find('div').contents();
                    $(this).find('div').replaceWith(contents);
                });
            });
            setTimeout('adjustLogInteractiveTable()', 100);
        }
    });
}

function deleteDisappearedCalls(calls) {

    var isDisappearedCall;
    var sid;
    var trOldElementsAll = $(progressTableElement + ' tbody tr');
    $.each(trOldElementsAll, function (i, trOldElement) {

        isDisappearedCall = true;
        $.each(calls, function(i, call) {
            sid = call['sid'];
            if ($(trOldElement).hasClass(getSidClass(sid))) {
                isDisappearedCall = false;
                return false;
            }
        });

        if (isDisappearedCall) {
            $(trOldElement).remove();
        }
    });
}

function addOrUpdateCalls(calls) {

    var trElement = '';
    var column = null;
    var links = '';
    var visible = '';
    var cellClass = '';

    var progressTableElem = $(progressTableElement);
    var maxRawsAmount = progressTableElem.attr('max_raws_amount');

    var nextRawClass = getRawClassFirst();

    var tabPanel = getTabPanelByElement(progressTableElem);
    var fieldsElems = tabPanel.find(fieldsElement);
    var cellTitle = null;
    var cell = null;

    var sid = null;
    var trOldCall = null;
    var trOldCallElement = null;

    var rawIndex = 1;
    var indElement = $('');

    var elementAppendToNextNewCall = $('');
    $.each(calls, function(i, call) {

        var sid = call['sid'];
        var trOldCall = $(progressTableElement + ' tr.' + getSidClass(sid));
        if (trOldCall.length > 0) {
            //NOTE:: will update call raw
            $.each(fieldsElems, function(i, checkboxElem) {
                cellTitle = $(checkboxElem).attr('column_code');
                cell = call[cellTitle];

                trOldCallElement = $(trOldCall).find('td[column_code="' + cellTitle + '"]');
                trOldCallElement.html(cell);
            });

            indElement = $(trOldCall).find('.' + tdIndexClass);
            if (indElement.length > 0) {
                $(indElement).html(rawIndex);
            }
            
            $(trOldCall)
                .removeClass('odd')
                .removeClass('even')
                .addClass(nextRawClass)
            ;

            links = call['links'] ? call['links'] : '';
            trOldCallElement = $(trOldCall).find('td.links');
            trOldCallElement.html(links);

        } else {
            //NOTE:: create call raw
            if (rawIndex <= maxRawsAmount) {
                trElement = '';
                $.each(fieldsElems, function(i, checkboxElem) {
                    cellTitle = $(checkboxElem).attr('column_code');
                    cell = call[cellTitle];
                    column = $(progressTableElement + ' thead th[column_code="' + cellTitle + '"]');
                    
                    if (column.length) {
                        visible = column.css('display') == 'none' ? 'style="display:none;"' : 'style="display:visible;"';
                        cellClass = (cellTitle == 'Call Status') ? cellClassStatus : '';
                        trElement +=
                            '\n\
                            <td ' + visible + 'class="' + cellClass + '" column_code="' + cellTitle + '">' + cell + '</td>'
                        ;
                    }
                });
                
                links = call['links'] ? call['links'] : '';
                trElement =
                    '\n\
                        <tr class="' + nextRawClass + ' ' + getSidClass(sid) + '">\n\
                            <td class="' + tdIndexClass + '">' + rawIndex + '</td>\n\
                            <td class="links">' + links + '</td>'
                            + trElement +
                            '<td>&nbsp;</td>\n\
                        </tr>'
                ;

                if (elementAppendToNextNewCall.length > 0) {
                    $(elementAppendToNextNewCall).after(trElement);
                } else {
                    var firstRaw = $(progressTableElement + ' tbody tr:first');
                    if (firstRaw.length > 0) {
                        firstRaw.before(trElement);
                    } else {
                        $(progressTableElement + ' tbody').append(trElement);
                    }
                }
            }
        }
        rawIndex++;
        nextRawClass = getRawClassNext(nextRawClass);
        elementAppendToNextNewCall = $(progressTableElement + ' tr.' + getSidClass(sid));
    });
}

function updateDateInfo(dateTo) {
    $(progressDateToElement).html(dateTo);
}

function getRawClassFirst() {

    var firstRawElem = $(progressTableElement + ' tbody tr:first');

    var lastRawClass = null;
    if (firstRawElem.length > 0) {
        if (firstRawElem.hasClass('odd')) {
            lastRawClass = 'odd';
        } else if (firstRawElem.hasClass('even')) {
            lastRawClass = 'even';
        }
    }

    return getRawClassNext(lastRawClass);
}

function getRawClassNext(prevRawClass) {

    var nextRawClass = null;
    switch (prevRawClass) {
        case 'odd':
            nextRawClass = 'even';
            break;
        case 'even':
            nextRawClass = 'odd';
            break;
        default:
            nextRawClass = 'odd';
    }
    return nextRawClass;
}

function getSidClass(sid) {
    return 'id-' + sid;
}

function getArrayElement(arr, index) {
    return arr[index] ? arr[index] : null;
}

function isProgressListContent(tabPanel) {
    var progressTableElem = tabPanel.find(progressTableElement);
    return (progressTableElem.length > 0);
}
