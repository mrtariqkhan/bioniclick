<?php

require_once 'FairDev/Payment/AuthorizeNet/MerchantAuth.php';

/**
 * Description of CustomerProfile
 *
 * @author Kirill chEbba Chebunin <iam at chebba.org>
 */
class FairDev_Payment_AuthorizeNet_CustomerProfile {

    /**
     * @var integer
     *  customerProfileId (Payment gateway assigned ID associated with the customer profile)
     */
    private $id;

    /**
     * @var array of FairDev_Payment_AuthorizeNet_CustomerPaymentProfile
     *  Contains payment profiles for the customer profile
     */
    private $paymentProfiles = array();

    /**
     * @var string(20)
     *  Merchant assigned ID for the customer (the unique id for customer)
     */
    private $merchantCustomerId;

    /**
     * @var string(255)
     *  Description of the customer or customer profile
     */
    private $description;

    /**
     * @var string(255)
     *  Email address associated with the customer profile
     */
    private $email;

    /**
     * @var array of 
     *  Contains shipping address information for the customer profile 
     */
    private $shipToList = array();



    /**
     *
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     * @param int $id
     * @return FairDev_Payment_AuthorizeNet_CustomerProfile
     */
    public function setId($id) {

        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return array array of FairDev_Payment_AuthorizeNet_CustomerPaymentProfile
     */
    public function getPaymentProfiles() {
        return $this->paymentProfiles;
    }

    /**
     *
     * @param array $paymentProfiles array of FairDev_Payment_AuthorizeNet_CustomerPaymentProfile
     * @return FairDev_Payment_AuthorizeNet_CustomerProfile
     */
    public function setPaymentProfiles(array $paymentProfiles) {

        $this->paymentProfiles = $paymentProfiles;
        return $this;
    }

    /**
     *
     * @param FairDev_Payment_AuthorizeNet_CustomerPaymentProfile $paymentProfile
     * @return FairDev_Payment_AuthorizeNet_CustomerProfile
     */
    public function addPaymentProfile(
        FairDev_Payment_AuthorizeNet_CustomerPaymentProfile $paymentProfile
    ) {

        $this->paymentProfiles[] = $paymentProfile;
        return $this;
    }

    /**
     *
     * @return string 
     */
    public function getMerchantCustomerId() {
        return $this->merchantCustomerId;
    }

    /**
     *
     * @param string $merchantCustomerId
     * @return FairDev_Payment_AuthorizeNet_CustomerProfile 
     */
    public function setMerchantCustomerId($merchantCustomerId) {

        $this->merchantCustomerId = $merchantCustomerId;
        return $this;
    }

    /**
     *
     * @return string 
     */
    public function getDescription() {
        return $this->description;
    }

    /**
     *
     * @param string $description
     * @return FairDev_Payment_AuthorizeNet_CustomerProfile 
     */
    public function setDescription($description) {

        $this->description = $description;
        return $this;
    }

    /**
     *
     * @return string 
     */
    public function getEmail() {
        return $this->email;
    }

    /**
     *
     * @param string $email
     * @return FairDev_Payment_AuthorizeNet_CustomerProfile 
     */
    public function setEmail($email) {

        $this->email = $email;
        return $this;
    }

    /**
     *
     * @return array array of
     */
    public function getShipToList() {
        return $this->shipToList;
    }

    /**
     *
     * @param array $shipToList array of
     * @return FairDev_Payment_AuthorizeNet_CustomerProfile 
     */
    public function setShipToList(array $shipToList) {

        $this->shipToList = $shipToList;
        return $this;
    }
}
