<?php

/**
 * Description of MerchantAuth
 *
 * @author Kirill chEbba Chebunin <iam at chebba.org>
 */
class FairDev_Payment_AuthorizeNet_MerchantAuth {

    /**
     * @var string(20)
     *  Authorize.Net API Login ID (is used by the payment gateway to identify 
     *  you as an authorized merchant)
     */
    private $name           = '';

    /**
     * @var string(16)
     *  Authorize.Net API Transaction Key (is used by the payment gateway to 
     *  authenticate that transactions submitted for your account are actually 
     *  being submitted by you)
     */
    private $transactionKey = '';



    public function getName() {
        return $this->name;
    }

    public function setName($name) {

        $this->name = $name;
        return $this;
    }

    public function getTransactionKey() {
        return $this->transactionKey;
    }

    public function setTransactionKey($transactionKey) {

        $this->transactionKey = $transactionKey;
        return $this;
    }
}
