<?php

/**
 * Description of CIMRequest
 *
 * @author Kirill chEbba Chebunin <iam at chebba.org>
 */
class FairDev_Payment_AuthorizeNet_CIMRequest {

    /**
     * @var FairDev_Payment_AuthorizeNet_MerchantAuth
     */
    private $merchantAuth;

    /**
     * @var array 
     */
    private $params;



    /**
     *
     * @return FairDev_Payment_AuthorizeNet_MerchantAuth 
     */
    public function getMerchantAuth() {
        return $this->merchantAuth;
    }

    /**
     *
     * @param FairDev_Payment_AuthorizeNet_MerchantAuth $merchantAuth
     * @return FairDev_Payment_AuthorizeNet_CIMRequest 
     */
    public function setMerchantAuth($merchantAuth) {

        $this->merchantAuth = $merchantAuth;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getParams() {
        return $this->params;
    }

    /**
     *
     * @param array $params
     * @return FairDev_Payment_AuthorizeNet_CIMRequest 
     */
    public function setParams($params) {

        $this->params = $params;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function toArray() {

        $arr = $params;
        $arr['merchantAuthentication'] = $this->merchantAuth;

        return $arr;
    }
}
