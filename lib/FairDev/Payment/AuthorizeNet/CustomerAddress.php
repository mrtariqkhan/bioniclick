<?php

/**
 * Description of CustomerAddress
 *
 * @author Kirill chEbba Chebunin <iam at chebba.org>
 */
class FairDev_Payment_AuthorizeNet_CustomerAddress {

    /**
     * @var string(50)
     *  The customer's first name
     */
    private $firstName;

    /**
     * @var string(50) 
     *  The customer's last name
     */
    private $lastName;

    /**
     * @var string(50) 
     *  The name of the company associated with the customer
     */
    private $company;
    
    /**
     * @var string(60) 
     *  The customer's address
     */
    private $address;

    /**
     * @var string(20) 
     *  The ZIP code of the customer's address
     */
    private $zip;



    /**
     *
     * @return string 
     */
    public function getFirstName() {
        return $this->firstName;
    }

    /**
     *
     * @param string $firstName
     * @return FairDev_Payment_AuthorizeNet_CustomerAddress 
     */
    public function setFirstName($firstName) {

        $this->firstName = $firstName;
        return $this;
    }

    /**
     *
     * @return string 
     */
    public function getLastName() {
        return $this->lastName;
    }

    /**
     *
     * @param string $lastName
     * @return FairDev_Payment_AuthorizeNet_CustomerAddress 
     */
    public function setLastName($lastName) {

        $this->lastName = $lastName;
        return $this;
    }
    
        /**
     *
     * @return string 
     */
    public function getCompany() {
        
        return $this->company;
    }

    /**
     *
     * @param string $lastName
     * @return FairDev_Payment_AuthorizeNet_CustomerAddress 
     */
    public function setCompany($company) {

        $this->company = $company;
        return $this;
    }

    /**
     *
     * @return string 
     */
    public function getAddress() {
        return $this->address;
    }

    /**
     *
     * @param string $address
     * @return FairDev_Payment_AuthorizeNet_CustomerAddress 
     */
    public function setAddress($address) {

        $this->address = $address;
        return $this;
    }

    /**
     *
     * @return string 
     */
    public function getZip() {
        return $this->zip;
    }

    /**
     *
     * @param string $zip
     * @return FairDev_Payment_AuthorizeNet_CustomerAddress 
     */
    public function setZip($zip) {

        $this->zip = $zip;
        return $this;
    }
}
