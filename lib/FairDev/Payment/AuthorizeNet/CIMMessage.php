<?php

/**
 * Description of CIMMessage
 *
 * @author Kirill chEbba Chebunin <iam at chebba.org>
 */
class FairDev_Payment_AuthorizeNet_CIMMessage {

    /**
     * @var string 
     */
    private $code;

    /**
     * @var string 
     */
    private $text;



    /**
     *
     * @return string
     */
    public function getCode() {
        return $this->code;
    }

    /**
     *
     * @param string $code 
     */
    public function setCode($code) {
        $this->code = $code;
    }

    /**
     *
     * @return string
     */
    public function getText() {
        return $this->text;
    }


    /**
     *
     * @param string $text 
     */
    public function setText($text) {
        $this->text = $text;
    }
}
