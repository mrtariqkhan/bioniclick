<?php

require_once 'FairDev/Payment/AuthorizeNet/PaymentType.php';
require_once 'FairDev/Payment/AuthorizeNet/CreditCard.php';

/**
 * Description of Payment
 *
 * @author Kirill chEbba Chebunin <iam at chebba.org>
 */
class FairDev_Payment_AuthorizeNet_Payment {

    /**
     * @var FairDev_Payment_AuthorizeNet_CreditCard 
     */
    private $creditCard;



    /**
     * Return credit card
     * @return FairDev_Payment_AuthorizeNet_CreditCard
     */
    public function getCreditCard() {
        return $this->creditCard;
    }

    /**
     *
     * @param FairDev_Payment_AuthorizeNet_CreditCard $creditCard 
     */
    public function setCreditCard(FairDev_Payment_AuthorizeNet_CreditCard $creditCard) {
        $this->creditCard = $creditCard;
    }
}
