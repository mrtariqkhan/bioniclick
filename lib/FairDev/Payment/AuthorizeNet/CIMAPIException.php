<?php

/**
 * Description of CIMAPIException
 *
 * @author Kirill chEbba Chebunin <iam at chebba.org>
 */
class FairDev_Payment_AuthorizeNet_CIMAPIException extends Exception {

    const DUPLICATE_ENTRY = 39;

    /**
     * @var array of FairDev_Payment_AuthorizeNet_CIMMessage
     */
    private $messages = array();


    /**
     * Creates self if response has error.
     *
     * @param FairDev_Payment_AuthorizeNet_CIMResponse $response
     * @return self 
     */
    public static function createException(
        FairDev_Payment_AuthorizeNet_CIMResponse $response
    ) {

        if (!$response->isError()) {
            return null;
        }

        $messages = $response->getMessages();

        /** @var $firstMessage FairDev_Payment_AuthorizeNet_CIMMessage */
        $firstMessage = $messages[0];

        $e = new self(
            $firstMessage->getText(),
            self::parseCode($firstMessage->getCode())
        );

        $e->setMessages($messages);

        return $e;
    }

    /**
     * Throws self if response has error.
     * @param FairDev_Payment_AuthorizeNet_CIMResponse $response 
     */
    public static function throwException(
        FairDev_Payment_AuthorizeNet_CIMResponse $response
    ) {

        $e = self::createException($response);
        if (!empty($e)) {
            throw $e;
        }
    }

    /**
     *
     * @param string $code
     * @return integer
     */
    protected static function parseCode($code) {
        return (int) substr($code, 1);
    }

    /**
     *
     * @return array array of FairDev_Payment_AuthorizeNet_CIMMessage
     */
    public function getMessages() {
        return $this->messages;
    }

    /**
     *
     * @param array $messages array of FairDev_Payment_AuthorizeNet_CIMMessage
     * @return FairDev_Payment_AuthorizeNet_CIMAPIException 
     */
    public function setMessages(array $messages) {

        $this->messages = $messages;
        return $this;
    }

    public function getDuplicateEntry() {

        if ($this->code != self::DUPLICATE_ENTRY) {
            return null;
        }

        $matches = array();
        if (preg_match('/[0-9]+/', $this->getMessage(), $matches)) {
            return (int) $matches[0];
        }

        return null;
    }
}
