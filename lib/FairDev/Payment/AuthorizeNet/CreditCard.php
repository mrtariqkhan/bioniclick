<?php

require_once 'FairDev/Payment/AuthorizeNet/PaymentType.php';

/**
 * Description of CreditCard
 *
 * @author Kirill chEbba Chebunin <iam at chebba.org>
 */
class FairDev_Payment_AuthorizeNet_CreditCard
    implements FairDev_Payment_AuthorizeNet_PaymentType {

    /**
     * @var string 
     *  The customer's credit card number
     *  13 to 16 digits
     */
    private $cardNumber;

    /**
     * @var string 
     *  The expiration date for the customer's credit card
     *  YYYY-MM
     */
    private $expirationDate;

    /**
     * @var integer 
     *  The customer’s card code (the 3- or 4-digit number on the back or front of a credit card)
     *  Is used when the merchant would like to use the Card Code Verification (CCV) filter
     */
    private $cardCode;



    /**
     *
     * @return string 
     */
    public function getCardNumber() {
        return $this->cardNumber;
    }

    /**
     *
     * @param string $cardNumber
     * @return FairDev_Payment_AuthorizeNet_CreditCard
     */
    public function setCardNumber($cardNumber) {

        $this->cardNumber = $cardNumber;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getExpirationDate() {
        return $this->expirationDate;
    }

    /**
     *
     * @param string $expirationDate
     * @return FairDev_Payment_AuthorizeNet_CreditCard
     */
    public function setExpirationDate($expirationDate) {
        $this->expirationDate = $expirationDate;
        return $this;
    }

    /**
     *
     * @return integer 
     */
    public function getCardCode() {
        return $this->cardCode;
    }

    /**
     *
     * @param integer $cardCode
     * @return FairDev_Payment_AuthorizeNet_CreditCard
     */
    public function setCardCode($cardCode) {

        $this->cardCode = $cardCode;
        return $this;
    }
}
