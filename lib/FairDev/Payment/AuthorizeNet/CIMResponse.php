<?php

/**
 * Description of CIMResponse
 *
 * @author Kirill chEbba Chebunin <iam at chebba.org>
 */
class FairDev_Payment_AuthorizeNet_CIMResponse {

    const RESULT_ERROR  = 'Error';
    const RESULT_OK     = 'Ok';

    /**
     * @var string 
     */
    private $resultCode;

    /**
     * @var array of FairDev_Payment_AuthorizeNet_CIMMessage
     */
    private $messages = array();

    /**
     * @var array
     */
    private $params = array();



    /**
     *
     * @return string 
     */
    public function getResultCode() {
        return $this->resultCode;
    }

    /**
     *
     * @param string $resultCode
     * @return FairDev_Payment_AuthorizeNet_CIMResponse 
     */
    public function setResultCode($resultCode) {

        $this->resultCode = $resultCode;
        return $this;
    }

    /**
     *
     * @return array array of FairDev_Payment_AuthorizeNet_CIMMessage 
     */
    public function getMessages() {
        return $this->messages;
    }

    /**
     *
     * @param array $messages array of FairDev_Payment_AuthorizeNet_CIMMessage
     * @return FairDev_Payment_AuthorizeNet_CIMResponse 
     */
    public function setMessages(array $messages) {

        $this->messages = $messages;
        return $this;
    }

    /**
     *
     * @return array
     */
    public function getParams() {
        return $this->params;
    }

    /**
     *
     * @param array $params
     * @return FairDev_Payment_AuthorizeNet_CIMResponse 
     */
    public function setParams(array $params) {

        $this->params = $params;
        return $this;
    }

    /**
     * Set param
     * @param string $name
     * @param type $value 
     */
    public function __set($name, $value) {
        $this->params[$name] = $value;
    }

    /**
     * Get param
     * @param string $name
     * @return type 
     */
    public function __get($name) {

        //FIXME: Some code from soap try to access 'any' property, can't understand, so just hack
        if ($name == 'any') {
            return null;
        }
        if (array_key_exists($name, $this->params)) {
            return $this->params[$name];
        }

        $trace = debug_backtrace();
        trigger_error(
            "Undefined property via __get(): '{$name}'",
            E_USER_NOTICE
        );
        return null;
    }

    /**
     * If param is set
     * @param string $name
     * @return type 
     */
    public function __isset($name) {
        return array_key_exists($name, $this->params);
    }

    /**
     * Unset param
     * @param string $name 
     */
    public function __unset($name) {
        unset($this->params[$name]);
    }

    /**
     * Returns true if resultCode is Error.
     * @return boolean 
     */
    public function isError() {
        return ($this->resultCode == self::RESULT_ERROR);
    }
}
