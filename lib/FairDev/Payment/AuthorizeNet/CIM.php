<?php

require_once 'Zend/Soap/Client.php';
require_once 'FairDev/Payment/AuthorizeNet/MerchantAuth.php';
require_once 'FairDev/Payment/AuthorizeNet/Log.php';
require_once 'FairDev/Payment/AuthorizeNet/CustomerProfile.php';
require_once 'FairDev/Payment/AuthorizeNet/CustomerPaymentProfile.php';
require_once 'FairDev/Payment/AuthorizeNet/CIMResponse.php';
require_once 'FairDev/Payment/AuthorizeNet/CIMMessage.php';
require_once 'FairDev/Payment/AuthorizeNet/CIMAPIException.php';
require_once 'FairDev/Payment/AuthorizeNet/ProfileTransaction.php';

/**
 * Description of CIM
 *
 * @author Kirill chEbba Chebunin <iam at chebba.org>
 */
class FairDev_Payment_AuthorizeNet_CIM {

    const VALIDATION_MODE_TEST  = 'testMode';
    const VALIDATION_MODE_LIVE  = 'liveMode';
    const VALIDATION_MODE_NONE  = 'none';
    
    private static $WSDL_URL        = 'https://api.authorize.net/soap/v1/Service.asmx?WSDL';
    private static $WSDL_URL_TEST   = 'https://apitest.authorize.net/soap/v1/Service.asmx?WSDL';
    private static $LOG_PREFIX      = '[CIM]: ';
    private static $classMap        = array(
        'merchantAuthentication'                        => 'FairDev_Payment_AuthorizeNet_MerchantAuth',
        'CustomerProfileType'                           => 'FairDev_Payment_AuthorizeNet_CustomerProfile',
        'CustomerProfileMaskedType'                     => 'FairDev_Payment_AuthorizeNet_CustomerProfile',
        'CustomerPaymentProfileType'                    => 'FairDev_Payment_AuthorizeNet_CustomerPaymentProfile',
        'CustomerPaymentProfileMaskedType'              => 'FairDev_Payment_AuthorizeNet_CustomerPaymentProfile',
        'CustomerPaymentProfileExType'                  => 'FairDev_Payment_AuthorizeNet_CustomerPaymentProfile',
        'CustomerAddressType'                           => 'FairDev_Payment_AuthorizeNet_CustomerAddress',
        'PaymentSimpleType'                             => 'FairDev_Payment_AuthorizeNet_Payment',
        'PaymentMaskedType'                             => 'FairDev_Payment_AuthorizeNet_Payment',
        'CreditCardType'                                => 'FairDev_Payment_AuthorizeNet_CreditCard',
        'CreditCardMaskedType'                          => 'FairDev_Payment_AuthorizeNet_CreditCard',
        'MessagesTypeMessage'                           => 'FairDev_Payment_AuthorizeNet_CIMMessage',
        'ProfileTransAuthCaptureType'                   => 'FairDev_Payment_AuthorizeNet_ProfileTransaction',
        'CreateCustomerProfileResponseType'             => 'FairDev_Payment_AuthorizeNet_CIMResponse',
        'CreateCustomerPaymentProfileResponseType'      => 'FairDev_Payment_AuthorizeNet_CIMResponse',
        'GetCustomerPaymentProfileResponseType'         => 'FairDev_Payment_AuthorizeNet_CIMResponse',
        'GetCustomerProfileIdsResponseType'             => 'FairDev_Payment_AuthorizeNet_CIMResponse',
        'DeleteCustomerProfileResponseType'             => 'FairDev_Payment_AuthorizeNet_CIMResponse',
        'DeleteCustomerPaymentProfileResponseType'      => 'FairDev_Payment_AuthorizeNet_CIMResponse',
        'GetCustomerProfileResponseType'                => 'FairDev_Payment_AuthorizeNet_CIMResponse',
        'UpdateCustomerPaymentProfileResponseType'      => 'FairDev_Payment_AuthorizeNet_CIMResponse',
        'CreateCustomerProfileTransactionResponseType'  => 'FairDev_Payment_AuthorizeNet_CIMResponse'
    );

    /**
     * @var FairDev_Payment_AuthorizeNet_MerchantAuth 
     */
    private $merchantAuth;

    /**
     * @var Zend_Soap_Client 
     */
    private $soap;

    /**
     * @var boolean 
     */
    private $testMode = false;

    /**
     * @var FairDev_Payment_AuthorizeNet_Log 
     */
    private $logger;



    /**
     *
     * @param FairDev_Payment_AuthorizeNet_MerchantAuth $merchantAuth
     * @param boolean $testMode 
     */
    public function __construct(
        FairDev_Payment_AuthorizeNet_MerchantAuth $merchantAuth = null,
        $testMode = false
    ) {

        $this->merchantAuth = $merchantAuth;
        $this->testMode = (bool) $testMode;
        $this->soap = new Zend_Soap_Client(
            $this->testMode ? self::$WSDL_URL_TEST : self::$WSDL_URL,
            array(
                'classMap' => self::$classMap,
                'features' => SOAP_USE_XSI_ARRAY_TYPE + SOAP_SINGLE_ELEMENT_ARRAYS
            )
        );
    }

    /**
     *
     * @return FairDev_Payment_AuthorizeNet_MerchantAuth
     */
    public function getMerchantAuth() {
        return $this->merchantAuth;
    }

    /**
     *
     * @param FairDev_Payment_AuthorizeNet_MerchantAuth $merchantAuth 
     */
    public function setMerchantAuth(
        FairDev_Payment_AuthorizeNet_MerchantAuth $merchantAuth
    ) {
        $this->merchantAuth = $merchantAuth;
    }

    /**
     *
     * @return FairDev_Payment_AuthorizeNet_Log
     */
    public function getLogger() {
        return $this->logger;
    }

    /**
     *
     * @param FairDev_Payment_AuthorizeNet_Log $logger 
     */
    public function setLogger(FairDev_Payment_AuthorizeNet_Log $logger) {
        $this->logger = $logger;
    }

    /**
     *
     * @param string $message
     * @param integer $priority 
     */
    protected function log(
        $message,
        $priority = FairDev_Payment_AuthorizeNet_Log::INFO
    ) {

        if ($this->logger !== null) {
            $this->logger->log(self::$LOG_PREFIX . $message, $priority);
        }
    }

    /**
     *
     * @param string $methodName method name to call
     * @param array $params method params to call
     * @return FairDev_Payment_AuthorizeNet_CIMResponse
     * @throws FairDev_Payment_AuthorizeNet_CIMAPIException if an error code was returned by API
     */
    protected function callMethod($methodName, array $params = array()) {

        $responseFieldName = ucfirst($methodName) . 'Result';

        $this->log(' ========== ' . ($this->testMode ? 'testMode' : 'liveMode') . ' ========== ');
        $this->log("Call soap method '$methodName' with params: " . print_r($params, true));
        $std = new stdClass();
        $std->merchantAuthentication = $this->merchantAuth;
        foreach ($params as $key => $value) {
            $std->$key = $value;
        }

        $result = $this->soap->$methodName($std);

        //NOTE:: Retrieves request XML
        $this->log("Soap request: '{$this->soap->getLastRequest()}'");
        //NOTE:: Gets response XML
        $this->log("Soap response: '{$this->soap->getLastResponse()}'");

        /** @var $response FairDev_Payment_AuthorizeNet_CIMResponse */
        $response = $result->$responseFieldName;
        $response->setMessages($response->getMessages()->MessagesTypeMessage);

        if ($response->isError()) {
            $this->log(
                sprintf(
                    "Soap error (%s): '%s'",
                    $response->getResultCode(),
                    print_r($response->getMessages(), true)
                )
            );
        }

        FairDev_Payment_AuthorizeNet_CIMAPIException::throwException($response);

        return $response;
    }

    /**
     *
     * @param FairDev_Payment_AuthorizeNet_CustomerProfile $customerProfile
     * @param string $validationMode one of self::VALIDATION_MODE_*
     * @throws FairDev_Payment_AuthorizeNet_CIMAPIException if an error code was returned by API
     */
    public function createCustomerProfile(
        FairDev_Payment_AuthorizeNet_CustomerProfile $customerProfile,
        $validationMode = self::VALIDATION_MODE_NONE
    ) {

        $response = $this->callMethod('createCustomerProfile', array(
            'profile'           => $customerProfile,
            'validationMode'    => $validationMode
        ));        
        $customerProfile->setId($response->customerProfileId);
        if (isset($response->customerPaymentProfileIdList)) {

            /** @var $customerPaymentProfile FairDev_Payment_AuthorizeNet_CustomerPaymentProfile */
            foreach ($customerProfile->getPaymentProfiles() as $i => $customerPaymentProfile) {
                $customerPaymentProfile->setId($response->customerPaymentProfileIdList->long[$i]);
            }
        }
    }

    /**
     *
     * @param integer $customerProfileId
     * @param FairDev_Payment_AuthorizeNet_CustomerPaymentProfile $customerPaymentProfile
     * @param string $validationMode one of self::VALIDATION_MODE_*
     */
    public function createCustomerPaymentProfile(
        $customerProfileId,
        FairDev_Payment_AuthorizeNet_CustomerPaymentProfile $customerPaymentProfile,
        $validationMode = self::VALIDATION_MODE_NONE
    ) {

        if (empty($customerProfileId)) {
            throw new Exception('Customer Profile is empty.');
        }

        $response = $this->callMethod('createCustomerPaymentProfile', array(
            'customerProfileId' => $customerProfileId,
            'paymentProfile'    => $customerPaymentProfile,
            'validationMode'    => $validationMode
        ));

        $customerPaymentProfile->setId($response->customerPaymentProfileId);
    }

    /**
     *
     * @return type 
     */
    public function getCustomerProfileIds() {

        $response = $this->callMethod('getCustomerProfileIds', array());
        return $response;
    }

    /**
     *
     * @param integer $customerProfileId
     * @return FairDev_Payment_AuthorizeNet_CustomerProfile
     */
    public function getCustomerProfile($customerProfileId) {

        if (empty($customerProfileId)) {
            throw new Exception('Customer Profile is empty.');
        }

        $response = $this->callMethod('getCustomerProfile', array(
            'customerProfileId' => $customerProfileId
        ));

        $response->profile->setId($customerProfileId);
        return $response->profile;
    }

    /**
     *
     * @param integer $customerProfileId
     * @param integer $customerPaymentProfileId
     * @return FairDev_Payment_AuthorizeNet_CustomerPaymentProfile 
     */
    public function getCustomerPaymentProfile($customerProfileId, $customerPaymentProfileId) {

        if (empty($customerProfileId)) {
            throw new Exception('Customer Profile is empty.');
        }

        if (empty($customerPaymentProfileId)) {
            throw new Exception('Customer Payment Profile is empty.');
        }

        $response = $this->callMethod('getCustomerPaymentProfile', array(
            'customerProfileId'         => $customerProfileId,
            'customerPaymentProfileId'  => $customerPaymentProfileId
        ));

        $paymentProfile = $response->paymentProfile;
        $paymentProfile->setId($customerPaymentProfileId);

        return $paymentProfile;
    }

    /**
     * Deletes an existing customer profile along with all associated customer 
     *  payment profiles and customer shipping addresses.
     * 
     * @param integer $customerProfileId 
     */
    public function deleteCustomerProfile($customerProfileId) {

        if (empty($customerProfileId)) {
            return;
        }

        $response = $this->callMethod('deleteCustomerProfile', array(
            'customerProfileId' => $customerProfileId
        ));
    }

    /**
     * 
     * @param integer $customerProfileId
     * @param integer $customerPaymentProfileId 
     */
    public function deleteCustomerPaymentProfile($customerProfileId, $customerPaymentProfileId) {

        if (empty($customerProfileId)) {
            throw new Exception('Customer Profile is empty.');
        }

        if (empty($customerPaymentProfileId)) {
            throw new Exception('Customer Payment Profile is empty.');
        }

        $response = $this->callMethod('deleteCustomerPaymentProfile', array(
            'customerProfileId'         => $customerProfileId,
            'customerPaymentProfileId'  => $customerPaymentProfileId
        ));
    }

    /**
     *
     * @param integer $customerProfileId
     * @param FairDev_Payment_AuthorizeNet_CustomerPaymentProfile $customerPaymentProfile
     * @param string $validationMode one of self::VALIDATION_MODE_*
     */
    public function updateCustomerPaymentProfile(
        $customerProfileId,
        FairDev_Payment_AuthorizeNet_CustomerPaymentProfile $customerPaymentProfile,
        $validationMode = self::VALIDATION_MODE_NONE
    ) {

        if (empty($customerProfileId)) {
            throw new Exception('Customer Profile is empty.');
        }

        $customerPaymentProfile->customerPaymentProfileId = $customerPaymentProfile->getId();
        $classMap = self::$classMap;
        unset($classMap['CustomerPaymentProfileType'], $classMap['CustomerPaymentProfileMaskedType']);
        $this->soap->setOptions(array('classmap' => $classMap));
        $response = $this->callMethod('updateCustomerPaymentProfile', array(
            'customerProfileId' => $customerProfileId,
            'paymentProfile'    => $customerPaymentProfile,
            'validationMode'    => $validationMode
        ));
        $this->soap->setOptions(array('classmap' => self::$classMap));
    }

    /**
     *
     * @param FairDev_Payment_AuthorizeNet_ProfileTransaction $transaction 
     */
    public function createCustomerProfileTransaction(
        FairDev_Payment_AuthorizeNet_ProfileTransaction $transaction
    ) {

        /*
         * NOTE:: "createCustomerProfileTransaction" API function is used to 
         *  create a payment transaction from an existing customer profile. 
         *  You can submit one of six transaction types: 
         *    - Authorization Only
         *    - Authorization and Capture
         *    - Capture Only
         *    - Prior Authorization and Capture
         *    - Refund
         *    - Void
         */

        $transactionObj = new stdClass();
        /* This transaction has type "Authorization and Capture" */
        $transactionObj->profileTransAuthCapture = $transaction;
        $response = $this->callMethod('createCustomerProfileTransaction', array(
            'transaction' => $transactionObj
        ));
    }

    protected function validationModes() {
        return array(
            self::VALIDATION_MODE_TEST,
            self::VALIDATION_MODE_LIVE
        );
    }
}
