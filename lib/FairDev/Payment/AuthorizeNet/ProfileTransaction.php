<?php

/**
 *
 * @author Kirill chEbba Chebunin <iam at chebba.org>
 */
class FairDev_Payment_AuthorizeNet_ProfileTransaction {

    /**
     * @var double 
     *  The total amount of the transaction
     *  (This amount should include all other amounts such as tax amount, shipping amount, etc.)
     *  Up to 4 digits after the decimal point (no dollar symbol)
     */
    private $amount;

    /**
     * @var integer
     *  Payment gateway assigned ID associated with the customer profile
     */
    private $customerProfileId;

    /**
     * @var integer
     *  Payment gateway assigned ID associated with the customer payment profile
     */
    private $customerPaymentProfileId;

    /**
     * @var integer
     *  The customer’s card code (the 3- or 4-digit number on the back or front of a credit card)
     *  Is used when the merchant would like to use the Card Code Verification (CCV) filter
     */
    private $cardCode;



    /**
     *
     * @return double 
     */
    public function getAmount() {
        return $this->amount;
    }

    /**
     *
     * @param double $amount
     * @return FairDev_Payment_AuthorizeNet_ProfileTransaction
     */
    public function setAmount($amount) {

        $this->amount = $amount;
        return $this;
    }

    /**
     *
     * @return integer 
     */
    public function getCustomerProfileId() {
        return $this->customerProfileId;
    }

    /**
     *
     * @param integer $customerProfileId
     * @return FairDev_Payment_AuthorizeNet_ProfileTransaction
     */
    public function setCustomerProfileId($customerProfileId) {

        $this->customerProfileId = $customerProfileId;
        return $this;
    }

    /**
     *
     * @return integer 
     */
    public function getCustomerPaymentProfileId() {
        return $this->customerPaymentProfileId;
    }

    /**
     *
     * @param integer $customerPaymentProfileId
     * @return FairDev_Payment_AuthorizeNet_ProfileTransaction
     */
    public function setCustomerPaymentProfileId($customerPaymentProfileId) {

        $this->customerPaymentProfileId = $customerPaymentProfileId;
        return $this;
    }

    /**
     *
     * @return integer 
     */
    public function getCardCode() {
        return $this->cardCode;
    }

    /**
     *
     * @param integer $cardCode
     * @return FairDev_Payment_AuthorizeNet_ProfileTransaction
     */
    public function setCardCode($cardCode) {

        $this->cardCode = $cardCode;
        return $this;
    }
}
