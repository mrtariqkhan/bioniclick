<?php

require_once 'FairDev/Payment/AuthorizeNet/CustomerAddress.php';
require_once 'FairDev/Payment/AuthorizeNet/Payment.php';

/**
 * Description of CustomerPaymentProfile
 *
 * @author Kirill chEbba Chebunin <iam at chebba.org>
 */
class FairDev_Payment_AuthorizeNet_CustomerPaymentProfile {

    const TYPE_INDIVIDUAL   = 'individual';
    const TYPE_BUSINESS     = 'business';

    /**
     * @var integer 
     *  customerPaymentProfileId 
     *  Payment gateway assigned ID associated with the customer payment profile
     */
    private $id;

    /**
     * @var string 
     *  One of self::TYPE_*
     */
    private $customerType;

    /**
     * @var FairDev_Payment_AuthorizeNet_CustomerAddress
     */
    private $billTo;

    /**
     * @var FairDev_Payment_AuthorizeNet_Payment
     */
    private $payment;



    /**
     *
     * @return integer
     */
    public function getId() {
        return $this->id;
    }

    /**
     *
     * @param integer $id
     * @return FairDev_Payment_AuthorizeNet_CustomerPaymentProfile
     */
    public function setId($id) {

        $this->id = $id;
        return $this;
    }

    /**
     *
     * @return string
     */
    public function getCustomerType() {
        return $this->customerType;
    }

    /**
     *
     * @param string $customerType One of self::TYPE_*
     * @return FairDev_Payment_AuthorizeNet_CustomerPaymentProfile
     */
    public function setCustomerType($customerType) {

        $this->customerType = $customerType;
        return $this;
    }

    /**
     *
     * @return FairDev_Payment_AuthorizeNet_CustomerAddress
     */
    public function getBillTo() {
        return $this->billTo;
    }

    /**
     *
     * @param FairDev_Payment_AuthorizeNet_CustomerAddress $billTo
     * @return FairDev_Payment_AuthorizeNet_CustomerPaymentProfile
     */
    public function setBillTo(FairDev_Payment_AuthorizeNet_CustomerAddress $billTo) {

        $this->billTo = $billTo;
        return $this;
    }

    /**
     *
     * @return FairDev_Payment_AuthorizeNet_Payment
     */
    public function getPayment() {
        return $this->payment;
    }

    /**
     *
     * @param FairDev_Payment_AuthorizeNet_Payment $payment
     * @return FairDev_Payment_AuthorizeNet_CustomerPaymentProfile
     */
    public function setPayment(FairDev_Payment_AuthorizeNet_Payment $payment) {

        $this->payment = $payment;
        return $this;
    }
}
