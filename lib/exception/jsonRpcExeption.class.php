<?php

/**
 * sfError404Exception is thrown when a JSON-RPC error occurs in an action.
 * http://groups.google.com/group/json-rpc/web/json-rpc-2-0
 *
 * @package    bionic
 * @subpackage exception
 * @author     Efremochkin Yury <yury.efremochkin@fairdevteam.com>
 * @version    SVN: $Id: sfError404Exception.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class jsonRpcException extends sfException {
    
    /**
    * Forwards to the JSON-RPC 2.0 error action.
    */
    public function printStackTrace() {
        
        Loggable::putExceptionLogMessage($this, ApplicationLogger::MAIN_LOG_TYPE, 'JSON-RPC: ');
        
        sfContext::getInstance()->getRequest()->setParameter('error.code',    $this->getCode());
        sfContext::getInstance()->getRequest()->setParameter('error.message', $this->getMessage());
        
        sfContext::getInstance()->getController()->forward('errors', 'jsonRpc');
    }
}
