<?php

class GlobalValidatorTwilioIncomingPhoneNumber extends GlobalValidatorBase {

    const OPTION_URL_FIELD_NAME = 'url_name';


    /**
     * @param sfValidatorCallback $validatorObject
     * @param array $values values from form
     * @param array $options additional arguments, which Form sends to this method while its configuring
     *
     * @throw sfValidatorError
     */
    public static function checkIfUrlIsAllowed($validator, $values, $options) {

        $fieldNameUrl = self::getArrayValueRequired($options, self::OPTION_URL_FIELD_NAME);

        $url = self::getArrayValueNonRequired($values, $fieldNameUrl);
        $url = is_null($url) ? null : trim($url);
        if (empty($url)) {
            return $values;
        }

        $available = false;

        $availableProjectBeginnings = sfConfig::get('app_available_project_urls_beginnings', array());
        foreach ($availableProjectBeginnings as $availableProjectBeginning) {
            if (strpos($url, $availableProjectBeginning) !== false) {
                $available = true;
                break;
            }
        }

        if ($available) {
            return $values;
        }

        $error = new sfValidatorError($validator, "URL: ($url) has unavailable domain.");
        throw new sfValidatorErrorSchema($validator, array($fieldNameUrl => $error));
    }
}
