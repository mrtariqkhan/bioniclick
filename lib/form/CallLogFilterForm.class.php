<?php

/**
 * Description of CallLogFilterForm
 *
 * @author fairdev
 */
class CallLogFilterForm extends fdBaseFilter {

    const EMBED_FORM_DATING     = 'dating';
    const EMBED_FORM_BRANCHING  = 'branching';

    const FIELD_NAME_VA_TYPE    = 'visitor_analytics_type';
    const FIELD_LABEL_VA_TYPE   = 'Type of Call';

    const OPTION_VA_TYPE = 'va_type';

    const VISITOR_ANALYTICS_DESCRIPTION_UNDEFINED           = 'All';
    const VISITOR_ANALYTICS_DESCRIPTION_IS_SUITABLE         = 'With visitor analytics';
    const VISITOR_ANALYTICS_DESCRIPTION_IS_SUITABLE_BUT_OLD = 'With old visitor analytics';
    const VISITOR_ANALYTICS_DESCRIPTION_NO_SUITABLE         = 'Without visitor analytics';
    const VISITOR_ANALYTICS_DESCRIPTION_CALL_TO_DEFAUL      = 'Call to default phone number';


    public function setup() {

        parent::setup();

        $this->widgetSchema->setNameFormat('call_log_filter[%s]');
    }

    public function configure() {

        parent::configure();

        $this->configureEmbeddedForms();

        if ($this->getOption('do_show_calls_type', false)) {
            $this->configureVisitorAnalyticsType();

            $this->addFilterField(array(self::FIELD_NAME_VA_TYPE => array(self::FIELD_NAME_VA_TYPE)));
        }
    }

    protected function configureVisitorAnalyticsType() {

        $fieldName = self::FIELD_NAME_VA_TYPE;
        $choices = array(
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED             => self::VISITOR_ANALYTICS_DESCRIPTION_UNDEFINED,
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE           => self::VISITOR_ANALYTICS_DESCRIPTION_IS_SUITABLE,
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD   => self::VISITOR_ANALYTICS_DESCRIPTION_IS_SUITABLE_BUT_OLD,
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE           => self::VISITOR_ANALYTICS_DESCRIPTION_NO_SUITABLE,
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT       => self::VISITOR_ANALYTICS_DESCRIPTION_CALL_TO_DEFAUL,
        );
        $default = $this->getOption(self::OPTION_VA_TYPE, current($choices));

        $this->configureFieldChoice(
            $fieldName, 
            $choices, 
            $default, 
            self::FIELD_LABEL_VA_TYPE
        );
    }

    protected function configureEmbeddedForms() {

        $options = $this->getOptions();


        $defaultsDating = $this->getDefault(self::EMBED_FORM_DATING);
        $optionsDating  = $options;
        $this->embedForm(self::EMBED_FORM_DATING, new DateRangePickerFilterForm($defaultsDating, $optionsDating));

        $optionsBranching   = array_merge($options, array('prefix' => ''));
        $defaultsBranching  = $this->getDefault(self::EMBED_FORM_BRANCHING, array());
        $this->embedForm(self::EMBED_FORM_BRANCHING, new BranchFilterForm($defaultsBranching, $optionsBranching));


        $embeddedForms = $this->getEmbeddedForms();
        foreach ($embeddedForms as $name => $embeddedForm) {
            $this->getWidgetSchema()->setLabel($name, ' ');
        }
    }
}
