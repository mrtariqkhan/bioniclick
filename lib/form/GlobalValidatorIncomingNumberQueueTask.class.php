<?php

class GlobalValidatorIncomingNumberQueueTask extends GlobalValidatorBase {

    const OPTION_USE_VOIP_FIELD_NAME                    = 'use_voip_field_name';
    const OPTION_USE_BIPN_FIELD_NAME                    = 'use_bion_field_name';
    const OPTION_URL_FIELD_NAME                         = 'url_field_name';
    const OPTION_FRIENDLY_NAME_FIELD_NAME               = 'friendly_name_field_name';
    const OPTION_BIONIC_PHONE_NUMBER_POOL_FIELD_NAME    = 'bipnp_field_name';


    /**
     * @param sfValidatorCallback $validatorObject
     * @param array $values values from form
     * @param array $options additional arguments, which Form sends to this method while its configuring
     *
     * @throw sfValidatorError, Exception
     */
    public static function checkRequiredParameters($validator, $values, $options) {

        $fieldNameUseVoIP   = self::getArrayValueRequired($options, self::OPTION_USE_VOIP_FIELD_NAME);
        $fieldNameUseBipn   = self::getArrayValueRequired($options, self::OPTION_USE_BIPN_FIELD_NAME);
        $fieldNameUrl       = self::getArrayValueRequired($options, self::OPTION_URL_FIELD_NAME);

        $doUseVoipPhoneNumbers  = self::getArrayValueNonRequired($values, $fieldNameUseVoIP);
        $doUseBipnPhoneNumbers  = self::getArrayValueNonRequired($values, $fieldNameUseBipn);

        if (empty($doUseBipnPhoneNumbers) && empty($doUseVoipPhoneNumbers)) {
            throw new sfValidatorError($validator, "You should check on pools will be used for fulfilling: Bionic Incoming Pool and / or VoIP server.");
        }

        $url = self::getArrayValueNonRequired($values, $fieldNameUrl);

        if ($doUseVoipPhoneNumbers) {
            if (empty($url)) {
                throw new sfValidatorError($validator, "Url is required if you want to use VoIP server for fulfilling.");
            }
        } else {
            $values[IncomingNumberQueueTaskForm::FIELD_NAME_URL]           = '';
            $values[IncomingNumberQueueTaskForm::FIELD_NAME_FRIENDLY_NAME] = '';
        }

        if (!$doUseBipnPhoneNumbers) {
            $values[IncomingNumberQueueTaskForm::FIELD_NAME_BIONIC_PHONE_NUMBER_POOL] = '';
        }

        return $values;
    }
}

