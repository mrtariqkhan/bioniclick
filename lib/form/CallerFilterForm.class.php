<?php

/**
 * Description of CallerFilterForm
 *
 * @author fairdev
 */
class CallerFilterForm extends fdBaseFilter {

    public function setup() {

        $this->widgetSchema->setNameFormat('caller_filter[%s]');

        parent::setup();
    }

    public function configure() {

        $this->setWidget('phone_number', new sfWidgetFormInputText());
        $this->setValidator(
            'phone_number', 
            new sfValidatorString(
                array(
                    'max_length'=> 10, 
                    'required'  => false,
                    'trim'      => true,
                )
            )
        );
        $this->addFilterField(array('phone_number' => array('phone_number')));

        $this->setDefault('phone_number', $this->getOption('phone_number', ''));

        parent::configure();       
    }
    
}
