<?php

/**
 * IncomingNumberQueueTask form.
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class IncomingNumberQueueTaskForm extends TwilioIncomingPhoneNumberNewForm {

    const OPTION_ORDER  = 'order';

    const FIELD_NAME_BIONIC_PHONE_NUMBER_POOL   = 'bionic_incoming_number_pool';
    const FIELD_NAME_URL                        = 'url';
    const FIELD_NAME_FRIENDLY_NAME              = 'friendly_name';
    const FIELD_NAME_USE_VOIP                   = 'do_use_voip';
    const FIELD_NAME_USE_BIPN                   = 'do_use_bipn';

    const TIMESTAMP_FORMAT = 'Y-m-d H:i:s O';
    

    public function configure() {

        $user = $this->getOption(self::OPTION_USER);
        if (empty($user)) {
            throw new Exception("user option is required for order's fulfilling");
        }

        parent::configure();

        $order = $this->getOption(self::OPTION_ORDER, null);
        if (empty($order)) {
            throw new Exception("order option is required for order's fulfilling");
        }

        $this->configureFieldsBipnParameters();
        $this->configureFieldsVoipParameters();

        $this->useFields(
            array(
                self::FIELD_NAME_USE_BIPN,
                self::FIELD_NAME_BIONIC_PHONE_NUMBER_POOL,
                self::FIELD_NAME_USE_VOIP,
                self::FIELD_NAME_URL,
                self::FIELD_NAME_FRIENDLY_NAME,
            )
        );
    }

    protected function configureFieldsBipnParameters() {

        $this->configureCheckbox(self::FIELD_NAME_USE_BIPN, 'Is it need to use Bionic Incoming Number Pool for fulfilling this order?');

        $this->configureFieldBionicPoolList();
    }

    protected function configureFieldsVoipParameters() {

        $this->configureCheckbox(self::FIELD_NAME_USE_VOIP, 'Is it need to use VoIP for fulfilling this order?');

        $this->modifyFieldUrl();
        $this->modifyFieldFriendlyName();

        $postValidatorNew = new sfValidatorCallback(
            array(
                'callback' => array('GlobalValidatorIncomingNumberQueueTask', 'checkRequiredParameters'),
                'arguments' => array(
                    GlobalValidatorIncomingNumberQueueTask::OPTION_USE_VOIP_FIELD_NAME => IncomingNumberQueueTaskForm::FIELD_NAME_USE_VOIP,
                    GlobalValidatorIncomingNumberQueueTask::OPTION_USE_BIPN_FIELD_NAME => IncomingNumberQueueTaskForm::FIELD_NAME_USE_BIPN,
                    GlobalValidatorIncomingNumberQueueTask::OPTION_URL_FIELD_NAME => IncomingNumberQueueTaskForm::FIELD_NAME_URL,
                ),
            )
        );
        $this->addPostValidator($postValidatorNew);
    }

    protected function configureFieldBionicPoolList() {

        $fieldName = self::FIELD_NAME_BIONIC_PHONE_NUMBER_POOL;

        $model = 'TwilioIncomingPhoneNumber';

        $order = $this->getOption(self::OPTION_ORDER);
        $lacId = $order->getTwilioLocalAreaCodeId();
        $query = TwilioIncomingPhoneNumberTable::createQueryBionicFreeAvailableNumbersByLocalAreaCode($lacId);

        $user = $this->getOption(self::OPTION_USER);
        $timezoneName = $user->findTimezoneName();

        $this
            ->setWidget(
                $fieldName,
//                new BionicWidgetFormDoctrineChoiceArr(
                new BionicWidgetFormDoctrineChoice(
                    array(
                        'query'     => $query,
                        'model'     => $model,
                        'multiple'  => true,
                        'expanded'  => true,
//                        'key_field' => 'id',
//                        'fields'   => array(
//                            'phone_number' => array(
//                                'title'         => 'Phone Number',
//                            ),
//                            'field_1' => array(
//                                'title'         => 'Calls amount in last 7 days',
//                            ),
//                            'field_2' => array(
//                                'title'         => 'Calls amount in last 30 days',
//                            ),
//                            'field_3' => array(
//                                'title'         => 'Start Time of the last call',
//                            ),
//                        ),
                        'methods'   => array(
                            '__toString'                => array(
                                'title'         => 'Phone Number',
                            ),
                            'getCallsAmountInLast7Days' => array(
                                'title' => 'Calls amount in last 7 days',
                            ),
                            'getCallsAmountInLast30Days'=> array(
                                'title' => 'Calls amount in last 30 days',
                            ),
                            'getStartTimeOfTheLastCall' => array(
                                'title'     => 'Start Time of the last call',
                                'parameters'=> array(
                                    'timezoneName' => $timezoneName, 
                                    'format' => self::TIMESTAMP_FORMAT
                                ),
                            ),
                        )
                    )
                )
            )
            ->setValidator(
                $fieldName,
                new sfValidatorDoctrineChoice(
                    array(
                        'query' => $query,
                        'model' => $model,
                        'multiple'  => true,
                        'required' => false,
                    )
                )
            )
        ;
    }

    protected function modifyFieldUrl() {

        $fieldName = self::FIELD_NAME_URL;
        $this->getValidator($fieldName)->setOption('required', false);
        $this->getWidgetSchema()->setHelp($fieldName, "Will be used when 'Is it need to use VoIP' is checked on");
    }

    protected function modifyFieldFriendlyName() {

        $fieldName = self::FIELD_NAME_FRIENDLY_NAME;
        $this->getValidator($fieldName)->setOption('required', false);
        $this->getWidgetSchema()->setHelp($fieldName, "Will be used when 'Is it need to use VoIP' is checked on");
    }

    protected function configureCheckbox($fieldName, $label) {
        $this
            ->setWidget(
                $fieldName,
                new sfWidgetFormInputCheckbox()
            )
            ->setValidator(
                $fieldName,
                new sfValidatorBoolean(
                    array(
                        'required' => false,
                    )
                )
            )
        ;
        $this->getWidgetSchema()->setLabel($fieldName, $label);
    }

    public function save($con = null) {

        $doUseVoipPhoneNumbers  = $this->getValue(self::FIELD_NAME_USE_VOIP);
        $url                    = $this->getValue(self::FIELD_NAME_URL);
        $friendlyName           = $this->getValue(self::FIELD_NAME_FRIENDLY_NAME);

        $doUseBipnPhoneNumbers  = $this->getValue(self::FIELD_NAME_USE_BIPN);
        $tipnIdsDesired    = $this->getValue(self::FIELD_NAME_BIONIC_PHONE_NUMBER_POOL);
        $tipnIdsDesiredStr = '';
        if (is_array($tipnIdsDesired) && !empty($tipnIdsDesired)) {
            $tipnIdsDesiredStr = implode(',', $tipnIdsDesired);
        }

        $order = $this->getOption(self::OPTION_ORDER);
        $orderId = $order->getId();

        $task = TaskHelper::createAndRunTask(
            'BuyVoIpNumbersTask',
            array(),
            array(
                'url'               => $url,
                'friendlyName'      => $friendlyName,
                'id'                => $orderId,
                'desired_tipn_ids'  => $tipnIdsDesiredStr,
                'do_use_voip'       => $doUseVoipPhoneNumbers ? 'true' : 'false',
                'do_use_binp'       => $doUseBipnPhoneNumbers ? 'true' : 'false',
                'env'               => sfConfig::get('sf_environment'),
            )
        );

        return $task->getIsResultSuccess();
    }
}
