<?php

/**
 * Description of GraphTopClientsFilterForm
 *
 * @author fairdev
 */
class GraphTopClientsFilterForm extends fdBaseFilter {

    const BY_WEB_PAGES      = 'by_web_pages';
    const BY_YELLOW_PAGES   = 'by_yellow_pages';
    const BY_BILLBOARDS     = 'by_billboards';
    const BY_EMAIL          = 'by_email';
    const BY_OTHER          = 'by_other';
    const BY_ALL            = 'by_all';
    const BY_DEFAULT        = self::BY_ALL;

    const NAME_BY_WEB_PAGES     = 'Web Pages Campaigns';
    const NAME_BY_YELLOW_PAGES  = 'Yellow Pages Campaigns';
    const NAME_BY_BILLBOARDS    = 'Billboards Campaigns';
    const NAME_BY_EMAIL         = 'Email Campaigns';
    const NAME_BY_OTHER         = 'Other Campaigns';
    const NAME_BY_ALL           = 'All';

    const FIELD_NAME_KIND = 'kind';


    public function setup() {

        parent::setup();
    }

    public function configure() {

        parent::configure();

        $this->configureFieldPerType();

        $this->widgetSchema->setNameFormat('graph_top_clients_filter[%s]');
    }

    public function getFromTo() {
        return array(
            'from'  => $this->getOption('from', null),
            'to'    => $this->getOption('to', null),
        );
    }

    protected function configureFieldPerType() {

        $fieldName = self::FIELD_NAME_KIND;

        $choicesInfo = $this->configureFieldPerTypeChoicesInfo();
        $default = $choicesInfo['default'];
        $choices = $choicesInfo['choices'];

        $this->configureFieldChoice(
            $fieldName, 
            $choices, 
            $default, 
            false
        );

        $this->addFilterField(array($fieldName => array($fieldName)));
    }

    protected function configureFieldPerTypeChoicesInfo() {

        $default = self::BY_DEFAULT;
        $choices = array(
            self::BY_WEB_PAGES      => self::NAME_BY_WEB_PAGES,
            self::BY_YELLOW_PAGES   => self::NAME_BY_YELLOW_PAGES,
            self::BY_BILLBOARDS     => self::NAME_BY_BILLBOARDS,
            self::BY_EMAIL          => self::NAME_BY_EMAIL,
            self::BY_OTHER          => self::NAME_BY_OTHER,
            self::BY_ALL            => self::NAME_BY_ALL,
        );

        return array(
            'choices'   => $choices,
            'default'   => $default,
        );
    }

    public function getFilteredValues($values, $isCleaned = false) {

        $result = parent::getFilteredValues($values, $isCleaned);

        $result = array_merge(
            array(
                'from'  => $this->getOption('from', null),
                'to'    => $this->getOption('to', null),
            ),
            $result
        );

        return $result;
    }

    protected function buildKindValue($args) {

        $kindFilterCode = $args[self::FIELD_NAME_KIND];
        $kindCode = null;
        switch ($kindFilterCode) {
            case self::BY_ALL:
                $kindCode = CampaignTable::KIND_CODE_ALL;
                break;
            case self::BY_WEB_PAGES:
                $kindCode = CampaignTable::KIND_CODE_WEB_PAGES;
                break;
            case self::BY_YELLOW_PAGES:
                $kindCode = CampaignTable::KIND_CODE_YELLOW_PAGES;
                break;
            case self::BY_BILLBOARDS:
                $kindCode = CampaignTable::KIND_CODE_BILLBOARDS;
                break;
            case self::BY_EMAIL:
                $kindCode = CampaignTable::KIND_CODE_EMAIL;
                break;
            case self::BY_OTHER:
                $kindCode = CampaignTable::KIND_CODE_OTHER;
                break;
            default:
        }

        return $kindCode;
    }
}