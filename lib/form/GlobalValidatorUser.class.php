<?php

class GlobalValidatorUser extends GlobalValidatorBase {

    const OPTION_SF_GUARD_USER_ID       = 'sfGuardUserId';
    const OPTION_EMAIL_FIELD_NAME       = 'email_field_name';
    const OPTION_USERNAME_FIELD_NAME    = 'username_field_name';

    const OPTION_SF_GUARD_USER_ADMIN    = 'adminUser';
    const OPTION_IS_NEW                 = 'isNew';
    const OPTION_ID                     = 'id';
    const OPTION_GROUPS_LIST_FIELD_NAME = 'groups_list_field_name';


    /**
     * @param sfValidatorCallback $validatorObject
     * @param array $values values from form
     * @param array $options additional arguments, which Form sends to this 
     *   method while its configuring. Must contain self::OPTION_SF_GUARD_USER_ID, self::OPTION_EMAIL_FIELD_NAME.
     *
     * @throw sfValidatorError, Exception
     */
    public static function checkIfUserWithThisEmailExists(sfValidatorCallback $validator, $values, $options) {

        $sfGuardUserId  = self::getArrayValueRequired($options, self::OPTION_SF_GUARD_USER_ID);
        $emailFieldName = self::getArrayValueRequired($options, self::OPTION_EMAIL_FIELD_NAME);

        $email = self::getArrayValueNonRequired($values, $emailFieldName);

        $existentUser = sfGuardUserTable::findIfDuplicateByEmail($email, $sfGuardUserId);
        if (!empty($existentUser)) {
            throw new sfValidatorError($validator, $validator->getMessage('invalid') . "($email).");
        }

        return $values;
    }

    /**
     * @param sfValidatorCallback $validatorObject
     * @param array $values values from form
     * @param array $options additional arguments, which Form sends to this 
     *   method while its configuring. Must contain self::OPTION_SF_GUARD_USER_ID, self::OPTION_USERNAME_FIELD_NAME.
     *
     * @throw sfValidatorError, Exception
     */
    public static function checkIfUserWithThisUsernameExists(sfValidatorCallback $validator, $values, $options) {

        $sfGuardUserId      = self::getArrayValueRequired($options, self::OPTION_SF_GUARD_USER_ID);
        $usernameFieldName  = self::getArrayValueRequired($options, self::OPTION_USERNAME_FIELD_NAME);

        $username = self::getArrayValueNonRequired($values, $usernameFieldName);
        if (!empty($username)) {
            $existentUser = sfGuardUserTable::findIfDuplicateByUsername($username, $sfGuardUserId);
            if (!empty($existentUser)) {
                throw new sfValidatorError($validator, $validator->getMessage('invalid') . "($username).");
            }
        }

        return $values;
    }
   
    /**
     * @param sfValidatorCallback $validatorObject
     * @param array $values values from form
     * @param array $options additional arguments, which Form sends to this method while its configuring
     *
     * @throw sfValidatorError, Exception
     */
    public static function checkAdminExists($validator, $values, $options) {

        $adminUser          = self::getArrayValueRequired($options, self::OPTION_SF_GUARD_USER_ADMIN);
        $isNew              = self::getArrayValueRequired($options, self::OPTION_IS_NEW);
        $id                 = self::getArrayValueRequired($options, self::OPTION_ID);
        $groupListFieldName = self::getArrayValueRequired($options, self::OPTION_GROUPS_LIST_FIELD_NAME);

        $newGroupsList = self::getArrayValueNonRequired($values, $groupListFieldName);

        if (!$isNew) {
            if (!sfGuardUserTable::ifWillHaveAdmin($adminUser, $id, $newGroupsList)) {
                $companyKind = $adminUser->isAdvertiserUser() ? 'Advertiser' : 'Company';
                throw new sfValidatorError($validator, "Cannot update user because $companyKind must have admin.");
            }
        }

        return $values;
    }

//    /**
//     * @param sfValidatorCallback $validatorObject
//     * @param array $values values from form
//     * @param array $options additional arguments, which Form sends to this method while its configuring
//     *
//     * @throw sfValidatorError
//     */
//    public static function checkIfEmailsAreSame($validator, $values, $options) {
//
//        $fieldName1      = $options['field_name1'];
//        $fieldName2      = $options['field_name2'];
//
//        $email1 = $values[$fieldName1];
//        $email2 = $values[$fieldName2];
//
//        if (!empty($email1) && !empty($email2)) {
//            if ($email1 != $email2) {
//                throw new sfValidatorError($validator, $validator->getMessage('invalid'));
//            }
//        }
//
//        return $values;
//    }
}
