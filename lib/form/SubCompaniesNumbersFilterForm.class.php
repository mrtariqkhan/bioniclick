<?php

/**
 * Description of SubCompaniesNumbersFilterForm
 *
 * @author fairdev
 */
class SubCompaniesNumbersFilterForm extends CustomersNumbersFilterForm {

    public function setup() {

        $this->widgetSchema->setNameFormat('sub_companies_numbers_filter[%s]');

        parent::setup();
    }
}

