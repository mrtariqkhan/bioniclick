<?php

/**
 * Description of GraphCallsByParameterFilterForm
 *
 * @author fairdev
 */
class GraphCallsByParameterFilterForm extends fdBaseFilter {

    const BY_STATUS             = 'by_call_status';
    const BY_REFERRER_SOURCE    = 'by_referrer_source';
    const BY_SEARCH_TYPE        = 'by_search_type';
    const BY_KEYWORD            = 'by_keyword';
    const BY_URL                = 'by_url';
    const BY_PARAMETER_DEFAULT= self::BY_SEARCH_TYPE;

    const NAME_BY_STATUS            = 'Calls By Call Status';
    const NAME_BY_REFERRER_SOURCE   = 'Calls By Referrer Source';
    const NAME_BY_SEARCH_TYPE       = 'Calls By Search Type';
    const NAME_BY_KEYWORD           = 'Calls By Keyword';
    const NAME_BY_URL               = 'Calls By URL';

    const OPTION_BY_KEYWORD_IS_NEED = 'keyword_option_is_need';

    const FIELD_NAME_BY_PARAMETER = 'by_parameter';


    public function setup() {

        parent::setup();
    }

    public function configure() {

        parent::configure();

        $this->configureFieldPerType();

        $this->widgetSchema->setNameFormat('graph_calls_by_parameter_filter[%s]');
    }

    public function getFromTo() {
        return array(
            'from'  => $this->getOption('from', null),
            'to'    => $this->getOption('to', null),
        );
    }

    protected function configureFieldPerType() {

        $choicesInfo = $this->configureFieldPerTypeChoicesInfo();
        $default = $choicesInfo['default'];
        $choices = $choicesInfo['choices'];

        $fieldName = self::FIELD_NAME_BY_PARAMETER;

        $this->configureFieldChoice(
            $fieldName, 
            $choices, 
            $default, 
            false
        );

        $this->addFilterField(array($fieldName => array($fieldName)));
    }

    protected function configureFieldPerTypeChoicesInfo() {

        //TODO:: uncomment it when keywords will be necessary always
        $default = self::BY_PARAMETER_DEFAULT;
        $choices = array(
            self::BY_STATUS             => self::NAME_BY_STATUS,
            self::BY_REFERRER_SOURCE    => self::NAME_BY_REFERRER_SOURCE,
            self::BY_SEARCH_TYPE        => self::NAME_BY_SEARCH_TYPE,
            self::BY_URL                => self::NAME_BY_URL,
//            self::BY_KEYWORD            => self::NAME_BY_KEYWORD,
        );

        if ($this->getOption(self::OPTION_BY_KEYWORD_IS_NEED, false)) {
            $choices[self::BY_KEYWORD] = self::NAME_BY_KEYWORD;
        }

        return array(
            'choices'   => $choices,
            'default'   => $default,
        );
    }

    public function getFilteredValues($values, $isCleaned = false) {

        $result = parent::getFilteredValues($values, $isCleaned);

        $result = array_merge(
            array(
                'from'  => $this->getOption('from', null),
                'to'    => $this->getOption('to', null),
            ),
            $result
        );

        return $result;
    }

}