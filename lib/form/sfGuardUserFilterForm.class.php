<?php

/**
 * Description of sfGuardUserFilterForm
 *
 * @author fairdev
 */
class sfGuardUserFilterForm extends fdBaseFilter {

    public function setup() {

        $this->widgetSchema->setNameFormat('user_filter[%s]');

        parent::setup();
    }

    public function  configure() {
        
        $needFields = array(
            'first_name',
            'last_name',
            'email_address',
            'username',
        );
        
        foreach ($needFields as $field) {
            $this->setWidget($field, new sfWidgetFormInputText());
            $this->setValidator($field, new sfValidatorString(array('max_length' => 100, 'required' => false)));
            $this->addFilterField(array($field => array($field)));
        }

        parent::configure();

        $this->configureEmbeddedForms();
    }

    protected function configureEmbeddedForms() {
        
        $defaults   = $this->getDefaults();
        $options    = $this->getOptions();
        
        $this->embedForm('branching', new BranchFilterForm($defaults, $options));
        
        $embeddedForms = $this->getEmbeddedForms();
        foreach ($embeddedForms as $name => $embeddedForm) {
            $this->getWidgetSchema()->setLabel($name, ' ');
        }
    }
}
