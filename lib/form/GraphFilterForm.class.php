<?php

/**
 * Description of GraphFilterForm
 *
 * @author fairdev
 */
class GraphFilterForm extends fdBaseFilter {

    const EMBED_FORM_DATING = 'dating';

    public function setup() {

        parent::setup();

        $this->widgetSchema->setNameFormat('graph_filter[%s]');
    }

    public function configure() {

        parent::configure();

        $this->configureEmbeddedForms();
    }

    protected function configureEmbeddedForms() {

        $options = $this->getOptions();


        $defaultsDating = $this->getDefault(self::EMBED_FORM_DATING);
        $optionsDating  = $options;
        $this->embedForm(self::EMBED_FORM_DATING, new DateRangePickerFilterForm($defaultsDating, $optionsDating));


        $embeddedForms = $this->getEmbeddedForms();
        foreach ($embeddedForms as $name => $embeddedForm) {
            $this->getWidgetSchema()->setLabel($name, ' ');
        }
    }
}
