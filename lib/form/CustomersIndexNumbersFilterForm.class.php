<?php
//
///**
// * Description of CustomersIndexNumbersFilterForm
// *
// * @author fairdev
// */
//class CustomersIndexNumbersFilterForm extends fdBaseFilter {
//
//    public function setup() {
//
//        $this->widgetSchema->setNameFormat('customers_index_numbers_filter[%s]');
//
//        parent::setup();
//    }
//
//    public function  configure() {
//
//        parent::configure();
//
//        $this->setWidget('phone_number', new sfWidgetFormInputText());
//        $this->setValidator('phone_number', new sfValidatorString(array('max_length' => 100, 'required' => false)));
//        $this->addFilterField(array('phone_number' => array('phone_number')));
//
//        $this->configureEmbeddedForms();
//    }
//
//    protected function configureEmbeddedForms() {
//
//        $defaults   = $this->getDefaults();
//        $options    = $this->getOptions();
//
//        $embeddedForms = $this->getEmbeddedForms();
//        foreach ($embeddedForms as $name => $embeddedForm) {
//            $this->getWidgetSchema()->setLabel($name, ' ');
//        }
//    }
//}

