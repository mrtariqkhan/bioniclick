<?php

/**
 * Description of BranchFilterForm
 *
 * @author fairdev
 */
class BranchFilterForm extends fdBaseFilter {

    public function  configure() {

        parent::configure();

        $levelNames = array();

        $levelName = CompanyTable::$LEVEL_NAME_ADVERTISER;
        if ($this->getOption($levelName, false)) {
            $this->setWidget($levelName, new sfWidgetFormInputText());
            $this->setValidator($levelName, new sfValidatorString(array('max_length' => 100, 'required' => false)));
            $this->addFilterField(array($levelName => array($levelName)));
            $levelNames[] = $levelName;
        }

        $levelName = CompanyTable::$LEVEL_NAME_AGENCY;
        if ($this->getOption($levelName, false)) {
            $this->setWidget($levelName, new sfWidgetFormInputText());
            $this->setValidator($levelName, new sfValidatorString(array('max_length' => 100, 'required' => false)));
            $this->addFilterField(array($levelName => array($levelName)));
            $levelNames[] = $levelName;
        }

        $levelName = CompanyTable::$LEVEL_NAME_RESELLER;
        if ($this->getOption($levelName, false)) {
            $this->setWidget($levelName, new sfWidgetFormInputText());
            $this->setValidator($levelName, new sfValidatorString(array('max_length' => 100, 'required' => false)));
            $this->addFilterField(array($levelName => array($levelName)));
            $levelNames[] = $levelName;
        }


        $prefix = $this->getOption('prefix', '');
        foreach ($levelNames as $levelName) {
            $label = sfInflector::camelize($levelName);
            $label = empty($prefix) ? $label : $prefix . ' ' . $label;
            $this->getWidgetSchema()->setLabel($levelName, $label);
        }
    }
}
