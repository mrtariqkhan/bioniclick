<?php

/**
 * Company form.
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CompanyFormCurrentEdit extends CompanyForm {

    public function configure() {

        if ($this->isNew()) {
            throw new Exception('Object is missed.');
        }

        $parentCompany = null;
        $object = $this->getObject();
        if ($object->getLevel() != CompanyTable::$LEVEL_BIONIC) {
            $parentCompany = $object->getParentCompany();
        }
        $this->setOption(self::OPTION_PARENT_COMPANY, $parentCompany);

        parent::configure();
    }

    protected function configureFieldPackage() {

        unset(
            $this['package_id'],
            $this['skin']
        );
    }
}
