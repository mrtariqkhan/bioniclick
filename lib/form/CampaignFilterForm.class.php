<?php

/**
 * Description of CampaignFilterForm
 *
 * @author fairdev
 */
class CampaignFilterForm extends fdBaseFilter {

    public function setup() {

        $this->widgetSchema->setNameFormat('campaign_filter[%s]');

        parent::setup();
    }

    public function  configure() {

        $this->setWidget('name', new sfWidgetFormInputText());
        $this->setValidator('name', new sfValidatorString(array('max_length' => 100, 'required' => false)));
        $this->addFilterField(array('name' => array('name')));

        parent::configure();

        $this->configureEmbeddedForms();
    }

    protected function configureEmbeddedForms() {

        $defaults   = $this->getDefaults();
        $options    = $this->getOptions();

        $this->embedForm('branching', new BranchFilterForm($defaults, $options));

        $embeddedForms = $this->getEmbeddedForms();
        foreach ($embeddedForms as $name => $embeddedForm) {
            $this->getWidgetSchema()->setLabel($name, ' ');
        }
    }
}
