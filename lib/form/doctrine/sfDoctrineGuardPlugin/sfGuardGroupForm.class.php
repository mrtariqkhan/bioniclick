<?php

/**
 * sfGuardGroup form.
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class sfGuardGroupForm extends PluginsfGuardGroupForm {

    public function configure() {

        unset(
            $this['users_list']
        );

        if (!$this->isNew()) {

            unset(
                $this['name']
            );
        }
    }
}