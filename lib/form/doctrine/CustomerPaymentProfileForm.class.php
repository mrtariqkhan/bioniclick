<?php

/**
 * CustomerPaymentProfile form.
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CustomerPaymentProfileForm extends BaseCustomerPaymentProfileForm {

    public function configure() {
        unset(
            $this['priority'],
            $this['was_temporary_revoked'],
            $this['authorize_customer_payment_profile_id'],
            $this['credit'],
            $this['minimal_balance'],
            $this['payment_step'],
            $this['company_id'],
            $this['advertiser_id']
        );
        
        $this->setWidget('card_number', new sfWidgetFormInputText());
        $this->setWidget('expires', new sfWidgetFormInputText());
        $this->setWidget('first_name', new sfWidgetFormInputText());
        $this->setWidget('last_name', new sfWidgetFormInputText());
        $this->setWidget('card_code', new sfWidgetFormInputText());
        $this->setWidget('address', new sfWidgetFormInputText());
        $this->setWidget('zip', new sfWidgetFormInputText());

        $this->setValidator('card_number', new sfValidatorString(array('max_length' => 16, 'min_length' => 13)));
        $this->setValidator('expires', new sfValidatorString(array('max_length' => 10)));
        $this->setValidator('first_name', new sfValidatorString(array('max_length' => 50)));
        $this->setValidator('last_name', new sfValidatorString(array('max_length' => 50)));
        $this->setValidator('card_code', new sfValidatorString(array('max_length' => 4)));
        $this->setValidator('address', new sfValidatorString(array('max_length' => 100)));
        $this->setValidator('zip', new sfValidatorString(array('max_length' => 5, 'min_length' => 5)));

        $this->getWidget('card_number')->setLabel('Card number (13 to 16 digits)');
        $this->getWidget('expires')->setLabel('Expires at (YYYY-mm)');
        $this->getWidget('first_name')->setLabel('Cardholder first name');
        $this->getWidget('last_name')->setLabel('Cardholder last name');
        $this->getWidget('card_code')->setLabel('Cvv (maximum 4 digits)');
        $this->getWidget('address')->setLabel('Street address (maximim 100 letters)');
        $this->getWidget('zip')->setLabel('Zip code (5 digits)');
        
        if (!$this->isNew()) {
            $this->getValidator('card_number')->setOption('required', false);
            $this->getValidator('card_code')->setOption('required', false);
            $this->getValidator('expires')->setOption('required', false);
        }
    }
    
    protected function updateDefaultsFromObject() {
        
        parent::updateDefaultsFromObject();
        if (!$this->isNew()) {
            $values = array(
                'card_number' => '',
                'card_code'   => '',
                'expires'     => ''
            );
            $this->setDefaults(array_merge($this->getDefaults(), $values));
        }        
    }
    
    protected function doUpdateObject($values) {
        
        if(!$this->isNew()) {
            if (empty($values['card_number'])) {
                $values['card_number'] = $this->getObject()->getCardNumber();
            }
            if (empty($values['card_code'])) {
                $values['card_code'] = $this->getObject()->getCardCode();
            }
            if (empty($values['expires'])) {
                $values['expires'] = $this->getObject()->getExpires();
            }
        }
        parent::doUpdateObject($values);
    }
}
