<?php

/**
 * VisitorAnalytics form base class.
 *
 * @method VisitorAnalytics getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseVisitorAnalyticsForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                         => new sfWidgetFormInputHidden(),
      'analytics_ip_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('AnalyticsIp'), 'add_empty' => false)),
      'analytics_time_id'          => new sfWidgetFormInputText(),
      'referrer_source_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ReferrerSource'), 'add_empty' => true)),
      'combo_id'                   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Combo'), 'add_empty' => false)),
      'referrer_source'            => new sfWidgetFormInputText(),
      'phone_number_assignment_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PhoneNumberAssignment'), 'add_empty' => false)),
      'keyword'                    => new sfWidgetFormInputText(),
      'traffic_source'             => new sfWidgetFormTextarea(),
      'time_on_page'               => new sfWidgetFormInputText(),
      'is_organic'                 => new sfWidgetFormInputText(),
      'analytics_time'             => new sfWidgetFormInputText(),
      'created_at'                 => new sfWidgetFormDateTime(),
      'updated_at'                 => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                         => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'analytics_ip_id'            => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('AnalyticsIp'))),
      'analytics_time_id'          => new sfValidatorInteger(),
      'referrer_source_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('ReferrerSource'), 'required' => false)),
      'combo_id'                   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Combo'))),
      'referrer_source'            => new sfValidatorPass(array('required' => false)),
      'phone_number_assignment_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PhoneNumberAssignment'))),
      'keyword'                    => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'traffic_source'             => new sfValidatorString(array('max_length' => 500, 'required' => false)),
      'time_on_page'               => new sfValidatorInteger(array('required' => false)),
      'is_organic'                 => new sfValidatorInteger(array('required' => false)),
      'analytics_time'             => new sfValidatorInteger(array('required' => false)),
      'created_at'                 => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                 => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('visitor_analytics[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisitorAnalytics';
  }

}
