<?php

/**
 * Widget form base class.
 *
 * @method Widget getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedInheritanceTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseWidgetForm extends WidgetParentForm
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema   ['html_content'] = new sfWidgetFormInputText();
    $this->validatorSchema['html_content'] = new sfValidatorPass(array('required' => false));

    $this->widgetSchema->setNameFormat('widget[%s]');
  }

  public function getModelName()
  {
    return 'Widget';
  }

}
