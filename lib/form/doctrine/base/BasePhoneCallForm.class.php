<?php

/**
 * PhoneCall form base class.
 *
 * @method PhoneCall getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePhoneCallForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                       => new sfWidgetFormInputHidden(),
      'advertiser_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Advertiser'), 'add_empty' => true)),
      'campaign_id'              => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Campaign'), 'add_empty' => true)),
      'visitor_analytics_id'     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisitorAnalytics'), 'add_empty' => true)),
      'phone_number_was_default' => new sfWidgetFormInputCheckbox(),
      'created_at'               => new sfWidgetFormDateTime(),
      'updated_at'               => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                       => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'advertiser_id'            => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Advertiser'), 'required' => false)),
      'campaign_id'              => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Campaign'), 'required' => false)),
      'visitor_analytics_id'     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('VisitorAnalytics'), 'required' => false)),
      'phone_number_was_default' => new sfValidatorBoolean(array('required' => false)),
      'created_at'               => new sfValidatorDateTime(array('required' => false)),
      'updated_at'               => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('phone_call[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PhoneCall';
  }

}
