<?php

/**
 * WidgetParent form base class.
 *
 * @method WidgetParent getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseWidgetParentForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'          => new sfWidgetFormInputHidden(),
      'html_type'   => new sfWidgetFormChoice(array('choices' => array('div' => 'div', 'span' => 'span'))),
      'html_id'     => new sfWidgetFormInputText(),
      'combo_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Combo'), 'add_empty' => false)),
      'was_deleted' => new sfWidgetFormInputCheckbox(),
      'created_at'  => new sfWidgetFormDateTime(),
      'updated_at'  => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'html_type'   => new sfValidatorChoice(array('choices' => array(0 => 'div', 1 => 'span'))),
      'html_id'     => new sfValidatorString(array('max_length' => 100)),
      'combo_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Combo'))),
      'was_deleted' => new sfValidatorBoolean(array('required' => false)),
      'created_at'  => new sfValidatorDateTime(array('required' => false)),
      'updated_at'  => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('widget_parent[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'WidgetParent';
  }

}
