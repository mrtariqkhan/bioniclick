<?php

/**
 * AdvertiserIncomingNumberPoolLog form base class.
 *
 * @method AdvertiserIncomingNumberPoolLog getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseAdvertiserIncomingNumberPoolLogForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                              => new sfWidgetFormInputHidden(),
      'twilio_incoming_phone_number_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioIncomingPhoneNumber'), 'add_empty' => false)),
      'incoming_numbers_queue_id'       => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('IncomingNumbersQueue'), 'add_empty' => true)),
      'action_date'                     => new sfWidgetFormInputText(),
      'created_at'                      => new sfWidgetFormDateTime(),
      'updated_at'                      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'twilio_incoming_phone_number_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioIncomingPhoneNumber'))),
      'incoming_numbers_queue_id'       => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('IncomingNumbersQueue'), 'required' => false)),
      'action_date'                     => new sfValidatorInteger(array('required' => false)),
      'created_at'                      => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                      => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('advertiser_incoming_number_pool_log[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'AdvertiserIncomingNumberPoolLog';
  }

}
