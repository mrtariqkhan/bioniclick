<?php

/**
 * TwilioIncomingCallArchived form base class.
 *
 * @method TwilioIncomingCallArchived getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseTwilioIncomingCallArchivedForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                              => new sfWidgetFormInputHidden(),
      'sid'                             => new sfWidgetFormInputText(),
      'twilio_account_id'               => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioAccount'), 'add_empty' => false)),
      'twilio_caller_phone_number_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioCallerPhoneNumber'), 'add_empty' => false)),
      'twilio_incoming_phone_number_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioIncomingPhoneNumber'), 'add_empty' => false)),
      'caller_country'                  => new sfWidgetFormInputText(),
      'caller_zip'                      => new sfWidgetFormInputText(),
      'caller_state'                    => new sfWidgetFormInputText(),
      'twilio_city_id'                  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioCity'), 'add_empty' => true)),
      'call_status'                     => new sfWidgetFormChoice(array('choices' => array('not yet dialed' => 'not yet dialed', 'in-progress' => 'in-progress', 'completed' => 'completed', 'busy' => 'busy', 'failed' => 'failed', 'no-answer' => 'no-answer'))),
      'start_time'                      => new sfWidgetFormInputText(),
      'end_time'                        => new sfWidgetFormInputText(),
      'duration'                        => new sfWidgetFormInputText(),
      'price'                           => new sfWidgetFormInputText(),
      'created_at'                      => new sfWidgetFormDateTime(),
      'updated_at'                      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'sid'                             => new sfValidatorString(array('max_length' => 34)),
      'twilio_account_id'               => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioAccount'))),
      'twilio_caller_phone_number_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioCallerPhoneNumber'))),
      'twilio_incoming_phone_number_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioIncomingPhoneNumber'))),
      'caller_country'                  => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'caller_zip'                      => new sfValidatorString(array('max_length' => 6, 'required' => false)),
      'caller_state'                    => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'twilio_city_id'                  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioCity'), 'required' => false)),
      'call_status'                     => new sfValidatorChoice(array('choices' => array(0 => 'not yet dialed', 1 => 'in-progress', 2 => 'completed', 3 => 'busy', 4 => 'failed', 5 => 'no-answer'), 'required' => false)),
      'start_time'                      => new sfValidatorInteger(array('required' => false)),
      'end_time'                        => new sfValidatorInteger(array('required' => false)),
      'duration'                        => new sfValidatorInteger(array('required' => false)),
      'price'                           => new sfValidatorNumber(array('required' => false)),
      'created_at'                      => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                      => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'TwilioIncomingCallArchived', 'column' => array('sid')))
    );

    $this->widgetSchema->setNameFormat('twilio_incoming_call_archived[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TwilioIncomingCallArchived';
  }

}
