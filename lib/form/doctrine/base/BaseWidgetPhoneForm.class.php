<?php

/**
 * WidgetPhone form base class.
 *
 * @method WidgetPhone getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedInheritanceTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseWidgetPhoneForm extends WidgetParentForm
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema   ['html_content_1'] = new sfWidgetFormInputText();
    $this->validatorSchema['html_content_1'] = new sfValidatorPass(array('required' => false));

    $this->widgetSchema   ['html_content_2'] = new sfWidgetFormInputText();
    $this->validatorSchema['html_content_2'] = new sfValidatorPass(array('required' => false));

    $this->widgetSchema   ['physical_phone_number_id'] = new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PhysicalPhoneNumber'), 'add_empty' => false));
    $this->validatorSchema['physical_phone_number_id'] = new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PhysicalPhoneNumber')));

    $this->widgetSchema   ['number_format_id'] = new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('NumberFormat'), 'add_empty' => false));
    $this->validatorSchema['number_format_id'] = new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('NumberFormat')));

    $this->widgetSchema->setNameFormat('widget_phone[%s]');
  }

  public function getModelName()
  {
    return 'WidgetPhone';
  }

}
