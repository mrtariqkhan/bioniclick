<?php

/**
 * PhoneNumberAssignment form base class.
 *
 * @method PhoneNumberAssignment getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePhoneNumberAssignmentForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                              => new sfWidgetFormInputHidden(),
      'page_id'                         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Page'), 'add_empty' => false)),
      'twilio_city_id'                  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioCity'), 'add_empty' => false)),
      'twilio_incoming_phone_number_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioIncomingPhoneNumber'), 'add_empty' => false)),
      'physical_phone_number_id'        => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PhysicalPhoneNumber'), 'add_empty' => false)),
      'max_timestamp'                   => new sfWidgetFormDateTime(),
      'time_last_ping'                  => new sfWidgetFormDateTime(),
      'created_at'                      => new sfWidgetFormDateTime(),
      'updated_at'                      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'page_id'                         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Page'))),
      'twilio_city_id'                  => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioCity'))),
      'twilio_incoming_phone_number_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioIncomingPhoneNumber'))),
      'physical_phone_number_id'        => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('PhysicalPhoneNumber'))),
      'max_timestamp'                   => new sfValidatorDateTime(),
      'time_last_ping'                  => new sfValidatorDateTime(),
      'created_at'                      => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                      => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('phone_number_assignment[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PhoneNumberAssignment';
  }

}
