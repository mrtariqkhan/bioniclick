<?php

/**
 * CallResult form base class.
 *
 * @method CallResult getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCallResultForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                      => new sfWidgetFormInputHidden(),
      'twilio_incoming_call_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioIncomingCall'), 'add_empty' => false)),
      'rating'                  => new sfWidgetFormChoice(array('choices' => array('Sales Call' => 'Sales Call', 'Service Call' => 'Service Call', 'Billing Call' => 'Billing Call', 'Complaint Call' => 'Complaint Call', 'Other Call' => 'Other Call', 'Call Not Answered' => 'Call Not Answered'))),
      'sex'                     => new sfWidgetFormChoice(array('choices' => array('Male' => 'Male', 'Female' => 'Female'))),
      'first_name'              => new sfWidgetFormInputText(),
      'last_name'               => new sfWidgetFormInputText(),
      'address'                 => new sfWidgetFormInputText(),
      'zip'                     => new sfWidgetFormInputText(),
      'phone'                   => new sfWidgetFormInputText(),
      'revenue'                 => new sfWidgetFormInputText(),
      'invoice_number'          => new sfWidgetFormInputText(),
      'note'                    => new sfWidgetFormTextarea(),
      'created_at'              => new sfWidgetFormDateTime(),
      'updated_at'              => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                      => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'twilio_incoming_call_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioIncomingCall'))),
      'rating'                  => new sfValidatorChoice(array('choices' => array(0 => 'Sales Call', 1 => 'Service Call', 2 => 'Billing Call', 3 => 'Complaint Call', 4 => 'Other Call', 5 => 'Call Not Answered'))),
      'sex'                     => new sfValidatorChoice(array('choices' => array(0 => 'Male', 1 => 'Female'), 'required' => false)),
      'first_name'              => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'last_name'               => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'address'                 => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'zip'                     => new sfValidatorString(array('max_length' => 11, 'required' => false)),
      'phone'                   => new sfValidatorString(array('max_length' => 22, 'required' => false)),
      'revenue'                 => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'invoice_number'          => new sfValidatorString(array('max_length' => 22, 'required' => false)),
      'note'                    => new sfValidatorString(array('max_length' => 300, 'required' => false)),
      'created_at'              => new sfValidatorDateTime(array('required' => false)),
      'updated_at'              => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('call_result[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CallResult';
  }

}
