<?php

/**
 * AdvertiserIncomingNumberPool form base class.
 *
 * @method AdvertiserIncomingNumberPool getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseAdvertiserIncomingNumberPoolForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                              => new sfWidgetFormInputHidden(),
      'advertiser_id'                   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Advertiser'), 'add_empty' => false)),
      'twilio_incoming_phone_number_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioIncomingPhoneNumber'), 'add_empty' => false)),
      'campaign_id'                     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Campaign'), 'add_empty' => true)),
      'is_default'                      => new sfWidgetFormInputCheckbox(),
      'purchased_at'                    => new sfWidgetFormInputText(),
      'created_at'                      => new sfWidgetFormDateTime(),
      'updated_at'                      => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                              => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'advertiser_id'                   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Advertiser'))),
      'twilio_incoming_phone_number_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioIncomingPhoneNumber'))),
      'campaign_id'                     => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Campaign'), 'required' => false)),
      'is_default'                      => new sfValidatorBoolean(array('required' => false)),
      'purchased_at'                    => new sfValidatorInteger(array('required' => false)),
      'created_at'                      => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                      => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('advertiser_incoming_number_pool[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'AdvertiserIncomingNumberPool';
  }

}
