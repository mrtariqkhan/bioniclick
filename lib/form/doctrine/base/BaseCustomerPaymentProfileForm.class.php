<?php

/**
 * CustomerPaymentProfile form base class.
 *
 * @method CustomerPaymentProfile getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCustomerPaymentProfileForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                                    => new sfWidgetFormInputHidden(),
      'authorize_customer_payment_profile_id' => new sfWidgetFormInputText(),
      'credit_card_type'                      => new sfWidgetFormChoice(array('choices' => array('Visa' => 'Visa', 'Mastercard' => 'Mastercard', 'Amex' => 'Amex'))),
      'card_code'                             => new sfWidgetFormInputText(),
      'advertiser_id'                         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Advertiser'), 'add_empty' => true)),
      'company_id'                            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Company'), 'add_empty' => true)),
      'priority'                              => new sfWidgetFormInputText(),
      'was_temporary_revoked'                 => new sfWidgetFormInputCheckbox(),
      'created_at'                            => new sfWidgetFormDateTime(),
      'updated_at'                            => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                                    => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'authorize_customer_payment_profile_id' => new sfValidatorInteger(array('required' => false)),
      'credit_card_type'                      => new sfValidatorChoice(array('choices' => array(0 => 'Visa', 1 => 'Mastercard', 2 => 'Amex'), 'required' => false)),
      'card_code'                             => new sfValidatorString(array('max_length' => 4, 'required' => false)),
      'advertiser_id'                         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Advertiser'), 'required' => false)),
      'company_id'                            => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Company'), 'required' => false)),
      'priority'                              => new sfValidatorInteger(array('required' => false)),
      'was_temporary_revoked'                 => new sfValidatorBoolean(array('required' => false)),
      'created_at'                            => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                            => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('customer_payment_profile[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CustomerPaymentProfile';
  }

}
