<?php

/**
 * Company form base class.
 *
 * @method Company getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCompanyForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                            => new sfWidgetFormInputHidden(),
      'name'                          => new sfWidgetFormInputText(),
      'title'                         => new sfWidgetFormInputText(),
      'url'                           => new sfWidgetFormTextarea(),
      'default_phone_number'          => new sfWidgetFormInputText(),
      'authorize_customer_profile_id' => new sfWidgetFormInputText(),
      'is_debtor'                     => new sfWidgetFormInputCheckbox(),
      'skin'                          => new sfWidgetFormInputText(),
      'export'                        => new sfWidgetFormInputCheckbox(),
      'date_range_export'             => new sfWidgetFormInputCheckbox(),
      'export_date_range'             => new sfWidgetFormInputText(),
      'package_id'                    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Package'), 'add_empty' => true)),
      'was_deleted'                   => new sfWidgetFormInputCheckbox(),
      'lft'                           => new sfWidgetFormInputText(),
      'rgt'                           => new sfWidgetFormInputText(),
      'level'                         => new sfWidgetFormInputText(),
      'created_at'                    => new sfWidgetFormDateTime(),
      'updated_at'                    => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'                          => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'title'                         => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'url'                           => new sfValidatorString(array('max_length' => 256, 'required' => false)),
      'default_phone_number'          => new sfValidatorString(array('max_length' => 22, 'required' => false)),
      'authorize_customer_profile_id' => new sfValidatorInteger(array('required' => false)),
      'is_debtor'                     => new sfValidatorBoolean(array('required' => false)),
      'skin'                          => new sfValidatorString(array('max_length' => 150, 'required' => false)),
      'export'                        => new sfValidatorBoolean(array('required' => false)),
      'date_range_export'             => new sfValidatorBoolean(array('required' => false)),
      'export_date_range'             => new sfValidatorString(array('max_length' => 200, 'required' => false)),
      'package_id'                    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Package'), 'required' => false)),
      'was_deleted'                   => new sfValidatorBoolean(array('required' => false)),
      'lft'                           => new sfValidatorInteger(array('required' => false)),
      'rgt'                           => new sfValidatorInteger(array('required' => false)),
      'level'                         => new sfValidatorInteger(array('required' => false)),
      'created_at'                    => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                    => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'Company', 'column' => array('authorize_customer_profile_id')))
    );

    $this->widgetSchema->setNameFormat('company[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Company';
  }

}
