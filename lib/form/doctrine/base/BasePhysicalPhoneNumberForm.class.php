<?php

/**
 * PhysicalPhoneNumber form base class.
 *
 * @method PhysicalPhoneNumber getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePhysicalPhoneNumberForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'             => new sfWidgetFormInputHidden(),
      'phone_number'   => new sfWidgetFormInputText(),
      'campaign_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Campaign'), 'add_empty' => false)),
      'twilio_city_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioCity'), 'add_empty' => true)),
      'was_deleted'    => new sfWidgetFormInputCheckbox(),
      'created_at'     => new sfWidgetFormDateTime(),
      'updated_at'     => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'             => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'phone_number'   => new sfValidatorString(array('max_length' => 22)),
      'campaign_id'    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Campaign'))),
      'twilio_city_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioCity'), 'required' => false)),
      'was_deleted'    => new sfValidatorBoolean(array('required' => false)),
      'created_at'     => new sfValidatorDateTime(array('required' => false)),
      'updated_at'     => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('physical_phone_number[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PhysicalPhoneNumber';
  }

}
