<?php

/**
 * Campaign form base class.
 *
 * @method Campaign getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseCampaignForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                           => new sfWidgetFormInputHidden(),
      'type'                         => new sfWidgetFormChoice(array('choices' => array('Online' => 'Online', 'Offline' => 'Offline'))),
      'name'                         => new sfWidgetFormInputText(),
      'domain'                       => new sfWidgetFormInputText(),
      'api_key'                      => new sfWidgetFormInputText(),
      'conversion_url'               => new sfWidgetFormInputText(),
      'custom'                       => new sfWidgetFormInputText(),
      'is_enabled'                   => new sfWidgetFormInputCheckbox(),
      'always_convert'               => new sfWidgetFormInputCheckbox(),
      'is_recordings_enabled'        => new sfWidgetFormInputText(),
      'dynamic_number_expiry'        => new sfWidgetFormInputText(),
      'advertiser_id'                => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Advertiser'), 'add_empty' => false)),
      'kind'                         => new sfWidgetFormChoice(array('choices' => array('Web Pages' => 'Web Pages', 'Yellow Pages' => 'Yellow Pages', 'Billboards' => 'Billboards', 'Email' => 'Email', 'Other' => 'Other'))),
      'was_deleted'                  => new sfWidgetFormInputCheckbox(),
      'call_whisper'                 => new sfWidgetFormInputText(),
      'call_alert_shorter_than_secs' => new sfWidgetFormInputText(),
      'call_alert_greater_than_secs' => new sfWidgetFormInputText(),
      'call_alert_greater_than_mins' => new sfWidgetFormInputText(),
      'created_at'                   => new sfWidgetFormDateTime(),
      'updated_at'                   => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'type'                         => new sfValidatorChoice(array('choices' => array(0 => 'Online', 1 => 'Offline'), 'required' => false)),
      'name'                         => new sfValidatorString(array('max_length' => 100)),
      'domain'                       => new sfValidatorString(array('max_length' => 200)),
      'api_key'                      => new sfValidatorString(array('max_length' => 32)),
      'conversion_url'               => new sfValidatorPass(array('required' => false)),
      'custom'                       => new sfValidatorPass(array('required' => false)),
      'is_enabled'                   => new sfValidatorBoolean(array('required' => false)),
      'always_convert'               => new sfValidatorBoolean(array('required' => false)),
      'is_recordings_enabled'        => new sfValidatorInteger(array('required' => false)),
      'dynamic_number_expiry'        => new sfValidatorInteger(array('required' => false)),
      'advertiser_id'                => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Advertiser'))),
      'kind'                         => new sfValidatorChoice(array('choices' => array(0 => 'Web Pages', 1 => 'Yellow Pages', 2 => 'Billboards', 3 => 'Email', 4 => 'Other'), 'required' => false)),
      'was_deleted'                  => new sfValidatorBoolean(array('required' => false)),
      'call_whisper'                 => new sfValidatorPass(array('required' => false)),
      'call_alert_shorter_than_secs' => new sfValidatorPass(array('required' => false)),
      'call_alert_greater_than_secs' => new sfValidatorPass(array('required' => false)),
      'call_alert_greater_than_mins' => new sfValidatorPass(array('required' => false)),
      'created_at'                   => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                   => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('campaign[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Campaign';
  }

}
