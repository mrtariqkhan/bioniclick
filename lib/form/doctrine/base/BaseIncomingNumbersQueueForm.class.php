<?php

/**
 * IncomingNumbersQueue form base class.
 *
 * @method IncomingNumbersQueue getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseIncomingNumbersQueueForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                        => new sfWidgetFormInputHidden(),
      'advertiser_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Advertiser'), 'add_empty' => false)),
      'twilio_local_area_code_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioLocalAreaCode'), 'add_empty' => false)),
      'campaign_id'               => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Campaign'), 'add_empty' => true)),
      'amount'                    => new sfWidgetFormInputText(),
      'done_amount'               => new sfWidgetFormInputText(),
      'sf_guard_user_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('User'), 'add_empty' => false)),
      'created_date'              => new sfWidgetFormInputText(),
      'updated_date'              => new sfWidgetFormInputText(),
      'status'                    => new sfWidgetFormChoice(array('choices' => array('Open' => 'Open', 'In Progress' => 'In Progress', 'Done' => 'Done', 'Closed' => 'Closed'))),
      'order_type'                => new sfWidgetFormChoice(array('choices' => array('PHONE NUMBER: PURCHASE' => 'PHONE NUMBER: PURCHASE', 'PHONE NUMBER: PURCHASE AND ASSIGN' => 'PHONE NUMBER: PURCHASE AND ASSIGN', 'PHONE NUMBER: ASSIGN' => 'PHONE NUMBER: ASSIGN', 'PHONE NUMBER: RESUME FROM CAMPAIGN' => 'PHONE NUMBER: RESUME FROM CAMPAIGN', 'PHONE NUMBER: RESUME FROM ADVERTISER' => 'PHONE NUMBER: RESUME FROM ADVERTISER'))),
      'is_blocked'                => new sfWidgetFormInputCheckbox(),
      'author_notes'              => new sfWidgetFormTextarea(),
      'created_at'                => new sfWidgetFormDateTime(),
      'updated_at'                => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                        => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'advertiser_id'             => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Advertiser'))),
      'twilio_local_area_code_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioLocalAreaCode'))),
      'campaign_id'               => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Campaign'), 'required' => false)),
      'amount'                    => new sfValidatorInteger(),
      'done_amount'               => new sfValidatorInteger(),
      'sf_guard_user_id'          => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('User'))),
      'created_date'              => new sfValidatorInteger(array('required' => false)),
      'updated_date'              => new sfValidatorInteger(array('required' => false)),
      'status'                    => new sfValidatorChoice(array('choices' => array(0 => 'Open', 1 => 'In Progress', 2 => 'Done', 3 => 'Closed'), 'required' => false)),
      'order_type'                => new sfValidatorChoice(array('choices' => array(0 => 'PHONE NUMBER: PURCHASE', 1 => 'PHONE NUMBER: PURCHASE AND ASSIGN', 2 => 'PHONE NUMBER: ASSIGN', 3 => 'PHONE NUMBER: RESUME FROM CAMPAIGN', 4 => 'PHONE NUMBER: RESUME FROM ADVERTISER'), 'required' => false)),
      'is_blocked'                => new sfValidatorBoolean(array('required' => false)),
      'author_notes'              => new sfValidatorString(array('max_length' => 700, 'required' => false)),
      'created_at'                => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('incoming_numbers_queue[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'IncomingNumbersQueue';
  }

}
