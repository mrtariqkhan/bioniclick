<?php

/**
 * Advertiser form base class.
 *
 * @method Advertiser getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseAdvertiserForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                            => new sfWidgetFormInputHidden(),
      'name'                          => new sfWidgetFormInputText(),
      'url'                           => new sfWidgetFormTextarea(),
      'company_id'                    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Company'), 'add_empty' => false)),
      'rating_level_id'               => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('RatingLevel'), 'add_empty' => false)),
      'default_phone_number'          => new sfWidgetFormInputText(),
      'authorize_customer_profile_id' => new sfWidgetFormInputText(),
      'is_debtor'                     => new sfWidgetFormInputCheckbox(),
      'package_id'                    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Package'), 'add_empty' => false)),
      'was_deleted'                   => new sfWidgetFormInputCheckbox(),
      'created_at'                    => new sfWidgetFormDateTime(),
      'updated_at'                    => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'name'                          => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'url'                           => new sfValidatorString(array('max_length' => 256, 'required' => false)),
      'company_id'                    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Company'))),
      'rating_level_id'               => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('RatingLevel'))),
      'default_phone_number'          => new sfValidatorString(array('max_length' => 22, 'required' => false)),
      'authorize_customer_profile_id' => new sfValidatorInteger(array('required' => false)),
      'is_debtor'                     => new sfValidatorBoolean(array('required' => false)),
      'package_id'                    => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Package'))),
      'was_deleted'                   => new sfValidatorBoolean(array('required' => false)),
      'created_at'                    => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                    => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'Advertiser', 'column' => array('authorize_customer_profile_id')))
    );

    $this->widgetSchema->setNameFormat('advertiser[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Advertiser';
  }

}
