<?php

/**
 * PaymentTransaction form base class.
 *
 * @method PaymentTransaction getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePaymentTransactionForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                          => new sfWidgetFormInputHidden(),
      'customer_payment_profile_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CustomerPaymentProfile'), 'add_empty' => true)),
      'customer_first_name'         => new sfWidgetFormInputText(),
      'customer_last_name'          => new sfWidgetFormInputText(),
      'customer_email'              => new sfWidgetFormInputText(),
      'customer_company'            => new sfWidgetFormInputText(),
      'card_number'                 => new sfWidgetFormInputText(),
      'card_first_name'             => new sfWidgetFormInputText(),
      'card_last_name'              => new sfWidgetFormInputText(),
      'card_address'                => new sfWidgetFormInputText(),
      'card_zip'                    => new sfWidgetFormInputText(),
      'amount'                      => new sfWidgetFormInputText(),
      'created_at'                  => new sfWidgetFormDateTime(),
      'updated_at'                  => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                          => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'customer_payment_profile_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('CustomerPaymentProfile'), 'required' => false)),
      'customer_first_name'         => new sfValidatorString(array('max_length' => 50)),
      'customer_last_name'          => new sfValidatorString(array('max_length' => 50)),
      'customer_email'              => new sfValidatorString(array('max_length' => 60)),
      'customer_company'            => new sfValidatorString(array('max_length' => 50)),
      'card_number'                 => new sfValidatorString(array('max_length' => 20)),
      'card_first_name'             => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'card_last_name'              => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'card_address'                => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'card_zip'                    => new sfValidatorString(array('max_length' => 10, 'required' => false)),
      'amount'                      => new sfValidatorNumber(),
      'created_at'                  => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                  => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('payment_transaction[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PaymentTransaction';
  }

}
