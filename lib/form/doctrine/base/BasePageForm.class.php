<?php

/**
 * Page form base class.
 *
 * @method Page getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePageForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'            => new sfWidgetFormInputHidden(),
      'protocol_type' => new sfWidgetFormChoice(array('choices' => array('http' => 'http', 'https' => 'https'))),
      'page_url'      => new sfWidgetFormTextarea(),
      'api_key'       => new sfWidgetFormInputText(),
      'campaign_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Campaign'), 'add_empty' => false)),
      'was_deleted'   => new sfWidgetFormInputCheckbox(),
      'created_at'    => new sfWidgetFormDateTime(),
      'updated_at'    => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'            => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'protocol_type' => new sfValidatorChoice(array('choices' => array(0 => 'http', 1 => 'https'))),
      'page_url'      => new sfValidatorString(array('max_length' => 256, 'required' => false)),
      'api_key'       => new sfValidatorString(array('max_length' => 32)),
      'campaign_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Campaign'))),
      'was_deleted'   => new sfValidatorBoolean(array('required' => false)),
      'created_at'    => new sfValidatorDateTime(array('required' => false)),
      'updated_at'    => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('page[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Page';
  }

}
