<?php

/**
 * Package form base class.
 *
 * @method Package getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BasePackageForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                           => new sfWidgetFormInputHidden(),
      'company_id'                   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Company'), 'add_empty' => true)),
      'name'                         => new sfWidgetFormInputText(),
      'monthly_fee'                  => new sfWidgetFormInputText(),
      'toll_free_number_monthly_fee' => new sfWidgetFormInputText(),
      'local_number_monthly_fee'     => new sfWidgetFormInputText(),
      'toll_free_number_per_minute'  => new sfWidgetFormInputText(),
      'local_number_per_minute'      => new sfWidgetFormInputText(),
      'was_deleted'                  => new sfWidgetFormInputCheckbox(),
      'created_at'                   => new sfWidgetFormDateTime(),
      'updated_at'                   => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'company_id'                   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('Company'), 'required' => false)),
      'name'                         => new sfValidatorString(array('max_length' => 100, 'required' => false)),
      'monthly_fee'                  => new sfValidatorNumber(array('required' => false)),
      'toll_free_number_monthly_fee' => new sfValidatorNumber(array('required' => false)),
      'local_number_monthly_fee'     => new sfValidatorNumber(array('required' => false)),
      'toll_free_number_per_minute'  => new sfValidatorNumber(array('required' => false)),
      'local_number_per_minute'      => new sfValidatorNumber(array('required' => false)),
      'was_deleted'                  => new sfValidatorBoolean(array('required' => false)),
      'created_at'                   => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                   => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('package[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Package';
  }

}
