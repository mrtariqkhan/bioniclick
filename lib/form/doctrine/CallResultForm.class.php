<?php

/**
 * CallResult form.
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CallResultForm extends BaseCallResultForm {

    public function configure() {

        $this->setWidget('twilio_incoming_call_id', new sfWidgetFormInputHidden());
        $this->setValidator(
            'twilio_incoming_call_id',
            new sfValidatorDoctrineChoice(
                array('model' => $this->getRelatedModelName('TwilioIncomingCall'))
            )
        );
        $this->getWidget('note')->setLabel('Notes');
        $this->getValidator('note')->setOption('max_length', 200);
    }
}
