<?php

/**
 * Campaign form.
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CampaignForm extends BaseCampaignForm {

    private static $EMBED_FORM_PHYSICAL_PHONE_NUMBER = 'physical_phone_number';

    public function configure() {

        $this->setBionicFormatter();

        unset(
            $this['advertiser_id'],
            $this['api_key'],
            $this['was_deleted'],
        	$this['call_alert_greater_than_secs']
        );

        $user = $this->getOption('user', null);
        if (empty($user)) {
            throw new Exception('User is required option of CampaignForm.');
        }

        $canSeeExpiry = $this->getOption('can_see_expiry', null);
        if (is_null($canSeeExpiry)) {
            throw new Exception('can_see_expiry is required option of CampaignForm.');
        }

        $object = $this->getObject();
        $campaignId = $this->isNew() ? null : $object->getId();
        $advertiserId = $object->getAdvertiserId();
        
        $this->setWidget('is_recordings_enabled', new sfWidgetFormInputHidden());
        $this->setValidator(
            'is_recordings_enabled',
            new sfValidatorInteger(
                array(
                    'min' => 0,
                    'max' => 3,
                    'required' => false
                )
            )
        );

        $fieldName = 'domain';
        $this->getWidgetSchema()->setHelp($fieldName, 'Do not enter protocol here: ' . CampaignTable::getDefaultProtocolName() . ' protocol is used always.');
        $this->setValidator($fieldName, new DomainValidator(array(
            'required'      => true,
        )));

        $this->configureFieldDefaultPhoneNumber();
        $this->configureFieldDynamicNumberExpiry();
		
        
        /* $secondChoices = array("" => "");
        
        //populate array choices for 60 seconds
        $i=0;
        while($i<=60){
        	
        	if($i>1){
        		$secondText = "seconds";
        		
        	}else{
        		
        		$secondText = "second";
        	}
        	
        	$secondChoices[$i] = $i." ".$secondText;
        	$i++;
        }
        
        
        // minute choices
        $minChoices = array("" => "");
        
        //populate array choices for 60 seconds
        $i=0;
        while($i<=60){
        	 
        	if($i>1){
        		$minsText = "minutes";
        
        	}else{
        
        		$minsText = "minute";
        	}
        	 
        	$minChoices[$i] = $i." ".$minsText;
        	$i++;
        } */
        
        
        
        
        $this->widgetSchema['call_alert_shorter_than_secs'] =  new sfWidgetFormInputText(array());
        $this->validatorSchema['call_alert_shorter_than_secs'] = new sfValidatorNumber(array('max' => 60, 'min' => 0, 'required' => false));
        $this->getWidgetSchema()->setLabel('call_alert_shorter_than_secs', 'Email me for the calls shorter than');
        $this->getWidgetSchema()->setHelp('call_alert_shorter_than_secs', 'seconds');
        
        /* $this->widgetSchema['call_alert_greater_than_secs'] = new sfWidgetFormSelect(array(
        		"choices" => $secondChoices,
        		'default' => ''));
        
        $this->getWidgetSchema()->setLabel('call_alert_greater_than_secs', 'Email me for calls greater than following seconds'); */
        
        
        $this->widgetSchema['call_alert_greater_than_mins'] = new sfWidgetFormInputText(array());
        $this->validatorSchema['call_alert_greater_than_mins'] = new sfValidatorNumber(array('max' => 1000, 'min' => 0, 'required' => false));
        
        
        
        $this->getWidgetSchema()->setLabel('call_alert_greater_than_mins', 'Email me for the calls longer than');
        $this->getWidgetSchema()->setHelp('call_alert_greater_than_mins', 'minutes');
        
        $this->getWidgetSchema()->setLabel('is_recordings_enabled', 'Call Recording enabled');  
        $this->getWidgetSchema()->setLabel('is_enabled', 'Campaign is active');
        $this->getWidgetSchema()->setLabel('always_convert', 'Convert offline calls');
        $this->getWidget('conversion_url')->setLabel('Call conversion page');        
        $this->setDefault('conversion_url', 'convert.html');

        $postValidatorNew = new sfValidatorCallback(
            array(
                'callback' => array('GlobalValidatorCampaign', 'checkCampaignsAmountLimitPerAdvertiser'),
                'arguments' => array(
                    GlobalValidatorCampaign::OPTION_ADVERTISER_ID => $advertiserId,
                ),
            )
        );
        $this->addPostValidator($postValidatorNew);

        $this->configureEmbeddedForms();
    }

    protected function configureFieldDefaultPhoneNumber() {

        $fieldName = 'default_phone_number';

        if ($this->isNew()) {

            $object = $this->getObject();
            $advertiserId = $object->getAdvertiserId();

//            $query = TwilioIncomingPhoneNumberTable::getQueryNewCampaignAvailableNumbers(
//                $advertiserId
//            );
//
//            $this->setWidget($fieldName,
//                new sfWidgetFormDoctrineChoice(
//                    array(
//                        'query' => $query,
//                        'model' => 'TwilioIncomingPhoneNumber',
//                        'method'=> '__toStringForNewCampaign',
//                    )
//                )
//            );
//            $this->setValidator($fieldName,
//                new sfValidatorDoctrineChoice(
//                    array(
//                        'query' => $query,
//                        'model' => 'TwilioIncomingPhoneNumber',
//                    )
//                )
//            );
            $query = AdvertiserIncomingNumberPoolTable::getQueryCampaignAvailableNumbers(
                $advertiserId
            );

            $this->setWidget($fieldName,
                new sfWidgetFormDoctrineChoice(
                    array(
                        'query' => $query,
                        'model' => 'AdvertiserIncomingNumberPool',
                    )
                )
            );
            $this->setValidator($fieldName,
                new sfValidatorDoctrineChoice(
                    array(
                        'query' => $query,
                        'model' => 'AdvertiserIncomingNumberPool',
                    )
                )
            );
            $this->getWidget($fieldName)->setLabel('Default Dynamic Phone Number');

        } else {

            $object = $this->getObject();
            $campaignId = $object->getId();
            $advertiserId = $object->getAdvertiserId();

            $query = AdvertiserIncomingNumberPoolTable::getQueryCampaignAvailableNumbers(
                $advertiserId,
                $campaignId
            );       

            $this->setWidget($fieldName,
                new sfWidgetFormDoctrineChoice(
                    array(
                        'query' => $query,
                        'model' => 'AdvertiserIncomingNumberPool',
                        'method'=> '__toStringForEditingCampaign',
                    )
                )
            );
            $this->setValidator($fieldName,
                new sfValidatorDoctrineChoice(
                    array(
                        'query' => $query,
                        'model' => 'AdvertiserIncomingNumberPool'
                    )
                )
            );
            $this->getWidget($fieldName)->setLabel('Default Dynamic Phone Number');

            $defaultId = $object->getDynamicDefaultNumberId();
            if (!empty($defaultId)) {
                $this->getWidget($fieldName)->setDefault($defaultId);
            }
        }
    }

    protected function configureFieldDynamicNumberExpiry() {

        $fieldName = 'dynamic_number_expiry';

        $canSeeExpiry = $this->getOption('can_see_expiry', false);
        if (!$canSeeExpiry) {
            unset($this[$fieldName]);
        } else {
            $this->getWidgetSchema()->setHelp($fieldName, "minutes");
            $this->getWidgetSchema()->setLabel($fieldName, 'Dynamic number expiry');
        }
    }

    protected function doSave($con = null) {

        $user = $this->getOption('user', null);

        $wasNew = $this->isNew();
        if (null === $con) {
            $con = $this->getConnection();
        }

        parent::doSave($con);

        $tained = $this->getTaintedValues();
        $fieldName = 'default_phone_number';
        $defaultPhoneNumberId = array_key_exists($fieldName, $tained) ? $tained[$fieldName] : null;

        $object = $this->getObject();
        if ($wasNew) {
//            $object->assignIncomingDefaultNumber($user, $defaultPhoneNumberId, $con);
            $object->changeDefaultNumber($user, $defaultPhoneNumberId, $con);
        } else {
            $object->changeDefaultNumber($user, $defaultPhoneNumberId, $con);
        }

        $object->autoSave($con, $wasNew);
    }

    public function saveEmbeddedForms($con = null, $forms = null) {

        if ($this->isNew()) {
            $campaignId = $this->getObject()->getId();
            $embeddedForm = $this->getEmbeddedForm(self::$EMBED_FORM_PHYSICAL_PHONE_NUMBER);
            $embeddedForm->getObject()->setCampaignId($campaignId);
        }

        parent::saveEmbeddedForms($con, $forms);
    }

    protected function configureEmbeddedForms() {
        
        $options = $this->getOptions();

        if ($this->isNew()) {
            $embedForm = new PhysicalPhoneNumberForm(null, $options);
            $embedForm->getWidgetSchema()->setLabel('phone_number', 'Call Forwarding Business Number');
            $this->embedForm(self::$EMBED_FORM_PHYSICAL_PHONE_NUMBER, $embedForm);
        }

//        $embeddedForms = $this->getEmbeddedForms();
//        foreach ($embeddedForms as $name => $embeddedForm) {
//            $this->getWidgetSchema()->setLabel($name, ' ');
//        }
    }
}
