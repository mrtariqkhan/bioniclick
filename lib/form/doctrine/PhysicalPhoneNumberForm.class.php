<?php

/**
 * PhysicalPhoneNumber form.
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PhysicalPhoneNumberForm extends BasePhysicalPhoneNumberForm {

//    public static $TIMESTAMP_FORMAT = 'Y-m-d H:i:s';

    public function configure() {

        unset(
            $this['currently_assigned'],
            $this['time_assigned'],
            $this['time_assigned_end'],
            $this['twilio_city_id'],
            $this['was_deleted']
            
        );

        $this->setWidget('campaign_id', new sfWidgetFormInputHidden());
        $this->setValidator('campaign_id', new sfValidatorPass());

        $this->setValidator('phone_number', new PhoneNumberValidator());
    }
}
