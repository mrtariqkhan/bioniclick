<?php

/**
 * WidgetPhone form.
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class WidgetPhoneForm extends BaseWidgetPhoneForm {

    public function configure() {

        parent::configure();

        $this->widgetSchema['html_content_1'] = new sfWidgetFormTextarea();
        $this->widgetSchema['html_content_2'] = new sfWidgetFormTextarea();

        $campaignId = $this->getObject()->findCampaignId();
        $query = PhysicalPhoneNumberTable::createQueryAllByCampaignId($campaignId);
        $this->setWidget('physical_phone_number_id',
            new sfWidgetFormDoctrineChoice(
                array(
                    'query' => $query,
                    'model' => 'PhysicalPhoneNumber'
                )
            )
        );
        $this->setValidator('physical_phone_number_id',
            new sfValidatorDoctrineChoice(
                array(
                    'query' => $query,
                    'model' => 'PhysicalPhoneNumber'
                )
            )
        );
    }
}
