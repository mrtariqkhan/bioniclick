<?php

/**
 * AdvertiserIncomingNumberPool form.
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class AdvertiserIncomingNumberPoolForm extends BaseAdvertiserIncomingNumberPoolForm {

    public function configure() {

        parent::configure();

        unset(
            $this['advertiser_id'],
            $this['is_default'],
            $this['twilio_incoming_phone_number_id'],
            $this['purchased_at']
        );

        $advertiserId = $this->getObject()->getAdvertiserId();
        if (empty($advertiserId)) {
            $this->getWidget('campaign_id')
                ->setOption(
                    'order_by',
                    array('name', 'asc')
                )
            ;
        } else {
            $queryCampaign = CampaignTable::createQueryByAdvertiserId($advertiserId);
            $this->getWidget('campaign_id')
                ->setOption('query', $queryCampaign)
            ;
            $this->getValidator('campaign_id')
                ->setOption( 'query', $queryCampaign)
            ;
        }

        $this->getWidgetSchema()->setLabel('campaign_id', 'Campaign');
    }
}
