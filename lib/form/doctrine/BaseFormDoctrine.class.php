<?php

/**
 * Project form base class.
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormBaseTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
abstract class BaseFormDoctrine extends sfFormDoctrine {

    const BY_ALL = 0;
    const NAME_BY_ALL = 'All';


    public function __construct($defaults = array(), $options = array(), $CSRFSecret = null) {
        parent::__construct($defaults, $options, $CSRFSecret);

        $this->postConfigure();
    }

    public function postConfigure() {
        $this->setMaxlengthLimitForInput();
    }
    
    public function setMaxlengthLimitForInput() {
        
        foreach($this->getWidgetSchema()->getFields() as $fieldName => $widget) {
            if (
                $widget instanceof sfWidgetFormInput
                && (
                    $widget->getOption('type') == 'text'
                    || $widget->getOption('type') == 'password'
                )
                && $maxlength = $this->getValidator($fieldName)->getOption('max_length')
            ) {
                $widget->setAttribute('maxlength', $maxlength);
            }
        }
    }

    public function setup() {
        $this->unsetTimestampableFields();
    }

    protected function unsetTimestampableFields() {
        unset($this['created_at'], $this['updated_at']);
    }

    protected function setBionicFormatter() {
        self::setBionicFormatterToForm($this);
    }

    public static function setBionicFormatterToForm(sfForm $form) {

        $formatterName = 'bionic';
        $widgetSchema = $form->getWidgetSchema();

        $formatter = new WidgetFormSchemaFormatterBionic($widgetSchema);
        $widgetSchema
            ->setFormFormatterName($formatterName)
            ->addFormFormatter($formatterName, $formatter)
        ;
    }

    public function getTaintedValue($name, $default) {

        $tained = $this->getTaintedValues();

        $value = $default;
        if (array_key_exists($name, $tained)) {
            $value = $tained[$name];
            if (empty($value)) {
                $value = $default;
            }
        }
        return $value;
    }


    protected function configureFieldChoiceWithAll(
        $fieldName, 
        array $choices, 
        $default = null
    ) {

        $choicesWithAll = array(self::BY_ALL => self::NAME_BY_ALL);
        foreach ($choices as $id => $choice) {
            $choicesWithAll[$id] = $choice;
        }
        $keys = array_keys($choicesWithAll);

        $this
            ->setWidget(
                $fieldName,
                new sfWidgetFormChoice(
                    array(
                        'choices' => $choicesWithAll,
                    )
                )
            )
            ->setValidator(
                $fieldName,
                new sfValidatorChoice(
                    array(
                        'required' => true, 
                        'choices' => $keys,
                    )
                )
            )
        ;

        if (!is_null($default)) {
            $this->getWidget($fieldName)->setDefault($default);
        }
    }

    protected function addPostValidator($postValidatorNew) {
        $this->addPostValidators(array($postValidatorNew));
    }

    protected function addPostValidators(array $postValidatorsNew) {

        $postValidator = $this->validatorSchema->getPostValidator();
        if ($postValidator) {
            $postValidatorsNew[] = $postValidator;
        }

        $this->replacePostValidators($postValidatorsNew);
    }

    protected function replacePostValidators(array $postValidatorsNew) {
        $this->validatorSchema->setPostValidator(new sfValidatorAnd($postValidatorsNew));
    }

    protected function copyWidget($fieldName1, $fieldName2) {

        $widget = $this->getWidget($fieldName1);
        $widgetClass = get_class($widget);
        $this->setWidget(
            $fieldName2,
            new $widgetClass(
                $widget->getOptions(),
                $widget->getAttributes()
            )
        );
    }

    protected function copyValidator($fieldName1, $fieldName2) {

        $validator = $this->getValidator($fieldName1);

        $newValidator = null;
        $validatorClass = get_class($validator);
        if ($validator instanceof sfValidatorAnd) {
            $newValidator = new $validatorClass(
                $validator->getValidators(),
                $validator->getOptions(),
                $validator->getMessages()
            );
        } else {
            $newValidator = new $validatorClass(
                $validator->getOptions(),
                $validator->getMessages()
            );
        }

        $this->setValidator(
            $fieldName2, 
            $newValidator
        );
    }
}
