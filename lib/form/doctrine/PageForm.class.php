<?php

/**
 * Page form.
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PageForm extends BasePageForm {

    public function configure() {

        unset(
            $this['api_key'],
            $this['was_deleted']
        );

        $this->setWidget('page_url', new sfWidgetFormInput());

        $this->setWidget('campaign_id', new sfWidgetFormInputHidden());
        $this->setValidator('campaign_id', new sfValidatorPass());

        $this->setWidget('protocol_type', new sfWidgetFormInputHidden());
        $this->setValidator('protocol_type', new sfValidatorPass());
    }
}
