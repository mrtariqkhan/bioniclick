<?php

/**
 * WidgetParent form.
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class WidgetParentForm extends BaseWidgetParentForm {

    const OPTION_IS_NEW  = 'is_new_template';
    const OPTION_IS_EDIT = 'is_edit_template';

    const FIELD_NAME_HTML_TYPE  = 'html_type';
    const FIELD_NAME_HTML_ID    = 'html_id';
    const FIELD_NAME_COMBO_ID   = 'combo_id';
    const FIELD_NAME_ID         = 'id';


    public function configure() {

        unset(
            $this[self::FIELD_NAME_COMBO_ID],
            $this['was_deleted']
        );

        $this->widgetSchema->setLabels(array(self::FIELD_NAME_HTML_ID => 'Html ID'));



        $object = $this->getObject();
        $comboId = $object->getComboId();

        $postValidatorNew = new sfValidatorCallback(
            array(
                'callback' => array('GlobalValidatorWidgetParent', 'checkUniqueHtmlIdInCombo'),
                'arguments' => array(
                    GlobalValidatorWidgetParent::OPTION_COMBO_ID            => $comboId,
                    GlobalValidatorWidgetParent::OPTION_ID_FIELD_NAME       => self::FIELD_NAME_ID,
                    GlobalValidatorWidgetParent::OPTION_HTML_ID_FIELD_NAME  => self::FIELD_NAME_HTML_ID,
                ),
            )
        );

        $this->addPostValidator($postValidatorNew);



        $isNewTemplate  = $this->getOption(self::OPTION_IS_NEW, false);
        $isEditTemplate = $this->getOption(self::OPTION_IS_EDIT, false);

        $nameFormatOld = $this->getWidgetSchema()->getNameFormat();
        $nameFormatSimple = str_replace('[%s]', '', $nameFormatOld);
        $nameFormatNew = 'WVE_' . $nameFormatSimple;

        //TODO:: make difference between edit and create forms.
        $nameFormat = $nameFormatOld;
        if ($isNewTemplate) {
            $nameFormat = $nameFormatNew . '_new[%s]';
        } elseif ($isEditTemplate) {
            $nameFormat = $nameFormatNew . '_edit[%s]';
        }
        $this->getWidgetSchema()->setNameFormat($nameFormat);
    }

    protected function setupInheritance() {

        parent::setupInheritance();

        //NOTE:: inherit Widget*Forms will contain only this fields:
        $this->useFields(
            array(
                self::FIELD_NAME_HTML_TYPE,
                self::FIELD_NAME_HTML_ID,
                self::FIELD_NAME_COMBO_ID,
            )
        );
    }
}
