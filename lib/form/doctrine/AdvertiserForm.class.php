<?php

/**
 * Advertiser form.
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class AdvertiserForm extends BaseAdvertiserForm {

    const OPTION_PARENT_COMPANY = 'parentCompany';

    private static $EMBED_FORM_ADMIN                        = 'user_admin';
    private static $EMBED_FORM_ADMIN_PREFIX                 = "Admin's";
    private static $EMBED_FORM_ADMIN_FIELD_ADVERTISER_ID    = 'advertiser_id';

    protected $parentCompany = null;
    protected $parentCompanyAdmin = null;


    public function configure() {

        $this->setBionicFormatter();

        if ($this->isNew()) {
            $this->parentCompany = $this->getOption(self::OPTION_PARENT_COMPANY, null);
            if (empty($this->parentCompany)) {
                throw new Exception(self::OPTION_PARENT_COMPANY . ' is required for new advertiser.');
            }

            $this->parentCompanyAdmin = $this->parentCompany->getAdmin();
            if (empty($this->parentCompanyAdmin)) {
                throw new Exception("Each company should have admin.");
            }
        }

        unset(
            $this['is_debtor'],
            $this['authorize_customer_profile_id'],
            $this['credit'],
            $this['company_id'],
            $this['was_deleted']
        );

        $this->configureFieldDefaultPhoneNumber();
        $this->configureFieldPackage();

        $this->configureEmbeddedForms();
    }

    protected function configureFieldDefaultPhoneNumber() {

        $fieldName = 'default_phone_number';

        $this->getWidgetSchema()->setHelp($fieldName, 'Input using numbers only');
        $this->setValidator($fieldName, new PhoneNumberValidator(array(
            'required'      => false,
        )));
        $this->getWidgetSchema()->setLabel($fieldName, 'Contact number');
    }

    protected function configureFieldPackage() {

        //TODO:: UNDO when issue #2806 is done
//        $object = $this->getObject();
//        $parentCompanyId = $object->getCompanyId();
//
//        $queryPackage = PackageTable::createQueryByCompanyId($parentCompanyId);
//        $this->getWidget('package_id')
//            ->setOption('query', $queryPackage)
//        ;
//        $this->getValidator('package_id')
//            ->setOption('query', $queryPackage)
//        ;

        if (!$this->isNew()) {
            unset($this['package_id']);
            return;
        }

        $queryPackage = PackageTable::createQueryByCompanyId($this->parentCompany->getId());
        $this->getWidget('package_id')
            ->setOption('query', $queryPackage)
        ;
        $this->getValidator('package_id')
            ->setOption('query', $queryPackage)
        ;
    }

    public function saveEmbeddedForms($con = null, $forms = null) {

        if ($this->isNew()) {
            $advertiserId = $this->getObject()->getId();
            $this->values[self::$EMBED_FORM_ADMIN][self::$EMBED_FORM_ADMIN_FIELD_ADVERTISER_ID] = $advertiserId;
        }

        $forms = $this->getEmbeddedForms();
        foreach ($forms as $embedFormName => $form) {
            $values = $this->getValue($embedFormName);
            $form->bind($values);
            $form->save($con);
        }
    }

    protected function configureEmbeddedForms() {

//        $defaults   = $this->getDefaults();
        $options    = $this->getOptions();

        if ($this->isNew()) {
            $object = null;
            $options = array(
                'sf_guard_user_admin'   => $this->parentCompanyAdmin,
                'isCompanyCustomer'     => false,
            );
            $class = sfConfig::get("app_sfApplyPlugin_sfApplyApplyCustomerAdminFormNew_class", "ApplyCustomerAdminFormNew");

            $embedForm = new $class($object, $options);

            $labels = $embedForm->getWidgetSchema()->getLabels();
            foreach ($labels as $field => $label) {
                $labelOld = empty($label) ? sfInflector::humanize($field) : $label;
                $labelNew = self::$EMBED_FORM_ADMIN_PREFIX . " $labelOld";
                $embedForm->getWidgetSchema()->setLabel($field, $labelNew);
            }

            $fieldName = self::getCSRFFieldName();
            unset($embedForm[$fieldName]);

            $this->embedForm(self::$EMBED_FORM_ADMIN, $embedForm);
        }
    }
}