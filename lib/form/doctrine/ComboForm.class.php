<?php

/**
 * Combo form.
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ComboForm extends BaseComboForm {
    public function configure() {
        unset(
            $this['api_key'],
            $this['was_deleted']
        );

        $this->setWidget('page_id', new sfWidgetFormInputHidden());
        $this->setValidator('page_id', new sfValidatorPass());
    }
}
