<?php

/**
 * Company form.
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CompanyForm extends BaseCompanyForm {

    const OPTION_PARENT_COMPANY = 'parentCompany';

    private static $EMBED_FORM_ADMIN                    = 'user_admin';
    private static $EMBED_FORM_ADMIN_PREFIX             = "Admin's";
    private static $EMBED_FORM_ADMIN_FIELD_COMPANY_ID   = 'company_id';

    protected $parentCompany = null;
    protected $parentCompanyAdmin = null;


    public function configure() {

        $this->setBionicFormatter();

        $this->parentCompany = $this->getOption(self::OPTION_PARENT_COMPANY, null);

        if ($this->isNew()) {
            if (empty($this->parentCompany)) {
                throw new Exception(self::OPTION_PARENT_COMPANY . ' is required for new company.');
            }

            $this->parentCompanyAdmin = $this->parentCompany->getAdmin();
            if (empty($this->parentCompanyAdmin)) {
                throw new Exception("Each company should have admin.");
            }
        }

        unset(
            $this['is_debtor'],
            $this['authorize_customer_profile_id'],
            $this['credit'],
            $this['lft'],
            $this['rgt'],
            $this['level'],
            $this['was_deleted']
        );

        $this->configureFieldDefaultPhoneNumber();
        $this->configureFieldPackage();
        $this->configureFieldSkin();

        $this->configureEmbeddedForms();
    }

    protected function configureFieldSkin() {

        $fieldName = 'skin';

        if (!empty($this->parentCompany)) {
            $parentLevel = $this->parentCompany->getLevel();
            if ($parentLevel != CompanyTable::$LEVEL_BIONIC) {
                $skinDefault = $this->parentCompany->getSkin();
                $this->getWidgetSchema()->setHelp($fieldName, "By default parent's style '$skinDefault' is used");
                return;
            }
        }

        $skinDefault = CompanyTable::SKIN_DEFAULT;
        $this->getWidgetSchema()->setHelp($fieldName, "By default style '$skinDefault' is used");
    }

    protected function configureFieldDefaultPhoneNumber() {

        $fieldName = 'default_phone_number';

        $this->getWidgetSchema()->setHelp($fieldName, 'Input using numbers only');
        $this->setValidator($fieldName, new PhoneNumberValidator(array(
            'required'      => false,
        )));
        $this->getWidgetSchema()->setLabel($fieldName, 'Contact number');
    }

    protected function configureFieldPackage() {

        //TODO:: UNDO when issue #2806 is done

//        $object = $this->getObject();
//
//        if ($this->isNew()) {
//            $companyId = $this->parentCompany->getId();
//        } elseif ($object->getLevel() == CompanyTable::$LEVEL_BIONIC) {
//            $companyId = $object->getId();
//        } else {
//            $companyId = $object->getParentCompanyId();
//        }
//
//        $queryPackage = PackageTable::createQueryByCompanyId($companyId);
//        $this->getWidget('package_id')
//            ->setOption('query', $queryPackage)
//            ->setOption('add_empty', false)
//        ;
//        $this->getValidator('package_id')
//            ->setOption('query', $queryPackage)
//            ->setOption('required', true)
//        ;

        if (!$this->isNew()) {
            unset($this['package_id']);
            return;
        }

        $queryPackage = PackageTable::createQueryByCompanyId($this->parentCompany->getId());
        $this->getWidget('package_id')
            ->setOption('query', $queryPackage)
            ->setOption('add_empty', false)
        ;
        $this->getValidator('package_id')
            ->setOption('query', $queryPackage)
            ->setOption('required', true)
        ;
    }

    protected function doUpdateObject($values) {

        parent::doUpdateObject($values);

        $skin = $this->getObject()->getSkin();
        if (empty($skin)) {

            $skinDefault = CompanyTable::SKIN_DEFAULT;

            if (!empty($this->parentCompany)) {
                $parentLevel = $this->parentCompany->getLevel();
                if ($parentLevel != CompanyTable::$LEVEL_BIONIC) {
                    $skinDefault = $this->parentCompany->getSkin();
                }
            }

            $this->getObject()->setSkin($skinDefault);
        }
    }

    protected function doSave($con = null) {

        if (null === $con) {
            $con = $this->getConnection();
        }

        $this->updateObject();

        $this->getObject()->saveAsNode($con, $this->parentCompany);

        // embedded forms
        $this->saveEmbeddedForms($con);
    }

    public function saveEmbeddedForms($con = null, $forms = null) {

        if ($this->isNew()) {
            $companyId = $this->getObject()->getId();
            $this->values[self::$EMBED_FORM_ADMIN][self::$EMBED_FORM_ADMIN_FIELD_COMPANY_ID] = $companyId;
        }

        $forms = $this->getEmbeddedForms();
        foreach ($forms as $embedFormName => $form) {
            $values = $this->getValue($embedFormName);
            $form->bind($values);
            $form->save($con);
        }
    }

    protected function configureEmbeddedForms() {

//        $defaults   = $this->getDefaults();
        $options    = $this->getOptions();

        if ($this->isNew()) {
            $object = null;
            $options = array(
                'sf_guard_user_admin'   => $this->parentCompanyAdmin,
                'isCompanyCustomer'     => true,
            );
            $class = sfConfig::get("app_sfApplyPlugin_sfApplyApplyCustomerAdminFormNew_class", "ApplyCustomerAdminFormNew");

            $embedForm = new $class($object, $options);

            $labels = $embedForm->getWidgetSchema()->getLabels();
            foreach ($labels as $field => $label) {
                $labelOld = empty($label) ? sfInflector::humanize($field) : $label;
                $labelNew = self::$EMBED_FORM_ADMIN_PREFIX . " $labelOld";
                $embedForm->getWidgetSchema()->setLabel($field, $labelNew);
            }

            $fieldName = self::getCSRFFieldName();
            unset($embedForm[$fieldName]);

            $this->embedForm(self::$EMBED_FORM_ADMIN, $embedForm);
        }
    }
}
