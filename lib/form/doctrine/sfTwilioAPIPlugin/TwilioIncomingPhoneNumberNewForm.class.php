<?php

class TwilioIncomingPhoneNumberNewForm extends TwilioIncomingPhoneNumberForm {

    const FIELD_NAME_URL                    = 'url';
    const FIELD_NAME_TLAC_ID                = 'twilio_local_area_code_id';
    const FIELD_NAME_COUNT_OF_NUMBERS       = 'count_of_numbers';
    const FIELD_NAME_FRIENDLY_NAME          = 'friendly_name';
    const FIELD_NAME_HTTP_METHOD_TYPE       = 'http_method_type';
    const FIELD_NAME_PHONE_NUMBER_TYPE      = 'phone_number_type';
    const FIELD_NAME_PHONE_NUMBER_COUNTRY_ID= 'phone_number_country_id';
    const FIELD_NAME_TWILIO_ACCOUNT_ID      = 'twilio_account_id';
    const FIELD_NAME_SID                    = 'sid';
    const FIELD_NAME_PHONE_NUMBER           = 'phone_number';


    public function  configure() {

        parent::configure();

        $this->setBionicFormatter();

        unset (
            $this['was_deleted'],
            $this['purchased_date'],
            $this[self::FIELD_NAME_TWILIO_ACCOUNT_ID],
            $this[self::FIELD_NAME_SID],
            $this[self::FIELD_NAME_PHONE_NUMBER]
        );

        $this->configureFieldUrl();
        $this->configureFieldCountOfNumbers();
        $this->configureFieldTwilioLocalAreaCode();
        //$this->configureFieldTwilioAccount();
        //$this->configureFieldSid();
        //$this->configureFieldPhoneNumber();

        $this->setFieldsOrder();
    }

    protected function configureFieldUrl() {

        $fieldName = self::FIELD_NAME_URL;

        $this->getValidator($fieldName)
            ->setOption('min_length', 1)
            ->setOption('trim', true)
        ;

        $postValidatorNew = new sfValidatorCallback(
            array(
                'callback'  => array('GlobalValidatorTwilioIncomingPhoneNumber', 'checkIfUrlIsAllowed'),
                'arguments' => array(
                    GlobalValidatorTwilioIncomingPhoneNumber::OPTION_URL_FIELD_NAME => $fieldName,
                ),
            )
        );
        $this->addPostValidator($postValidatorNew);
    }

    protected function configureFieldTwilioLocalAreaCode() {

        $this->configureFieldPhoneNumberType();
        $this->configureFieldPhoneNumberCountryId();

        $fieldName = self::FIELD_NAME_TLAC_ID;

        $url = 'twilioIncomingPhoneNumber/ajaxFillAreaCodeAutocompleteFullNew';

        $formTagName = $this->getName();
        $phoneNumberFieldName           = self::FIELD_NAME_PHONE_NUMBER_TYPE;
        $phoneNumberCountryIdFieldName  = self::FIELD_NAME_PHONE_NUMBER_COUNTRY_ID;

        $autocompleterWidget = new BionicWidgetFormDoctrineJQueryAutocompleter(array(
            'model'                         => 'TwilioLocalAreaCode',
            'url'                           => $url,
            'name_form'                     => $formTagName,
            'name_phone_number'             => $phoneNumberFieldName,
            'name_phone_number_country_id'  => $phoneNumberCountryIdFieldName,
        ));

        $this->setWidget($fieldName, $autocompleterWidget);

        $this->getWidgetSchema()
            ->setLabel($fieldName, 'Area code')
            ->setHelp($fieldName, "Double-click, begin typing in field")
        ;
    }

    protected function configureFieldPhoneNumberType() {

        $fieldName = self::FIELD_NAME_PHONE_NUMBER_TYPE;
        $choices = TwilioLocalAreaCodeTable::findPhoneNumberTypesCoded();

        $this->configureFieldChoiceWithAll($fieldName, $choices);
    }

    protected function configureFieldPhoneNumberCountryId() {

        $fieldName = self::FIELD_NAME_PHONE_NUMBER_COUNTRY_ID;
        $choices = TwilioCountryTable::findAllNames();

        $this->configureFieldChoiceWithAll($fieldName, $choices);
    }

    protected function configureFieldCountOfNumbers() {

        $fieldName = self::FIELD_NAME_COUNT_OF_NUMBERS;

        $this->setWidget(
            $fieldName,
            new sfWidgetFormInputText(array('default' => 1))
        );

        $this->setValidator(
            $fieldName,
            new sfValidatorInteger(array('min' => 1))
        );
    }

//    protected function configureFieldTwilioAccount() {
//
//        $fieldName = self::FIELD_NAME_TWILIO_ACCOUNT_ID;
//
//        $this->setWidget($fieldName, new sfWidgetFormInputHidden());
//
//        $this->setValidator(
//            $fieldName,
//            new sfValidatorDoctrineChoice(
//                array('model' => $this->getRelatedModelName('TwilioAccount'))
//            )
//        );
//    }
//
//    protected function configureFieldSid() {
//
//        $fieldName = self::FIELD_NAME_SID;
//
//        $this->setWidget($fieldName, new sfWidgetFormInputHidden());
//
//        $this->setValidator(
//            $fieldName,
//            new sfValidatorString(array('max_length' => 34))
//        );
//    }
//
//    protected function configureFieldPhoneNumber() {
//
//        $fieldName = self::FIELD_NAME_PHONE_NUMBER;
//
//        $this->setWidget($fieldName, new sfWidgetFormInputHidden());
//
//        $this->setValidator(
//            $fieldName,
//            new sfValidatorString(array('max_length' => 22))
//        );
//    }

    protected function setFieldsOrder() {

        $this->widgetSchema->moveField(
            self::FIELD_NAME_PHONE_NUMBER_COUNTRY_ID,
            sfWidgetFormSchema::AFTER,
            self::FIELD_NAME_PHONE_NUMBER_TYPE
        );
        $this->widgetSchema->moveField(
            self::FIELD_NAME_TLAC_ID,
            sfWidgetFormSchema::AFTER,
            self::FIELD_NAME_PHONE_NUMBER_COUNTRY_ID
        );
        $this->widgetSchema->moveField(
            self::FIELD_NAME_COUNT_OF_NUMBERS,
            sfWidgetFormSchema::AFTER,
            self::FIELD_NAME_TLAC_ID
        );
    }

    protected function doSave($con = null) {
        
        $localAreaCode = TwilioLocalAreaCodeTable::getInstance()->findById(
            $this->getValue(self::FIELD_NAME_TLAC_ID)
        );
        if (empty($localAreaCode)) {
            throw new Exception('Area code has not been found.');
        }

        $numbersCount = $this->getValue(self::FIELD_NAME_COUNT_OF_NUMBERS);        
        $numbersAdded = 0;
        try {
            for ($i = 0; $i < $numbersCount; $i++) {
                $bionicIncomingNumberPool = TwilioIncomingPhoneNumberTable::buyAndRegisterIncomingNumber(
                    $localAreaCode, 
                    $this->getValue(self::FIELD_NAME_URL),
                    $this->getValue(self::FIELD_NAME_FRIENDLY_NAME),
                    $this->getValue(self::FIELD_NAME_HTTP_METHOD_TYPE)
                );
                if (!empty($bionicIncomingNumberPool)) {
                    $numbersAdded++;
                }
            }

            if ($numbersAdded == 0) {
                throw new Exception("Noone number has been registered!");
            } elseif ($numbersAdded < $numbersCount) {
                throw new Exception("Only $numbersAdded numbers have been successfully registered");
            }

        } catch (TwilioPhoneNumberAPIException $e) {
            Loggable::putExceptionLogMessage($e, ApplicationLogger::TWILIO_LOG_TYPE);            
            throw new TwilioPhoneNumberAPIException("$numbersAdded numbers has been registered. {$e->getMessage()}");
        }
    }

}

