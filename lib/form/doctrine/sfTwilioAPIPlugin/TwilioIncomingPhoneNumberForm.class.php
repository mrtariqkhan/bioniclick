<?php

/**
 * TwilioIncomingPhoneNumber form.
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class TwilioIncomingPhoneNumberForm extends PluginTwilioIncomingPhoneNumberForm {

    const OPTION_USER = 'user';


    public function configure() {

        $user = $this->getOption(self::OPTION_USER, null);
        if (empty($user)) {
            throw new Exception('Option ' . self::OPTION_USER . ' is required.');
        }
        if ($user->getLevel() != CompanyTable::$LEVEL_BIONIC) {
            throw new Exception('Form is available for bionic users only.');
        }
    }
}
