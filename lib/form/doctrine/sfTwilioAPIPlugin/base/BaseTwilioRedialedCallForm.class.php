<?php

/**
 * TwilioRedialedCall form base class.
 *
 * @method TwilioRedialedCall getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseTwilioRedialedCallForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                      => new sfWidgetFormInputHidden(),
      'segment_sid'             => new sfWidgetFormInputText(),
      'twilio_incoming_call_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ParentCall'), 'add_empty' => false)),
      'called_number'           => new sfWidgetFormInputText(),
      'call_status'             => new sfWidgetFormChoice(array('choices' => array('not yet dialed' => 'not yet dialed', 'in-progress' => 'in-progress', 'completed' => 'completed', 'busy' => 'busy', 'failed' => 'failed', 'no-answer' => 'no-answer'))),
      'start_time'              => new sfWidgetFormInputText(),
      'end_time'                => new sfWidgetFormInputText(),
      'duration'                => new sfWidgetFormInputText(),
      'price'                   => new sfWidgetFormInputText(),
      'created_at'              => new sfWidgetFormDateTime(),
      'updated_at'              => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                      => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'segment_sid'             => new sfValidatorString(array('max_length' => 34)),
      'twilio_incoming_call_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('ParentCall'))),
      'called_number'           => new sfValidatorString(array('max_length' => 22)),
      'call_status'             => new sfValidatorChoice(array('choices' => array(0 => 'not yet dialed', 1 => 'in-progress', 2 => 'completed', 3 => 'busy', 4 => 'failed', 5 => 'no-answer'), 'required' => false)),
      'start_time'              => new sfValidatorInteger(array('required' => false)),
      'end_time'                => new sfValidatorInteger(array('required' => false)),
      'duration'                => new sfValidatorInteger(array('required' => false)),
      'price'                   => new sfValidatorNumber(array('required' => false)),
      'created_at'              => new sfValidatorDateTime(array('required' => false)),
      'updated_at'              => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'TwilioRedialedCall', 'column' => array('segment_sid')))
    );

    $this->widgetSchema->setNameFormat('twilio_redialed_call[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TwilioRedialedCall';
  }

}
