<?php

/**
 * TwilioCallRecording form base class.
 *
 * @method TwilioCallRecording getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseTwilioCallRecordingForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                      => new sfWidgetFormInputHidden(),
      'sid'                     => new sfWidgetFormInputText(),
      'twilio_incoming_call_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioIncomingCall'), 'add_empty' => false)),
      'file_url'                => new sfWidgetFormInputText(),
      'date_created'            => new sfWidgetFormInputText(),
      'date_updated'            => new sfWidgetFormInputText(),
      'duration'                => new sfWidgetFormInputText(),
      'dollar_amount'           => new sfWidgetFormInputText(),
      'name'                    => new sfWidgetFormInputText(),
      'description'             => new sfWidgetFormInputText(),
      'gender'                  => new sfWidgetFormInputText(),
      'created_at'              => new sfWidgetFormDateTime(),
      'updated_at'              => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                      => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'sid'                     => new sfValidatorString(array('max_length' => 34)),
      'twilio_incoming_call_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioIncomingCall'))),
      'file_url'                => new sfValidatorString(array('max_length' => 255)),
      'date_created'            => new sfValidatorInteger(array('required' => false)),
      'date_updated'            => new sfValidatorInteger(array('required' => false)),
      'duration'                => new sfValidatorInteger(array('required' => false)),
      'dollar_amount'           => new sfValidatorNumber(array('required' => false)),
      'name'                    => new sfValidatorString(array('max_length' => 50, 'required' => false)),
      'description'             => new sfValidatorPass(array('required' => false)),
      'gender'                  => new sfValidatorString(array('max_length' => 6, 'required' => false)),
      'created_at'              => new sfValidatorDateTime(array('required' => false)),
      'updated_at'              => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'TwilioCallRecording', 'column' => array('sid')))
    );

    $this->widgetSchema->setNameFormat('twilio_call_recording[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TwilioCallRecording';
  }

}
