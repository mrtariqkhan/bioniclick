<?php

/**
 * TwilioIncomingPhoneNumber form base class.
 *
 * @method TwilioIncomingPhoneNumber getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseTwilioIncomingPhoneNumberForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                        => new sfWidgetFormInputHidden(),
      'sid'                       => new sfWidgetFormInputText(),
      'twilio_account_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioAccount'), 'add_empty' => false)),
      'url'                       => new sfWidgetFormInputText(),
      'http_method_type'          => new sfWidgetFormChoice(array('choices' => array('GET' => 'GET', 'POST' => 'POST'))),
      'phone_number'              => new sfWidgetFormInputText(),
      'friendly_name'             => new sfWidgetFormInputText(),
      'twilio_local_area_code_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioLocalAreaCode'), 'add_empty' => false)),
      'was_deleted'               => new sfWidgetFormInputCheckbox(),
      'purchased_date'            => new sfWidgetFormInputText(),
      'created_at'                => new sfWidgetFormDateTime(),
      'updated_at'                => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                        => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'sid'                       => new sfValidatorString(array('max_length' => 34)),
      'twilio_account_id'         => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioAccount'))),
      'url'                       => new sfValidatorString(array('max_length' => 255)),
      'http_method_type'          => new sfValidatorChoice(array('choices' => array(0 => 'GET', 1 => 'POST'))),
      'phone_number'              => new sfValidatorString(array('max_length' => 22)),
      'friendly_name'             => new sfValidatorString(array('max_length' => 255, 'required' => false)),
      'twilio_local_area_code_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioLocalAreaCode'))),
      'was_deleted'               => new sfValidatorBoolean(array('required' => false)),
      'purchased_date'            => new sfValidatorInteger(array('required' => false)),
      'created_at'                => new sfValidatorDateTime(array('required' => false)),
      'updated_at'                => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'TwilioIncomingPhoneNumber', 'column' => array('sid')))
    );

    $this->widgetSchema->setNameFormat('twilio_incoming_phone_number[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TwilioIncomingPhoneNumber';
  }

}
