<?php

/**
 * TwilioCallerPhoneNumber form base class.
 *
 * @method TwilioCallerPhoneNumber getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseTwilioCallerPhoneNumberForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'           => new sfWidgetFormInputHidden(),
      'phone_number' => new sfWidgetFormInputText(),
      'is_blocked'   => new sfWidgetFormInputCheckbox(),
      'reason'       => new sfWidgetFormTextarea(),
      'created_at'   => new sfWidgetFormDateTime(),
      'updated_at'   => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'           => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'phone_number' => new sfValidatorString(array('max_length' => 22)),
      'is_blocked'   => new sfValidatorBoolean(array('required' => false)),
      'reason'       => new sfValidatorString(array('max_length' => 300, 'required' => false)),
      'created_at'   => new sfValidatorDateTime(array('required' => false)),
      'updated_at'   => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'TwilioCallerPhoneNumber', 'column' => array('phone_number')))
    );

    $this->widgetSchema->setNameFormat('twilio_caller_phone_number[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TwilioCallerPhoneNumber';
  }

}
