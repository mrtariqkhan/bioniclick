<?php

/**
 * TwilioState form base class.
 *
 * @method TwilioState getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseTwilioStateForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'code'              => new sfWidgetFormInputText(),
      'name'              => new sfWidgetFormInputText(),
      'twilio_country_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioCountry'), 'add_empty' => false)),
      'created_at'        => new sfWidgetFormDateTime(),
      'updated_at'        => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'code'              => new sfValidatorPass(array('required' => false)),
      'name'              => new sfValidatorPass(),
      'twilio_country_id' => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioCountry'))),
      'created_at'        => new sfValidatorDateTime(array('required' => false)),
      'updated_at'        => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->widgetSchema->setNameFormat('twilio_state[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TwilioState';
  }

}
