<?php

/**
 * TwilioLocalAreaCode form base class.
 *
 * @method TwilioLocalAreaCode getObject() Returns the current form's model object
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormGeneratedTemplate.php 29553 2010-05-20 14:33:00Z Kris.Wallsmith $
 */
abstract class BaseTwilioLocalAreaCodeForm extends BaseFormDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'id'                => new sfWidgetFormInputHidden(),
      'code'              => new sfWidgetFormInputText(),
      'phone_number_type' => new sfWidgetFormChoice(array('choices' => array('local' => 'local', 'toll free' => 'toll free'))),
      'twilio_state_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioState'), 'add_empty' => true)),
      'created_at'        => new sfWidgetFormDateTime(),
      'updated_at'        => new sfWidgetFormDateTime(),
    ));

    $this->setValidators(array(
      'id'                => new sfValidatorChoice(array('choices' => array($this->getObject()->get('id')), 'empty_value' => $this->getObject()->get('id'), 'required' => false)),
      'code'              => new sfValidatorInteger(),
      'phone_number_type' => new sfValidatorChoice(array('choices' => array(0 => 'local', 1 => 'toll free'))),
      'twilio_state_id'   => new sfValidatorDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioState'), 'required' => false)),
      'created_at'        => new sfValidatorDateTime(array('required' => false)),
      'updated_at'        => new sfValidatorDateTime(array('required' => false)),
    ));

    $this->validatorSchema->setPostValidator(
      new sfValidatorDoctrineUnique(array('model' => 'TwilioLocalAreaCode', 'column' => array('code')))
    );

    $this->widgetSchema->setNameFormat('twilio_local_area_code[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TwilioLocalAreaCode';
  }

}
