<?php

class TwilioIncomingPhoneNumberEditForm extends TwilioIncomingPhoneNumberForm {

    const FIELD_NAME_URL = 'url';


    public function  configure() {

        parent::configure();

        unset (
            $this['twilio_account_id'],
            $this['phone_number'],
            $this['sid'],
            $this['twilio_local_area_code_id'],
            $this['was_deleted'],
            $this['purchased_date']
        );

        $this->configureFieldUrl();
    }

    protected function configureFieldUrl() {

        $fieldName = self::FIELD_NAME_URL;

        $this->getValidator($fieldName)
            ->setOption('min_length', 1)
            ->setOption('trim', true)
        ;

        $postValidatorNew = new sfValidatorCallback(
            array(
                'callback'  => array('GlobalValidatorTwilioIncomingPhoneNumber', 'checkIfUrlIsAllowed'),
                'arguments' => array(
                    GlobalValidatorTwilioIncomingPhoneNumber::OPTION_URL_FIELD_NAME => $fieldName,
                ),
            )
        );

        $this->addPostValidator($postValidatorNew);
    }
}

