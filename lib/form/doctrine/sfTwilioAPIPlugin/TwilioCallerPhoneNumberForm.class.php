<?php

/**
 * TwilioCallerPhoneNumber form.
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class TwilioCallerPhoneNumberForm extends PluginTwilioCallerPhoneNumberForm {

    public function configure() {
        unset(
            $this['phone_number']
        );

        $object = $this->getObject();

        $wantToBlock = !empty($object) && !$object->isNew() && $object->getIsBlocked();
        if ($wantToBlock) {
            $reasonValidator = new sfValidatorString(
                array(
                    'max_length'    => 300,
                    'min_length'    => 5,
                    'trim'          => true
                )
            );
            $this->setValidator('reason', $reasonValidator);
        }

        if (!$wantToBlock) {
            $this->setWidget('reason', new sfWidgetFormInputHidden());
            $this->setValidator('reason', new sfValidatorPass());
        } else {
            $this->setValidator('reason', new sfValidatorString(array('max_length' => 300, 'min_length' => 5, 'required' => true)));
        }


        if (empty($object)) {
            unset($this['is_blocked']);
        } else {
            $this->setWidget('is_blocked', new sfWidgetFormInputHidden());
            $this->setValidator('is_blocked', new sfValidatorPass());
        }
    }

    protected function doSave($con = null) {

        if (null === $con) {
            $con = $this->getConnection();
        }

        $this->updateObject();

        $object = $this->getObject();
        if ($object->getIsBlocked()) {
            $object->block($con);
        } else {
            $object->unblock($con);
        }

        // embedded forms
        $this->saveEmbeddedForms($con);
    }
}
