<?php

/**
 * Package form.
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PackageForm extends BasePackageForm {

    public function configure() {

        unset(
            $this['company_id'],
            $this['was_deleted']
        );

        $this->setFieldsLabels();
    }

    protected function setFieldsLabels() {

        $currencySign = sfConfig::get('app_package_default_currency_sign', null);

        $needCurrencySignFields = array(
            'monthly_fee',
            'toll_free_number_monthly_fee',
            'local_number_monthly_fee',
            'toll_free_number_per_minute',
            'local_number_per_minute',
        );

        $widgetSchema = $this->getWidgetSchema();
        foreach ($needCurrencySignFields as $needCurrencySignField) {
            $labelOld = sfInflector::humanize($needCurrencySignField);
            $labelNew = "$labelOld ($currencySign)";
            $widgetSchema->setLabel($needCurrencySignField, $labelNew);
        }
    }
}
