<?php

class GlobalValidatorCampaign extends GlobalValidatorBase {

    const OPTION_ADVERTISER_ID    = 'advertiserId';


    /**
     * @param sfValidatorCallback $validatorObject
     * @param array $values values from form
     * @param array $options additional arguments, which Form sends to this method while its configuring
     *
     * @throw sfValidatorError, Exception
     */
    public static function checkCampaignsAmountLimitPerAdvertiser($validator, $values, $options) {

        $advertiserId = self::getArrayValueRequired($options, self::OPTION_ADVERTISER_ID);

        if (AdvertiserTable::isCampaignLimitExhausted($advertiserId)) {
            throw new sfValidatorError($validator, 'Campaigns amount limit is exhausted per advertiser.');
        }

        return $values;
    }
}
