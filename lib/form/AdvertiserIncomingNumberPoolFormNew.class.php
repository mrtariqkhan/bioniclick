<?php

/**
 * AdvertiserIncomingNumberPool form.
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class AdvertiserIncomingNumberPoolFormNew extends AdvertiserIncomingNumberPoolForm {

    const OPTION_USER                           = 'user';
    const OPTION_PHONE_NUMBER_TYPE_CODE         = 'phoneNumberTypeCode';
    const OPTION_PHONE_NUMBER_COUNTRY_ID        = 'phoneNumberCountry';
    const OPTION_PHONE_NUMBER_LOCAL_AREA_CODE_ID= 'twilioLocalAreaCode';
    const OPTION_PHONE_NUMBERS_COUNT            = 'numbersCount';
    const OPTION_INCOMING_NUMBERS_QUEUE         = 'incomingNumbersQueue';
    const OPTION_PHONE_NUMBER_NOTES             = 'phoneNumberNotes';

    const FIELD_NAME_PHONE_NUMBER_LOCAL_AREA_CODE   = 'twilio_local_area_code_id';
    const FIELD_NAME_PHONE_NUMBER_TYPE_CODE         = 'phone_number_type';
    const FIELD_NAME_PHONE_NUMBER_COUNTRY_ID        = 'phone_number_country_id';
    const FIELD_NAME_PHONE_NUMBERS_COUNT            = 'numbers_count';
    const FIELD_NAME_CAMPAIGN_ID                    = 'campaign_id';
    const FIELD_NAME_PHONE_NUMBER_NOTES             = 'notes';


    public function configure() {

        parent::configure();
        
        $user = $this->getOption(self::OPTION_USER);
        if (empty($user)) {
            throw new Exception('user option is required for new AdvertiserIncomingNumberPool');
        }

        $this->setBionicFormatter();

        $this->configureFieldCountOfNumbers();
        $this->configureFieldPhoneNumberNotes();
        $this->configureFieldTwilioLocalAreaCode();

        $this->setFieldsOrder();
    }

    protected function configureFieldTwilioLocalAreaCode() {

        $this->configureFieldPhoneNumberType();
        $this->configureFieldPhoneNumberCountryId();


        $fieldName = self::FIELD_NAME_PHONE_NUMBER_LOCAL_AREA_CODE;

        $url = 'twilioIncomingPhoneNumber/ajaxFillAreaCodeAutocompleteFullNew';

        $formTagName = $this->getName();
        $phoneNumberFieldName           = self::FIELD_NAME_PHONE_NUMBER_TYPE_CODE;
        $phoneNumberCountryIdFieldName  = self::FIELD_NAME_PHONE_NUMBER_COUNTRY_ID;

        $defaultLocalAreaCode = $this->getOption(self::OPTION_PHONE_NUMBER_LOCAL_AREA_CODE_ID, null);

        $autocompleterWidget = new BionicWidgetFormDoctrineJQueryAutocompleter(array(
            'default'                       => $defaultLocalAreaCode,

            'model'                         => 'TwilioLocalAreaCode',
            'url'                           => $url,
            'name_form'                     => $formTagName,
            'name_phone_number'             => $phoneNumberFieldName,
            'name_phone_number_country_id'  => $phoneNumberCountryIdFieldName,
        ));

        $this->setWidget($fieldName, $autocompleterWidget);
        $this->getWidgetSchema()
            ->setLabel($fieldName, 'Area code')
            ->setHelp($fieldName, "Double-click, begin typing in field")
        ;

        $this
            ->setValidator(
                $fieldName,
                new sfValidatorDoctrineChoice(
                    array(
                        'model' => 'TwilioLocalAreaCode',
                        'query' => TwilioLocalAreaCodeTable::createQueryExistentByPhoneNumberType()
                    )
                )
            )
        ;
    }

    protected function configureFieldPhoneNumberType() {

        $fieldName = self::FIELD_NAME_PHONE_NUMBER_TYPE_CODE;

        $default = null;
        if ($choices = TwilioLocalAreaCodeTable::findPhoneNumberTypesCoded()) {
            $default = $this->getOption(self::OPTION_PHONE_NUMBER_TYPE_CODE, current($choices));
        }

        $this->configureFieldChoiceWithAll($fieldName, $choices, $default);
    }

    protected function configureFieldPhoneNumberCountryId() {

        $fieldName = self::FIELD_NAME_PHONE_NUMBER_COUNTRY_ID;

        $default = null;
        if ($choices = TwilioCountryTable::findAllNames()) {
            $default = $this->getOption(self::OPTION_PHONE_NUMBER_COUNTRY_ID, current($choices));
        }

        $this->configureFieldChoiceWithAll($fieldName, $choices, $default);
    }

    protected function configureFieldCountOfNumbers() {

        $fieldName = self::FIELD_NAME_PHONE_NUMBERS_COUNT;

        $this->setWidget(
            $fieldName,
            new sfWidgetFormInputText(array('default' => 1))
        );

        $this->setValidator(
            $fieldName,
            new sfValidatorInteger(array('min' => 1))
        );

        if ($default = $this->getOption(self::OPTION_PHONE_NUMBERS_COUNT)) {
            $this->getWidget($fieldName)->setDefault($default);
        }
    }

    protected function configureFieldPhoneNumberNotes() {

        $fieldName = self::FIELD_NAME_PHONE_NUMBER_NOTES;

        $this->setWidget(
            $fieldName,
            new sfWidgetFormTextarea(array('default' => ''))
        );

        $this->setValidator(
            $fieldName,
            new sfValidatorString(array(
                'max_length' => 700, 
                'required' => false,
            ))
        );

        if ($default = $this->getOption(self::OPTION_PHONE_NUMBER_NOTES)) {
            $this->getWidget($fieldName)->setDefault($default);
        }
    }

    protected function setFieldsOrder() {

        $this->widgetSchema->moveField(
            self::FIELD_NAME_PHONE_NUMBER_TYPE_CODE,
            sfWidgetFormSchema::FIRST
        );
        $this->widgetSchema->moveField(
            self::FIELD_NAME_PHONE_NUMBER_COUNTRY_ID,
            sfWidgetFormSchema::AFTER,
            self::FIELD_NAME_PHONE_NUMBER_TYPE_CODE
        );
        $this->widgetSchema->moveField(
            self::FIELD_NAME_PHONE_NUMBER_LOCAL_AREA_CODE,
            sfWidgetFormSchema::AFTER,
            self::FIELD_NAME_PHONE_NUMBER_COUNTRY_ID
        );
        $this->widgetSchema->moveField(
            self::FIELD_NAME_CAMPAIGN_ID,
            sfWidgetFormSchema::AFTER,
            self::FIELD_NAME_PHONE_NUMBER_LOCAL_AREA_CODE
        );
        $this->widgetSchema->moveField(
            self::FIELD_NAME_PHONE_NUMBERS_COUNT,
            sfWidgetFormSchema::AFTER,
            self::FIELD_NAME_CAMPAIGN_ID
        );
        $this->widgetSchema->moveField(
            self::FIELD_NAME_PHONE_NUMBER_NOTES,
            sfWidgetFormSchema::AFTER,
            self::FIELD_NAME_PHONE_NUMBERS_COUNT
        );
    }

    public function getSuccessfullyPurchasedAmount() {
        return $this->successfullyPurchasedAmount;
    }

    public function save($con = null) {

        if (!$this->isValid()) {
            throw $this->getErrorSchema();
        }

        if (null === $con) {
            $con = $this->getConnection();
        }

        $object = $this->getObject();


        $advertiser = $object->getAdvertiser();
        $user = $this->getOption(self::OPTION_USER);
        $now = time();

        $incomingNumbersQueue = $this->getOption(self::OPTION_INCOMING_NUMBERS_QUEUE, null);
        if (empty($incomingNumbersQueue)) {
            $incomingNumbersQueue = new IncomingNumbersQueue();
            $incomingNumbersQueue->setAdvertiser($advertiser);
            $incomingNumbersQueue->setDoneAmount(0);
            $incomingNumbersQueue->setCreatedDate($now);
            $incomingNumbersQueue->setStatus(IncomingNumbersQueueTable::STATUS_TYPE_OPEN);
        }

        //NOTE:: it is strange, but:
        // <code> $incomingNumbersQueue->setCampaignId($this->getValue(self::FIELD_NAME_CAMPAIGN_ID))
        // 
        // does not change $incomingNumbersQueue->campaign relation.
        // 
        // 1). if campaign was NULL and now is not NULL, then saving will throw SQL exception.
        // 2). if campaign was not null now is not NULL but user has changed it, then 
        //       <code> will not change relation, <code> will change $incomingNumbersQueue->campaignId only.
        //(1) - Wrong, DOES NOT WORK.
        //(2) - Wrong, but DOES WORK.
        // 
        // We have found temporary solution:
        $campaignId = $this->getValue(self::FIELD_NAME_CAMPAIGN_ID);
        $campaign = empty($campaignId) ? null : CampaignTable::getInstance()->find($campaignId);
        $incomingNumbersQueue->setCampaign($campaign);

        $incomingNumbersQueue->setTwilioLocalAreaCodeId($this->getValue(self::FIELD_NAME_PHONE_NUMBER_LOCAL_AREA_CODE));
        $incomingNumbersQueue->setAmount($this->getValue(self::FIELD_NAME_PHONE_NUMBERS_COUNT));
        $incomingNumbersQueue->setAuthorNotes($this->getValue(self::FIELD_NAME_PHONE_NUMBER_NOTES));
        $incomingNumbersQueue->setUser($user);
        $incomingNumbersQueue->setUpdatedDate($now);

        if (empty($campaign)) {
            $incomingNumbersQueue->setOrderType(IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE);
        } else {
            $incomingNumbersQueue->setOrderType(IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE_AND_ASSIGN);
        }

        $incomingNumbersQueue->save($con);

        return $incomingNumbersQueue;
    }
}
