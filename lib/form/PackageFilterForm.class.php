<?php

/**
 * Description of CompanyFilterForm
 *
 * @author fairdev
 */
class PackageFilterForm extends fdBaseFilter {

    public function setup() {

        $this->widgetSchema->setNameFormat('package_filter[%s]');

        parent::setup();
    }

    public function  configure() {

        $needFields = array(
            'name',
            'monthly_fee',
            'toll_free_number_monthly_fee',
            'local_number_monthly_fee',
            'toll_free_number_per_minute',
            'local_number_per_minute',
        );
        foreach ($needFields as $field) {
            $this->setWidget($field, new sfWidgetFormInputText());
            $this->setValidator($field, new sfValidatorString(array('max_length' => 100, 'required' => false)));
            $this->addFilterField(array($field => array($field)));
        }

        parent::configure();

        $this->configureEmbeddedForms();
    }

    protected function configureEmbeddedForms() {

        $defaults   = $this->getDefaults();
        $options    = $this->getOptions();

        $this->embedForm('branching', new BranchFilterForm($defaults, $options));

        $embeddedForms = $this->getEmbeddedForms();
        foreach ($embeddedForms as $name => $embeddedForm) {
            $this->getWidgetSchema()->setLabel($name, ' ');
        }
    }
}
