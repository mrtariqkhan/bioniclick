<?php

/**
 * sfGuardUserProfileFormEdit form.
 */
class sfGuardUserProfileFormEdit extends UserFormBase {

    protected $sfGuardUser = null;


    public function getModelName() {
        return 'sfGuardUserProfile';
    }

    public function setup() {

        parent::setup();

        $this->widgetSchema->setNameFormat('sf_guard_user_profile[%s]');
    }

    public function configure() {

        if ($this->isNew()) {
            throw new Exception('Object is missed.');
        }

        $sfGuardUser = $this->getObject()->getUser();
        if (empty($sfGuardUser) || !$sfGuardUser->exists()) {
            throw new Exception('Object is missed.');
        }
        $this->setSfGuardUser($sfGuardUser);

        parent::configure();

        $this->configureFieldFirstName(self::FIELD_NAME_FIRST_NAME, $sfGuardUser->getFirstName());
        $this->configureFieldLastName(self::FIELD_NAME_LAST_NAME, $sfGuardUser->getLastName());
        $this->configureFieldEmail($sfGuardUser->getId());
        $this->configureFieldUsername($sfGuardUser->getId(), self::FIELD_NAME_USERNAME, $sfGuardUser->getUsername());
        $this->configureFieldTimezoneId();

        $this->setFieldsOrder();
    }

    protected function doSave($con = null) {

        if (null === $con) {
            $con = $this->getConnection();
        }

        $sfGuardUser = $this->getSfGuardUser();

        $sfGuardUser->setFirstName($this->getValue(self::FIELD_NAME_FIRST_NAME));
        $sfGuardUser->setLastName($this->getValue(self::FIELD_NAME_LAST_NAME));
        $sfGuardUser->setUsername($this->getValue(self::FIELD_NAME_USERNAME));
        $sfGuardUser->setEmailAddress($this->getValue(self::FIELD_NAME_EMAIL));

        $sfGuardUser->save($con);

        parent::doSave($con);
    }

    protected function setFieldsOrder() {

        $widgetSchema = $this->getWidgetSchema();

        $widgetSchema->moveField(self::FIELD_NAME_FIRST_NAME, sfWidgetFormSchema::FIRST);
        $widgetSchema->moveField(self::FIELD_NAME_LAST_NAME, sfWidgetFormSchema::AFTER, self::FIELD_NAME_FIRST_NAME);
        $widgetSchema->moveField(self::FIELD_NAME_EMAIL, sfWidgetFormSchema::AFTER, self::FIELD_NAME_LAST_NAME);
        $widgetSchema->moveField(self::FIELD_NAME_USERNAME, sfWidgetFormSchema::AFTER, self::FIELD_NAME_EMAIL);
        $widgetSchema->moveField(self::FIELD_NAME_TIMEZONE_ID, sfWidgetFormSchema::AFTER, self::FIELD_NAME_USERNAME);
    }

    protected function getSfGuardUser() {
        return $this->sfGuardUser;
    }
    protected function setSfGuardUser($sfGuardUser) {
        $this->sfGuardUser = $sfGuardUser;
    }
}
