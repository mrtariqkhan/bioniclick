<?php

/**
 * Description of CompanyFilterForm
 *
 * @author fairdev
 */
class CompanyFilterForm extends fdBaseFilter {

    public function setup() {

        $this->widgetSchema->setNameFormat('company_filter[%s]');

        parent::setup();
    }

    public function  configure() {

        $needFields = array(
            'name',
            'url',
            'default_phone_number',
        );     
        foreach ($needFields as $field) {
            $this->setWidget($field, new sfWidgetFormInputText());
            $this->setValidator($field, new sfValidatorString(array('max_length' => 100, 'required' => false)));
            $this->addFilterField(array($field => array($field)));
        }
        
        $this->getWidgetSchema()->setLabel('default_phone_number', 'Contact number');

        parent::configure();

        $this->configureEmbeddedForms();
    }

    protected function configureEmbeddedForms() {

        $defaults   = $this->getDefaults();
        $options    = $this->getOptions();

        $this->embedForm('branching', new BranchFilterForm($defaults, $options));

        $embeddedForms = $this->getEmbeddedForms();
        foreach ($embeddedForms as $name => $embeddedForm) {
            $this->getWidgetSchema()->setLabel($name, ' ');
        }
    }
}
