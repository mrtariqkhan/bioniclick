<?php

/**
 * Advertiser form.
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class AdvertiserFormCurrentEdit extends AdvertiserForm {

    public function configure() {

        if ($this->isNew()) {
            throw new Exception('Object is missed.');
        }

        $this->setOption(self::OPTION_PARENT_COMPANY, null);

        parent::configure();
    }

    protected function configureFieldPackage() {

        unset(
            $this['package_id']
        );
    }
}
