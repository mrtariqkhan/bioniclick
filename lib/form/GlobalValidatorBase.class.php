<?php

class GlobalValidatorBase {

    /**
     * @param array $arr array of values
     * @param mixed $key key of need value
     * @return mixed need value
     * @throw Exception
     */
    protected static function getArrayValueRequired($arr, $key) {
        return self::getArrayValue($arr, $key, true);
    }

    /**
     * @param array $arr array of values
     * @param mixed $key key of need value
     * @param mixed $default default value of need value
     * @return mixed need value
     * @throw Exception
     */
    protected static function getArrayValueNonRequired($arr, $key, $default = null) {
        return self::getArrayValue($arr, $key, false, $default);
    }

    /**
     *
     * @param array $arr array of values
     * @param mixed $key key of need value
     * @param boolean $isRequired if key id required
     * @param mixed $default default value of need value
     * @return mixed need value
     * @throws Exception 
     */
    protected static function getArrayValue($arr, $key, $isRequired, $default = null) {

        if (!empty($arr) && array_key_exists($key, $arr)) {
            return $arr[$key];
        }

        if ($isRequired) {
            throw new Exception("$key is required.");
        }

        return $default;
    }
}
