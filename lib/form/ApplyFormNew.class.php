<?php

/**
 * Description of ApplyFormNew
 *
 * @author root
 */
class ApplyFormNew extends UserGroupedFormBase {

    const FIELD_NAME_EMAIL_2        = 'email2';
    const FIELD_NAME_PASSWORD_2     = 'password2';
    const FIELD_NAME_COMPANY_ID     = 'company_id';
    const FIELD_NAME_ADVERTISER_ID  = 'advertiser_id';

    protected $userId = null;


    public function getModelName() {
        return 'sfGuardUserProfile';
    }

    public function setup() {

        parent::setup();

        $this->widgetSchema->setNameFormat('sfApplyApply[%s]');
    }

    public function configure() {

        parent::configure();

        if (!$this->isNew()) {
            throw new Exception('Object should be missed.');
        }

        $this->configureFieldFirstName();
        $this->configureFieldLastName();
        $this->configureFieldPassword();
        $this->configureFieldPassword2();
        $this->configureFieldEmail(null);
        $this->configureFieldEmail2();
        $this->configureFieldsCompanyAdvertiser();
        $this->configureFieldGroupList(null);
        $this->configureFieldTimezoneId();

        $this->setFieldsOrder();
    }

    protected function configureFieldPassword2() {

        $fieldName1 = self::FIELD_NAME_PASSWORD;
        $fieldName2 = self::FIELD_NAME_PASSWORD_2;

        $this->copyWidget($fieldName1, $fieldName2);
        $this->copyValidator($fieldName1, $fieldName2);

        $this->getWidgetSchema()->setLabel(self::FIELD_NAME_PASSWORD_2, 'Confirm password');

        $this->getWidgetSchema()->setHelp(
            $fieldName2,
            "Length must be greater then " . (self::PASSWORD_MIN_LENGTH - 1)
        );

        $postValidatorNew = new sfValidatorSchemaCompare(
            $fieldName1,
            sfValidatorSchemaCompare::EQUAL,
            $fieldName2,
            array(),
            array('invalid' => 'The passwords did not match.')
        );

        $this->addPostValidator($postValidatorNew);
    }

    protected function configureFieldEmail2() {

        $fieldName1 = self::FIELD_NAME_EMAIL;
        $fieldName2 = self::FIELD_NAME_EMAIL_2;

        $this->copyWidget($fieldName1, $fieldName2);
        $this->copyValidator($fieldName1, $fieldName2);

        $this->getWidgetSchema()->setLabel(self::FIELD_NAME_EMAIL_2, 'Confirm email');

        $postValidatorNew = new sfValidatorSchemaCompare(
            $fieldName1,
            sfValidatorSchemaCompare::EQUAL,
            $fieldName2,
            array(),
            array('invalid' => 'The email addresses did not match.')
        );

        $this->addPostValidator($postValidatorNew);
    }

    protected function configureFieldsCompanyAdvertiser() {

        $object = $this->getObject();

        $currentCompanyAdmin = $this->getSfGuardUserAdmin();
        if ($currentCompanyAdmin->isAdvertiserUser()) {
            unset($this[self::FIELD_NAME_COMPANY_ID]);
            $fieldName = self::FIELD_NAME_ADVERTISER_ID;
            $object->setAdvertiserId($currentCompanyAdmin->getAdvertiserId());
        } else {
            unset($this[self::FIELD_NAME_ADVERTISER_ID]);
            $fieldName = self::FIELD_NAME_COMPANY_ID;
            $object->setCompanyId($currentCompanyAdmin->getCompanyId());
        }

        $this->setWidget($fieldName, new sfWidgetFormInputHidden());
        $this->setValidator($fieldName, new sfValidatorPass());
    }

    protected function setFieldsOrder() {

        $widgetSchema = $this->getWidgetSchema();
        $widgetSchema->moveField(self::FIELD_NAME_FIRST_NAME, sfWidgetFormSchema::FIRST);
        $widgetSchema->moveField(self::FIELD_NAME_LAST_NAME, sfWidgetFormSchema::AFTER, self::FIELD_NAME_FIRST_NAME);
        $widgetSchema->moveField(self::FIELD_NAME_EMAIL, sfWidgetFormSchema::AFTER, self::FIELD_NAME_LAST_NAME);
        $widgetSchema->moveField(self::FIELD_NAME_EMAIL_2, sfWidgetFormSchema::AFTER, self::FIELD_NAME_EMAIL);
        $widgetSchema->moveField(self::FIELD_NAME_PASSWORD, sfWidgetFormSchema::AFTER, self::FIELD_NAME_EMAIL_2);
        $widgetSchema->moveField(self::FIELD_NAME_PASSWORD_2, sfWidgetFormSchema::AFTER, self::FIELD_NAME_PASSWORD);
    }

    protected function doSave($con = null) {

        if (null === $con) {
            $con = $this->getConnection();
        }

        $isActive = sfGuardUserProfileTable::ifUseSimpleUserCreation();

        $user = new sfGuardUser();
        $user->setFirstName($this->getValue(self::FIELD_NAME_FIRST_NAME));
        $user->setLastName($this->getValue(self::FIELD_NAME_LAST_NAME));
        $user->setUsername($this->getValue(self::FIELD_NAME_EMAIL));
        $user->setEmailAddress($this->getValue(self::FIELD_NAME_EMAIL));
        $user->setPassword($this->getValue(self::FIELD_NAME_PASSWORD));
        $user->setIsActive($isActive);

        $user->saveSimple($con);

        $this->userId = $user->getId();

        $this->saveGroupsList($user, $con);
        $user->saveSimple($con);

        parent::doSave($con);
    }

    public function updateObject($values = null) {

        $object = parent::updateObject($values);

        $object->setUserId($this->userId);

        if ($this->isNew()) {
            $object->setValidateByType(sfGuardUserProfileTable::VALIDATE_TYPE_NEW);
        }

        return $object;
    }

    protected function getGroupIdsSelected() {
        return $this->getValue(self::FIELD_NAME_GROUPS_LIST);
    }

    protected function getGroupIdsExisting() {
        return array();
    }
}
