<?php

/**
 * Description of DateRangePickerFilterForm
 *
 * @author fairdev
 */
class DateRangePickerFilterForm extends fdBaseFilter {

    const TYPE_CODE_DATE_RANGE          = 'date_range';
    const TYPE_CODE_TODAY               = 'today';
    const TYPE_CODE_YESTERDAY           = 'yesterday';
    const TYPE_CODE_THIS_WEEK_SUN       = 'this_week_sun';
    const TYPE_CODE_THIS_WEEK_MON       = 'this_week_mon';
    const TYPE_CODE_LAST_7_DAYS         = 'last_seven_days';
    const TYPE_CODE_LAST_WEEK_SUN       = 'last_week_sun';
    const TYPE_CODE_LAST_WEEK_MON       = 'last_week_mon';
    const TYPE_CODE_LAST_WEEK_BUISNESS  = 'last_business_week';
    const TYPE_CODE_LAST_14_DAYS        = 'last_fourteen_days';
    const TYPE_CODE_THIS_MONTH          = 'this_month';
    const TYPE_CODE_LAST_30_DAYS        = 'last_thirty_days';
    const TYPE_CODE_LAST_MONTH          = 'last_month';
    const TYPE_CODE_ALL                 = 'all_time';

    const FIELD_NAME_TYPE               = 'type';
    const FIELD_NAME_DATE_RANGE         = 'date_range';

    public function configure() {

        if (!$this->getOption('timezone_name', false)) {
            throw new Exception('timezon_name is required option for CallLogFilterForm.');
        }

        $this->configureFieldType();
        $this->configureFieldDateRange();


        $needFilterFields = array(
            self::FIELD_NAME_TYPE,
            self::FIELD_NAME_DATE_RANGE,
        );
        $this->addFilterField(array('from' => $needFilterFields));
        $this->addFilterField(array('to' => $needFilterFields));

        parent::configure();
    }

    protected function configureFieldType() {

        $fieldName = self::FIELD_NAME_TYPE;
        $default = $this->getDefault($fieldName);
        $choices = $this->createDateTypeList();

        $this->configureFieldChoice(
            $fieldName, 
            $choices, 
            $default, 
            false
        );
    }

    protected function configureFieldDateRange() {

        $fieldName = self::FIELD_NAME_DATE_RANGE;

        $this->setWidget(
            $fieldName,
            new sfWidgetFormInputText(
                array(), 
                array(
                    'class'         => 'date-picker',
                    'autocomplete'  => 'off',
                    'readonly'      => $this->getOption('input_is_readonly', false),
                )
            )
        );
        $this->setValidator(
            $fieldName,
            new sfValidatorString(
                array(
                    'required' => false
                )
            )
        );
    }
    
    protected static function createDateTypeList() {

        return array(
            self::TYPE_CODE_DATE_RANGE          => 'Date range',
            self::TYPE_CODE_TODAY               => 'Today',
            self::TYPE_CODE_YESTERDAY           => 'Yesterday',
            self::TYPE_CODE_THIS_WEEK_SUN       => 'This week (Sun - Today)',
            self::TYPE_CODE_THIS_WEEK_MON       => 'This week (Mon - Today)',
            self::TYPE_CODE_LAST_7_DAYS         => 'Last 7 days',
            self::TYPE_CODE_LAST_WEEK_SUN       => 'Last week (Sun - Sat)',
            self::TYPE_CODE_LAST_WEEK_MON       => 'Last week (Mon - Sun)',
            self::TYPE_CODE_LAST_WEEK_BUISNESS  => 'Last business week (Mon - Fri)',
            self::TYPE_CODE_LAST_14_DAYS        => 'Last 14 days',
            self::TYPE_CODE_THIS_MONTH          => 'This month',
            self::TYPE_CODE_LAST_30_DAYS        => 'Last 30 days',
            self::TYPE_CODE_LAST_MONTH          => 'Last month',
            self::TYPE_CODE_ALL                 => 'All time',
        );
    }

    public function buildFromValue(array $args) {

        $range = array();
        if ($args['type'] == self::TYPE_CODE_DATE_RANGE) {
            $range = $this->convertRangeType($args['date_range']);
        } else {
            $range = $this->convertType($args['type']);
        }
        $result = array_key_exists('from', $range) ? $range['from'] : false;

        return $result;
    }

    public function buildToValue(array $args) {

        $range = array();
        if ($args['type'] == self::TYPE_CODE_DATE_RANGE) {
            $range = $this->convertRangeType($args['date_range']);
        } else {
            $range = $this->convertType($args['type']);
        }
        $result = array_key_exists('to', $range) ? $range['to'] : false;

        return $result;
    }



    protected function convertType($value) {

        $timezoneName = $this->getOption('timezone_name');
        return $this->typeHandler($value, $timezoneName);
    }

    protected function convertRangeType($value) {

        $timezoneName = $this->getOption('timezone_name');
        return $this->rangeTypeHandler($value, $timezoneName);
    }


    protected function typeHandler($value, $timezoneName) {

        $result = array();
        switch ($value) {
            case self::TYPE_CODE_TODAY:
                $result['from'] = TimeConverter::getTodayUnixTimestamp($timezoneName);
                $result['to']   = TimeConverter::getUnixTimestamp();
                break;
            case self::TYPE_CODE_YESTERDAY:
                $result = TimeConverter::getYesterdayUnixTimestampRange($timezoneName);
                break;
            case self::TYPE_CODE_THIS_WEEK_SUN:
                $result['from'] = TimeConverter::getLastSundayUnixTimestamp($timezoneName);
                $result['to']   = TimeConverter::getUnixTimestamp();
                break;
            case self::TYPE_CODE_THIS_WEEK_MON:
                $result['from'] = TimeConverter::getLastMondayUnixTimestamp($timezoneName);
                $result['to']   = TimeConverter::getUnixTimestamp();
                break;
            case self::TYPE_CODE_LAST_7_DAYS:
                $result['from'] = TimeConverter::getNDaysAgoUnixTimestamp($timezoneName, 7);
                $result['to']   = TimeConverter::getUnixTimestamp();
                break;
            case self::TYPE_CODE_LAST_WEEK_SUN:
                $result = TimeConverter::getLastWeekSunUnixTimestampRange($timezoneName);
                break;
            case self::TYPE_CODE_LAST_WEEK_MON:
                $result = TimeConverter::getLastWeekMonUnixTimestampRange($timezoneName);
                break;
            case self::TYPE_CODE_LAST_WEEK_BUISNESS:
                $result = TimeConverter::getLastBusinessWeekUnixTimestampRange($timezoneName);
                break;
            case self::TYPE_CODE_LAST_14_DAYS:
                $result['from'] = TimeConverter::getNDaysAgoUnixTimestamp($timezoneName, 14);
                $result['to']   = TimeConverter::getUnixTimestamp();
                break;
            case self::TYPE_CODE_THIS_MONTH:
                $result['from'] = TimeConverter::getThisMonthUnixTimestamp($timezoneName);
                $result['to']   = TimeConverter::getUnixTimestamp();
                break;
            case self::TYPE_CODE_LAST_30_DAYS:
                $result['from'] = TimeConverter::getNDaysAgoUnixTimestamp($timezoneName, 30);
                $result['to']   = TimeConverter::getUnixTimestamp();
                break;
            case self::TYPE_CODE_LAST_MONTH:
                $result = TimeConverter::getLastMonthUnixTimestampRange($timezoneName);
                break;            
        }
        return $result;
    }

    protected function rangeTypeHandler($value, $timezoneName) {

        $result = array();

        if (empty($value)) {
            return $result;
        }

        if (strrpos($value, '-') === false) {
            $dateFrom   = $value;
            $dateTo     = $value;
        } else {
            $dates = array();
            preg_match_all('/\d{1,2}\/\d{1,2}\/\d{2,4}/', $value, $dates);
            $dateFrom   = $dates[0][0];
            $dateTo     = $dates[0][1];
        }

        $dateFrom   = $this->makeDateString($dateFrom);
        $dateTo     = $this->makeDateString($dateTo);

        $result['from'] = TimeConverter::getUnixTimestamp("$dateFrom 00:00:00", $timezoneName);
        $result['to']   = TimeConverter::getUnixTimestamp("$dateTo 23:59:59", $timezoneName);

        return $result;
    }

    protected function makeDateString($dateStringFormatted) {

        preg_match('/(\d{1,2})\/(\d{1,2})\/(\d{2,4})/', $dateStringFormatted, $matches);

        $month  = $matches[1];
        $day    = $matches[2];
        $year   = $matches[3];
        $dateString = "$year/$month/$day";

        return $dateString;
    }
}