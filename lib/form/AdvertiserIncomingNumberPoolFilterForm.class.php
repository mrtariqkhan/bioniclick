<?php

/**
 * Description of AdvertiserIncomingNumberPoolFilterForm
 *
 * @author fairdev
 */
class AdvertiserIncomingNumberPoolFilterForm extends fdBaseFilter {

    public function setup() {

        $this->widgetSchema->setNameFormat('advertiser_incoming_number_pool_filter[%s]');

        parent::setup();
    }

    public function configure() {

        $user = $this->getOption('user');
        $advertisersIds = $user->findSubAdvertisersIds();

        $this->configureFieldCampaign($advertisersIds);
        $this->configureFieldIncomingPhoneNumber($advertisersIds);

        $this->setFieldsOrder();

        parent::configure();

        $this->configureEmbeddedForms();
    }

    protected function configureFieldCampaign($advertisersIds) {

        $fieldName = 'campaign_id';
        $relatedModelName = 'Advertiser';

        $query = CampaignTable::createQueryByAdvertiserId($advertisersIds);

        $widget = new sfWidgetFormDoctrineChoice(array(
            'model'     => $relatedModelName,
            'query'     => $query,
            'add_empty' => true,
            'order_by'  => array('name', 'asc'),
        ));
        $this->setWidget($fieldName, $widget);

        $validator = new sfValidatorDoctrineChoice(array(
            'required'  => false, 
            'model'     => $relatedModelName,
            'column'    => 'id',
            'query'     => $query,
        ));
        $this->setValidator($fieldName, $validator);

        $this->getWidgetSchema()->setLabel($fieldName, 'Campaign');

        $this->addFilterField(array($fieldName => array($fieldName)));
    }

    protected function configureFieldIncomingPhoneNumber($advertisersIds) {

        $fieldName = 'twilio_incoming_phone_number';

        $widget = new sfWidgetFormInput();
        $this->setWidget($fieldName, $widget);

        $validator = new sfValidatorPass(array('required' => false));
        $this->setValidator($fieldName, $validator);

        $this->getWidgetSchema()->setLabel($fieldName, 'Incoming phone number');

        $this->addFilterField(array($fieldName => array($fieldName)));
    }

    protected function setFieldsOrder() {
        $this->widgetSchema->moveField('campaign_id', sfWidgetFormSchema::FIRST);
    }

    protected function configureEmbeddedForms() {

        $defaults   = $this->getDefaults();
        $options    = $this->getOptions();

        $this->embedForm('branching', new BranchFilterForm($defaults, $options));

        $embeddedForms = $this->getEmbeddedForms();
        foreach ($embeddedForms as $name => $embeddedForm) {
            $this->getWidgetSchema()->setLabel($name, ' ');
        }
    }
}
