<?php

/**
 * Description of SubAdvertisersNumbersFilterForm
 *
 * @author fairdev
 */
class SubAdvertisersNumbersFilterForm extends CustomersNumbersFilterForm {

    public function setup() {

        $this->widgetSchema->setNameFormat('sub_advertisers__numbers_filter[%s]');

        parent::setup();
    }
}

