<?php

/**
 * Description of DateRangeMonthPickerFilterForm
 *
 * @author fairdev
 */
class DateRangeMonthPickerFilterForm extends DateRangePickerFilterForm {

    public function configure() {

        $this->setOption('input_is_readonly', true);

        parent::configure();
    }

    protected static function createDateTypeList() {
        return array(
            'date_range'        => 'Date range',
            'last_month'        => 'Last month',
            'this_month'        => 'This month',
            'next_month'        => 'Next month',
        );
    }


    protected function typeHandler($value, $timezoneName) {

        $result = array();
        switch ($value) {
            case 'this_month':
                $result['from'] = TimeConverter::getThisMonthUnixTimestamp($timezoneName);
                $result['to']   = TimeConverter::getUnixTimestamp();
                break;
            case 'last_month':
                $result = TimeConverter::getLastMonthUnixTimestampRange($timezoneName);
                break;
            case 'next_month':
                $result = TimeConverter::getNextMonthUnixTimestampRange($timezoneName);
                break;
        }
        return $result;
    }

}
