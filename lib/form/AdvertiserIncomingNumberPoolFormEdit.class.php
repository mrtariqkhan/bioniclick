<?php

/**
 * AdvertiserIncomingNumberPool form.
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class AdvertiserIncomingNumberPoolFormEdit extends AdvertiserIncomingNumberPoolForm {

    const OPTION_USER = 'user';

    const FIELD_NAME_PHONE_NUMBER_ID    = 'twilio_incoming_phone_number_id';
    const FIELD_NAME_CAMPAIGN_ID        = 'campaign_id';


    public function configure() {

        parent::configure();
        
        $user = $this->getOption(self::OPTION_USER);
        if (empty($user)) {
            throw new Exception('user option is required for new AdvertiserIncomingNumberPool');
        }

        unset(
            $this[self::FIELD_NAME_PHONE_NUMBER_ID]
        );



        $this->setWidget(
            self::FIELD_NAME_PHONE_NUMBER_ID,
            new sfWidgetFormInputHidden()
        );
        $this->setValidator(
            self::FIELD_NAME_PHONE_NUMBER_ID,
            new sfValidatorDoctrineChoice(
                array(
                    'model' => 'TwilioIncomingPhoneNumber',
                    'column' => 'id',
                    'required' => false
                )
            )
        );



        $this->widgetSchema->moveField(
            self::FIELD_NAME_CAMPAIGN_ID,
            sfWidgetFormSchema::FIRST
        );
    }

    protected function doSave($con = null) {

        $user = $this->getOption(self::OPTION_USER);
        $newCampaignId = $this->getTaintedValue(self::FIELD_NAME_CAMPAIGN_ID, null);
        $advertiserIncomingNumberPool = $this->getObject();

        AdvertiserIncomingNumberPoolTable::moveNumberToCampaignFromCampaign(
            $advertiserIncomingNumberPool, 
            $newCampaignId, 
            $user, 
            $con
        );
    }
}
