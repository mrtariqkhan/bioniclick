<?php

/**
 * sfGuardUserFormEdit form.
 */
class sfGuardUserFormEdit extends UserGroupedFormBase {

    const FIELD_NAME_EMAIL_ADDRESS = 'email_address';


    public function getModelName() {
        return 'sfGuardUser';
    }

    public function setup() {

        parent::setup();

        $this->widgetSchema->setNameFormat('sfGuardUserFormEdit[%s]');
    }

    public function configure() {

        parent::configure();

        if ($this->isNew()) {
            throw new Exception('Object is missed.');
        }

        $sfGuardUser = $this->getObject();

        $this->configureFieldFirstName();
        $this->configureFieldLastName();
        $this->configureFieldEmail($sfGuardUser->getId(), self::FIELD_NAME_EMAIL_ADDRESS);
        $this->configureFieldUsername($sfGuardUser->getId());
        $this->configureFieldGroupList($sfGuardUser);
    }

    protected function configureFieldGroupList(
        $sfGuardUser,
        $fieldName = self::FIELD_NAME_GROUPS_LIST
    ) {

        parent::configureFieldGroupList($sfGuardUser, $fieldName);

        $currentCompanyAdmin = $this->getSfGuardUserAdmin();
        $postValidatorNew = new sfValidatorCallback(
            array(
                'callback' => array('GlobalValidatorUser', 'checkAdminExists'),
                'arguments' => array(
                    GlobalValidatorUser::OPTION_SF_GUARD_USER_ADMIN      => $currentCompanyAdmin,
                    GlobalValidatorUser::OPTION_IS_NEW                   => false, 
                    GlobalValidatorUser::OPTION_ID                       => $this->getObject()->getId(),
                    GlobalValidatorUser::OPTION_GROUPS_LIST_FIELD_NAME   => self::FIELD_NAME_GROUPS_LIST,
                ),
            )
        );
        $this->addPostValidator($postValidatorNew);
    }

    protected function doSave($con = null) {

        if (null === $con) {
            $con = $this->getConnection();
        }

        $this->saveGroupsList($this->getObject(), $con);

        parent::doSave($con);
    }

    protected function getGroupIdsSelected() {
        return $this->getValue(self::FIELD_NAME_GROUPS_LIST);
    }

    protected function getGroupIdsExisting() {
        return $this->getObject()->getGroups()->getPrimaryKeys();
    }
}