<?php

/**
 * ApplyCustomerAdminFormNew is embedded form.
 */
class ApplyCustomerAdminFormNew extends ApplyFormNew {

    const OPTION_IS_COMPANY_CUSTOMER = 'isCompanyCustomer';

    protected $isCompanyCustomer = null;


    public function setup() {

        parent::setup();

        $this->widgetSchema->setNameFormat('sfApplyApplyCustomer[%s]');
    }

    public function configure() {

        $this->isCompanyCustomer = $this->getOption(self::OPTION_IS_COMPANY_CUSTOMER);
        if (is_null($this->isCompanyCustomer)) {
            throw new Exception(self::OPTION_IS_COMPANY_CUSTOMER . ' option is required.');
        }

        parent::configure();
    }

    protected function configureFieldsCompanyAdvertiser() {

        $parentCompanyAdmin = $this->getSfGuardUserAdmin();
        if ($this->isCompanyCustomer) {
            unset($this[self::FIELD_NAME_ADVERTISER_ID]);
            $fieldName = self::FIELD_NAME_COMPANY_ID;
            $model = 'Company';
            $query = CompanyTable::createQueryByParentCompany($parentCompanyAdmin->getFirstCompany());
        } else {
            unset($this[self::FIELD_NAME_COMPANY_ID]);
            $fieldName = self::FIELD_NAME_ADVERTISER_ID;
            $model = 'Advertiser';
            $query = AdvertiserTable::createQueryByParentCompany($parentCompanyAdmin->getFirstCompany());
        }

//        $this->setWidget($fieldName, new sfWidgetFormDoctrineChoice(array('model' => $model)));
//        $this->getWidget($fieldName)
//            ->setOption('query', $query)
//        ;
        $this->setWidget(
            $fieldName, 
            new sfWidgetFormInputHidden()
        );

        $this->setValidator(
            $fieldName, 
            new sfValidatorDoctrineChoice(
                array(
                    'model'     => $model,
                    'query'     => $query,
                    'required'  => false,
                )
            )
        );
    }

    protected function configureFieldGroupList(
        $sfGuardUser,
        $fieldName = self::FIELD_NAME_GROUPS_LIST
    ) {

        $this->setWidget(
            $fieldName, 
            new sfWidgetFormInputHidden()
        );
        $this->setValidator(
            $fieldName, 
            new sfValidatorPass()
        );
    }

    protected function getGroupIdsSelected() {

        if ($this->isCompanyCustomer) {
            $fieldName = self::FIELD_NAME_COMPANY_ID;
        } else {
            $fieldName = self::FIELD_NAME_ADVERTISER_ID;
        }

        $companyAdvertiserId = $this->getValue($fieldName);
        $groupsIds = sfGuardGroupTable::findAdminGroupIdsForCompany($companyAdvertiserId, $this->isCompanyCustomer);

        if (empty($groupsIds)) {
            throw new Exception('Cannot create Admin, because there is no admin groups.');
        }

        return $groupsIds;
    }

    protected function getGroupIdsExisting() {
        return array();
    }
}
