<?php

class GlobalValidatorWidgetParent extends GlobalValidatorBase {

    const OPTION_COMBO_ID           = 'comboId';
    const OPTION_ID_FIELD_NAME      = 'id_field_name';
    const OPTION_HTML_ID_FIELD_NAME = 'html_id_field_name';


    /**
     * @param sfValidatorCallback $validatorObject
     * @param array $values values from form
     * @param array $options additional arguments, which Form sends to this method while its configuring
     *
     * @throw sfValidatorError, Exception
     */
    public static function checkUniqueHtmlIdInCombo($validator, $values, $options) {

        $comboId        = self::getArrayValueRequired($options, self::OPTION_COMBO_ID);
        $idFieldName    = self::getArrayValueRequired($options, self::OPTION_ID_FIELD_NAME);
        $htmlIdFieldName= self::getArrayValueRequired($options, self::OPTION_HTML_ID_FIELD_NAME);

        $id      = self::getArrayValueNonRequired($values, $idFieldName);
        $htmlId  = self::getArrayValueNonRequired($values, $htmlIdFieldName);

        $id      = is_null($id) ? null : (int)trim($id);
        $htmlId  = is_null($htmlId) ? '' : trim($htmlId);

        if (ComboTable::existComboWidgetWithSameHtmlId($id, $comboId, $htmlId)) {
            $error = new sfValidatorError($validator, "Html id: ($htmlId) exists in this combo already." );
            throw new sfValidatorErrorSchema($validator, array('html_id' => $error));
        }

        return $values;
    }
}
