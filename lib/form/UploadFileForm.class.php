<?php

class UploadFileForm extends BaseForm {

    public function configure() {

        $this->getName();

        $this->setWidget(
            'file',
            new sfWidgetFormInputFile(
                array(
                    'label' => 'Upload CSV file:'
                )
            )
        );

        $this->setValidator(
            'file',
            new sfValidatorFile(
                array(
                    'required'  => true,
                    'max_size'  => sfConfig::get('app_max_filesize'),
                    'mime_types'=> array('application/csv')
                )
            )
        );

        $this->widgetSchema->setNameFormat('upload[%s]');
    }
}
