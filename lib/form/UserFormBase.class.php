<?php

/**
 * UserFormBase form.
 */
abstract class UserFormBase extends BaseFormDoctrine {

    const NAME_MIN_LENGTH       = 2;
    const NAME_MAX_LENGTH       = 40;

    const EMAIL_MAX_LENGTH      = 80;
    const USERNAME_MAX_LENGTH   = 128;

    const PASSWORD_MIN_LENGTH   = 6;
    const PASSWORD_MAX_LENGTH   = 128;

    const FIELD_NAME_FIRST_NAME     = 'first_name';
    const FIELD_NAME_LAST_NAME      = 'last_name';
    const FIELD_NAME_EMAIL          = 'email';
    const FIELD_NAME_PASSWORD       = 'password';
    const FIELD_NAME_USERNAME       = 'username';
    const FIELD_NAME_TIMEZONE_ID    = 'timezone_id';


    public function setup() {

        $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

        $this->setupInheritance();

        parent::setup();

        $this->widgetSchema->setFormFormatterName('list');

        $this->setBionicFormatter();
    }

    protected function configureFieldFirstName(
        $fieldName = self::FIELD_NAME_FIRST_NAME,
        $default = null
    ) {

        $this->setWidget($fieldName, new sfWidgetFormInput());

        $this->setValidator(
            $fieldName,
            new sfValidatorString(
                array(
                    'required'      => false,
                    'trim'          => true,
                    'min_length'    => self::NAME_MIN_LENGTH,
                    'max_length'    => self::NAME_MAX_LENGTH,
                ),
                array(
                    'min_length' => 'That first name is too short. It must contain a minimum of %min_length% characters.'
                )
            )
        );

        $this->getWidgetSchema()->setHelp(
            self::FIELD_NAME_FIRST_NAME,
            "Length must be greater then " . (self::NAME_MIN_LENGTH - 1)
        );

        if (!is_null($default)) {
            $this->setDefault($fieldName, $default);
        }
    }

    protected function configureFieldLastName(
        $fieldName = self::FIELD_NAME_LAST_NAME,
        $default = null
    ) {

        $this->setWidget($fieldName, new sfWidgetFormInput());

        $this->setValidator(
            $fieldName,
            new sfValidatorString(
                array(
                    'required'      => false,
                    'trim'          => true,
                    'min_length'    => self::NAME_MIN_LENGTH,
                    'max_length'    => self::NAME_MAX_LENGTH,
                ),
                array(
                    'min_length' => 'That last name is too short. It must contain a minimum of %min_length% characters.'
                )
            )
        );

        $this->getWidgetSchema()->setHelp(
            $fieldName,
            "Length must be greater then " . (self::NAME_MIN_LENGTH - 1)
        );

        if (!is_null($default)) {
            $this->setDefault($fieldName, $default);
        }
    }

    protected function configureFieldUsername(
        $sfGuardUserId,
        $fieldName = self::FIELD_NAME_USERNAME,
        $default = null
    ) {

        $this->setWidget($fieldName, new sfWidgetFormInput());

        $this->setValidator(
            $fieldName,
            new sfValidatorString(
                array(
                    'required'      => true,
                    'trim'          => true,
                    'max_length'    => self::USERNAME_MAX_LENGTH,
                )
            )
        );

        $postValidatorNew = new sfValidatorCallback(
            array(
                'callback' => array('GlobalValidatorUser', 'checkIfUserWithThisUsernameExists'),
                'arguments' => array(
                    GlobalValidatorUser::OPTION_SF_GUARD_USER_ID    => $this->isNew() ? null : $sfGuardUserId,
                    GlobalValidatorUser::OPTION_USERNAME_FIELD_NAME => $fieldName,
                ),
            ),
            array(
                'invalid' => 'An account with this username already exists',
            )
        );

        $this->addPostValidator($postValidatorNew);

        if (!is_null($default)) {
            $this->setDefault($fieldName, $default);
        }
    }

    protected function configureFieldEmail(
        $sfGuardUserId,
        $fieldName = self::FIELD_NAME_EMAIL,
        $default = null
    ) {

        $this->setWidget($fieldName, new sfWidgetFormInputText());

        $this->setValidator(
            $fieldName,
            new sfValidatorAnd(
                array(
                    new sfValidatorEmail(array(
                        'required'  => true,
                        'trim'      => true
                    )),
                    new sfValidatorString(
                        array(
                            'required'      => true,
                            'max_length'    => self::EMAIL_MAX_LENGTH
                        ),
                        array(
                            'max_length' => 'Is too long ('. self::EMAIL_MAX_LENGTH . ' characters max).'
                        )
                    ),
                )
            )
        );

        $postValidatorNew = new sfValidatorCallback(
            array(
                'callback' => array('GlobalValidatorUser', 'checkIfUserWithThisEmailExists'),
                'arguments' => array(
                    GlobalValidatorUser::OPTION_SF_GUARD_USER_ID    => $this->isNew() ? null : $sfGuardUserId,
                    GlobalValidatorUser::OPTION_EMAIL_FIELD_NAME    => $fieldName,
                ),
            ),
            array(
                'invalid' => 'An account with that email address already exists',
            )
        );

        $this->addPostValidator($postValidatorNew);

        if (!is_null($default)) {
            $this->setDefault($fieldName, $default);
        }
    }

    protected function configureFieldPassword(
        $fieldName = self::FIELD_NAME_PASSWORD
    ) {

        $this->setWidget(
            $fieldName,
            new sfWidgetFormInputPassword(
                array(),
                array(
                    'maxlength' => self::PASSWORD_MAX_LENGTH
                )
            )
        );

        $this->setValidator(
            $fieldName,
            new sfValidatorString(
                array(
                    'required'      => true,
                    'trim'          => true,
                    'min_length'    => self::PASSWORD_MIN_LENGTH,
                    'max_length'    => self::PASSWORD_MAX_LENGTH
                ),
                array(
                    'min_length' => 'That password is too short. It must contain a minimum of %min_length% characters.'
                )
            )
        );

        $this->getWidgetSchema()->setHelp(
            $fieldName,
            "Length must be greater then " . (self::PASSWORD_MIN_LENGTH - 1)
        );
    }

    protected function configureFieldTimezoneId(
        $fieldName = self::FIELD_NAME_TIMEZONE_ID,
        $default = null
    ) {

        $this->setWidget(
            $fieldName, 
            new sfWidgetFormDoctrineChoice(
                array(
                    'multiple'  => false, 
                    'model'     => $this->getRelatedModelName('Timezone'),
                    'add_empty' => false,
                )
            )
        );
        $this->setValidator(
            $fieldName, 
            new sfValidatorDoctrineChoice(
                array(
                    'multiple'  => false, 
                    'model'     => $this->getRelatedModelName('Timezone'),
                )
            )
        );

        $default = $this->isNew() ? TimezoneTable::getDefaultId() : null;
        if (!empty($default)) {
            $this->setDefault($fieldName, $default);
        }
    }
}
