<?php

/**
 * Description of GraphCallStatusFilterForm
 *
 * @author fairdev
 */
class GraphCallStatusFilterForm extends fdBaseFilter {

    const PER_DAY       = 'per_day';
    const PER_WEEK_DAY  = 'per_week_day';
    const PER_HOUR      = 'per_hour';

    const NAME_PER_DAY      = 'Per Month Day';
    const NAME_PER_WEEK_DAY = 'Per Weekday';
    const NAME_PER_HOUR     = 'Per Hour';

    const FIELD_NAME_PER_TYPE = 'per_type';


    public function setup() {

        parent::setup();
    }

    public function configure() {

        parent::configure();

        $this->configureFieldPerType();

        $this->widgetSchema->setNameFormat('graph_call_status_filter[%s]');
    }

    public function getFromTo() {
        return array(
            'from'  => $this->getOption('from', null),
            'to'    => $this->getOption('to', null),
        );
    }

    protected function configureFieldPerType() {

        $choicesInfo = $this->configureFieldPerTypeChoicesInfo();
        $default = $choicesInfo['default'];
        $choices = $choicesInfo['choices'];

        $fieldName = self::FIELD_NAME_PER_TYPE;

        $this->configureFieldChoice(
            $fieldName, 
            $choices, 
            $default, 
            false
        );

        $this->addFilterField(array($fieldName => array($fieldName)));
    }

    protected function configureFieldPerTypeChoicesInfo() {

        $default = self::PER_DAY;
        $choices = array(
            self::PER_DAY       => self::NAME_PER_DAY,
            self::PER_WEEK_DAY  => self::NAME_PER_WEEK_DAY,
            self::PER_HOUR      => self::NAME_PER_HOUR,
        );

        return array(
            'choices'   => $choices,
            'default'   => $default,
        );
    }

    public function getFilteredValues($values, $isCleaned = false) {

        $result = parent::getFilteredValues($values, $isCleaned);

        $result = array_merge(
            array(
                'from'  => $this->getOption('from', null),
                'to'    => $this->getOption('to', null),
            ),
            $result
        );

        return $result;
    }

}