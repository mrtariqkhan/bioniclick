<?php

class ApplyCustomerFormNew extends ApplyFormNew {

    public function setup() {

        parent::setup();

        $this->widgetSchema->setNameFormat('sfApplyApplyCustomer[%s]');
    }
}
