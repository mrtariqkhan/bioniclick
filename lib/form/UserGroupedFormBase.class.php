<?php

/**
 * UserGroupedFormBase form.
 */
abstract class UserGroupedFormBase extends UserFormBase {

    const OPTION_SF_GUARD_USER_ADMIN = 'sf_guard_user_admin';

    const FIELD_NAME_GROUPS_LIST = 'groups_list';
    
    protected $sfGuardUserAdmin = null;


    public function configure() {

        $sfGuardUserAdmin = $this->getOption(self::OPTION_SF_GUARD_USER_ADMIN, null);
        if (empty($sfGuardUserAdmin)) {
            throw new Exception('Option ' . self::OPTION_SF_GUARD_USER_ADMIN . ' is required.');
        }

        $this->setSfGuardUserAdmin($sfGuardUserAdmin);

        parent::configure();
    }

    protected function configureFieldGroupList(
        $sfGuardUser,
        $fieldName = self::FIELD_NAME_GROUPS_LIST
    ) {

        $query = sfGuardGroupTable::createQuerySubGroupsByParentUser($this->getSfGuardUserAdmin());
        $model = 'sfGuardGroup';

        $this->setWidget(
            $fieldName, 
            new sfWidgetFormDoctrineChoice(
                array(
                    'multiple'  => true, 
                    'model'     => $model,
                    'query'     => $query,
                )
            )
        );
        $this->setValidator(
            $fieldName, 
            new sfValidatorDoctrineChoice(
                array(
                    'multiple'  => true, 
                    'model'     => $model,
                    'query'     => $query,
                    'required'  => false,
                )
            )
        );

        if (!empty($sfGuardUser)) {
            //NOTE:: it needs to refresh Object to refresh references (Groups), otherwise: 
            // 1). wrong Groups will be selected by default,
            // 2). SQL exception while save-method
            $sfGuardUser->refresh(true);

            $groups = $sfGuardUser->getGroups();
            $groupIds = $groups->getPrimaryKeys();
            $this->setDefault($fieldName, $groupIds);
        }
    }

    public function saveGroupsList($sfGuardUser, $con = null) {

        if (!$this->isValid()) {
            throw $this->getErrorSchema();
        }

        if (!isset($this->widgetSchema[self::FIELD_NAME_GROUPS_LIST])) {
            // somebody has unset this widget
            return;
        }

        $existing   = $this->getGroupIdsExisting();
        $values     = $this->getGroupIdsSelected();
        if (!is_array($values)) {
            $values = array();
        }

        $unlink = array_diff($existing, $values);
        if (count($unlink)) {
            $sfGuardUser->unlink('Groups', array_values($unlink));
        }

        $link = array_diff($values, $existing);
        if (count($link)) {
            $sfGuardUser->link('Groups', array_values($link));
        }
    }

    abstract protected function getGroupIdsSelected();
    abstract protected function getGroupIdsExisting();

    protected function getSfGuardUserAdmin() {
        return $this->sfGuardUserAdmin;
    }
    protected function setSfGuardUserAdmin($sfGuardUserAdmin) {
        $this->sfGuardUserAdmin = $sfGuardUserAdmin;
    }
}
