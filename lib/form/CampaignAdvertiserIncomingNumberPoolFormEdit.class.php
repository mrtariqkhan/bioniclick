<?php

/**
 * CampaignAdvertiserIncomingNumberPool edit-form.
 *
 * @package    bionic
 * @subpackage form
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class CampaignAdvertiserIncomingNumberPoolFormEdit extends CampaignForm {

    const OPTION_USER                           = 'user';
    const OPTION_PHONE_NUMBER_TYPE_CODE         = 'phoneNumberTypeCode';
    const OPTION_PHONE_NUMBER_COUNTRY_ID        = 'phoneNumberCountry';
    const OPTION_PHONE_NUMBER_LOCAL_AREA_CODE_ID= 'twilioLocalAreaCode';
    const OPTION_PHONE_NUMBERS_COUNT            = 'numbersCount';
    const OPTION_PHONE_NUMBER_NOTES             = 'phoneNumberNotes';

    const FIELD_NAME_PHONE_NUMBER_LOCAL_AREA_CODE   = 'twilio_local_area_code_id';
    const FIELD_NAME_PHONE_NUMBER_TYPE_CODE         = 'phone_number_type';
    const FIELD_NAME_PHONE_NUMBER_COUNTRY_ID        = 'phone_number_country_id';
    const FIELD_NAME_PHONE_NUMBERS_COUNT            = 'numbers_count';
    const FIELD_NAME_CAMPAIGN_ID                    = 'id';
    const FIELD_NAME_PHONE_NUMBER_NOTES             = 'desired_part_of_number';


    public function configure() {

        $user = $this->getOption(self::OPTION_USER);
        if (empty($user)) {
            throw new Exception('user option is required for numbers assignment to campaign');
        }

        $this->widgetSchema->setNameFormat('campaign_incoming_number_pool[%s]');

        $this->useFields(array());

        $this->setBionicFormatter();


        $this->configureFieldCountOfNumbers();
        $this->configureFieldPhoneNumberNotes();
        $this->configureFieldTwilioLocalAreaCode(self::FIELD_NAME_PHONE_NUMBERS_COUNT);

        $this->setFieldsOrder();
    }

    protected function configureFieldTwilioLocalAreaCode($numbersCountFieldName) {

        $advertiserId = $this->getObject()->getAdvertiserId();

        $this->configureFieldPhoneNumberType();
        $this->configureFieldPhoneNumberCountryId();


        $fieldName = self::FIELD_NAME_PHONE_NUMBER_LOCAL_AREA_CODE;

        $url = 'twilioIncomingPhoneNumber/ajaxFillAreaCodeAutocompleteForCampaign';
        $urlNumberCount = "advertiserFillUserPhoneNumbersCountForCampaign/$advertiserId";

        $formTagName = $this->getName();
        $phoneNumberFieldName           = self::FIELD_NAME_PHONE_NUMBER_TYPE_CODE;
        $phoneNumberCountryIdFieldName  = self::FIELD_NAME_PHONE_NUMBER_COUNTRY_ID;

        $defaultLocalAreaCode = $this->getOption(self::OPTION_PHONE_NUMBER_LOCAL_AREA_CODE_ID, null);

        $autocompleterWidget = new BionicWidgetFormDoctrineJQueryAutocompleter(array(
            'default'                       => $defaultLocalAreaCode,

            'model'                         => 'TwilioLocalAreaCode',
            'url'                           => $url,
            'name_form'                     => $formTagName,
            'name_phone_number'             => $phoneNumberFieldName,
            'name_phone_number_country_id'  => $phoneNumberCountryIdFieldName,

            'name_number_count'             => $numbersCountFieldName,
            'advertiser_id'                 => $advertiserId,
            'url_number_count'              => $urlNumberCount,
            'text_number_count_prefix'      => '(',
            'text_number_count_postfix'     => ' available number(s) with selected code)',
        ));

        $this->setWidget($fieldName, $autocompleterWidget);
        $this->getWidgetSchema()
            ->setLabel($fieldName, 'Area code')
            ->setHelp($fieldName, "Double-click, begin typing in field")
        ;

        $this
            ->setValidator(
                $fieldName,
                new sfValidatorDoctrineChoice(
                    array(
                        'model' => 'TwilioLocalAreaCode',
                        'query' => TwilioLocalAreaCodeTable::createQueryExistentInAdvertiserPoolByPhoneNumberType($advertiserId)
                    )
                )
            )
        ;
    }

    protected function configureFieldPhoneNumberType() {

        $fieldName = self::FIELD_NAME_PHONE_NUMBER_TYPE_CODE;

        $advertiserId = $this->getObject()->getAdvertiserId();
        $default = null;
        if ($choices = TwilioLocalAreaCodeTable::findPhoneNumberTypesCodedByAdvertiserId($advertiserId)) {
            $default = $this->getOption(self::OPTION_PHONE_NUMBER_TYPE_CODE, current($choices));
        }

        $this->configureFieldChoiceWithAll($fieldName, $choices, $default);
    }

    protected function configureFieldPhoneNumberCountryId() {

        $fieldName = self::FIELD_NAME_PHONE_NUMBER_COUNTRY_ID;

        $advertiserId = $this->getObject()->getAdvertiserId();
        $default = null;
        if ($choices = TwilioLocalAreaCodeTable::findPhoneNumberCountriesByAdvertiserId($advertiserId)) {
            $default = $this->getOption(self::OPTION_PHONE_NUMBER_COUNTRY_ID, current($choices));
        }

        $this->configureFieldChoiceWithAll($fieldName, $choices, $default);
    }

    protected function configureFieldCountOfNumbers() {

        $fieldName = self::FIELD_NAME_PHONE_NUMBERS_COUNT;

        $this->setWidget(
            $fieldName,
            new sfWidgetFormInputText(array('default' => 1))
        );

        $this->setValidator(
            $fieldName,
            new sfValidatorInteger(array('min' => 1))
        );

        $this->getWidgetSchema()->setHelp(self::FIELD_NAME_PHONE_NUMBERS_COUNT, "0 available number(s) with selected code");
        if ($default = $this->getOption(self::OPTION_PHONE_NUMBERS_COUNT, null)) {
            $this->getWidget($fieldName)->setDefault($default);
        }
    }

    protected function configureFieldPhoneNumberNotes() {

        $fieldName = self::FIELD_NAME_PHONE_NUMBER_NOTES;

        $this->setWidget(
            $fieldName,
            new sfWidgetFormInputText(array('default' => ''))
        );

        $this->setValidator(
            $fieldName,
            new PhoneNumberValidator(array(
                'required'      => false,
                'min_length'    => 0,
            ))
        );

        if ($default = $this->getOption(self::OPTION_PHONE_NUMBER_NOTES)) {
            $this->getWidget($fieldName)->setDefault($default);
        }
    }

    protected function setFieldsOrder() {

        $this->widgetSchema->moveField(
            self::FIELD_NAME_PHONE_NUMBER_TYPE_CODE,
            sfWidgetFormSchema::FIRST
        );
        $this->widgetSchema->moveField(
            self::FIELD_NAME_PHONE_NUMBER_COUNTRY_ID,
            sfWidgetFormSchema::AFTER,
            self::FIELD_NAME_PHONE_NUMBER_TYPE_CODE
        );
        $this->widgetSchema->moveField(
            self::FIELD_NAME_PHONE_NUMBER_LOCAL_AREA_CODE,
            sfWidgetFormSchema::AFTER,
            self::FIELD_NAME_PHONE_NUMBER_COUNTRY_ID
        );
        $this->widgetSchema->moveField(
            self::FIELD_NAME_PHONE_NUMBERS_COUNT,
            sfWidgetFormSchema::AFTER,
            self::FIELD_NAME_PHONE_NUMBER_LOCAL_AREA_CODE
        );
        $this->widgetSchema->moveField(
            self::FIELD_NAME_PHONE_NUMBER_NOTES,
            sfWidgetFormSchema::AFTER,
            self::FIELD_NAME_PHONE_NUMBERS_COUNT
        );
    }

    protected function doSave($con = null) {

        $user = $this->getOption(self::OPTION_USER);
        $advertiser = $this->getObject()->getAdvertiser();

        $campaignId         = $this->getValue(self::FIELD_NAME_CAMPAIGN_ID);
        $numbersCount       = $this->getValue(self::FIELD_NAME_PHONE_NUMBERS_COUNT);
        $localAreaCodeId    = $this->getValue(self::FIELD_NAME_PHONE_NUMBER_LOCAL_AREA_CODE);
        $desiredPhoneNumber = $this->getValue(self::FIELD_NAME_PHONE_NUMBER_NOTES);

        AdvertiserIncomingNumberPoolTable::moveFreeNumbersToCampaignFromAdvertiserDesiredOrNot(
            $advertiser,
            $localAreaCodeId,
            $desiredPhoneNumber,
            $numbersCount, 
            $campaignId,
            $user
        );
    }
}
