<?php

class ApplyResetRequestForm extends sfApplyResetRequestForm {

    public function configure() {

        parent::configure();

        $queryExistingUsers = sfGuardUserTable::createQueryExisting();

        $this->setValidator('username_or_email',
            new sfValidatorOr(array(
                new sfValidatorAnd(array(
                    new sfValidatorString(array(
                        'required'  => true,
                        'trim'      => true,
                        'min_length'=> ApplyFormNew::NAME_MIN_LENGTH,
                        'max_length'=> ApplyFormNew::NAME_MAX_LENGTH,
                    )),
                    new sfValidatorDoctrineChoice(
                        array(
                            'model' => 'sfGuardUser',
                            'column'=> 'username',
                            'query' => $queryExistingUsers,
                        ),
                        array(
                            'invalid' => 'There is no such user.'
                        )
                    )
                )),
                new sfValidatorEmail(array(
                    'required' => true,
                ))
            ))
        );
    }
}

