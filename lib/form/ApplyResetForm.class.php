<?php

class ApplyResetForm extends sfApplyResetForm {

    public function configure() {

        parent::configure();

        $this->setWidget('password',
            new sfWidgetFormInputPassword(
                array(),
                array(
                    'maxlength'=> ApplyFormNew::PASSWORD_MAX_LENGTH,
                )
            )
        );

        $this->setWidget('password2',
            new sfWidgetFormInputPassword(
                array(),
                array(
                    'maxlength'=> ApplyFormNew::PASSWORD_MAX_LENGTH,
                )
            )
        );

        $this->setValidator('password',
            new sfValidatorString(
                array(
                    'required'  => true,
                    'trim'      => true,
                    'min_length'=> ApplyFormNew::PASSWORD_MIN_LENGTH,
                    'max_length'=> ApplyFormNew::PASSWORD_MAX_LENGTH,
                ),
                array(
                    'min_length'=> 'That password is too short. It must contain a minimum of %min_length% characters.',
                    'max_length'=> 'That password is too long. It must contain a maximum of %max_length% characters.',
                )
            )
        );

        $this->setValidator('password2', 
            new sfValidatorString(
                array(
                    'required'  => true,
                    'trim'      => true,
                    'min_length'=> ApplyFormNew::PASSWORD_MIN_LENGTH,
                    'max_length'=> ApplyFormNew::PASSWORD_MAX_LENGTH,
                ), array(
                    'min_length'=> 'That password is too short. It must contain a minimum of %min_length% characters.',
                    'max_length'=> 'That password is too long. It must contain a maximum of %max_length% characters.',
                )
            )
        );
    }
}

