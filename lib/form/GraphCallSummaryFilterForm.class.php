<?php

/**
 * Description of GraphCallSummaryFilterForm
 *
 * @author fairdev
 */
class GraphCallSummaryFilterForm extends fdBaseFilter {

    const PER_DAY   = 'per_day';
    const PER_WEEK  = 'per_week';
    const PER_MONTH = 'per_month';
    const PER_YEAR  = 'per_year';

    const NAME_PER_DAY      = 'Per Day (last 7 days at max)';
    const NAME_PER_WEEK     = 'Per Week (last 8 weeks at max)';
    const NAME_PER_MONTH    = 'Per Month (last 12 months at max)';
    const NAME_PER_YEAR     = 'Per Year (last 10 years at max)';

    const FIELD_NAME_PER_TYPE = 'per_type';


    public function setup() {

        parent::setup();
    }

    public function configure() {

        parent::configure();

        $this->configureFieldPerType();

        $this->widgetSchema->setNameFormat('graph_call_summary_filter[%s]');
    }

    public function getFromTo() {
        return array(
            'from'  => $this->getOption('from', null),
            'to'    => $this->getOption('to', null),
        );
    }

    protected function configureFieldPerType() {

        $choicesInfo = $this->configureFieldPerTypeChoicesInfo();
        $default = $choicesInfo['default'];
        $choices = $choicesInfo['choices'];

        $fieldName = self::FIELD_NAME_PER_TYPE;

        $this->configureFieldChoice(
            $fieldName, 
            $choices, 
            $default, 
            false
        );

        $this->addFilterField(array($fieldName => array($fieldName)));
    }

    protected function configureFieldPerTypeChoicesInfo() {

//        $fromTimestamp  = $this->getOption('from', null);
//        $toTimestamp    = $this->getOption('to', null);
//
//        if (empty($fromTimestamp) && empty($toTimestamp)) {
//            $defaultChoice = 'per_day';
//            $choices = array(
//                'per_day'   => 'Per Day',
//                'per_week'  => 'Per Week',
//                'per_month' => 'Per Month',
//                'per_year'  => 'Per Year',
//            );
//        } else {
//            $toTimestamp = empty($toTimestamp) ? TimeConverter::getUnixTimestamp() : $toTimestamp;
//
//            $seconds = $toTimestamp - $fromTimestamp;
//            $periods = TimeConverter::secondsToPeriods($seconds);
//
//            $defaultChoice = 'per_day';
//            $choices = array(
//                'per_day'   => 'Per Day',
//                'per_week'  => 'Per Week',
//                'per_month' => 'Per Month',
//                'per_year'  => 'Per Year',
//            );
//        }

        $default = self::PER_DAY;
        $choices = array(
            self::PER_DAY   => self::NAME_PER_DAY,
            self::PER_WEEK  => self::NAME_PER_WEEK,
            self::PER_MONTH => self::NAME_PER_MONTH,
            self::PER_YEAR  => self::NAME_PER_YEAR,
        );

        return array(
            'choices'   => $choices,
            'default'   => $default,
        );
    }

}