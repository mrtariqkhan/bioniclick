<?php

/**
 * PhoneNumberValidator validates PhoneNumbers.
 */
class PhoneNumberValidator extends sfValidatorRegex {

    const REGEX_PHONE_NUMBER = '/^\+?\d+$/';

    const DEFAULT_OPTION_MIN_LENGTH = 5;
    const DEFAULT_OPTION_MAX_LENGTH = 22;

    /**
    * @see sfValidatorRegex
    */
    protected function configure($options = array(), $messages = array()) {

        parent::configure($options, $messages);

        $this->setOptionIfNotSet('min_length', self::DEFAULT_OPTION_MIN_LENGTH);
        $this->setOptionIfNotSet('max_length', self::DEFAULT_OPTION_MAX_LENGTH);

        $this->setOption('pattern', self::REGEX_PHONE_NUMBER);
    }

    protected function setOptionIfNotSet($name, $default) {

        if (!$this->hasOption($name)) {
            $this->setOption($name, $default);
        }

        return $this;
    }
}
