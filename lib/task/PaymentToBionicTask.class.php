<?php
require_once dirname(__FILE__).'/../../config/ProjectConfiguration.class.php';

class PaymentToBionicTask extends BionicTask {

    const PARAMETER_NAME_PERIOD = 'period';
    const PARAMETER_VALUE_PERIOD_LAST_MONTH = 'last';
    const PARAMETER_VALUE_PERIOD_THIS_MONTH = 'this';

    const COMMENT_SUM_BEFORE= 'BEFORE: ';
    const COMMENT_SUM_SHOULD= 'SHOULD: ';
    const COMMENT_SUM_DURING= 'DURING: ';
    const COMMENT_SUM_AFTER = 'AFTER: ';

    const COMMENT_COMPANY_TITLE             = '| ';
    const COMMENT_COMPANY_ABOUT             = '|        ';
    const COMMENT_PROCESS_PAY               = '|     $  ';
    const COMMENT_PROCESS_PAY_ONE_CARD_FAIL = '|    .$  ';
    const COMMENT_PROCESS_PAY_FAIL          = '|    !$  ';
    const COMMENT_PROCESS_DEBTOR            = '|  ****  ';
    const COMMENT_PROCESS_DEBTOR_ERROR      = '|  !***  ';
    const COMMENT_PROCESS_DEBTOR_SUB        = '|   ***  ';
    const COMMENT_PROCESS_DEBTOR_NOT        = '|  *OK*  ';

    public function __construct(sfEventDispatcher $dispatcher, sfFormatter $formatter) {

        $this->setName('payment_to_bionic');
        $this->setNameSmall('pay');
        $this->setNameSection('payment');

        parent::__construct($dispatcher, $formatter);
    }

    protected function configure() {

        parent::configure();

        $optionDescriptionPeriod = "Period '" 
            . self::PARAMETER_VALUE_PERIOD_LAST_MONTH. "'/'" 
            . self::PARAMETER_VALUE_PERIOD_THIS_MONTH 
            . "' (last or this month). OPTIONAL"
        ;

        $this->addOptions(array(
            new sfCommandOption(
                self::PARAMETER_NAME_PERIOD , 
                null, 
                sfCommandOption::PARAMETER_OPTIONAL, 
                $optionDescriptionPeriod, 
                self::PARAMETER_VALUE_PERIOD_LAST_MONTH
            ),
        ));

        $this->briefDescription = "Executes payments from direct Bionic's children to Bionic using Authorize.Net.";
        $this->detailedDescription = <<<EOF
The [pay|INFO] executes payments from direct Bionic's children to Bionic using Authorize.Net.

Example:
    [php symfony bionic:pay|INFO]                - pay to Bionic for last month
    [php symfony bionic:pay --period=last|INFO]  - pay to Bionic for last month
    [php symfony bionic:pay --period=this|INFO]  - pay to Bionic for this month
EOF
;
    }

    protected function execute($arguments = array(), $options = array()) {

        parent::execute($arguments, $options);

        $types = array(
            'company' => array(
                'tableName' => 'Company',
                'plural'    => 'companies',
            ),
            'advertiser' => array(
                'tableName' => 'Advertiser',
                'plural'    => 'advertisers',
            ),
        );

        if ($options[self::PARAMETER_NAME_PERIOD] == 'last') {
            $range = TimeConverter::getLastMonthUnixTimestampRange(TimeConverter::TIMEZONE_UTC);
        } else {
            $range = TimeConverter::getThisMonthUnixTimestampRange(TimeConverter::TIMEZONE_UTC);
        }

        $timeFrom   = TimeConverter::getDateTime($range['from'], TimeConverter::TIMEZONE_UTC);
        $timeTo     = TimeConverter::getDateTime($range['to'], TimeConverter::TIMEZONE_UTC);

        $this->logSectionNamed("Time From: $timeFrom");
        $this->logSectionNamed("Time To:   $timeTo");
        $this->log('');

        foreach ($types as $singular => $typeInfo) {
            $tableName  = $typeInfo['tableName'];
            $plural     = $typeInfo['plural'];

            $this->runMoneyTransaction($tableName, $singular, $plural, $range);
        }
    }

    protected function runMoneyTransaction($tableName, $singular, $plural, $range) {

        $this->logSectionNamed('----------------------------------------------------------------');
        $this->logSectionNamed("Payments from non-debtor $plural to Bionic.");
        $this->log('');

        $objectsNotDebtors = $this->getObjectsToProcessAndLogStatistic($tableName, $plural, self::COMMENT_SUM_BEFORE);

        $amountSuccess = 0;
        foreach ($objectsNotDebtors as $object) {
            $sucess = false;

            $this->logSectionNamed(self::COMMENT_COMPANY_TITLE . "$singular (id={$object->getId()}, name='{$object->getName()}')");

            $amount = self::getAmount($object, $range);
            $this->logSectionNamed(self::COMMENT_COMPANY_ABOUT . "It has to pay $$amount.");

            if ($amount <= 0) {
                $sucess = true;
            } else {
                $customerPaymentProfiles = $object->getCustomerPaymentProfiles();
                $this->logSectionNamed(self::COMMENT_COMPANY_ABOUT . "It has {$customerPaymentProfiles->count()} customer payment profiles");

                foreach ($customerPaymentProfiles as $i => $customerPaymentProfile) {
                    $objectInfoCard = "card (priority={$customerPaymentProfile->getPriority()}, code={$customerPaymentProfile->getCardCode()}, id = {$customerPaymentProfile->getId()})";
                    try {
                        //$amount = 0.01    // small value for testing
                        $customerPaymentProfile->moneyTransaction($amount);
                        $amountSuccess++;
                        $sucess = true;
                        break;

                    } catch (Exception $e) {
                        $this->logSectionNamed(self::COMMENT_PROCESS_PAY_ONE_CARD_FAIL . "payment hasn't been executed from $objectInfoCard. Fail # " . ($i + 1) . ".");
                    }
                }

                if ($sucess) {
                    $this->logSectionNamed(self::COMMENT_PROCESS_PAY . "payment has been executed from $objectInfoCard.");
                } else {
                    $this->logSectionNamed(self::COMMENT_PROCESS_PAY_FAIL . "payment hasn't been executed.");
                }
            }

            if ($sucess) {
                $this->logSectionNamed(self::COMMENT_PROCESS_DEBTOR_NOT . "this $singular is not debtor (and its subcompanies).");
            } else {
                try {
                    $debtorsTyped = $object->declareIsDebtor(true);
                    $this->logSectionNamed(self::COMMENT_PROCESS_DEBTOR . "this $singular is debtor and its subcompanies:");
                    foreach ($debtorsTyped as $type => $debtors) {
                        foreach ($debtors as $debtor) {
                            $this->logSectionNamed(self::COMMENT_PROCESS_DEBTOR_SUB . "$type (id={$debtor->getId()}, name='{$debtor->getName()}') is debtor");
                        }
                    }
                } catch (Exception $e) {
                    $this->logSectionNamed(self::COMMENT_PROCESS_DEBTOR_ERROR . "Fail while declaring that this $singular and its subcompanies are debtors");
                }
            }
            $this->log('');
        }

        $this->log('');
        $this->logSectionNamed(self::COMMENT_SUM_DURING . "{$amountSuccess} $plural have been executed successfully.");
        $this->log('');


        $this->getObjectsToProcessAndLogStatistic($tableName, $plural, self::COMMENT_SUM_AFTER);

        $this->log('');
        $this->log('');
    }

    protected function getObjectsToProcessAndLogStatistic($tableName, $plural, $section) {

        $table = Doctrine::getTable($tableName);

        $objectsAll         = $table->findBionicDirectChildByCustomerProfile(null);
        $objectsCorrect     = $table->findBionicDirectChildByCustomerProfile(false);
        $objectsToUpdate    = $table->findBionicDirectChildByCustomerProfile(true);
        $objectsNotDebtors  = $table->findBionicDirectChildNotDebtors();

        $this->logSectionNamed($section . "{$objectsAll->count()} existent $plural which have to pay to Bionic.");
        $this->logSectionNamed($section . "{$objectsCorrect->count()} $plural among them have Authorize.Net Customer Profile.");
        $this->logSectionNamed($section . "{$objectsToUpdate->count()} $plural among them have no Authorize.Net Customer Profile.");
        $this->logSectionNamed($section . "{$objectsNotDebtors->count()} $plural among them are not debtors.");
        $this->log('');

        return $objectsNotDebtors;
    }

    /**
     *
     * @param type $object Company or Advertiser
     * @param array $range 
     */
    protected static function getAmount($object, $range) {

        $invoiceGenerator = new InvoicesGeneratorSummary(
            $object, 
            InvoicesGeneratorBase::INVOICE_TYPE_HAVE_TO_PAY, 
            InvoicesGeneratorBase::INVOICE_TYPE_DETAILS_SUMMARY, 
            $range
        );

        $data = $invoiceGenerator->generateData();

        $total  = array_key_exists(InvoicesGenerator::LEVEL_TOTAL, $data) ? $data[InvoicesGenerator::LEVEL_TOTAL] : array();
        $amount = array_key_exists(InvoicesGenerator::INFO_PRICE_TOTAL, $total) ? $total[InvoicesGenerator::INFO_PRICE_TOTAL] : 0;

        return $amount;
    }
}
