<?php
require_once dirname(__FILE__).'/../../config/ProjectConfiguration.class.php';

class IncomingNumbersSyncTask extends BionicTask {

    const COMMENT_SUM_VOIP  = 'VoIP: ';
    const COMMENT_SUM_BEFORE= 'BEFORE: ';
    const COMMENT_SUM_SHOULD= 'SHOULD: ';
    const COMMENT_SUM_DURING= 'DURING: ';
    const COMMENT_SUM_AFTER = 'AFTER: ';
    const COMMENT_EXCEPTION = '';
    const COMMENT_PROCESS               = '... ';
    const COMMENT_PROCESS_NEW           = ' + ';
    const COMMENT_PROCESS_RESTORE       = ' ~^';
    const COMMENT_PROCESS_UPDATE        = ' ~ ';
    const COMMENT_PROCESS_DELETE        = ' - ';
    const COMMENT_PROCESS_DELETE_RESTORE= ' -*';
    const COMMENT_PROCESS_DELETE_ORDER  = '  ^';
    const COMMENT_DELETE_FAIL = 'It was FAIL of:';
    const COMMENT_PROCESS_DELETE_ERROR          = '!-  ';
    const COMMENT_PROCESS_DELETE_RESTORE_ERROR  = '!-* ';
    const COMMENT_PROCESS_DELETE_ORDER_ERROR    = '!  ^';

    const COMMENT_DEFAULT_NUMBER_ADD_SELF       = ' [default: my] ';
    const COMMENT_DEFAULT_NUMBER_ADD_SELF_ERROR = '![default: my] ';
    const COMMENT_DEFAULT_NUMBER_ADD            = ' [default: parent ] ';
    const COMMENT_DEFAULT_NUMBER_ADD_ERROR      = '![default: parent ] ';
    const COMMENT_DEFAULT_NUMBER_SEVERAL        = ' [default: several] ';
    const COMMENT_DEFAULT_NUMBER_SEVERAL_ERROR  = '![default: several] ';

    const COMMENT_DELETED_CAMPAIGN_NUMBER           = ' [del cam] ';
    const COMMENT_DELETED_CAMPAIGN_NUMBER_ERROR     = '![del cam] ';
    const COMMENT_DELETED_ADVERTISER_NUMBER         = ' [del adv] ';
    const COMMENT_DELETED_ADVERTISER_NUMBER_ERROR   = '![del adv] ';

    const COMMENT_TO_ADD_BIONIC         = ' +[bionic num] ';
    const COMMENT_TO_ADD_BIONIC_ERROR   = '!+[bionic num] ';

    public function __construct(sfEventDispatcher $dispatcher, sfFormatter $formatter) {

        $this->setName('inc_num_sync');
        $this->setNameSmall('sync');
        $this->setNameSection('sync');

        parent::__construct($dispatcher, $formatter);
    }

    protected function configure() {

        parent::configure();

        $this->addOptions(array(
            new sfCommandOption('direction' , null, sfCommandOption::PARAMETER_OPTIONAL, 'Direction of synchronization (from or to). OPTIONAL', 'to'),
            new sfCommandOption('delete'    , null, sfCommandOption::PARAMETER_OPTIONAL, "Mark or not numbers which don't exist at VoIP server as deleted (true or false). OPTIONAL", 'true'),
            new sfCommandOption('belong'    , null, sfCommandOption::PARAMETER_OPTIONAL, "Mark (if TRUE) or not (if FALSE) numbers which don't belong to need projects (true or false). OPTIONAL", 'true'),
        ));

        $this->briefDescription = 'Synchronizes incoming numbers';
        $this->detailedDescription = <<<EOF
The [inc_num_sync|INFO] 
    1. update incoming phone numbers from Bionic or to Bionic
    2. update URL of incoming numbers only on Bionic

Example:
    Update all numbers to Bionic from VoIP server with deletion non-existent numbers (application, environment, database connection - default):
        [php symfony bionic:inc_num_sync --direction="to" --delete="true" --belong="true"|INFO]
EOF
. "\n"
. "\n" . self::COMMENT_PROCESS_NEW                  . ' new number'
. "\n" . self::COMMENT_PROCESS_UPDATE               . ' update number'
. "\n" . self::COMMENT_PROCESS_RESTORE              . ' updated number has been restore because it is not stranger now'
. "\n" . self::COMMENT_PROCESS_DELETE               . ' delete number'
. "\n" . self::COMMENT_PROCESS_DELETE_ERROR         . ' FAIL of delete number'
. "\n" . self::COMMENT_PROCESS_DELETE_RESTORE       . ' delete number, it needs to create order'
. "\n" . self::COMMENT_PROCESS_DELETE_RESTORE_ERROR . ' FAIL of delete number, it needs to create order'
. "\n" . self::COMMENT_PROCESS_DELETE_ORDER         . ' create order'
. "\n" . self::COMMENT_PROCESS_DELETE_ORDER_ERROR   . ' FAIL of create order'

. "\n" . self::COMMENT_DEFAULT_NUMBER_ADD           . " campaign has no default phone number. It will be added from advertiser's or bionic's pool"
. "\n" . self::COMMENT_DEFAULT_NUMBER_ADD_ERROR     . " FAIL to add default numbers from advertiser's or bionic's pool"
. "\n" . self::COMMENT_DEFAULT_NUMBER_ADD_SELF      . " campaign has no default phone number. But it has numbers. Default one will be added from campaign's pool"
. "\n" . self::COMMENT_DEFAULT_NUMBER_ADD_SELF_ERROR. " FAIL o add default numbers from campaign's pool"
. "\n" . self::COMMENT_DEFAULT_NUMBER_SEVERAL       . ' campaign has several default numbers'
. "\n" . self::COMMENT_DEFAULT_NUMBER_SEVERAL_ERROR . ' FAIL to make unnecessary default numbers simple'

. "\n" . self::COMMENT_DELETED_CAMPAIGN_NUMBER          . ' campaign is deleted but has default numbers'
. "\n" . self::COMMENT_DELETED_CAMPAIGN_NUMBER_ERROR    . ' FAIL while removing campaign which is deleted but has default numbers'
. "\n" . self::COMMENT_DELETED_ADVERTISER_NUMBER        . ' advertiser is deleted but has numbers'
. "\n" . self::COMMENT_DELETED_ADVERTISER_NUMBER_ERROR  . ' FAIL while removing advertiser which is deleted but has numbers'

. "\n" . self::COMMENT_TO_ADD_BIONIC                    . ' add bionic number (because DB is incorrect: twilio table has this number)'
. "\n" . self::COMMENT_TO_ADD_BIONIC_ERROR              . ' FAIL while add bionic number (because DB is incorrect: twilio table has this number)'
;
    }

    protected function execute($arguments = array(), $options = array()) {

        parent::execute($arguments, $options);

        $this->logSectionNamed('==========================================================');
        $this->logSectionNamed(TimeConverter::getDateTimeFormattedByString('now', TimeConverter::TIMEZONE_UTC, TimeConverter::FORMAT_ALL));


        if ($options['direction'] == 'from') {
            $this->executeFromDirection();
        } elseif ($options['direction'] == 'to') {
            $this->executeToDirection(
                $this->getBooleanOption($options, 'delete'),
                $this->getBooleanOption($options, 'belong')
            );
        } else {
            $this->log('Wrong --direction value');
        }
    }

    protected function executeFromDirection() {
        
        //TODO: Use local Bionic host accounts
        $account = TwilioAccountTable::getInstance()->findDefaultAccount();
        $accountSid = $account->getSid();
        $authToken = $account->getAuthToken();
        
        $incomingPhoneNumbersList =
            Doctrine_Core::getTable('TwilioIncomingPhoneNumber')
                ->findAll(Doctrine_Core::HYDRATE_ARRAY)
        ;
        foreach ($incomingPhoneNumbersList as $number) {
            $result = TwilioPhoneNumberAPI::sendIncomingPhoneNumber(
                $accountSid,
                $authToken,
                $number['sid'],
                $number['url'],
                $number['friendlyName'],
                $number['twilio_local_area_code']['phone_number_type'],
                $number['twilio_local_area_code']['code'],
                $number['http_method_type']
            );
            if (empty($result)) {
                $this->logSectionNamed(self::COMMENT_PROCESS_UPDATE . $number['phone_number'] . ' has not been updated!!!');
            } else {
                $this->logSectionNamed(self::COMMENT_PROCESS_UPDATE . $number['phone_number'] . ' has been updated (Bionic -> VoIP server)');
            }
        }
    }

    public function executeToDirection($needDelete, $needDeleteStrangers) {

        $this->logSectionNamed(self::COMMENT_PROCESS . 'Retrieving data about actual incoming phone numbers from VoIP server');

        //TODO: Use local Bionic host accounts
        $account = TwilioAccountTable::getInstance()->findDefaultAccount();
        $accountSid = $account->getSid();
        $authToken  = $account->getAuthToken();

        $incomingPhoneNumbers = TwilioPhoneNumberAPI::getAllIncomingPhoneNumbers(
            $accountSid,
            $authToken
        );
        $receivedNumbersCountExistent = count($incomingPhoneNumbers);

        $this->logSectionNamed(self::COMMENT_SUM_VOIP . "{$receivedNumbersCountExistent} numbers received from VoIP server");
        $this->log('');



        $this->executeToDirectionUpdateCreate($account, $incomingPhoneNumbers);

        if ($needDelete) {
            $this->executeToDirectionDelete($incomingPhoneNumbers);
        }

        if ($needDeleteStrangers) {
            $this->executeToDirectionDeleteStrangers();
        }

        $this->restoreDefaultNumbers();
        $this->deleteUnnecessaryDefaultNumbers();
        $this->deleteDeletedCampaignsNumbers();
        $this->deleteDeletedAdvertisersNumbers();

        $this->addNecessaryBionicNumbers();
    }

    protected function getStoredState() {

        $state = array(
            'ALL'           => TwilioIncomingPhoneNumberTable::calc(WasDeletedHelper::TYPE_ALL),
            'DELETED'       => TwilioIncomingPhoneNumberTable::calc(WasDeletedHelper::TYPE_DELETED),
            'NON_DELETED'   => TwilioIncomingPhoneNumberTable::calc(WasDeletedHelper::TYPE_NON_DELETED),
        );

        return $state;
    }

    protected function preExecuteToDirectionPart() {

        $state = $this->getStoredState();

        $this->logSectionNamed(self::COMMENT_SUM_BEFORE . "{$state['ALL']} all numbers were at the DB before synchronizing");
        $this->logSectionNamed(self::COMMENT_SUM_BEFORE . "{$state['DELETED']} deleted numbers were at the DB before synchronizing");
        $this->logSectionNamed(self::COMMENT_SUM_BEFORE . "{$state['NON_DELETED']} non deleted numbers were at the DB before synchronizing");

        return $state;
    }

    protected function postExecuteToDirectionPart() {

        $state = $this->getStoredState();

        $this->logSectionNamed(self::COMMENT_SUM_AFTER . "{$state['ALL']} all numbers are at the DB after synchronizing");
        $this->logSectionNamed(self::COMMENT_SUM_AFTER . "{$state['DELETED']} deleted numbers are at the DB after synchronizing");
        $this->logSectionNamed(self::COMMENT_SUM_AFTER . "{$state['NON_DELETED']} non deleted numbers are at the DB after synchronizing");

        $this->log('');

        return $state;
    }

    protected function executeToDirectionUpdateCreate($account, $incomingPhoneNumbers) {

        $this->preExecuteToDirectionPart();


        $updatedNumbersCount = 0;
        $createdNumbersCount = 0;
        
        if (!empty($account)) {
            foreach ($incomingPhoneNumbers as $incomingNumber) {
                try {                
                    $storedIncomingNumber = TwilioIncomingPhoneNumberTable::getInstance()
                        ->findBySid($incomingNumber['Sid'])
                    ;
                    if (!empty($storedIncomingNumber)) {
                        $updated = $this->updateIncomingPhoneNumber($incomingNumber, $storedIncomingNumber);
                        if ($updated > 0) {
                            $this->logSectionNamed(self::COMMENT_PROCESS_UPDATE . "Existing incoming number with Sid {$incomingNumber['Sid']} and number {$incomingNumber['PhoneNumber']} has been updated from VoIP server");
                            if ($updated == 2) {
                                $this->logSectionNamed(self::COMMENT_PROCESS_RESTORE . "Incoming number for Sid {$incomingNumber['Sid']}} and number {$incomingNumber['PhoneNumber']} has been restored");
                            }
                            $updatedNumbersCount++;
                        }
                    } else {
                        $areaCode = substr($incomingNumber['PhoneNumber'], 0, 3);
                        $twilioLocalAreaCode = TwilioLocalAreaCodeTable::getInstance()->findByCode($areaCode);
                        if (empty($twilioLocalAreaCode)) {
                            $twilioLocalAreaCode = new TwilioLocalAreaCode();
                            //TODO:: how we can get type (local or toll free)
                            $twilioLocalAreaCode->setCode($areaCode);
                            $twilioLocalAreaCode->setPhoneNumberType('local');
                            $twilioLocalAreaCode->save();
                        }
                        if (!empty($twilioLocalAreaCode)) {
                            $newIncomingPhoneNumber = new TwilioIncomingPhoneNumber();
                            $newIncomingPhoneNumber->setSid($incomingNumber['Sid']);
                            $newIncomingPhoneNumber->setTwilioAccount($account);
                            $newIncomingPhoneNumber->setUrl($incomingNumber['Url']);
                            $newIncomingPhoneNumber->setHttpMethodType($incomingNumber['Method']);
                            $newIncomingPhoneNumber->setPhoneNumber($incomingNumber['PhoneNumber']);
                            $newIncomingPhoneNumber->setFriendlyName($incomingNumber['FriendlyName']);
                            $newIncomingPhoneNumber->setTwilioLocalAreaCode($twilioLocalAreaCode);
                            $newIncomingPhoneNumber->save();

                            //New number must belong to Bionic company
                            $bionicIncomingNumberPool = new BionicIncomingNumberPool();
                            $bionicIncomingNumberPool->addNewIncomingPhoneNumberToCleaningPool($newIncomingPhoneNumber);
                            $this->logSectionNamed(self::COMMENT_PROCESS_NEW . "New incoming number for Sid {$incomingNumber['Sid']} and number {$incomingNumber['PhoneNumber']} has been created");

                            $createdNumbersCount++;
                        }
                    }
                } catch(Exception $e) {
                    $this->log($e->getTraceAsString());
                }
            }
        }

        $this->logSectionNamed(self::COMMENT_SUM_DURING . "{$updatedNumbersCount} numbers updated successfully");
        $this->logSectionNamed(self::COMMENT_SUM_DURING . "{$createdNumbersCount} numbers created successfully");


        $this->postExecuteToDirectionPart();
    }

    protected function executeToDirectionDelete($incomingPhoneNumbers) {

        $state = $this->preExecuteToDirectionPart();
        $receivedNumbersCountExistent = count($incomingPhoneNumbers);


        $toDeleteCountAll = $state['NON_DELETED'] - $receivedNumbersCountExistent;
        $toDeleteCountAll = $toDeleteCountAll > 0 ? $toDeleteCountAll : 0;
        $this->logSectionNamed(self::COMMENT_SUM_SHOULD . "{$toDeleteCountAll} EXISTENT numbers should be marked as deleted at the DB after synchronizing");

        $toDeleteCount = $toDeleteCountAll - $state['DELETED'];
        $toDeleteCount = $toDeleteCount > 0 ? $toDeleteCount : 0;
        $this->logSectionNamed(self::COMMENT_SUM_SHOULD . "{$toDeleteCount} EXISTENT numbers should be marked as deleted during synchronizing");

        $deletedCount = $this->deleteUnexistentNumbers($incomingPhoneNumbers);
        $this->logSectionNamed(self::COMMENT_SUM_DURING . "{$deletedCount} numbers marked as deleted successfully");


        $this->postExecuteToDirectionPart();
    }

    protected function executeToDirectionDeleteStrangers() {

        $this->preExecuteToDirectionPart();


        $result = $this->deleteExistentNumbersFromStrangerProjects();
        $deletedStrangersCount  = $result['deletedStrangersCount'];
        $addedOrdersCount       = $result['addedOrdersCount'];
        $needOrdersCount        = $result['needOrdersCount'];

        $this->logSectionNamed(self::COMMENT_SUM_DURING . "{$deletedStrangersCount} numbers from stranger projects marked as deleted successfully");
        $this->logSectionNamed(self::COMMENT_SUM_SHOULD . "{$needOrdersCount} orders SHOULD have been added.");
        $this->logSectionNamed(self::COMMENT_SUM_DURING . "{$addedOrdersCount} orders have been added successfully");


        $this->postExecuteToDirectionPart();
    }

    /**
     * @param array $incomingNumber
     * @param TwilioIncomingPhoneNumber $storedIncomingPhoneNumber
     * @return 0 - it does not need to upfate; 1 - has been updated; 2 - has been updated and restored
     */
    protected function updateIncomingPhoneNumber(
        Array $incomingNumber,
        TwilioIncomingPhoneNumber $storedIncomingPhoneNumber
    ) {

        $updated = 0;

        $wasStrangerUrl = TwilioIncomingPhoneNumberTable::isStrangerProjectUrl($storedIncomingPhoneNumber->getUrl());

        $storedIncomingPhoneNumber->setUrl($incomingNumber['Url']);
        $storedIncomingPhoneNumber->setFriendlyName($incomingNumber['FriendlyName']);
        $storedIncomingPhoneNumber->setHttpMethodType($incomingNumber['Method']);

        if ($storedIncomingPhoneNumber->isModified()) {

            $restored = false;
            if ($wasStrangerUrl) {
                $isStrangerUrl = TwilioIncomingPhoneNumberTable::isStrangerProjectUrl($incomingNumber['Url']);
                if (!$isStrangerUrl) {
                    if ($storedIncomingPhoneNumber->getWasDeleted()) {
                        $storedIncomingPhoneNumber->setWasDeleted(false);

                        $bionicIncomingNumberPool = new BionicIncomingNumberPool();
                        $bionicIncomingNumberPool->addNewIncomingPhoneNumberToCleaningPool($storedIncomingPhoneNumber);

                        $restored = true;
                    }
                }
            }

            $savedNumber = $storedIncomingPhoneNumber->save();

            $updated = $restored ? 2 : 1;
        }

        return $updated;
    }

    protected function deleteUnexistentNumbers(Array $incomingPhoneNumbers) {

        $user = sfGuardUserTable::findSuperAdmin();

        // Sids of existent numbers
        $numberSidArray = array();
        foreach ($incomingPhoneNumbers as $serverNumber) {
            $numberSidArray[] = $serverNumber['Sid'];
        }

        // Numbers need to be deleted
        $numbersToDeleteList =
            TwilioIncomingPhoneNumberTable::findAllOutOfSidList($numberSidArray)
        ;

        $deletedCount = 0;
        foreach ($numbersToDeleteList as $number) {
            try {
                $number->deleteByUser($user);
                $deletedCount++;
                $this->logSectionNamed(self::COMMENT_PROCESS_DELETE . "Incoming number with Sid {$number->getSid()} and number {$number->getPhoneNumber()} has been marked as deleted");
            } catch (Exception $e) {
               $this->log($e->getMessage());
               $this->log($e->getTraceAsString());
            }
        }

        return $deletedCount;
    }

    protected function deleteExistentNumbersFromStrangerProjects() {

        $superAdminUser = sfGuardUserTable::findSuperAdmin();

        // Numbers need to be deleted
        $numbersToDeleteList =
            TwilioIncomingPhoneNumberTable::findExistentNumbersFromStrangerProjects()
        ;
        $this->logSectionNamed(self::COMMENT_SUM_SHOULD . "{$numbersToDeleteList->count()} EXISTENT incoming numbers from stranger projects should be marked as deleted during synchronizing");

        $deletedStrangersCount  = 0;
        $addedOrdersCount       = 0;
        $needOrdersCount        = 0;
        foreach ($numbersToDeleteList as $number) {

            $success = true;
            $needToRestore  = false;
            $advertiser     = null;
            $campaign       = null;

            $urlBase = $number->getUrl();
            $url = substr($urlBase, 0, 30);
            $url = (strlen($urlBase) > 30) ? "$url..." : $url;

            $msg = "Sid={$number->getSid()} url='{$url}'";

            $localAreaCode = $number->getTwilioLocalAreaCode();
            if ($localAreaCode->exists()) {
                $msg .= "; tlacId={$localAreaCode->getId()} code='{$localAreaCode->getCode()}'";
            }

            $ainp = $number->getAdvertiserIncomingNumberPool();
            if ($ainp->exists()) {
                $advertiser = $ainp->getAdvertiser();
                if ($advertiser->exists()) {
                    $msg .= "; advId={$advertiser->getId()}";
                    $needToRestore = true;
                    $needOrdersCount++;
                }
                $campaign = $ainp->getCampaign();
                if ($campaign->exists()) {
                    $msg .= "; camId={$campaign->getId()}";
                } else {
                    //NOTE:: strange behaviour of doctrine. Temporary solution.
                    $campaign = null;
                    $ainp->setCampaign(null);
                }
            }

            try {
                $number->deleteByUser($superAdminUser);
                $deletedStrangersCount++;
            } catch (Exception $e) {
                $success = false;
                $this->log($e->getMessage());
                $this->log($e->getTraceAsString());
            }

            $msg = "Number($msg) marked as deleted";
            if ($needToRestore) {
                if ($success) {
                    $this->logSectionNamed(self::COMMENT_PROCESS_DELETE_RESTORE . $msg);
                } else {
                    $this->logSectionNamed(self::COMMENT_PROCESS_DELETE_RESTORE_ERROR . $msg);
                }
            } else {
                if ($success) {
                    $this->logSectionNamed(self::COMMENT_PROCESS_DELETE . $msg);
                } else {
                    $this->logSectionNamed(self::COMMENT_PROCESS_DELETE_ERROR . $msg);
                }
            }

            if (!$success) {
                continue;
            }

            if ($needToRestore) {
                $msgOrder = '';
                try {
                    $incomingNumbersQueue = new IncomingNumbersQueue();
                    $incomingNumbersQueue->setTwilioLocalAreaCode($localAreaCode);
                    $incomingNumbersQueue->setAdvertiser($advertiser);
                    $incomingNumbersQueue->setCampaign($campaign);
                    $incomingNumbersQueue->setStatus(IncomingNumbersQueueTable::STATUS_TYPE_OPEN);
                    $incomingNumbersQueue->setAmount(1);
                    $incomingNumbersQueue->setDoneAmount(0);
                    $incomingNumbersQueue->setCreatedDate(time());
                    $incomingNumbersQueue->setUser($superAdminUser);
                    if (empty($campaign)) {
                        $incomingNumbersQueue->setOrderType(IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE);
                    } else {
                        $incomingNumbersQueue->setOrderType(IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE_AND_ASSIGN);
                    }

                    $incomingNumbersQueue->save();
                    $addedOrdersCount++;

                    $msgOrder = "id={$incomingNumbersQueue->getId()}";
                } catch (Exception $e) {
                    $success = false;
                    $this->log($e->getMessage());
                    $this->log($e->getTraceAsString());
                }

                $msgOrder = "Order to restore number has been created ($msgOrder)";
                if ($success) {
                    $this->logSectionNamed(self::COMMENT_PROCESS_DELETE_ORDER . $msg);
                } else {
                    $this->logSectionNamed(self::COMMENT_PROCESS_DELETE_ORDER_ERROR . $msg);
                }
            }
        }

        return array(
            'deletedStrangersCount'=> $deletedStrangersCount,
            'addedOrdersCount'     => $addedOrdersCount,
            'needOrdersCount'      => $needOrdersCount,
        );
    }

    /**
     * If campaign has no default numbers then:
     *   1. if campaign has numbers, then one of them will be made default
     *   2. if campaign has no number, then some advertiser's or bionic's number will be moved and made default
     */
    protected function restoreDefaultNumbers() {

        $superAdminUser = sfGuardUserTable::findSuperAdmin();

        $campaignsWithoutDefaultNumbers = CampaignTable::findAllExistentWithoutDefaultNumber();
        foreach ($campaignsWithoutDefaultNumbers as $i => $campaign) {
            $con = null;

            $campaignId     = $campaign->getId();
            $name           = $campaign->getName();

            $campaignInfo = "campaign (id=$campaignId '$name')";

            $campaignAINPs = $campaign->getAdvertiserIncomingNumberPool();
            if ($campaignAINPs->count() <= 0) {
                try {
                    $newDefaultNumber = $campaign->assignDefaultNumber($superAdminUser);
                    $newDefaultNumberId = $newDefaultNumber->getId();
                    $this->logSectionNamed(self::COMMENT_DEFAULT_NUMBER_ADD . "$campaignInfo HADN'T its own number(s), newDefaultNumberId=$newDefaultNumber");
                } catch (Exception $e) {
                    $this->logSectionNamed(self::COMMENT_DEFAULT_NUMBER_ADD_ERROR . "FAIL: $campaignInfo still HASN'T default number.");
                }
            } else {
                try {
                    $newDefaultNumber = $campaignAINPs->getFirst();
                    $newDefaultNumber->setIsDefault(true);
                    $newDefaultNumber->setCampaignId($campaignId);
                    $newDefaultNumber->save($con);

                    $newDefaultNumberId = $newDefaultNumber->getId();
                    $amount = $campaignAINPs->count();
                    $this->logSectionNamed(self::COMMENT_DEFAULT_NUMBER_ADD_SELF . "$campaignInfo HAS $amount its own number(s), so newDefaultNumberId=$newDefaultNumber");
                } catch (Exception $e) {
                    $this->logSectionNamed(self::COMMENT_DEFAULT_NUMBER_ADD_SELF_ERROR . "FAIL: $campaignInfo still HASN'T default number.");
                }
            }
        }
        $this->log('');
    }

    /**
     * If campaign has several default numbers then one of them still be default, other will be made not default
     */
    protected function deleteUnnecessaryDefaultNumbers() {

        $campaignsWithSeveralDefaultNumbers = CampaignTable::findAllExistentWithSeveralDefaultNumbers();
        foreach ($campaignsWithSeveralDefaultNumbers as $i => $campaign) {

            try {
                $campaignId     = $campaign->getId();
                $name           = $campaign->getName();

                $campaignInfo = "campaign (id=$campaignId '$name')";

                $unnecessaryInfo = $campaign->removeUnnecessaryDefaultNumbers();
                $unnecessaryAmount  = $unnecessaryInfo['amount_unnecessary_had'];
                $successAmount      = $unnecessaryInfo['amount_unnecessary_deleted'];
                $this->logSectionNamed(self::COMMENT_DEFAULT_NUMBER_SEVERAL . "$campaignInfo HAD $unnecessaryAmount unnecessary default numbers. $successAmount of them have been successfully made simple");
            } catch (Exception $e) {
                $this->logSectionNamed(self::COMMENT_DEFAULT_NUMBER_SEVERAL_ERROR . "FAIL: $campaignInfo still HAS unnecessary default numbers");
            }
        }
    }

    protected function deleteDeletedCampaignsNumbers() {

        $campaignsDeletedWithNumbers = CampaignTable::findAllDeletedWithNumber();
        foreach ($campaignsDeletedWithNumbers as $campaign) {
            try {
                $advertiserId       = $campaign->getAdvertiserId();
                $advertiserName     = (string)($campaign->getAdvertiser());
                $advertiser         = $campaign->getAdvertiser();
                $campaignId         = $campaign->getId();
                $name               = $campaign->getName();

                $msg = "Advertiser(id=$advertiserId '$advertiserName') Deleted Campaign (id=$campaignId '$name') has numbers -> redelete them";

                $campaign->delete();

                $this->logSectionNamed(self::COMMENT_DELETED_CAMPAIGN_NUMBER . $msg);
            } catch (Exception $e) {
                $this->logSectionNamed(self::COMMENT_DELETED_CAMPAIGN_NUMBER_ERROR . $msg);
            }
        }
    }

    protected function deleteDeletedAdvertisersNumbers() {

        $advertiserDeletedWithNumbers = AdvertiserTable::findAllDeletedWithNumber();
        foreach ($advertiserDeletedWithNumbers as $advertiser) {
            try {
                $advertiserId   = $advertiser->getId();
                $advertiserName = $advertiser->getName();
                $msg = "Deleted Advertiser(id=$advertiserId '$advertiserName') has numbers -> redelete them";

                $advertiser->delete();

                $this->logSectionNamed(self::COMMENT_DELETED_ADVERTISER_NUMBER . $msg);
            } catch (Exception $e) {
                $this->logSectionNamed(self::COMMENT_DELETED_ADVERTISER_NUMBER_ERROR . $msg);
            }
        }
    }

    protected function addNecessaryBionicNumbers() {

        $existentIncomingPhoneNumbersWithoutBionic = TwilioIncomingPhoneNumberTable::findAllWithoutBionic();

        $this->log('');
        $this->logSectionNamed(self::COMMENT_SUM_SHOULD . $existentIncomingPhoneNumbersWithoutBionic->count() . ' bionic numbers should be added');

        $amountSuccessful = 0;
        foreach ($existentIncomingPhoneNumbersWithoutBionic as $incomingPhoneNumber) {
            try {
                //New number must belong to Bionic company
                $bionicIncomingNumberPool = new BionicIncomingNumberPool();
                $bionicIncomingNumberPool->addNewIncomingPhoneNumberToCleaningPool($incomingPhoneNumber);
                $amountSuccessful++;
                $this->logSectionNamed(self::COMMENT_TO_ADD_BIONIC . "New Bionic number for Sid {$incomingNumber->getSid()} and number {$incomingNumber->getPhoneNumber()} has been created");
            } catch (Exception $e) {
                $this->logSectionNamed(self::COMMENT_TO_ADD_BIONIC_ERROR . "New Bionic number for Sid {$incomingNumber->getSid()} has NOT been created");
            }
        }

        $this->logSectionNamed(self::COMMENT_SUM_DURING . "$amountSuccessful bionic numbers have been added successfully.");
        $this->log('');
    }
}
