<?php

class sendIncomingCallDurationAlertsTask extends sfBaseTask
{
  protected function configure()
  {
    // // add your own arguments here
    // $this->addArguments(array(
    //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
    // ));

    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      // add your own options here
    ));

    $this->namespace        = '';
    $this->name             = 'sendIncomingCallDurationAlerts';
    $this->briefDescription = '';
    $this->detailedDescription = <<<EOF
The [sendIncomingCallDurationAlerts|INFO] task sends emails based on alert times set in campaign's table.
Call it with:

  [php symfony sendIncomingCallDurationAlerts|INFO]
EOF;
  }

  protected function execute($arguments = array(), $options = array())
  {
    // initialize the database connection
    $databaseManager = new sfDatabaseManager($this->configuration);
    $connection = $databaseManager->getDatabase($options['connection'])->getConnection();

    // get all twilio_incoming_phone_number_id  from advertiser_incoming_number_pool for each of the campaigns
    
    // get all active campaigns
    $q = Doctrine_Query::create()
    ->from('campaign c')
    ->where('c.is_enabled = ?', '1')
    ->andWhere('c.call_alert_shorter_than_secs > 0 OR c.call_alert_greater_than_secs > 0 OR c.call_alert_greater_than_mins > 0');
    
    $this->campaigns = $q->execute();
    
    $campaignsIds = array();
    
    $callResultsfile = fopen(__DIR__ ."/../../web/callResults.html", "w") or die("Unable to open file!");
    
    
    foreach($this->campaigns as $campaign){
    	
    	
    	// get campaign's Advertiser's email_id and username
    	
    	$advertiserName = $campaign->getAdvertiser();
    	$advertiser_id = $campaign->getAdvertiser()->getId();
    	
    	
    	// get Doctrine_Connection object
    	$con = Doctrine_Manager::getInstance()->connection();
    	// execute SQL query, receive Doctrine_Connection_Statement
    	$st = $con->execute("SELECT email FROM sf_guard_user_profile a WHERE a.advertiser_id =".$advertiser_id);
    	// fetch query result
    	$this->emails = $st->fetchObject();
    	$emailsArray = array();
    	
    	foreach($this->emails as $email){
    		
    		$emailsArray[$email] = $email;
    		
    		
    	}
    	

    	
    	$resultHtmlTr = '';
    	$serialNumber = 0;
    	$trackAlertCallsIds = array();
    	
    	// get all twilio incoming phone number ids related to campaign
    	
    	// get Doctrine_Connection object
    	$con = Doctrine_Manager::getInstance()->connection();
    	// execute SQL query, receive Doctrine_Connection_Statement
    	$st = $con->execute("SELECT twilio_incoming_phone_number_id FROM advertiser_incoming_number_pool a WHERE a.campaign_id =".$campaign->getId());
    	// fetch query result
    	$this->numberPool = $st->fetchAll();
    	
    	//print_r($this->numberPool);
    	
    	//get all numbers attached to campaign
    	
    	 $numberPoolArray = array();
    	 
    	foreach($this->numberPool as $numberPoolNumber){
    		 
    		$numberPoolArray[] = $numberPoolNumber['twilio_incoming_phone_number_id'];
    		
    	}
    	
    	if(count($numberPoolArray)>0 && isset($numberPoolArray)){
    		
    		echo "count of number pool".count($numberPoolArray)."\n";
    		
    		
    		// get Doctrine_Connection object
    		$con = Doctrine_Manager::getInstance()->connection();
    		// execute SQL query, receive Doctrine_Connection_Statement
    		$st = $con->execute("SELECT * FROM twilio_incoming_call a WHERE a.twilio_incoming_phone_number_id in(".implode(",", $numberPoolArray).") AND end_time >= UNIX_TIMESTAMP(DATE_SUB(now(), INTERVAL 1 hour)) AND sent_email_alert != 1 ORDER BY created_at DESC");
    		// fetch query result
    		$this->twilioIncomingCalls = $st->fetchAll();
    		
    		//echo count($this->twilioIncomingCalls)."\n";
    		
    		foreach($this->twilioIncomingCalls as $twilioIncomingCall){
    			 
    			 
    		    // calculate call durations
    			
    			 $dateTimeConverted1 =  date('Y-m-d h:i:s',$twilioIncomingCall['start_time'])."\n";
    			$dateTimeConverted2 =  date('Y-m-d h:i:s',$twilioIncomingCall['end_time'])."\n";
    			
    			
    			
    			$datetime1 = new DateTime($dateTimeConverted1);
    			$datetime2 = new DateTime($dateTimeConverted2);
    			$interval = $datetime1->diff($datetime2); 
    			
    			$seconds = $twilioIncomingCall['duration'];
    			$H = floor($seconds / 3600);
    			$i = ($seconds / 60) % 60;
    			$s = $seconds % 60;
    			//echo sprintf("%2d:%2d:%2d", $H, $i, $s);
    			
				
    			
    			if ( $campaign->getCallAlertShorterThanSecs()>0 && $twilioIncomingCall['duration'] <= $campaign->getCallAlertShorterThanSecs() ){
    				
    				//get the twilio call number 
    				
    				$st = $con->execute("SELECT phone_number FROM `twilio_caller_phone_number` WHERE id=".$twilioIncomingCall['twilio_caller_phone_number_id']);
    				// fetch query result
    				$this->callerNumber = $st->fetchObject();
    				
    				//get the incoming caller number
    				
    				$st = $con->execute("SELECT phone_number  FROM `twilio_incoming_phone_number` WHERE id=".$twilioIncomingCall['twilio_incoming_phone_number_id']);
    				// fetch query result
    				$this->incomingCallerNumber = $st->fetchObject();
    				
    				
    				//var_dump($this->callerNumber); die();
    				$serialNumber++;
    				$trackAlertCallsIds[$twilioIncomingCall['id']] = $twilioIncomingCall['id'];
    				
    				if($twilioIncomingCall['duration'] > 60 && $twilioIncomingCall['duration'] < 3600 ) {
    				
    					$intervalText = '<td>'.sprintf("%2d min %2d sec", $i, $s).'</td>';
    				
    				}elseif($twilioIncomingCall['duration'] > 60 && $twilioIncomingCall['duration'] > 3600 ){
    				
    					$intervalText =  '<td>'.sprintf("%2d hr %2d min %2d sec", $H, $i, $s).'</td>';
    				}else{
    				
    					$intervalText =  '<td>'.sprintf("%2d sec", $s).'</td>';
    				
    				}
    				
    				$resultHtmlTr .= '<tr>
    								<th>'.$serialNumber.'</th>
								    <td>'.$campaign->getName().'</td>
								    <td align="center">'.$this->callerNumber->phone_number.'</td>
								    <td align="center">'.$this->incomingCallerNumber->phone_number.'</td>
								    <td>'.$dateTimeConverted1.'</td>
								    <td>'.$dateTimeConverted2.'</td>
								     '.$intervalText.'
								    <td>Call is shorter than '.$campaign->getCallAlertShorterThanSecs().' sec</td>
								  </tr>';
    				
    				
    			}elseif($campaign->getCallAlertGreaterThanMins() > 0 || $campaign->getCallAlertGreaterThanSecs() > 0){
    				
    				//get the twilio call number
    				
    				$st = $con->execute("SELECT phone_number FROM `twilio_caller_phone_number` WHERE id=".$twilioIncomingCall['twilio_caller_phone_number_id']);
    				// fetch query result
    				$this->callerNumber = $st->fetchObject();
    				
    				//get the incoming caller number
    				
    				$st = $con->execute("SELECT phone_number  FROM `twilio_incoming_phone_number` WHERE id=".$twilioIncomingCall['twilio_incoming_phone_number_id']);
    				// fetch query result
    				$this->incomingCallerNumber = $st->fetchObject();
    				
    				
    				$alertDurationSeconds = 0;
    				
    				//check if seconds are filled and calculate total check duration in seconds
    				
    				if($campaign->getCallAlertGreaterThanMins() > 0){
    					
    					$alertDurationGreaterThanSeconds = 0;
    					
    					if( $campaign->getCallAlertGreaterThanSecs() > 0){
    						
    						$alertDurationGreaterThanSeconds = $campaign->getCallAlertGreaterThanSecs();
    						
    					}
    					
    					$alertDurationSeconds = $alertDurationGreaterThanSeconds + ($campaign->getCallAlertGreaterThanMins() * 60 );
    					$alertReasonText = $campaign->getCallAlertGreaterThanMins()." min";
    					
    				}else{
    					
    					$alertDurationSeconds =  $campaign->getCallAlertGreaterThanSecs();
    					$alertReasonText = $campaign->getCallAlertGreaterThanSecs()." sec";
    				}
    				
    				
    				
    				
    				if( $twilioIncomingCall['duration'] >= $alertDurationSeconds){
    					
    					$serialNumber++;
    					$trackAlertCallsIds[$twilioIncomingCall['id']] = $twilioIncomingCall['id'];
    					
    					if($twilioIncomingCall['duration'] > 60 && $twilioIncomingCall['duration'] < 3600 ) {
    						
    						$intervalText = '<td>'.sprintf("%2d min %2d sec", $i, $s).'</td>';
    						
    					}elseif($twilioIncomingCall['duration'] > 60 && $twilioIncomingCall['duration'] > 3600 ){
    						
    						$intervalText =  '<td>'.sprintf("%2d hr %2d min %2d sec", $H, $i, $s).'</td>';
    					}else{
    						
    						$intervalText =  '<td>'.sprintf("%2d sec", $s).'</td>';
    						
    					}
    					
    					$resultHtmlTr .= '<tr>
    							    <th>'.$serialNumber.'</th>
								    <td>'.$campaign->getName().'</td>
								    <td align="center">'.$this->callerNumber->phone_number.'</td>
								    <td align="center">'.$this->incomingCallerNumber->phone_number.'</td>
								    <td>'.$dateTimeConverted1.'</td>
								    <td>'.$dateTimeConverted2.'</td>
								    '.$intervalText.'
								    <td>Call is longer than '.$alertReasonText.'</td>
								  </tr>';
    						
    				}	
    				
    				
    			}
    			
	 
    		} 
			
    		
    		// if there are results for this call generate the email
    		
    		if(count($trackAlertCallsIds) >0){
	    		// start email template for sending emails per template
	    		$html ='<html>
	    				<span>Hello '.$advertiserName.',<span>
	    						<br /> <br/>
	    				<span> Following has been generated according to the Alert set in Your Campaign Settings.
	    						<br /> <br /> 
	    				
	    			  <table cellspacing="0px" cellpadding="3px" border="1">
						  <tr style="background-color:grey">
		    				<th>Sr#</th>
						    <th>Campaign</th>
						    <th>Caller Number</th>
		    				<th>Incoming Number</th>
						    <th>Start Time</th>
						    <th>End Time</th>
						    <th>Duration</th>
						    <th>Remarks</th>
						  </tr>
						  '.$resultHtmlTr.'
				       </table>
					   <br /> <br />
						Thanks! <br />
						Bionic Click Team.  		
						<br /> <br />  	
						 Emails will be sent out to the following recepient/s for this campaign ('.implode('","', $emailsArray).') 		
					</html>   		
					';
	    		
	    		//$mailer = sfContext::getInstance()->getMailer();
	    		
	    		$fromEmail = "Bionic@cron.com";
	    		$fromFullName = "Bionic Click";
	    		$toEmail = "mrtariqkhan@gmail.com";
	    		
	    		$toEmail = array(
	    				'mrtariqkhan@gmail.com',
	    				'm.sohail.safdar@gmail.com',
	    		);
	    		$toFullName = "Tariq khan";
	    		$subject = "Campaign Call Duration Alert";
	    		
	    		
	    		
	    		$message = $this->getMailer()->compose();
	    		$message->setSubject($subject);
	    		$message->setTo($toEmail);
	    		$message->setFrom($fromEmail);
	    		
	    		$message->setBody($html, 'text/html');
	
	    		$this->getMailer()->send($message);
	    		
	    		
	    		
	    		$st = $con->execute('UPDATE twilio_incoming_call SET sent_email_alert = 1 where id in ("'.implode('","', $trackAlertCallsIds).'")');
	    		// fetch query result
	    		//$this->callerNumber = $st->fetchObject();
	    		
	    		fwrite($callResultsfile, $html);
    		}
    	}
    	
    }
    
    fclose($callResultsfile);

    // then check in twilio_incoming_call for those phone_number_ids 
    //log in a CSV file the call duration and set the "sent_email_alert" field in the same table if it meets the condition

    
    
    
  }
}
