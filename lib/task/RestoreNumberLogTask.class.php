<?php

class RestoreNumberLogTask extends BionicTask {

    const COMMENT_SUM_BEFORE= 'BEFORE: ';
//    const COMMENT_SUM_SHOULD= 'SHOULD: ';
    const COMMENT_SUM_AFTER = 'AFTER: ';

    const COMMENT_PROCESS_ORDER                 = '  ';
    const COMMENT_PROCESS_NEED                  = '        ';
    const COMMENT_PROCESS_STATUS_IS_NOT_DONE    = '  NOT PAST: ';
    const COMMENT_PROCESS_ORDER_IS_DONE         = '  DONE ';
    const COMMENT_PROCESS_CREATE_DETAIL         = '     ~> ';
    const COMMENT_PROCESS_CREATE_DETAIL_ERROR   = '    !~> ';
    const COMMENT_PROCESS_NO_COMPATIBLE_ERROR   = '  !     ';
    const COMMENT_PROCESS_COMPATIBLE            = '        ';
    const COMMENT_PROCESS_NOW_IS_OK             = '  OK';
    const COMMENT_PROCESS_NOW_IS_ALMOST_OK      = '  ALMOST OK: ';


    public function __construct(sfEventDispatcher $dispatcher, sfFormatter $formatter) {

        $this->setName('restore-number-log');
        $this->setNameSmall('rnl');
        $this->setNameSection('restore number log');
        parent::__construct($dispatcher, $formatter);
    }
    
    protected function configure() {

        parent::configure();

        $this->addOptions(array(
            
        ));

        $this->briefDescription = 'Buys numbers from BionicIncomingNumberPool or from VoIP server';
        $this->detailedDescription = <<<EOF
The [BuyVoIpNumbers|INFO] task restores phone number log.
Call it with:

[php symfony RestoreNumberLogTask|INFO]

For example:
        [php symfony bionic:restore-number-log|INFO]
    or
        [php symfony bionic:rnl|INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {

        $this->log('');
        $this->logSectionNamed('==========================================================');
        $this->logSectionNamed(TimeConverter::getDateTimeFormattedByString('now', TimeConverter::TIMEZONE_UTC, TimeConverter::FORMAT_ALL));
        $this->log('');

        parent::execute($arguments, $options);

        $this->restoreDetailsByExistingOrders();

        $this->restoreOrdersByExistingNumbers();

        $this->log('');
    }

    protected function restoreDetailsByExistingOrders() {


        $this->preExecute('Restore Details By Existing Orders');
        $advertisers = AdvertiserTable::getInstance()->findAll();
        foreach ($advertisers as $advertiser) {
            $advertiserId = $advertiser->getId();

            $this->logSectionNamed("--- Advertiser '$advertiser' (advId=$advertiserId) ------------------------------------------");

            $orders = $advertiser->getIncomingNumbersQueues();
            foreach ($orders as $order) {
                $id             = $order->getId();
                $type           = $order->getOrderType();
                $campaignId     = $order->getCampaignId();
                $lacId          = $order->getTwilioLocalAreaCodeId();
                $status         = $order->getStatus();
                $amount         = $order->getDoneAmount();
                $needAmount     = $order->getNeedNewDetailsAmount();

                $this->logSectionNamed(self::COMMENT_PROCESS_ORDER . "$status Order #$id ['$type'] (campaignId=$campaignId, areCodeId=$lacId).");
                if ($status != IncomingNumbersQueueTable::STATUS_TYPE_DONE) {
                    $this->logSectionNamed(self::COMMENT_PROCESS_STATUS_IS_NOT_DONE . "Orders of such status are not from the past. So this task will not work with them.");
                } elseif ($needAmount > 0) {
                    $this->logSectionNamed(self::COMMENT_PROCESS_NEED . "It needs to restore $needAmount details (from $amount).");
                    $advertiserIncomingNumberPools = AdvertiserIncomingNumberPoolTable::findNotDetailedCompatible($advertiserId, $campaignId, $lacId, $needAmount);

                    $count = count($advertiserIncomingNumberPools);
                    if (empty($count)) {
                        $this->logSectionNamed(self::COMMENT_PROCESS_NO_COMPATIBLE_ERROR . "there is no compatible phoneNumbers.");
                    } else {
                        $this->logSectionNamed(self::COMMENT_PROCESS_COMPATIBLE . "there is $count compatible phoneNumbers.");

                        $i = 1;
                        foreach ($advertiserIncomingNumberPools as $advertiserIncomingNumberPool) {
                            try {
                                $ainpId = $advertiserIncomingNumberPool->getId();
                                $tipn = $advertiserIncomingNumberPool->getTwilioIncomingPhoneNumber();

                                $purchasedAt = $advertiserIncomingNumberPool->getPurchasedAt();
                                if (empty($purchasedAt)) {
                                    $actionDate = $order->getUpdatedDate();
                                } else {
                                    $actionDate = $purchasedAt;
                                }

                                $advertiserIncomingNumberPoolLog = new AdvertiserIncomingNumberPoolLog();
                                $advertiserIncomingNumberPoolLog->setTwilioIncomingPhoneNumber($tipn);
                                $advertiserIncomingNumberPoolLog->setIncomingNumbersQueueId($id);
                                $advertiserIncomingNumberPoolLog->setActionDate($actionDate);
                                $advertiserIncomingNumberPoolLog->save();

                                $this->logSectionNamed(self::COMMENT_PROCESS_CREATE_DETAIL . "$i). phoneNumber ('$tipn', Sid={$tipn->getSid()}, ainpId=$ainpId, actionDate=$actionDate).");
                            } catch (Exception $e) {
                                $this->logSectionNamed(self::COMMENT_PROCESS_CREATE_DETAIL_ERROR . "$i). phoneNumber ('$tipn', Sid={$tipn->getSid()}, ainpId=$ainpId, actionDate=$actionDate).");
                            }
                            $i++;
                        }

                        --$i;
                        $stillNeedAmount = $needAmount - $i;
                        if ($stillNeedAmount == 0) {
                            $this->logSectionNamed(self::COMMENT_PROCESS_NOW_IS_OK);
                        } else {
                            $this->logSectionNamed(self::COMMENT_PROCESS_NOW_IS_ALMOST_OK . "It still need to restore $stillNeedAmount phoneNumbers.");
                        }
                    }
                } else {
                    $this->logSectionNamed(self::COMMENT_PROCESS_ORDER_IS_DONE);
                }
                $this->log('');
            }
            $this->log('');
        }

        $this->postExecute();
    }

    protected function restoreOrdersByExistingNumbers() {

        $this->preExecute('Restore Orders By Existing Numbers');

        $user = sfGuardUserTable::findSuperAdmin();
        $advertisers = AdvertiserTable::getInstance()->findAll();
        foreach ($advertisers as $advertiser) {
            $advertiserId = $advertiser->getId();

            $this->logSectionNamed("--- Advertiser '$advertiser' (advId=$advertiserId) ------------------------------------------");

            $advertiserIncomingNumberPools = AdvertiserIncomingNumberPoolTable::findNotDetailed($advertiserId);

            $needAmount = count($advertiserIncomingNumberPools);
            if ($needAmount == 0) {
                $this->logSectionNamed(self::COMMENT_PROCESS_ORDER_IS_DONE);
            } else {
                $this->logSectionNamed(self::COMMENT_PROCESS_NEED . "It needs to restore $needAmount orders and its details for this advertiser.");

                $i = 1;
                foreach ($advertiserIncomingNumberPools as $advertiserIncomingNumberPool) {
                    try {
                        $ainpId = $advertiserIncomingNumberPool->getId();
                        $tipn = $advertiserIncomingNumberPool->getTwilioIncomingPhoneNumber();
                        $campaignId = $advertiserIncomingNumberPool->getCampaignId();

                        $actionDate = null;
                        $createdAt = $advertiserIncomingNumberPool->getCreatedAt();
                        if (!empty($createdAt)) {
                            $createdAtTimestamp = TimeConverter::getUnixTimestamp($createdAt, TimeConverter::TIMEZONE_UTC);
                            $actionDate = $createdAtTimestamp;
                        }


                        $orderType = empty($campaignId)
                            ? IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE
                            : IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE_AND_ASSIGN
                        ;

                        $incomingNumbersQueue = new IncomingNumbersQueue();
                        $incomingNumbersQueue->setAdvertiser($advertiser);
                        $incomingNumbersQueue->setDoneAmount(1);
                        $incomingNumbersQueue->setCreatedDate($actionDate);
                        $incomingNumbersQueue->setStatus(IncomingNumbersQueueTable::STATUS_TYPE_DONE);
                        $incomingNumbersQueue->setCampaignId($campaignId);
                        $incomingNumbersQueue->setTwilioLocalAreaCodeId($tipn->getTwilioLocalAreaCodeId());
                        $incomingNumbersQueue->setAmount(1);
                        $incomingNumbersQueue->setUser($user);
                        $incomingNumbersQueue->setOrderType($orderType);
                        $incomingNumbersQueue->save();

                        $id = $incomingNumbersQueue->getId();
                        $advertiserIncomingNumberPoolLog = new AdvertiserIncomingNumberPoolLog();
                        $advertiserIncomingNumberPoolLog->setTwilioIncomingPhoneNumber($tipn);
                        $advertiserIncomingNumberPoolLog->setIncomingNumbersQueueId($id);
                        $advertiserIncomingNumberPoolLog->setActionDate($actionDate);
                        $advertiserIncomingNumberPoolLog->save();

                        $this->logSectionNamed(self::COMMENT_PROCESS_CREATE_DETAIL . "$i). order(id=$id) and details ('$tipn', Sid={$tipn->getSid()}, ainpId=$ainpId, actionDate=$actionDate).");
                    } catch (Exception $e) {
                        $this->logSectionNamed(self::COMMENT_PROCESS_CREATE_DETAIL_ERROR . "$i). order(id=$id) and details ('$tipn', Sid={$tipn->getSid()}, ainpId=$ainpId, actionDate=$actionDate).");
                    }
                    $i++;
                }

                --$i;
                $stillNeedAmount = $needAmount - $i;
                if ($stillNeedAmount == 0) {
                    $this->logSectionNamed(self::COMMENT_PROCESS_NOW_IS_OK);
                } else {
                    $this->logSectionNamed(self::COMMENT_PROCESS_NOW_IS_ALMOST_OK . "It still need to restore $stillNeedAmount orders and its details.");
                }
            }
            $this->log('');
        }

        $this->postExecute();
    }



    protected function preExecute($partName) {

        $this->log('');
        $this->logSectionNamed("====== $partName ==============================================");
        $this->log('');

        $all = AdvertiserIncomingNumberPoolTable::getInstance()->findAll();
        $allCount = count($all);
        $countNotDetailedAll = AdvertiserIncomingNumberPoolTable::countNotDetailedAll();
        $countHave = $allCount - $countNotDetailedAll;
        $allOrders = IncomingNumbersQueueTable::getInstance()->findAll();
        $allOrdersCount = count($allOrders);

        $this->logSectionNamed(self::COMMENT_SUM_BEFORE . "$allOrdersCount orders at all");
        $this->logSectionNamed(self::COMMENT_SUM_BEFORE . "$allCount advertiser phone numbers at all");
        $this->logSectionNamed(self::COMMENT_SUM_BEFORE . "$countHave advertiser phone numbers have details");
        $this->logSectionNamed(self::COMMENT_SUM_BEFORE . "$countNotDetailedAll advertiser phone numbers which have NO details");

        $this->log('');
    }

    protected function postExecute() {

        $all = AdvertiserIncomingNumberPoolTable::getInstance()->findAll();
        $allCount = count($all);
        $countNotDetailedAll = AdvertiserIncomingNumberPoolTable::countNotDetailedAll();
        $countHave = $allCount - $countNotDetailedAll;
        $allOrders = IncomingNumbersQueueTable::getInstance()->findAll();
        $allOrdersCount = count($allOrders);

        $this->logSectionNamed(self::COMMENT_SUM_AFTER . "$allOrdersCount orders at all");
        $this->logSectionNamed(self::COMMENT_SUM_AFTER . "$allCount advertiser phone numbers at all");
        $this->logSectionNamed(self::COMMENT_SUM_AFTER . "$countHave advertiser phone numbers have details");
        $this->logSectionNamed(self::COMMENT_SUM_AFTER . "$countNotDetailedAll advertiser phone numbers which still have NO details");

        $this->log('');
        $this->log('');
    }
}