<?php

require_once(dirname(__FILE__) . '/../Utils/invoices/InvoicesGenerator.class.php');

class GenerateInvoicesTask extends BionicTask {

    const INVOICE_TYPE_GLUE = ' and ';
    const INVOICE_TYPE_COMMENT_GLUE = ', ';

    public function __construct(sfEventDispatcher $dispatcher, sfFormatter $formatter) {

        $this->setName('generate-invoices');
        $this->setNameSmall('gi');
        $this->setNameSection('invoice');

        parent::__construct($dispatcher, $formatter);
    }
    
    protected function configure() {

        parent::configure();

        $typesMoney     = InvoicesGenerator::getTypesMoney();
        $typesDetails   = InvoicesGenerator::getTypesDetails();
        $typesLevel     = InvoicesGenerator::getTypesLevel();

        $invoiceTypesMoneyStringOption   = $typesMoney->toStringFormatted("%s", self::INVOICE_TYPE_GLUE);
        $invoiceTypesDetailsStringOption = $typesDetails->toStringFormatted("%s", self::INVOICE_TYPE_GLUE);
        $invoiceTypesLevelStringOption   = $typesLevel->toStringFormatted("%s", self::INVOICE_TYPE_GLUE);

        $invoiceTypesMoneyStringDescription = $typesMoney->toStringFormatted("%s", self::INVOICE_TYPE_COMMENT_GLUE);

        $invoiceTypesMoneyStringComment     = $typesMoney->toStringFormatted("[%s|COMMENT]", self::INVOICE_TYPE_COMMENT_GLUE);
        $invoiceTypesDetailsStringComment   = $typesDetails->toStringFormatted("[%s|COMMENT]", self::INVOICE_TYPE_COMMENT_GLUE);
        $invoiceTypesLevelStringComment     = $typesLevel->toStringFormatted("[%s|COMMENT]", self::INVOICE_TYPE_COMMENT_GLUE);

        $this->addOptions(array(
            new sfCommandOption('type'      , null, sfCommandOption::PARAMETER_OPTIONAL, "Type of invoices (default: '' that means $invoiceTypesMoneyStringOption)", ''),
            new sfCommandOption('details'   , null, sfCommandOption::PARAMETER_OPTIONAL, "Type of details (default: '' that means $invoiceTypesDetailsStringOption)", ''),
            new sfCommandOption('level'     , null, sfCommandOption::PARAMETER_OPTIONAL, "Type of level (default: '' that means $invoiceTypesLevelStringOption)", ''),
            new sfCommandOption('emails'    , null, sfCommandOption::PARAMETER_OPTIONAL, 'Send or not emails to company admin (true or false). OPTIONAL', 'true'),
        ));

        $this->briefDescription = "Generates $invoiceTypesMoneyStringDescription invoices";

        $this->detailedDescription =
<<<EOF
The [GenerateInvoices|INFO] task:
    1. generates GET, PAY invoices
    2. sends emails to company admin with attached GET, PAY invoices.

Types of invoices are: {$invoiceTypesMoneyStringComment}.
Details of invoices are: {$invoiceTypesDetailsStringComment}.
Levels of invoices are: {$invoiceTypesLevelStringComment}.

For example:
    Generates Pay Summary invoices for Resellers. Emails will not been sent to admins (application, environment, database connection - default):
        [php symfony bionic:generate-invoices --emails=false --type=Pay --details=Summary --level=Reseller|INFO]
    or
        [php symfony bionic:gi --emails=false --type=Pay --details=Summary --level=Reseller|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array()) {

        parent::execute($arguments, $options);

        $doSendEmails = $this->getBooleanOption($options, 'emails');
        $typeMoneyName  = $options['type'];
        $typeDetailsName= $options['details'];
        $typeLevelName  = $options['level'];

        $range = TimeConverter::getLastMonthUnixTimestampRange(TimeConverter::TIMEZONE_UTC);

        $companiesByLevel = $this->configureCompaniesByLevelArray($typeLevelName);
        foreach ($companiesByLevel as $level => $companies) {

            $invoiceTypesMoney   = InvoicesGenerator::getTypesMoney($level)->getTypesByName($typeMoneyName);
            $invoiceTypesDetails = InvoicesGenerator::getTypesDetails($level)->getTypesByName($typeDetailsName);

            $companyLevelName = ucfirst(CompanyTable::getLevelName($level));
            $this->logSectionNamed("============ $companyLevelName invoices: ============");

            foreach ($companies as $company) {
                $companyName    = '"' . $company->getName() . '"';

                foreach ($invoiceTypesMoney as $invoiceTypeMoney => $invoiceTypeMoneyName) {
                    foreach ($invoiceTypesDetails as $invoiceTypeDetails => $invoiceTypeDetailsName) {

                        $this->logSectionNamed("$companyName $invoiceTypeMoneyName $invoiceTypeDetailsName invoice generating.");

                        $emailAddress = '';
                        $filePath = self::generateInvoiceFileSimple(
                            $company, 
                            $invoiceTypeMoney, 
                            $invoiceTypeDetails, 
                            $invoiceTypeDetailsName,
                            $range,
                            $emailAddress
                        );

                        if (empty($filePath)) {
                            $this->logSectionNamed("File has not been created.");
                            continue;
                        }
                        $this->logSection('file+', $filePath);

                        if ($doSendEmails) {
                            if (!empty($emailAddress)) {
                                $this->logSectionNamed("Send email with invoice to admin ($emailAddress).");
                                $templateName = 'global/invoice_mail_template_' . strtolower($invoiceTypeMoneyName);
                                $this->mail(
                                    $emailAddress,
                                    $templateName,
                                    'Invoice',
                                    $filePath,
                                    'application/vnd.ms-excel'
                                );
                            } else {
                                $this->logSectionNamed("Can't send email because email is empty.");
                            }
                        }

//                        //TODO:: uncomment if need to delete generated files
//                        unlink($foundFile);
//                        $this->logSection('file-', $foundFile);

                        $this->log("");
                    }
                }
            }
        }
    }

    protected function mail($to, $templateName, $subject = 'Invoice', $filePath = null, $mimeType = null) {

        $options = array(
            EmailHelper::OPTION_FROM                => array(
                EmailHelper::SUB_OPTION_FULLNAME=> 'Bionicclick',
            ),
            EmailHelper::OPTION_TO                  => array(
                EmailHelper::SUB_OPTION_EMAIL   => $to,
                EmailHelper::SUB_OPTION_FULLNAME=> '',
            ),
            EmailHelper::OPTION_SUBJECT             => $subject,
            EmailHelper::OPTION_TEMPLATES           => array(
                array(
                    EmailHelper::SUB_OPTION_TEMPLATE                => $templateName,
                    EmailHelper::SUB_OPTION_TEMPLATE_PARAMETERS     => array(),
                    EmailHelper::SUB_OPTION_TEMPLATE_CONTENT_TYPE   => EmailHelper::CONTENT_TYPE_TEXT_HTML,
                    EmailHelper::SUB_OPTION_TEMPLATE_PARAMETER_LOGO => 'logo',
                ),
            ),
            EmailHelper::OPTION_ATTACHEMENTS        => array(
                array(
                    EmailHelper::SUB_OPTION_ATTACHEMENT_FILE_PATH   => $filePath,
                    EmailHelper::SUB_OPTION_ATTACHEMENT_MIME_TYPE   => $mimeType,
                ),
            ),
        );

        EmailHelper::mail($options);
    }

    protected function configureCompaniesByLevelArray($typeLevelName) {

        $invoiceTypesLevel = InvoicesGenerator::getTypesLevel()->getTypesByName($typeLevelName);

        $companiesByLevel = array();
        $bionic = CompanyTable::getBionic();
        $bionicLft = $bionic->getLft();
        $bionicRgt = $bionic->getRgt();
        $bionicId = $bionic->getId();
        foreach ($invoiceTypesLevel as $invoiceTypeLevel => $invoiceTypeLevelName) {
            switch ($invoiceTypeLevel) {
                case CompanyTable::$LEVEL_BIONIC:
                    $companiesByLevel[CompanyTable::$LEVEL_BIONIC] = array($bionic);
                    break;
                case CompanyTable::$LEVEL_RESELLER:
//                    $resellers = CompanyTable::findResellers(null, null, false);
                    $resellers = CompanyTable::findResellers($bionicLft, $bionicRgt, false);
                    $companiesByLevel[CompanyTable::$LEVEL_RESELLER] = empty($resellers) ? array() : $resellers->getData();
                    break;
//                case CompanyTable::$LEVEL_AGENCY:
//                    $agencies = CompanyTable::findAgencies(null, null, false);
//                    $companiesByLevel[CompanyTable::$LEVEL_AGENCY] = empty($agencies) ? array() : $agencies->getData();
//                    break;
                case CompanyTable::$LEVEL_ADVERTISER:
//                    $advertisers = AdvertiserTable::findAllByWasDeleted(null, null, WasDeletedHelper::TYPE_NON_DELETED);
                    $advertisers = AdvertiserTable::findByCompanyId($bionicId, WasDeletedHelper::TYPE_NON_DELETED);
                    $companiesByLevel[CompanyTable::$LEVEL_ADVERTISER] = empty($advertisers)? array() : $advertisers->getData();
                    break;
            }
        }

        return $companiesByLevel;
    }

    protected static function generateInvoiceFileSimple(
        $company,
        $invoiceTypeMoney,
        $invoiceTypeDetails,
        $invoiceTypeDetailsName,
        $range,
        & $emailAddress,
        & $fileName = null
    ) {

        $classNameInvoiceGenerator = "InvoicesGenerator{$invoiceTypeDetailsName}";
        $invoiceGenerator = new $classNameInvoiceGenerator(
            $company, 
            $invoiceTypeMoney, 
            $invoiceTypeDetails, 
            $range
        );

        $data = $invoiceGenerator->generateData();

        $infoTotal = $data[InvoicesGenerator::LEVEL_TOTAL];
        $emailAddress = $infoTotal[InvoicesGenerator::INFO_EMAIL_TO];

        $className = "ExcelInvoiceHelper{$invoiceTypeDetailsName}";
        $excelInvoiceHelper = new $className($data);
        $fileName   = $excelInvoiceHelper->generateFile();
        $pathToDir  = $excelInvoiceHelper->getPathToDir();

        $finder = sfFinder::type('file')->name($fileName);
        $foundFiles = $finder->in($pathToDir);
        $foundFilePath = empty($foundFiles) ? null : $foundFiles[0];

        unset($excelInvoiceHelper);
        unset($invoiceGenerator);

        return $foundFilePath;
    }

    /**
     *
     * @param type $company Advertiser or Company
     * @param type $invoiceTypeMoney    one of InvoicesGeneratorBase::INVOICE_TYPE_HAVE_TO_
     * @param type $invoiceTypeDetails  one of InvoicesGeneratorBase::INVOICE_TYPE_DETAILS_
     * @return type 
     */
    public static function generateInvoiceFile(
        $company,
        $invoiceTypeMoney,
        $invoiceTypeDetails,
        $range,
        & $fileName
    ) {

        $level = $company->getLevel();

        $invoiceTypesDetails = InvoicesGenerator::getTypesDetails($level);
        $invoiceTypeDetailsName = $invoiceTypesDetails->getTypeName($invoiceTypeDetails);

        $emailAddress = '';

        return self::generateInvoiceFileSimple(
            $company, 
            $invoiceTypeMoney, 
            $invoiceTypeDetails, 
            $invoiceTypeDetailsName,
            $range,
            $emailAddress,
            $fileName
        );
    }

}
