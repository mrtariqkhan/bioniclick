<?php

class ExportByAgencyTask extends sfBaseTask
{
  protected function configure()
  {
   date_default_timezone_set('UTC');
   $this->today = date('m/d/y');

   $from = date('m/d/y', time() - 60 * 60 * 24);
   $to = $this->today;

   $this->addOptions(array(
    new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
    new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
    new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      // add your own options here
    new sfCommandOption('date_range', null, sfCommandOption::PARAMETER_OPTIONAL, 'Date Range', $from . ' - ' . $to),

    new sfCommandOption('audio', null, sfCommandOption::PARAMETER_OPTIONAL, 'Export Audio files', 'false'),
    ));

   $this->namespace        = '';
   $this->name             = 'export-campaigns-by-agency';
   $this->briefDescription = '';
   $this->detailedDescription = 'Export all campaigns associated with all advertisers in specific Agencies ';
 }

 protected function execute($arguments = array(), $options = array())
 {
    // initialize the database connection
  $databaseManager = new sfDatabaseManager($this->configuration);
  $connection = $databaseManager->getDatabase($options['connection'])->getConnection();

  $this->exportAudio = array_key_exists('audio', $options) ? $options['audio'] : 'false';


    //super admin user
  $this->user = sfGuardUserTable::findById(14);
  $companies = CompanyTable::findAllByExport();
  foreach( $companies as $company){
    echo "\nExporting Agency with this id:".$company->getId();
    if($company->getDateRangeExport()){
      $companyDateRange = ($company->getExportDateRange() != null) ? $company->getExportDateRange() : '';
      $filterValues = $this->parseDateRange($companyDateRange);
      $company->setDateRangeExport(0);
      $company->setExportDateRange('');
      $company->save();
    }
    else{
      $filterValues = $this->parseDateRange($options['date_range']);
    }
    echo "\n Exporting date range is from: " . date('m/d/Y', $filterValues['from']) . " to: " . date('m/d/Y', $filterValues['to']);
    $this->createCompanyFilesStructure($company);

    $advertisers = AdvertiserTable::findByCompanyId($company->getId());
    foreach( $advertisers as $advertiser){
      $this->exportAdvertiser($advertiser, $filterValues);
    }
  }
  echo "\n ----- Exporting Completed-----";
}

protected function exportAdvertiser($advertiser, $filterValues){
  if(empty($advertiser)){
    echo "\nNo such an advertiser with this id:".$id;
    continue;
  }

  echo "\n ----- Exporting Advertiser:".$advertiser->getName()." -----";
  $this->createAdvertiserFilesStructure($advertiser);
  echo "\n ----- Advertiser:".$advertiser->getName()." file structure was created successfully-----";

  $this->preparePager($advertiser, $filterValues);

  $this->exportCSVFile($advertiser);
  echo "\n ----- Advertiser:".$advertiser->getName()." csv file was exported successfully-----";

  if($this->exportAudio == 'true'){
    echo "\n ----- Advertiser:".$advertiser->getName()." exporting audio files-----";
    $this->exportAudioFiles($advertiser);
  }
}

protected function createCompanyFilesStructure($company){
  $company_dir = 'exports/by_agency/'.$company->getName();
  if(!file_exists($company_dir)){mkdir($company_dir);}
}

protected function createAdvertiserFilesStructure($advertiser){
  $advertiser_dir = 'exports/by_agency/'.$advertiser->getCompany()->getName().'/'.$advertiser->getName();
  if(!file_exists($advertiser_dir)){mkdir($advertiser_dir);}

  $today_dir = $advertiser_dir.'/'.date('Y-m-d');
  if(!file_exists($today_dir)) mkdir($today_dir) ;

  $audio_dir = $today_dir.'/'.'audio';
  if(!file_exists($audio_dir)) mkdir($audio_dir) ;

    //$audio_dir = $today_dir.'/'.'audio';
    //if(!file_exists($audio_dir)) mkdir($audio_dir) ;

    // $csv_dir = $today_dir.'/'.date('Y-m-d_h:i:s').".csv";
    // fopen($csv_dir, 'w'); // for 'write' file access

  echo "\n Files structures are created successfuly......\n";
}
protected function preparePager($advertiser, $filterValues){

  $limit = sfConfig::get('app_call_logs_interactive_last_amount', 0);
  $pager = new TwilioIncomingCallIndexPager('', $limit);
  $pager->init();
  $pager->setPage(0);


  $indexHelper = new TwilioIncomingCallCSVHelper(
    null, $this->user, null, $advertiser->getId()
    );    

  $pager = $indexHelper->retrieveAll($pager, $filterValues);

  $this->indexHelper = $indexHelper;
  $this->pager = $pager;
}
protected function exportCSVFile($advertiser){

  $campaign_dir = 'exports/by_agency/'.$advertiser->getCompany()->getName().'/'.$advertiser->getName();
  $today_dir = $campaign_dir.'/'.date('Y-m-d');
  $csv_file_path = $today_dir.'/'.date('Y-m-d_h:i:s').".csv";

  echo "\n Starting exporting csv file\n";

  $csvGenerator = new CsvGenerator($this->pager, $this->indexHelper);
  $pathToFile     = $csvGenerator->generateFile();
  $outFileName    = $csvGenerator->getFileName();
  copy($pathToFile, $csv_file_path);

  echo "\n CSV file exported successfuly..........\n";

}
protected function exportAudioFiles($advertiser){
  $campaign_dir = 'exports/by_agency/'.$advertiser->getCompany()->getName().'/'.$advertiser->getName();
  $today_dir = $campaign_dir.'/'.date('Y-m-d');
  $audio_dir = $today_dir.'/'.'audio';

  $logTitles = $this->indexHelper->getTitles();
  $rawTitles = array_values($logTitles);
  $rawTitles = array_merge(
    array(''),
    $rawTitles
    );

  $list = $this->pager->getResults();
  $i = $this->pager->getFirstIndice();
  $curlRequest = '';
  foreach ($list as $raw) {
        // $curlRequest = $curlRequest.' -o '.$audio_dir.'/'.$raw['vaId'].'.mp3 '.$raw['recording'];
    if(empty($raw['recording'])){
      $i++;
      continue;
    }
    if($raw['vaId'] == 'Undefined')
      $curlRequest = 'curl -o "'.$audio_dir.'/Undefined'.$i.'.mp3" '.$raw['recording'];
    else
      $curlRequest = 'curl -o "'.$audio_dir.'/'.$raw['vaId'].'.mp3" '.$raw['recording'];
    exec($curlRequest);
  }


  echo "\n Audio files exported successfuly..........\n";
}

protected function parseDateRange($dateRange){
  list($from, $to) = split(' - ', $dateRange);
  $to = isset($to) ? $to : $this->to;

  $dateRangeFromTo = array('from' => strtotime($from), 'to' => strtotime($to));
  return $dateRangeFromTo;
}
}