<?php

class ExportCampaignTask extends sfBaseTask
{
  protected function configure()
  {
    // // add your own arguments here
    // $this->addArguments(array(
    //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
    // ));

    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      new sfCommandOption('audio', null, sfCommandOption::PARAMETER_OPTIONAL, 'Export Audio files', 'false'),
      // add your own options here
    ));

    $this->namespace        = '';
    $this->name             = 'export-campaign';
    $this->briefDescription = '';
    $this->detailedDescription = 'Export specific campaign';
  }

  protected function execute($arguments = array(), $options = array())
  {
    // initialize the database connection
    $databaseManager = new sfDatabaseManager($this->configuration);
    $connection = $databaseManager->getDatabase($options['connection'])->getConnection();
    
    date_default_timezone_set('UTC');
    
    $campaignsIds = array(86, 88, 100, 108, 109, 115, 164, 176);
    $this->exportAudio = array_key_exists('audio', $options) ? $options['audio'] : 'false';

    foreach( $campaignsIds as $id){
        echo "\n &&&&&&&&&&& Campaign:".$id." &&&&&&&&&&& \n";
        $campaign = CampaignTable::findById($id);
        if(empty($campaign)){
          echo "\n No such a campaign with this id\n.....................................\n";
          continue;
        }
        $this->preparePager($campaign);
        $this->createCampaignFilesStructure($campaign);
        $this->exportCSVFile($campaign);

        if($this->exportAudio == 'true'){
          echo "\n Retrieving Audio files \n";
          $this->exportAudioFiles($campaign);
        }
        echo "\n &&&&&&&&&&& End of Campaign:".$id." &&&&&&&&&&& \n";
    }
    
  }

  protected function createCampaignFilesStructure($campaign){
    $campaign_id = $campaign->getId();
    $campaign_dir = 'exports/by_campaign/'.$campaign_id;
    if(!file_exists($campaign_dir)) mkdir($campaign_dir) ;

    $today_dir = $campaign_dir.'/'.date('Y-m-d');
    if(!file_exists($today_dir)) mkdir($today_dir) ;

    $audio_dir = $today_dir.'/'.'audio';
    if(!file_exists($audio_dir)) mkdir($audio_dir) ;

    $csv_dir = $today_dir.'/'.date('Y-m-d').".csv";
    fopen($csv_dir, 'w'); // for 'write' file access

    echo "\n Files structures are created successfuly......\n";
  }
protected function preparePager($campaign){
    $user = sfGuardUserTable::findById(14);

    $nowTimestamp = TimeConverter::getUnixTimestamp();
    $cleanedValues = array(
        'to' => $nowTimestamp,
    );
    if (!empty($beginTimestamp)) {
        $cleanedValues['from'] = $beginTimestamp;
    }


    $limit = sfConfig::get('app_call_logs_interactive_last_amount', 0);
    $pager = new TwilioIncomingCallIndexPager('', $limit);
    $pager->init();
    $pager->setPage(0);

    echo "\n ............................\n";
    
    $indexHelper = new TwilioIncomingCallCSVHelper(
            $campaign->getId(), $user
        );    

    $pager = $indexHelper->retrieveAll($pager, $cleanedValues);

    $this->indexHelper = $indexHelper;
    $this->pager = $pager;
  }
  protected function exportCSVFile($campaign){
    $campaign_id = $campaign->getId();

    $campaign_dir = 'exports/by_campaign/'.$campaign_id;
    $today_dir = $campaign_dir.'/'.date('Y-m-d');
    $csv_file_path = $today_dir.'/'.date('Y-m-d').".csv";

    echo "\n Starting exporting csv file\n";
   
    $csvGenerator = new CsvGenerator($this->pager, $this->indexHelper);
    $pathToFile     = $csvGenerator->generateFile();
    $outFileName    = $csvGenerator->getFileName();
    copy($pathToFile, $csv_file_path);

   echo "\n CSV file exported successfuly..........\n";

  }
  protected function exportAudioFiles($campaign){
    $campaign_id = $campaign->getId();
    $campaign_dir = 'exports/by_campaign/'.$campaign_id;
    $today_dir = $campaign_dir.'/'.date('Y-m-d');
    $audio_dir = $today_dir.'/'.'audio';

    $logTitles = $this->indexHelper->getTitles();
    $rawTitles = array_values($logTitles);
    $rawTitles = array_merge(
            array(''),
            $rawTitles
    );

    $list = $this->pager->getResults();
    $i = $this->pager->getFirstIndice();
    $curlRequest = '';
    foreach ($list as $raw) {
        // $curlRequest = $curlRequest.' -o '.$audio_dir.'/'.$raw['vaId'].'.mp3 '.$raw['recording'];
      $curlRequest = 'curl -o '.$audio_dir.'/'.$raw['vaId'].'.mp3 '.$raw['recording'];
      exec($curlRequest);
        $i++;
    }

    //exec('curl'.$curlRequest);

    echo "\n Audio files exported successfuly..........\n";
  }
 
}
