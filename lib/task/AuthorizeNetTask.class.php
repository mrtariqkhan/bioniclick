<?php
require_once dirname(__FILE__).'/../../config/ProjectConfiguration.class.php';

class AuthorizeNetTask extends BionicTask {

    const COMMENT_SUM_BEFORE= 'BEFORE: ';
    const COMMENT_SUM_SHOULD= 'SHOULD: ';
    const COMMENT_SUM_DURING= 'DURING: ';
    const COMMENT_SUM_AFTER = 'AFTER: ';

    const COMMENT_PROCESS_NEW       = ' + ';
    const COMMENT_PROCESS_NEW_ERROR = '!+ ';

    public function __construct(sfEventDispatcher $dispatcher, sfFormatter $formatter) {

        $this->setName('authorize_net');
        $this->setNameSmall('auth');
        $this->setNameSection('authorize');

        parent::__construct($dispatcher, $formatter);
    }

    protected function configure() {

        parent::configure();

        $this->addOptions(array(
        ));

        $this->briefDescription = 'Creates Authorize.Net Customer Profiles for necessary companies and advertisers';
        $this->detailedDescription = <<<EOF
The [auth|INFO] creates Authorize.Net Customer Profiles for necessary companies and advertisers

Example:
    [php symfony bionic:auth|INFO]
EOF
;
    }

    protected function execute($arguments = array(), $options = array()) {

        parent::execute($arguments, $options);

        $types = array(
            'company' => array(
                'tableName' => 'Company',
                'plural'    => 'companies',
            ),
            'advertiser' => array(
                'tableName' => 'Advertiser',
                'plural'    => 'advertisers',
            ),
        );

        foreach ($types as $singular => $typeInfo) {
            $tableName  = $typeInfo['tableName'];
            $plural     = $typeInfo['plural'];

            $this->createNecessaryAuthorizeCustomerProfiles($tableName, $singular, $plural);
        }
    }

    protected function createNecessaryAuthorizeCustomerProfiles($tableName, $singular, $plural) {

        $this->logSectionNamed('----------------------------------------------------------------');
        $this->logSectionNamed("Add necessary Authorize.Net Customer Profiles to $plural.");

        $table = Doctrine::getTable($tableName);

        $objectsAll     = $table->findBionicDirectChildByCustomerProfile(null);
        $objectsCorrect = $table->findBionicDirectChildByCustomerProfile(false);
        $objectsToUpdate= $table->findBionicDirectChildByCustomerProfile(true);

        $this->logSectionNamed(self::COMMENT_SUM_BEFORE . "{$objectsAll->count()} $plural which should have Authorize.Net Customer Profile at all.");
        $this->logSectionNamed(self::COMMENT_SUM_BEFORE . "{$objectsCorrect->count()} $plural have Authorize.Net Customer Profile already.");
        $this->logSectionNamed(self::COMMENT_SUM_BEFORE . "{$objectsToUpdate->count()} $plural have no Authorize.Net Customer Profile yet.");
        $this->log('');

        $amountSuccess = 0;
        foreach ($objectsToUpdate as $object) {
            $objectInfo = "$singular (id={$object->getId()}, name='{$object->getName()}')";
            try {
                $authorizeCustomerProfileId = AuthorizeNet::createCustomerProfile(
                    AuthorizeNet::OBJECT_TYPE_CODE_COMPANY,
                    $object->getId()
                );
                $object->setAuthorizeCustomerProfileId($authorizeCustomerProfileId);
                $object->save();
                $amountSuccess++;

                $this->logSectionNamed(self::COMMENT_PROCESS_NEW . "Authorize.Net Customer Profile has been successfully created for $objectInfo");
            } catch (Exception $e) {
                $this->logSectionNamed(self::COMMENT_PROCESS_NEW_ERROR . "Authorize.Net Customer Profile hasn't been created for $objectInfo");
            }
        }

        $this->logSectionNamed(self::COMMENT_SUM_DURING . "{$amountSuccess} $plural have Authorize.Net Customer Profile now.");
        $this->log('');


        $objectsAll     = $table->findBionicDirectChildByCustomerProfile(null);
        $objectsCorrect = $table->findBionicDirectChildByCustomerProfile(false);
        $objectsToUpdate= $table->findBionicDirectChildByCustomerProfile(true);

        $this->logSectionNamed(self::COMMENT_SUM_AFTER . "{$objectsAll->count()} $plural which should have Authorize.Net Customer Profile at all.");
        $this->logSectionNamed(self::COMMENT_SUM_AFTER . "{$objectsCorrect->count()} $plural have Authorize.Net Customer Profile already.");
        $this->logSectionNamed(self::COMMENT_SUM_AFTER . "{$objectsToUpdate->count()} $plural have no Authorize.Net Customer Profile yet.");

        $this->log('');
        $this->log('');
    }
}
