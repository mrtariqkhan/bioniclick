<?php

abstract class BionicTask extends sfBaseTask {

    const NAME_PREFIX = 'bionic';

    protected $name         = '';
    protected $nameSmall    = '';
    protected $nameSection  = '';

    protected function configure() {

//        //NOTE:: add your own arguments here
//        $this->addArguments(array(
//            new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
//        ));

        $this->addOptions(array(
            new sfCommandOption('application'   , null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'backend'),
            new sfCommandOption('env'           , null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection'    , null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
            // add your own options here
        ));

        $this->namespace        = self::NAME_PREFIX;
        $this->name             = $this->getName();

        $nameSmall = $this->getNameSmall();
        if (!empty($nameSmall)) {
            $this->aliases      = array(self::NAME_PREFIX . ':' . $nameSmall);
        }
    }

    protected function execute($arguments = array(), $options = array()) {

        $configuration = ProjectConfiguration::getApplicationConfiguration(
            $options['application'],
            $options['env'],
            isset($debug) ? $debug : true
        );
        sfContext::createInstance($configuration);

        $this->exitIfMust();

        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection = $databaseManager->getDatabase(
            $options['connection']
            ? $options['connection']
            : null
        )->getConnection();
    }

    protected function exitIfMust() {

        $taskMayBeExecuted = sfConfig::get('app_task_may_be_executed', false);
        $taskMayBeExecuted = ($taskMayBeExecuted == 'true') ? true : false;
        if (!$taskMayBeExecuted) {
            $this->logSectionNamed('');
            $this->logSectionNamed('! Task cannot be executed yet. Please, execute it later.');
            $this->logSectionNamed('');
            die;
        }
    }

    protected function getBooleanOption($options, $name, $default = false) {

        if (array_key_exists($name, $options)) {
            return ($options[$name] === 'true') ? true : false;
        }

        return $default;
    }

    public function logSectionNamed($message, $size = null, $style = 'INFO') {
        $this->logSection($this->getNameSection(), $message, $size, $style);
    }

    public function logBlockNamed($messages, $style = 'INFO') {

        if (!is_array($messages)) {
            $messages = array($messages);
        }

        $messagesToLog = array();
        foreach ($messages as $key => $message) {
            $keyStr = is_string($key) ? "          $key" : '     ';
            $messagesToLog[] = "{$this->getNameSection()} $keyStr: $message";
        }
        $this->logBlock($messagesToLog, $style);
    }

    public function getName() {
        return $this->name;
    }
    protected function setName($name) {
        $this->name = $name;
    }

    public function getNameSmall() {
        return $this->nameSmall;
    }
    protected function setNameSmall($nameSmall) {
        $this->nameSmall = $nameSmall;
    }

    public function getNameSection() {
        return $this->nameSection;
    }
    protected function setNameSection($nameSection) {
        $this->nameSection = $nameSection;
    }
}