<?php

class BuyVoIpNumbersTask extends BionicTask {

    const COMMENT_SUM_BEFORE= 'BEFORE: ';
    const COMMENT_SUM_SHOULD= 'SHOULD: ';
    const COMMENT_SUM_DURING= 'DURING: ';
    const COMMENT_SUM_AFTER = 'AFTER: ';
    const COMMENT_EXCEPTION = '';

    const COMMENT_PROCESS       = '... ';
    const COMMENT_PROCESS_NEW_FROM_BIONIC_MOVE  = '  ~> BIONIC: ';
    const COMMENT_PROCESS_NEW_FROM_VOIP_BUY     = ' $~> VOIP SERVER: ';
    const COMMENT_PROCESS_NEW_FROM_VOIP_MOVE    = '  ~> VOIP SERVER: ';
    const COMMENT_PROCESS_NEW_FROM_BIONIC_ERR   = ' !~> BIONIC: ';
    const COMMENT_PROCESS_NEW_FROM_VOIP_ERR     = ' !~> VOIP SERVER: ';
    const COMMENT_PROCESS_STATUS_CLOSED_OLD     = '  - OLD ~> CLOSED: ';
    const COMMENT_PROCESS_STATUS_CLOSED_WRONG   = '  - WRONG ~> CLOSED: ';

    /**
     * If
     *  true - sucessfully fulfilled
     *  false - fail
     *  null - there is no need to fulfill some order.
     * @var boolean, null 
     */
    protected $isResultSuccess = null;


    public function __construct(sfEventDispatcher $dispatcher, sfFormatter $formatter) {

        $this->setName('buy_numbers');
        $this->setNameSmall('bn');
        $this->setNameSection('buy numbers');
        parent::__construct($dispatcher, $formatter);
    }

    public function getIsResultSuccess() {
        return $this->isResultSuccess;
    }
    
    protected function configure() {

        parent::configure();

        $this->addOptions(array(
            new sfCommandOption('url'               , null, sfCommandOption::PARAMETER_OPTIONAL, 'Voice url for new incoming numbers'),
            new sfCommandOption('friendlyName'      , null, sfCommandOption::PARAMETER_OPTIONAL, 'Friendly name for new incoming numbers'),
            new sfCommandOption('id'                , null, sfCommandOption::PARAMETER_OPTIONAL, 'id of order'),
            new sfCommandOption('do_use_voip'       , null, sfCommandOption::PARAMETER_OPTIONAL, 'true or false. True if do use VoIP new phone number to fulfill order.', true),
            new sfCommandOption('do_use_binp'       , null, sfCommandOption::PARAMETER_OPTIONAL, 'true or false. True if do use BionicIncomingNumberPool to fulfill order.', true),
            new sfCommandOption('desired_tipn_ids'  , null, sfCommandOption::PARAMETER_OPTIONAL, '(1,4,5)desired twilio_incoming_phone_number_ids to fulfill the order which have the highest priority.', null),
        ));

        $this->briefDescription = 'Buys numbers from BionicIncomingNumberPool or from VoIP server';
        $this->detailedDescription = <<<EOF
The [BuyVoIpNumbers|INFO] task finds PURCHASE or PURCAHSE-AND-ASSIGN application with need if it is set or next otherwise.
Call it with:

[php symfony BuyVoIpNumbers|INFO]

For example:
        [php symfony bionic:buy_numbers --url=http://fairdevteam.dyndns.org:8010/backend_dev.php/incomingCalls/create --friendlyName=testing|INFO]
    or
        [php symfony bionic:bn --url=http://fairdevteam.dyndns.org:8010/backend_dev.php/incomingCalls/create --friendlyName=testing            |INFO]
        [php symfony bionic:bn --url=http://project3.bionicclick.com/backend_dev.php/incomingCalls/create    --friendlyName=production         |INFO]
        [php symfony bionic:bn --url=http://project3.bionicclick.com/backend_dev.php/incomingCalls/create    --friendlyName=production --id=2,4|INFO]
EOF;

    }

    protected function execute($arguments = array(), $options = array()) {

        $this->isResultSuccess = false;

        parent::execute($arguments, $options);

        $id                     = array_key_exists('id', $options) ? $options['id'] : null;
        $desiredTipnIdsStr      = array_key_exists('desired_tipn_ids', $options) ? $options['desired_tipn_ids'] : '';
        $doUseVoipPhoneNumbers  = $this->getBooleanOption($options, 'do_use_voip', true);
        $doUseBionicPool        = $this->getBooleanOption($options, 'do_use_binp', true);
        $desiredTipnIds = 
            (!is_string($desiredTipnIdsStr) || empty($desiredTipnIdsStr)) 
            ? array() 
            : explode(',', $desiredTipnIdsStr)
        ;

        $this->preExecuteBuy($id);

        //NOTE:: get applications to purchase IncomingNumbers from Queue (IncomingNumbersQueue-table)
        $application = IncomingNumbersQueueTable::findForHandling($id);
        if (empty($application)) {
            $this->isResultSuccess = null;
        } else {
            $application->setIsBlocked(false);
        
            $neededNumbersCount = $application->getNotDoneAmount();

            $this->logSectionNamed(self::COMMENT_SUM_DURING . "Application: {$application->getId()}.");
            $this->logSectionNamed(self::COMMENT_SUM_DURING . "Status: {$application->getStatusFullString()}.");

            $neededNumbersCount = $this->closeApplicationIfNeed($arguments, $options, $application, $neededNumbersCount);

            //NOTE:: an attempt to satisfy application using purchased VoIP IncomingPhoneNumbers related to BionicIncomingNumberPool
            if ($neededNumbersCount > 0) {
                if ($doUseBionicPool) {
                    $neededNumbersCount = $this->moveFromBionicPool(
                        $arguments, 
                        $options, 
                        $application, 
                        $neededNumbersCount, 
                        $desiredTipnIds
                    );
                }

                //NOTE:: if BionicIncomingNumberPool has not enough numbers then purchase new numbers from VoIP server
                if ($neededNumbersCount > 0) {
                    if ($doUseVoipPhoneNumbers) {
                        $neededNumbersCount = $this->moveFromVoIPServer(
                            $arguments, 
                            $options, 
                            $application, 
                            $neededNumbersCount
                        );
                    } else {
                        $this->logSectionNamed(self::COMMENT_SUM_DURING . "Options are so that task cannot use VoIP phone numbers.");
                    }
                }
            }

            $application = $this->markApplicationAsDoneIfNeed($arguments, $options, $application, $neededNumbersCount);

            $this->logSectionNamed(self::COMMENT_SUM_DURING . "Status: {$application->getStatusFullString()}.");
            $this->log('');

            $this->isResultSuccess = true;
        }

        $this->postExecuteBuy();
    }

    protected function closeApplicationIfNeed($arguments, $options, $application, $neededNumbersCount) {

        $nowTimestamp = TimeConverter::getUnixTimestamp();

        /**
        * Period of time. When this period after creation of statement expires 
        * Bionic sets its status = true and won't handle it in the future
        * @var integer
        */
        $intervalSeconds      = sfConfig::get('app_task_register_numbers_interval_seconds', 60);

        if ($neededNumbersCount <= 0 || $application->getCreatedDate() + $intervalSeconds < $nowTimestamp) {
            $application->setStatus(IncomingNumbersQueueTable::STATUS_TYPE_CLOSED)
                ->save()
            ;
            $msg = ($neededNumbersCount <= 0)
                ? self::COMMENT_PROCESS_STATUS_CLOSED_WRONG
                    . 'Amounts of numbers are wrong in application. Status has been changed to Closed'
                : self::COMMENT_PROCESS_STATUS_CLOSED_OLD
                    . 'Application is too old. Status has been changed to Closed'
            ;
            $this->logSectionNamed($msg);
            
            return 0;
        }

        return $neededNumbersCount;
    }

    protected function moveFromBionicPool(
        $arguments, 
        $options, 
        $application, 
        $neededNumbersCount, 
        $desiredTipnIds
    ) {

        $localAreaCodeId = $application->getTwilioLocalAreaCodeId();
        $incomingNumbersAvailible = 
            TwilioIncomingPhoneNumberTable::findBionicFreeAvailableNumbersByLocalAreaCodeDesiredOrNot(
                $localAreaCodeId,
                $desiredTipnIds,
                $neededNumbersCount
            )
        ;

        if (empty($incomingNumbersAvailible)) {
            $this->logSectionNamed(self::COMMENT_PROCESS_NEW_FROM_BIONIC_ERR
                . "There is no free phone numbers of such area code in Bionic Pool. "
            );
            $this->logSectionNamed(self::COMMENT_PROCESS_NEW_FROM_BIONIC_ERR
                . "Let's try to register number from VoIP Server"
            );
        }

        //NOTE:: create need AdvertiserIncomingNumberPool
        foreach ($incomingNumbersAvailible as $incomingNumberArr) {
            if ($neededNumbersCount <= 0) {
                break;
            }

            $incomingNumberId = array_key_exists('id', $incomingNumberArr) ? $incomingNumberArr['id'] : null;
            $bionicIncomingNumberPool = BionicIncomingNumberPoolTable::findByIncomingPhoneNumberId(
                $incomingNumberId
            );
            try {
                AdvertiserIncomingNumberPoolTable::moveNumberToAdvertiserFromBionicByOrder(
                    $application,
                    $bionicIncomingNumberPool
                );
                $neededNumbersCount--;
                $this->logSectionNamed(self::COMMENT_PROCESS_NEW_FROM_BIONIC_MOVE 
                    . 'Phone number has been moved'
                );
            } catch (Exception $e) {
                $this->logSectionNamed(self::COMMENT_PROCESS_NEW_FROM_BIONIC_ERR 
                    . "Phone number has not been moved. Let's try to register number from VoIP Server"
                );
                break;
            }
        }

        return $neededNumbersCount;
    }

    protected function moveFromVoIPServer($arguments, $options, $application, $neededNumbersCount) {

        /**
        * Max count of failed requests to VoIP server
        * @var integer
        */
        $maxVoipFailsCount    = sfConfig::get('app_task_register_numbers_max_voip_fails_count', 1);

        $url            = array_key_exists('url', $options) ? $options['url'] : null;
        $friendlyName   = array_key_exists('friendlyName', $options) ? $options['friendlyName'] : null;

        if (!empty($url)) {

            $localAreaCode = $application->getTwilioLocalAreaCode();

            $failsCount = 0;
            while ($neededNumbersCount > 0 && $failsCount < $maxVoipFailsCount) {
                try {
                    $newBionicIncomingNumberPool = 
                        TwilioIncomingPhoneNumberTable::buyAndRegisterIncomingNumber(
                            $localAreaCode,
                            $url,
                            $friendlyName
                        )
                    ;

                    if (empty($newBionicIncomingNumberPool)) {
                        throw new Exception('Error while buying and registering new number from VoIP server');
                    }

                    $this->logSectionNamed(self::COMMENT_PROCESS_NEW_FROM_VOIP_BUY 
                        . 'Phone number has been bought, registered'
                    );
                    AdvertiserIncomingNumberPoolTable::moveNumberToAdvertiserFromBionicByOrder(
                        $application,
                        $newBionicIncomingNumberPool
                    );
                    $neededNumbersCount--;
                    $this->logSectionNamed(self::COMMENT_PROCESS_NEW_FROM_VOIP_MOVE 
                        . 'Registered phone number has been moved'
                    );
                } catch (Exception $e) {
                    $failsCount++;
                    $this->logSectionNamed(self::COMMENT_PROCESS_NEW_FROM_VOIP_ERR 
                        . "Phone number has not been bought, registered, moved. Fail #$failsCount"
                    );
                    $this->logSectionNamed(self::COMMENT_PROCESS_NEW_FROM_VOIP_ERR 
                        . "   ({$e->getMessage()})"
                    );
                }
            }

            $this->logSectionNamed(self::COMMENT_SUM_DURING . "Fails: $failsCount.");
        }

        return $neededNumbersCount;
    }

    protected function markApplicationAsDoneIfNeed($arguments, $options, $application, $neededNumbersCount) {

        if ($neededNumbersCount <= 0) {
            $application->setStatus(IncomingNumbersQueueTable::STATUS_TYPE_DONE)
                ->setDoneAmount($application->getAmount())
                ->save()
            ;
            $this->logSectionNamed(self::COMMENT_SUM_DURING . 'Application HAS been done');
        } else {
            $this->logSectionNamed(self::COMMENT_SUM_DURING . 'Application HAS NOT been done');
            $application->refreshDoneAmount();
        }

        return $application;
    }

    protected function preExecuteBuy($id = null) {

        $this->logSectionNamed(TimeConverter::getDateTimeFormattedByString('now', TimeConverter::TIMEZONE_UTC, TimeConverter::FORMAT_ALL));
        $this->log('');

        $state = $this->getStoredState();

        $this->logSectionNamed(self::COMMENT_SUM_BEFORE . "{$state['ALL']           } all applications were at the DB");
        $this->logSectionNamed(self::COMMENT_SUM_BEFORE . "{$state['OPEN']          } open applications were at the DB");
        $this->logSectionNamed(self::COMMENT_SUM_BEFORE . "{$state['IN_PROGRESS']   } in-progress applications were at the DB");
        $this->logSectionNamed(self::COMMENT_SUM_BEFORE . "{$state['DONE']          } done applications were at the DB");
        $this->logSectionNamed(self::COMMENT_SUM_BEFORE . "{$state['CLOSED']        } closed applications were at the DB");

        $this->log('');

        if (!empty($id)) {
            $this->log("An attempt to execute specific order(#$id)");
            $this->log('');
        }

        return $state;
    }

    protected function postExecuteBuy() {

        $state = $this->getStoredState();

        $this->logSectionNamed(self::COMMENT_SUM_AFTER . "{$state['ALL']        } all applications are at the DB");
        $this->logSectionNamed(self::COMMENT_SUM_AFTER . "{$state['OPEN']       } open applications are at the DB");
        $this->logSectionNamed(self::COMMENT_SUM_AFTER . "{$state['IN_PROGRESS']} in-progress applications are at the DB");
        $this->logSectionNamed(self::COMMENT_SUM_AFTER . "{$state['DONE']       } done applications are at the DB");
        $this->logSectionNamed(self::COMMENT_SUM_AFTER . "{$state['CLOSED']     } closed applications are at the DB");

        $this->log('***************************************************************');
        $this->log('');
        $this->log('');

        return $state;
    }

    protected function getStoredState() {

        $state = array(
            'ALL'           => IncomingNumbersQueueTable::calc(IncomingNumbersQueueTable::STATUS_TYPE_ALL),
            'OPEN'          => IncomingNumbersQueueTable::calc(IncomingNumbersQueueTable::STATUS_TYPE_OPEN),
            'IN_PROGRESS'   => IncomingNumbersQueueTable::calc(IncomingNumbersQueueTable::STATUS_TYPE_IN_PROGRESS),
            'DONE'          => IncomingNumbersQueueTable::calc(IncomingNumbersQueueTable::STATUS_TYPE_DONE),
            'CLOSED'        => IncomingNumbersQueueTable::calc(IncomingNumbersQueueTable::STATUS_TYPE_CLOSED),
        );

        return $state;
    }
}