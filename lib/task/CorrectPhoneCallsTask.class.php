<?php

class CorrectPhoneCallsTask extends BionicTask {
    //TODO:: think if works correctly

    const OPTION_CAMPAIGN   = 'campaign';
    const OPTION_ADVERTISER = 'advertiser';
    const OPTION_FROM       = 'from';
    const OPTION_TO         = 'to';
    const OPTION_CORRECT    = 'correct';
    const OPTION_WITHOUT    = 'without_only';

    const OPTION_VALUE_CAMPAIGN_ALL     = 'ALL';
    const OPTION_VALUE_CAMPAIGN_NULL    = 'NULL';

    const OPTION_VALUE_ADVERTISER_ALL   = 'ALL';
    const OPTION_VALUE_ADVERTISER_NULL  = 'NULL';
    
    const DEFAULT_OPTION_VALUE_FROM         = null;
    const DEFAULT_OPTION_VALUE_CORRECT      = false;
    const DEFAULT_OPTION_VALUE_WITHOUT      = false;

    const PREFIX_LENGTH_MAX = 4;


    const NONE                          = '      ';
    const ERROR                         = '    ! ';
    const EXCEPTION                     = '!!!   ';

    const SIGN_OK                       = '.';
    const SIGN_ADD                      = '+';
    const SIGN_MODIFY                   = '~';
    const SIGN_UNKNOWN                  = '?';

    const CODE_ALL                      = 'ALL';
    const CODE_OK                       = '.                      ';
    const CODE_OTHER                    = 'OTHER                  ';
    const CODE_TASK_HAS_UPDATED         = 'TASK HAS REVIEWED      ';
//    const CODE_NON_UPDATED              = 'NON_UPDATED            ';

    const CODE_WRONG_OWNER_WHILE_CALL   = '  REVIEWED BELONG_CALL ';
//    const CODE_WRONG_OWNER_WHILE_VA     = '  WRONG BELONG_VA      ';
    const CODE_WRONG_VA                 = '  REVIEWED VA          ';
    const CODE_WRONG_NEW                = '  REVIEWED NEW         ';
    const CODE_WRONG_OTHER              = '  REVIEWED OTHER       ';
    const CODE_WRONG_CALLS_TO_DEFAULT   = '  REVIEWED CALLS TO DEFAULT ';


    public function __construct(sfEventDispatcher $dispatcher, sfFormatter $formatter) {

        $this->setName('correct_calls');
        $this->setNameSmall('cc');
        $this->setNameSection('correct_calls');

        parent::__construct($dispatcher, $formatter);
    }

    protected function configure() {

        parent::configure();

        $this->addOptions(array(
            new sfCommandOption(
                self::OPTION_CAMPAIGN,
                null,
                sfCommandOption::PARAMETER_OPTIONAL,
                'Owner Campaign Id ("' . self::OPTION_VALUE_CAMPAIGN_ALL. '", "5", "' . self::OPTION_VALUE_CAMPAIGN_NULL. '"). OPTIONAL. Default: NULL.' 
                    . 'Is used when ' .  self::OPTION_WITHOUT . 'is False',
                null
            ),
            new sfCommandOption(
                self::OPTION_ADVERTISER,
                null,
                sfCommandOption::PARAMETER_OPTIONAL,
                'Owner Advertiser Id ("' . self::OPTION_VALUE_ADVERTISER_ALL. '", "5", "' . self::OPTION_VALUE_ADVERTISER_NULL. '"). OPTIONAL. Default: NULL.'
                    . 'Is used when ' .  self::OPTION_WITHOUT . 'is False',
                null
            ),
            new sfCommandOption(
                self::OPTION_FROM,
                null,
                sfCommandOption::PARAMETER_OPTIONAL,
                "UTC from time. OPTIONAL. '. Is used always.",
                null
            ),
            new sfCommandOption(
                self::OPTION_TO, 
                null, 
                sfCommandOption::PARAMETER_OPTIONAL, 
                "UTC to time. OPTIONAL. '. Is used always.", 
                null
            ),
            new sfCommandOption(
                self::OPTION_CORRECT, 
                null, 
                sfCommandOption::PARAMETER_OPTIONAL, 
                "Boolean. True -> correct phoneCalls if need, False -> do not correct just analyse. Default: " . self::DEFAULT_OPTION_VALUE_CORRECT . '. Is used always.', 
                null
            ),
            new sfCommandOption(
                self::OPTION_WITHOUT, 
                null, 
                sfCommandOption::PARAMETER_OPTIONAL, 
                "Boolean. True -> check calls without phoneCalls only, False -> check calls with phoneCall only. Default: " 
                    . self::DEFAULT_OPTION_VALUE_WITHOUT . '. If True -> no matter what values have ' . self::OPTION_CAMPAIGN . ', ' . self::OPTION_ADVERTISER, 
                null
            ),
        ));

        $this->briefDescription = 'Corrects phone calls for campaign.';
        $this->detailedDescription = <<<EOF
The [correct_calls|INFO] 

Parameters from, to, correct can be used in every type of task.
If without_only = True, then no matter what advertiser and campaign are set.

Example:
  1. Analyse calls with phoneCall to Bionic only:
        [php symfony bionic:cc                                                                                    |INFO]
  2. Correct calls with phoneCall to Bionic only:
        [php symfony bionic:cc                                    --correct=true                                  |INFO]
  3. Analyse calls with phoneCall to Bionic only in time ranges:
        [php symfony bionic:cc                                                   --from=1322524800 --to=1323734400|INFO]
  4. Correct calls with phoneCall to Bionic only in time ranges:
        [php symfony bionic:cc                                    --correct=true --from=1322524800 --to=1323734400|INFO]
  5. Analyse calls without phoneCall only:
        [php symfony bionic:cc      --without_only=true                                                           |INFO]
  6. Analyse calls without phoneCall only in time ranges:
        [php symfony bionic:cc      --without_only=true                          --from=1322524800 --to=1323734400|INFO]
  7. Correct calls without phoneCall only:
        [php symfony bionic:cc      --without_only=true           --correct=true                                  |INFO]
  8. Correct calls without phoneCall only in time ranges:
        [php symfony bionic:cc      --without_only=true           --correct=true --from=1322524800 --to=1323734400|INFO]

  9. Correct calls with phoneCall to All advertisers and All campaigns only in time ranges:
        [php symfony bionic:cc --advertiser=ALL --campaign=ALL    --correct=true --from=1322524800 --to=1323734400|INFO]
  10. Correct calls with phoneCall to advertiser and campaign only in time ranges:
        [php symfony bionic:cc --advertiser=1   --campaign=2      --correct=true --from=1322524800 --to=1323734400|INFO]
  11. Correct calls with phoneCall to advertiser only in time ranges:
        [php symfony bionic:cc --advertiser=1                     --correct=true --from=1322524800 --to=1323734400|INFO]

  12. Analyse calls with phoneCall to All advertisers and All campaigns only in time ranges:
        [php symfony bionic:cc --advertiser=ALL --campaign=ALL                   --from=1322524800 --to=1323734400|INFO]
  13. Analyse calls with phoneCall to advertiser and campaign only in time ranges:
        [php symfony bionic:cc --advertiser=1   --campaign=2                     --from=1322524800 --to=1323734400|INFO]
  14. Analyse calls with phoneCall to advertiser only in time ranges:
        [php symfony bionic:cc --advertiser=1                                    --from=1322524800 --to=1323734400|INFO]

EOF
;
    }

    protected function execute($arguments = array(), $options = array()) {

        parent::execute($arguments, $options);

        $timezoneName = TimeConverter::TIMEZONE_UTC;
        $nowTimestamp = TimeConverter::getUnixTimestamp();


        $campaignId = array_key_exists(self::OPTION_CAMPAIGN, $options) && !empty($options[self::OPTION_CAMPAIGN]) 
            ? $options[self::OPTION_CAMPAIGN] 
            : null
        ;
        $advId = array_key_exists(self::OPTION_ADVERTISER, $options) && !empty($options[self::OPTION_ADVERTISER]) 
            ? $options[self::OPTION_ADVERTISER] 
            : null
        ;
        $from = array_key_exists(self::OPTION_FROM, $options) && !empty($options[self::OPTION_FROM])
            ? $options[self::OPTION_FROM] 
            : self::DEFAULT_OPTION_VALUE_FROM
        ;
        $to = array_key_exists(self::OPTION_TO, $options) && !empty($options[self::OPTION_TO])
            ? $options[self::OPTION_TO]
            : $nowTimestamp
        ;
        $replace                = $this->getBooleanOption($options, self::OPTION_CORRECT, self::DEFAULT_OPTION_VALUE_CORRECT);
        $withoutPhoneCallOnly   = $this->getBooleanOption($options, self::OPTION_WITHOUT, self::DEFAULT_OPTION_VALUE_WITHOUT);

        $this->logSectionNamed('==========================================================');
        $this->logSectionNamed(TimeConverter::getDateTime($nowTimestamp, $timezoneName));
        $this->logSectionNamed('');
        $this->logSectionNamed('From : ' . TimeConverter::getDateTime($from, $timezoneName));
        $this->logSectionNamed('To   : ' . TimeConverter::getDateTime($to, $timezoneName));

        if ($withoutPhoneCallOnly) {
            $accumulatedAmounts = $this->checkCallsWithoutPhoneCall($from, $to, $replace);
        } else {
            if (empty($campaignId) || $campaignId == self::OPTION_VALUE_CAMPAIGN_NULL) {
                if (empty($advId) || $advId == self::OPTION_VALUE_ADVERTISER_NULL) {
                    $accumulatedAmounts = $this->checkBionic($from, $to, $replace);
                } elseif ($advId == self::OPTION_VALUE_ADVERTISER_ALL) {
                    $accumulatedAmounts = $this->checkAdvertisers(null, $from, $to, $replace);
                } else {
                    $accumulatedAmounts = $this->checkAdvertisers($advId, $from, $to, $replace);
                }
            } elseif ($campaignId == self::OPTION_VALUE_CAMPAIGN_ALL) {
                $accumulatedAmounts = $this->checkCampaigns(null, $from, $to, $replace);
            } else {
                $accumulatedAmounts = $this->checkCampaigns($campaignId, $from, $to, $replace);
            }
        }

        $this->logSectionNamed('+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++');
        $this->doLogResults($accumulatedAmounts);
    }

    protected function checkCampaigns($campaignId, $from, $to, $replace) {

        $j = 0;
        if (empty($campaignId)) {
            $accumulatedAmounts = $this->initialiseAmounts();
            $campaigns = CampaignTable::getInstance()->findBy('was_deleted', false);
            $this->logSectionNamed("Amount of Objects = " . count($campaigns));
            foreach ($campaigns as $campaign) {
                $newAmounts = $this->checkCallLogs($j++, 0, $campaign, $from, $to, $replace);
                $accumulatedAmounts = $this->accumulateResults($accumulatedAmounts, $newAmounts);
            }
            return $accumulatedAmounts;
        }


        $this->logSectionNamed("Amount of Objects = 1");
        $campaign = CampaignTable::getInstance()->find($campaignId);
        if (empty($campaign) || !$campaign->exists()) {
            throw new Exception('Campaign has not been found.');
        }

        return $this->checkCallLogs($j++, 0, $campaign, $from, $to, $replace);
    }

    protected function checkAdvertisers($advertiserId, $from, $to, $replace) {

        $j = 0;
        if (empty($advertiserId)) {
            $accumulatedAmounts = $this->initialiseAmounts();
            $advertisers = AdvertiserTable::getInstance()->findBy('was_deleted', false);
            $this->logSectionNamed("Amount of Objects = " . count($advertisers));
            foreach ($advertisers as $advertiser) {
                $newAmounts = $this->checkCallLogs(++$j, $advertiser, 0, $from, $to, $replace);
                $accumulatedAmounts = $this->accumulateResults($accumulatedAmounts, $newAmounts);
            }
            return $accumulatedAmounts;
        }


        $this->logSectionNamed("Amount of Objects = 1");
        $advertiser = AdvertiserTable::getInstance()->find($advertiserId);
        if (empty($advertiser) || !$advertiser->exists()) {
            throw new Exception('Advertiser has not been found.');
        }

        return $this->checkCallLogs($j++, $advertiser, 0, $from, $to, $replace);
    }

    protected function checkBionic($from, $to, $replace) {

        $this->logSectionNamed("Amount of Objects = 1");
        return $this->checkCallLogs(1, null, null, $from, $to, $replace);
    }

    protected function checkCallsWithoutPhoneCall($from, $to, $replace) {
        return $this->checkCallLogs(1, null, null, $from, $to, $replace, true);
    }
    
    protected function checkCallLogs($j, $advertiser, $campaign, $from, $to, $replace = false, $withoutPhoneCallOnly = false) {

        $this->logSectionNamed('');
        $this->logSectionNamed('');
        $this->logSectionNamed('-------------------------------------------------------------------------');
        $this->logSectionNamed('Task works with actual calls only.');
        $timezoneName = TimeConverter::TIMEZONE_UTC;

        $prefix = str_pad("#$j ", self::PREFIX_LENGTH_MAX);

        $advertiserId   = null;
        $campaignId     = null;
        if ($withoutPhoneCallOnly) {
            $this->logSectionNamed("$prefix Calls without phoneCall only.");
        } else {
            if (empty($advertiser)) {
                if (empty($campaign)) {
                    $this->logSectionNamed("$prefix Bionic");
                } else {
                    $campaignId     = $campaign->getId();
                    $advertiserId   = $campaign->getAdvertiserId();
                    $this->logSectionNamed("$prefix Campaign <id = $campaignId> $campaign");
                }
            } else {
                $advertiserId = $advertiser->getId();
                $this->logSectionNamed("$prefix Advertiser <id = $advertiserId> $advertiser");

                if (!empty($campaign)) {
                    $campaignId = $campaign->getId();
                } else {
                    $campaignId = 0;
                }
            }
        }

        if ($withoutPhoneCallOnly) {
            $tics = TwilioIncomingCallTable::findWithoutPhoneCall(
                $from,
                $to
            );
        } else {
            $tics = TwilioIncomingCallTable::findPhoneCallsActualWithPhoneCall(
                $advertiserId,
                $campaignId,
                $from,
                $to
            );
        }

        $amountAll = count($tics);
        $this->logSectionNamed("$prefix Amount: $amountAll");
        $this->logSectionNamed("$prefix");

        $amounts = $this->initialiseAmounts();
        $amounts[self::CODE_ALL] = $amountAll;
        $i = 0;
        foreach ($tics as $twilioIncomingCall) {
            $i++;

            $ticId          = $twilioIncomingCall->getId();
            $ticStartTime   = $twilioIncomingCall->getStartTime();

            $tS = empty($ticStartTime) ? '' : TimeConverter::getDateTime($ticStartTime, $timezoneName);
            $this->logSectionNamed($prefix . self::NONE . "#$i. Call: <$ticId> $tS");

            try {
                $info = PhoneCallTable::createOrUpdateByActualTwilioIncomingCall($twilioIncomingCall, $replace);

                $type = $info[PhoneCallTable::CREATE_PHONE_CALL_INFO_CORRECTION_TYPE];
                switch ($type) {
                    case PhoneCallTable::CREATE_PHONE_CALL_TYPE_NEW:
                        $sign = self::SIGN_ADD;
                        $amounts[self::CODE_TASK_HAS_UPDATED]++;
                        $amounts[self::CODE_WRONG_NEW]++;
                        break;
                    case PhoneCallTable::CREATE_PHONE_CALL_TYPE_MODIFIED:
                        $sign = self::SIGN_MODIFY;
                        $amounts[self::CODE_TASK_HAS_UPDATED]++;
                        break;
                    case PhoneCallTable::CREATE_PHONE_CALL_TYPE_CORRECT:
                        $sign = self::SIGN_OK;
                        $amounts[self::CODE_OK]++;
                        break;
                    default:
                        $sign = self::SIGN_UNKNOWN;
                        $amounts[self::CODE_OTHER]++;
                }
                if ($replace) {
                    $this->logSectionNamed($prefix . self::NONE . "#$i.   $sign");
                } else {
                    $this->logSectionNamed($prefix . self::NONE . "#$i. ( $sign )");
                }

                $new = $info[PhoneCallTable::CREATE_PHONE_CALL_INFO_UPDATED];
                switch ($type) {
                    case PhoneCallTable::CREATE_PHONE_CALL_TYPE_MODIFIED:

                        $errors     = $info[PhoneCallTable::CREATE_PHONE_CALL_INFO_ERRORS];
                        $old        = $info[PhoneCallTable::CREATE_PHONE_CALL_INFO_OLD];

                        foreach ($errors as $errorIndex => $error) {
                            switch ($error) {
                                case PhoneCallTable::CREATE_PHONE_CALL_ERROR_OWNER_WHILE_CALL_ADV:
                                    $code = self::CODE_WRONG_OWNER_WHILE_CALL;
                                    $comments = "Related Adv old: <{$this->getArrValue('advertiser_id', $old)}> need: <{$this->getArrValue('advertiser_id', $new)}>";
                                    break;
                                case PhoneCallTable::CREATE_PHONE_CALL_ERROR_OWNER_WHILE_CALL_CAM:
                                    $code = self::CODE_WRONG_OWNER_WHILE_CALL;
                                    $comments = "Related Cam old: <{$this->getArrValue('campaign_id', $old)}> need: <{$this->getArrValue('campaign_id', $new)}>";
                                    break;
                                case PhoneCallTable::CREATE_PHONE_CALL_ERROR_VA:
                                    $code = self::CODE_WRONG_VA;
                                    $comments = "Related VA old: <{$this->getArrValue('visitor_analytics_id', $old)}> need: <{$this->getArrValue('visitor_analytics_id', $new)}>";
                                    break;
                                case PhoneCallTable::CREATE_PHONE_CALL_ERROR_TO_DEFAULT:
                                    $code = self::CODE_WRONG_CALLS_TO_DEFAULT;
                                    $comments = "If Call was to Default: <{$this->getArrValue('phone_number_was_default', $old)}> need: <{$this->getArrValue('phone_number_was_default', $new)}>";
                                    break;
                                default:
                                    $code = self::CODE_WRONG_OTHER;
                                    $comments = '';
                            }

                            $amounts[$code]++;

                            $errorIndex++;
                            $this->logSectionNamed($prefix . self::ERROR . "#$i.     Error $errorIndex: $error");
                            $this->logSectionNamed($prefix . self::NONE  . "#$i.                ($comments)");
                        }
                        break;
                    default:
                        $comments = "Adv <{$this->getArrValue('advertiser_id', $new)}>, Cam <{$this->getArrValue('campaign_id', $new)}>, VA <{$this->getArrValue('visitor_analytics_id', $new)}>";
                        $this->logSectionNamed($prefix . self::NONE . "#$i.     ($comments)");
                        break;
                }

            } catch (Exception $e) {
                $this->logSectionNamed($prefix . self::EXCEPTION . "#$i.   Error. PhoneCall has NOT been reviewed:");
                $this->logSectionNamed($prefix . self::EXCEPTION . "          {$e->getMessage()}");
                $this->logSectionNamed("$prefix");
            }
        }

        $this->doLogResults($amounts, $prefix);

        return $amounts;
    }

    protected function doLogResults($amounts, $prefix = '') {

        $amountAll = $amounts[self::CODE_ALL];
        $okAmounts = $amounts[self::CODE_OK];
        $updatedAmounts = $amounts[self::CODE_TASK_HAS_UPDATED];
        $wrongAmounts = $amountAll - $okAmounts;
        
        $this->logSectionNamed($prefix);
        $this->logSectionNamed("$prefix Amount: $amountAll");
        $this->logSectionNamed("$prefix Amount Of 'OK': $okAmounts");
        $this->logSectionNamed("$prefix Amount Of '" . self::CODE_TASK_HAS_UPDATED . "': $updatedAmounts (from $wrongAmounts)");
        if ($wrongAmounts > 0) {
            $wrongAmounts = $amounts;
            unset($wrongAmounts[self::CODE_OK]);
            unset($wrongAmounts[self::CODE_ALL]);
            unset($wrongAmounts[self::CODE_TASK_HAS_UPDATED]);
            foreach ($wrongAmounts as $code => $amount) {
                if ($amount > 0) {
                    $this->logSectionNamed("$prefix Amount Of '$code': $amount");
                }
            }
        }
        $this->logSectionNamed('');
    }

    protected function initialiseAmounts() {

        return array(
            self::CODE_ALL                      => 0,
            self::CODE_OK                       => 0,
            self::CODE_WRONG_OWNER_WHILE_CALL   => 0,
            self::CODE_WRONG_VA                 => 0,
            self::CODE_WRONG_CALLS_TO_DEFAULT   => 0,
            self::CODE_WRONG_NEW                => 0,
            self::CODE_OTHER                    => 0,
            self::CODE_TASK_HAS_UPDATED         => 0,
        );
    }

    protected function accumulateResults($accumulatedAmounts, $newPartOfAmounts) {

        foreach ($accumulatedAmounts as $code => $accumulatedAmount) {
            $accumulatedAmounts[$code] += $newPartOfAmounts[$code];
        }

        return $accumulatedAmounts;
    }

    protected function getArrValue($key, $searcharray, $default = '') {
        return array_key_exists($key, $searcharray) ? $searcharray[$key] : $default;
    }
}
