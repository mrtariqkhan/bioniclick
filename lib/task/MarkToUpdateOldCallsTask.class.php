<?php

class MarkToUpdateOldCallsTask extends BionicTask {

    public function __construct(sfEventDispatcher $dispatcher, sfFormatter $formatter) {

        $this->setName('mark_to_update_old_calls');
        $this->setNameSmall('mark_up_old');
        $this->setNameSection('mark_up_old');

        parent::__construct($dispatcher, $formatter);
    }

    protected function configure() {

        parent::configure();

        $this->addOptions(array(
//            new sfCommandOption('from'      , null, sfCommandOption::PARAMETER_OPTIONAL, "UTC from time. OPTIONAL", null),
        ));

        $this->briefDescription = 'Updates twilio incoming calls which have not been updated yet.';
        $this->detailedDescription = <<<EOF
The [correct_calls|INFO] 

Example:
        [php symfony bionic:mark_up_old|INFO]
EOF
;
    }

    protected function execute($arguments = array(), $options = array()) {

        parent::execute($arguments, $options);

        $this->logSectionNamed('===========================================================');
        $this->logSectionNamed(TimeConverter::getDateTimeFormattedByString('now', TimeConverter::TIMEZONE_UTC, TimeConverter::FORMAT_ALL));


        $oldTicsInProgress = TwilioIncomingCallTable::findOldNotUpdatedInProgressCalls();
        $this->markOldNotUpdatedCalls($oldTicsInProgress, 'old not-updated IN-PROGRESS');
        $this->logSectionNamed('');

        $this->logSectionNamed('-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --');
        $oldTicsCompleted = TwilioIncomingCallTable::findOldNotUpdatedCalls();
        $this->markOldNotUpdatedCalls($oldTicsCompleted, 'old not-updated');
        $this->logSectionNamed('');

        $this->logSectionNamed('');
    }

    protected function markOldNotUpdatedCalls($oldTics, $callsType) {

        $amountAll = count($oldTics);
        $this->logSectionNamed("Amount of $callsType calls to MARK for updating: $amountAll");

        $amountSuccess = 0;
        foreach ($oldTics as $i => $oldTic) {
            if ($this->markCallToBeUpdated($i, $oldTic)) {
                $amountSuccess++;
            }
        }
        $this->logSectionNamed("$amountSuccess (from $amountAll) $callsType calls have been successfully marked for updating.");
        
    }

    protected function markCallToBeUpdated($i, TwilioIncomingCall $oldTic) {
        try {
            $account = TwilioAccountTable::getInstance()->findDefaultAccount();

            $method = TwilioIncomingCallTable::METHOD_POST;
            $callId     = $oldTic->getId();
            $startTime  = TimeConverter::getDateTime($oldTic->getStartTime(), TimeConverter::TIMEZONE_UTC);
            $callSid    = $oldTic->getSid();
            $accountSid = $account->getSid();

            $this->logSectionNamed("   $i). call <startTime = $startTime id = $callId, Sid = $callSid>.");

            $twilioParams = array(
                TwilioIncomingCallTable::PARAMETER_CALL_SID         => $callSid,
                TwilioIncomingCallTable::PARAMETER_CALL_ACCOUNT_SID => $accountSid,
                TwilioIncomingCallTable::PARAMETER_CALL_STATUS      => TwilioIncomingCallTable::STATUS_COMPLETED,
            );
            TwilioIncomingCallTable::processTwilioCallSummary($method, $twilioParams);

            $this->logSectionNamed("        call has been marked for updating.");
        } catch (Exception $e) {
            $this->logSectionNamed("  !!    call has NOT been marked for updating.");
            return false;
        }
        return true;
    }
}
