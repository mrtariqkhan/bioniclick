<?php

class ExportByAdvertiserTask extends sfBaseTask
{
  protected function configure()
  {
    // // add your own arguments here
    // $this->addArguments(array(
    //   new sfCommandArgument('my_arg', sfCommandArgument::REQUIRED, 'My argument'),
    // ));

    $this->addOptions(array(
      new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name'),
      new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
      new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
      // add your own options here
      new sfCommandOption('ids', null, sfCommandOption::PARAMETER_OPTIONAL, 'The Advertiserd ids', ''),
      new sfCommandOption('audio', null, sfCommandOption::PARAMETER_OPTIONAL, 'Export Audio files', 'false'),
    ));

    $this->namespace        = '';
    $this->name             = 'export-campaigns-by-advertiser';
    $this->briefDescription = '';
    $this->detailedDescription = 'Export all campaigns associated with specific advertisers';
  }

  protected function execute($arguments = array(), $options = array())
  {
    // initialize the database connection
    $databaseManager = new sfDatabaseManager($this->configuration);
    $connection = $databaseManager->getDatabase($options['connection'])->getConnection();
    
    date_default_timezone_set('UTC');

    $ids = array_key_exists('ids', $options) ? $options['ids'] : '';
    $this->exportAudio = array_key_exists('audio', $options) ? $options['audio'] : 'false';

    if(empty($ids) || $ids == ''){
      $ids = "10,14,19,27,28,30,32,38,39,56";
      echo "\nNo advertisers ids detected";
      echo "\ndefault advertisers ids for Aquisio company will be used";
    }

    
    $advertiserIds = split(',', $ids);

    //super admin user
    $this->user = sfGuardUserTable::findById(14);

    foreach( $advertiserIds as $id){
        $advertiser = AdvertiserTable::findById(intval($id));
        // $profiles = sfGuardUserProfileTable::findAllByAdvertisers(array($advertiser));
        // foreach( $profiles as $profile){
        //   $user = $profile->getUser();
        //   if($user->getWasDeleted() == 0){
        //     $this->user = $profile->getUser();
        //     break;
        //   }
        // }
        if(empty($advertiser)){
            echo "\nNo such an advertiser with this id:".$id;
            continue;
        }
        if(empty($this->user)){
            echo "\nNo such user for advertiser with this id:".$id;
            continue;
        }

        echo "\n ----- Exporting Advertiser:".$advertiser->getName()." -----";
        $this->createAdvertiserFilesStructure($advertiser);
        echo "\n ----- Advertiser:".$advertiser->getName()." file structure was created successfully-----";

        $this->preparePager($advertiser);

        $this->exportCSVFile($advertiser);
        echo "\n ----- Advertiser:".$advertiser->getName()." csv file was exported successfully-----";

        if($this->exportAudio == 'true'){
          echo "\n ----- Advertiser:".$advertiser->getName()." exporting audio files-----";
          $this->exportAudioFiles($advertiser);
        }
    }
    echo "\n ----- Exporting Completed-----";
  }
  protected function createAdvertiserFilesStructure($advertiser){
    $advertiser_dir = 'exports/by_advertiser/'.$advertiser->getName();
    if(!file_exists($advertiser_dir)){mkdir($advertiser_dir);}

    $today_dir = $advertiser_dir.'/'.date('Y-m-d');
    if(!file_exists($today_dir)) mkdir($today_dir) ;

    $audio_dir = $today_dir.'/'.'audio';
    if(!file_exists($audio_dir)) mkdir($audio_dir) ;

    // $csv_dir = $today_dir.'/'.date('Y-m-d_h:i:s').".csv";
    // fopen($csv_dir, 'w'); // for 'write' file access

    echo "\n Files structures are created successfuly......\n";
  }
protected function preparePager($advertiser){

    $nowTimestamp = TimeConverter::getUnixTimestamp();
    $cleanedValues = array(
        'to' => $nowTimestamp,
    );
    if (!empty($beginTimestamp)) {
        $cleanedValues['from'] = $beginTimestamp;
    }


    $limit = sfConfig::get('app_call_logs_interactive_last_amount', 0);
    $pager = new TwilioIncomingCallIndexPager('', $limit);
    $pager->init();
    $pager->setPage(0);

    
    $indexHelper = new TwilioIncomingCallCSVHelper(
            null, $this->user, null, $advertiser->getId()
        );    

    $pager = $indexHelper->retrieveAll($pager, $cleanedValues);

    $this->indexHelper = $indexHelper;
    $this->pager = $pager;
  }
  protected function exportCSVFile($advertiser){

    $campaign_dir = 'exports/by_advertiser/'.$advertiser->getName();
    $today_dir = $campaign_dir.'/'.date('Y-m-d');
    $csv_file_path = $today_dir.'/'.date('Y-m-d_h:i:s').".csv";

    echo "\n Starting exporting csv file\n";
   
    $csvGenerator = new CsvGenerator($this->pager, $this->indexHelper);
    $pathToFile     = $csvGenerator->generateFile();
    $outFileName    = $csvGenerator->getFileName();
    copy($pathToFile, $csv_file_path);

   echo "\n CSV file exported successfuly..........\n";

  }
  protected function exportAudioFiles($advertiser){
    $campaign_dir = 'exports/by_advertiser/'.$advertiser->getName();
    $today_dir = $campaign_dir.'/'.date('Y-m-d');
    $audio_dir = $today_dir.'/'.'audio';

    $logTitles = $this->indexHelper->getTitles();
    $rawTitles = array_values($logTitles);
    $rawTitles = array_merge(
            array(''),
            $rawTitles
    );

    $list = $this->pager->getResults();
    $i = $this->pager->getFirstIndice();
    $curlRequest = '';
    foreach ($list as $raw) {
        // $curlRequest = $curlRequest.' -o '.$audio_dir.'/'.$raw['vaId'].'.mp3 '.$raw['recording'];
      if(empty($raw['recording'])){
        $i++;
        continue;
      }
      if($raw['vaId'] == 'Undefined')
        $curlRequest = 'curl -o "'.$audio_dir.'/Undefined'.$i.'.mp3" '.$raw['recording'];
      else
        $curlRequest = 'curl -o "'.$audio_dir.'/'.$raw['vaId'].'.mp3" '.$raw['recording'];
      exec($curlRequest);
    }


    echo "\n Audio files exported successfuly..........\n";
  }
 
}
