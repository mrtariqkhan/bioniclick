<?php


class BionicIncomingNumberPoolTable extends Doctrine_Table {
    
    public static function getInstance() {
        return Doctrine_Core::getTable('BionicIncomingNumberPool');
    }

    public static function findCompanyByIncomingPhoneNumberId($incomingPhoneNumberId) {

        $bionicIncomingNumberPool = BionicIncomingNumberPoolTable::findByIncomingPhoneNumberId($incomingPhoneNumberId, true);
        if (!empty($bionicIncomingNumberPool)) {
            return CompanyTable::getBionic();
        }

        return null;
    }

    public static function findByIncomingPhoneNumberId($incomingPhoneNumberId, $isOwner = null) {

        $query = self::getInstance()->createQuery()
            ->andWhere('twilio_incoming_phone_number_id = ' . $incomingPhoneNumberId)
        ;

        if (!is_null($isOwner)) {
            $query->andWhere('is_owner = ', $isOwner);
        }

        $companyIncomingNumber = $query->fetchOne();

        return $companyIncomingNumber;
    }

    public static function calcAvailibleToPurchaseNumbers() {
        return self::findCountByType(null, null);
    }

    public static function findCountByType($type = null, $isOwner = true) {

        $query = self::getInstance()->createQuery('binp')
            ->innerJoin('binp.TwilioIncomingPhoneNumber tipn')
            ->innerJoin('tipn.TwilioLocalAreaCode tlac')
        ;

        if (!is_null($type)) {
            $query->andWhere('tlac.phone_number_type = ?', $type);
        }

        if (!is_null($isOwner)) {
            $query->andWhere("is_owner = $isOwner");
        }

        return $query->count();
    }

    /**
     *
     * @return integer id of default number for campaign
     */
    public static function findRandomDefaultNumber() {

        $query = self::getInstance()->createQuery('bipn')
            ->andWhere('bipn.is_owner = 1')
        ;

        return $query->fetchOne();
    }
}