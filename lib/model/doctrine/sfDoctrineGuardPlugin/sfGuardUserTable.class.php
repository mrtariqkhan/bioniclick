<?php

class sfGuardUserTable extends PluginsfGuardUserTable {
    
    /**
     * Creates an instance of the Table class
     * @return Doctrine_Table
     */
    public static function getInstance() {
        return Doctrine_Core::getTable('sfGuardUser');
    }

    /**
     * Finds and returns user by ID
     * @param Integer $id ID of the user
     * @return sfGuardUser user found
     */
    public static function findById($id) {
        return self::getInstance()->findOneBy('id', $id);
    }

    /**
     * Finds and returns user by username
     * @param string $username username
     * @return sfGuardUser user found
     */
    public static function findByUsername($username) {
        return self::getInstance()->findOneBy('username', $username);
    }

    public static function findSuperAdmin() {
        return self::getInstance()->findOneBy('is_super_admin', true);
    }

    protected static function createBranchIndexQuery(sfGuardUser $user, $filterValues = array()) {

        //$id = $user->getId();

        $query = self::getInstance()->createQuery('user')            
            ->leftJoin('user.Profile profile')
            ->addOrderBy('first_name ASC')
            ->addOrderBy('last_name ASC')
            //->andWhere("id != $id")
        ;

        $wasDeletedType = WasDeletedHelper::getType($user);
        if ($wasDeletedType != WasDeletedHelper::TYPE_ALL) {
            $query->andWhere('user.was_deleted = ?', $wasDeletedType);
        }

        $simpleFilterFieldNames = array(
            BranchHelper::SIMPLE_FILTER_FIELD_TYPE_STRING => array(
                'first_name'    => 'first_name',
                'last_name'     => 'last_name',
                'email_address' => 'email_address',
                'username'      => 'username',
                'is_active'     => 'is_active',
                'last_login'    => 'last_login',
            ),
        );
        $branchHelper = new BranchHelperUser();
        $query = $branchHelper->createQueryBranch($query, $user, $filterValues, $simpleFilterFieldNames);

        return $query;
    }

    public static function createIndexQuery(sfGuardUser $user, $filterValues = array()) {

        if ($user->getIsSuperAdmin()) {
            $query = self::createBranchIndexQuery($user, $filterValues);
            
        } else {
            $query = self::getInstance()->createQuery('user')
                ->andWhere(false)
            ;
        }

        return $query;
    }

    public static function createCompanySubCompaniesIndexQuery(sfGuardUser $user, $filterValues = array()) {

        $query = self::createBranchIndexQuery($user, $filterValues);

        //NOTE:: this condition is to escape my company but not escape advertisers of my company.
        $companyAlias = BranchHelperUser::ALIAS_COM;
        $company = $user->getCompany();
        $needCompanyLevel = $company->getLevel() + 1;
        $query
            ->andWhere("$companyAlias.id IS NOT NULL")
            ->andWhere("$companyAlias.level = {$needCompanyLevel}")
        ;

        return $query;
    }

    public static function createCompanySubAdvertisersIndexQuery(sfGuardUser $user, $filterValues = array()) {

        $query = self::createBranchIndexQuery($user, $filterValues);
        $companyId = $user->getFirstCompanyId();

        //NOTE:: this condition is to escape my company but not escape advertisers of my company.
        $companyAlias = BranchHelperUser::ALIAS_ADV_COM;
        $company = $user->getCompany();
        $query
            ->andWhere("$companyAlias.id = $companyId")
        ;

        return $query;
    }

    public static function createAdvertiserIndexQuery(sfGuardUser $user) {

        $advertiserId = $user->getAdvertiserId();

        $query = self::getInstance()->createQuery('user')
            ->andWhere('user.was_deleted = 0')
            ->innerJoin('user.Profile profile')
            ->andWhere("profile.advertiser_id = $advertiserId")
        ;

        return $query;
    }
    
    public static function findByAdvertiserId($advertiserId) {
        return self::getInstance()->createQuery('user')
            ->andWhere('user.was_deleted = 0')
            ->innerJoin('user.Profile profile')
            ->andWhere("profile.advertiser_id = $advertiserId")
            ->execute();
    }

    public static function createCompanyIndexQuery(sfGuardUser $user) {

        $companyId = $user->getCompanyId();

        $query = self::getInstance()->createQuery('user')
            ->andWhere('user.was_deleted = 0')
            ->innerJoin('user.Profile profile')
            ->andWhere("profile.company_id = $companyId")
        ;

        return $query;
    }

    public static function getAdminByCompany($company) {

        $companyId = $company->getId();
        $level = $company->getLevel();

        return self::getAdminByCompanyOrAdvertiser($level, $companyId);
    }

    public static function getAdminByAdvertiser($advertiser) {

        $advertiserId = $advertiser->getId();
        $level = $advertiser->getLevel();

        return self::getAdminByCompanyOrAdvertiser($level, $advertiserId);
    }

    /**
     *
     * @param type $level one of CompanyTable::$LEVEL_*
     * @param type $id id of company or advertiser
     * @return type
     */
    protected static function getAdminByCompanyOrAdvertiser($level, $id) {

        $groupName = sfGuardGroupTable::getGroupNameByLevel($level, sfGuardGroupTable::SUB_NAME_ROLE_ADMIN);

        $query = self::getInstance()->createQuery('u')
            ->innerJoin('u.Profile p')
            ->innerJoin('u.Groups g')
            ->andWhere('g.name = ?', $groupName)
            ->andWhere("u.was_deleted = ?", WasDeletedHelper::TYPE_NON_DELETED)
        ;

        if ($level == CompanyTable::$LEVEL_ADVERTISER) {
            $query->andWhere('p.advertiser_id = ?', $id);
        } else {
            $query->andWhere('p.company_id = ?', $id);
        }

        $admin = $query->fetchOne();

        return $admin;
    }

    public static function ifHasAdvertiserAdmins($editingUserId, $advertiserId, $newGroupsList = array()) {
        return self::ifHasCustomerAdmins($editingUserId, $newGroupsList, $advertiserId, 'advertiser_id', false);
    }

    public static function ifHasCompanyAdmins($editingUserId, $companyId, $newGroupsList = array()) {
        return self::ifHasCustomerAdmins($editingUserId, $newGroupsList, $companyId, 'company_id', true);
    }

    protected static function ifHasCustomerAdmins($editingUserId, $newGroupsList, $id, $fieldName, $isCompany) {

        $groupName = sfGuardGroupTable::SUB_NAME_ROLE_ADMIN;
        $value = mysql_escape_string($groupName);

        $query = self::getInstance()->createQuery('user')
            ->andWhere('user.was_deleted = 0')
            ->innerJoin('user.Profile profile')
            ->innerJoin('user.Groups group')
            ->andWhere("profile.$fieldName = $id")
            ->andWhere("group.name LIKE '%$value%'")
            ->andWhere("user.id != $editingUserId")
        ;

        $count = $query->count();

        if ($count <= 0 && !empty($newGroupsList)) {
            $adminGroupsIds = sfGuardGroupTable::findAdminGroupIdsForCompany($id, $isCompany);

            $curUserWillBeAdmin = false;
            foreach ($newGroupsList as $newGroupId) {
                if (false !== array_search($newGroupId, $adminGroupsIds)) {
                    $curUserWillBeAdmin = true;
                    break;
                }
            }

            if ($curUserWillBeAdmin) {
                $count = 1;
            }
        }
        return ($count > 0);
    }

    public static function ifWillHaveAdmin($adminUser, $editingUserId, $newGroupsList = array()) {

        if ($adminUser->isAdvertiserUser()) {
            $advertiserId = $adminUser->getAdvertiserId();
            return sfGuardUserTable::ifHasAdvertiserAdmins($editingUserId, $advertiserId, $newGroupsList);
        }

        $companyId = $adminUser->getCompanyId();
        return sfGuardUserTable::ifHasCompanyAdmins($editingUserId, $companyId, $newGroupsList);
    }
    
    public static function retrieveByUsernameWithWasDeleted($username_or_email) {
        return self::getInstance()->createQuery('u')
            ->andWhere('u.username = ? OR u.email_address = ?', array($username_or_email, $username_or_email))
            ->andWhere('u.was_deleted = 0')
            ->fetchOne()
        ;        
    }
    
    public static function createQueryExisting() {
        return self::getInstance()->createQuery('u')
            ->andWhere('u.was_deleted = 0')
        ;
    }
    
    public static function findByCompanyId($companyId) {
        return self::getInstance()->createQuery('u')
            ->innerJoin('u.Profile p')
            ->andWhere('p.company_id = ?', $companyId)
            ->andWhere('u.was_deleted = 0')
            ->execute()
        ;
    }

    public static function findIfDuplicateByEmail($email, $id = null) {

        $query = self::getInstance()->createQuery()
            ->andWhere("email_address = '$email'")
            ->andWhere(WasDeletedHelper::FIELD_NAME . ' = ' . WasDeletedHelper::TYPE_NON_DELETED)
        ;

        if (!empty($id)) {
            $query->andWhere("id != $id");
        }

        return $query->fetchOne();
    }

    public static function findIfDuplicateByUsername($username, $id = null) {

        $query = self::getInstance()->createQuery()
            ->andWhere("username = '$username'")
            ->andWhere(WasDeletedHelper::FIELD_NAME . ' = ' . WasDeletedHelper::TYPE_NON_DELETED)
        ;

        if (!empty($id)) {
            $query->andWhere("id != $id");
        }

        return $query->fetchOne();
    }
}
