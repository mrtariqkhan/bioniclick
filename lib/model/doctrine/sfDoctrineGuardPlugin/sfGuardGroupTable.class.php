<?php

class sfGuardGroupTable extends PluginsfGuardGroupTable {

    const SUB_NAME_LEVEL_BIONIC     = 'Bionic';
    const SUB_NAME_LEVEL_RESELLER   = 'Reseller';
    const SUB_NAME_LEVEL_AGENCY     = 'Agency';
    const SUB_NAME_LEVEL_ADVERTISER = 'Advertiser';

    const SUB_NAME_ROLE_ADMIN   = 'Admin';
    const SUB_NAME_ROLE_MANAGER = 'Manager';
    const SUB_NAME_ROLE_USER    = 'User';


    public static function getInstance() {
        return Doctrine_Core::getTable('sfGuardGroup');
    }

    public static function createIndexQuery() {

        $query = self::getInstance()->createQuery()
            ->addOrderBy('name ASC')
        ;

        return $query;
    }

    /**
     *
     * @param string $subNameLevel SUB_NAME_LEVEL_*
     * @param string $subNameRole SUB_NAME_ROLE_*
     * @return string
     */
    public static function getGroupName($subNameLevel, $subNameRole) {
        return "$subNameLevel $subNameRole";
    }

    /**
     *
     * @param int $level company level
     * @param string $subNameRole SUB_NAME_ROLE_*
     * @return string
     */
    public static function getGroupNameByLevel($level, $subNameRole) {

        $level = intval($level);
        $subNameLevel = '';

        switch ($level) {
            case CompanyTable::$LEVEL_BIONIC:
                $subNameLevel = self::SUB_NAME_LEVEL_BIONIC;
                break;

            case CompanyTable::$LEVEL_RESELLER:
                $subNameLevel = self::SUB_NAME_LEVEL_RESELLER;
                break;

            case CompanyTable::$LEVEL_AGENCY:
                $subNameLevel = self::SUB_NAME_LEVEL_AGENCY;
                break;

            case CompanyTable::$LEVEL_ADVERTISER:
                $subNameLevel = self::SUB_NAME_LEVEL_ADVERTISER;
                break;

            default:
                $subNameLevel = '';
        }

        return self::getGroupName($subNameLevel, $subNameRole);
    }

    public static function getRoleSubNames() {

        $className = get_class();
        $reflectionClass = new ReflectionClass($className);
        $classConstants = $reflectionClass->getConstants();

        $subNames = array();
        foreach ($classConstants as $classConstantName => $constantName) {
            if (preg_match('/^SUB_NAME_ROLE_(.*)$/', $classConstantName, $matches)) {
                $subNames[] = $constantName;
            }
        }

        return $subNames;
    }

    public static function createQuerySubGroupsByParentUser(sfGuardUser $parentUser) {

        $query = self::getInstance()->createQuery();

        if (!$parentUser->getIsSuperAdmin()) {
            $level = $parentUser->getLevel();
            switch ($level) {
                case CompanyTable::$LEVEL_BIONIC:
                    $subname = self::SUB_NAME_LEVEL_BIONIC;
                    $valueSubName = mysql_escape_string($subname);
                    $query->andWhere("name LIKE '%$valueSubName%'");
                    break;
                case CompanyTable::$LEVEL_RESELLER:
                    $subname = self::SUB_NAME_LEVEL_RESELLER;
                    $valueSubName = mysql_escape_string($subname);
                    $query->andWhere("name LIKE '%$valueSubName%'");
                    break;
                case CompanyTable::$LEVEL_AGENCY:
                    $subname = self::SUB_NAME_LEVEL_AGENCY;
                    $valueSubName = mysql_escape_string($subname);
                    $query->andWhere("name LIKE '%$valueSubName%'");
                    break;
                case CompanyTable::$LEVEL_ADVERTISER:
                    $subname = self::SUB_NAME_LEVEL_ADVERTISER;
                    $valueSubName = mysql_escape_string($subname);
                    $query->andWhere("name LIKE '%$valueSubName%'");
                    break;
                default:
                    $query->andWhere('false');
            }

            $subGroupSubNames = self::getSubGroupSubNames($parentUser->getGroupNames());
            if (empty($subGroupSubNames)) {
                $query->andWhere('false');
            } else {
                $orConditions = array();
                foreach ($subGroupSubNames as $subGroupSubName) {
                    $valueSubGroupSubName= mysql_escape_string($subGroupSubName);
                    $orConditions[] = "name LIKE '%$valueSubGroupSubName%'";
                }
                $condition = implode(' OR ', $orConditions);
                $query->andWhere($condition);
            }
        }

        return $query;
    }

    public static function findAdminGroupForCompany($id, $isCompany = false) {

        if (empty($id)) {
            return array();
        }

        $id = intval($id);
        if ($isCompany) {
            $company = CompanyTable::getInstance()->findOneBy('id', $id);
        } else {
            $company = AdvertiserTable::getInstance()->findOneBy('id', $id);
        }

        $query = self::getInstance()->createQuery();

        $level = $company->getLevel();
        switch ($level) {
            case CompanyTable::$LEVEL_BIONIC:
                break;
            case CompanyTable::$LEVEL_RESELLER:
                $subname = self::SUB_NAME_LEVEL_RESELLER;
                $valueSubName = mysql_escape_string($subname);
                $query->andWhere("name LIKE '%$valueSubName%'");
                break;
            case CompanyTable::$LEVEL_AGENCY:
                $subname = self::SUB_NAME_LEVEL_AGENCY;
                $valueSubName = mysql_escape_string($subname);
                $query->andWhere("name LIKE '%$valueSubName%'");
                break;
            case CompanyTable::$LEVEL_ADVERTISER:
                $subname = self::SUB_NAME_LEVEL_ADVERTISER;
                $valueSubName = mysql_escape_string($subname);
                $query->andWhere("name LIKE '%$valueSubName%'");
                break;
            default:
                $query->andWhere('false');
        }

        $subGroupSubNames = array(
            self::SUB_NAME_ROLE_ADMIN,
        );
        if (empty($subGroupSubNames)) {
            $query->andWhere('false');
        } else {
            $andConditions = array();
            foreach ($subGroupSubNames as $subGroupSubName) {
                $valueSubGroupSubName= mysql_escape_string($subGroupSubName);
                $andConditions[] = "name LIKE '%$valueSubGroupSubName%'";
            }
            $condition = implode(' AND ', $andConditions);
            $query->andWhere($condition);
        }

        $groups = $query->execute();
        return $groups;
    }

    public static function findAdminGroupIdsForCompany($id, $isCompany = false) {

        $groups = self::findAdminGroupForCompany($id, $isCompany);

        $groupsIds = array();
        foreach ($groups as $group) {
            $groupsIds[] = $group->getId();
        }

        return $groupsIds;
    }

    public static function getSubGroupSubNames($groupNames) {

        $subGroupSubNames = array();

        if (empty($groupNames)) {
            return $subGroupSubNames;
        }

        $subNames = self::getRoleSubNames();

        foreach ($groupNames as $groupName) {

            foreach ($subNames as $subName) {
                if (preg_match("/(.*)$subName(.*)/", $groupName, $matches)) {

                    switch ($subName) {
                        case self::SUB_NAME_ROLE_ADMIN:
                            $subGroupSubNames[] = self::SUB_NAME_ROLE_ADMIN;
                            $subGroupSubNames[] = self::SUB_NAME_ROLE_MANAGER;
                            $subGroupSubNames[] = self::SUB_NAME_ROLE_USER;
                            break;
                        case self::SUB_NAME_ROLE_MANAGER:
                            $subGroupSubNames[] = self::SUB_NAME_ROLE_USER;
                            break;
                        case self::SUB_NAME_ROLE_USER:
                            break;
                        default:
                    }

                    break;
                }
            }

        }

        return $subGroupSubNames;
    }
}