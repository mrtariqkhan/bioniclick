<?php

class CompanyTable extends Doctrine_Table {

    public static $LEVEL_BIONIC     = 0;
    public static $LEVEL_RESELLER   = 1;
    public static $LEVEL_AGENCY     = 2;
    public static $LEVEL_ADVERTISER = 3;

    public static $LEVEL_NAME_BIONIC     = 'bionic';
    public static $LEVEL_NAME_RESELLER   = 'reseller';
    public static $LEVEL_NAME_AGENCY     = 'agency';
    public static $LEVEL_NAME_ADVERTISER = 'advertiser';
    public static $LEVEL_NAME_ALL        = 'all';

    const SKIN_DEFAULT = 'green';

    
    public static function getInstance() {
        return Doctrine_Core::getTable('Company');
    }

    public static function getBionic() {
        $query = CompanyTable::getInstance()->createQuery()
            ->andWhere('level = ?', self::$LEVEL_BIONIC);
        return $query->fetchOne();
    }

    public static function getBionicId() {
        $bionic = self::getBionic();
        if (empty($bionic)) {
            throw new Exception('There are no Bionic company.');
        }
        return $bionic->getId();
    }

    public static function findById($id) {
        return self::getInstance()->findOneBy('id', $id);
    }

    public static function findCompanyLevelById($id) {
        return self::findById($id)->getCompanyLevelName();
    }

    public static function findAllByExport(){
        $query = CompanyTable::getInstance()->createQuery('com')
            ->where('com.export = ?', 1);
        return $query->execute();
    }
    /**
     *
     * @param integer $userId user's id
     * @return Company user's company
     */
    public static function findCompanyByUserId($userId) {

        $query = CompanyTable::getInstance()->createQuery('com')
            ->innerJoin('com.Profiles p')
            ->where('p.user_id = ?', $userId);

        return $query->fetchOne();
    }

    public static function getLevels() {
        return array(
            CompanyTable::$LEVEL_BIONIC     => CompanyTable::$LEVEL_NAME_BIONIC,
            CompanyTable::$LEVEL_RESELLER   => CompanyTable::$LEVEL_NAME_RESELLER,
            CompanyTable::$LEVEL_AGENCY     => CompanyTable::$LEVEL_NAME_AGENCY,
            CompanyTable::$LEVEL_ADVERTISER => CompanyTable::$LEVEL_NAME_ADVERTISER,
        );
    }

    /**
     *
     * @param integer $level one from CompanyTable::$LEVEL_*
     * @return string one of $LEVEL_NAME_*
     */
    public static function getLevelName($level) {

        $levels = self::getLevels();

        if (array_key_exists($level, $levels)) {
            return $levels[$level];
        }
        return self::$LEVEL_NAME_ALL;
    }

    public static function getLevelByName($levelName) {

        $levels = self::getLevels();

        $level = array_search($levelName, $levels);
        if ($level === false) {
            return -1;
        }
        return $level;
    }

    public static function createQueryIndex(sfGuardUser $user, $filterValues) {

        $query = self::getInstance()->createQuery('com');

        if ($user->getIsSuperAdmin()) {
            $query->addOrderBy('level, name ASC');
        } else {
            $query = self::createQueryCompanyCustomersFirstIndex($user, $query);
        }

        $wasDeletedType = WasDeletedHelper::getType($user);
        if ($wasDeletedType != WasDeletedHelper::TYPE_ALL) {
            $query->andWhere("was_deleted = ?", $wasDeletedType);
        }


        $simpleFilterFieldNames = array(
            BranchHelper::SIMPLE_FILTER_FIELD_TYPE_STRING => array(
                'name'                  => 'name',
                'url'                   => 'url',
                'default_phone_number'  => 'default_phone_number',
            ),
        );
        $branchHelperCompany = new BranchHelperCompany();
        $query = $branchHelperCompany->createQueryBranch($query, $user, $filterValues, $simpleFilterFieldNames);

        return $query;
    }

    public static function createQueryFirstByParentCompanyAndUser(sfGuardUser $user, Company $company, $filterValues = array()) {

        $query = self::getInstance()->createQuery('com')
            ->andWhere("lft BETWEEN {$company->getLft()} AND {$company->getRgt()}")
            ->andWhere("level = {$company->getLevel()} + 1")
            ->addOrderBy('level, name ASC')
        ;

        $wasDeletedType = WasDeletedHelper::getType($user);
        if ($wasDeletedType != WasDeletedHelper::TYPE_ALL) {
            $query->andWhere("was_deleted = ?", $wasDeletedType);
        }


        $simpleFilterFieldNames = array(
            BranchHelper::SIMPLE_FILTER_FIELD_TYPE_STRING => array(
                'name'                  => 'name',
                'url'                   => 'url',
                'default_phone_number'  => 'default_phone_number',
            ),
        );
        $branchHelperCompany = new BranchHelperCompany();
        $query = $branchHelperCompany->createQueryBranch($query, $user, $filterValues, $simpleFilterFieldNames);

        return $query;
    }

    protected static function createQueryCompanyCustomersFirstIndex(sfGuardUser $user, $query) {

        if ($user->isAdvertiserUser()) {
            $query->andWhere('false');
        } else {
            $company = $user->getCompany();

            $query
                ->andWhere("lft BETWEEN {$company->getLft()} AND {$company->getRgt()}")
                ->andWhere("level = {$company->getLevel()} + 1")
            ;
        }

        return $query;
    }

    public static function findAgencies($lft = null, $rgt = null, $wasDeleted = null) {
        return self::findSubCompanies(
            CompanyTable::$LEVEL_AGENCY,
            $lft,
            $rgt,
            $wasDeleted
        );
    }

    public static function findResellers($lft = null, $rgt = null, $wasDeleted = null) {
        return self::findSubCompanies(
            CompanyTable::$LEVEL_RESELLER,
            $lft,
            $rgt,
            $wasDeleted
        );
    }

    /**
     *
     * @param type $level one of CompanyTable::$LEVEL_*
     * @param type $lft
     * @param type $rgt
     * @param type $wasDeleted
     * @return type 
     */
    protected static function findSubCompanies($level, $lft = null, $rgt = null, $wasDeleted = null) {

        $query = self::getInstance()->createQuery()
            ->andWhere('level = ?', $level)
            ->addOrderBy('level, name ASC')
        ;

        if (!empty($lft) && !empty($rgt)) {
            $query->andWhere("lft BETWEEN $lft AND $rgt");
        }

        if (!is_null($wasDeleted)) {
            $query->andWhere("was_deleted = ?", $wasDeleted);
        }

        return $query->execute();
    }

    public static function findSubCompaniesFirstForInvoice($company, $from, $to) {

        $level = null;
        switch ($company->getLevel()) {
            case CompanyTable::$LEVEL_BIONIC:
                $level = CompanyTable::$LEVEL_RESELLER;
                break;
            case CompanyTable::$LEVEL_RESELLER:
                $level = CompanyTable::$LEVEL_AGENCY;
                break;
            default:
                return array();
        }

        $query = self::getInstance()->createQuery('com')
            ->andWhere("com.level = $level")
            ->andWhere("com.lft BETWEEN {$company->getLft()} AND {$company->getRgt()}")
            ->andWhere(
                "(
                    (
                            was_deleted = 0
                        AND UNIX_TIMESTAMP(created_at) <= $to
                    ) OR (
                        was_deleted = 1 
                        AND (
                                UNIX_TIMESTAMP(created_at) <= $to
                            AND UNIX_TIMESTAMP(updated_at) >= $from 
                        )
                    )
                )"
            )
        ;

        return $query->execute();
    }

    public static function createQueryByParentCompany($parentCompany) {

        $id = $parentCompany->getId();
        $lft = $parentCompany->getLft();
        $rgt = $parentCompany->getRgt();

        $query = self::getInstance()->createQuery('com')
            ->andWhere("com.lft BETWEEN $lft AND $rgt")
            ->andWhere("com.id != $id")
        ;

        return $query;
    }

    public static function findFirstIndex(sfGuardUser $user, Company $company, $filterValues = array()) {

        $query = self::createQueryFirstByParentCompanyAndUser($user, $company, $filterValues);

        return $query->execute();
    }

    public static function findLevelById($id) {

        $company = self::getInstance()->find($id);

        return $company->getLevel();
    }

    public static function getBranchCompaniesRecursive(Company $company, &$parentCompanies) {

        $level = $company->getLevel();

        if ($company->hasParentCompany()) {
            $parentCompany = $company->getParentCompany();
            self::getBranchCompaniesRecursive($parentCompany, $parentCompanies);
        }

        $parentCompanies[$level] = $company;
    }

    /**
     *
     * @param array $advertisersIds 
     * @return array ('id' => $companyId) for all $advertisersIds
     */
    public static function findOrderedByAdvertiserIds($advertisersIds) {

        $result = array();

        if (!empty($advertisersIds)) {
            $query = self::getInstance()->createQuery('com')
                ->innerJoin('com.Advertisers adv')
                ->andWhereIn('adv.id', $advertisersIds)
            ;

            $companies = $query->execute();

            if (!empty($companies)) {
                foreach ($companies as $company) {
                    $result[$company->getId()] = $company;
                }
            }
        }

        return $result;
    }

    /**
     * Fot task
     * @param type $isCustomerProfileEmpty true, false, null
     * @return type 
     */
    public static function findBionicDirectChildByCustomerProfile($isCustomerProfileEmpty) {

        $query = self::getInstance()->createQuery()
            ->andWhere('level = ' . self::$LEVEL_RESELLER)
            ->andWhere('was_deleted=?', WasDeletedHelper::TYPE_NON_DELETED)
        ;

        if ($isCustomerProfileEmpty) {
            $query->andWhere('authorize_customer_profile_id IS NULL');
        } elseif ($isCustomerProfileEmpty === false) {
            $query->andWhere('authorize_customer_profile_id IS NOT NULL');
        } else {

        }

        return $query->execute();
    }

    public static function findBionicDirectChildNotDebtors() {

        $query = self::getInstance()->createQuery()
            ->andWhere('level = ' . self::$LEVEL_RESELLER)
            ->andWhere('is_debtor = false')
            ->andWhere('was_deleted=?', WasDeletedHelper::TYPE_NON_DELETED)
        ;

        return $query->execute();
    }

    public static function declareSubtreeIsDebtor(Company $parentCompany, $isDebtor = true) {

        $querySubCompanies = self::getInstance()->createQuery('com')
            ->andWhere("(lft BETWEEN {$parentCompany->getLft()} AND {$parentCompany->getRgt()}) OR id = {$parentCompany->getId()}")
            ->andWhere("was_deleted = ?", WasDeletedHelper::TYPE_NON_DELETED)
        ;
        $companies = $querySubCompanies->execute();


        $querySubCompanies
            ->set('is_debtor', $isDebtor)
            ->update()
        ;
        $querySubCompanies->execute();


        //TODO:: think why query does not contain aliases. So MySQL cannot find what is_debtor
        // it should set, who is owner of lft field.
//        $querySubAdvertisers = AdvertiserTable::createQueryByParentCompany($parentCompany)
//            ->set('adv.is_debtor', $isDebtor)
//            ->update()
//        ;
        $querySubAdvertisers = AdvertiserTable::createQueryByParentCompany($parentCompany);
        $advertisers = $querySubAdvertisers->execute();
        foreach ($advertisers as $advertiser) {
            $advertiser->declareIsDebtor();
        }

        return array(
            'company'   => $companies,
            'adveriser' => $advertisers,
        );
    }

    /**
     *
     * @param type $obj Company or Advertiser
     */
    public static function isDirectBionicChild($obj) {

        $level = $obj->getLevel();
        switch ($level) {
            case CompanyTable::$LEVEL_RESELLER:
                return true;
            case CompanyTable::$LEVEL_ADVERTISER:
                $parentCompany = $obj->getCompany();
                $parentLevel = empty($parentCompany) ? null : $parentCompany->getLevel();
                return ($parentLevel == CompanyTable::$LEVEL_BIONIC);
            default:
                return false;
        }
    }
}
