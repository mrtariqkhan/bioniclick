<?php


class PhysicalPhoneNumberTable extends Doctrine_Table {
    
    public static function getInstance() {
        return Doctrine_Core::getTable('PhysicalPhoneNumber');
    }

    public static function getOneByCampaignId($campaignId) {
        return self::createQueryAllByCampaignId($campaignId)->fetchOne();
    }

    public static function createQueryAllByCampaignId($campaignId) {

        return self::getInstance()->createQuery()
            ->andWhere('campaign_id=' . $campaignId)
            ->andWhere('was_deleted = ' . WasDeletedHelper::TYPE_NON_DELETED)
        ;
    }
    
    public static function findByCampaignId($campaignId) {
        return self::createQueryAllByCampaignId($campaignId)->execute();
    }
    
    /**
     * @param integer campaignId id of physicalPhoneNumber's campaign
     * @return <Doctrine_Query> query to select collection of PhysicalPhoneNumbers
     */
    public static function getIndexQuery($campaignId) {
        return self::createQueryAllByCampaignId($campaignId);
    }   

    public static function findByComboAsArray($combo, $limit = null) {

        $page = $combo->getPage();
        $campaignId = $page->getCampaignId();

        $query = self::getInstance()->createQuery()
            ->select('id')
            ->addSelect('phone_number')
            ->andWhere('campaign_id=' . $campaignId)
            ->andWhere('was_deleted = 0')
        ;

        if (!empty($limit)) {
            $query->limit($limit);
        }

        $list = $query->execute(array(), Doctrine_Core::HYDRATE_ARRAY);

        $result = array();
        foreach ($list as $object) {
            $result[$object['id']] = $object['phone_number'];
        }
        return $result;
    }

    public static function isLastCampaignNumber($id, $campaignId) {

        $query = self::getInstance()->createQuery()
            ->andWhere('campaign_id = ' . $campaignId)
            ->andWhere('id != ' . $id)
            ->andWhere('was_deleted = ' . WasDeletedHelper::TYPE_NON_DELETED)
        ;

        $amountOfOther = $query->count();

        return ($amountOfOther == 0);
    }
}