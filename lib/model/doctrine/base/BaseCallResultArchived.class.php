<?php

/**
 * BaseCallResultArchived
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property integer $twilio_incoming_call_id
 * @property enum $rating
 * @property enum $sex
 * @property string $first_name
 * @property string $last_name
 * @property string $address
 * @property string $zip
 * @property string $phone
 * @property string $revenue
 * @property string $invoice_number
 * @property string $note
 * 
 * @method integer            getId()                      Returns the current record's "id" value
 * @method integer            getTwilioIncomingCallId()    Returns the current record's "twilio_incoming_call_id" value
 * @method enum               getRating()                  Returns the current record's "rating" value
 * @method enum               getSex()                     Returns the current record's "sex" value
 * @method string             getFirstName()               Returns the current record's "first_name" value
 * @method string             getLastName()                Returns the current record's "last_name" value
 * @method string             getAddress()                 Returns the current record's "address" value
 * @method string             getZip()                     Returns the current record's "zip" value
 * @method string             getPhone()                   Returns the current record's "phone" value
 * @method string             getRevenue()                 Returns the current record's "revenue" value
 * @method string             getInvoiceNumber()           Returns the current record's "invoice_number" value
 * @method string             getNote()                    Returns the current record's "note" value
 * @method CallResultArchived setId()                      Sets the current record's "id" value
 * @method CallResultArchived setTwilioIncomingCallId()    Sets the current record's "twilio_incoming_call_id" value
 * @method CallResultArchived setRating()                  Sets the current record's "rating" value
 * @method CallResultArchived setSex()                     Sets the current record's "sex" value
 * @method CallResultArchived setFirstName()               Sets the current record's "first_name" value
 * @method CallResultArchived setLastName()                Sets the current record's "last_name" value
 * @method CallResultArchived setAddress()                 Sets the current record's "address" value
 * @method CallResultArchived setZip()                     Sets the current record's "zip" value
 * @method CallResultArchived setPhone()                   Sets the current record's "phone" value
 * @method CallResultArchived setRevenue()                 Sets the current record's "revenue" value
 * @method CallResultArchived setInvoiceNumber()           Sets the current record's "invoice_number" value
 * @method CallResultArchived setNote()                    Sets the current record's "note" value
 * @property TwilioIncomingCallArchived $
 * 
 * @package    bionic
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseCallResultArchived extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('call_result_archived');
        $this->hasColumn('id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             ));
        $this->hasColumn('twilio_incoming_call_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('rating', 'enum', 18, array(
             'type' => 'enum',
             'length' => 18,
             'values' => 
             array(
              0 => 'Sales Call',
              1 => 'Service Call',
              2 => 'Billing Call',
              3 => 'Complaint Call',
              4 => 'Other Call',
              5 => 'Call Not Answered',
             ),
             'notnull' => true,
             ));
        $this->hasColumn('sex', 'enum', 6, array(
             'type' => 'enum',
             'length' => 6,
             'values' => 
             array(
              0 => 'Male',
              1 => 'Female',
             ),
             ));
        $this->hasColumn('first_name', 'string', 50, array(
             'type' => 'string',
             'length' => 50,
             ));
        $this->hasColumn('last_name', 'string', 50, array(
             'type' => 'string',
             'length' => 50,
             ));
        $this->hasColumn('address', 'string', 200, array(
             'type' => 'string',
             'length' => 200,
             ));
        $this->hasColumn('zip', 'string', 11, array(
             'type' => 'string',
             'length' => 11,
             ));
        $this->hasColumn('phone', 'string', 22, array(
             'type' => 'string',
             'length' => 22,
             ));
        $this->hasColumn('revenue', 'string', 10, array(
             'type' => 'string',
             'length' => 10,
             ));
        $this->hasColumn('invoice_number', 'string', 22, array(
             'type' => 'string',
             'length' => 22,
             ));
        $this->hasColumn('note', 'string', 300, array(
             'type' => 'string',
             'length' => 300,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('TwilioIncomingCallArchived', array(
             'local' => 'twilio_incoming_call_id',
             'foreign' => 'id'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             'created' => 
             array(
              'name' => 'created_at',
              'type' => 'timestamp',
              'format' => 'Y-m-d H:i:s',
              'options' => 
              array(
              'notnull' => false,
              ),
             ),
             'updated' => 
             array(
              'name' => 'updated_at',
              'type' => 'timestamp',
              'format' => 'Y-m-d H:i:s',
              'options' => 
              array(
              'notnull' => false,
              ),
             ),
             ));
        $this->actAs($timestampable0);
    }
}