<?php

/**
 * BaseCombo
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $name
 * @property string $api_key
 * @property integer $page_id
 * @property boolean $was_deleted
 * @property Page $Page
 * @property Doctrine_Collection $WidgetParents
 * @property Doctrine_Collection $VisitorAnalytics
 * 
 * @method integer             getId()               Returns the current record's "id" value
 * @method string              getName()             Returns the current record's "name" value
 * @method string              getApiKey()           Returns the current record's "api_key" value
 * @method integer             getPageId()           Returns the current record's "page_id" value
 * @method boolean             getWasDeleted()       Returns the current record's "was_deleted" value
 * @method Page                getPage()             Returns the current record's "Page" value
 * @method Doctrine_Collection getWidgetParents()    Returns the current record's "WidgetParents" collection
 * @method Doctrine_Collection getVisitorAnalytics() Returns the current record's "VisitorAnalytics" collection
 * @method Combo               setId()               Sets the current record's "id" value
 * @method Combo               setName()             Sets the current record's "name" value
 * @method Combo               setApiKey()           Sets the current record's "api_key" value
 * @method Combo               setPageId()           Sets the current record's "page_id" value
 * @method Combo               setWasDeleted()       Sets the current record's "was_deleted" value
 * @method Combo               setPage()             Sets the current record's "Page" value
 * @method Combo               setWidgetParents()    Sets the current record's "WidgetParents" collection
 * @method Combo               setVisitorAnalytics() Sets the current record's "VisitorAnalytics" collection
 * 
 * @package    bionic
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseCombo extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('combo');
        $this->hasColumn('id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             ));
        $this->hasColumn('name', 'string', 100, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 100,
             ));
        $this->hasColumn('api_key', 'string', 32, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 32,
             ));
        $this->hasColumn('page_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('was_deleted', 'boolean', null, array(
             'type' => 'boolean',
             'default' => false,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Page', array(
             'local' => 'page_id',
             'foreign' => 'id'));

        $this->hasMany('WidgetParent as WidgetParents', array(
             'local' => 'id',
             'foreign' => 'combo_id'));

        $this->hasMany('VisitorAnalytics', array(
             'local' => 'id',
             'foreign' => 'combo_id'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             'created' => 
             array(
              'name' => 'created_at',
              'type' => 'timestamp',
              'format' => 'Y-m-d H:i:s',
              'options' => 
              array(
              'notnull' => false,
              ),
             ),
             'updated' => 
             array(
              'name' => 'updated_at',
              'type' => 'timestamp',
              'format' => 'Y-m-d H:i:s',
              'options' => 
              array(
              'notnull' => false,
              ),
             ),
             ));
        $this->actAs($timestampable0);
    }
}