<?php

/**
 * BaseCampaign
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property enum $type
 * @property string $name
 * @property string $domain
 * @property string $api_key
 * @property text $conversion_url
 * @property text $custom
 * @property boolean $is_enabled
 * @property boolean $always_convert
 * @property integer $is_recordings_enabled
 * @property integer $dynamic_number_expiry
 * @property integer $advertiser_id
 * @property enum $kind
 * @property boolean $was_deleted
 * @property varchar $call_whisper
 * @property int $call_alert_shorter_than_secs
 * @property int $call_alert_greater_than_secs
 * @property int $call_alert_greater_than_mins
 * @property Advertiser $Advertiser
 * @property Doctrine_Collection $Pages
 * @property Doctrine_Collection $PhysicalPhoneNumbers
 * @property Doctrine_Collection $PhoneCall
 * @property Doctrine_Collection $AdvertiserIncomingNumberPool
 * @property Doctrine_Collection $IncomingNumbersQueues
 * 
 * @method integer             getId()                           Returns the current record's "id" value
 * @method enum                getType()                         Returns the current record's "type" value
 * @method string              getName()                         Returns the current record's "name" value
 * @method string              getDomain()                       Returns the current record's "domain" value
 * @method string              getApiKey()                       Returns the current record's "api_key" value
 * @method text                getConversionUrl()                Returns the current record's "conversion_url" value
 * @method text                getCustom()                       Returns the current record's "custom" value
 * @method boolean             getIsEnabled()                    Returns the current record's "is_enabled" value
 * @method boolean             getAlwaysConvert()                Returns the current record's "always_convert" value
 * @method integer             getIsRecordingsEnabled()          Returns the current record's "is_recordings_enabled" value
 * @method integer             getDynamicNumberExpiry()          Returns the current record's "dynamic_number_expiry" value
 * @method integer             getAdvertiserId()                 Returns the current record's "advertiser_id" value
 * @method enum                getKind()                         Returns the current record's "kind" value
 * @method boolean             getWasDeleted()                   Returns the current record's "was_deleted" value
 * @method varchar             getCallWhisper()                  Returns the current record's "call_whisper" value
 * @method int                 getCallAlertShorterThanSecs()     Returns the current record's "call_alert_shorter_than_secs" value
 * @method int                 getCallAlertGreaterThanSecs()     Returns the current record's "call_alert_greater_than_secs" value
 * @method int                 getCallAlertGreaterThanMins()     Returns the current record's "call_alert_greater_than_mins" value
 * @method Advertiser          getAdvertiser()                   Returns the current record's "Advertiser" value
 * @method Doctrine_Collection getPages()                        Returns the current record's "Pages" collection
 * @method Doctrine_Collection getPhysicalPhoneNumbers()         Returns the current record's "PhysicalPhoneNumbers" collection
 * @method Doctrine_Collection getPhoneCall()                    Returns the current record's "PhoneCall" collection
 * @method Doctrine_Collection getAdvertiserIncomingNumberPool() Returns the current record's "AdvertiserIncomingNumberPool" collection
 * @method Doctrine_Collection getIncomingNumbersQueues()        Returns the current record's "IncomingNumbersQueues" collection
 * @method Campaign            setId()                           Sets the current record's "id" value
 * @method Campaign            setType()                         Sets the current record's "type" value
 * @method Campaign            setName()                         Sets the current record's "name" value
 * @method Campaign            setDomain()                       Sets the current record's "domain" value
 * @method Campaign            setApiKey()                       Sets the current record's "api_key" value
 * @method Campaign            setConversionUrl()                Sets the current record's "conversion_url" value
 * @method Campaign            setCustom()                       Sets the current record's "custom" value
 * @method Campaign            setIsEnabled()                    Sets the current record's "is_enabled" value
 * @method Campaign            setAlwaysConvert()                Sets the current record's "always_convert" value
 * @method Campaign            setIsRecordingsEnabled()          Sets the current record's "is_recordings_enabled" value
 * @method Campaign            setDynamicNumberExpiry()          Sets the current record's "dynamic_number_expiry" value
 * @method Campaign            setAdvertiserId()                 Sets the current record's "advertiser_id" value
 * @method Campaign            setKind()                         Sets the current record's "kind" value
 * @method Campaign            setWasDeleted()                   Sets the current record's "was_deleted" value
 * @method Campaign            setCallWhisper()                  Sets the current record's "call_whisper" value
 * @method Campaign            setCallAlertShorterThanSecs()     Sets the current record's "call_alert_shorter_than_secs" value
 * @method Campaign            setCallAlertGreaterThanSecs()     Sets the current record's "call_alert_greater_than_secs" value
 * @method Campaign            setCallAlertGreaterThanMins()     Sets the current record's "call_alert_greater_than_mins" value
 * @method Campaign            setAdvertiser()                   Sets the current record's "Advertiser" value
 * @method Campaign            setPages()                        Sets the current record's "Pages" collection
 * @method Campaign            setPhysicalPhoneNumbers()         Sets the current record's "PhysicalPhoneNumbers" collection
 * @method Campaign            setPhoneCall()                    Sets the current record's "PhoneCall" collection
 * @method Campaign            setAdvertiserIncomingNumberPool() Sets the current record's "AdvertiserIncomingNumberPool" collection
 * @method Campaign            setIncomingNumbersQueues()        Sets the current record's "IncomingNumbersQueues" collection
 * 
 * @package    bionic
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseCampaign extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('campaign');
        $this->hasColumn('id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             ));
        $this->hasColumn('type', 'enum', 12, array(
             'type' => 'enum',
             'length' => 12,
             'values' => 
             array(
              0 => 'Online',
              1 => 'Offline',
             ),
             'notnull' => false,
             'default' => 'Online',
             ));
        $this->hasColumn('name', 'string', 100, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 100,
             ));
        $this->hasColumn('domain', 'string', 200, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 200,
             ));
        $this->hasColumn('api_key', 'string', 32, array(
             'type' => 'string',
             'notnull' => true,
             'length' => 32,
             ));
        $this->hasColumn('conversion_url', 'text', null, array(
             'type' => 'text',
             ));
        $this->hasColumn('custom', 'text', null, array(
             'type' => 'text',
             ));
        $this->hasColumn('is_enabled', 'boolean', null, array(
             'type' => 'boolean',
             'notnull' => true,
             'default' => 1,
             ));
        $this->hasColumn('always_convert', 'boolean', null, array(
             'type' => 'boolean',
             'notnull' => true,
             'default' => 0,
             ));
        $this->hasColumn('is_recordings_enabled', 'integer', 1, array(
             'type' => 'integer',
             'notnull' => true,
             'default' => 1,
             'length' => 1,
             ));
        $this->hasColumn('dynamic_number_expiry', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             'default' => 60,
             ));
        $this->hasColumn('advertiser_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('kind', 'enum', 12, array(
             'type' => 'enum',
             'length' => 12,
             'values' => 
             array(
              0 => 'Web Pages',
              1 => 'Yellow Pages',
              2 => 'Billboards',
              3 => 'Email',
              4 => 'Other',
             ),
             'notnull' => false,
             'default' => 'Other',
             ));
        $this->hasColumn('was_deleted', 'boolean', null, array(
             'type' => 'boolean',
             'default' => false,
             ));
        $this->hasColumn('call_whisper', 'varchar', 255, array(
             'type' => 'varchar',
             'length' => 255,
             ));
        $this->hasColumn('call_alert_shorter_than_secs', 'int', 1, array(
             'type' => 'int',
             'length' => 1,
             ));
        $this->hasColumn('call_alert_greater_than_secs', 'int', 1, array(
             'type' => 'int',
             'length' => 1,
             ));
        $this->hasColumn('call_alert_greater_than_mins', 'int', 1, array(
             'type' => 'int',
             'length' => 1,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Advertiser', array(
             'local' => 'advertiser_id',
             'foreign' => 'id'));

        $this->hasMany('Page as Pages', array(
             'local' => 'id',
             'foreign' => 'campaign_id'));

        $this->hasMany('PhysicalPhoneNumber as PhysicalPhoneNumbers', array(
             'local' => 'id',
             'foreign' => 'campaign_id'));

        $this->hasMany('PhoneCall', array(
             'local' => 'id',
             'foreign' => 'campaign_id'));

        $this->hasMany('AdvertiserIncomingNumberPool', array(
             'local' => 'id',
             'foreign' => 'campaign_id'));

        $this->hasMany('IncomingNumbersQueue as IncomingNumbersQueues', array(
             'local' => 'id',
             'foreign' => 'campaign_id'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             'created' => 
             array(
              'name' => 'created_at',
              'type' => 'timestamp',
              'format' => 'Y-m-d H:i:s',
              'options' => 
              array(
              'notnull' => false,
              ),
             ),
             'updated' => 
             array(
              'name' => 'updated_at',
              'type' => 'timestamp',
              'format' => 'Y-m-d H:i:s',
              'options' => 
              array(
              'notnull' => false,
              ),
             ),
             ));
        $this->actAs($timestampable0);
    }
}