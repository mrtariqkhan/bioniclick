<?php

/**
 * BaseNumberFormat
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $format
 * @property Doctrine_Collection $WidgetPhones
 * 
 * @method integer             getId()           Returns the current record's "id" value
 * @method string              getFormat()       Returns the current record's "format" value
 * @method Doctrine_Collection getWidgetPhones() Returns the current record's "WidgetPhones" collection
 * @method NumberFormat        setId()           Sets the current record's "id" value
 * @method NumberFormat        setFormat()       Sets the current record's "format" value
 * @method NumberFormat        setWidgetPhones() Sets the current record's "WidgetPhones" collection
 * 
 * @package    bionic
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseNumberFormat extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('number_format');
        $this->hasColumn('id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             ));
        $this->hasColumn('format', 'string', 20, array(
             'type' => 'string',
             'length' => 20,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasMany('WidgetPhone as WidgetPhones', array(
             'local' => 'id',
             'foreign' => 'number_format_id'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             'created' => 
             array(
              'name' => 'created_at',
              'type' => 'timestamp',
              'format' => 'Y-m-d H:i:s',
              'options' => 
              array(
              'notnull' => false,
              ),
             ),
             'updated' => 
             array(
              'name' => 'updated_at',
              'type' => 'timestamp',
              'format' => 'Y-m-d H:i:s',
              'options' => 
              array(
              'notnull' => false,
              ),
             ),
             ));
        $this->actAs($timestampable0);
    }
}