<?php

/**
 * BaseAdvertiser
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @property integer $id
 * @property string $name
 * @property string $url
 * @property integer $company_id
 * @property integer $rating_level_id
 * @property string $default_phone_number
 * @property integer $authorize_customer_profile_id
 * @property boolean $is_debtor
 * @property integer $package_id
 * @property boolean $was_deleted
 * @property Company $Company
 * @property Package $Package
 * @property RatingLevel $RatingLevel
 * @property Doctrine_Collection $Profiles
 * @property Doctrine_Collection $Campaigns
 * @property Doctrine_Collection $PhoneCall
 * @property Doctrine_Collection $AdvertiserIncomingNumberPool
 * @property Doctrine_Collection $CustomerPaymentProfiles
 * @property Doctrine_Collection $IncomingNumbersQueues
 * 
 * @method integer             getId()                            Returns the current record's "id" value
 * @method string              getName()                          Returns the current record's "name" value
 * @method string              getUrl()                           Returns the current record's "url" value
 * @method integer             getCompanyId()                     Returns the current record's "company_id" value
 * @method integer             getRatingLevelId()                 Returns the current record's "rating_level_id" value
 * @method string              getDefaultPhoneNumber()            Returns the current record's "default_phone_number" value
 * @method integer             getAuthorizeCustomerProfileId()    Returns the current record's "authorize_customer_profile_id" value
 * @method boolean             getIsDebtor()                      Returns the current record's "is_debtor" value
 * @method integer             getPackageId()                     Returns the current record's "package_id" value
 * @method boolean             getWasDeleted()                    Returns the current record's "was_deleted" value
 * @method Company             getCompany()                       Returns the current record's "Company" value
 * @method Package             getPackage()                       Returns the current record's "Package" value
 * @method RatingLevel         getRatingLevel()                   Returns the current record's "RatingLevel" value
 * @method Doctrine_Collection getProfiles()                      Returns the current record's "Profiles" collection
 * @method Doctrine_Collection getCampaigns()                     Returns the current record's "Campaigns" collection
 * @method Doctrine_Collection getPhoneCall()                     Returns the current record's "PhoneCall" collection
 * @method Doctrine_Collection getAdvertiserIncomingNumberPool()  Returns the current record's "AdvertiserIncomingNumberPool" collection
 * @method Doctrine_Collection getCustomerPaymentProfiles()       Returns the current record's "CustomerPaymentProfiles" collection
 * @method Doctrine_Collection getIncomingNumbersQueues()         Returns the current record's "IncomingNumbersQueues" collection
 * @method Advertiser          setId()                            Sets the current record's "id" value
 * @method Advertiser          setName()                          Sets the current record's "name" value
 * @method Advertiser          setUrl()                           Sets the current record's "url" value
 * @method Advertiser          setCompanyId()                     Sets the current record's "company_id" value
 * @method Advertiser          setRatingLevelId()                 Sets the current record's "rating_level_id" value
 * @method Advertiser          setDefaultPhoneNumber()            Sets the current record's "default_phone_number" value
 * @method Advertiser          setAuthorizeCustomerProfileId()    Sets the current record's "authorize_customer_profile_id" value
 * @method Advertiser          setIsDebtor()                      Sets the current record's "is_debtor" value
 * @method Advertiser          setPackageId()                     Sets the current record's "package_id" value
 * @method Advertiser          setWasDeleted()                    Sets the current record's "was_deleted" value
 * @method Advertiser          setCompany()                       Sets the current record's "Company" value
 * @method Advertiser          setPackage()                       Sets the current record's "Package" value
 * @method Advertiser          setRatingLevel()                   Sets the current record's "RatingLevel" value
 * @method Advertiser          setProfiles()                      Sets the current record's "Profiles" collection
 * @method Advertiser          setCampaigns()                     Sets the current record's "Campaigns" collection
 * @method Advertiser          setPhoneCall()                     Sets the current record's "PhoneCall" collection
 * @method Advertiser          setAdvertiserIncomingNumberPool()  Sets the current record's "AdvertiserIncomingNumberPool" collection
 * @method Advertiser          setCustomerPaymentProfiles()       Sets the current record's "CustomerPaymentProfiles" collection
 * @method Advertiser          setIncomingNumbersQueues()         Sets the current record's "IncomingNumbersQueues" collection
 * 
 * @package    bionic
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class BaseAdvertiser extends sfDoctrineRecord
{
    public function setTableDefinition()
    {
        $this->setTableName('advertiser');
        $this->hasColumn('id', 'integer', null, array(
             'type' => 'integer',
             'primary' => true,
             'autoincrement' => true,
             ));
        $this->hasColumn('name', 'string', 100, array(
             'type' => 'string',
             'length' => 100,
             ));
        $this->hasColumn('url', 'string', 256, array(
             'type' => 'string',
             'length' => 256,
             ));
        $this->hasColumn('company_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('rating_level_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('default_phone_number', 'string', 22, array(
             'type' => 'string',
             'notnull' => true,
             'default' => '8770000000',
             'length' => 22,
             ));
        $this->hasColumn('authorize_customer_profile_id', 'integer', null, array(
             'type' => 'integer',
             'unique' => true,
             ));
        $this->hasColumn('is_debtor', 'boolean', null, array(
             'type' => 'boolean',
             'default' => false,
             ));
        $this->hasColumn('package_id', 'integer', null, array(
             'type' => 'integer',
             'notnull' => true,
             ));
        $this->hasColumn('was_deleted', 'boolean', null, array(
             'type' => 'boolean',
             'default' => false,
             ));
    }

    public function setUp()
    {
        parent::setUp();
        $this->hasOne('Company', array(
             'local' => 'company_id',
             'foreign' => 'id'));

        $this->hasOne('Package', array(
             'local' => 'package_id',
             'foreign' => 'id'));

        $this->hasOne('RatingLevel', array(
             'local' => 'rating_level_id',
             'foreign' => 'id'));

        $this->hasMany('sfGuardUserProfile as Profiles', array(
             'local' => 'id',
             'foreign' => 'advertiser_id'));

        $this->hasMany('Campaign as Campaigns', array(
             'local' => 'id',
             'foreign' => 'advertiser_id'));

        $this->hasMany('PhoneCall', array(
             'local' => 'id',
             'foreign' => 'advertiser_id'));

        $this->hasMany('AdvertiserIncomingNumberPool', array(
             'local' => 'id',
             'foreign' => 'advertiser_id'));

        $this->hasMany('CustomerPaymentProfile as CustomerPaymentProfiles', array(
             'local' => 'id',
             'foreign' => 'advertiser_id'));

        $this->hasMany('IncomingNumbersQueue as IncomingNumbersQueues', array(
             'local' => 'id',
             'foreign' => 'advertiser_id'));

        $timestampable0 = new Doctrine_Template_Timestampable(array(
             'created' => 
             array(
              'name' => 'created_at',
              'type' => 'timestamp',
              'format' => 'Y-m-d H:i:s',
              'options' => 
              array(
              'notnull' => false,
              ),
             ),
             'updated' => 
             array(
              'name' => 'updated_at',
              'type' => 'timestamp',
              'format' => 'Y-m-d H:i:s',
              'options' => 
              array(
              'notnull' => false,
              ),
             ),
             ));
        $this->actAs($timestampable0);
    }
}