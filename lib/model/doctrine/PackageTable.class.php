<?php

class PackageTable extends Doctrine_Table {

    public static function getInstance() {
        return Doctrine_Core::getTable('Package');
    }

    public static function createQueryIndexAll($user, $filterValues = array()) {

        if ($user->getIsSuperAdmin()) {
            $companyId = null;
        } else {
            return self::getInstance()->createQuery()->andWhere('false');
        }

        $query = self::createQueryByCompanyIdAndWasDeleted($companyId, WasDeletedHelper::getType($user));

        $alias = $query->getRootAlias();
        $query->innerJoin("$alias.Company com");

        $simpleFilterFieldNames = array(
            BranchHelper::SIMPLE_FILTER_FIELD_TYPE_STRING => array(
                'name'  => 'name',
            ),
            BranchHelper::SIMPLE_FILTER_FIELD_TYPE_OTHER => array(
                'monthly_fee'                   => 'monthly_fee',
                'toll_free_number_monthly_fee'  => 'toll_free_number_monthly_fee',
                'local_number_monthly_fee'      => 'local_number_monthly_fee',
                'toll_free_number_per_minute'   => 'toll_free_number_per_minute',
                'local_number_per_minute'       => 'local_number_per_minute',
            ),
        );
        WasDeletedHelper::addSimpleFilterFieldName($user, $simpleFilterFieldNames);

        $branchHelper = new BranchHelperCompany();
        $query = $branchHelper->createQueryBranch($query, $user, $filterValues, $simpleFilterFieldNames);

        return $query;
    }

    public static function createQueryIndex($user) {

        if ($user->getIsSuperAdmin()) {
            $companyId = null;
        } elseif ($user->isCompanyUser()) {
            $companyId = $user->getCompanyId();
        } else {
            return self::getInstance()->createQuery()->andWhere('false');
        }

        return self::createQueryByCompanyIdAndWasDeleted($companyId, WasDeletedHelper::getType($user));
    }

    public static function createQueryByCompanyId($companyId) {
        return self::createQueryByCompanyIdAndWasDeleted($companyId, WasDeletedHelper::TYPE_NON_DELETED);
    }

    protected static function createQueryByCompanyIdAndWasDeleted($companyId, $wasDeletedType) {

        $query = self::getInstance()->createQuery()
            ->addOrderBy('name ASC')
        ;

        if (!empty($companyId)) {
            $query->andWhere('company_id = ' . $companyId);
        }

        if ($wasDeletedType != WasDeletedHelper::TYPE_ALL) {
            $query->andWhere("was_deleted = ?", $wasDeletedType);
        }

        return $query;
    }

    public static function countByUser(sfGuardUser $user) {

        $companyId = $user->getFirstCompanyId();
        $query = self::createQueryByCompanyId($companyId);

        return $query->count();
    }
    
    public static function findByCompanyId($companyId) {
        return self::createQueryByCompanyId($companyId)->execute();
    }

    public static function countNonDeletedCompanies($packageId) {

        $query = self::getInstance()->createQuery('pac')
            ->innerJoin('pac.Companies com')
            ->andWhere("pac.id = $packageId")
            ->andWhere('com.' . WasDeletedHelper::FIELD_NAME . ' = ' . WasDeletedHelper::TYPE_NON_DELETED)
        ;
        return $query->count();
    }

    public static function countNonDeletedPackagesForParentCompany($companyId) {

        $query = self::getInstance()->createQuery('pac')
            ->innerJoin('pac.Company com')
            ->andWhere("pac.company_id = $companyId")
            ->andWhere('pac.' . WasDeletedHelper::FIELD_NAME . ' = ' . WasDeletedHelper::TYPE_NON_DELETED)
        ;
        return $query->count();
    }

    public static function countNonDeletedAdverisers($packageId) {

        $query = self::getInstance()->createQuery('pac')
            ->innerJoin('pac.Advertisers adv')
            ->andWhere("pac.id = $packageId")
            ->andWhere('adv.' . WasDeletedHelper::FIELD_NAME . ' = ' . WasDeletedHelper::TYPE_NON_DELETED)
        ;
        return $query->count();
    }
}