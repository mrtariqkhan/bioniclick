<?php

/**
 * PhoneCallTable.
 *
 * @package    bionic
 * @subpackage Model
 * @author     Efremochkin Yury <yury.efremochkin@fairdevteam.com>
 * @version    SVN: $Id: actions.class.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class PhoneCallTable extends Doctrine_Table {

    const CREATE_PHONE_CALL_INFO_OLD                = 'PHONE CALL [OLD]:';
    const CREATE_PHONE_CALL_INFO_OWNER_WHILE_CALL   = 'PHONE CALL [OWNER]:';
    const CREATE_PHONE_CALL_INFO_NEED_ANALYTICS     = 'PHONE CALL [ANALYTICS]:';
    const CREATE_PHONE_CALL_INFO_UPDATED            = 'PHONE CALL [UPDATED]:';
    const CREATE_PHONE_CALL_INFO_CORRECTION_TYPE    = 'PHONE CALL [CORRECTIONS TYPE]:';
    const CREATE_PHONE_CALL_INFO_ERRORS             = 'PHONE CALL [ERROR TYPE]:';

    const CREATE_PHONE_CALL_TYPE_NEW                = 'NEW';
    const CREATE_PHONE_CALL_TYPE_MODIFIED           = 'HAS BEEN MODIFIED';
    const CREATE_PHONE_CALL_TYPE_CORRECT            = 'WAS CORRECT';

    const CREATE_PHONE_CALL_ERROR_OWNER_WHILE_CALL_ADV  = 'Owher advertiser while call is wrong.';
    const CREATE_PHONE_CALL_ERROR_OWNER_WHILE_CALL_CAM  = 'Owher campaign while call is wrong.';
    const CREATE_PHONE_CALL_ERROR_VA                    = 'Related visitor analytics is wrong.';
    const CREATE_PHONE_CALL_ERROR_TO_DEFAULT            = 'If call was to default phone number is wrong.';


    
    /**
     * Returns an instance of this class.
     *
     * @return object PhoneCallTable
     */
    public static function getInstance() {
        return Doctrine_Core::getTable('PhoneCall');
    }

    public static function createTemporaryByTwilioIncomingCallWithoutAnalytics(TwilioIncomingCall $twilioIncomingCall) {

        $twilioIncomingCallId           = $twilioIncomingCall->getId();
        $twilioIncomingPhoneNumberId    = $twilioIncomingCall->getTwilioIncomingPhoneNumberId();
        $tmpStartTime                   = $twilioIncomingCall->getStartTime();

        $id = $twilioIncomingCallId;
        $phoneCall = PhoneCallTable::getInstance()->find($id);
        if (empty($phoneCall) || !$phoneCall->exists()) {
            $phoneCall = new PhoneCall();
            $phoneCall->setId($twilioIncomingCallId);
        }

        $ownerWhileCall = TwilioIncomingPhoneNumberTable::findOwnerAtTime($twilioIncomingPhoneNumberId, $tmpStartTime);

        $advertiserId   = array_key_exists('advertiser_id', $ownerWhileCall) ? $ownerWhileCall['advertiser_id'] : null;
        $campaignId     = array_key_exists('campaign_id', $ownerWhileCall) ? $ownerWhileCall['campaign_id'] : null;

        if (empty($advertiserId)) {
            $phoneCall->setAdvertiser(Doctrine_Collection::getNullObject());
        } else {
            $phoneCall->setAdvertiserId($advertiserId);
        }
        if (empty($campaignId)) {
            $phoneCall->setCampaign(Doctrine_Collection::getNullObject());
        } else {
            $phoneCall->setCampaignId($campaignId);
        }
        $phoneCall->setVisitorAnalytics(Doctrine_Collection::getNullObject());

        //TODO:: when call is started: we shall add info if called phone number WAS default in tmpStartTime of the call (using orders).
        $isDefault = AdvertiserIncomingNumberPoolTable::findIsDefaultTwilioIncomingPhoneNumberId($twilioIncomingPhoneNumberId, $campaignId);
        $phoneCall->setPhoneNumberWasDefault($isDefault);

        if ($phoneCall->isNew() || $phoneCall->isModified()) {
            $phoneCall->save();
        }
        return $phoneCall;
    }
    
    public static function createOrUpdateByActualTwilioIncomingCall(TwilioIncomingCall $twilioIncomingCall, $replace = true) {

        $info = array(
            self::CREATE_PHONE_CALL_INFO_CORRECTION_TYPE    => '',
            self::CREATE_PHONE_CALL_INFO_ERRORS             => array(),
            self::CREATE_PHONE_CALL_INFO_OLD                => array(),
            self::CREATE_PHONE_CALL_INFO_OWNER_WHILE_CALL   => array(),
            self::CREATE_PHONE_CALL_INFO_NEED_ANALYTICS     => array(),
            self::CREATE_PHONE_CALL_INFO_UPDATED            => array(),
        );

        $twilioIncomingCallId           = $twilioIncomingCall->getId();
        $twilioIncomingPhoneNumberId    = $twilioIncomingCall->getTwilioIncomingPhoneNumberId();
        $actualStartTime                = $twilioIncomingCall->getStartTime();

        $id = $twilioIncomingCallId;
        $phoneCall = PhoneCallTable::getInstance()->find($id);
        if (empty($phoneCall) || !$phoneCall->exists()) {
            $phoneCall = new PhoneCall();
            $phoneCall->setId($twilioIncomingCallId);
        } else {
            $info[self::CREATE_PHONE_CALL_INFO_OLD] = $phoneCall->toArray();
        }

        $oldAdvertiserId    = $phoneCall->isNew() ? null : $phoneCall->getAdvertiserId();
        $oldCampaignId      = $phoneCall->isNew() ? null : $phoneCall->getCampaignId();
        $oldVaId            = $phoneCall->isNew() ? null : $phoneCall->getVisitorAnalyticsId();
        $oldWasDefault      = $phoneCall->isNew() ? null : $phoneCall->getPhoneNumberWasDefault();

        $ownerWhileCall = TwilioIncomingPhoneNumberTable::findOwnerAtTime($twilioIncomingPhoneNumberId, $actualStartTime);
        $info[self::CREATE_PHONE_CALL_INFO_OWNER_WHILE_CALL] = $ownerWhileCall;

        $advertiserId   = array_key_exists('advertiser_id', $ownerWhileCall) ? $ownerWhileCall['advertiser_id'] : null;
        $campaignId     = array_key_exists('campaign_id', $ownerWhileCall) ? $ownerWhileCall['campaign_id'] : null;
        $vaId           = null;

        if (!empty($advertiserId)) {
            $phoneCall->setAdvertiserId($advertiserId);

            if (!empty($campaignId)) {
                $phoneCall->setCampaignId($campaignId);

                $lastVA = VisitorAnalyticsTable::findLastForNumberTillTime(
                    $twilioIncomingPhoneNumberId,
                    $actualStartTime
                );
                if (!empty($lastVA) && $lastVA->exists()) {
                    $campaignOfLastVA = $lastVA->findCampaignId();
                    if ($campaignOfLastVA == $campaignId) {
                        $vaId = $lastVA->getId();
                        $phoneCall->setVisitorAnalyticsId($vaId);
                        $info[self::CREATE_PHONE_CALL_INFO_NEED_ANALYTICS] = $lastVA->toArray();
                    }
                }
            }
        }

        if (empty($advertiserId) && !empty($oldAdvertiserId)) {
            $phoneCall->setAdvertiser(Doctrine_Collection::getNullObject());
        }
        if (empty($campaignId) && !empty($oldCampaignId)) {
            $phoneCall->setCampaign(Doctrine_Collection::getNullObject());
        }
        if (empty($vaId) && !empty($oldVaId)) {
            $phoneCall->setVisitorAnalytics(Doctrine_Collection::getNullObject());
        }

        //TODO:: when call is ended: we shall add info if called phone number WAS default in StartTime of the call (using orders).
        $isDefault = AdvertiserIncomingNumberPoolTable::findIsDefaultTwilioIncomingPhoneNumberId($twilioIncomingPhoneNumberId, $campaignId);
        $phoneCall->setPhoneNumberWasDefault($isDefault);

        if ($phoneCall->isNew()) {
            $info[self::CREATE_PHONE_CALL_INFO_CORRECTION_TYPE] = self::CREATE_PHONE_CALL_TYPE_NEW;
        } elseif ($phoneCall->isModified()) {
            $info[self::CREATE_PHONE_CALL_INFO_CORRECTION_TYPE] = self::CREATE_PHONE_CALL_TYPE_MODIFIED;

            if (
                    empty($advertiserId) && !empty($oldAdvertiserId)
                || !empty($advertiserId) &&  empty($oldAdvertiserId)
                || $advertiserId != $oldAdvertiserId
            ) {
                $info[self::CREATE_PHONE_CALL_INFO_ERRORS][] = self::CREATE_PHONE_CALL_ERROR_OWNER_WHILE_CALL_ADV;
            }
            if (
                    empty($campaignId) && !empty($oldCampaignId)
                || !empty($campaignId) &&  empty($oldCampaignId)
                || $campaignId != $oldCampaignId
            ) {
                $info[self::CREATE_PHONE_CALL_INFO_ERRORS][] = self::CREATE_PHONE_CALL_ERROR_OWNER_WHILE_CALL_CAM;
            }
            if (
                    empty($vaId) && !empty($oldVaId)
                || !empty($vaId) &&  empty($oldVaId)
                || $vaId != $oldVaId
            ) {
                $info[self::CREATE_PHONE_CALL_INFO_ERRORS][] = self::CREATE_PHONE_CALL_ERROR_VA;
            }
            if (
                    empty($isDefault) && !empty($oldWasDefault)
                || !empty($isDefault) &&  empty($oldWasDefault)
                || $isDefault != $oldWasDefault
            ) {
                $info[self::CREATE_PHONE_CALL_INFO_ERRORS][] = self::CREATE_PHONE_CALL_ERROR_TO_DEFAULT;
            }
        } else {
            $info[self::CREATE_PHONE_CALL_INFO_CORRECTION_TYPE] = self::CREATE_PHONE_CALL_TYPE_CORRECT;
        }
        $info[self::CREATE_PHONE_CALL_INFO_UPDATED] = $phoneCall->toArray();


        if ($replace) {
            if ($phoneCall->isNew() || $phoneCall->isModified()) {
                $phoneCall->save();
            }
        }

        return $info;
    }
    
//    public static function createByTwilioIncomingCall(TwilioIncomingCall $twilioIncomingCall) {
//
//        //NOTE:: is old code, because $startTime is not time of actual call's start
//
//        $twilioIncomingCallId           = $twilioIncomingCall->getId();
//        $twilioIncomingPhoneNumberId    = $twilioIncomingCall->getTwilioIncomingPhoneNumberId();
//        $startTime                      = $twilioIncomingCall->getStartTime();
//
//        $sqlString = "
//INSERT INTO `phone_call`
//    (`id`, `advertiser_id`, `campaign_id`, `visitor_analytics_id`, `created_at`, `updated_at`)
//SELECT
//    tic.id
//  , aip.advertiser_id advertiser_id
//  , aip.campaign_id campaign_id
//  , maxtwic.va_id visitor_analytics_id
//  , NOW()
//  , NOW()
//FROM
//    (
//        SELECT $twilioIncomingCallId id
//    ) tic
//
//    LEFT JOIN advertiser_incoming_number_pool aip ON aip.twilio_incoming_phone_number_id = $twilioIncomingPhoneNumberId
//
//    LEFT JOIN (
//        SELECT
//            va.id AS va_id,
//            va.combo_id AS combo_id
//        FROM
//            visitor_analytics va
//            INNER JOIN analytics_phone_number mapn ON va.id = mapn.visitor_analytics_id
//        WHERE
//                mapn.twilio_incoming_phone_number_id = $twilioIncomingPhoneNumberId
//            AND va.analytics_time < $startTime
//        ORDER BY
//            va.analytics_time DESC
//        LIMIT 1
//    ) AS maxtwic ON aip.id
//
//    LEFT JOIN combo ON combo.`id` = maxtwic.`combo_id`
//    LEFT JOIN page ON page.`id` = combo.`page_id`
//    LEFT JOIN campaign ON campaign.`id` = page.campaign_id AND campaign.`id` = aip.campaign_id
//LIMIT 1;
//        ";
//
//        self::getInstance()->getConnection('doctrine')
//            ->execute($sqlString)
//        ;
//    }
//
//    /**
//     * 
//     * 
//     * @param int $advertiserId id (adevrtiser with id) or 0 (all advertisers) or null (no one). If null then it does not matter what campaign is set.
//     * @param int $campaignId id (campaign with id) or 0 (all campaigns) or null (no one).
//     * @param int $from timestamp
//     * @param int $to timestamp
//     * @return type 
//     */
//    public static function findCallsSummaryInfoToOwner($advertiserId, $campaignId, $from = null, $to = null) {
//        //TODO:: test this method
//
//        $query = self::getInstance()->createQuery('pc')
//
//            ->leftJoin('pc.TwilioIncomingCall tic')
//            ->leftJoin('tic.TwilioCallQueue tcq')
//            ->leftJoin('pc.VisitorAnalytics va')
//            ->leftJoin('va.Combo cmb')
//            ->leftJoin('cmb.Page pg')
//            ->leftJoin('pg.Campaign cam')
//            ->leftJoin('va.AnalyticsPhoneNumbers apn')
//
//            ->andWhere('(apn.twilio_incoming_phone_number_id IS NULL OR apn.twilio_incoming_phone_number_id = tic.twilio_incoming_phone_number_id)')
//
//            ->distinct()
//
//            ->select('tic.id AS id')
//            ->addSelect('pc.id AS id')
//            ->addSelect('va.id AS id')
//            ->addSelect('cmb.id AS id')
//            ->addSelect('pg.id AS id')
//            ->addSelect('
//                IF(
//                    (
//                        tic.end_time IS NULL 
//                        OR tic.call_status = "' . TwilioIncomingCallTable::STATUS_IN_PROGRESS . '" 
//                        OR tcq.id IS NOT NULL
//                    )
//                    , false
//                    , true
//                ) AS call_has_been_updated
//            ')
//
//            ->addSelect('pc.campaign_id AS campaign_id')
//            ->addSelect('pg.campaign_id AS campaign_id')
//
//            ->addSelect('pc.advertiser_id AS advertiser_id')
//            ->addSelect('cam.advertiser_id AS advertiser_id')
//
//            ->addSelect('tic.twilio_incoming_phone_number_id AS tipn_id')
//            ->addSelect('apn.twilio_incoming_phone_number_id AS tipn_id')
//
//            ->addSelect('tic.start_time AS start_time')
//            ->addSelect('va.analytics_time AS analytics_time')
//
//            ->addOrderBy('va.analytics_time DESC')
//            ->addOrderBy('tic.start_time DESC')
//        ;
//
//        if (is_null($advertiserId)) {
//            $query->andWhere("pc.advertiser_id IS NULL");
//        } else {
//            if (!empty($advertiserId)) {
//                $query->andWhere("pc.advertiser_id = $advertiserId");
//            }
//
//            if (is_null($campaignId)) {
//                $query->andWhere("pc.campaign_id IS NULL");
//            } elseif (!empty($campaignId)) {
//                $query->andWhere("pc.campaign_id = $campaignId");
//            }
//        }
//
//        if (!empty($from)) {
//            $query->andWhere("(tic.start_time >= $from OR va.analytics_time >= $from)");
//        }
//
//        if (!empty($to)) {
//            $query->andWhere("(tic.start_time <= $to OR va.analytics_time <= $to)");
//        }
//
//        return $query->execute(array(), Doctrine_Core::HYDRATE_SCALAR);
//    }
}