<?php

class PageTable extends Doctrine_Table {
    
    public static function getInstance() {
        return Doctrine_Core::getTable('Page');
    }

    /**
     * @param <type> $user
     * @param integer campaignId id of page's campaign
     * @return <Doctrine_Query> query to select collection of Pages
     */
    public static function getIndexQueryByUserAndCampaignId($user, $campaignId) {

        $query = self::getInstance()->createQuery()
            ->andWhere('campaign_id = ?', $campaignId)
            ->addOrderBy('page_url ASC')
        ;

        $wasDeletedType = WasDeletedHelper::getType($user);
        if ($wasDeletedType != WasDeletedHelper::TYPE_ALL) {
            $query->andWhere('was_deleted = ?', $wasDeletedType);
        }

        return $query;
    }
    
    public static function findByCampaignId($campaignId) {
        return self::getInstance()->createQuery()
            ->andWhere('campaign_id = ?', $campaignId)
            ->andWhere('was_deleted = 0')
            ->execute()
        ;
    }
}