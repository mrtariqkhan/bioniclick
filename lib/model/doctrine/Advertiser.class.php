<?php

/**
 * Advertiser
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    bionic
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7691 2011-02-04 15:43:29Z jwage $
 */
class Advertiser extends BaseAdvertiser {
    //TODO:: add work with Authorize while creating, deleting

    public function getWasDeleted() {
        //NOTE:: method has been added to make possible to use method_exists
        return $this->_get('was_deleted');
    }

    public function getWasDeletedString() {
        return WasDeletedHelper::getAsString($this->getWasDeleted());
    }

    public function getBranchAdvertiser() {
        return $this;
    }

    public function getBranchCompany() {
        return $this->getCompany();
    }

    public function getLevel() {
        return CompanyTable::$LEVEL_ADVERTISER;
    }

    public function getAdmin() {
        return sfGuardUserTable::getAdminByAdvertiser($this);
    }

    public function getEmailAddress() {

        $admin          = $this->getAdmin();
        $emailAddress   = empty($admin) ? '' : $admin->getEmailAddress();

        return $emailAddress;
    }

    public function findCampaigns() {
        return CampaignTable::findByAdvertiserId($this->getId());
    }

    public function findCampaignsForInvoice($from, $to) {
        return CampaignTable::findForInvoice($this->getId(), $from, $to);
    }

    /**
     *
     * @param sfGuardUser $sfGuardUser
     * @return boolean true if Campaign's owner is $sfGuardUser or some sub-user (from company sub-tree of $sfGuardUser)
     */
    public function isAllowable($sfGuardUser, $forUpdating = true) {
        return $sfGuardUser->ifHasSubAdvertiser($this);
    }

    /**
     *
     * @param integer $from UNIX timestamp
     * @param integer $to UNIX timestamp
     * @return integer 
     */
    public function calcTollCallTime($from, $to) {
        return TwilioIncomingCallTable::findIncomingCallTimeByAdvertiser(
            $this->getId(),
            TwilioLocalAreaCodeTable::TYPE_TOLL_FREE,
            $from,
            $to
        );
    }

    /**
     *
     * @param integer $from UNIX timestamp
     * @param integer $to UNIX timestamp
     * @return integer 
     */
    public function calcLocalCallTime($from, $to) {
        return TwilioIncomingCallTable::findIncomingCallTimeByAdvertiser(
            $this->getId(),
            TwilioLocalAreaCodeTable::TYPE_LOCAL,
            $from,
            $to
        );
    }

    /**
     *
     * @param integer $from UNIX timestamp
     * @param integer $to UNIX timestamp
     * @return integer 
     */
    public function calcTollNumbers($from, $to) {
        return TwilioIncomingPhoneNumberTable::findCountByAdvertiser(
            $this,
            $from,
            $to,
            TwilioLocalAreaCodeTable::TYPE_TOLL_FREE
        );
    }

    /**
     *
     * @param integer $from UNIX timestamp
     * @param integer $to UNIX timestamp
     * @return integer 
     */
    public function calcLocalNumbers($from, $to) {
        return TwilioIncomingPhoneNumberTable::findCountByAdvertiser(
            $this,
            $from,
            $to,
            TwilioLocalAreaCodeTable::TYPE_LOCAL
        );
    }

    /**
     *
     * @param integer $from UNIX timestamp
     * @param integer $to UNIX timestamp
     * @return integer 
     */
    public function calcTollCallTimeUnassigned($from, $to) {
        return TwilioIncomingCallTable::findIncomingCallTimeByAdvertiserUnassigned(
            $this->getId(),
            TwilioLocalAreaCodeTable::TYPE_TOLL_FREE,
            $from,
            $to
        );
    }

    /**
     *
     * @param integer $from UNIX timestamp
     * @param integer $to UNIX timestamp
     * @return integer 
     */
    public function calcLocalCallTimeUnassigned($from, $to) {
        return TwilioIncomingCallTable::findIncomingCallTimeByAdvertiserUnassigned(
            $this->getId(),
            TwilioLocalAreaCodeTable::TYPE_LOCAL,
            $from,
            $to
        );
    }

    public function save(Doctrine_Connection $conn = null) {

        $wasNew = $this->isNew();

        parent::save($conn);

        if ($wasNew) {
            $authorizeCustomerProfileId = AuthorizeNet::createCustomerProfile(
                AuthorizeNet::OBJECT_TYPE_CODE_ADVERTISER,
                $this->getId()
            );
            $this->setAuthorizeCustomerProfileId($authorizeCustomerProfileId);
            parent::save($conn);
        }
    }

    public function deleteByUser(sfGuardUser $userAuthor, Doctrine_Connection $conn = null) {

        if (!$conn) {
            $conn = Doctrine::getConnectionByTableName(AdvertiserTable::getInstance()->getTableName());
        }
        $conn->beginTransaction();
        try {
            $this->deleteByUserUnChecked($userAuthor, $conn);

            $conn->commit();

        } catch (Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }

    protected function deleteByUserUnChecked(sfGuardUser $userAuthor, Doctrine_Connection $conn) {

        $authorizeCustomerProfileId = $this->getAuthorizeCustomerProfileId();
        AuthorizeNet::deleteCustomerProfile($authorizeCustomerProfileId);

        $advertiserId = $this->getId();

        $campaigns = CampaignTable::findByAdvertiserId($advertiserId);
        foreach ($campaigns as $campaign) {
            $campaign->deleteByUser($userAuthor, $conn);
        }
        
        $users = SfGuardUserTable::findByAdvertiserId($advertiserId);
        foreach ($users as $user) {
            $user->delete($conn);
        }

        //TODO:: think about grouping by localAreaCode
        $advertiserIncomingNumberPools = AdvertiserIncomingNumberPoolTable::findByAdvertiserId($advertiserId);
        foreach ($advertiserIncomingNumberPools as $advertiserIncomingNumberPool) {
            $advertiserIncomingNumberPool->setIsDefault(false);
            $advertiserIncomingNumberPool->save($conn);
            AdvertiserIncomingNumberPoolTable::moveNumberToParentPool(
                $advertiserIncomingNumberPool, 
                $userAuthor, 
                $conn
            );
        }

        $orders = IncomingNumbersQueueTable::findUnFinishedByAdvertiserId($advertiserId);
        foreach ($orders as $order) {
            $order->closeByUser($userAuthor, $conn);
        }

        $this->setAuthorizeCustomerProfileId(null);
        $this->setWasDeleted(WasDeletedHelper::TYPE_DELETED);
        $this->save($conn);
    }

    public function getParentBranchCompanies() {

        $parentCompany = $this->getCompany();

        $parentCompanies = $parentCompany->getBranchCompanies();

        return $parentCompanies;
    }

    public function calcAvailibleToPurchaseNumbers() {
        return BionicIncomingNumberPoolTable::calcAvailibleToPurchaseNumbers();
    }

    public static function findAllByEmptyCustomerProfile() {

        $query = CompanyTable::getInstance()->createQuery()->andWhere('authorize_customer_profile_id IS NULL');
        return $query->execute();
    }

    public function getCustomerPaymentProfiles() {
        return CustomerPaymentProfileTable::findByAdvertiserId($this->getId());
    }

    public function declareIsDebtor($isDebtor = true) {

        $this->setIsDebtor($isDebtor);
        $this->save();

        return array(
            'company'   => array(),
            'advertiser'=> array($this)
        );
    }

    public function isDirectBionicChild() {
        return CompanyTable::isDirectBionicChild($this);
    }
    
    public function getChartsBreadCrumb() {
        
        return new BreadCrumb(
            'homepage_for_child_advertiser',
            array('advertiser_id' => $this->getId()),
            'Charts for',
            "'{$this->getName()}'",
            'icon-charts'
        );
    }
    
    public function getChartsParentBreadCrumbs(sfGuardUser $user) {

        $breadCrumbs = array();
        if ($user->isAdvertiserUser()) {
            if ($user->getAdvertiserId() == $this->getId()) {
                return $breadCrumbs;
            }
            throw new Exception('Access denied.');
        } elseif (!$user->ifHasSubAdvertiser($this)) {
            throw new Exception('Access denied.');
        }

        $parent = $this->getCompany();
        $breadCrumbs = $parent->getChartsParentBreadCrumbs($user);
        $breadCrumbs[] = $parent->getChartsBreadCrumb();

        return $breadCrumbs;
    }
}