<?php

class MerchantPaymentProfileTable extends Doctrine_Table {

    /**
     * Creates an instance of the Table class
     * @return Doctrine_Table
     */
    public static function getInstance() {
        return Doctrine_Core::getTable('MerchantPaymentProfile');
    }
}