<?php

class PaymentProfileTable extends Doctrine_Table {

    public static function getInstance() {
        return Doctrine_Core::getTable('PaymentProfile');
    }

    public static function createQueryByUserId($userId) {
        $query = self::getInstance()->createQuery('pp')
            ->where('pp.sf_guard_user_id = ?', $userId);
        return $query;
    }
}