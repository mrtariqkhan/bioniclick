<?php

/**
 * WidgetPhone
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    bionic
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class WidgetPhone extends BaseWidgetPhone {

    /**
     *
     * @param sfGuardUser $sfGuardUser
     * @return boolean true if WidgetPhone's owner is $sfGuardUser or some sub-user (from company sub-tree of $sfGuardUser)
     */
    public function  isAllowable($sfGuardUser, $forUpdating = true) {
        if (!parent::isAllowable($sfGuardUser, $forUpdating)) {
            return false;
        }

        if (!IsAllowableHelper::isAllowable($this->getPhysicalPhoneNumber(), $sfGuardUser, $forUpdating)) {
            return false;
        }

        return true;
    }

    public function asArray() {

        $arrayParent = parent::asArray();

        $arrayOwn = array(
            'id'                        => $this->getId(),
            'html_content_1'            => $this->getHtmlContent_1(),
            'html_content_2'            => $this->getHtmlContent_2(),
            'number_format_id'          => $this->getNumberFormatId(),
            'physical_phone_number_id'  => $this->getPhysicalPhoneNumberId(),
        );

        $result = array_merge($arrayParent, $arrayOwn);

        return $result;
    }

    public static function generateDefault($combo = null) {

        $success = false;

        $defaultWidgetPhone = DefaultHelper::generate(get_class());

        if (!empty($combo)) {
            $defaultWidgetPhone->setCombo($combo);

//            $arrPhysicalPhoneNumber = PhysicalPhoneNumberTable::findByComboAsArray($combo, 1);
//            if (!empty($arrPhysicalPhoneNumber)) {
//                $physicalPhoneNumberIds = array_keys($arrPhysicalPhoneNumber);
//                $physicalPhoneNumberId = $physicalPhoneNumberIds[0];
//                if (!empty($physicalPhoneNumberId)) {
//                    $defaultWidgetPhone->setPhysicalPhoneNumberId($physicalPhoneNumberId);
//                }
//            }
            $page       = empty($combo) ? null : $combo->getPage();
            $campaign   = empty($page) ? null : $page->getCampaign();
            $numbers    = empty($campaign) ? null : $campaign->getPhysicalPhoneNumbers();

            if ($numbers->count() > 0) {
                $physicalPhoneNumber = $numbers[0];
                if (!empty($physicalPhoneNumber)) {
                    $defaultWidgetPhone->setPhysicalPhoneNumber($physicalPhoneNumber);
                    $success = true;
                }
            }
        }

        if ($success) {
            $numberFormat = NumberFormatTable::getInstance()->createQuery()->fetchOne();
            if (!empty($numberFormat)) {
                $defaultWidgetPhone->setNumberFormatId($numberFormat);
            }
        }

        $defaultWidgetPhone = $success ? $defaultWidgetPhone : null;

        return $defaultWidgetPhone;
    }
   
}
