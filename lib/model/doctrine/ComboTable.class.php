<?php

class ComboTable extends Doctrine_Table {
    
    public static function getInstance() {
        return Doctrine_Core::getTable('Combo');
    }

    /**
     * @param <type> $user
     * @param integer $pageId id of combo's page
     * @return <Doctrine_Query> query to select collection of Combos
     */
    public static function getIndexQueryByUserAndPageId($user, $pageId, $query) {

        $rootAlias = 'c';
        if (empty($query)) {
            $query = self::getInstance()->createQuery($rootAlias);
        } else {
            $rootAlias = $query->getRootAlias();
        }

        $query
            ->andWhere("$rootAlias.page_id = ?", $pageId)
            ->addOrderBy('name ASC')
        ;

        $wasDeletedType = WasDeletedHelper::getType($user);
        if ($wasDeletedType != WasDeletedHelper::TYPE_ALL) {
            $query->andWhere("$rootAlias.was_deleted = ?", $wasDeletedType);
        }

        return $query;
    }

    /**
     * Checks if another widget with the same html_id exists in combo already
     *  among all widgets.
     * @param integer $id widget's id
     * @param integer $comboId combo's id
     * @param string $htmlId need html_id
     * @return boolean true if another exists
     */
    public static function existComboWidgetWithSameHtmlId($id, $comboId, $htmlId) {

        $queryGlobal = array();

        $valueHtml = mysql_escape_string($htmlId);

        $inheritances = WidgetParentTable::findAllInheritances();
        foreach ($inheritances as $tableName) {
            $query = Doctrine::getTable($tableName)->getInstance()->createQuery()
                ->select('id')
                ->addSelect('html_id')
                ->addSelect('combo_id')
                ->andWhere('id != ' . $id)
                ->andWhere('combo_id = ' . $comboId)
                ->andWhere('was_deleted = 0')
                ->andWhere("html_id LIKE '$valueHtml'")
            ;

            $queryGlobal[] = '(' . $query->getSqlQuery() . ')';
        }

        $statement = implode(' UNION ', $queryGlobal);

        $conn = Doctrine_Manager::getInstance()->getConnection('doctrine');
        $resultArray = $conn->fetchAssoc($statement, array());
        $amount = count($resultArray);

        return $amount > 0;
    }

    /**
     * @param integer comboId combo Id
     * @return <string> combo URL
     */
    public static function buildUrl($comboId) {

        $url = null;
        if (!empty($comboId)) {
            $combo = self::getInstance()->findOneBy('id', $comboId);
            if (!empty($combo)) {
                $url = $combo->buildUrl();
            }
        }

        return $url;
    }

    public static function findById($id) {
        return ComboTable::getInstance()->find($id);        
    }

    public static function findByPageId($pageId) {
        return self::getInstance()->createQuery()
            ->andWhere('page_id = ?', $pageId)
            ->andWhere('was_deleted = 0')
            ->execute();
    }
    
    public static function findByNameAndApiKey() {
        
    }
}