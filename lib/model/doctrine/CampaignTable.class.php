<?php

class CampaignTable extends Doctrine_Table {

    const PROTOCOL_NAME_HTTP = 'HTTP';
    const PROTOCOL_HTTP = 'http://';

    const KIND_CODE_ALL            = 1;
    const KIND_CODE_WEB_PAGES      = 2;
    const KIND_CODE_YELLOW_PAGES   = 3;
    const KIND_CODE_BILLBOARDS     = 4;
    const KIND_CODE_EMAIL          = 5;
    const KIND_CODE_OTHER          = 6;

    const KIND_WEB_PAGES        = 'Web Pages';
    const KIND_YELLOW_PAGES     = 'Yellow Pages';
    const KIND_BILLBOARDS       = 'Billboards';
    const KIND_EMAIL            = 'Email';
    const KIND_OTHER            = 'Other';
    const KIND_ALL              = 'All';

    
    /**
     * Creates an instance of the Table class
     * @return Doctrine_Table
     */
    public static function getInstance() {
        return Doctrine_Core::getTable('Campaign');
    }

    /**
     * Finds and returns campaigns by ID
     * @param String $id campaign ID
     * @return Campaign campaign found
     */
    public static function findById($id) {
        return self::getInstance()->findOneBy('id', $id);
    }

    /**
     * Finds and returns campaign by API key
     * @param string $apiKey API key of the campaign
     * @return Campaign campaign found
     */
    public static function findByApiKey($apiKey) {
        return self::getInstance()->findOneBy('api_key', $apiKey);
    }
    public static function findByName($name) {
        return self::getInstance()->findOneBy('name', $name);
    }

//    /**
//     * Finds and returns campaigns allowed for user
//     * @param sfGuardUser $user user from the root company (advertiser)
//     * @param bool $wasDeletedType WasDeletedHelper::TYPE_*
//     * @return Doctrine_Collection Campaigns of user's tree
//     */
//    public static function findByUser($user, $wasDeletedType = WasDeletedHelper::TYPE_NON_DELETED) {
//
//        $query = self::createQueryByUser($user, $wasDeletedType);
//
//        return $query->execute();
//    }
//
//    /**
//     * Finds and returns campaigns allowed for user
//     * @param Integer $userId ID user from the root company (advertiser)
//     * @return Doctrine_Collection Campaigns of user's tree
//     */
//    public static function findByUserId($userId) {
//
//        $query = self::createQueryByUserId($userId);
//
//        return $query->execute();
//    }

    /**
     * Finds and returns ordered list of campaigns by user and part of campaign name
     * @param sfGuardUser $user user
     * @param Advertiser $advertiser user
     * @return type
     */
    public static function findCampaignsOrderedForAdvertiser(sfGuardUser $user, Advertiser $advertiser) {

        $query = self::createQueryByAdvertiser($user, $advertiser);

        $campaigns = $query->execute();

        return $campaigns;
    }
    /**
     * Finds and returns ordered list of campaigns by user and part of campaign name
     * @param sfGuardUser $user user
     * @return type
     */
    public static function findCampaignsOrdered(sfGuardUser $user) {

        $query = self::createQueryByUser($user);

        $campaigns = $query->execute();

        return $campaigns;
    }

    public static function getByTwilioIncomingPhoneNumberId(
        $twilioIncomingPhoneNumberId
    ) {
        $query = self::getInstance()->createQuery('c')
            ->innerJoin('c.AdvertiserIncomingNumberPool pool')
            ->where(
                'pool.twilio_incoming_phone_number_id = ?',
                $twilioIncomingPhoneNumberId
            )
        ;

        return $query->fetchOne();
    }

    /**
     *
     * @param type $user
     * @param type $filterValues
     * @return type
     */
    public static function createQueryByUser($user, $filterValues = array()) {

        $query = self::getInstance()->createQuery('cam')
            ->innerJoin('cam.Advertiser adv')
            ->orderBy('cam.name ASC')
        ;

        $wasDeletedType = WasDeletedHelper::getType($user);
        if ($wasDeletedType != WasDeletedHelper::TYPE_ALL) {
            $query->andWhere("cam.was_deleted = ?", $wasDeletedType);
        }

        $simpleFilterFieldNames = array(
            BranchHelper::SIMPLE_FILTER_FIELD_TYPE_STRING => array(
                'name'  => 'name',
            ),
        );
        $branchHelper = new BranchHelper();
        $query = $branchHelper->createQueryBranch($query, $user, $filterValues, $simpleFilterFieldNames);

        return $query;
    }

    public static function createQueryByAdvertiser($user, $advertiser, $filterValues = array()) {

        $query = self::getInstance()->createQuery('cam')
            ->innerJoin('cam.Advertiser adv')
            ->andWhere('adv.id = ' . $advertiser->getId())
            ->orderBy('cam.name ASC')
        ;

        $wasDeletedType = WasDeletedHelper::getType($user);
        if ($wasDeletedType != WasDeletedHelper::TYPE_ALL) {
            $query->andWhere("cam.was_deleted = ?", $wasDeletedType);
        }

        if (array_key_exists('name', $filterValues)) {
            $value = mysql_escape_string($filterValues['name']);
            $query->andWhere("(cam.name LIKE '%$value%')");
        }

        return $query;
    }

    /**
     * Creates and returns query for campaigns allowed for user
     * @param Integer $userId ID user from the root company (advertiser)
     * @return Doctrine_Query query to select collection Campaigns of user's tree
     */
    public static function createQueryByUserId($userId) {

        $user = sfGuardUserTable::findById($userId);
        $query = self::createQueryByUser($user);

        return $query;
    }

    /**
     * Creates and returns query for campaigns allowed for advertiser
     * @param integer/array $advertiserId ID of the advertiser
     * @return Doctrine_Query query to select collection of Campaign of advertiser
     */
    public static function createQueryByAdvertiserId($advertiserId) {

        $query = self::getInstance()->createQuery('cam')
            ->andWhere('cam.was_deleted = 0')
            ->orderBy('cam.name ASC')
        ;

        if (is_array($advertiserId)) {
            $query->andWhereIn('cam.advertiser_id', $advertiserId);
        } else {
            $query->andWhere('cam.advertiser_id = ?', $advertiserId);
        }
        
        return $query;
    }   

    public static function findByAdvertiserId($advertiserId) {
       return self::createQueryByAdvertiserId($advertiserId)->execute();
    }

    public static function findForInvoice($advertiserId, $from, $to) {

        $query = self::getInstance()->createQuery()
            ->andWhere("advertiser_id = $advertiserId")
            ->andWhere(
                "(
                    (
                            was_deleted = 0
                        AND UNIX_TIMESTAMP(created_at) <= $to
                    ) OR (
                        was_deleted = 1 
                        AND (
                                UNIX_TIMESTAMP(created_at) <= $to
                            AND UNIX_TIMESTAMP(updated_at) >= $from 
                        )
                    )
                )"
            )
        ;

        return $query->execute();
    }
    
    public static function findCountByAdvertiserId($advertiserId) {

        $query = self::createQueryByAdvertiserId($advertiserId);
        $amount = $query->count();
        return $amount;
    }

    public static function findByTwilioIncomingPhoneNumberId($twilioIncomingPhoneNumberId) {

        $campaign = null;
        if (!empty($twilioIncomingPhoneNumberId)) {
            $query = AdvertiserIncomingNumberPoolTable::getInstance()
                ->createQuery()
                ->addWhere('twilio_incoming_phone_number_id = ' . $twilioIncomingPhoneNumberId)
                ->addWhere('campaign_id IS NOT NULL')
            ;
            $pooledNumber = $query->fetchOne();

            if (!empty($pooledNumber)) {
                $campaign = $pooledNumber->getCampaign();
            }
        }

        return $campaign;
    }

    public static function getDefaultProtocolName() {
        return self::PROTOCOL_NAME_HTTP;
    }
    public static function getDefaultProtocol() {
        return self::PROTOCOL_HTTP;
    }

    public static function findAllExistentWithoutDefaultNumber() {

        $query = self::getInstance()->createQuery('cam');

        $subQuery = $query->createSubquery();
        $subQuery
            ->select('ainp.id')
            ->from('AdvertiserIncomingNumberPool ainp')
            ->andWhere('ainp.is_default = true')
            ->andWhere('ainp.campaign_id = cam.id')
        ;
        $subQueryDql = $subQuery->getDql();

        $query
            ->andWhere("NOT EXISTS ($subQueryDql)")
            ->andWhere('cam.was_deleted=?', WasDeletedHelper::TYPE_NON_DELETED)
        ;
        
        return $query->execute();
    }

    public static function findAllDeletedWithNumber() {
        //NOTE:: need for DB corrections. IncomingNumbersSyncTask uses it

        $query = self::getInstance()->createQuery('cam');

        $subQuery = $query->createSubquery();
        $subQuery
            ->select('ainp.id')
            ->from('AdvertiserIncomingNumberPool ainp')
            ->andWhere('ainp.campaign_id = cam.id')
        ;
        $subQueryDql = $subQuery->getDql();

        $query
            ->andWhere("EXISTS ($subQueryDql)")
            ->andWhere('cam.was_deleted=?', WasDeletedHelper::TYPE_DELETED)
            ->orderBy('cam.advertiser_id')
        ;
        
        return $query->execute();
    }

    public static function findAllExistentWithSeveralDefaultNumbers() {

        $query = self::getInstance()->createQuery('cam')
            ->leftJoin('cam.AdvertiserIncomingNumberPool ainp')
            ->andWhere('cam.was_deleted=?', WasDeletedHelper::TYPE_NON_DELETED)
            ->andWhere('ainp.is_default=true')
            ->groupBy('cam.id')
            ->having('COUNT(ainp.id) > 1')
        ;

        return $query->execute();
    }

    public static function findIdByVisitorAnalyticsId($vaId) {
        //TODO:: test it

        $query = self::getInstance()->createQuery('cam')
            ->select('cam.id')
            ->innerJoin('cam.Pages pg')
            ->innerJoin('pg.Combos combo')
            ->innerJoin('combo.VisitorAnalytics va')
            ->andWhere("va.id = $vaId")
            ->limit(1)
        ;

        $campaignId = $query->fetchOne(array(), Doctrine_Core::HYDRATE_SINGLE_SCALAR);
        $campaignId = empty($campaignId) ? null : $campaignId;

        return $campaignId;
    }

    /**
     *
     * @param int $kindCode one of self::KIND_CODE_*
     * @return string one of self::KIND_* or null
     */
    public static function getKindByCode($kindCode) {

        switch ($kindCode) {
            case self::KIND_CODE_WEB_PAGES:
                return self::KIND_WEB_PAGES;
            case self::KIND_CODE_YELLOW_PAGES:
                return self::KIND_YELLOW_PAGES;
            case self::KIND_CODE_BILLBOARDS:
                return self::KIND_BILLBOARDS;
            case self::KIND_CODE_EMAIL:
                return self::KIND_EMAIL;
            case self::KIND_CODE_OTHER:
                return self::KIND_OTHER;
            default:
                return null;
        }
    }
}