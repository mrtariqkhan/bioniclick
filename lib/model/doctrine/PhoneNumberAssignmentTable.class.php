<?php

class PhoneNumberAssignmentTable extends Doctrine_Table {
    
    public static function getInstance() {
        return Doctrine_Core::getTable('PhoneNumberAssignment');
    }

    /**
     * TODO::strange method. need to rewrite it with using of all params.
     *
     * @param <type> $incomingNumberId
     * @param <type> $cityId
     * @param <type> $callBeginTimestamp
     * @return <type>
     */
    public static function getPhysicalPhoneNumber($incomingNumberId) {

        $query = PhysicalPhoneNumberTable::getInstance()->createQuery('p_p_n')
            ->innerJoin('p_p_n.PhoneNumberAssignments p_n_a')
            ->innerJoin('p_n_a.PhoneNumberNoncompleteAssignments')
            ->where('p_n_a.twilio_incoming_phone_number_id = ?', $incomingNumberId);
        return $query->fetchOne();
    }
}