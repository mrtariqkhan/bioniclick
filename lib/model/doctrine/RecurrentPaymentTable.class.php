<?php

class RecurrentPaymentTable extends Doctrine_Table {
    
    public static function getInstance() {
        return Doctrine_Core::getTable('RecurrentPayment');
    }

    public static function findByUserId($userId) {
        return self::getInstance()->findOneBy('sf_guard_user_id', $userId);
    }
}