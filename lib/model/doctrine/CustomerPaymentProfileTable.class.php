<?php

class CustomerPaymentProfileTable extends Doctrine_Table {

    /**
     * Creates an instance of the Table class
     * @return Doctrine_Table
     */
    public static function getInstance() {
        return Doctrine_Core::getTable('CustomerPaymentProfile');
    }

    /**
     *
     * @return Doctrine_Query 
     */
    protected static function createdQueryOrderedByPriority() {

        $query = self::getInstance()->createQuery()
            ->addOrderBy('priority')
        ;

        return $query;
    }

    /**
     *
     * @param integer $companyId
     * @return Doctrine_Collection 
     */
    public static function findByCompanyId($companyId) {

        $query = self::createdQueryOrderedByPriority();

        $query->andWhere("company_id = $companyId");

        return $query->execute();
    }

    /**
     *
     * @param integer $advertiserId
     * @return Doctrine_Collection 
     */
    public static function findByAdvertiserId($advertiserId) {

        $query = self::createdQueryOrderedByPriority();

        $query->andWhere("advertiser_id = $advertiserId");

        return $query->execute();
    }

    protected static function createQueryFindByCompanyIdAndAdvertiserId($companyId, $advertiserId, $priorityOrder) {

        $query = self::getInstance()->createQuery()
            ->addOrderBy("priority $priorityOrder")
        ;

        if (!empty($companyId)) {
            $query->andWhere("company_id = $companyId");
        } elseif (!empty($advertiserId)) {
            $query->andWhere("advertiser_id = $advertiserId");
        } else {
            $query->andWhere('false');
        }

        return $query;
    }

    /**
     *
     * @param integer $companyId
     * @param integer $advertiserId
     * @return integer next priority 
     */
    public static function findNextPriorityByCompanyIdAndAdvertiserId($companyId, $advertiserId) {

        $query = self::createQueryFindByCompanyIdAndAdvertiserId(
            $companyId, 
            $advertiserId, 
            'DESC'
        );

        $customerPaymentProfileLast = $query->fetchOne();
        $priorityLast = empty($customerPaymentProfileLast) ? 0 : $customerPaymentProfileLast->getPriority();

        return $priorityLast + 1;
    }

    public static function findPrevCustomerPaymentProfile(CustomerPaymentProfile $customerPaymentProfile) {

        $query = self::createQueryFindByCompanyIdAndAdvertiserId(
            $customerPaymentProfile->getCompanyId(), 
            $customerPaymentProfile->getAdvertiserId(), 
            'DESC'
        );

        $priority = $customerPaymentProfile->getPriority();
        $query->andWhere("priority < $priority");

        $customerPaymentProfilePrev = $query->fetchOne();

        return $customerPaymentProfilePrev;
    }

    protected static function createQueryNextCustomerPaymentProfile(
        $companyId,
        $advertiserId,
        $priority
    ) {

        $query = self::createQueryFindByCompanyIdAndAdvertiserId(
            $companyId, 
            $advertiserId, 
            'ASC'
        );

        $query->andWhere("priority > $priority");

        return $query;
    }

    public static function findNextCustomerPaymentProfile(CustomerPaymentProfile $customerPaymentProfile) {

        $query = self::createQueryNextCustomerPaymentProfile(
            $customerPaymentProfile->getCompanyId(), 
            $customerPaymentProfile->getAdvertiserId(), 
            $customerPaymentProfile->getPriority()
        );

        $customerPaymentProfileNext = $query->fetchOne();

        return $customerPaymentProfileNext;
    }

    /**
     *
     * @param CustomerPaymentProfile $customerPaymentProfile1
     * @param CustomerPaymentProfile $customerPaymentProfile2
     * @return type 
     */
    public static function exchangePriority(
        $customerPaymentProfile1,
        $customerPaymentProfile2
    ) {
        if (empty($customerPaymentProfile1) || empty($customerPaymentProfile2)) {
            return;
        }

        $conn = Doctrine::getConnectionByTableName(self::getInstance()->getTableName());
        $conn->beginTransaction();
        try {

            $priority1 = $customerPaymentProfile1->getPriority();
            $priority2 = $customerPaymentProfile2->getPriority();

            $customerPaymentProfile1->setPriority($priority2);
            $customerPaymentProfile2->setPriority($priority1);

            $customerPaymentProfile1->saveSimple($conn);
            $customerPaymentProfile2->saveSimple($conn);

            $conn->commit();

        } catch (Exception $e) {
            $conn->rollBack();
            throw $e;
        }
    }

    /**
     *
     * @param CustomerPaymentProfile $customerPaymentProfileToDelete
     */
    public static function deletePriority($companyId, $advertiserId, $priority) {

        $query = 
            self::createQueryNextCustomerPaymentProfile(
                $companyId, 
                $advertiserId, 
                $priority
            )
            ->set('priority', 'priority - 1')
            ->update()
            ->execute()
        ;
    }
}
