<?php

class VisitorAnalyticsTable extends Doctrine_Table {

    const TYPE_CODE_UNDEFINED          = 0;
    const TYPE_CODE_IS_SUITABLE        = 1;
    const TYPE_CODE_IS_SUITABLE_BUT_OLD= 2;
    const TYPE_CODE_NO_SUITABLE        = 3;
    const TYPE_CODE_CALL_TO_DEFAULT    = 4;

    const TYPE_DESCRIPTION_UNDEFINED           = '';
    const TYPE_DESCRIPTION_IS_SUITABLE         = 'Call has suitable Visitor Analytics';
    const TYPE_DESCRIPTION_IS_SUITABLE_BUT_OLD = 'Call has Old suitable Visitor Analytics';
    const TYPE_DESCRIPTION_NO_SUITABLE         = 'Call has no suitable Visitor Analytics';
    const TYPE_DESCRIPTION_CALL_TO_DEFAULT     = 'Call to default phone number';


    const TYPE_PPC      = 0; // Pay Per Click (paid link as result of search)
    const TYPE_ORGANIC  = 1; // (non-paid link as result of search)
    const TYPE_DIRECT   = 2;
    const TYPE_OFFLINE  = 'Offline';
    const TYPE_OTHERS  = 'Others';

    const TYPE_NAME_PPC     = 'PPC';
    const TYPE_NAME_ORGANIC = 'Organic';
    const TYPE_NAME_DIRECT  = 'Direct';


    public static function getInstance() {
        return Doctrine_Core::getTable('VisitorAnalytics');
    }

    public static function convertIsOrganic($searchType) {

        if (is_null($searchType)) {
            return 'Undefined';
        }

        switch ($searchType) {
            case self::TYPE_OFFLINE:
                return self::TYPE_OFFLINE;
            case self::TYPE_OTHERS:
                return self::TYPE_OTHERS;                  
            case self::TYPE_PPC:
                return self::TYPE_NAME_PPC;
            case self::TYPE_ORGANIC:
                return self::TYPE_NAME_ORGANIC;
            case self::TYPE_DIRECT:
                return self::TYPE_NAME_DIRECT;
            default:
                return '';
        }
    }

    /**
     * TODO: Probably this function is deprecated
     *
     * @param TwilioIncomingCall $twilioIncomingCallId
     * @param $startTime
     * @return VisitorAnalytics
     */
    public function findByIncomingCall($twilioIncomingCallId, $startTime) {

        $timeInterval = sfConfig::get('app_seconds_between_visit_and_call');

        $visitorAnalytics = null;

        if (!empty($twilioIncomingCallId) && !empty($startTime)) {

            $query = self::getInstance()->createQuery('va')
                ->innerJoin('va.AnalyticsPhoneNumbers apn')
                ->innerJoin('apn.TwilioIncomingPhoneNumber tipn')
                ->innerJoin('tipn.TwilioIncomingCalls tic')

                ->andWhere('tic.id = ?', $twilioIncomingCallId)
                ->andWhere("$startTime > va.analytics_time")
                ->andWhere("$startTime - {$timeInterval} <= va.analytics_time")
                ->orderBy('va.analytics_time DESC')
            ;

            Loggable::putQueryLogMessage($query);

            $visitorAnalytics = $query->fetchOne();
            $visitorAnalytics = empty($visitorAnalytics) ? null : $visitorAnalytics;
        }

        return $visitorAnalytics;
    }

    /**
     *
     * @param int $tipnId TwilioIncomingCall id
     * @param timestamp $tillTimestamp
     * @param int $campaignId Campaign id
     * @return VisitorAnalytics 
     */
    public static function findLastForNumberTillTime(
        $tipnId,
        $tillTimestamp
    ) {

        $query = self::getInstance()->createQuery('va')
            ->innerJoin('va.AnalyticsPhoneNumbers apn')

            ->where("apn.twilio_incoming_phone_number_id = $tipnId")
            ->andWhere("va.analytics_time < $tillTimestamp")

            ->addOrderBy('va.analytics_time DESC')

            ->limit(1)
        ;

        $va = $query->fetchOne();

        return $va;
    }

    public static function getTypeDescriptions() {
        return array(
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE         => VisitorAnalyticsTable::TYPE_DESCRIPTION_IS_SUITABLE,
            VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD => VisitorAnalyticsTable::TYPE_DESCRIPTION_IS_SUITABLE_BUT_OLD,
            VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE         => VisitorAnalyticsTable::TYPE_DESCRIPTION_NO_SUITABLE,
            VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT     => VisitorAnalyticsTable::TYPE_DESCRIPTION_CALL_TO_DEFAULT,
            VisitorAnalyticsTable::TYPE_CODE_UNDEFINED           => VisitorAnalyticsTable::TYPE_DESCRIPTION_UNDEFINED,
        );
    }

    public static function createConditionToSwitchTypeCode(
        $vaAlias = 'va', 
        $ticAlias = 'tic', 
        $pcAlias = 'pc', 
        $availableSecondsBetweenAnalyticsAndCall = null
    ) {

        return self::createConditionToSwitchTypeParameter(
            array(
                self::TYPE_CODE_NO_SUITABLE         => self::TYPE_CODE_NO_SUITABLE,
                self::TYPE_CODE_IS_SUITABLE         => self::TYPE_CODE_IS_SUITABLE,
                self::TYPE_CODE_IS_SUITABLE_BUT_OLD => self::TYPE_CODE_IS_SUITABLE_BUT_OLD,
                self::TYPE_CODE_CALL_TO_DEFAULT     => self::TYPE_CODE_CALL_TO_DEFAULT,
            ),
            $vaAlias, 
            $ticAlias, 
            $pcAlias,
            $availableSecondsBetweenAnalyticsAndCall
        );
    }

    public static function createConditionToSwitchTypeDescription(
        $vaAlias = 'va', 
        $ticAlias = 'tic', 
        $pcAlias = 'pc', 
        $availableSecondsBetweenAnalyticsAndCall = null
    ) {

        return self::createConditionToSwitchTypeParameter(
            array(
                self::TYPE_CODE_NO_SUITABLE         => '"' . self::TYPE_DESCRIPTION_NO_SUITABLE . '"',
                self::TYPE_CODE_IS_SUITABLE         => '"' . self::TYPE_DESCRIPTION_IS_SUITABLE . '"',
                self::TYPE_CODE_IS_SUITABLE_BUT_OLD => '"' . self::TYPE_DESCRIPTION_IS_SUITABLE_BUT_OLD . '"',
                self::TYPE_CODE_CALL_TO_DEFAULT     => '"' . self::TYPE_DESCRIPTION_CALL_TO_DEFAULT . '"',
            ),
            $vaAlias, 
            $ticAlias, 
            $pcAlias,
            $availableSecondsBetweenAnalyticsAndCall
        );
    }

    protected static function createConditionToSwitchTypeParameter(
        $needArr,
        $vaAlias = 'va', 
        $ticAlias = 'tic', 
        $pcAlias = 'pc',
        $availableSecondsBetweenAnalyticsAndCall = null
    ) {

        $availableSecondsBetweenAnalyticsAndCall = 
            is_null($availableSecondsBetweenAnalyticsAndCall) || !is_int($availableSecondsBetweenAnalyticsAndCall)
            ? sfConfig::get('app_available_seconds_between_visitor_analytics_and_call', 0)
            : $availableSecondsBetweenAnalyticsAndCall;
        ;

        $condition =
            "IF(
                $pcAlias.phone_number_was_default
                , " . $needArr[self::TYPE_CODE_CALL_TO_DEFAULT] . "
                , IF(
                    $vaAlias.id IS NULL 
                    , " . $needArr[self::TYPE_CODE_NO_SUITABLE] . "
                    , IF(
                        $vaAlias.analytics_time >= ($ticAlias.start_time - $availableSecondsBetweenAnalyticsAndCall)
                        , " . $needArr[self::TYPE_CODE_IS_SUITABLE] . "
                        , " . $needArr[self::TYPE_CODE_IS_SUITABLE_BUT_OLD] . "
                    )
                )
            )"
        ;

        return $condition;
    }
}