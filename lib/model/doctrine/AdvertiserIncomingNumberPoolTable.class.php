<?php


class AdvertiserIncomingNumberPoolTable extends Doctrine_Table {

    public static function getInstance() {
        return Doctrine_Core::getTable('AdvertiserIncomingNumberPool');
    }

    /**
     *
     * @param integer $advertiserId
     * @param integer $type TwilioLocalAreaCodeTable::TYPE_*
     * @return <type>
     */
    public static function createQueryByAdvertiserId($advertiserId, $type = null, $unassignedOnly = false) {

        $query = self::getInstance()->createQuery('ainp')
            ->where("advertiser_id = $advertiserId")
            ->addOrderBy('is_default DESC')
            ->addOrderBy('campaign_id ASC')
        ;

        if (!empty($type)) {
            $query
                ->innerJoin('ainp.TwilioIncomingPhoneNumber tipn')
                ->innerJoin('tipn.TwilioLocalAreaCode tlac')
                ->andWhere('tlac.phone_number_type = ?', $type)
            ;
        }

        if ($unassignedOnly) {
            $query->andWhere('ainp.campaign_id IS NULL');
        }

        return $query;
    }

    /**
     *
     * @param <type> $campaignId
     * @return <type>
     */
    public static function createQueryByCampaignId($campaignId, $type = null) {

        $query = self::getInstance()->createQuery('ainp')
            ->andWhere("campaign_id = $campaignId")
        ;

        if (!empty($type)) {
            $query
                ->innerJoin('ainp.TwilioIncomingPhoneNumber tipn')
                ->innerJoin('tipn.TwilioLocalAreaCode tlac')
                ->andWhere('tlac.phone_number_type = ?', $type)
            ;
        }

        return $query;
    }

    /**
    *
    * @param integer $advertiserId
    * @return <Doctrine_Query> query to select ordered collection of AdvertiserIncomingNumberPool for need $advertiserId
    */
    public static function createIndexQueryByAdvertiserId(
        $advertiserId,
        $query = null
    ) {

        if (empty($query)) {
            $query = self::getInstance()->createQuery('ainp');
            $rootAliasName = 'ainp';
        } else {
            $rootAliasName = $query->getRootAlias();
        }

        $query
            ->innerJoin("$rootAliasName.TwilioIncomingPhoneNumber tipn")
            ->leftJoin("$rootAliasName.Campaign c")
            ->addWhere("$rootAliasName.advertiser_id = $advertiserId")
            ->addOrderBy('c.name ASC')
            ->addOrderBy("$rootAliasName.is_default DESC")
            ->addOrderBy('tipn.phone_number');

        return $query;
    }

    public static function createIndexQueryByUser(
        $user,
        $filterValues
    ) {

        $query = self::getInstance()->createQuery('ainp');
        $rootAliasName = 'ainp';

        if (empty($user)) {
            $query->where('false');
            return $query;
        }

        $advertiserId = $user->findSubAdvertisersIds();

        $query
            ->innerJoin("$rootAliasName.TwilioIncomingPhoneNumber tipn")
            ->innerJoin("$rootAliasName.Advertiser adv")
            ->leftJoin("$rootAliasName.Campaign cam")
            ->andWhere('tipn.was_deleted = ?', WasDeletedHelper::TYPE_NON_DELETED)
            ->addOrderBy('cam.name ASC')
            ->addOrderBy("$rootAliasName.is_default DESC")
            ->addOrderBy('tipn.phone_number')
        ;

        if (is_array($advertiserId)) {
            $query->andWhereIn("$rootAliasName.advertiser_id", $advertiserId);
        } else {
            $query->andWhere("$rootAliasName.advertiser_id = ?", $advertiserId);
        }


        $simpleFilterFieldNames = array(
            BranchHelper::SIMPLE_FILTER_FIELD_TYPE_OTHER => array(
                'campaign_id' => 'campaign_id',
            ),
            BranchHelper::SIMPLE_FILTER_FIELD_TYPE_STRING => array(
                'tipn.phone_number' => 'twilio_incoming_phone_number',
            ),
        );

        $branchHelper = new BranchHelper();
        $query = $branchHelper->createQueryBranch($query, $user, $filterValues, $simpleFilterFieldNames);

        return $query;
    }

    /**
     *
     * @param integer $advertiserId
     * @param integer $campaignId
     * @return Doctrine_Query query to select availible numbers for company and user.
     */
    public static function getQueryCampaignAvailableNumbers(
        $advertiserId,
        $campaignId = null
    ) {

        $subQuery = Doctrine_Query::create()
            ->andWhere('campaign_id IS NULL')
        ;
        if (!empty($campaignId)) {
            $subQuery->orWhere('campaign_id=' . $campaignId);
        }
        $whereCondition = implode(' ', $subQuery->getDqlPart('where'));

        $query = self::getInstance()->createQuery('ainp')
            ->select('tipn.phone_number name')
            ->addSelect('ainp.id')
            ->addSelect('tipn.id')
            ->andWhere('ainp.advertiser_id=' . $advertiserId)
            ->andWhere($whereCondition)
            ->innerJoin('ainp.TwilioIncomingPhoneNumber tipn')
            ->innerJoin('tipn.TwilioLocalAreaCode tlac')
            ->addOrderBy('tlac.code')
            ->addOrderBy('ainp.campaign_id DESC')
            ->addOrderBy('tipn.phone_number')
        ;

        return $query;
    }

    public static function findDefaultTwilioIncomingPhoneNumberIdByCampaignId($campaignId) {

        $query = self::getInstance()->createQuery('')
            ->select('twilio_incoming_phone_number_id')
            ->where('campaign_id = ?', $campaignId)
            ->andWhere('is_default = ?', 1)
        ;

        $result = $query->fetchOne(null, Doctrine_Core::HYDRATE_ARRAY);
        if (!empty($result)) {
            return $result['twilio_incoming_phone_number_id'];
        }

        return null;
    }

    public static function findIsDefaultTwilioIncomingPhoneNumberId(
        $tipnId,
        $campaignId
    ) {

        if (empty($campaignId) || empty($tipnId)) {
            return false;
        }

        $query = self::getInstance()->createQuery('')
            ->select('is_default')
            ->where("campaign_id = $campaignId")
            ->andWhere("twilio_incoming_phone_number_id = $tipnId")
        ;

        $result = $query->fetchOne(null, Doctrine_Core::HYDRATE_ARRAY);

        if (!empty($result)) {
            return $result['is_default'];
        }

        return false;
    }

    /**
     *
     * @param integer $advertiserId
     * @return integer id of default number for campaign
     */
    public static function findRandomDefaultNumber($advertiserId) {

        $query = self::getInstance()->createQuery('aipn')
            ->andWhere('aipn.campaign_id is null')
            ->andWhere('aipn.advertiser_id = ?', $advertiserId)
        ;

        return $query->fetchOne();
    }

    public static function getCampaignDefaultNumber($campaignId) {

        $query = self::getInstance()->createQuery()
            ->andWhere('campaign_id = ?', $campaignId)
            ->andWhere('is_default = ?', true)
        ;
        $pooledNumber = $query->fetchOne();

        return $pooledNumber;
    }

    public static function getCampaignDynamicNumbers($campaignId) {

        $query = self::getInstance()->createQuery()
            ->andWhere('campaign_id = ?', $campaignId);
        ;
        $pooledNumbers = $query->fetchArray();

        return $pooledNumbers;
    }

    /**
     *
     * @param integer $campaignId
     * @return integer id of default number for campaign
     */
    public static function getCampaignDefaultNumberId($campaignId) {

        $pooledNumber = self::getCampaignDefaultNumber($campaignId);
        $id = empty($pooledNumber) ? null : $pooledNumber->getId();

        return $id;
    }

    public static function findByIncomingPhoneNumberId($twilioIncomingPhoneNumberId) {

        $query = self::getInstance()->createQuery()
            ->where('twilio_incoming_phone_number_id = ?', $twilioIncomingPhoneNumberId)
        ;

        return $query->fetchOne();
    }

    public static function findAdvertiserByIncomingPhoneNumberId($twilioIncomingPhoneNumberId) {

        $query = AdvertiserTable::getInstance()->createQuery('a')
            ->innerJoin('a.AdvertiserIncomingNumberPool ainp')
            ->where('ainp.twilio_incoming_phone_number_id = ?', $twilioIncomingPhoneNumberId)
        ;

        return $query->fetchOne();
    }

    public static function calcFreeAdvertiserNumbers($campaignId) {

        $campaign = CampaignTable::findById($campaignId);
        return AdvertiserIncomingNumberPoolTable::calcFreeNumbersCount(
            $campaign->getAdvertiserId()
        );
    }

    public static function calcFreeNumbersCount($advertiserId) {

        if (empty($advertiserId)) {
            return 0;
        }
        $query = self::getInstance()->createQuery()
            ->andWhere('advertiser_id = ?', $advertiserId)
            ->andWhere('campaign_id IS NULL')
        ;
        
        return $query->count();
    }

    /**
     * Creates DONE order. Method moves number from BionicIncomingNumberPool to AdvertiserIncomingNumberPool of campaign.
     * Method creates AdvertiserIncomingNumberPoolLog of this operation.
     *
     * @param Advertiser $advertiser
     * @param TwilioIncomingPhoneNumber $incomingNumber
     * @param integer $campaignId
     * @param sfGuardUser $user
     * @param Doctrine_Connection $con 
     */
    public static function moveNumberToCampaignFromBionic(
        $advertiser, 
        $incomingNumber, 
        $campaignId, 
        $user, 
        $con
    ) {

        $incomingNumberId = $incomingNumber->getId();

        $newPoolObject = new AdvertiserIncomingNumberPool();
        $newPoolObject->setAdvertiser($advertiser);
        $newPoolObject->setPurchasedAt(TimeConverter::getUnixTimestamp());
        $newPoolObject->setTwilioIncomingPhoneNumberId($incomingNumberId);
        $newPoolObject->setCampaignId($campaignId);
        $newPoolObject->save($con);

        $bionicIncomingNumberPool = self::getParentCompanyIncomingNumberByTwilioIncomingNumberId(
            $incomingNumberId
        );
        $bionicIncomingNumberPool->setIsOwner(false);
        $bionicIncomingNumberPool->save($con);

        self::createDoneDetailedOrder(
            $advertiser,
            $incomingNumber, 
            $campaignId, 
            $user,
            IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE_AND_ASSIGN,
            $con
        );
    }

    /**
     * Creates DONE order. Method moves number from AdvertiserIncomingNumberPool to AdvertiserIncomingNumberPool of campaign.
     * Method creates AdvertiserIncomingNumberPoolLog of this operation.
     * 
     * @param AdvertiserIncomingNumberPool $advertiserIncomingNumberPool
     * @param integer $campaignId
     * @param sfGuardUser $user
     * @param Doctrine_Connection $con 
     */
    public static function moveNumberToCampaignFromAdvertiser(
        $advertiserIncomingNumberPool, 
        $campaignId, 
        $user, 
        $con = null
    ) {

        $incomingNumber = $advertiserIncomingNumberPool->getTwilioIncomingPhoneNumber();
        $advertiser = $advertiserIncomingNumberPool->getAdvertiser();

        $oldCampaignId = $advertiserIncomingNumberPool->getCampaignId();
        if (!empty($oldCampaignId)) {
            throw new Exception('This phone number belongs to some campaign already.');
        }
        $advertiserIncomingNumberPool->setCampaignId($campaignId);
        $advertiserIncomingNumberPool->save($con);

        self::createDoneDetailedOrder(
            $advertiser,
            $incomingNumber, 
            $campaignId, 
            $user,
            IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_ASSIGN,
            $con
        );
    }

    protected static function findFreeNumbersOfAdvertiserDesiredOrNot(
        $advertiserId,
        $localAreaCodeId,
        $desiredPhoneNumber,
        $numbersCount
    ) {

        $advertiserIncomingNumbersToEditDesired = AdvertiserIncomingNumberPoolTable::findDesiredFreeNumbersByLocalAreaCode(
            $advertiserId,
            $localAreaCodeId,
            $desiredPhoneNumber,
            $numbersCount
        );
        $advertiserIncomingNumbersToEdit = Doctrine_Collection::create(Doctrine::getTable('AdvertiserIncomingNumberPool'));

        $numbersCountOfAny = $numbersCount - $advertiserIncomingNumbersToEditDesired->count();
        $ids = $advertiserIncomingNumbersToEditDesired->getPrimaryKeys();
        if ($numbersCountOfAny) {
            $advertiserIncomingNumbersToEdit = AdvertiserIncomingNumberPoolTable::findFreeNumbersByLocalAreaCode(
                $advertiserId,
                $localAreaCodeId,
                $ids,
                false,
                $numbersCountOfAny
            );
        }
        $advertiserIncomingNumbersToEditAll = $advertiserIncomingNumbersToEditDesired->merge($advertiserIncomingNumbersToEdit);

        return $advertiserIncomingNumbersToEditAll;
    }

    /**
     * Creates DONE order. Method moves numbers from AdvertiserIncomingNumberPool to AdvertiserIncomingNumberPool of campaign.
     * Method creates AdvertiserIncomingNumberPoolLogs of this operation.
     * Method finds phone numbers with desired part first of all. If
     * they are then they will be used before of all other.
     * 
     * @param Advertiser $advertiser
     * @param integer $localAreaCodeId
     * @param string $desiredPhoneNumber desired part of phone_number
     * @param integer $numbersCount
     * @param integer $campaignId
     * @param sfGuardUser $user
     * @throws Exception 
     */
    public static function moveFreeNumbersToCampaignFromAdvertiserDesiredOrNot(
        $advertiser,
        $localAreaCodeId,
        $desiredPhoneNumber,
        $numbersCount,
        $campaignId, 
        $user
    ) {

        $advertiserId = $advertiser->getId();
        $advertiserIncomingNumbersToEditAll = self::findFreeNumbersOfAdvertiserDesiredOrNot(
            $advertiserId, 
            $localAreaCodeId, 
            $desiredPhoneNumber, 
            $numbersCount
        );

        $now = time();

        $orderType = IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_ASSIGN;

        $incomingNumbersQueue = new IncomingNumbersQueue();
        $incomingNumbersQueue->setAdvertiser($advertiser);
        $incomingNumbersQueue->setDoneAmount(0);
        $incomingNumbersQueue->setCreatedDate($now);
        $incomingNumbersQueue->setStatus(IncomingNumbersQueueTable::STATUS_TYPE_OPEN);
        $incomingNumbersQueue->setCampaignId($campaignId);
        $incomingNumbersQueue->setTwilioLocalAreaCodeId($localAreaCodeId);
        $incomingNumbersQueue->setAmount($numbersCount);
        $incomingNumbersQueue->setAuthorNotes($desiredPhoneNumber);
        $incomingNumbersQueue->setUser($user);
        $incomingNumbersQueue->setUpdatedDate($now);
        $incomingNumbersQueue->setOrderType($orderType);
        $incomingNumbersQueue->save();


        $successCount = 0;
        foreach ($advertiserIncomingNumbersToEditAll as $advertiserIncomingNumberPool) {
            try {
                $incomingNumber = $advertiserIncomingNumberPool->getTwilioIncomingPhoneNumber();

                $oldCampaignId = $advertiserIncomingNumberPool->getCampaignId();
                if (!empty($oldCampaignId)) {
                    throw new Exception('This phone number belongs to some campaign already.');
                }
                $advertiserIncomingNumberPool->setCampaignId($campaignId);
                $advertiserIncomingNumberPool->save();

                $advertiserIncomingNumberPoolLog = new AdvertiserIncomingNumberPoolLog();
                $advertiserIncomingNumberPoolLog->setTwilioIncomingPhoneNumberId($incomingNumber->getId());
                $advertiserIncomingNumberPoolLog->setIncomingNumbersQueueId($incomingNumbersQueue->getId());
                $advertiserIncomingNumberPoolLog->save();

                ++$successCount;
            } catch (Exception $e) {

            }
        }

        $status = IncomingNumbersQueueTable::STATUS_TYPE_OPEN;
        if ($numbersCount > $successCount && $successCount != 0) {
            $status = IncomingNumbersQueueTable::STATUS_TYPE_IN_PROGRESS;
        } elseif ($numbersCount == $successCount) {
            $status = IncomingNumbersQueueTable::STATUS_TYPE_DONE;
        }
        $incomingNumbersQueue->setDoneAmount($successCount);
        $incomingNumbersQueue->setStatus($status);
        $incomingNumbersQueue->save();
    }

    /**
     * Creates DONE order and AdvertiserIncomingNumberPoolLog of need type.
     * 
     * @param Advertiser $advertiser
     * @param TwilioIncomingPhoneNumber $incomingNumber
     * @param integer $campaignId
     * @param sfGuardUser $user
     * @param string $orderType one of IncomingNumbersQueueTable::ORDER_TYPE_*
     * @param Doctrine_Connection $con 
     */
    protected static function createDoneDetailedOrder(
        $advertiser,
        $incomingNumber,
        $campaignId, 
        $user, 
        $orderType,
        $con
    ) {

        $now = time();

        $incomingNumbersQueue = new IncomingNumbersQueue();
        $incomingNumbersQueue->setAdvertiser($advertiser);
        $incomingNumbersQueue->setDoneAmount(1);
        $incomingNumbersQueue->setCreatedDate($now);
        $incomingNumbersQueue->setStatus(IncomingNumbersQueueTable::STATUS_TYPE_DONE);
        $incomingNumbersQueue->setCampaignId($campaignId);
        $incomingNumbersQueue->setTwilioLocalAreaCodeId($incomingNumber->getTwilioLocalAreaCodeId());
        $incomingNumbersQueue->setAmount(1);
        $incomingNumbersQueue->setUser($user);
        $incomingNumbersQueue->setUpdatedDate($now);
        $incomingNumbersQueue->setOrderType($orderType);
        $incomingNumbersQueue->save($con);

        $advertiserIncomingNumberPoolLog = new AdvertiserIncomingNumberPoolLog();
        $advertiserIncomingNumberPoolLog->setTwilioIncomingPhoneNumberId($incomingNumber->getId());
        $advertiserIncomingNumberPoolLog->setIncomingNumbersQueueId($incomingNumbersQueue->getId());
        $advertiserIncomingNumberPoolLog->save($con);
    }

    /**
     * The order exists. Method moves number from BionicIncomingNumberPool to AdvertiserIncomingNumberPool.
     * Method creates AdvertiserIncomingNumberPoolLog of this operation.
     * 
     * @param IncomingNumbersQueue $incomingNumbersQueue
     * @param BionicIncomingNumberPool $bionicIncomingNumberPool 
     */
    public static function moveNumberToAdvertiserFromBionicByOrder(
        $incomingNumbersQueue,
        $bionicIncomingNumberPool
    ) {

        $con = Doctrine_Manager::getInstance()->getConnection('doctrine');

        try {
            $con->beginTransaction();


            $incomingNumberId = $bionicIncomingNumberPool->getTwilioIncomingPhoneNumberId();

            $advertiserIncomingNumberPool = $incomingNumbersQueue->createAdvertiserIncomingNumberPool($incomingNumberId);
            $advertiserIncomingNumberPool->save($con);

            $bionicIncomingNumberPool->removeFromCleaningPool($con);

            $advertiserIncomingNumberPoolLog = new AdvertiserIncomingNumberPoolLog();
            $advertiserIncomingNumberPoolLog->setTwilioIncomingPhoneNumberId($bionicIncomingNumberPool->getTwilioIncomingPhoneNumberId());
            $advertiserIncomingNumberPoolLog->setIncomingNumbersQueueId($incomingNumbersQueue->getId());
            $advertiserIncomingNumberPoolLog->save($con);

            $con->commit();

        } catch (Exception $e) {

            $con->rollBack();
            throw $e;
        }
    }

    /**
     * Creates DONE order. Method moves number from AdvertiserIncomingNumberPool of campaign to AdvertiserIncomingNumberPool.
     * Method creates AdvertiserIncomingNumberPoolLog of this operation.
     * 
     * @param AdvertiserIncomingNumberPool $advertiserIncomingNumberPool
     * @param sfGuardUser $user
     * @param Doctrine_Connection $con 
     */
    public static function moveNumberToAdvertiserFromCampaign(
        $advertiserIncomingNumberPool, 
        $user, 
        $con
    ) {

        $campaignId = $advertiserIncomingNumberPool->getCampaignId();
        if (empty($campaignId)) {
            throw new Exception('This phone number does not belong to some campaign.');
        }
        if ($advertiserIncomingNumberPool->getIsDefault()) {
            throw new Exception('This phone number is default.');
        }
        $advertiserIncomingNumberPool->setCampaignId(null);
        $advertiserIncomingNumberPool->save($con);

        $incomingNumber = $advertiserIncomingNumberPool->getTwilioIncomingPhoneNumber();
        $advertiser = $advertiserIncomingNumberPool->getAdvertiser();

        self::createDoneDetailedOrder(
            $advertiser,
            $incomingNumber, 
            $campaignId, 
            $user,
            IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_RESUME_FROM_CAMPAIGN,
            $con
        );
    }

    /**
     * Creates DONE order. Method moves number from AdvertiserIncomingNumberPool of campaign-1 to AdvertiserIncomingNumberPool of campaign-2.
     * Method creates AdvertiserIncomingNumberPoolLogs of operations.
     * 
     * @param AdvertiserIncomingNumberPool $advertiserIncomingNumberPool
     * @param integer $newCampaignId
     * @param sfGuardUser $user
     * @param Doctrine_Connection $con 
     */
    public static function moveNumberToCampaignFromCampaign(
        $advertiserIncomingNumberPool, 
        $newCampaignId, 
        $user,
        $con
    ) {

        $oldCampaignId = $advertiserIncomingNumberPool->getCampaignId();

        if ($oldCampaignId === $newCampaignId) {
            return;
        }

        if (!empty($oldCampaignId)) {
            self::moveNumberToAdvertiserFromCampaign($advertiserIncomingNumberPool, $user, $con);
        }

        if (!empty($newCampaignId)) {
            self::moveNumberToCampaignFromAdvertiser($advertiserIncomingNumberPool, $newCampaignId, $user, $con);
        }
    }
    
    /**
     * Creates DONE order. Method reverts number from AdvertiserIncomingNumberPool to BionicIncomingNumberPool.
     * It deletes record from AdvertiserIncomingNumberPool table.
     * Method creates AdvertiserIncomingNumberPoolLog of this operation.
     * 
     * @param AdvertiserIncomingNumberPool $advertiserIncomingNumberPool
     * @param sfGuardUser $user
     * @param Doctrine_Connection $con 
     */
    public static function moveNumberToParentPool(
        $advertiserIncomingNumberPool, 
        $user, 
        $con
    ) {

        $advertiser = $advertiserIncomingNumberPool->getAdvertiser();
        $campaignId = $advertiserIncomingNumberPool->getCampaignId();
        $incomingNumber = $advertiserIncomingNumberPool->getTwilioIncomingPhoneNumber();
        $incomingNumberId = $advertiserIncomingNumberPool->getTwilioIncomingPhoneNumberId();

        $bionicIncomingNumberPool = BionicIncomingNumberPoolTable::findByIncomingPhoneNumberId(
            $incomingNumberId            
        );
        if (empty($bionicIncomingNumberPool)) {
            throw new Exception("There is no such phone number in Bionic number Pool");
        }

        $bionicIncomingNumberPool->revertToCleaningPool($con);

        $advertiserIncomingNumberPool->deleteAtAll($con);

        self::createDoneDetailedOrder(
            $advertiser,
            $incomingNumber, 
            $campaignId, 
            $user,
            IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_RESUME_FROM_ADVERTISER,
            $con
        );
    }

    protected static function getParentCompanyIncomingNumberByTwilioIncomingNumberId(
        $incomingNumberId
    ) {

        $parentCompanyIncomingNumber = BionicIncomingNumberPoolTable::findByIncomingPhoneNumberId(
            $incomingNumberId,
            true
        );
        if (empty($parentCompanyIncomingNumber)) {
            throw new Exception("There is no such phone in parent's Pool");
        }

        return $parentCompanyIncomingNumber;
    }

    public static function createQueryFreeNumbersByLocalAreaCode($advertiserId, $localAreaCodeId = null, $limit = null) {

        $query = self::getInstance()->createQuery('ainp')
            ->innerJoin('ainp.TwilioIncomingPhoneNumber tipn')
            ->andWhere('ainp.advertiser_id =' . $advertiserId)
            ->andWhere('ainp.campaign_id IS NULL')
        ;

        if (!empty($localAreaCodeId)) {
            $query->addWhere('tipn.twilio_local_area_code_id=' . $localAreaCodeId);
        }

        if (!empty($limit)) {
            $query->limit($limit);
        }

        return $query;
    }

    protected static function findFreeNumbersByLocalAreaCode(
        $advertiserId, 
        $localAreaCodeId = null, 
        $aipnIds = array(),
        $aipnIdsAreDesired = false,
        $limit = null
    ) {

        if (empty($limit) && !is_null($limit)) {
            return Doctrine_Collection::create(Doctrine_Core::getTable('AdvertiserIncomingNumberPool'));
        }

        $query = self::createQueryFreeNumbersByLocalAreaCode(
            $advertiserId,
            $localAreaCodeId,
            $limit
        );

        if (is_array($aipnIds) && !empty($aipnIds)) {
            $not = ($aipnIdsAreDesired) ? '' : 'NOT';
            $ids = implode(', ', $aipnIds);
            $alias = $query->getRootAlias();
            $query
                ->andWhere("$alias.id $not IN ($ids)")
            ;
        }

        Loggable::putQueryLogMessage($query);

        return $query->execute();
    }
    
    protected static function findDesiredFreeNumbersByLocalAreaCode(
        $advertiserId, 
        $localAreaCodeId = null,  
        $desiredPhoneNumber = null, 
        $limit = null
    ) {

        $query = self::createQueryFreeNumbersByLocalAreaCode(
            $advertiserId,
            $localAreaCodeId,
            $limit
        );

        if (!empty($desiredPhoneNumber)) {
            $query->andWhere("tipn.phone_number LIKE '%$desiredPhoneNumber%'");
        }

        Loggable::putQueryLogMessage($query);

        return $query->execute();
    }

    public static function findByAdvertiserId($advertiserId) {
        return self::getInstance()->createQuery()
            ->andWhere('advertiser_id = ?', $advertiserId)
            ->execute()
        ;
    }
    
    public static function findByAdvertiserIdAndNumberId($advertiserId, $numberId) {
        return self::getInstance()->createQuery()
            ->andWhere('advertiser_id = ?', $advertiserId)
            ->andWhere('twilio_incoming_phone_number_id = ?', $numberId)
            ->fetchOne()
        ;
    }

    /**
     * 
     * @param int $advertiserId
     * @return amount of free phone numbers of advertiser(bionic free numbers are not included)
     */
    public static function countAvailibleCampaignDefaultNumbers($advertiserId) {
//
//        $countAvailibleForBionic    = BionicIncomingNumberPoolTable::calcAvailibleToPurchaseNumbers();
//        $countAvailibleForAdvertiser= self::calcFreeNumbersCount(
//            $advertiserId
//        );
//
//        return $countAvailibleForAdvertiser + $countAvailibleForBionic;

        $countAvailibleForAdvertiser= self::calcFreeNumbersCount(
            $advertiserId
        );

        return $countAvailibleForAdvertiser;
    }

    protected static function findUnnecessaryDefaultNumbersByCampaignId($campaignId) {

        $query = self::getInstance()->createQuery('ainp')
            ->andWhere('ainp.is_default=true')
            ->andWhere("ainp.campaign_id = $campaignId")
        ;

        $allCampaignDefaultNumbers = $query->execute();

        if ($allCampaignDefaultNumbers->count() <= 1) {
            return array();
        }

        $unnecessaryCampaignDefaultNumbers = $allCampaignDefaultNumbers;
        unset($unnecessaryCampaignDefaultNumbers[0]);

        return $unnecessaryCampaignDefaultNumbers;
    }

    /**
     *
     * @param integer $campaignId
     * @return array (array('amount_unnecessary_had', 'amount_unnecessary_deleted');
     */
    public static function removeUnnecessaryDefaultNumbersByCampaignId($campaignId) {

        $unnecessaryCampaignDefaultNumbers = self::findUnnecessaryDefaultNumbersByCampaignId($campaignId);

        $successAmount = 0;
        foreach ($unnecessaryCampaignDefaultNumbers as $unnecessaryCampaignDefaultNumber) {
            try {
                $unnecessaryCampaignDefaultNumber->setIsDefault(false);
                $unnecessaryCampaignDefaultNumber->save();
                $successAmount++;
            } catch (Exception $e) {
                
            }
        }

        return array(
            'amount_unnecessary_had'        => count($unnecessaryCampaignDefaultNumbers),
            'amount_unnecessary_deleted'    => $successAmount,
        );
    }

    //NOTE:: for RestoreNumberLogTask
    public static function findNotDetailedCompatible(
        $advertiserId,
        $campaignId,
        $twilioLocalAreaCodeId,
        $limit
    ) {

        $query = self::getInstance()->createQuery('ainp')
            ->innerJoin('ainp.TwilioIncomingPhoneNumber tipn')
            ->leftJoin('tipn.AdvertiserIncomingNumberPoolLogs ainpl')
            ->andWhere('ainpl.id IS NULL')
            ->andWhere("advertiser_id = $advertiserId")
            ->andWhere("tipn.twilio_local_area_code_id = $twilioLocalAreaCodeId")
            ->limit($limit)
        ;

        if (!empty($campaignId)) {
            $query->andWhere("campaign_id = $campaignId");
        }

        return $query->execute();
    }

    //NOTE:: for RestoreNumberLogTask
    public static function findNotDetailed(
        $advertiserId
    ) {

        $query = self::getInstance()->createQuery('ainp')
            ->innerJoin('ainp.TwilioIncomingPhoneNumber tipn')
            ->leftJoin('tipn.AdvertiserIncomingNumberPoolLogs ainpl')
            ->andWhere('ainpl.id IS NULL')
            ->andWhere("advertiser_id = $advertiserId")
        ;

        return $query->execute();
    }

    //NOTE:: for RestoreNumberLogTask
    public static function countNotDetailedAll() {

        $query = self::getInstance()->createQuery('ainp')
            ->innerJoin('ainp.TwilioIncomingPhoneNumber tipn')
            ->leftJoin('tipn.AdvertiserIncomingNumberPoolLogs ainpl')
            ->andWhere('ainpl.id IS NULL')
        ;

        return $query->count();
    }
}