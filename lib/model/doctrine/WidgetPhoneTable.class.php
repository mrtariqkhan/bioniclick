<?php

class WidgetPhoneTable extends Doctrine_Table {
    
    public static function getInstance() {
        return Doctrine_Core::getTable('WidgetPhone');
    }

    public static function getIndexQueryByUserAndComboId($user, $comboId) {

        $query = self::createQueryByComboId($comboId);

        $wasDeletedType = WasDeletedHelper::getType($user);
        if ($wasDeletedType != WasDeletedHelper::TYPE_ALL) {
            $query->andWhere('was_deleted = ?', $wasDeletedType);
        }

        return $query;
    }

    public static function getByComboId($comboId) {

        return self::createQueryByComboId($comboId)
            ->andWhere('was_deleted = ?', WasDeletedHelper::TYPE_NON_DELETED)
            ->execute()
        ;
    }

    public static function findByComboAsArray($comboId) {

        $widgets = self::createQueryByComboId($comboId)
            ->andWhere('was_deleted = ?', WasDeletedHelper::TYPE_NON_DELETED)
            ->execute()
        ;

        $result = array();
        foreach($widgets as $widget) {
            $result[$widget->getHtmlId()] = $widget->asArray();
        }

        return $result;
    }

    protected static function createQueryByComboId($comboId) {

        return self::getInstance()->createQuery()
            ->andWhere('combo_id=' . $comboId)
            ->addOrderBy('html_type, html_id ASC')
        ;
    }
}

