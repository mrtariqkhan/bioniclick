<?php

/**
 * PhysicalPhoneNumber
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    bionic
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class PhysicalPhoneNumber extends BasePhysicalPhoneNumber {

    public function getWasDeleted() {
        //NOTE:: method has been added to make possible to use method_exists
        return $this->_get('was_deleted');
    }

    /**
     *
     * @param sfGuardUser $sfGuardUser
     * @return boolean true if PhysicalPhoneNumber's owner is $sfGuardUser or some sub-user (from company sub-tree of $sfGuardUser)
     */
    public function isAllowable($sfGuardUser, $forUpdating = true) {
        return IsAllowableHelper::isAllowable($this->getCampaign(), $sfGuardUser, $forUpdating);
    }

    public function __toString() {
        return $this->getPhoneNumber();
    }
    
    public function save(Doctrine_Connection $conn = null) {

        //FIXME: temporary insert random city
        $twilioCityId = $this->getTwilioCityId();
        if (empty($twilioCityId)) {
            $someCity = TwilioCityTable::findSomeCity();
            $this->setTwilioCity($someCity);
        }
        parent::save($conn);
    }

    public function delete(Doctrine_Connection $conn = null) {

        if ($this->isLastCampaignNumber()) {
            throw new Exception("Campaign must have even one buisness phone number. Cannot delete the last one.");
        }

        $this->setWasDeleted(1);
        $this->save();
    }

    public function deleteUnChecked(Doctrine_Connection $conn = null) {
        $this->setWasDeleted(1);
        $this->save();
    }

    protected function isLastCampaignNumber() {

        return PhysicalPhoneNumberTable::isLastCampaignNumber(
            $this->getId(),
            $this->getCampaignId()
        );
    }
}
