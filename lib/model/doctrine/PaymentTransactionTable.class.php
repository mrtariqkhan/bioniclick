<?php

class PaymentTransactionTable extends Doctrine_Table {

    public static function getInstance() {
        return Doctrine_Core::getTable('PaymentTransaction');
    }

    public static function findLastByPaymentProfileId($paymentProfileId) {
        $query = self::getInstance()->createQuery('t')
            ->where('t.payment_profile_id = ?', $paymentProfileId)
            ->orderBy('t.created_at DESC');
        return $query->fetchOne();
    }
}