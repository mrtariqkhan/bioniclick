<?php

class TimezoneTable extends Doctrine_Table {

    public static function getInstance() {
        return Doctrine_Core::getTable('Timezone');
    }

    public static function getDefaultId() {

        $defaultTimezoneName = sfConfig::get('app_user_default_timezone_name', null);
        if (empty($defaultTimezoneName)) {
            return null;
        }

        $query = self::getInstance()->createQuery()
            ->select("id")
            ->andWhere("name = ?", $defaultTimezoneName)
        ;

        $result = $query->fetchOne(array(), Doctrine_Core::HYDRATE_ARRAY);

        $id = empty($result) ? null : intval($result['id']);
        return $id;
    }
}