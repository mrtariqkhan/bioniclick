<?php

class AdvertiserTable extends Doctrine_Table {
    
    public static function getInstance() {
        return Doctrine_Core::getTable('Advertiser');
    }

    public static function findById($id) {
        return self::getInstance()->findOneBy('id', $id);
    }

    public static function findByName($name) {
        return self::getInstance()->findOneBy('name', $name);
    }
    public static function findAllByNames($advertiserNames){
        $advertisers = Doctrine::getTable('Advertiser')->createQuery('adv')
            ->whereIn('adv.name', $advertiserNames)
            ->execute();
        return $advertisers;
    }
    public static function isCampaignLimitExhausted($advertiserId) {

        if (empty($advertiserId)) {
            return false;
        }

        $limit = sfConfig::get('app_default_campaigns_amount_limit_per_advertiser');
        $amount = CampaignTable::findCountByAdvertiserId($advertiserId);

        return $amount >= $limit;
    }

    /**
     *
     * @return string error message string if cannot create, returns empty string otherwise
     */
    public static function getWhyCannotCreateNewCampaign($id, $name, $wasDeleted) {

        $name = empty($name) ? 'advertiser' : "the '$name' ";

        if (!WasDeletedHelper::ifCanUpdateAsArray(array(WasDeletedHelper::KEY => $wasDeleted))) {
            return "You can't create new campaigns because of $name company is deleted.";
        }

        if (self::isCampaignLimitExhausted($id)) {
            return "You can't create new campaigns because of available limit exhausting for $name company.";
        }

        $countAvailibleDefaultNumbers = AdvertiserIncomingNumberPoolTable::countAvailibleCampaignDefaultNumbers($id);
        if ($countAvailibleDefaultNumbers <= 0) {
            return "To create a new campaign, you must first purchase new phone numbers for $name Number Pool. There are currently no available numbers to assign to a new campaign for this company.";
        }

        return '';
    }

    public static function findAdvertiserIdsInBranch($user, $filterValues) {

        $query = AdvertiserTable::createQueryAdvertiserIdsInBranch($user, $filterValues);
        $result = $query->execute(array(), Doctrine::HYDRATE_ARRAY);

        $ids = array();
        foreach ($result as $row) {
            $ids[] = $row['id'];
        }

        return $ids;

    }

    protected static function createQueryAdvertiserIdsInBranch($user, $filterValues) {

        // $query = self::getInstance()->createQuery('adv')
        //     ->andWhere('was_deleted = 0');
        $query = self::getInstance()->createQuery('adv');

        $branchHelper = new BranchHelper();
        $query = $branchHelper->createQueryBranch($query, $user, $filterValues);
        $query->select('id');

        return $query;
    }

    public static function createQueryIndex(sfGuardUser $user, $filterValues = array()) {

        $query = self::getInstance()->createQuery('adv');

        if ($user->getIsSuperAdmin()) {
            $query->addOrderBy('adv.name ASC');
        } else {
            $query = self::createQueryCompanyAdvertisersFirstIndex($user, $query);
        }

        $wasDeletedType = WasDeletedHelper::getType($user);
        if ($wasDeletedType != WasDeletedHelper::TYPE_ALL) {
            $query->andWhere("was_deleted = ?", $wasDeletedType);
        }

        $simpleFilterFieldNames = array(
            BranchHelper::SIMPLE_FILTER_FIELD_TYPE_STRING => array(
                'name'                  => 'name',
                'url'                   => 'url',
                'default_phone_number'  => 'default_phone_number',
            ),
        );
        $branchHelper = new BranchHelper();
        $query = $branchHelper->createQueryBranch($query, $user, $filterValues, $simpleFilterFieldNames);

        return $query;
    }

    public static function createQueryFirstByParentCompanyAndUser(sfGuardUser $user, Company $company, $filterValues = array()) {

        $query = self::getInstance()->createQuery('adv')
            ->andWhere("company_id = {$company->getId()}")
            ->addOrderBy('adv.name ASC')
        ;

        $wasDeletedType = WasDeletedHelper::getType($user);
        if ($wasDeletedType != WasDeletedHelper::TYPE_ALL) {
            $query->andWhere("was_deleted = ?", $wasDeletedType);
        }

        $simpleFilterFieldNames = array(
            BranchHelper::SIMPLE_FILTER_FIELD_TYPE_STRING => array(
                'name'                  => 'name',
                'url'                   => 'url',
                'default_phone_number'  => 'default_phone_number',
            ),
        );
        $branchHelper = new BranchHelper();
        $query = $branchHelper->createQueryBranch($query, $user, $filterValues, $simpleFilterFieldNames);

        return $query;
    }

    public static function findFirstIndex(sfGuardUser $user, Company $company, $filterValues = array()) {

        $query = self::createQueryFirstByParentCompanyAndUser($user, $company, $filterValues);

        return $query->execute();
    }

    public static function countFirstIndex(sfGuardUser $user, Company $company, $filterValues = array()) {

        $query = self::createQueryFirstByParentCompanyAndUser($user, $company, $filterValues);

        return $query->count();
    }


    protected static function createQueryCompanyAdvertisersFirstIndex(sfGuardUser $user, $query) {

        if ($user->isAdvertiserUser()) {
            $query->andWhere('false');
        } else {
            $company = $user->getCompany();

            $query->andWhere("company_id = {$company->getId()}");
        }

        return $query;
    }

    public static function createQueryByParentCompany($parentCompany) {

        $lft = $parentCompany->getLft();
        $rgt = $parentCompany->getRgt();

        $query = self::getInstance()->createQuery('adv')
            ->innerJoin('adv.Company com')
            ->andWhere('adv.was_deleted = 0')
            ->andWhere("com.lft BETWEEN $lft AND $rgt")
        ;

        return $query;
    }
    
    public static function findByCompanyId($companyId, $wasDeleted = null) {

        $query = self::getInstance()->createQuery()
            ->andWhere("company_id = $companyId")
            ->addOrderBy('name ASC')
        ;

        if (!is_null($wasDeleted)) {
            $query->andWhere('was_deleted = ?', $wasDeleted);
        }

        return $query->execute();
    }
    
    public static function findForInvoice($companyId, $from, $to) {

        $query = self::getInstance()->createQuery()
            ->andWhere("company_id = $companyId")
            ->andWhere(
                "(
                    (
                            was_deleted = 0
                        AND UNIX_TIMESTAMP(created_at) <= $to
                    ) OR (
                        was_deleted = 1 
                        AND (
                                UNIX_TIMESTAMP(created_at) <= $to
                            AND UNIX_TIMESTAMP(updated_at) >= $from 
                        )
                    )
                )"
            )
        ;

        return $query->execute();
    }
    
    public static function findSubAdvertisersIdsByUser($user) {

        $advertisersIds = array();

        if ($user->isCompanyUser()) {
            $company = $user->getCompany();

            $lft = $company->getLft();
            $rgt = $company->getRgt();

            $query = self::getInstance()->createQuery('adv')
                ->select('adv.id')
                ->innerJoin('adv.Company com')
                ->andWhere("com.lft BETWEEN $lft AND $rgt")
                ->addOrderBy('adv.name ASC')
            ;

            $wasDeletedType = WasDeletedHelper::getType($user);
            if ($wasDeletedType != WasDeletedHelper::TYPE_ALL) {
                $query->andWhere('was_deleted = ?', $wasDeletedType);
            }

            $advertisersIds = $query->execute(array(), Doctrine_Core::HYDRATE_SINGLE_SCALAR);
        }

        return $advertisersIds;
    }

    /**
     *
     * @param array $ids 
     * @return array ('id' => $advertiser)
     */
    public static function findOrderedByIds($ids) {

        $result = array();

        if (!empty($ids)) {
            $query = self::getInstance()->createQuery()
                ->andWhereIn('id', $ids)
            ;

            $advertisers = $query->execute();

            if (!empty($advertisers)) {
                foreach ($advertisers as $advertiser) {
                    $result[$advertiser->getId()] = $advertiser;
                }
            }
        }

        return $result;
    }

//    public static function findAllByWasDeleted($wasDeleted = WasDeletedHelper::TYPE_NON_DELETED) {
//
//        $query = self::getInstance()->createQuery();
//
//        if ($wasDeleted != WasDeletedHelper::TYPE_ALL) {
//            $query->andWhere('was_deleted = ?', $wasDeleted);
//        }
//
//        return $query->execute();
//    }

    public static function findAllDeletedWithNumber() {
        //NOTE:: need for DB corrections. IncomingNumbersSyncTask uses it

        $query = self::getInstance()->createQuery('adv')
            ->select('adv.id')
            ->innerJoin('adv.AdvertiserIncomingNumberPool ainp')
            ->andWhere('adv.was_deleted=?', WasDeletedHelper::TYPE_DELETED)
        ;
       
        return $query->execute();
    }

    /**
     * Fot task
     * @param type $isCustomerProfileEmpty true, false, null
     * @return type 
     */
    public static function findBionicDirectChildByCustomerProfile($isCustomerProfileEmpty) {

        $bionicId = CompanyTable::getBionicId();

        $query = self::getInstance()->createQuery('adv')
            ->innerJoin('adv.Company com')
            ->andWhere("com.id = $bionicId")
            ->andWhere('adv.was_deleted=?', WasDeletedHelper::TYPE_NON_DELETED)
        ;

        if ($isCustomerProfileEmpty) {
            $query->andWhere('authorize_customer_profile_id IS NULL');
        } elseif ($isCustomerProfileEmpty == false) {
            $query->andWhere('authorize_customer_profile_id IS NOT NULL');
        } else {

        }

        return $query->execute();
    }

    public static function findBionicDirectChildNotDebtors() {

        $bionicId = CompanyTable::getBionicId();

        $query = self::getInstance()->createQuery('adv')
            ->innerJoin('adv.Company com')
            ->andWhere("com.id = $bionicId")
            ->andWhere('adv.is_debtor = false')
            ->andWhere('adv.was_deleted=?', WasDeletedHelper::TYPE_NON_DELETED)
        ;

        return $query->execute();
    }
}
