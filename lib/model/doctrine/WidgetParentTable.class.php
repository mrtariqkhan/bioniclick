<?php

class WidgetParentTable extends Doctrine_Table {
    
    public static function getInstance() {
        return Doctrine_Core::getTable('WidgetParent');
    }

    public static function findAllInheritances() {

        $tableNameExclude = Doctrine::getTable('WidgetParent')->getTableName();
        $originalTableNames = self::findOriginalTableNamesByNamePart('widget', $tableNameExclude);

        $tableNames = array();
        foreach ($originalTableNames as $originalTableName) {
            $tableNames[] = sfInflector::camelize($originalTableName);
        }

        return $tableNames;
    }

    /**
     *
     * @param string $tableNamePart part of need table name
     * @param string $tableNameExclude exclude original table name
     * @return array of original table names
     */
    public static function findOriginalTableNamesByNamePart($tableNamePart, $tableNameExclude = '') {
        $statement = 'show tables like "%' . $tableNamePart . '%"';
        $conn = Doctrine_Manager::getInstance()->getConnection('doctrine');
        $resultArray = $conn->fetchAssoc($statement, array());

        $tables = array();
        foreach ($resultArray as $tableInfo) {
            $info = array_values($tableInfo);
            $originalTableName = $info[0];

            if ($tableNameExclude != $originalTableName) {
                $tables[] = $originalTableName;
            }
        }

        return $tables;
    }

    public static function findByComboAsArray($comboId) {

        $inheritances = self::findAllInheritances();

        $result = array();
        foreach ($inheritances as $tableName) {
            $result = array_merge(
                $result,
                Doctrine::getTable($tableName)->findByComboAsArray($comboId)
            );
        }

        return $result;
    }
    
    /**
     * Return all existing (with was_deleted = 0) widgets. 
     * @return array(['widget'] => array(...), ['widget_phone'] => array(...))
     */
    public static function findByCombo($comboId) {
        $inheritances = self::findAllInheritances();

        $result = array();
        foreach ($inheritances as $tableName) {
            $result[$tableName] = Doctrine::getTable($tableName)->getByComboId($comboId);
        }

        return $result;
    }

    public static function findWidgetTypesAsArray() {

        $inheritances = self::findAllInheritances();

        $result = array();
        foreach ($inheritances as $tableName) {
            $typeCode = strtoupper(sfInflector::underscore($tableName));
            $result[$typeCode] = $tableName;
        }

        return $result;
    }
}