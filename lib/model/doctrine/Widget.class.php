<?php

/**
 * Widget
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    bionic
 * @subpackage model
 * @author     Your name here
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
class Widget extends BaseWidget {

    /**
     *
     * @param sfGuardUser $sfGuardUser
     * @return boolean true if Widget's owner is $sfGuardUser or some sub-user (from company sub-tree of $sfGuardUser)
     */
    public function isAllowable($sfGuardUser, $forUpdating = true) {
        return parent::isAllowable($sfGuardUser, $forUpdating);
    }

    public function asArray() {

        $arrayParent = parent::asArray();

        $arrayOwn = array(
            'id'            => $this->getId(),
            'html_content'  => $this->getHtmlContent(),
        );

        $result = array_merge($arrayParent, $arrayOwn);

        return $result;
    }

    public static function generateDefault($combo = null) {

        $defaultWidget = DefaultHelper::generate(get_class());

        if (!empty($combo)) {
            $defaultWidget->setCombo($combo);
        }

        return $defaultWidget;
    }
    
    public function getBreadCrumb() {
        
        
    }
    
    public function getParentBreadCrumbs() {
        
       
    }
}
