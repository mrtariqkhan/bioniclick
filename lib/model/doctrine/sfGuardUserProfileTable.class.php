<?php

class sfGuardUserProfileTable extends Doctrine_Table {

    const VALIDATE_TYPE_NEW     = "n";
    const VALIDATE_TYPE_RESET   = "r";
    const VALIDATE_TYPE_VALID   = null;

    const VALIDATE_TYPE_NAME_NEW    = "New";
    const VALIDATE_TYPE_NAME_RESET  = "Reset";
    const VALIDATE_TYPE_NAME_INVALID= "Invalid";
    const VALIDATE_TYPE_NAME_VALID  = "Valid";
    
    /**
     * Creates an instance of the Table class
     * @return Doctrine_Table
     */
    public static function getInstance() {
        return Doctrine_Core::getTable('sfGuardUserProfile');
    }

    /**
     * Finds and returns user profile by user ID
     * @param Integer $userId ID of the user
     * @return sfGuardUserProfile user profile found
     */
    public static function findByUserId($userId) {
        return self::getInstance()->findOneBy('user_id', $userId);
    }
    
    public static function findAllByAdvertisers($advertisers){
        $advertisersIds = array();
        $advertisersComapnyIds = array();
        foreach( $advertisers as $adv){
          array_push($advertisersIds, $adv->getId());
          array_push($advertisersComapnyIds, $adv->getCompanyId());
        }

        $profiles = Doctrine::getTable('sfGuardUserProfile')->createQuery('user_profile')
            ->whereIn('user_profile.advertiser_id', $advertisersIds)
            ->andWhereIn('user_profile.company_id', $advertisersComapnyIds)
            ->execute();
        return $profiles;
    }

    public static function ifUseSimpleUserCreation() {
        return sfConfig::get("app_sfApplyPlugin_useSimpleUserCreation", false);
    }

    /**
     * NOTE:: see BasesfApplyActions::createGuid method
     *
     * @param type $type sfGuardUserProfileTable::VALIDATE_TYPE_*
     * @return type
     */
    public static function generateValidate($type) {

        if ($type !== self::VALIDATE_TYPE_RESET &&
            (
                self::ifUseSimpleUserCreation()
                || $type == self::VALIDATE_TYPE_VALID
            )
        ) {
            $validate = null;
        } else {
            $validate = "$type";
            for ($i = 0; ($i < 8); $i++) {
                $validate .= sprintf("%02x", mt_rand(0, 255));
            }
        }

        return $validate;
    }

    public static function getValidationType($validate) {

        if ($validate == self::VALIDATE_TYPE_VALID) {
            return self::VALIDATE_TYPE_NAME_VALID;
        }

        $type = substr($validate, 0, 1);

        switch ($type) {
            case self::VALIDATE_TYPE_NEW:
                return self::VALIDATE_TYPE_NAME_NEW;
            case self::VALIDATE_TYPE_RESET:
                return self::VALIDATE_TYPE_NAME_RESET;
            default :
                return self::VALIDATE_TYPE_NAME_INVALID;
        }
    }
}