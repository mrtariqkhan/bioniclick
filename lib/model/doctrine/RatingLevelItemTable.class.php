<?php

/**
 * RatingLevelItemTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class RatingLevelItemTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object RatingLevelItemTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('RatingLevelItem');
    }
}