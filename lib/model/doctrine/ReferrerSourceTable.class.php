<?php

class ReferrerSourceTable extends Doctrine_Table {
    
    public static function getInstance() {
        return Doctrine_Core::getTable('ReferrerSource');
    }

    public function findById($id) {
        
        $result = null;
        if (!empty($id)) {
            $result = self::getInstance()->findOneBy('id', $id);
        }

        return $result;
    }
}