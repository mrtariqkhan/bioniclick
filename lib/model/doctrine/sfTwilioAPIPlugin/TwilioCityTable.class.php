<?php


class TwilioCityTable extends PluginTwilioCityTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('TwilioCity');
    }
    
    public static function findSomeCity() {
        $someCity = self::getInstance()->createQuery()->fetchOne();
        return $someCity;
    }
}