<?php


class TwilioAccountTable extends PluginTwilioAccountTable {
    
    public static function getInstance() {
        return Doctrine_Core::getTable('TwilioAccount');
    }    

    public function findById($id) {
        return self::getInstance()->findOneBy('id', $id);
    }

    //TODO: add relations with allowed account
    public function findDefaultAccount() {
        return $this->findOneBy('id', 1);
    }
}