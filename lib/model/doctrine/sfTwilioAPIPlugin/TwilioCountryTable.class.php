<?php


class TwilioCountryTable extends PluginTwilioCountryTable {
    
    public static function getInstance() {
        return Doctrine_Core::getTable('TwilioCountry');
    }

    public static function findAllNames() {

        $query = self::getInstance()->createQuery()
            ->select('name')
        ;

        $namesWithIds = $query->execute(array(), Doctrine_Core::HYDRATE_ARRAY);
        $names = array();
        foreach ($namesWithIds as $nameWithId) {
            $names[$nameWithId['id']] = $nameWithId['name'];
        }

        return $names;
    }
}