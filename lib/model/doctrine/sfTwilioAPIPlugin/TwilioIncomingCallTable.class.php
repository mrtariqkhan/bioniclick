<?php

class TwilioIncomingCallTable extends PluginTwilioIncomingCallTable {

    const STATUS_NOT_YET_DIALED = 'not yet dialed';
    const STATUS_IN_PROGRESS    = 'in-progress';
    const STATUS_COMPLETED      = 'completed';
    const STATUS_BUSY           = 'busy';
    const STATUS_FAILED         = 'failed';
    const STATUS_NO_ANSWER      = 'no-answer';

    const METHOD_POST   = 'POST';
    const METHOD_GET    = 'GET';

    const PARAMETER_CALL_SID            = 'CallSid';
    const PARAMETER_CALL_ACCOUNT_SID    = 'AccountSid';
    const PARAMETER_CALL_STATUS         = 'CallStatus';


    public static function getInstance() {
        return Doctrine_Core::getTable('TwilioIncomingCall');
    }

    public static function createIncomingCall(array $postParams) {

        $incomingCall = parent::createIncomingCall($postParams);

        if (!empty($incomingCall)) {
            $phoneCall = PhoneCallTable::createTemporaryByTwilioIncomingCallWithoutAnalytics($incomingCall);
            if (empty($phoneCall)) {
                Loggable::putLogMessage(
                    ApplicationLogger::TWILIO_LOG_TYPE,
                    'Temporary PhoneCall has not been added.',
                    sfLogger::ERR
                );
            } else {
                Loggable::putLogMessage(
                    ApplicationLogger::TWILIO_LOG_TYPE,
                    'Temporary PhoneCall has been added.',
                    sfLogger::INFO
                );
                Loggable::putArrayLogMessage(
                    ApplicationLogger::TWILIO_LOG_TYPE,
                    sfLogger::INFO,
                    $phoneCall->toArray()
                );
            }
        }

        return $incomingCall;
    }

    public static function updateIncomingCallBySegmentsAndRecordings(
        PluginTwilioIncomingCall $incomingCall,
        array $callSegments,
        array $recordingList
    ) {

        parent::updateIncomingCallBySegmentsAndRecordings($incomingCall, $callSegments, $recordingList);

        $info = PhoneCallTable::createOrUpdateByActualTwilioIncomingCall($incomingCall);
        $logType = ApplicationLogger::TWILIO_LOG_TYPE;

        Loggable::putLogMessage(
            $logType,
            "\n   " . PhoneCallTable::CREATE_PHONE_CALL_INFO_CORRECTION_TYPE . $info[PhoneCallTable::CREATE_PHONE_CALL_INFO_CORRECTION_TYPE],
            sfLogger::INFO
        );
        Loggable::putArrayLogMessage(
            $logType,
            sfLogger::INFO, 
            $info[PhoneCallTable::CREATE_PHONE_CALL_INFO_ERRORS],
            "\n   " . PhoneCallTable::CREATE_PHONE_CALL_INFO_ERRORS
        );
        Loggable::putArrayLogMessage(
            $logType,
            sfLogger::INFO, 
            $info[PhoneCallTable::CREATE_PHONE_CALL_INFO_OLD],
            "\n   " . PhoneCallTable::CREATE_PHONE_CALL_INFO_OLD
        );
        Loggable::putArrayLogMessage(
            $logType,
            sfLogger::INFO, 
            $info[PhoneCallTable::CREATE_PHONE_CALL_INFO_UPDATED],
            "\n   " . PhoneCallTable::CREATE_PHONE_CALL_INFO_UPDATED
        );
        Loggable::putArrayLogMessage(
            $logType,
            sfLogger::INFO, 
            $info[PhoneCallTable::CREATE_PHONE_CALL_INFO_OWNER_WHILE_CALL],
            "\n   " . PhoneCallTable::CREATE_PHONE_CALL_INFO_OWNER_WHILE_CALL
        );
        Loggable::putArrayLogMessage(
            $logType,
            sfLogger::INFO, 
            $info[PhoneCallTable::CREATE_PHONE_CALL_INFO_NEED_ANALYTICS],
            "\n   " . PhoneCallTable::CREATE_PHONE_CALL_INFO_NEED_ANALYTICS
        );
    }

    public static function getBySid($sid) {
        return self::getInstance()->findOneBy('sid', $sid);
    }

    /**
     * Creates new or updates twilioIncomingCall
     *  call has been started   : $twilioParams are (CallSid, AccountSid, Caller, Called, CallerCity, CallerState, CallerZip, CallerCountry)
     *  call has been finished  : $twilioParams are (CallSid, AccountSid, CallStatus)
     * 
     * @param string $method one of self::METHOD_*
     * @param array $twilioParams
     * @return array(TwiMLBody => string, 'errorMessageSpecific' => string)
     */
    public static function processTwilioCallSummary($method, $twilioParams) {

        $result = array(
            'TwiMLBody'             => '',
            'errorMessageSpecific'  => ''
        );

        Loggable::putArrayLogMessage(
            ApplicationLogger::TWILIO_LOG_TYPE,
            sfLogger::INFO, $twilioParams,
            "\n Twilio $method Params:"
        );
        if (empty($twilioParams) ||
            !array_key_exists(self::PARAMETER_CALL_ACCOUNT_SID, $twilioParams) ||
            !TwilioAccountTable::findBySid($twilioParams[self::PARAMETER_CALL_ACCOUNT_SID])
        ) {
            return $result;
        }
        
        $incomingCall = TwilioIncomingCallTable::getBySid($twilioParams[self::PARAMETER_CALL_SID]);
        if (empty($incomingCall)) {
            $incomingCall = TwilioIncomingCallTable::createIncomingCall($twilioParams);
            if ($incomingCall) {
                if ($incomingCall->getCallStatus() == TwilioIncomingCallTable::STATUS_COMPLETED) {
                    $incomingCall->createQueueForUpdating();
                }
                $result['TwiMLBody'] = IncomingCallHandler::getTwiMlBody($incomingCall);
                Loggable::putLogMessage(
                    ApplicationLogger::TWILIO_LOG_TYPE,
                    "TwiMlBody = {$result['TwiMLBody']}",
                    sfLogger::INFO
                );
            } else {
                $result['errorMessageSpecific'] = "We are sorry. This is error.";
            }
        } else {
            TwilioIncomingCallTable::updateIncomingCall($incomingCall, $twilioParams);
            $incomingCall->createQueueForUpdating();
        }

        return $result;
    }

    public static function findPageRowsIds(
        $user,
        $campaignId,
        $first,
        $amount,
        $filterValues
    ) {

        $result = array(
            'ids'   => array(),
            'count' => 0,
        );

        $query = self::getInstance()
            ->createQuery('tic')
            ->select('id')
            ->innerJoin('tic.TwilioIncomingPhoneNumber tipn')
            ->innerJoin('tipn.AdvertiserIncomingNumberPool ainp')
            ->orderBy('tic.start_time desc')
            ->offset($first)
            ->limit($amount)
        ;

        if ($campaignId) {
            $query->addWhere('ainp.campaign_id = ' . $campaignId);
        } else {
            if ($user->isAdvertiserUser()) {
//                $query
//                    ->andWhere('ainp.advertiser_id = ' . $user->getAdvertiserId())
//                ;
            } else {
                $company = $user->getFirstCompany();
                $query
                    ->innerJoin('ainp.Advertiser adv')
                    ->innerJoin('adv.Company com')
                    ->andWhere("com.lft BETWEEN {$company->getLft()} AND {$company->getRgt()}")
                ;
            }
        }

        //NOTE:: filtering by branch:
        $advIds = BranchHelper::findAdvertiserIdsInBranch($user, $filterValues);
        if (empty($advIds)) {
            return $result;
        }
        
        $query->andWhere('ainp.advertiser_id IN (' . implode(',', $advIds) . ')');

        //NOTE:: filtering by date:
        if (array_key_exists('from', $filterValues)) {
            $query->andWhere("tic.start_time >= {$filterValues['from']}");
        }
        if (array_key_exists('to', $filterValues)) {
            $query->andWhere("tic.start_time <= {$filterValues['to']}");
        }


        $sqlQueryString = $query->getSqlQuery();
        $sqlQueryString = str_replace('SELECT ', 'SELECT SQL_CALC_FOUND_ROWS ', $sqlQueryString);

        $conn = Doctrine_Manager::getInstance()->getConnection('doctrine');
        $rows = $conn->fetchAssoc($sqlQueryString, array());

        $sqlQueryStringAmount = 'SELECT FOUND_ROWS() as amount';
        $count = $conn->fetchOne($sqlQueryStringAmount, array());

//        Loggable::putQueryLogMessage($query, ApplicationLogger::MAIN_LOG_TYPE, sfLogger::INFO);

        $alias = $query->getRootAlias();
        $aliasMap = $query->getTableAliasMap();
        $sqlAlias = array_search($alias, $aliasMap);
        $sqlColumnName = $sqlAlias . '__id';

        $ids = array();
        foreach ($rows as $row) {
            $ids[] = $row[$sqlColumnName];
        }

        $result['ids']      = $ids;
        $result['count']    = $count;
        return $result;
    }

    public static function getPageRows(
        $user,
        $campaignId,
        $first,
        $amount,
        $filterValues,
        $alias = 'tic',
        $advertiserId = -1
    ) {
        
        //NOTE:: filtering by branch:
        if($advertiserId == -1){
            $advIds = BranchHelper::findAdvertiserIdsInBranch($user, $filterValues);
            if (empty($advIds)) {
                return null;
            }
        }else{
           $advIds = array($advertiserId) ;
        }

        
        $query = self::getInstance()->createQuery($alias)
            ->innerJoin("$alias.PhoneCall pc")
        ;

        if ($campaignId) {
            $query->addWhere('pc.campaign_id = ' . $campaignId);
        } else {
            if ($user->isAdvertiserUser()) {
                $company = $user->getFirstCompany();
                $query
                    ->innerJoin('pc.Advertiser adv')
                    ->innerJoin('adv.Company com')
                    ->andWhere("com.lft BETWEEN {$company->getLft()} AND {$company->getRgt()}")
                ;
            }
        }
        // if($advertiserId == -1){
            // $query->andWhere('pc.advertiser_id IN (' . implode(',', $advIds) . ') OR pc.advertiser_id IS NULL');
        // }else{
            $query->andWhere('pc.advertiser_id IN (' . implode(',', $advIds) . ')');
        // }

        //NOTE:: filtering by date:

        if (isset($filterValues['from'])) {
            $query->andWhere("$alias.start_time >= {$filterValues['from']}");
        }
        if (isset($filterValues['to'])) {
            $query->andWhere("$alias.start_time <= {$filterValues['to']}");
        }
        
        $query
            ->select('id')
            ->addSelect('pc.id')
            ->orderBy("$alias.start_time desc")
            ->offset($first)
            ->limit($amount)
        ;
        return $query;
    }




    public static function findIncomingCallTimeByCompany(
        $company,
        $type = null,
        $from = null,
        $to = null
    ) {

        $query = self::createIncomingCallTimeQuery($type, $from, $to);
        $query
            ->innerJoin('pc.Advertiser adv')
            ->innerJoin('adv.Company com')
            ->andWhere("com.lft BETWEEN {$company->getLft()} AND {$company->getRgt()}")
        ;

        return self::findIncomingCallTimeByQuery($query);
    }

    public static function findIncomingCallTimeByAdvertiser(
        $advertiserId,
        $type = null,
        $from = null,
        $to = null
    ) {

        $query = self::createIncomingCallTimeQuery($type, $from, $to);
        $query
            ->andWhere("pc.advertiser_id = $advertiserId")
        ;

        return self::findIncomingCallTimeByQuery($query);
    }

    public static function findIncomingCallTimeByAdvertiserUnassigned(
        $advertiserId,
        $type = null,
        $from = null,
        $to = null
    ) {

        $query = self::createIncomingCallTimeQuery($type, $from, $to);
        $query
            ->andWhere("pc.advertiser_id = $advertiserId")
            ->andWhere("pc.campaign_id IS NULL")
        ;

        return self::findIncomingCallTimeByQuery($query);
    }

    public static function findIncomingCallTimeByCampaign(
        $campaignId,
        $type = null,
        $from = null,
        $to = null
    ) {

        $query = self::createIncomingCallTimeQuery($type, $from, $to);
        $query
            ->andWhere("pc.campaign_id = $campaignId")
        ;

        return self::findIncomingCallTimeByQuery($query);
    }

    protected static function findIncomingCallTimeByQuery($query) {

        $result = 0;

        $resultFieldName = 'duration';
        $query = self::addSelectRoundedDurationInMinutes($query, $resultFieldName);

        $resultArr = $query->execute(array(), Doctrine_Core::HYDRATE_ARRAY);
        if (count($resultArr)) {
            $result = intval($resultArr[0][$resultFieldName]);
        }

        return $result;
    }

    protected static function addSelectRoundedDurationInMinutes($query, $resultFieldName) {

        $alias = $query->getRootAlias();

//        $query->select("SUM($alias.duration) $resultFieldName");
        $query->select("SUM(CEIL($alias.duration / 60.0)) $resultFieldName");

        return $query;
    }

    protected static function createIncomingCallTimeQuery(
        $type = null,
        $from = null,
        $to = null
    ) {

        $query = self::getInstance()->createQuery('tic')
            ->innerJoin('tic.PhoneCall pc')
            ->innerJoin('tic.TwilioIncomingPhoneNumber tipn')
            ->innerJoin('tipn.TwilioLocalAreaCode tlac')
        ;

        if (!empty($type)) {
            $query->andWhere('tlac.phone_number_type = ?', $type);
        }
        if (!empty($from)) {
            $query->andWhere("tic.start_time >= $from" );
        }
        if (!empty($to)) {
            $query->andWhere("tic.start_time <= $to");
        }

        return $query;
    }

    protected static function createSubTreeIncomingCallTimeQuery(
        $type = null,
        $from = null,
        $to = null
    ) {

        $query = self::getInstance()->createQuery('tic')
            ->innerJoin('tic.PhoneCall pc')
            ->innerJoin('tic.TwilioIncomingPhoneNumber tipn')
            ->innerJoin('tipn.TwilioLocalAreaCode tlac')
        ;

        if (!empty($type)) {
            $query->andWhere('tlac.phone_number_type = ?', $type);
        }
        if (!empty($from)) {
            $query->andWhere("tic.start_time >= $from" );
        }
        if (!empty($to)) {
            $query->andWhere("tic.start_time <= $to");
        }

        return $query;
    }

    /**
     *
     * @param Doctrine_Query $query 
     * @return integer 
     */
    static protected function getAmountTotal(Doctrine_Query $query) {
        return $query->count();
    }

    /**
     *
     * @param Doctrine_Query $query
     * @param String $aliasForDuration
     * @return array of strings
     */
    static protected function getDurationStatistics(Doctrine_Query $query, $aliasForDuration) {

        $fieldNameAvg   = 'duration_avg';
        $fieldNameMax   = 'duration_max';
        $fieldNameTotal = 'duration_total';
//        $fieldNameRound = 'duration_round';
        $query
            ->select("AVG($aliasForDuration.duration) $fieldNameAvg")
            ->addSelect("MAX($aliasForDuration.duration) $fieldNameMax")
            ->addSelect("SUM($aliasForDuration.duration) $fieldNameTotal")
//            ->addSelect("SUM(CEIL($aliasForDuration.duration / 60.0)) $fieldNameRound")
        ;

        $resultArr = $query->execute(array(), Doctrine_Core::HYDRATE_ARRAY);

        $durationInfo = array(
            'avg'   => 0,
            'max'   => 0,
            'total' => 0,
//            'round' => 0,
        );
        if (count($resultArr)) {
            $result = $resultArr[0];
            $durationInfo['avg']    = TimeConverter::convertSecondsToTime($result[$fieldNameAvg]);
            $durationInfo['max']    = TimeConverter::convertSecondsToTime($result[$fieldNameMax]);
            $durationInfo['total']  = TimeConverter::convertSecondsToTime($result[$fieldNameTotal]);
//            $durationInfo['round']  = TimeConverter::convertSecondsToTime(intval($result[$fieldNameRound]) * 60);
        }

        return $durationInfo;
        
    }

    /**
     *
     * @param Doctrine_Query $query 
     * @return array of strings
     */
    static protected function getIncomingDurationStatistics(Doctrine_Query $query) {

        $alias = $query->getRootAlias();

        return self::getDurationStatistics($query, $alias);
    }

    /**
     *
     * @param Doctrine_Query $query 
     * @return array of strings
     */
    static protected function getRedialedDurationStatistics(Doctrine_Query $query) {

        $alias = $query->getRootAlias();
        $query->innerJoin("$alias.TwilioRedialedCall trc");

        return self::getDurationStatistics($query, 'trc');
    }

    /**
     *
     * @param Doctrine_Query $query
     * @param boolean $ifAssignToAdvertiser
     * @param boolean $ifAssignToCampaign
     * @return integer 
     */
    static protected function getAmountAssignedTo(
        Doctrine_Query $query, 
        $ifAssignToAdvertiser = true, 
        $ifAssignToCampaign = true
    ) {

        if ($ifAssignToCampaign && !$ifAssignToAdvertiser) {
            throw new Exception('If phone number belongs to some campaign it belongs to some advertiser always.');
        }

        $aliasPhoneCall = 'pc';
        if ($ifAssignToCampaign) {
            $query->andWhere("$aliasPhoneCall.campaign_id IS NOT NULL");
        } else {
            $query->andWhere("$aliasPhoneCall.campaign_id IS NULL");
            if ($ifAssignToAdvertiser) {
                $query->andWhere("$aliasPhoneCall.advertiser_id IS NOT NULL");
            } else {
                $query->andWhere("$aliasPhoneCall.advertiser_id IS NULL");
            }
        }

        return $query->count();
    }

    static protected function getFieldAliasName(Doctrine_Query $query, $tableAlias, $columnName) {

        $columnName = 'duration';
        $subAliasReal = $query->getSqlTableAlias($tableAlias);

        return $subAliasReal . '__' . $columnName;
    }

    /**
     *
     * @param Doctrine_Query $subQuery 
     * @return array of strings
     */
    static protected function getUniqueIncomingDurationStatistics(Doctrine_Query $subQuery) {

        $subAlias = $subQuery->getRootAlias();
        $subColumnName = 'duration';
        $subFieldAliasReal = self::getFieldAliasName($subQuery, $subAlias, $subColumnName);
        $subQuery
            ->select("$subAlias.$subColumnName")
            ->addOrderBy("$subAlias.start_time ASC")
            ->addGroupBy("$subAlias.twilio_caller_phone_number_id")
        ;

        $alias = 'tmp';
        $subQueryString = $subQuery->getSqlQuery();
//        echo $subQueryString;
//        die;

        $fieldNameTotal = 'duration_total';
        $sqlQueryString = 
            "
            SELECT
                SUM(tmp.$subFieldAliasReal) AS $fieldNameTotal
            FROM (
                $subQueryString
            ) AS tmp
            "
        ;

        $queryResult = array();
        if (!empty($sqlQueryString)) {
            $conn = Doctrine_Manager::getInstance()->getConnection('doctrine');
            $queryResult = $conn->fetchOne($sqlQueryString, array());
        }

        return TimeConverter::convertSecondsToTime($queryResult);
    }

    /**
     *
     * @param Doctrine_Query $query 
     * @return integer 
     */
    static protected function getAmountUniqueLeads(Doctrine_Query $query) {

        $alias = $query->getRootAlias();
        $query
            ->select("$alias.twilio_caller_phone_number_id")
            ->addOrderBy("$alias.start_time ASC")
            ->addGroupBy("$alias.twilio_caller_phone_number_id")
        ;

        return $query->count();
    }

    static public function getOverallValues($advertiser, $company, $filterValues) {
        $type   = null;
        $from   = array_key_exists('from', $filterValues) ? $filterValues['from'] : null;
        $to     = array_key_exists('to', $filterValues) ? $filterValues['to'] : null;

        $query = self::createIncomingCallTimeQuery($type, $from, $to);

        $isBionicCompany = false;
        if (!empty($company)) {
            $isBionicCompany = $company->isBionicCompany();
            if (!$isBionicCompany) {
                $query
                    ->innerJoin('pc.Advertiser adv')
                    ->innerJoin('adv.Company com')
                    ->andWhere("com.lft BETWEEN {$company->getLft()} AND {$company->getRgt()}")
                ;
            }
        } elseif (!empty($advertiser)) {
            $advertiserId = $advertiser->getId();
            $query
                ->andWhere("pc.advertiser_id = $advertiserId")
            ;
        } else {
            throw new Exception('user must belong to some company or advertiser');
        }

        $callsTotal                 = self::getAmountTotal($query->copy());
        $uniqueLeads                = self::getAmountUniqueLeads($query->copy());
        $incomingDurationUnique     = self::getUniqueIncomingDurationStatistics($query->copy());
        $callsAssigned              = self::getAmountAssignedTo($query->copy(), true, true);
        $callsUnassignedAdvOwner    = self::getAmountAssignedTo($query->copy(), true, false);
        $callsUnassignedBionicOwner = null;
        if ($isBionicCompany) {
            $callsUnassignedBionicOwner = self::getAmountAssignedTo($query->copy(), false, false);
        }

        $incomingDurationStatistics = self::getIncomingDurationStatistics($query->copy());
        $incomingDurationAvg    = $incomingDurationStatistics['avg'];
        $incomingDurationMax    = $incomingDurationStatistics['max'];
        $incomingDurationTotal  = $incomingDurationStatistics['total'];
//        $incomingDurationRound  = $incomingDurationStatistics['round'];

        $redialedDurationStatistics = self::getRedialedDurationStatistics($query->copy());
        $redialedDurationAvg    = $redialedDurationStatistics['avg'];
        $redialedDurationMax    = $redialedDurationStatistics['max'];
        $redialedDurationTotal  = $redialedDurationStatistics['total'];
//        $redialedDurationRound  = $redialedDurationStatistics['round'];

        return array(
            'calls_total'                       => $callsTotal,
            'leads_unique'                      => $uniqueLeads,
            'duration_incoming_unique'          => $incomingDurationUnique,
            'calls_assigned'                    => $callsAssigned,
            'calls_unassigned_advertiser_owner' => $callsUnassignedAdvOwner,
            'calls_unassigned_bionic_owner'     => $callsUnassignedBionicOwner,
            'incoming_duration_avg'             => $incomingDurationAvg,
            'incoming_duration_max'             => $incomingDurationMax,
//            'incoming_duration_round'           => $incomingDurationRound,
            'incoming_duration_total'           => $incomingDurationTotal,
            'redialed_duration_avg'             => $redialedDurationAvg,
            'redialed_duration_max'             => $redialedDurationMax,
            'redialed_duration_total'           => $redialedDurationTotal,
//            'redialed_duration_round'           => $redialedDurationRound,
        );
    }

    public static function findCallStatuses() {

        $callStatuses = self::getInstance()->getEnumValues('call_status');

        $result = array();
        foreach ($callStatuses as $callStatus) {
            $callStatusCode = $callStatus;
            $result[$callStatusCode] = $callStatus;
        }

        return $result;
    }

    public static function findUsefulStatuses() {

        $result = self::findCallStatuses();

//        unset($result['not yet dialed']);
//        unset($result['failed']);

        return $result;
    }

    /**
     * 
     * Returns amount of calls during last N days since $to or now datetime in GMT0 for all need $tipnIds
     *
     * @param mixed $tipnIds array or int or string twilio incoming phone number ids
     * @param int $daysAmount 
     * @param string $to in format 'YYYY-mm-dd hh:mm:ss' in GMT0, if null then will be set to 'now' in GMT0
     * 
     * @return array (tipnId => amount of calls during last N days)
     */
    public static function createQueryAmountOfCallsToPhoneNumbersDuringLastNDays(
        $query,
        $aliasForTic, 
        $fieldNameAmount,
        $fieldNameTipnId,
        $daysAmount, 
        $to = ''
    ) {

        $needTzDiff = DataBaseTimezoneHelper::DEFAULT_OFFSET;

        $unixDateString = "FROM_UNIXTIME($aliasForTic.start_time)";
        $dateString = DataBaseTimezoneHelper::configureConvertedFromUnixDate($unixDateString, $needTzDiff);

        if (empty($to)) {
            $to = DataBaseTimezoneHelper::configureConvertedFromUnixDate('NOW()', $needTzDiff);
        } else {
            $to = "'$to'";
        }

        $query
            ->addSelect("COUNT($aliasForTic.id) AS $fieldNameAmount")
            ->addSelect("$aliasForTic.twilio_incoming_phone_number_id AS $fieldNameTipnId")
            ->andWhere("$dateString <= $to")
            ->andWhere("$dateString >= DATE_SUB($to, INTERVAL $daysAmount day)")
            ->addGroupBy("$aliasForTic.twilio_incoming_phone_number_id")
            ->addOrderBy("$aliasForTic.twilio_incoming_phone_number_id")
        ;
        
        return $query;
    }

    /**
     * NOTE:: is public for tests only.
     * 
     * Returns amount of calls during last N days since $to or now datetime in GMT0 for all need $tipnIds
     *
     * @param mixed $tipnIds array or int or string twilio incoming phone number ids
     * @param int $daysAmount 
     * @param string $to in format 'YYYY-mm-dd hh:mm:ss' in GMT0, if null then will be set to 'now' in GMT0
     * 
     * @return array (tipnId => amount of calls during last N days)
     */
    public static function getAmountOfCallsToPhoneNumbersDuringLastNDays($tipnIds, $daysAmount, $to = '') {

        $result = array();
        if (empty($tipnIds)) {
            return $result;
        }

        $tipnIds = is_array($tipnIds) ? $tipnIds : array($tipnIds);
        $tipnIdAsString = implode(',', $tipnIds);

        $fieldName = 'amount';
        $fieldNameTipnId = 'twilio_incoming_phone_number_id';

        $query = self::getInstance()->createQuery('tic');
        $query = self::createQueryAmountOfCallsToPhoneNumbersDuringLastNDays(
            $query,
            'tic', 
            $fieldName,
            $fieldNameTipnId,
            $daysAmount, 
            $to
        );
        $query
            ->andWhere("tic.twilio_incoming_phone_number_id IN ($tipnIdAsString)")
        ;

        $res = $query->execute(array(), Doctrine_Core::HYDRATE_ARRAY);


        foreach ($tipnIds as $tipnIds) {
            $result[$tipnIds] = 0;
        }

        if (!empty($res)) {
            foreach ($res as $callsAmountForPhoneNumber) {
                $id = $callsAmountForPhoneNumber[$fieldNameTipnId];
                $result[$id] = $callsAmountForPhoneNumber[$fieldName];
            }
        }
        
        return $result;
    }

    /**
     *
     * Returns last call's datetime in need format for all need $tipnIds
     * 
     * @param mixed $tipnIds array or int or string twilio incoming phone number ids
     * @param string $timezoneName
     * @param string $format of result datetimes
     * @return array datetime  array(tipnId => last call's datetime)
     */
    public static function createQueryDatetimeOfLastCallsToPhoneNumbers(
        $query, 
        $aliasForTic, 
        $timezoneName, 
        $format,
        $fieldNameStartTime,
        $fieldNameTipnId
    ) {

        $query
            ->addSelect("MAX($aliasForTic.start_time) AS $fieldNameStartTime")
            ->addSelect("tic.twilio_incoming_phone_number_id AS $fieldNameTipnId")
            ->addGroupBy("$aliasForTic.twilio_incoming_phone_number_id")
            ->addOrderBy("$aliasForTic.twilio_incoming_phone_number_id")
        ;

        return $query;
    }

    /**
     * NOTE:: is public for tests only.
     *
     * Returns last call's datetime in need format for all need $tipnIds
     * 
     * @param mixed $tipnIds array or int or string twilio incoming phone number ids
     * @param string $timezoneName
     * @param string $format of result datetimes
     * @return array datetime  array(tipnId => last call's datetime)
     */
    public static function findDatetimeOfLastCallsToPhoneNumbers($tipnIds, $timezoneName, $format) {

        $result = array();
        if (empty($tipnIds)) {
            return $result;
        }

        $tipnIds = is_array($tipnIds) ? $tipnIds : array($tipnIds);
        $tipnIdAsString = implode(',', $tipnIds);

        $fieldName = 'start_time';
        $fieldNameTipnId = 'twilio_incoming_phone_number_id';

        $query = self::getInstance()->createQuery('tic');
        $query = self::createQueryDatetimeOfLastCallsToPhoneNumbers($query, 'tic', $timezoneName, $format, $fieldName, $fieldNameTipnId);
        $query
            ->andWhere("tic.twilio_incoming_phone_number_id IN ($tipnIdAsString)")
        ;

        $res = $query->execute(array(), Doctrine_Core::HYDRATE_ARRAY);


        foreach ($tipnIds as $tipnIds) {
            $result[$tipnIds] = '';
        }

        if (!empty($res)) {
            foreach ($res as $info) {
                $id = $info[$fieldNameTipnId];
                $result[$id] = TimeConverter::getDateTime($info[$fieldName], $timezoneName, $format);
            }
        }

        return $result;
    }

    /**
     *
     * @param Doctrine_Query $query query to select need twilio incoming phone numbers
     * @param string $timezoneName
     * @param string $format of result datetimes
     * @return array(tipnId => array('amount_7' => amount of calls during last 7 days, 'amount_30' => amount of calls during last 30 days, 'last_call' => datetime of last call))
     */
    public static function findLastCallsInfo(Doctrine_Query $query, $timezoneName, $format) {

        $query = $query->copy();
        $alias = $query->getRootAlias();
        $query->select("$alias.id");

        $ids = $query->execute(array(), Doctrine_Core::HYDRATE_SINGLE_SCALAR);

        $ids = is_array($ids) ? $ids : array($ids);

        $amounts7   = self::getAmountOfCallsToPhoneNumbersDuringLastNDays($ids, 7);
        $amounts30  = self::getAmountOfCallsToPhoneNumbersDuringLastNDays($ids, 30);
        $lastCalls  = self::findDatetimeOfLastCallsToPhoneNumbers($ids, $timezoneName, $format);

        $result = array();
        foreach ($ids as $tipnId) {
            $result[$tipnId] = array(
                'amount_7'  => (array_key_exists($tipnId, $amounts7) ? $amounts7[$tipnId] : 0),
                'amount_30' => (array_key_exists($tipnId, $amounts30) ? $amounts30[$tipnId] : 0),
                'last_call' => (array_key_exists($tipnId, $lastCalls) ? $lastCalls[$tipnId] : ''),
            );
        }

        return $result;
    }

    protected static function createQueryOldCalls() {

         $seconds = sfConfig::get('app_twilio_incoming_call_is_old_interval_seconds', 24 * 60 * 60);
         $now = TimeConverter::getUnixTimestamp();
         $from = $now - $seconds;

         $query = self::getInstance()->createQuery('tic')
             ->andWhere("tic.start_time <= $from")
             ->addOrderBy('tic.start_time DESC')
         ;

         return $query;
    }

    public static function findOldNotUpdatedInProgressCalls() {

         $query = self::createQueryOldCalls();
         $alias = $query->getRootAlias();

         $query->andWhere("$alias.call_status = '" . self::STATUS_IN_PROGRESS . "'");

         return $query->execute();
    }

    public static function findOldNotUpdatedCalls() {

         $query = self::createQueryOldCalls();
         $alias = $query->getRootAlias();

         $query->andWhere("$alias.end_time IS NULL");

         return $query->execute();
    }

    protected static function createQueryPhoneCallsActual($from = null, $to = null) {

        $query = self::getInstance()->createQuery('tic')

            ->leftJoin('tic.PhoneCall pc')
            ->leftJoin('tic.TwilioCallQueue tcq')

            ->select('tic.id AS id')
            ->addSelect('tic.start_time AS start_time')
            ->addSelect('tic.twilio_incoming_phone_number_id AS tipn_id')

            ->where('tic.end_time IS NOT NULL')
            ->andWhere('tic.call_status != "' . TwilioIncomingCallTable::STATUS_IN_PROGRESS . '"')
            ->andWhere('tcq.id IS NULL')

            ->addOrderBy('tic.start_time DESC')
        ;

        if (!empty($from)) {
            $query->andWhere("tic.start_time >= $from");
        }

        if (!empty($to)) {
            $query->andWhere("tic.start_time <= $to");
        }

        return $query;
    }

    public static function findWithoutPhoneCall($from = null, $to = null) {

        $query = self::createQueryPhoneCallsActual($from, $to)
            ->where('pc.id IS NULL')
        ;

        return $query->execute();
    }

    /**
     * 
     * @param int $advertiserId id (adevrtiser with id) or 0 (all advertisers) or null (no one). If null then it does not matter what campaign is set.
     * @param int $campaignId id (campaign with id) or 0 (all campaigns) or null (no one).
     * @param int $from timestamp
     * @param int $to timestamp
     * @return type 
     */
    public static function findPhoneCallsActualWithPhoneCall($advertiserId, $campaignId, $from = null, $to = null) {

        $query = self::createQueryPhoneCallsActual($from, $to)
            ->where('pc.id IS NOT NULL')
        ;

        if (is_null($advertiserId)) {
            $query->andWhere("pc.advertiser_id IS NULL");
        } else {
            if (!empty($advertiserId)) {
                $query->andWhere("pc.advertiser_id = $advertiserId");
            }

            if (is_null($campaignId)) {
                $query->andWhere("pc.campaign_id IS NULL");
            } elseif (!empty($campaignId)) {
                $query->andWhere("pc.campaign_id = $campaignId");
            }
        }

        return $query->execute();
    }

    /**
     * NOTE: for tests only
     * 
     * @param type $tipnId
     * @param type $exectlyStartTime
     * @return type 
     */
    public static function findOneByTipnAndTime($tipnId, $exectlyStartTime) {

        $query = self::getInstance()->createQuery('tic')
            ->andWhere("tic.twilio_incoming_phone_number_id = $tipnId")
            ->andWhere("tic.start_time = $exectlyStartTime")
            ->limit(1)
        ;

        return $query->fetchOne();
    }
}
