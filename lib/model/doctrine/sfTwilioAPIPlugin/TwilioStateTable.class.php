<?php


class TwilioStateTable extends PluginTwilioStateTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('TwilioState');
    }
}