<?php

class TwilioCallRecordingTable extends PluginTwilioCallRecordingTable {

    public static function getInstance() {
        return Doctrine_Core::getTable('TwilioCallRecording');
    }
}