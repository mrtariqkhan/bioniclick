<?php

class TwilioRedialedCallTable extends PluginTwilioRedialedCallTable {
    
    public static function getInstance() {
        return Doctrine_Core::getTable('TwilioRedialedCall');
    }

    public static function saveRedialedCall($callParameters, $incomingCall) {

        $success = true;

        $redialedCall = new TwilioRedialedCall();

        $redialedCall->setSegmentSid($callParameters['call_segment_sid']);
        if (!isset($incomingCall)) {
            Loggable::putLogMessage(ApplicationLogger::TWILIO_LOG_TYPE, "You try to save redialedCall for nonexistent incomingCall.", sfLogger::ERR);
            $success = false;
        } else {
            $redialedCall->setParentCall($incomingCall);
        }
        $redialedCall->setCalledNumber($callParameters['called_number']);

        $startTime  = TimeConverter::getDateTimeFormattedByString($callParameters['start_time']);
        $endTime    = TimeConverter::getDateTimeFormattedByString($callParameters['end_time']);
        $redialedCall->setStartTime($startTime);
        $redialedCall->setEndTime($endTime);

        $duration = empty($callParameters['duration']) ? 0 : $callParameters['duration'];
        $redialedCall->setDuration($duration);

        $price = empty($callParameters['price']) ? 0 : $callParameters['price'];
        $redialedCall->setPrice($price);

        if (!$success) {
            return null;
        }

        try {
            $redialedCall->save();
        } catch (Exception $e) {
            Loggable::putExceptionFullLogMessage($e, ApplicationLogger::TWILIO_LOG_TYPE);
            return null;
        }

        return $redialedCall;
    }
}