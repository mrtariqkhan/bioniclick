<?php

class TwilioCallerPhoneNumberTable extends PluginTwilioCallerPhoneNumberTable {
    
    public static function getInstance() {
        return Doctrine_Core::getTable('TwilioCallerPhoneNumber');
    }

    /**
     *
     * @return <Doctrine_Query> query to select ordered collection of PluginTwilioCallerPhoneNumbers
     */
    public static function getIndexQuery($user, $filterValues = array()) {
      
        $query = self::getInstance()->createQuery('tcpn')
            ->innerJoin('tcpn.TwilioIncomingCalls tic')
            ->innerJoin('tic.PhoneCall pc')
            ->orderBy('tcpn.phone_number ASC')
        ;

        if (array_key_exists('phone_number', $filterValues)) {
            $value = mysql_escape_string($filterValues['phone_number']);
            $query->andWhere("(tcpn.phone_number LIKE '%$value%')");
        }

        if ($user->isAdvertiserUser()) {
            $advertiserId = $user->getAdvertiserId();
            $query->andWhere("pc.advertiser_id = $advertiserId");
        } elseif ($user->isCompanyUser()) {
            $company = $user->getCompany();
            $query
                ->innerJoin('pc.Advertiser adv')
                ->innerJoin('adv.Company com')
                ->andWhere("(com.lft BETWEEN {$company->getLft()} AND {$company->getRgt()})")
            ;
        } else {
            $query->andWhere(false);
        }

        return $query;
    }

}