<?php

class TwilioLocalAreaCodeTable extends PluginTwilioLocalAreaCodeTable {

    const TYPE_SHORT_TOLL_FREE  = 'toll';
    const TYPE_SHORT_LOCAL      = 'local';
    const TYPE_SHORT_SOME       = null;

    const TYPE_TOLL_FREE        = 'toll free';
    const TYPE_LOCAL            = 'local';
    const TYPE_SOME             = null;

    const TYPE_NAME_TOLL_FREE   = 'toll-free';
    const TYPE_NAME_LOCAL       = 'local';
    const TYPE_NAME_SOME        = null;
    
    /**
     * Creates an instance of the Table class
     * @return Doctrine_Table
     */
    public static function getInstance() {
        return Doctrine_Core::getTable('TwilioLocalAreaCode');
    }

    /**
     * Finds and returns local area code by Id
     * @param String $id local area code Id
     * @return TwilioLocalAreaCode
     */
    public function findById($id) {
        return $this->findOneBy('id', $id);
    }

    /**
     * Finds and returns local area code by Id
     * @param String $id local area code Id
     * @return TwilioLocalAreaCode
     */
    public function findByCode($code) {
        return $this->findOneBy('code', $code);
    }

    public static function createQueryExistentInAdvertiserPoolByPhoneNumberType(
        $advertiserId,
        $phoneNumberType = null
    ) {

        $query = self::createQueryExistentByPhoneNumberType($phoneNumberType);
        $alias = $query->getRootAlias();
        $query
            ->innerJoin("$alias.TwilioIncomingPhoneNumbers tipn")
            ->innerJoin('tipn.AdvertiserIncomingNumberPool ainp')
            ->andWhere('ainp.advertiser_id = ' . $advertiserId)
            ->andWhere('ainp.campaign_id is null')
            ->andWhere('tipn.was_deleted = 0')
        ;

        return $query;
    }

    protected static function createQueryExistentInBionicPoolByPhoneNumberType(
        $phoneNumberType = null
    ) {

        $query = self::createQueryExistentByPhoneNumberType($phoneNumberType);
        $alias = $query->getRootAlias();
        $query
            ->innerJoin("$alias.TwilioIncomingPhoneNumbers tipn")
            ->innerJoin('tipn.BionicIncomingNumberPool binp')
            ->andWhere('binp.is_owner = 1')
            ->andWhere('tipn.was_deleted = 0')
        ;

        return $query;
    }

    public static function createQueryExistentByPhoneNumberType(
        $phoneNumberType = null
    ) {

        $query = self::getInstance()->createQuery('tlac')
            ->select('tlac.id, tlac.code')
            ->distinct()
            ->leftJoin('tlac.TwilioState ts')
            ->addSelect('ts.name as state_name')
            ->leftJoin('ts.TwilioCountry tc')
            ->addSelect('tc.name as country_name')
            ->orderBy('tlac.code asc')
        ;

        if (!empty($phoneNumberType)) {
            $query->andWhere('tlac.phone_number_type = "' . $phoneNumberType . '"');
        }

        return $query;
    }

    public static function findExistentInAdvertiserPoolByPhoneNumberType(
        $advertiserId,
        $phoneNumberType = null
    ) {

        $query = self::createQueryExistentInAdvertiserPoolByPhoneNumberType(
            $advertiserId,
            $phoneNumberType
        );

        Loggable::putQueryLogMessage($query);

        return $query->fetchArray();
    }

    public static function findExistentInBionicPoolByPhoneNumberType($phoneNumberType = null) {

        $query = self::createQueryExistentInBionicPoolByPhoneNumberType(
            $phoneNumberType
        );

        Loggable::putQueryLogMessage($query);

        return $query->fetchArray();
    }

    public static function findByPhoneNumberType($phoneNumberType = null) {

        $query = self::createQueryExistentByPhoneNumberType(
            $phoneNumberType
        );

        Loggable::putQueryLogMessage($query);

        return $query->fetchArray();
    }

    /**
     *
     * @param type $typeShortName one of self::TYPE_SHORT_*
     * @return one of self::TYPE_*
     */
    public static function getNameByShortName($typeShortName) {
        switch ($typeShortName) {
            case self::TYPE_SHORT_TOLL_FREE:
                return self::TYPE_TOLL_FREE;
            case self::TYPE_SHORT_LOCAL:
                return self::TYPE_LOCAL;
            default:
                return self::TYPE_SOME;
        }
    }

    /**
     *
     * @param type $type one of self::TYPE_*
     * @return one of self::TYPE_SHORT_*
     */
    public static function getShortNameByName($type) {
        switch ($type) {
            case self::TYPE_TOLL_FREE:
                return self::TYPE_SHORT_TOLL_FREE;
            case self::TYPE_LOCAL:
                return self::TYPE_SHORT_LOCAL;
            default:
                return self::TYPE_SHORT_SOME;
        }
    }

    public static function findPhoneNumberTypesCoded() {

        $types = self::getInstance()->getEnumValues('phone_number_type');

        $result = array();
        foreach ($types as $type) {
            $typeCode = self::getShortNameByName($type);
            $result[$typeCode] = $type;
        }

        return $result;
    }

    protected static function findPhoneNumberTypes() {
        return self::getInstance()->getEnumValues('phone_number_type');
    }

    public static function findPhoneNumberTypesCodedByAdvertiserId($advertiserId) {

        $types = self::findPhoneNumberTypes();

        $result = array();
        foreach ($types as $type) {
            $query = self::createQueryExistentInAdvertiserPoolByPhoneNumberType(
                $advertiserId,
                $type
            );
            $count = $query->count();
            if ($count > 0) {
                $typeCode = self::getShortNameByName($type);
                $result[$typeCode] = $type;
            }
        }

        return $result;
    }

    public static function findPhoneNumberCountriesByAdvertiserId($advertiserId) {

        $query = TwilioCountryTable::getInstance()->createQuery('tc')
            ->innerJoin('tc.TwilioStates ts')
            ->innerJoin('ts.TwilioLocalAreaCodes tlac')
            ->innerJoin('tlac.TwilioIncomingPhoneNumbers tipn')
            ->andWhere('tipn.was_deleted = 0')
            ->innerJoin('tipn.AdvertiserIncomingNumberPool ainp')
            ->andWhere('ainp.advertiser_id = ' . $advertiserId)
            ->andWhere('ainp.campaign_id is null')
            ->orderBy('tc.id')
        ;

        $namesWithIds = $query->execute(array(), Doctrine_Core::HYDRATE_ARRAY);
        $names = array();
        foreach ($namesWithIds as $nameWithId) {
            $names[$nameWithId['id']] = $nameWithId['name'];
        }

        return $names;
    }

    //TODO:: I think it is unnecessary now
    public static function findPhoneNumberTypesForBionic() {

        $types = self::findPhoneNumberTypes();

        $result = array();
        foreach ($types as $type) {
//            $query = self::createQueryExistentInBionicPoolByPhoneNumberType($type);
//            $count = $query->count();
//            if ($count > 0) {
                $result[$type] = $type;
//            }
        }

        return $result;
    }


    protected static function createQueryForSelect(
        $q,
        $limit,
        $typeShortName = null,
        $countryId = null
    ) {

        $q = trim($q);

        $query = self::getInstance()->createQuery('tlac')
            ->leftJoin('tlac.TwilioState ts')
            ->addOrderBy('code')
            ->limit($limit)
        ;

        if (empty($typeShortName) || $typeShortName != TwilioLocalAreaCodeTable::TYPE_SHORT_TOLL_FREE) {
            if (!empty($countryId)) {
                $query->andWhere("ts.twilio_country_id = $countryId");
            }
        }

        $type = TwilioLocalAreaCodeTable::getNameByShortName($typeShortName);
        if (!empty($type)) {
            $query->andWhere("phone_number_type like '%$type%'");
        }

        if (empty($q) && $q != "0") {
            $query->addGroupBy('SUBSTR(CAST(code AS CHAR(8)), 1, 1)');
        } else {
            $query->andWhere("code like '$q%'");
        }
//        $dql = $query->getDql();
//        $sql = $query->getSqlQuery();

        return $query;
    }

    protected static function findResultForSelect($query) {
        
        $areaCodes = $query->execute();

        $result = array();
        foreach ($areaCodes as $areaCode) {
            $result[$areaCode->getId()] = (string)$areaCode;
        }

        return $result;
    }

    /**
     *
     * @param type $q
     * @param type $limit
     * @param type $typeShortName one of TwilioLocalAreaCodeTable::TYPE_SHORT*
     * @param type $countryId
     * @return type 
     */
    public static function findForSelectToFilter(
        $q,
        $limit,
        $typeShortName = null,
        $countryId = null
    ) {

        $query = self::createQueryForSelect($q, $limit, $typeShortName, $countryId);
        $query
            ->innerJoin('tlac.TwilioIncomingPhoneNumbers tipn')
            ->andWhere('tipn.was_deleted = 0')
        ;

        return self::findResultForSelect($query);
    }

    /**
     *
     * @param type $q
     * @param type $limit
     * @param type $typeShortName one of TwilioLocalAreaCodeTable::TYPE_SHORT*
     * @param type $countryId
     * @return type 
     */
    public static function findForSelectToFreeFilter(
        $q,
        $limit,
        $typeShortName = null,
        $countryId = null
    ) {

        $query = self::createQueryForSelect($q, $limit, $typeShortName, $countryId);
        $query
            ->innerJoin('tlac.TwilioIncomingPhoneNumbers tipn')
            ->andWhere('tipn.was_deleted = 0')
            ->innerJoin('tipn.BionicIncomingNumberPool binp')
            ->andWhere('binp.is_owner = true')
        ;

        return self::findResultForSelect($query);
    }

    /**
     *
     * @param type $q
     * @param type $limit
     * @param type $typeShortName one of TwilioLocalAreaCodeTable::TYPE_SHORT*
     * @param type $countryId
     * @return type 
     */
    public static function findForSelectToCreate(
        $q,
        $limit,
        $typeShortName = null,
        $countryId = null
    ) {

        $query = self::createQueryForSelect($q, $limit, $typeShortName, $countryId);

        return self::findResultForSelect($query);
    }

    public static function findForSelectForCampaign(
        $q,
        $limit,
        $typeShortName = null,
        $countryId = null,
        $advertiserId
    ) {

        $query = self::createQueryForSelect($q, $limit, $typeShortName, $countryId);

        $query
            ->select('tlac.id, tlac.code')
            ->distinct()
            ->innerJoin('tlac.TwilioIncomingPhoneNumbers tipn')
            ->andWhere('tipn.was_deleted = 0')
            ->innerJoin('tipn.AdvertiserIncomingNumberPool ainp')
            ->andWhere('ainp.advertiser_id = ' . $advertiserId)
            ->andWhere('ainp.campaign_id is null')
        ;

        return self::findResultForSelect($query);
    }
}