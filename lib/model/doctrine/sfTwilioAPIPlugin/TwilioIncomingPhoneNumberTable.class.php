<?php
/**
 * Extends sfTwilioPlugin class for custom methods
 */
class TwilioIncomingPhoneNumberTable extends PluginTwilioIncomingPhoneNumberTable {
    
    /**
     * Creates an instance of the Table class
     * @return Doctrine_Table
     */
    public static function getInstance() {
        return Doctrine_Core::getTable('TwilioIncomingPhoneNumber');
    }

    /**
     * Finds and returns incoming phone number by Sid
     * @param String $sid incoming phone number Sid
     * @return TwilioIncomingPhoneNumber
     */
    public static function findBySid($sid) {

        $query = self::getInstance()->createQuery()
            ->andWhere('sid = ?', $sid)
        ;
        
        return $query->fetchOne();
    }   

    /**
     * Creates and returns query object to retrieve all entities
     * @return Doctrine_Query
     */
    protected static function createQueryForAll($query = null, $wasDeletedType = WasDeletedHelper::TYPE_NON_DELETED) {

        if (empty($query)) {
            $query = self::getInstance()->createQuery('tipn');
        }

        if ($wasDeletedType != WasDeletedHelper::TYPE_ALL) {
            $query->andWhere("was_deleted = $wasDeletedType");
        }

        $query->orderBy('phone_number asc');

        return $query;
    }

    public static function createIndexQuery(
        sfGuardUser $user, 
        $convertedPurchasedDateColumnName,
        $query
    ) {

        $rootAlias = $query->getRootAlias();

        $needTzDiff = $user->findGmtDifference();
        $unixDateString = "FROM_UNIXTIME($rootAlias.purchased_date)";
        $dateString = DataBaseTimezoneHelper::configureConvertedFromUnixDate($unixDateString, $needTzDiff);

        $query
            ->select("$rootAlias.*")
            ->addSelect("$dateString as $convertedPurchasedDateColumnName")
        ;

        return self::createQueryForAll($query, WasDeletedHelper::getType($user));
    }

    public static function createFreeIndexQuery(
        sfGuardUser $user, 
        $convertedPurchasedDateColumnName,
        $convertedCleaningDateColumnName,
        $query = null
    ) {

        if (empty($query)) {
            $query = self::getInstance()->createQuery('tipn');
        }

        $rootAlias = $query->getRootAlias();

        $needTzDiff = $user->findGmtDifference();

        $purchasedUnixDateString = "FROM_UNIXTIME($rootAlias.purchased_date)";
        $purchasedDateString = DataBaseTimezoneHelper::configureConvertedFromUnixDate($purchasedUnixDateString, $needTzDiff);

        $cleaningUnixDateString = "FROM_UNIXTIME(binp.added_for_cleaning_date)";
        $cleaningDateString = DataBaseTimezoneHelper::configureConvertedFromUnixDate($cleaningUnixDateString, $needTzDiff);

        $query
            ->select("$rootAlias.*")
            ->addSelect("$purchasedDateString as $convertedPurchasedDateColumnName")
            ->addSelect("$cleaningDateString as $convertedCleaningDateColumnName")
            ->innerJoin("$rootAlias.BionicIncomingNumberPool binp")
            ->andWhere('binp.is_owner = true')
        ;

        return self::createQueryForAll($query, WasDeletedHelper::getType($user));
    }        
   
    /**
     * Returns all entities out of given list of Sid field values
     * @return Doctrine_Query
     */
    public static function findAllOutOfSidList($numberSidArray) {

        return self::getInstance()
            ->createQuery('tipn')
            ->whereNotIn('tipn.sid', $numberSidArray)
            ->addWhere('tipn.was_deleted = 0')
            ->execute()
        ;
    }

    /**
     * Returns query to select entities which belong to stranger projects
     * @return Doctrine_Query
     */
    protected static function createQueryExistentNumbersFromStrangerProjects($wasDeleted) {

        $query = self::getInstance()
            ->createQuery('tipn')
            ->addWhere('tipn.was_deleted = ?', $wasDeleted)
            ->addOrderBy('tipn.url, tipn.id')
        ;

        $fieldQueryName = 'tipn.url';
        $availableProjectBeginnings = sfConfig::get('app_available_project_urls_beginnings', array());
        foreach ($availableProjectBeginnings as $availableProjectBeginning) {
            $value = mysql_escape_string($availableProjectBeginning);
            $query->andWhere("$fieldQueryName NOT LIKE '$value%'");
        }

        return $query;
    }

    /**
     * Returns all existent entities which belong to stranger projects
     * @return Doctrine_Query
     */
    public static function findExistentNumbersFromStrangerProjects() {

        $query = self::createQueryExistentNumbersFromStrangerProjects(WasDeletedHelper::TYPE_NON_DELETED);

        return $query->execute();
    }

    /**
     * Returns count of all (deleted and non-deleted) entities which belong to stranger projects
     * @return Doctrine_Query
     */
    public static function countNumbersFromStrangerProjects() {

        $query = self::createQueryExistentNumbersFromStrangerProjects(WasDeletedHelper::TYPE_ALL);

        return $query->count();
    }

    /**
     *
     * @param String $url
     */
    public static function isStrangerProjectUrl($url) {

        $availableProjectBeginnings = sfConfig::get('app_available_project_urls_beginnings', array());
        foreach ($availableProjectBeginnings as $availableProjectBeginning) {
            $value = str_replace('/', '\/', $availableProjectBeginning);
            $pattern = "/^$value/";
            if (preg_match($pattern, $url)) {
                return false;
            }
        }

        return true;
    }

    public static function calc($wasDeleted = WasDeletedHelper::TYPE_NON_DELETED) {
        return self::createQueryForAll(null, $wasDeleted)->count();
    }

    protected static function createQueryBionicFreeNumbersByLocalAreaCode(
        $localAreaCodeId = null,
        $limit = null
    ) {

        $query = self::createQueryForAll()
            ->select('tipn.id, tipn.phone_number')
            ->innerJoin('tipn.BionicIncomingNumberPool binp')
            ->andWhere('binp.is_owner = 1')
        ;        

        if (!empty($localAreaCodeId)) {
            $query->addWhere("tipn.twilio_local_area_code_id = $localAreaCodeId");
        }
        
        if (!empty($limit)) {
            $query->limit($limit);
        }

        return $query;
    }

    public static function createQueryBionicFreeAvailableNumbersByLocalAreaCode(
        $localAreaCodeId = null, 
        $limit = null
    ) {

        $query = self::createQueryBionicFreeNumbersByLocalAreaCode(
            $localAreaCodeId,
            $limit
        );

        $aliasName = $query->getRootAlias();
        $query->leftJoin("$aliasName.TwilioIncomingCalls tic");
        $query->orderBy('tic.start_time ASC');
        $query->addOrderBy("$aliasName.phone_number ASC");
        $query->groupBy("$aliasName.phone_number");

        $fieldQueryName = 'tipn.url';
        $conditions = array();
        $availableProjectBeginnings = sfConfig::get('app_available_project_urls_beginnings', array());
        foreach ($availableProjectBeginnings as $availableProjectBeginning) {
            $value = mysql_escape_string($availableProjectBeginning);
            $conditions [] = "$fieldQueryName LIKE '$value%'";
        }
        $condition = implode(' OR ', $conditions);
        $condition = empty($condition) ? 'false' : $condition;
        $query->andWhere($condition);

        return $query;
    }

//    public static function createQueryBionicFreeAvailableNumbersByLocalAreaCodeWithStatisticsForSelect(
//        $localAreaCodeId, 
//        $timezoneName, 
//        $format, 
//        $limit = null
//    ) {
//      //TODO:: write this query without Doctrine, because we cannot use subqueries in FROM-part
//
//        $query = self::createQueryBionicFreeAvailableNumbersByLocalAreaCode(
//            $localAreaCodeId,
//            $limit
//        );
//
//        $aliasName = $query->getRootAlias();
//        $query->select("$aliasName.phone_number");
//        $query
////            ->addSelect("$aliasName.phone_number as phone_number1")
//            ->addSelect("$aliasName.phone_number as phone_number2")
//            ->addSelect("$aliasName.phone_number as phone_number3")
//        ;
//
//        $query1 = TwilioIncomingCallTable::createQueryAmountOfCallsToPhoneNumbersDuringLastNDays(
//            $query->copy(), 
//            'tic', 
//            'phone_number1',
//            'tipn_id',
//            7
//        );
//        $query2 = TwilioIncomingCallTable::createQueryAmountOfCallsToPhoneNumbersDuringLastNDays(
//            $query->copy(), 
//            'tic', 
//            'phone_number2',
//            'tipn_id',
//            30
//        );
//        $query3 = TwilioIncomingCallTable::createQueryDatetimeOfLastCallsToPhoneNumbers(
//            $query->copy(), 
//            'tic', 
//            $timezoneName, 
//            $format, 
//            'phone_number3',
//            'tipn_id'
//        );
//
//        return $query;
//    }

    /**
     * Method finds phone numbers with $desiredTipnIds first of all. If
     * they are then they will be used before of all other.
     * 
     * @param type $localAreaCodeId
     * @param type $desiredTipnIds
     * @param type $limit
     * @return type 
     */
    public static function findBionicFreeAvailableNumbersByLocalAreaCodeDesiredOrNot(
        $localAreaCodeId = null, 
        $desiredTipnIds = array(),
        $limit = null
    ) {

        $tipnIds = $desiredTipnIds;

        $phoneNumbersDesired = self::findBionicFreeAvailableNumbersByLocalAreaCode(
            $localAreaCodeId, 
            $tipnIds, 
            true,
            $limit
        );

        $phoneNumbersAny = array();
        $numbersCountOfAny = $limit - count($phoneNumbersDesired);
        if ($numbersCountOfAny > 0) {
            $phoneNumbersAny = self::findBionicFreeAvailableNumbersByLocalAreaCode(
                $localAreaCodeId, 
                $tipnIds, 
                false, 
                $numbersCountOfAny
            );
        }

        $phoneNumbersDesiredOrNot = array_merge($phoneNumbersDesired, $phoneNumbersAny);

        return $phoneNumbersDesiredOrNot;
    }

    protected static function findBionicFreeAvailableNumbersByLocalAreaCode(
        $localAreaCodeId = null, 
        $tipnIds = array(),
        $tipnIdsAreDesired = true,
        $limit = null
    ) {

        $result = array();
        if (empty($limit)) {
            return $result;
        }

        $query = self::createQueryBionicFreeAvailableNumbersByLocalAreaCode(
            $localAreaCodeId,
            $limit
        );

        if (is_array($tipnIds) && !empty($tipnIds)) {
            $not = ($tipnIdsAreDesired) ? '' : 'NOT';
            $ids = implode(', ', $tipnIds);
            $query
                ->andWhere("tipn.id $not IN ($ids)")
            ;
        }

        Loggable::putQueryLogMessage($query);

        return $query->fetchArray();
    }

    public static function countFreeNumbersByLocalAreaCode($localAreaCodeId) {

        $query = self::createQueryBionicFreeNumbersByLocalAreaCode(
            $localAreaCodeId
        );
        
        return $query->count();
    }

    public static function countFreeNumbersByLocalAreaCodeForCampaign($advertiserId, $localAreaCodeId) {

        $query = self::createQueryForAll()
            ->select('tipn.id, tipn.phone_number')
            ->innerJoin('tipn.AdvertiserIncomingNumberPool ainp')
            ->andWhere('ainp.advertiser_id = ?', $advertiserId)
            ->andWhere('ainp.campaign_id is null')
        ;

        if (!empty($localAreaCodeId)) {
            $query->addWhere('tipn.twilio_local_area_code_id = ?', $localAreaCodeId);
        }

        return $query->count();
    }

//    /**
//     *
//     * @param integer/array $advertiserId ID of the advertiser
//     * @return type 
//     */
//    public static function createQueryByAdvertiserId($advertiserId) {
//
//        $query = self::createQueryForAll()
//            ->select('tipn.id, tipn.phone_number')
//            ->innerJoin('tipn.AdvertiserIncomingNumberPool ainp')
//        ;
//
//        if (is_array($advertiserId)) {
//            $query->andWhereIn('ainp.advertiser_id', $advertiserId);
//        } else {
//            $query->andWhere('ainp.advertiser_id = ?', $advertiserId);
//        }
//
//        return $query;
//    }

    public static function findOwnerCompanyByIncomingNumberId($incomingPhoneNumberId) {

        $bionicIncomingNumberPool = BionicIncomingNumberPoolTable::findByIncomingPhoneNumberId($incomingPhoneNumberId, true);
        $ownerCompany = empty($bionicIncomingNumberPool) ? null : CompanyTable::getBionic();

        return $ownerCompany;
    }

    /**
     * @param Company $company
     * @param String $type one of TwilioLocalAreaCodeTable::TYPE_*
     * @param String $from unixtimestamp
     * @param String $to unixtimestamp
     */
    public static function findMyCompanyIncomingCallTime($company, $type = null, $from = null, $to = null) {

        $result = 0;

        if ($company->isBionicCompany()) {
            $query = self::createIncomingCallsQueryBionic($type, $from, $to);
            $query->select('SUM(tic.duration) duration');

            $queryResult = $query->execute(array(), Doctrine_Core::HYDRATE_ARRAY);
            if (count($queryResult)) {
                $result = intval($queryResult[0]['duration']);
            }
        }

        return $result;
    }

    protected static function createIncomingCallsQueryBionic (
        $type = null,
        $from = null,
        $to = null
    ) {

        $query = TwilioIncomingCallTable::getInstance()->createQuery('tic')
            ->innerJoin('tic.TwilioIncomingPhoneNumber tipn')
            ->innerJoin('tipn.TwilioLocalAreaCode tlac')
            ->innerJoin('tipn.BionicIncomingNumberPool binp')
            ->andWhere('binp.is_owner = 1')
        ;

        if (!empty($type)) {
            $query->andWhere('tlac.phone_number_type = ?', $type);
        }
        if (!empty($from)) {
            $query->andWhere('tic.start_time >= ?', $from);
        }
        if (!empty($to)) {
            $query->andWhere('tic.start_time <= ?', $to);
        }

        return $query;
    }

    /**
     * Returns amount of phones of company, of it's subtree and company's advertisers
     *  => phones of advertisers of subtree
     * 
     * @param Company $company
     * @param integer $from UNIX timestamp
     * @param integer $to UNIX timestamp
     * @param String $type one of TwilioLocalAreaCodeTable::TYPE_*
     * @return integer 
     */
    public static function findCountByCompany(
        $company, 
        $from, 
        $to, 
        $type
    ) {

        //NOTE:: see self::findCountByAdvertiser.
        //TODO:: 1). create query using Doctrine 2). introduce method
        $typePurchase       = IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE;
        $typePurchaseAssign = IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE_AND_ASSIGN;
        $typeResume         = IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_RESUME_FROM_ADVERTISER;

        $sqlQueryString =
<<<EOD
    SELECT
        tipn.id
    FROM
        twilio_incoming_phone_number AS tipn
        INNER JOIN twilio_local_area_code AS tlac
            ON tipn.twilio_local_area_code_id = tlac.id
        INNER JOIN advertiser_incoming_number_pool_log AS ainpl
            ON tipn.id = ainpl.twilio_incoming_phone_number_id
        INNER JOIN incoming_numbers_queue AS inq 
            ON ainpl.incoming_numbers_queue_id = inq.id
-- -> diff
        INNER JOIN advertiser AS adv
            ON inq.advertiser_id = adv.id
        INNER JOIN company AS com
            ON adv.company_id = com.id
-- <- diff
    WHERE
        (
                ainpl.action_date BETWEEN $from AND $to
            OR
            (
                EXISTS
                (
                    SELECT
                        NULL
                    FROM
                        advertiser_incoming_number_pool_log startAinpl
                        INNER JOIN incoming_numbers_queue AS startInq 
                            ON startAinpl.incoming_numbers_queue_id = startInq.id
                -- -> diff
                        INNER JOIN advertiser AS startAdv
                            ON startInq.advertiser_id = startAdv.id
                        INNER JOIN company AS startCom
                            ON startAdv.company_id = startCom.id
                -- <- diff
                    WHERE
                        (
                                tipn.id = startAinpl.twilio_incoming_phone_number_id
                            AND startInq.order_type IN ('$typePurchase', '$typePurchaseAssign')
                            AND startAinpl.action_date <= $from
                -- -> diff
                            AND startCom.lft BETWEEN {$company->getLft()} AND {$company->getRgt()}
                -- <- diff
                        )
                        AND
                        (
                            NOT EXISTS
                            (
                                SELECT
                                    NULL
                                FROM
                                    advertiser_incoming_number_pool_log finishAinpl
                                    INNER JOIN incoming_numbers_queue AS finishInq 
                                        ON finishAinpl.incoming_numbers_queue_id = finishInq.id
                                WHERE
                                    (
                                            tipn.id = finishAinpl.twilio_incoming_phone_number_id
                                        AND finishInq.order_type IN ('$typeResume')
                                        AND finishAinpl.action_date BETWEEN startAinpl.action_date AND $from
                                    )
                                ORDER BY finishAinpl.action_date ASC
                                LIMIT 1
                            )
                        )
                    ORDER BY startAinpl.action_date DESC
                    LIMIT 1
                )
            )
        )
        AND tlac.phone_number_type = '$type'
        AND inq.order_type IN ('$typePurchase', '$typePurchaseAssign', '$typeResume')
-- -> diff
        AND com.lft BETWEEN {$company->getLft()} AND {$company->getRgt()}
-- <- diff
    GROUP BY tipn.id
EOD;

        $conn = Doctrine_Manager::getInstance()->getConnection('doctrine');
        $result = $conn->fetchAssoc($sqlQueryString, array());

        return count($result);
    }

    /**
     *
     * @param Advertiser $advertiser
     * @param integer $from UNIX timestamp
     * @param integer $to UNIX timestamp
     * @param String $type one of TwilioLocalAreaCodeTable::TYPE_*
     * @param boolean $unassignedOnly
     * @return integer count...
     */
    public static function findCountByAdvertiser($advertiser, $from, $to, $type) {

        //NOTE:: see self::findCountByCompany.
        //TODO:: 1). create query using Doctrine 2). introduce method

        $advertiserId = $advertiser->getId();

        $typePurchase       = IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE;
        $typePurchaseAssign = IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE_AND_ASSIGN;
        $typeResume         = IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_RESUME_FROM_ADVERTISER;

        $sqlQueryString =
<<<EOD
    SELECT
        tipn.id
    FROM
        twilio_incoming_phone_number AS tipn
        INNER JOIN twilio_local_area_code AS tlac
            ON tipn.twilio_local_area_code_id = tlac.id
        INNER JOIN advertiser_incoming_number_pool_log AS ainpl
            ON tipn.id = ainpl.twilio_incoming_phone_number_id
        INNER JOIN incoming_numbers_queue AS inq 
            ON ainpl.incoming_numbers_queue_id = inq.id
    WHERE
        (
                ainpl.action_date BETWEEN $from AND $to
            OR
            (
                EXISTS
                (
                    SELECT
                        NULL
                    FROM
                        advertiser_incoming_number_pool_log startAinpl
                        INNER JOIN incoming_numbers_queue AS startInq 
                            ON startAinpl.incoming_numbers_queue_id = startInq.id
                -- -> diff
                -- <- diff
                    WHERE
                        (
                                tipn.id = startAinpl.twilio_incoming_phone_number_id
                            AND startInq.order_type IN ('$typePurchase', '$typePurchaseAssign')
                            AND startAinpl.action_date <= $from
                -- -> diff
                            AND startInq.advertiser_id = $advertiserId
                -- <- diff
                        )
                        AND
                        (
                            NOT EXISTS
                            (
                                SELECT
                                    NULL
                                FROM
                                    advertiser_incoming_number_pool_log finishAinpl
                                    INNER JOIN incoming_numbers_queue AS finishInq 
                                        ON finishAinpl.incoming_numbers_queue_id = finishInq.id
                                WHERE
                                    (
                                            tipn.id = finishAinpl.twilio_incoming_phone_number_id
                                        AND finishInq.order_type IN ('$typeResume')
                                        AND finishAinpl.action_date BETWEEN startAinpl.action_date AND $from
                                    )
                                ORDER BY finishAinpl.action_date ASC
                                LIMIT 1
                            )
                        )
                    ORDER BY startAinpl.action_date DESC
                    LIMIT 1
                )
            )
        )
        AND tlac.phone_number_type = '$type'
        AND inq.order_type IN ('$typePurchase', '$typePurchaseAssign', '$typeResume')
-- -> diff
        AND inq.advertiser_id = $advertiserId
-- <- diff
    GROUP BY tipn.id
EOD;

        $conn = Doctrine_Manager::getInstance()->getConnection('doctrine');
        $result = $conn->fetchAssoc($sqlQueryString, array());

        return count($result);


//        $query = self::getInstance()->createQuery('tipn')
//            ->innerJoin('tipn.TwilioLocalAreaCode tlac')
//            ->innerJoin('tipn.AdvertiserIncomingNumberPoolLogs ainpl')
//            ->innerJoin('ainpl.IncomingNumbersQueue inq')
//            ->andWhere('tlac.phone_number_type = ?', $type)
//            ->andWhere("inq.advertiser_id = $advertiserId")
//            //TODO:: think about it:
//            ->addGroupBy('tipn.id')
//
//            ->andWhere(
//                "
//                (
//                    inq.order_type IN ($typesStr)
//                    AND ainpl.action_date BETWEEN $from AND $to
//                )
//                OR
//                (
//                )
//                "
//            )
//        ;
//
//        return $query->count();
    }
    
    /**
     * Buy new number from Twilio and create record in BionicIncomingNumberPool
     * 
     * @param TwilioLocalAreaCode $localAreaCode
     * @param String $url
     * @param String $friendlyName
     * @param type $httpMethod
     * @return BionicIncomingNumberPool 
     */
    public static function buyAndRegisterIncomingNumber(
        $localAreaCode,
        $url,
        $friendlyName,
        $httpMethod = 'POST'
    ) {

        $canBuyPhoneNumbers = sfConfig::get('app_voip_can_buy_phone_numbers', false);
        if (empty($canBuyPhoneNumbers)) {
            throw new Exception("Change configs to make possible to buy new phone numbers from VoIP server");
        }

        $twilioAccount = TwilioAccountTable::getInstance()->findDefaultAccount();
        $requestResult = TwilioPhoneNumberAPI::sendIncomingPhoneNumber(
            $twilioAccount->getSid(),
            $twilioAccount->getAuthToken(),
            null,
            $url,
            $friendlyName,
            $localAreaCode->getPhoneNumberType(),
            $localAreaCode->getCode(),
            $httpMethod
        );

        Loggable::putArrayLogMessage(
            ApplicationLogger::TWILIO_LOG_TYPE,
            sfLogger::DEBUG,
            $requestResult,
            "Result from twilio:"
        );

        if (!isset($requestResult['error'])) {

            $now = time();

            $newNumberData = array();
            $newNumberData['twilio_account_id']         = $twilioAccount->getId();
            $newNumberData['sid']                       = $requestResult['sid'];
            $newNumberData['phone_number']              = $requestResult['phone_number'];
            $newNumberData['friendly_name']             = $requestResult['friendly_name'];
            $newNumberData['url']                       = $url;
            $newNumberData['http_method_type']          = $httpMethod;
            $newNumberData['twilio_local_area_code_id'] = $localAreaCode->getId();
            $newNumberData['purchased_date']            = $now;

            $newIncomingPhoneNumber = new TwilioIncomingPhoneNumber();
            $newIncomingPhoneNumber->fromArray($newNumberData);
            $newIncomingPhoneNumber->save();

            $bionicIncomingNumberPool = new BionicIncomingNumberPool();
            $bionicIncomingNumberPool->addNewIncomingPhoneNumberToCleaningPool($newIncomingPhoneNumber);

            return $bionicIncomingNumberPool;
        }

        return null;
    }
   
    /**
     * For task
     * @return Doctrine_Query
     */
    public static function findAllWithoutBionic() {

        return self::getInstance()
            ->createQuery('tipn')
            ->leftJoin('tipn.BionicIncomingNumberPool binp')
            ->addWhere('tipn.was_deleted = 0')
            ->addWhere('binp.id IS NULL')
            ->addOrderBy('tipn.twilio_local_area_code_id')
            ->addOrderBy('tipn.id')
            ->execute()
        ;
    }

//    /**
//     *
//     * @param integer $advertiserId
//     * @return Doctrine_Query query to select availible numbers for company and user.
//     */
//    public static function getQueryNewCampaignAvailableNumbers(
//        $advertiserId
//    ) {
//
//        $query = self::getInstance()->createQuery('tipn')
//            ->andWhere(
//                "
//                    (
//                            ainp.id IS NULL
//                        AND binp.id IS NOT NULL
//                        AND binp.is_owner=true
//                    ) OR (
//                            ainp.advertiser_id=$advertiserId
//                        AND ainp.campaign_id IS NULL
//                    )
//                "
//            )
//            ->andWhere("tipn.was_deleted=false")
//            ->innerJoin('tipn.TwilioLocalAreaCode tlac')
//            ->leftJoin('tipn.AdvertiserIncomingNumberPool ainp')
//            ->leftJoin('tipn.BionicIncomingNumberPool binp')
//            ->addOrderBy('tlac.code')
//        ;
//
//        return $query;
//    }

    public static function findOwnerAtTime($tipnId, $atTime) {

        $result = array(
            'advertiser_id' => null,
            'campaign_id'   => null,
            //TODO:: add info:
//            'phone_number_was_defult'   => null,
        );
        
        $tipn = TwilioIncomingPhoneNumberTable::getInstance()->find($tipnId);
        if (empty($tipn) || !$tipn->exists()) {
            throw new Exception('TwilioIncomingPhoneNumber has not been found.');
        }

        $query = AdvertiserIncomingNumberPoolLogTable::getInstance()->createQuery('ainpl')
            ->innerJoin('ainpl.IncomingNumbersQueue inq')
            ->andWhere("ainpl.twilio_incoming_phone_number_id = $tipnId")
            ->andWhere("ainpl.action_date <= $atTime")
            ->addOrderBy('ainpl.action_date DESC')
            ->limit(1)
        ;


        $queryPurchaseOrAssign = $query
            ->copy()
            ->andWhere("(
                   inq.order_type = '" . IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE . "'
                OR inq.order_type = '" . IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE_AND_ASSIGN . "'
                OR inq.order_type = '" . IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_ASSIGN . "'
            )")
        ;
        $purchaseOrAssignLog = $queryPurchaseOrAssign->fetchOne();
        if (empty($purchaseOrAssignLog)) {
            return $result;
        }
        $purchaseOrAssignQueue = $purchaseOrAssignLog->getIncomingNumbersQueue();
        $purchaseOrAssignType = $purchaseOrAssignQueue->getOrderType();
        $purchaseOrAssignDate = $purchaseOrAssignLog->getActionDate();


        $queryResume = $query
            ->copy()
            ->andWhere("ainpl.action_date >= $purchaseOrAssignDate")
        ;

        $queryResumeFromAdv = $queryResume
            ->copy()
            ->andWhere("inq.order_type = '" . IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_RESUME_FROM_ADVERTISER . "'")
        ;

        $resumeFromAdvLog = $queryResumeFromAdv->fetchOne();
        if (!empty($resumeFromAdvLog)) {
            return $result;
        }
        $result['advertiser_id']= $purchaseOrAssignQueue->getAdvertiserId();


        if (
               $purchaseOrAssignType == IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_PURCHASE_AND_ASSIGN
            || $purchaseOrAssignType == IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_ASSIGN
        ) {
            $queryResumeFromCam = $queryResume
                ->copy()
                ->andWhere("inq.order_type = '" . IncomingNumbersQueueTable::ORDER_TYPE_PHONE_NUMBER_RESUME_FROM_CAMPAIGN . "'")
            ;

            $resumingFromCamLog = $queryResumeFromCam->fetchOne();
            if (empty($resumingFromCamLog)) {
                $result['campaign_id']  = $purchaseOrAssignQueue->getCampaignId();
            }
        }

        return $result;
    }

    /**
     *
     * @param int $advertiserId id (adevrtiser with id) or 0 (some advertiser) or null (no one). If null then it does not matter what campaign is set.
     * @param int $campaignId id (campaign with id) or 0 (some campaign or noone campaign) or null (no one).
     * @param int $atTime timestamp
     * @return boolean 
     */
    public static function findIfBelongToAtTime($tipnId, $advertiserId, $campaignId, $atTime) {

        if (is_null($advertiserId)) {
            if (!is_null($campaignId)) {
                throw new Exception('Wrong parameter.');
            }
        }

        $result = self::findOwnerAtTime($tipnId, $atTime);
        $resultAdvId = $result['advertiser_id'];
        $resultCamId = $result['campaign_id'];



        if (is_null($advertiserId)) {
            if (is_null($resultAdvId)) {
                return is_null($resultCamId);
            }
            return false;

        } elseif (empty($advertiserId)) {
            if (empty($resultAdvId)) {
                return false;
            } else {
                if (is_null($campaignId)) {
                    return is_null($resultCamId);
                } elseif (empty($campaignId)) {
                    return true;
                }
                return ($campaignId == $resultCamId);
            }

        }


        if (empty($resultAdvId) || $advertiserId != $resultAdvId) {
            return false;
        } else {
            if (is_null($campaignId)) {
                return is_null($resultCamId);
            } elseif (empty($campaignId)) {
                return true;
            } 
            return ($campaignId == $resultCamId);
        }
    }
}
