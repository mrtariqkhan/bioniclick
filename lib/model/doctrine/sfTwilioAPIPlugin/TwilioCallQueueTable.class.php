<?php


class TwilioCallQueueTable extends PluginTwilioCallQueueTable
{
    
    public static function getInstance()
    {
        return Doctrine_Core::getTable('TwilioCallQueue');
    }
}