<?php

/**
 * AdministrativeUnitTable
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 */
class AdministrativeUnitTable extends Doctrine_Table
{
    /**
     * Returns an instance of this class.
     *
     * @return object AdministrativeUnitTable
     */
    public static function getInstance()
    {
        return Doctrine_Core::getTable('AdministrativeUnit');
    }
}