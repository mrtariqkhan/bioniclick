<?php

class NumberFormatTable extends Doctrine_Table {
    
    public static function getInstance() {
        return Doctrine_Core::getTable('NumberFormat');
    }

    public static function findAsArray() {
        $query = self::getInstance()->createQuery()
            ->select('id')
            ->addSelect('format')
        ;

        $list = $query->execute(array(), Doctrine_Core::HYDRATE_ARRAY);

        $result = array();
        foreach ($list as $object) {
            $result[$object['id']] = $object['format'];
        }
        return $result;
    }
}