<?php

/**
 * DomainValidator validates PhoneNumbers.
 */
class DomainValidator extends sfValidatorRegex {

//    const REGEX_DOMAIN = '/^(?!(https|http|ftp)).*/';
    const REGEX_DOMAIN = '/^((?!:\/\/).)*$/';

    const DEFAULT_OPTION_MIN_LENGTH = 1;
    const DEFAULT_OPTION_MAX_LENGTH = 200;

    /**
    * @see sfValidatorRegex
    */
    protected function configure($options = array(), $messages = array()) {

        parent::configure($options, $messages);

        $this->setOptionIfNotSet('min_length', self::DEFAULT_OPTION_MIN_LENGTH);
        $this->setOptionIfNotSet('max_length', self::DEFAULT_OPTION_MAX_LENGTH);

        $this->setOption('pattern', self::REGEX_DOMAIN);
    }

    protected function setOptionIfNotSet($name, $default) {

        if (!$this->hasOption($name)) {
            $this->setOption($name, $default);
        }

        return $this;
    }
}
