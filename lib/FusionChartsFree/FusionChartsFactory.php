<?php

/**
 * Description of FusionChartsHelper
 *
 * @author fairdev
 * @link http://www.fusioncharts.com/free/docs/
 */
class FusionChartsFactory {

    const chartsSwfDir = '/swf/fusion_charts_free/';

    protected static $instance = null;

    static public function getInstance() {

        if (empty(self::$instance)) {
            self::$instance = new FusionChartsFactory();
        }
        return self::$instance;
    }

    /**
     *  @param $dateSet
     *  @param String $chartType one of Column2D, Column3D, Line, Pie3D, Pie2D, Bar2D,
     *                    Area2D, Doughnut2D, MSColumn3D, MSColumn2D, MSArea2D,
     *                    MSLine, MSBar2D, StackedColumn2D, StackedColumn3D,
     *                    StackedBar2D, StackedArea2D, MSColumn3DLineDY,
     *                    MSColumn2DLineDY
     */
    public function createFC(
        $dataSet,
        $chartType,
        $width,
        $height,
        array $params = array(),
        $combinated = false,
        $chartId = '11'
    ) {

        $isSingle = $this->isSingleChart($dataSet);

        $defaultParams = array(
            'showValues'        => 0,
            'showNames'         => 0,
            'showLimits'        => 1,
//            'numdivlines'       => 0,

            'rotateNames'       => 0,

            'decimalPrecision'  => 0,

            'pieBorderAlpha'    => 40,
            'pieSliceDepth'     => 25,
            'pieRadius'         => 100,

            'formatNumberScale' => 0,
            'baseFontSize'      => 10,
            'baseFont'          => 'Arial',

            'yaxismaxvalue'     => 1,

            'chartLeftMargin'   => 15,
            'chartRightMargin'  => 15,
            'chartTopMargin'    => 25,
            'chartBottomMargin' => 15,
        );

        $params = array_merge($defaultParams, $params);
        $params = $this->generateParamString($params);

        $fc = new FusionCharts($chartType, $width, $height, $chartId, true);
        $fc->setSWFPath(self::chartsSwfDir);
        $fc->setChartParams($params);

        if ($isSingle == 0) {
            $fc = $this->createMultipleChartData($dataSet, $fc, $combinated);
        } else {
            $fc = $this->createSingleChartData($dataSet, $fc);
        }

        return $fc;
    }

    private function generateParamString($params) {

        $result = "";
        foreach ($params as $name => $value) {
            $result .= "$name=$value;";
        }

        return $result;
    }

    private function isSingleChart($dataSet) {

        foreach ($dataSet as $name => $value) {
            return !is_array($value);
        }
    }

    private function createSingleChartData($dataSet, $fc) {

        foreach($dataSet as $name => $value) {
            $fc->addChartData($value, "name=$name");
        }

        return $fc;
    }

    private function createMultipleChartData($dataSet, $fc, $combinated = false) {

        $value = array();
        foreach ($dataSet as $name => $value) {
            $fc->addCategory($name);
        }

        $count = count($value);
        $i = 1;
        foreach ($value as $setName => $setValue) {
            if ($combinated) {
                $property = "parentYAxis=" . (($count == $i++) ? 'S' : 'P');
            } else {
                $property = '';
            }
            $fc->addDataset($setName, $property);

            foreach ($dataSet as $item) {
                $fc->addChartData($item[$setName]);
            }
        }

        return $fc;
    }
}

