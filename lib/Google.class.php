<?php
//TODO:: check this class
//Google.class.php by Arun on 30-Jun-2009


class Google {

    public static $STATUS_SUCCESS = 'SUCCESS';
    public static $STATUS_FAILURE = 'FAILURE';

    protected static $DEFAULT_URL_LOGIN              = 'https://www.google.com/accounts/ServiceLoginBox
?service=adwords
&hl=en_US
&ltmpl=login
&ifr=true
&passive=true
&skipvpage=true
&rm=hide
&fpui=1
&nui=15
&alwf=true
&continue=https://adwords.google.com/select/gaiaauth%3Fapt=None
&followup=https://adwords.google.com/select/gaiaauth%3Fapt=None'
    ;
    protected static $DEFAULT_URL_GOOGLE_AUTH        = 'https://www.google.com/accounts/ClientLogin';
    protected static $DEFAULT_URL_LOGIN_BOX          = 'https://www.google.com/accounts/ServiceLoginBoxAuth';
    protected static $DEFAULT_URL_LOGOUT             = 'https://adwords.google.com/select/gaialogout';
    protected static $DEFAULT_URL_LOGIN_CONTINUE     = 'https://adwords.google.com/select/gaiaauth?apt';
    protected static $DEFAULT_URL_SNAPSHOT           = 'https://adwords.google.com/select/snapshot';
    protected static $DEFAULT_URL_CONVERSION_HOME    = 'https://adwords.google.com/select/ConversionTrackingHome';
    protected static $DEFAULT_URL_CREATE_ACTION      = 'https://adwords.google.com/select/CreateAction';
    protected static $DEFAULT_URL_CREATE_ACTION_INPUT= 'https://adwords.google.com/select/CreateActionInput';

    protected $trackingAction;
    protected $userName;
    protected $password;
    protected $actionName;
    protected $curl;        // will contain the object of classs cURL
    protected $resultFile;  // will contain the response returned by the curl request
    protected $referer;
    protected $success;     // contains data if successfully loaded
    protected $errorMessage;// contains the error messages
    protected $errorCounter;// contains counter of errors
    protected $status;      // contains status: Google::$STATUS_*


    /**
     *
     * @param <type> $options
     * @throws Exception if curl does not set.
     */
    public function __construct($options) {

        $this->setTrackingAction(   $options['trackingAction']);
        $this->setUserName(         $options['userName']);
        $this->setPassword(         $options['password']);
        $this->setActionName(       $options['actionName']);
        $this->setCurl(             $options['curl']);
        $this->setReferer(          $options['referer']);

        $this->clearErrors();
    }

    public function updateData() {

        $this->clearErrors();

        //NOTE::do logout initially if user is logged in to same browser.
        $this->doLogout();

        $this->doLogin();
    }

    /**
     * LogOut from adwords.
     * @throws Exception if curl has invalid request.
     */
    protected function doLogout() {
        $this->doCurlGet(self::$DEFAULT_URL_LOGOUT, $this->referer);
    }

    /**
     * LogIn and proceed further
     * @throws Exception if curl has invalid request.
     */
    protected function doLogin() {

        $this->setGoogleAuthParams();

        $this->resultFile = $this->doCurlGet(self::$DEFAULT_URL_LOGIN, $this->referer);
        $subResult = $this->resultFile;

        $pattern = '/<form\s*action="ServiceLoginBoxAuth"\s*/is';
        if (preg_match($pattern, $subResult, $matcher)) {
            $subResult = $this->after($matcher[0], $subResult);

            $pattern = '/<\/form>/is';
            if (preg_match($pattern, $subResult, $matcher)) {
                $subResult = $this->before($matcher[0], $subResult);

                $formData = $this->parseFormData($subResult);

                $formData['service']    = 'adwords';
                $formData['hl']         = 'en_US';
                $formData['ltmpl']      = 'login';
                $formData['ifr']        = 'true';
                $formData['skipvpage']  = 'true';
                $formData['rm']         = 'hide';
                $formData['fpui']       = '1';
                $formData['nui']        = '15';
                $formData['alwf']       = 'true';
                $formData['continue']   = self::$DEFAULT_URL_LOGIN_CONTINUE;
                $formData['Email']      = $this->userName;
                $formData['Passwd']     = $this->password;

                $this->resultFile = $this->doCurlPost(self::$DEFAULT_URL_LOGIN_BOX, $formData);
                $subResult = $this->resultFile;

                $pattern = '/The\s*username\s*or\s*password\s*you\s*entered\s*is\s*incorrect/is';
                if (preg_match($pattern, $subResult, $matcher)) {
                    $this->addError('The username or password you entered is incorrect.');
                    return;
                } else {
                    $pattern = '/<a\s*href="(.*?)"/is';
                    if (preg_match($pattern, $subResult, $matcher)) {
                        $url = $matcher[1];
                        $url = preg_replace('/amp;/is', '', $url);

                        //TODO:: check if is need to use self::$DEFAULT_URL_SNAPSHOT.
                        $url = self::$DEFAULT_URL_SNAPSHOT;

                        $this->resultFile = $this->doCurlGet($url);
                        $this->clickConversiontracking();
                    } else {
                        $this->addError('Could not redirect after login, change in website.');
                    }
                }
            } else {
                $this->addError('Closing form not found on login page.');
            }
        } else {
            $this->addError('Form name has been changed on login page.');
        }
    }

    /**
     * @throws Exception if curl has invalid request.
     */
    protected function clickConversiontracking() {

        $this->resultFile = $this->doCurlGet(self::$DEFAULT_URL_CONVERSION_HOME);
        $this->clickCreateAction();
    }

    /**
     * @throws Exception if curl has invalid request.
     */
    protected function clickCreateAction() {

        $this->resultFile = $this->doCurlGet(self::$DEFAULT_URL_CREATE_ACTION);
        $this->getCode($this->resultFile);
    }

    /**
     * @throws Exception if curl has invalid request.
     */
    protected function getCode($resultFile) {

        $subResult = $resultFile;

        if (preg_match('/name\W+createEditAction\W/is', $subResult, $matcher)) {
            $subResult = $this->after($matcher[0], $subResult);

            $pattern = '/<\/form>/is';
            if (preg_match($pattern, $subResult, $matcher)) {
                $subResult = $this->before($matcher[0], $subResult);

                $formData = $this->parseFormData($subResult);

                $formData['conversionTypeId']   = '0';
                $formData['isTrivialChange']    = 'true';
                $formData['userActivity']       = 'CREATE';
                $formData['name']               = $this->actionName;
                $formData['revenueValue']       = '';
                $formData['hidden_revenueValue']= '';
                $formData['markupLanguage']     = 'HTML';
                $formData['backgroundColor']    = 'ffffff';
                $formData['conversionCategory'] = $this->trackingAction;
                $formData['conversionPageLang'] = 'en_US';
                $formData['httpProtocol']       = 'HTTP';
                $formData['markupLanguage']     = 'HTML';
                $formData['textFormat']         = 'HIDDEN';
                $formData['continueButton']     = 'Save Action & Get Code';

                $this->resultFile = $this->doCurlPost(self::$DEFAULT_URL_CREATE_ACTION_INPUT, $formData);
                $subResult = $this->resultFile;

                $subResult = preg_replace('/&lt;/is', '<', $subResult);
                $subResult = preg_replace('/&gt;/is', '>', $subResult);
                $subResult = preg_replace('/&quot;/is', '"', $subResult);
                $subResult = preg_replace('/&amp;/is', '&', $subResult);

                $pattern = '/Please\s*enter\s*a\s*valid\s*name/is';
                if (!(preg_match($pattern, $subResult, $matcher))) {

                    $pattern = '/This\s*name\s*is\s*already\s*in\s*use\s*by\s*another\s*action/is';
                    if (!(preg_match($pattern, $subResult, $matcher))) {

                        $pattern = '/<textarea\s*id="snippettext".*?>(.*?)<\/textarea>/is';
                        if (preg_match($pattern, $subResult, $matcher)) {
                            //$this->success['snippetCode'] = $matcher[1];
                            $this->success['snippetCode'] = htmlspecialchars($matcher[1]);
                            $this->status = self::$STATUS_SUCCESS;
                        } else {
                            $this->addError('Some change in website while scraping snippet code.');
                        }
                    } else {
                        $this->addError('This name is already in use by another action.');
                    }
                } else {
                    $this->addError('Please enter a valid trackingAction name.');
                }
            } else {
                $this->addError('Closing form not found for form name createEditAction. Change in website.');
            }
        } else {
            $this->addError('Form --- createEditAction not found. Change in website.');
        }
    }

    protected function setGoogleAuthParams() {
        $params = array();
        $params['Email']        = $this->userName;
        $params['Passwd']       = $this->password;
        $params['accountType']  = 'HOSTED_OR_GOOGLE';
        $params['service']      = 'adwords';
        $params['source']       = '';

        $this->resultFile = $this->doCurlPost(self::$DEFAULT_URL_GOOGLE_AUTH, $params);
        $subResult = $this->resultFile;

        if (preg_match('/Error=(.*)\s*/i', $subResult, $matcher)) {
            $errorMessage = $matcher[1];
            throw new Exception($errorMessage);
        }

        $authParams = array();
        if (preg_match('/SID=(.*)\s*/i', $subResult, $matcher)) {
            $authParams['SID'] = $matcher[1];
        }
        if (preg_match('/LSID=(.*)\s*/i', $subResult, $matcher)) {
            $authParams['LSID'] = $matcher[1];
        }
        if (preg_match('/Auth=(.*)\s*/i', $subResult, $matcher)) {
            $authParams['Auth'] = $matcher[1];
        }

        $auth = $authParams['Auth'];
        $additionalHeader = "Authorization: GoogleLogin auth=$auth";
        $this->curl->addHeader($additionalHeader);
    }



    /**
     * executes get cUrl, validates response
     * @throws Exception if curl has invalid request.
     */
    protected function doCurlGet($url, $referer = '') {

        $resultFile = $this->curl->get($url, $referer);
        return $this->validateCurlResponse($resultFile);
    }

    /**
     * executes post cUrl, validates response
     * @throws Exception if curl has invalid request.
     */
    protected function doCurlPost($url, $postParamsArray, $additionalHeader = '') {

        $postParams = $this->makePostParams($postParamsArray);
        $resultFile = $this->curl->post($url, $postParams, '', $additionalHeader);
        return $this->validateCurlResponse($resultFile);
    }

    /**
     * validates response of cUrl
     * @throws Exception if curl has invalid request.
     */
    protected function validateCurlResponse($cUrlResponse) {

        $this->doDelay();

        $pattern = '/\s*Invalid\s*request\s*/is';
        $errorMsg = $this->curl->getLastCurlErrorMsg();
        if (!empty($errorMsg)) {
            $this->addError($errorMsg);
            throw new Exception($this->getErrorMessageOne());

        } elseif (preg_match($pattern, $cUrlResponse, $matcher)) {
            $this->addError('Invalid request.');
            throw new Exception($this->getErrorMessageOne());
        }

        return $cUrlResponse;
    }




    /**
     * Make string postParameters to be posted with curl
     * @param array $postParamsArray associative aaray('param's name' => 'param's value')
     * @return string post params as string 'key=value&...'
     */
    protected function makePostParams($postParamsArray) {

        $postParams = '';
        foreach ($postParamsArray as $key => $value) {
            $key = preg_replace('/ /is', '%20', $key);
            $key = preg_replace('/:/is', '%3A', $key);
            $key = preg_replace('/&/is', '%26', $key);
            $key = preg_replace('/=/is', '%3D', $key);
            $key = preg_replace('/\//is', '%2F', $key);

            $value = preg_replace('/\//is', '%2F', $value);
            $value = preg_replace('/:/is', '%3A', $value);
            $value = preg_replace('/&/is', '%26', $value);
            $value = preg_replace('/=/is', '%3D', $value);
            $value = preg_replace('/\+/is', '%2B', $value);
            $value = preg_replace('/ /is', '%20', $value);

            $postParams .= $key . '=' . $value . '&';
        }

        $postParams = substr($postParams, 0, (strlen($postParams) - 1));
        return $postParams;
    }

    /**
     * Parse form fields into associative array
     * @param string $htmlForm
     * @return array associative aaray('input field name' => 'input field value')
     */
    protected function parseFormData($htmlForm) {

        $formData = array();
        $pattern = '/<input(.*?)>/is';
        while (preg_match($pattern, $htmlForm, $matcher)) {
            $htmlForm = $this->after($matcher[0], $htmlForm);
            $data = $matcher[1];

            $pattern1 = '/type\W+(hidden|text|password)\W+/is';
            if (preg_match($pattern1, $data, $matcher_1)) {

                $pattern2 = '/\s+name="(.*?)"/is';
                if (preg_match($pattern2, $data, $matcher_1)) {
                    $name = $matcher_1[1];
                    if ($name != '') {
                        $value = "";
                        $pattern3 = '/\s+value="(.*?)"/';
                        if (preg_match($pattern3, $data, $matcher_1)) {
                            $value = $matcher_1[1];
                        }
                        $formData[$name] = $value;
                    }
                }
            }
        }
        return $formData;
    }




    protected function clearErrors() {

        $this->success      = array();
        $this->errorCounter = 0;
        $this->errorMessage = array();
        $this->status       = '';
    }

    protected function addError($message) {
        $this->errorMessage[$this->errorCounter++] = $message;
        $this->status = self::$STATUS_FAILURE;
    }


    protected function doDelay() {
        sleep(rand(1, 3));
    }



    public function before ($inthis, $inthat) {
        return substr($inthat, 0, strpos($inthat, $inthis));
    }
    public function between ($inthis, $that, $inthat) {
        return $this->before($that, $this->after($inthis, $inthat));
    }
    public function after ($inthis, $inthat) {

        if (!is_bool(strpos($inthat, $inthis))) {
            return substr($inthat, strpos($inthat, $inthis) + strlen($inthis));
        }
    }




    /**
     *
     * @return string error messages in one string
     */
    public function getErrorMessageOne() {
        $errors = $this->getErrorMessage();

        $errorMsg = $errors;
        if (is_array($errors)) {
            $errorMsg = '';
            foreach ($errors as $key => $value) {
                $errorMsg .= $value . "/";
            }
        }

        return $errorMsg;
    }


    public function getTrackingAction() {
        return $this->trackingAction;
    }
    public function setTrackingAction($value) {
        $this->trackingAction = htmlspecialchars(trim($value));
    }
    public function getUserName() {
        return $this->userName;
    }
    public function setUserName($value) {
        $this->userName = htmlspecialchars(trim($value));
    }
    public function getPassword() {
        return $this->password;
    }
    public function setPassword($value) {
        $this->password = htmlspecialchars(trim($value));
    }
    public function getActionName() {
        return $this->actionName;
    }
    public function setActionName($value) {
        $this->actionName = htmlspecialchars(trim($value));
    }
    public function setCurl($curl) {
        if (is_object($curl)) {
            $this->curl = $curl;
        } else {
            throw new Exception('Curl is not an object.');
        }
    }
    public function getReferer() {
        return $this->referer;
    }
    public function setReferer($value) {
        $this->referer = htmlspecialchars(trim($value));
    }
    public function getStatus() {
        return $this->status;
    }
    public function getSnippetCode() {
        return $this->success;
    }
    public function getErrorMessage() {
        return $this->errorMessage;
    }
    public function getSuccess() {
        return $this->success;
    }
}
