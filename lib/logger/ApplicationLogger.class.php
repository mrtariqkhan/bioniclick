<?php

class ApplicationLogger extends sfFileLogger {

    const OPTION_FILE_NAME          = 'file_name';
    const OPTION_LEVEL_BY_DEFAULT   = 'level_by_default';
    const OPTION_LOG_TYPE           = 'type';
    const OPTION_NAME               = 'name';

    const MAIN_LOG_TYPE     = 0;
    const TWILIO_LOG_TYPE   = 1;
    const PAYMENTS_LOG_TYPE = 2;
    const EMAIL_LOG_TYPE    = 3;

    private $_lasyLoggers = array();


    /**
     *
     * @param string $logDir
     * @param array $options is array(array(self::OPTION_LOG_TYPE => int, self::OPTION_FILE_NAME => string, self::OPTION_LEVEL_BY_DEFAULT => int))
     */
    public function __construct($logDir, $options = array()) {

        $lasyLoggers = array();

        $loggerConfigs = $options;
        foreach ($loggerConfigs as $loggerConfig) {
            $logType = (int)$loggerConfig[self::OPTION_LOG_TYPE];

            $lasyLoggers[$logType] = new LasyFileLogger(
                array(
                    LasyFileLogger::OPTION_NAME         => $loggerConfig[self::OPTION_NAME],
                    LasyFileLogger::OPTION_LEVEL        => $loggerConfig[self::OPTION_LEVEL_BY_DEFAULT],
                    LasyFileLogger::OPTION_FILE_NAME    => $logDir . DIRECTORY_SEPARATOR . $loggerConfig[self::OPTION_FILE_NAME],
                )
            );
        }

        $this->_lasyLoggers = $lasyLoggers;
    }

    public function putLogMessage($logType, $message, $level = sfLogger::INFO) {

        if (sfConfig::get('sf_logging_enabled')) {

            //$this->getContextLogger()->info("Logger parameters: '$logType', '$message', '$level'");

            $logger = $this->getLoggerByType($logType);
            if (!empty($logger)) {
                //$this->getContextLogger()->info("$lazyLogger logger parameters: '$logType', '$message', '$level'");

                $logger->setLogLevel($level);
                $logger->log($message, $level);
            }
        }
    }

    public function __destruct() {

        $logTypesToShutdown = array(
            self::MAIN_LOG_TYPE,
        );

        foreach ($logTypesToShutdown as $logType) {
            $logger = $this->getLoggerByType($logType);
            if (!empty($logger)) {
                $logger->shutdown();
            }
        }
    }

    public function isLoggerInDebugMode($logType) {

        $lasyLogger = $this->getLazyLoggerByType($logType);
        if (!empty($lasyLogger)) {
            return $lasyLogger->isDebugLevel();
        }

        return false;
    }

    /**
     *
     * @param type $logType
     * @return FileLogger or null
     */
    protected function getLoggerByType($logType) {

        $lazyLogger = $this->getLazyLoggerByType($logType);
        if (!empty($lazyLogger)) {
            return $lazyLogger->getLogger();
        }
        return null;
    }

    /**
     *
     * @param type $logType
     * @return LasyFileLogger or null
     */
    protected function getLazyLoggerByType($logType) {

        $logType = (int)$logType;
        if (array_key_exists($logType, $this->_lasyLoggers)) {
            return $this->_lasyLoggers[$logType];
        }

        $this->getContextLogger()->err("Wrong logger type: $logType");
        return null;
    }

    /**
     *
     * @return sfLogger 
     */
    protected function getContextLogger() {
        return sfContext::getInstance()->getLogger();
    }

//    public function getLoggerLevel($logType) {
//
//        $lasyLogger = $this->getLazyLoggerByType($logType);
//        if (!empty($lasyLogger)) {
//            return $lasyLogger->getLevel();
//        }
//
//        return sfLogger::INFO;
//    }

//    protected function setLasyLoggerByType($logType, $lasyLogger) {
//        $this->_lasyLoggers[$logType] = $lasyLogger;
//    }
}