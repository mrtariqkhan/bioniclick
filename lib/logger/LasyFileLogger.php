<?php

class LasyFileLogger {

    const OPTION_FILE_NAME  = 'file';
    const OPTION_NAME       = 'name';
    const OPTION_LEVEL      = 'level';

    /**
     * @var array options
     */
    protected $options;

    /**
     * @var FileLogger $logger
     */
    protected $logger;

    /**
     * @var string name of logger
     */
    protected $name;

    /**
     * @var string Name of file
     */
    protected $fileName;

    /**
     * @var integer log level
     */
    protected $level;


    public function __construct(
        array $options
    ) {

        $this->setOptions($options);

        $this->setFileName( $this->getOption(self::OPTION_FILE_NAME));
        $this->setName(     $this->getOption(self::OPTION_NAME));
        $this->setLevel(    $this->getOption(self::OPTION_LEVEL));

        $this->setLogger(null);
    }

    public function __toString() {
        return $this->getName();
    }

    public function isDebugLevel() {

        $level = $this->getLevel();
        return (!empty($level) && ($level == sfLogger::DEBUG));
    }

    public function getLogger() {
        return $this->doInitialiseLogger();
    }
    public function getOptions() {
        return $this->options;
    }
    public function getName() {
        return $this->name;
    }
    public function getFileName() {
        return $this->fileName;
    }
    public function getLevel() {
        return $this->level;
    }

    protected function setFileName($fileName) {
        $this->fileName = $fileName;
    }
    protected function setName($name) {
        $this->name = $name;
    }
    protected function setLevel($level) {
        $this->level = $level;
    }
    protected function setOptions($options) {
        $this->options = $options;
    }
    protected function setLogger($logger) {
        $this->logger = $logger;
    }

    protected function getLoggerAsIs() {
        return $this->logger;
    }

    /**
     * Initialises logger if it does not exist. Sets logger also.
     *
     * @return FileLogger 
     */
    protected function doInitialiseLogger() {
        
        $logger = $this->getLoggerAsIs();
        $logger = 
            $logger
            ? $logger
            : new FileLogger(
                sfContext::getInstance()->getEventDispatcher(),
                array(
                    'file' => $this->getFileName(),
                )
            )
        ;
        $logger->setLogLevel($this->getLevel());

        $this->setLogger($logger);

        return $logger;
    }

    protected function getOption($option, $default = null) {

        $options = $this->getOptions();

        if (array_key_exists($option, $options)) {
            return $options[$option];
        }

        return $default;
    }
}