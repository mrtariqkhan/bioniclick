<?php
//TODO: add __sleep and __wakeup actions to open/close file cursor
class FileLogger extends sfFileLogger {

    /**
     * Logs a message.
     *
     * @param string $message   Message
     * @param string $priority  Message priority
     */
    protected function doLog($message, $priority) {
        ob_start();
        @flock($this->fp, LOCK_EX);
        @fwrite(
            $this->fp,
            strtr(
                $this->format,
                array(
                    '%type%' => $this->type,
                    '%message%' => $message,
                    '%time%' => strftime($this->timeFormat),
                    '%priority%' => $this->getPriority($priority),
                    '%EOL%' => PHP_EOL,
                )
            )
        );
        @flock($this->fp, LOCK_UN);
        ob_end_clean();
    }

    public function __destruct() {
        $this->shutdown();
    }
}