<?php

class Loggable {

    const LOG_CONFIG_FILE_NAME          = 'file_name';
    const LOG_CONFIG_LEVEL_BY_DEFAULT   = 'level_by_default';
    const LOG_CONFIG_LOG_TYPE           = 'log_type';
    const LOG_CONFIG_NAME               = 'name';

    /**
     * @var ApplicationLogger Application loger
     */
    private static $__logger = null;


    private static function createLogger() {

        if (empty(self::$__logger)) {

            $logDir         = sfConfig::get('sf_log_dir');
            $logsConfigs    = sfConfig::get('app_logging', array());

            $options = array();
            foreach ($logsConfigs as $logsConfig) {
                $options[] = array(
                    ApplicationLogger::OPTION_FILE_NAME         => $logsConfig[self::LOG_CONFIG_FILE_NAME],
                    ApplicationLogger::OPTION_LEVEL_BY_DEFAULT  => $logsConfig[self::LOG_CONFIG_LEVEL_BY_DEFAULT],
                    ApplicationLogger::OPTION_LOG_TYPE          => $logsConfig[self::LOG_CONFIG_LOG_TYPE],
                    ApplicationLogger::OPTION_NAME              => $logsConfig[self::LOG_CONFIG_NAME],
                );
            }

            self::$__logger = new ApplicationLogger($logDir, $options);
        }
    }

    /**
     * Log message in a separated log file
     * 
     * @param int $logType ApplicationLogger::*_TYPE constants
     * @param string $message
     * @param int $level
     */
    public static function putLogMessage($logType, $message, $level) {

        self::createLogger();
        
        self::$__logger->putLogMessage($logType, $message, $level);
    }

    public static function putQueryLogMessage($query, $logType = ApplicationLogger::MAIN_LOG_TYPE, $level = sfLogger::DEBUG) {
        self::putLogMessage(
            $logType,
            $query->getSqlQuery(),
            $level
        );
    }

    public static function putExceptionLogMessage(Exception $e, $logType = ApplicationLogger::MAIN_LOG_TYPE, $errorPrefix = '') {
        self::putLogMessage(
            $logType,
            $errorPrefix . '[' . $e->getCode() . ']: ' . $e->getMessage(),
            sfLogger::ERR
        );
    }

    public static function putExceptionFullLogMessage(Exception $e, $logType = ApplicationLogger::MAIN_LOG_TYPE, $errorPrefix = '') {
        self::putLogMessage(
            $logType,
            $errorPrefix . $e->getMessage() . ', trace: ' . $e->getTraceAsString(),
            sfLogger::ERR
        );
    }
    
    public static function getVarDump($var, $logType = ApplicationLogger::MAIN_LOG_TYPE) {

        $varDumpResult = "";
        if (sfConfig::get('sf_logging_enabled')) {

            self::createLogger();

            if (self::$__logger->isLoggerInDebugMode($logType)) {
                ob_start();
                var_dump($var);
                $varDumpResult = ob_get_contents();
                ob_end_clean();
            }
        }
        
        return $varDumpResult;
    }

    public static function putArrayLogMessage($logType, $level, $array, $message = null, $beforeElem = '') {

        self::createLogger();

        if (!empty($message)) {
            self::$__logger->putLogMessage($logType, $message, $level);
        }

        foreach ($array as $key => $value) {
            self::$__logger->putLogMessage($logType, "$beforeElem" . "$key: $value", $level);
            if (is_array($value)) {
                self::putArrayLogMessage($logType, $level, $value, null, "$beforeElem" . "  ");
            }
        }
    }
}
