<?php

class IncomingCallHandler {

    /**
     *
     * @param TwilioIncomingCall $incomingCall
     * @return string 
     */
    public static function getTwiMlBody($incomingCall) {

        $twiMlBody = '';

        if (!empty($incomingCall)) {
            $twilioIncomingPhoneNumberId = $incomingCall->getTwilioIncomingPhoneNumberId();
            if ($incomingCall->getTwilioCallerPhoneNumber()->getIsBlocked()) {
                $twiMlBody = self::getBlockedCallCommand();
            } else {
                $twiMlBody = self::getCallRedirectCommand($twilioIncomingPhoneNumberId);
            }
        }

        if (empty($twiMlBody)) {
            $twiMlBody = '<Say>We are sorry, called number is unavailable.</Say>';
        }

        return $twiMlBody;
    }

    /**
     *
     * @param integer $incomingNumberId TwilioIncomingPhoneNumber's id
     * @return string 
     */
    protected static function getCallRedirectCommand($incomingNumberId) {

        $twiMlBody = '';

        $campaign = CampaignTable::getByTwilioIncomingPhoneNumberId($incomingNumberId);
        if (!empty($campaign)) {
            $advertiser = $campaign->getAdvertiser();
            if ($advertiser->getIsDebtor()) {
                $twiMlBody = self::getDebtorCallCommand();
            } else {
                $twiMlBody = self::generateTwiMlByCampaign($campaign, $incomingNumberId);
            }
        }

        if (empty($twiMlBody)) {
            $advertiser = AdvertiserIncomingNumberPoolTable::findAdvertiserByIncomingPhoneNumberId($incomingNumberId);
            if (!empty($advertiser)) {
                if ($advertiser->getIsDebtor()) {
                    $twiMlBody = self::getDebtorCallCommand();
                } else {
                    $twiMlBody = self::generateTwiMlByAdvertiser($advertiser);
                }
            }
        }

        if (empty($twiMlBody)) {
            //NOTE:: Bionic company can have its own free numbers.
            $company = BionicIncomingNumberPoolTable::findCompanyByIncomingPhoneNumberId($incomingNumberId);
            if (!empty($company)) {
                $twiMlBody = self::generateTwiMlForBionic($company);
            }
        }

        return $twiMlBody;
    }

    /**
     * 
     * @param Campaign $campaign
     * @param integer $incomingNumberId TwilioIncomingPhoneNumber's id
     * @return string 
     */
    public static function generateTwiMlByCampaign($campaign, $incomingNumberId) {
		//mail("ahsan@nimblewebsolutions.com","Incomming call handler","body=".$campaign->getId());
        //NOTE:: is public for testing

        $twiMlBody  = '';

        $dialVerb   = '';
        $sayVerb    = '';
        $recordVerb = '';
        $playVerb   = '';

        $physicalPhoneNumber = PhoneNumberAssignmentTable::getPhysicalPhoneNumber(
            $incomingNumberId
        );
        if (!empty($physicalPhoneNumber)) {
            $phoneNumber = $physicalPhoneNumber->getPhoneNumber();
        } else {
            $firstPhysicalNumber = $campaign->getSomePhysicalNumber();
            if (!empty($firstPhysicalNumber)) {
                $phoneNumber = $firstPhysicalNumber->getPhoneNumber();
            }
        }
        if (!empty($phoneNumber)) {
            $recordAttribute = '';
            if ($campaign->getIsRecordingsEnabled()) {
                if ($campaign->getIsRecordingsEnabled() == 2) {
                    $campaignId = $campaign->getId();
					$random = rand(0, 10000);
                    if (is_readable("audio/custom-redirect-msg/$campaignId.mp3")) {
                        $playVerb = "<Play>/audio/custom-redirect-msg/$campaignId.mp3?v=$random</Play>";
                    } elseif (is_readable("audio/custom-redirect-msg/$campaignId.wav")) {
                        $playVerb = "<Play>/audio/custom-redirect-msg/$campaignId.wav?v=$random</Play>";
                    } else {
                        $playVerb = "<Play>/audio/redirect_message.mp3</Play>";
                    }
                } else if($campaign->getIsRecordingsEnabled() == 3){ 
                	
                } else {
                    $playVerb = "<Play>/audio/redirect_message.mp3</Play>";
                }

                $recordAttribute = ' record="true"'; 
            }
            
            $q = Doctrine_Query::create()->select('c.*')->from('campaign c')->where('id = ?', $campaign->getId());
            $users = $q->fetchArray();
            $whisper = urlencode($users[0]['call_whisper']);
	            if(trim($whisper) != ''){
	            	
	            	//sfApplicationConfiguration::getActive()->loadHelpers(array('Url'));
	            	//$whisperUrl = url_for('@twilio_whisper_handler?whisper='.$whisper, true);
	            	
	            	$whisperUrl = "http://project4.bionicclick.com/incomingCalls/whisper?whisper=".$whisper;
	            	
	            	$dialVerb =  "<Dial timeout=\"120\"$recordAttribute><Number url=\"$whisperUrl\" method=\"POST\">$phoneNumber</Number></Dial>";
	            	
	            }else{
	            	
	            	$dialVerb = "<Dial timeout=\"120\"$recordAttribute><Number>$phoneNumber</Number></Dial>";
	            	
	            }
            
        } else {
            if ($campaign->getIsRecordingsEnabled()) {
                $sayVerb = '<Say>Please leave your name, phone number and a brief message after the beep, and we will contact you as soon as possible</Say>';
                $recordVerb = '<Record/>';
            } else {
                $sayVerb = '<Say>We are sorry, called number is unavailable. We will contact you as soon as possible</Say>';
            }
        }
        $twiMlBody = $sayVerb . $recordVerb . $playVerb . $dialVerb;
        return $twiMlBody;
    }

    /**
     *
     * @param Advertiser $advertiser
     * @return string 
     */
    protected static function generateTwiMlByAdvertiser($advertiser) {
        //NOTE:: There is no campaign for this incoming number

        $twiMlBody = '';

        $sayVerb    = '';
        $dialVerb   = '';

        $defaultNumber = $advertiser->getDefaultPhoneNumber();
        if (!empty($defaultNumber)) {
            $sayVerb = '<Say>We are sorry, called number is unavailable. You are calling to the support staff now</Say>';
            $dialVerb = "<Dial timeout=\"120\" record=\"true\"><Number>$defaultNumber</Number></Dial>";
        }

        $twiMlBody = $sayVerb . $dialVerb;

        return $twiMlBody;
    }

    /**
     *
     * @param Company $company
     * @return string 
     */
    protected static function generateTwiMlForBionic($company) {

        $twiMlBody = '';

        if (!empty($company) && $company->getLevel() == CompanyTable::$LEVEL_BIONIC) {

//            //TODO: what about if company doesn't want to pay for redialed call
//            $defaultNumber = $company->getDefaultPhoneNumber();
//            if (!empty($defaultNumber)) {
//                $sayVerb = '<Say>We are sorry, called number is unavailable. You are calling to the support staff now.</Say>';
//                $dialVerb = "<Dial timeout=\"120\" record=\"true\"><Number>$defaultNumber</Number></Dial>";
//                $twiMlBody = $sayVerb . $dialVerb;
//            }

            $sayVerb = '<Say>We are sorry, called number is unavailable.</Say>';
            $twiMlBody = $sayVerb;
        }

        return $twiMlBody;
    }

    /**
     * Get a response for phone number of some debtor.
     * 
     * @return string
     */
    protected static function getDebtorCallCommand() {
        return "<Reject />";
    }

    /**
     * Get a response for blocked phone number
     * 
     * @return string
     */
    protected static function getBlockedCallCommand() {
        return "<Reject />";
    }
}


