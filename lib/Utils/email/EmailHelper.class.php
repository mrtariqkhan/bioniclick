<?php

class EmailHelper {

    const CONTENT_TYPE_TEXT_HTML    = 'text/html';
    const CONTENT_TYPE_TEXT_PLAIN   = 'text/plain';

    const OPTION_FROM                   = 'from';
    const OPTION_TO                     = 'to';
    const OPTION_SUBJECT                = 'subject';
    const OPTION_TEMPLATES              = 'templates';
    const OPTION_ATTACHEMENTS           = 'attachements';

    const SUB_OPTION_TEMPLATE               = 'template';
    const SUB_OPTION_TEMPLATE_PARAMETERS    = 'templateParameters';
    const SUB_OPTION_TEMPLATE_CONTENT_TYPE  = 'templateContentType';
    const SUB_OPTION_TEMPLATE_PARAMETER_LOGO= 'logo';

    const SUB_OPTION_EMAIL                  = 'email';
    const SUB_OPTION_FULLNAME               = 'fullname';

    const SUB_OPTION_ATTACHEMENT_FILE_PATH  = 'attachementFilePath';
    const SUB_OPTION_ATTACHEMENT_MIME_TYPE  = 'attachementMimeType';


    const PATH_FROM_WEB_TO_LOGO = 'images/skins/green/logo.png';


    public static function mail($options) {

        $mailer = sfContext::getInstance()->getMailer();

        $from               = self::getOption($options, self::OPTION_FROM, true);
        $to                 = self::getOption($options, self::OPTION_TO, true);
        $subject            = self::getOption($options, self::OPTION_SUBJECT, true);
        $templates          = self::getOption($options, self::OPTION_TEMPLATES, false, array());
        $attachements       = self::getOption($options, self::OPTION_ATTACHEMENTS, false, array());

        $templates      = is_array($templates) ? $templates : array();
        $attachements   = is_array($attachements) ? $attachements : array();

//        $to = array(
//            self::SUB_OPTION_EMAIL      => '',
//            self::SUB_OPTION_FULLNAME   => 'Test Name',
//        );

        $fromEmail      = $mailer->getRealtimeTransport()->getUsername();
        $fromFullName   = self::getOption($from, self::SUB_OPTION_FULLNAME, false);
        $toEmail        = self::getOption($to, self::SUB_OPTION_EMAIL, false);
        $toFullName     = self::getOption($to, self::SUB_OPTION_FULLNAME, false);

        $message = Swift_Message::newInstance()
            ->setFrom($fromEmail, $fromFullName)
            ->setTo($toEmail, $toFullName)
            ->setSubject($subject)
        ;

        foreach ($templates as $templateInfo) {
            $partialName        = self::getOption($templateInfo, self::SUB_OPTION_TEMPLATE, true);
            $partialParameters  = self::getOption($templateInfo, self::SUB_OPTION_TEMPLATE_PARAMETERS, false, array());
            $contentType        = self::getOption($templateInfo, self::SUB_OPTION_TEMPLATE_CONTENT_TYPE, true);
            $logoParameterName  = self::getOption($templateInfo, self::SUB_OPTION_TEMPLATE_PARAMETER_LOGO, false);

            if (!empty($logoParameterName)) {
                $logo = self::embedLogoImage($message);

                $partialParameters = array_merge(
                    $partialParameters,
                    array($logoParameterName => $logo)
                );
            }

            self::attachPartial($message, $partialName, $partialParameters, $contentType);
        }

        foreach ($attachements as $attachementInfo) {
            $filePath   = self::getOption($attachementInfo, self::SUB_OPTION_ATTACHEMENT_FILE_PATH, true);
            $mimeType   = self::getOption($attachementInfo, self::SUB_OPTION_ATTACHEMENT_MIME_TYPE, true);

            self::attachFile($message, $filePath, $mimeType);
        }

        $message->setEncoder(Swift_Encoding::get8BitEncoding());

        //NOTE:: Task #3526: make it Critical for production environment
        Loggable::putLogMessage(
            ApplicationLogger::EMAIL_LOG_TYPE,
            "Email will be sent from '$fromEmail' to '$toEmail' with subject '$subject'",
//            sfLogger::INFO
            sfLogger::CRIT
        );

        $mailer->send($message);
    }

    protected static function attachPartial(
        &$message, 
        $partialName, 
        $partialParameters, 
        $contentType
    ) {

        sfContext::getInstance()->getConfiguration()->loadHelpers('Partial');
        $message->attach(
            self::createContentAttachmentAny(
                get_partial($partialName, $partialParameters),
                $contentType
            )
        );
    }

    protected static function attachFile(
        &$message, 
        $filePath, 
        $mimeType
    ) {
        if (!empty($filePath) && !empty($mimeType)) {
            $swiftAttachment = Swift_Attachment::fromPath($filePath, $mimeType);
            $message->attach($swiftAttachment);
        }
    }

    protected static function embedLogoImage(&$message) {

        $swiftImage = Swift_Image::fromPath(sfConfig::get('sf_web_dir') . DIRECTORY_SEPARATOR . self::PATH_FROM_WEB_TO_LOGO);

        $logo = $message->embed($swiftImage);

        return $logo;
    }
    
    protected static function createContentAttachmentAny($content, $contentType) {

        $part = Swift_MimePart::newInstance($content, $contentType);
        $part->setEncoder(Swift_Encoding::get8BitEncoding());

        return $part;
    }

    protected static function getOption($search, $key, $isRequired, $default = '') {

        if (array_key_exists($key, $search)) {
            return $search[$key];
        }

        if ($isRequired) {
            throw new sfException("option $key is required for email sending.");
        }

        return $default;
    }
}
