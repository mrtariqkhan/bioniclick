<?php

class SecurityHelper {

    const PERMISSION_SUFFIX_READ    = 'read';
    const PERMISSION_SUFFIX_CREATE  = 'create';
    const PERMISSION_SUFFIX_UPDATE  = 'update';
    const PERMISSION_SUFFIX_DELETE  = 'delete';
    const PERMISSION_SUFFIX_FILTER  = 'filter';

    const PERMISSION_SPECIAL_SUFFIX_REVERT          = 'revert';
    const PERMISSION_SPECIAL_SUFFIX_SNIPPET         = 'snippet';
    const PERMISSION_SPECIAL_SUFFIX_INSTRUCTIONS    = 'instructions';
    const PERMISSION_SPECIAL_SUFFIX_SEE_EXPIRY_FIELD= 'expiry';

    const PERMISSION_SPECIAL_SUPER_ADMIN_SUFFIX_BILLING = 'billing';

    protected $excludePermissions = array();
    protected $excludePermissionsSuffixesSuperAdmin = array(
//        'campaign'  => array('create'), //, 'update'),
        'tabs'      => array('billing', 'administration'),
    );

    protected $moduleName = null;

    protected $secureUser   = null;
    protected $guardUser    = null;

    protected $permissionSuffixesBase = array();
    protected $permissionSuffixesAll  = array();

    public function __construct($actions, $moduleName = null) {

        $this->setModule($actions, $moduleName);

        $this->secureUser   = $actions->getUser();
        $this->guardUser    = $actions->getUser()->getGuardUser();

        $this->excludePermissions = $this->configureExcludePermissions();

        $this->permissionSuffixesBase = $this->configurePermissionSuffixesBase();
        $this->permissionSuffixesAll  = $this->configurePermissionSuffixesAll();
    }

    public function checkUserPermissions($creadentials) {

        if (empty($creadentials)) {
            return true;
        }

        foreach ($creadentials as $creadential) {
            if ($this->secureUser->hasCredential($creadential)) {
                return $this->checkPermissionSpecialCases($creadential);
            }
        }

        return false;
    }

    public function getPermissionFullName($suffix) {

        $permissionName = $this->getPermissionBaseName();
        $permissionName = empty($permissionName) ? null : $permissionName . '_' . $suffix;

        return $permissionName;
    }

    public function getPermissionBaseName() {
        return $this->moduleName;
    }

    public function isSuperAdmin() {
        return $this->secureUser->isSuperAdmin();
    }

    public function getPermissionSuffixesBase() {
        return $this->permissionSuffixesBase;
    }

    public function getPermissionSuffixesAll() {
        return $this->permissionSuffixesAll;
    }

    public function checkBasePermissions() {
        return $this->checkPermissionsBySuffixes($this->getPermissionSuffixesBase());
    }

    public function checkAllPermissions() {
        return $this->checkPermissionsBySuffixes($this->getPermissionSuffixesAll());
    }

    public function checkPermissionsBySuffixes($suffixes) {

        $resultAllow = array();

        foreach ($suffixes as $suffix) {

            $subjectToAllow = 'can_' . $suffix;
            $resultAllow[$subjectToAllow] = $this->checkPermissionCommon($suffix);
        }

        return $resultAllow;
    }

    public function checkPermissionCommon($suffix) {

        $permissionName = $this->getPermissionFullName($suffix);

        return $this->checkPermission($permissionName);
    }

    protected function setModule($actions, $moduleName = null) {

        if (empty($moduleName)) {
            $moduleName = $actions->getModuleName();
        }

        $this->moduleName = $moduleName;
    }

    protected function checkPermission($permissionName) {

        $isAllowed = false;

        if ($this->secureUser->isAuthenticated()) {
            $isAllowed = $this->secureUser->isSuperAdmin()
                || $this->secureUser->hasCredential($permissionName)
            ;
        } else {
            return $isAllowed;
        }

        if ($isAllowed) {
            $isAllowed = $this->checkPermissionSpecialCases($permissionName);
        }

        return $isAllowed;
    }

    protected static function configurePermissionSuffixesBase() {

        $suffixes = array(
            self::PERMISSION_SUFFIX_READ,
            self::PERMISSION_SUFFIX_CREATE,
            self::PERMISSION_SUFFIX_UPDATE,
            self::PERMISSION_SUFFIX_DELETE,
            self::PERMISSION_SUFFIX_FILTER,
        );
        return $suffixes;
    }

    protected static function configurePermissionSuffixesAll() {

        $className = get_class();
        $reflectionClass = new ReflectionClass($className);
        $classConstants = $reflectionClass->getConstants();

        $suffixes = array();
        foreach ($classConstants as $classConstantName => $suffix) {
            if (preg_match('/^PERMISSION_SUFFIX_(.*)$/', $classConstantName, $matches)
                || preg_match('/^PERMISSION_SPECIAL_SUFFIX_(.*)$/', $classConstantName, $matches)
            ) {
                $suffixes[] = $suffix;
            }
        }
        return $suffixes;
    }

    protected function configureExcludePermissions() {

        $excludePermissions = array();
        $excludePermissionsSuffixesAll = array();

        if ($this->secureUser->isSuperAdmin()) {
            $excludePermissionsSuffixesAll = $this->excludePermissionsSuffixesSuperAdmin;
        }

        if (!empty($excludePermissionsSuffixesAll)) {
            $baseName = $this->getPermissionBaseName();
            $excludePermissionsSuffixes = array_key_exists($baseName, $excludePermissionsSuffixesAll)
                ? $excludePermissionsSuffixesAll[$baseName]
                : array()
            ;

            foreach ($excludePermissionsSuffixes as $suffix) {
                $excludePermissions[] = $this->getPermissionFullName($suffix);
            }
        }

        return $excludePermissions;
    }

    protected function checkPermissionSpecialCases($permissionName) {

        $isAllowed = true;

        foreach ($this->excludePermissions as $excludePermissionName) {
            if ($permissionName == $excludePermissionName) {
                return false;
            }
        }

        return $isAllowed;
    }

    public function checkCanSeeTabBilling() {

        if (!sfConfig::get('app_authorize_work_on')) {
            return false;
        }
        return $this->checkPermissionCommon(self::PERMISSION_SPECIAL_SUPER_ADMIN_SUFFIX_BILLING);
    }

//TODO:: uncomment when it will be need
//    public function checkCanRead() {
//        return $this->checkPermissionCommon(self::PERMISSION_SUFFIX_READ);
//    }
//
//    public function checkCanCreate() {
//        return $this->checkPermissionCommon(self::PERMISSION_SUFFIX_CREATE);
//    }
//
//    public function checkCanUpdate() {
//        return $this->checkPermissionCommon(self::PERMISSION_SUFFIX_UPDATE);
//    }
//
//    public function checkCanDelete() {
//        return $this->checkPermissionCommon(self::PERMISSION_SUFFIX_DELETE);
//    }
//
//    public function checkCanRevert() {
//        return $this->checkPermissionCommon(self::PERMISSION_SUFFIX_REVERT);
//    }
//
//    public function checkCanFilter() {
//        return $this->checkPermissionCommon(self::PERMISSION_SUFFIX_FILTER);
//    }
}
