<?php

class WasDeletedHelper {

    const TYPE_NON_DELETED  = 0;
    const TYPE_DELETED      = 1;
    const TYPE_ALL          = 2;

    const TITLE     = 'Was Deleted';
    const KEY       = 'getWasDeleted';
    const FIELD_NAME= 'was_deleted';

    public static function getAsString($wasDeleted) {
        return BooleanHelper::getAsString($wasDeleted);
    }

    public static function ifNeedWasDeletedColumn($user) {

        $wasDeletedType = self::getType($user);
        return $wasDeletedType == self::TYPE_ALL;
    }

    /**
     *
     * @param type $object must have getWasDeleted method
     * @return type
     */
    public static function ifCanUpdate($object) {

        if (empty($object)) {
            return false;
        }

        $methodName = self::KEY;
        if (!method_exists($object, $methodName)) {
            return true;
        }

        $wasDeleted = $object->$methodName();
        $ifShowUpdateLinks = !$wasDeleted;

        return $ifShowUpdateLinks;
    }

    public static function ifCanUpdateAsArray(array $raw) {

        $ifShowUpdateLinks = !array_key_exists(WasDeletedHelper::KEY, $raw) || !$raw[WasDeletedHelper::KEY];

        return $ifShowUpdateLinks;
    }

    public static function getType($user) {

        $wasDeletedType = !empty($user) && $user->getIsSuperAdmin()
            ?  WasDeletedHelper::TYPE_ALL
            : WasDeletedHelper::TYPE_NON_DELETED
        ;
        return $wasDeletedType;
    }

    /**
     *
     * @param type $wasDeletedType one of WasDeletedHelper::TYPE_*
     * @param array $objectFieldsHeaders
     */
    public static function addHeader($user, & $objectFieldsHeaders) {

        $wasDeletedType = self::getType($user);
        self::addHeaderByType($wasDeletedType, $objectFieldsHeaders);
    }

    /**
     *
     * @param type $wasDeletedType one of WasDeletedHelper::TYPE_*
     * @param array $objectFieldsHeaders
     */
    protected static function addHeaderByType($wasDeletedType, & $objectFieldsHeaders) {

        if ($wasDeletedType == WasDeletedHelper::TYPE_ALL) {
            $objectFieldsHeaders[self::KEY] = self::TITLE;
        }
    }

    /**
     *
     * @param type $wasDeletedType one of WasDeletedHelper::TYPE_*
     * @param array $simpleFilterFieldNames
     */
    public static function addSimpleFilterFieldName($user, & $simpleFilterFieldNames) {

        $wasDeletedType = self::getType($user);
        self::addSimpleFilterFieldNameByType($wasDeletedType, $simpleFilterFieldNames);
    }

    /**
     *
     * @param type $wasDeletedType one of WasDeletedHelper::TYPE_*
     * @param array $simpleFilterFieldNames
     */
    protected static function addSimpleFilterFieldNameByType($wasDeletedType, & $simpleFilterFieldNames) {

        if ($wasDeletedType == WasDeletedHelper::TYPE_ALL) {
            $key = BranchHelperBase::SIMPLE_FILTER_FIELD_TYPE_STRING;
            if (!array_key_exists($key, $simpleFilterFieldNames)) {
                $simpleFilterFieldNames[$key] = array();
            }
            array_push($simpleFilterFieldNames[$key], self::FIELD_NAME);
        }
    }

}
