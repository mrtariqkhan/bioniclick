<?php

/**
 * Description of CsvHelper
 *
 * @author fairdev
 */
class CsvGenerator extends FileGeneratorBase {

    const FILE_EXTENSION            = '.csv';
    const SF_DIR_TO_GENERATE        = 'sf_upload_dir';
    const PATH_TO_DIR_ADDITIONAL    = 'csv/';

    protected $file         = null;
    protected $pager        = null;
    protected $indexHelper= null;


    /**
     *
     * @param type $data 
     */
    public function __construct($pager, $indexHelper) {

        $this->setFileExtension(self::FILE_EXTENSION);
        $this->setSfDirToGenerate(self::SF_DIR_TO_GENERATE);
        $this->setPathToDirAdditional(self::PATH_TO_DIR_ADDITIONAL);

        parent::__construct();

        $this->setPager($pager);
        $this->setIndexHelper($indexHelper);
    }

    protected function preFillFile() {

        $this->generateFileName();
        $this->deleteFile();

        $file = fopen($this->getPathToFile(), 'w+');
        $this->setFile($file);
    }
    protected function fillFile() {

        $pager      = $this->getPager();
        $indexHelper= $this->getIndexHelper();

        $logTitles = $indexHelper->getTitles();
       // $logTitles = array_diff($logTitles, array('Agency', 'Reseller','Price' ,'Recording','Page',"Combo"));
        $rawTitles = array_values($logTitles);
        $rawTitles = array_merge(
            array('Call Record'),
            $rawTitles
        );

        $this->doWriteRaw($rawTitles);

        $list = $pager->getResults();

        $i = $pager->getFirstIndice();

        foreach ($list as $raw) {

            $indexHelper->makeRaw($raw);
            $rawToWrite = array();
            $rawToWrite[] = $i++;
            
            foreach ($logTitles as $fieldKey => $fieldTitle) {
                $rawToWrite[] = $raw[$fieldKey];
            }

            $this->doWriteRaw($rawToWrite);
        }



    }
    protected function postFillFile() {

        $file = $this->getFile();
        fclose($file);

        return $this->getPathToFile();
    }

    protected function doWriteRaw(array $raw) {
        fputcsv($this->getFile(), $raw, ',');
    }

    protected function generateFileNameUnique() {

        $fileNameGenerated = 'Call Logs ' . date("Y-m-d");

        return $fileNameGenerated;
    }
    protected function generateFileNamePrefix() {
        return '';
    }

    public function getFile() {
        return $this->file;
    }
    protected function setFile($file) {
        $this->file = $file;
    }

    public function getPager() {
        return $this->pager;
    }
    protected function setPager($pager) {
        $this->pager = $pager;
    }

    public function getIndexHelper() {
        return $this->indexHelper;
    }
    protected function setIndexHelper($indexHelper) {
        $this->indexHelper = $indexHelper;
    }
}

