<?php

class TwilioIncomingCallIndexInteractiveHelper extends TwilioIncomingCallIndexHelper{


    protected function configureHeaders() {

        parent::configureHeaders();

        $this->removeHeader('recording');
        $this->removeHeader('price');
        $this->removeHeader('recording_id');
        $this->removeHeader('caller_id');
    }
}
