<?php

class TwilioIncomingCallCSVHelper extends TwilioIncomingCallIndexHelper {


    protected function configureHeaders() {

        parent::configureHeaders();

        $headerInfoVaDescr = $this->getHeader('va_status_description');

        $headerInfoDuration = array(
            self::HEADER_PARAMETER_TABLE        => 'trc',
            self::HEADER_PARAMETER_SELECT       => 'IF(trc.id IS NOT NULL, trc.duration, tic.duration)',
            self::HEADER_PARAMETER_TITLE        => 'Duration in seconds',
        );

        $this->removeHeader('callerNumber.blocked');
        //$this->removeHeader('recording');
        //$this->removeHeader('recording_id');
        $this->removeHeader('caller_id');
        $this->removeHeader('call_result_id');
        $this->removeHeader('sid');
        // $this->removeHeader('vaId');
//        $this->removeHeader('va_status'); // NOTE:: it is need
        $this->removeHeader('va_status_description');

        $this->addHeaderInfoAfter('duration_sec', $headerInfoDuration, 'endTime');

        if ($this->doShowVaInfo()) {
            $headerInfoVaDescr[self::HEADER_PARAMETER_TITLE] = "Call's visitor analytics";
            $this->addHeaderInfoAfter('va_status_description', $headerInfoVaDescr, null);
        }
    }
}