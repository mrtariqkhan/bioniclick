<?php

class TwilioIncomingCallIndexPager extends sfPager {

    public function init() {

        if (0 == $this->getPage() || 0 == $this->getMaxPerPage() || 0 == $this->getNbResults()) {
            $this->setLastPage(0);
        } else {
            $this->setLastPage(ceil($this->getNbResults() / $this->getMaxPerPage()));
        }
    }

    public function getResults() {
        return $this->results;
    }

    public function setResults($results) {
        $this->results = $results;
    }

    public function setCount($count) {
        $this->setNbResults($count);
    }

    protected function retrieveObject($offset) {

    }
}
