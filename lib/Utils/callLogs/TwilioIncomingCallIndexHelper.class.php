<?php

class TwilioIncomingCallIndexHelper {

    const RECORDING_UNDEFINED   = 'None';
    const VALUE_UNDEFINED       = 'Undefined';

    const DIRECT_KEYWORD        = 'Direct';
    const DIRECT_SEARCH_TYPE    = 'Direct';
//    const DIRECT_TRAFFIC_SOURCE = 'Direct';


    const FORMAT_START_TIME     = 'h:i:s a D m/d/Y'; //'Y-m-d h:i a D'; //'Y-m-d H:i:s';
    const FORMAT_END_TIME       = 'h:i:s a D m/d/Y'; //'Y-m-d h:i a D'; //'Y-m-d H:i:s';
    const FORMAT_VISITING_TIME  = 'h:i:s a D m/d/Y'; //'Y-m-d h:i a D'; //'Y-m-d H:i:s';

    const FORMAT_START_TIME_SQL     = '%h:%i:%s %a %D %m/%d/%Y'; //'Y-m-d h:i a D'; //'Y-m-d H:i:s';
    const FORMAT_END_TIME_SQL       = '%h:%i:%s %a %D %m/%d/%Y'; //'Y-m-d h:i a D'; //'Y-m-d H:i:s';
    const FORMAT_VISITING_TIME_SQL  = '%h:%i:%s %a %D %m/%d/%Y'; //'Y-m-d h:i a D'; //'Y-m-d H:i:s';

    const PARAMETER_NAME_VA_TYPE = 'visitor_analytics_type';


    protected $user                                     = null;
    protected $userCompanyLevel                         = null;
    protected $campaignId                               = null;
    protected $timezoneName                             = null;
    protected $availableSecondsBetweenAnalyticsAndCall  = null;

    /*
     *  array (
     *      shortNameTable = array (
     *          shortNanePartTable,
     *          nameTable,
     *      )
     *  )
     */
    protected $tableAliases = array(
        'ad'    => array('pc',  'Advertiser'),
        'cp'    => array('pc',  'Campaign'),
        'va'    => array('pc',  'VisitorAnalytics'),
        'cm'    => array('va',  'Combo'),
        'pg'    => array('cm',  'Page'),
        'rs'    => array('va',  'ReferrerSource'),
        'cr'    => array('tic', 'CallResult'),
        'trc'   => array('tic', 'TwilioRedialedCall'),
        'tcpn'  => array('tic', 'TwilioCallerPhoneNumber'),
        'tipn'  => array('tic', 'TwilioIncomingPhoneNumber'),
        'tcr'   => array('tic', 'TwilioCallRecording'),
        'tc'    => array('tic', 'TwilioCity'),
    );

    const HEADER_PARAMETER_TABLE         = 'table';
    const HEADER_PARAMETER_SELECT        = 'select';
    const HEADER_PARAMETER_TITLE         = 'title';
    const HEADER_PARAMETER_ABRIDGENT     = 'abridgement';
    const HEADER_PARAMETER_IS_VA_COLUMN  = 'is_va_column';
    const HEADER_PARAMETER_UNDEFINED     = 'undefined';

    /*
     * $headers is
     *  array (
     *      shortName = array (
     *          self::HEADER_PARAMETER_TABLE        => '',
     *          self::HEADER_PARAMETER_SELECT       => '',
     *          self::HEADER_PARAMETER_TITLE        => '',
     *          self::HEADER_PARAMETER_ABRIDGENT    => '',
     *          self::HEADER_PARAMETER_IS_VA_COLUMN => '',
     *      )
     *   )
     */
    protected $headers = array();

    /*
     * $titles is array (shortName => title)
     */
    protected $titles = array();


    public function __construct(
        $campaignId,
        sfGuardUser $user,
        $availableSecondsBetweenAnalyticsAndCall = null,
        $advertiserId = -1
    ) {

        $this->user = $user;
        $this->userCompanyLevel = $user->getLevel();
        $this->campaignId = $campaignId;
        $this->advertiserId = $advertiserId;
        $this->timezoneName = $this->user->findTimezoneName();
        $this->availableSecondsBetweenAnalyticsAndCall = $availableSecondsBetweenAnalyticsAndCall;

        $this->configureHeaders();
        $this->configureTitles();
    }

    protected function configureTitles() {

        $titles = array();
        foreach ($this->headers as $fieldTitle => $fieldInfo) {
            if (isset($fieldInfo[self::HEADER_PARAMETER_TITLE])) {
                $titles[$fieldTitle] = $fieldInfo[self::HEADER_PARAMETER_TITLE];
            }
        }

        $this->titles = $titles;
    }

    protected function configureHeaders() {
       
        $this->headers = array(
            'callToDefault' => array(
                self::HEADER_PARAMETER_TABLE        => 'pc',
                self::HEADER_PARAMETER_SELECT       => 'IF(pc.phone_number_was_default, "Yes", "")',
                self::HEADER_PARAMETER_TITLE        => 'Is it Call To Default Number',
            ),
            'referrerSource' => array(
                self::HEADER_PARAMETER_TABLE        => 'rs',
                self::HEADER_PARAMETER_SELECT       => 'IF(va.is_organic = "' . VisitorAnalyticsTable::TYPE_DIRECT . '", "' . self::DIRECT_SEARCH_TYPE . '", rs.url)',
                self::HEADER_PARAMETER_TITLE        => 'Referrer Source',
                self::HEADER_PARAMETER_ABRIDGENT    => true,
                self::HEADER_PARAMETER_IS_VA_COLUMN => true,
            ),
            'searchType' => array(
                self::HEADER_PARAMETER_TABLE        => 'va',
                self::HEADER_PARAMETER_SELECT       => 'va.is_organic',
                //TODO:: uncomment when visitorAnalytics will be incorrect va.is_organic for bing also
//                self::HEADER_PARAMETER_SELECT        => 'IF(rs.url LIKE "%bing%", NULL, va.is_organic)',
                self::HEADER_PARAMETER_TITLE        => 'Search Type',
                self::HEADER_PARAMETER_IS_VA_COLUMN => true,
            ),
//            'visitingTime' => array(
//                self::HEADER_PARAMETER_TABLE         => 'va',
//                self::HEADER_PARAMETER_SELECT        => 'va.analytics_time',
//                self::HEADER_PARAMETER_TITLE         => 'Visiting Time',
//                self::HEADER_PARAMETER_IS_VA_COLUMN  => true,
//            ),
            'startTime' => array(
                self::HEADER_PARAMETER_TABLE        => 'trc',
                self::HEADER_PARAMETER_SELECT       => 'IF(trc.id IS NOT NULL, FROM_UNIXTIME(trc.start_time),  FROM_UNIXTIME(tic.start_time))',
                self::HEADER_PARAMETER_TITLE        => 'Start Time',
            ),
            'endTime' => array(
                self::HEADER_PARAMETER_TABLE        => 'trc',
                self::HEADER_PARAMETER_SELECT       => 'IF(trc.id IS NOT NULL, FROM_UNIXTIME(trc.end_time), FROM_UNIXTIME(tic.end_time))',
                self::HEADER_PARAMETER_TITLE        => 'End Time',
            ),
           'duration' => array(
                self::HEADER_PARAMETER_TABLE        => 'trc',
                self::HEADER_PARAMETER_SELECT       => 'IF(trc.id IS NOT NULL, trc.duration, tic.duration)',
                self::HEADER_PARAMETER_TITLE        => 'Duration',
            ),
            'keyword' => array(
                self::HEADER_PARAMETER_TABLE        => 'va',
                self::HEADER_PARAMETER_SELECT       => '
                    IF(
                        va.is_organic = "' . VisitorAnalyticsTable::TYPE_DIRECT . '"
                        , "' . self::DIRECT_KEYWORD . '"
                        , IF(
                            va.keyword = "0"
                                OR va.keyword = "undefined"
                                OR va.keyword = ""
                                OR va.keyword LIKE "N/A"
                                OR va.keyword LIKE "&esrc"
                            , NULL
                            , va.keyword
                        )
                    )
                ',
                self::HEADER_PARAMETER_TITLE        => 'Keyword',
                self::HEADER_PARAMETER_ABRIDGENT    => true,
                self::HEADER_PARAMETER_IS_VA_COLUMN => true,
            ),
            'url' => array(
                self::HEADER_PARAMETER_TABLE        => 'pg',
                self::HEADER_PARAMETER_SELECT       => '
                    IF(
                        va.traffic_source IS NULL
                        , CONCAT("' . CampaignTable::PROTOCOL_HTTP . '", cp.domain, "/", pg.page_url, "?cmbid=", cm.api_key)
                        , va.traffic_source
                    )
                ',
                self::HEADER_PARAMETER_TITLE        => 'URL',
                self::HEADER_PARAMETER_IS_VA_COLUMN => true,
            ),
            'revenue' => array(
                self::HEADER_PARAMETER_TABLE        => 'cr',
                self::HEADER_PARAMETER_SELECT       => 'cr.revenue',
                self::HEADER_PARAMETER_TITLE        => 'Revenue',
            ),
            'callerNumber' => array(
                self::HEADER_PARAMETER_TABLE        => 'tcpn',
                self::HEADER_PARAMETER_SELECT       => 'tcpn.phone_number',
                self::HEADER_PARAMETER_TITLE        => 'Caller Number',
            ),
            'callerNumber.blocked' => array(
                self::HEADER_PARAMETER_TABLE        => 'tcpn',
                self::HEADER_PARAMETER_SELECT       => 'IF(tcpn.is_blocked, 1, 0)',
            ),
            'campaign' => array(
                self::HEADER_PARAMETER_TABLE        => 'pc',
                self::HEADER_PARAMETER_SELECT       => 'cp.name',
                self::HEADER_PARAMETER_TITLE        => 'Campaign',
            ),
            'conversion_url' => array(
                self::HEADER_PARAMETER_TABLE        => 'pc',
                self::HEADER_PARAMETER_SELECT       => 'cp.conversion_url',
                self::HEADER_PARAMETER_TITLE        => 'Conversion URL',
            ),
            'custom' => array(
                self::HEADER_PARAMETER_TABLE        => 'pc',
                self::HEADER_PARAMETER_SELECT       => 'cp.custom',
                self::HEADER_PARAMETER_TITLE        => 'Custom',
            ),
            'recording' => array(
                self::HEADER_PARAMETER_TABLE        => 'tcr',
               // self::HEADER_PARAMETER_SELECT       => 'tcr.file_url', // commenting it out to replace the db value of api.twilio.com to recordings.bionicclick.com
            	self::HEADER_PARAMETER_SELECT       =>" REPLACE(tcr.file_url, 'https://api.twilio.com', 'http://recordings.bionicclick.com') ",
                self::HEADER_PARAMETER_TITLE        => 'Recording',
                self::HEADER_PARAMETER_UNDEFINED    => '',
            ),
            'firstName' => array(
                self::HEADER_PARAMETER_TABLE        => 'cr',
                self::HEADER_PARAMETER_SELECT       => 'cr.first_name',
                self::HEADER_PARAMETER_TITLE        => 'First Name',
            ),
            'lastName' => array(
                self::HEADER_PARAMETER_TABLE        => 'cr',
                self::HEADER_PARAMETER_SELECT       => 'cr.last_name',
                self::HEADER_PARAMETER_TITLE        => 'Last Name',
            ),
            'sex' => array(
                self::HEADER_PARAMETER_TABLE        => 'cr',
                self::HEADER_PARAMETER_SELECT       => 'cr.sex',
                self::HEADER_PARAMETER_TITLE        => 'Sex',
            ),
            'address' => array(
                self::HEADER_PARAMETER_TABLE        => 'cr',
                self::HEADER_PARAMETER_SELECT       => 'cr.address',
                self::HEADER_PARAMETER_TITLE        => 'Address',
            ),
//            'trafficSource' => array(
//                self::HEADER_PARAMETER_TABLE        => 'va',
//                self::HEADER_PARAMETER_SELECT       => 'IF(va.is_organic = "' . VisitorAnalyticsTable::TYPE_DIRECT . '", "' . self::DIRECT_TRAFFIC_SOURCE . '", va.traffic_source)',
//                self::HEADER_PARAMETER_TITLE        => 'Traffic Source',
//                self::HEADER_PARAMETER_ABRIDGENT    => true,
//                self::HEADER_PARAMETER_IS_VA_COLUMN => true,
//            ),
            'page' => array(
                self::HEADER_PARAMETER_TABLE        => 'pg',
                self::HEADER_PARAMETER_SELECT       => 'pg.page_url',
                self::HEADER_PARAMETER_TITLE        => 'Page',
                self::HEADER_PARAMETER_IS_VA_COLUMN => true,
            ),
            'combo' => array(
                self::HEADER_PARAMETER_TABLE        => 'cm',
                self::HEADER_PARAMETER_SELECT       => 'cm.name',
                self::HEADER_PARAMETER_TITLE        => 'Combo',
                self::HEADER_PARAMETER_IS_VA_COLUMN => true,
            ),
            'incomingNumber' => array(
                self::HEADER_PARAMETER_TABLE        => 'tipn',
                self::HEADER_PARAMETER_SELECT       => 'tipn.phone_number',
                self::HEADER_PARAMETER_TITLE        => 'Incoming Number',
            ),
           'calledPhysicalNumber' => array(
                self::HEADER_PARAMETER_TABLE        => 'trc',
                self::HEADER_PARAMETER_SELECT       => 'IF(trc.id IS NOT NULL, trc.called_number, "Undefined")',
                self::HEADER_PARAMETER_TITLE        => 'Called Physical Number',
            ),
            'callerCountry' => array(
                self::HEADER_PARAMETER_TABLE        => 'tic',
                self::HEADER_PARAMETER_SELECT       => 'tic.caller_country',
                self::HEADER_PARAMETER_TITLE        => 'Caller Country',
            ),
            'callerZip' => array(
                self::HEADER_PARAMETER_TABLE        => 'tic',
                self::HEADER_PARAMETER_SELECT       => 'tic.caller_zip',
                self::HEADER_PARAMETER_TITLE        => 'Caller Zip',
            ),
            'callerState' => array(
                self::HEADER_PARAMETER_TABLE        => 'tic',
                self::HEADER_PARAMETER_SELECT       => 'tic.caller_state',
                self::HEADER_PARAMETER_TITLE        => 'Caller State',
            ),
            'callerCity' => array(
                self::HEADER_PARAMETER_TABLE        => 'tc',
                self::HEADER_PARAMETER_SELECT       => 'tc.name',
                self::HEADER_PARAMETER_TITLE        => 'Caller City',
            ),
            'callStatus' => array(
                self::HEADER_PARAMETER_TABLE        => 'trc',
                self::HEADER_PARAMETER_SELECT       => 'IF(trc.id IS NOT NULL, trc.call_status, tic.call_status)',
                self::HEADER_PARAMETER_TITLE        => 'Call Status',
            ),
            'price' => array(
                self::HEADER_PARAMETER_TABLE        => 'tic',
                self::HEADER_PARAMETER_SELECT       => 'tic.price',
                self::HEADER_PARAMETER_TITLE        => 'Price',
            ),
            'zip' => array(
                self::HEADER_PARAMETER_TABLE        => 'cr',
                self::HEADER_PARAMETER_SELECT       => 'cr.zip',
                self::HEADER_PARAMETER_TITLE        => 'Zip',
            ),
            'phone' => array(
                self::HEADER_PARAMETER_TABLE        => 'cr',
                self::HEADER_PARAMETER_SELECT       => 'cr.phone',
                self::HEADER_PARAMETER_TITLE        => 'Phone',
            ),
            'rating' => array(
                self::HEADER_PARAMETER_TABLE        => 'cr',
                self::HEADER_PARAMETER_SELECT       => 'cr.rating',
                self::HEADER_PARAMETER_TITLE        => 'Rating',
            ),
            'notes' => array(
                self::HEADER_PARAMETER_TABLE        => 'cr',
                self::HEADER_PARAMETER_SELECT       => 'cr.note',
                self::HEADER_PARAMETER_TITLE        => 'Notes',
                self::HEADER_PARAMETER_ABRIDGENT    => true,
            ),
            'advertiser'        => array(
                self::HEADER_PARAMETER_TABLE        => 'ad',
                self::HEADER_PARAMETER_SELECT       => 'ad.name',
                self::HEADER_PARAMETER_TITLE        => 'Advertiser',
            ),
            'agency' => array(
                self::HEADER_PARAMETER_TITLE        => 'Agency',
            ),
           'reseller' => array(
                self::HEADER_PARAMETER_TITLE        => 'Reseller',
            ),
            'recording_id' => array(
                self::HEADER_PARAMETER_TABLE        => 'tcr',
                self::HEADER_PARAMETER_SELECT       => 'tcr.id',
                self::HEADER_PARAMETER_UNDEFINED    => '',
            ),
            'caller_id' => array(
                self::HEADER_PARAMETER_TABLE        => 'tcpn',
                self::HEADER_PARAMETER_SELECT       => 'tcpn.id',
                self::HEADER_PARAMETER_UNDEFINED    => '',
            ),
            'call_result_id' => array(
                self::HEADER_PARAMETER_TABLE        => 'cr',
                self::HEADER_PARAMETER_SELECT       => 'cr.id',
                self::HEADER_PARAMETER_UNDEFINED    => '',
            ),
            'sid' => array(
                self::HEADER_PARAMETER_TABLE        => 'tic',
                self::HEADER_PARAMETER_SELECT       => 'tic.sid',
            ),
            'vaId' => array(
                self::HEADER_PARAMETER_TABLE        => 'va',
                self::HEADER_PARAMETER_SELECT       => 'va.id',
                self::HEADER_PARAMETER_TITLE        => 'Visitors Analytics Id',
                self::HEADER_PARAMETER_IS_VA_COLUMN => true,
            ),
            'va_referrer_source' => array(
                self::HEADER_PARAMETER_TABLE        => 'va',
                self::HEADER_PARAMETER_SELECT       => 'va.referrer_source',
                self::HEADER_PARAMETER_TITLE        => 'Visitors Analytics referrer_source',
                self::HEADER_PARAMETER_IS_VA_COLUMN => true,
            ),
            'va_status' => array(
                self::HEADER_PARAMETER_TABLE        => 'va',
                self::HEADER_PARAMETER_SELECT       => VisitorAnalyticsTable::createConditionToSwitchTypeCode(
                    'va',
                    'tic',
                    'pc',
                    $this->availableSecondsBetweenAnalyticsAndCall
                ),
            ),
            'va_status_description' => array(
                self::HEADER_PARAMETER_TABLE        => 'va',
                self::HEADER_PARAMETER_SELECT       => VisitorAnalyticsTable::createConditionToSwitchTypeDescription(
                    'va',
                    'tic',
                    'pc',
                    $this->availableSecondsBetweenAnalyticsAndCall
                ),
            ),
        );
    }

    protected function getHeader($key) {
        return array_key_exists($key, $this->headers) ? $this->headers[$key] : array();
    }

    protected function addHeaderInfoAfter($key, $info, $afterKey = null) {

        $headers = array();
        if (is_null($afterKey)) {
            $headers[$key] = $info;
        }

        foreach ($this->headers as $headerKey => $headerInfo) {
            $headers[$headerKey] = $headerInfo;

            if ($afterKey == $headerKey) {
                $headers[$key] = $info;
            }
        }

        $this->headers = $headers;
    }

    protected function removeHeader($key) {
        unset($this->headers[$key]);
    }

    /**
     * NOTE:: is public for testing
     *
     * @param array $filterValues
     * @param type $maxPerPage
     * @param type $firstIndex
     * @return integer amount of raws 
     */
    public function createIndexQuery(
        $filterValues,
        $maxPerPage = 'all',
        $firstIndex = 1
    ) {

        $alias = 'tic';
        $query = TwilioIncomingCallTable::getPageRows(
            $this->user,
            $this->campaignId,
            $firstIndex,
            $maxPerPage,
            $filterValues,
            $alias,
            $this->advertiserId
        );

        if (empty($query)) {
            $query = TwilioIncomingCallTable::getInstance()->createQuery($alias)->andWhere('false');
        } else {
            foreach ($this->tableAliases as $key => $table) {
                $join = "{$table[0]}.{$table[1]} $key";
//                //TODO:: uncomment to show calls which has suitable VA only without old VA.
//                if ($key == 'va') {
//                    $join = 
//                        "$join ON (
//                                {$table[0]}.visitor_analytics_id = $key.id 
//                            AND va.analytics_time < $alias.start_time 
//                            AND va.analytics_time >= ($alias.start_time - $this->availableSecondsBetweenAnalyticsAndCall)
//                        )"
//                    ;
//                }
                $query->leftJoin($join);
                $query->addSelect("{$table[0]}.id");
            }

            foreach ($this->headers as $key => $header) {
                if (isset($header[self::HEADER_PARAMETER_SELECT])) {
                    $query->addSelect("{$header[self::HEADER_PARAMETER_SELECT]} $key");
                }
            }

            $vaTypeCode = array_key_exists(self::PARAMETER_NAME_VA_TYPE, $filterValues) ? $filterValues[self::PARAMETER_NAME_VA_TYPE] : VisitorAnalyticsTable::TYPE_CODE_UNDEFINED;
            $vaTypeCode = intval($vaTypeCode);

            switch ($vaTypeCode) {
                case VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE:
                case VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD:
                case VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE:
                case VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT:
                    $query->andWhere(
                        VisitorAnalyticsTable::createConditionToSwitchTypeCode(
                            'va',
                            'tic',
                            'pc',
                            $this->availableSecondsBetweenAnalyticsAndCall
                        ) . " = $vaTypeCode"
                    );
                    break;
                default:
            }

//            //TODO:: uncomment to show calls which has VA only.
//            $query->andWhere("va.id IS NOT NULL");

        }

      //  echo $query->getSqlQuery();
        return $query;
    }

    /**
     *
     * @param TwilioIncomingCallIndexPager $pager
     * @param array $filterValues
     * @return TwilioIncomingCallIndexPager $pager
     */
    public function retrieveAll(
        TwilioIncomingCallIndexPager $pager,
        $filterValues,
        $csv =0
    ) {
        $pager->setResults(array());
        $maxPerPage = $pager->getMaxPerPage();
        $firstIndex = $pager->getFirstIndice() - 1;
        $query = $this->createIndexQuery(
            $filterValues,
            $maxPerPage, 
            $firstIndex
        );
        $raws = $query->execute()->toArray();
        if (!$csv) {
            $this->makeRaws($raws);
        }      

        $pager->setResults($raws);
        $pager->setCount($query->count());

        return $pager;
    }

    public function getTitles() {
        return $this->titles;
    }

    /**
     * Converts result of $pager->getResults(Doctrine_Core::HYDRATE_SCALAR) to
     * array with need names of columns
     * @param array $raw
     * @return array $raw
     */
    protected function makeRaws(& $raws) {

        $ifShowAllInfo = $this->doShowVaInfo();
        foreach ($raws as $i => $raw) {
            $raws[$i]['startTime']      = date_format(date_create($raw['startTime']),  self::FORMAT_START_TIME);
            $raws[$i]['endTime']        = date_format(date_create($raw['endTime']), self::FORMAT_END_TIME);
            $raws[$i]['duration']       = TimeConverter::convertSecondsToMinutes($raw['duration']);
            $raws[$i]['searchType']     = VisitorAnalyticsTable::convertIsOrganic($raw['searchType']);
            $raws[$i]['visitingTime']   = 
                empty($raw['visitingTime']) 
                ? self::VALUE_UNDEFINED 
                : TimeConverter::getDateTime($raw['visitingTime'], $this->timezoneName, self::FORMAT_VISITING_TIME)
            ;

            foreach ($this->headers as $headerKey => $header) {

                //TODO:: rewrite it:

                $status = self::VALUE_UNDEFINED;
                $campaignId = $raws[$i]['PhoneCall']['campaign_id'];
                if($campaignId){
                    $campaign = CampaignTable::findById($campaignId);
                    if($campaign && $campaign->isOffline())
                        $status = 'Offline';
                }

                if (
                        isset($header[self::HEADER_PARAMETER_IS_VA_COLUMN]) 
                    && !$ifShowAllInfo 
                    && $raws[$i]['va_status'] != VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE
                ) {
                    //set undefined fields to its campign name
                    $raws[$i][$headerKey] = $status;
                } else {
                    if (!isset($raws[$i][$headerKey]) || (!$raws[$i][$headerKey] && $raws[$i][$headerKey] != 0)) {
                        $raws[$i][$headerKey] = array_key_exists(self::HEADER_PARAMETER_UNDEFINED, $header) 
                            ? $header[self::HEADER_PARAMETER_UNDEFINED] 
                            : self::VALUE_UNDEFINED;

                        if($raws[$i][$headerKey] == self::VALUE_UNDEFINED &&
                            ($headerKey == 'referrerSource' || $headerKey == 'url' || $headerKey == 'keyword'))
                            $raws[$i][$headerKey] = $status;

                    } elseif (isset($header[self::HEADER_PARAMETER_ABRIDGENT]) && strlen($raw[$headerKey]) > 20) {
                        $raws[$i][$headerKey . '.short'] = substr($raw[$headerKey], 0, 15);
                    } elseif($headerKey == 'callToDefault' && $raws[$i][$headerKey] == ''){
                        // $raws[$i][$headerKey] = ($status == self::VALUE_UNDEFINED)? '' : $status;
                    }else if($headerKey == 'searchType'){
                        $raws[$i][$headerKey] = ($raws[$i][$headerKey] == self::VALUE_UNDEFINED)
                            ? $status 
                            : $raws[$i][$headerKey];
                    }

                }

                if (isset($header[self::HEADER_PARAMETER_IS_VA_COLUMN]) && $ifShowAllInfo) {
                    $raws[$i][$headerKey . '.is_va_column'] = true;
                }
            }

            //TODO: This is a temporary and slow solution
            $advertiser = AdvertiserTable::findById($raw['PhoneCall']['advertiser_id']);

            $branchHelper = new BranchHelper();
            $branch = $branchHelper->findBranchToLevelByObjects(
                array(
                    'advertiser'=> $advertiser,
                    'company'   => $advertiser->getCompany(),
                ), 
                CompanyTable::$LEVEL_BIONIC
            );

            $raws[$i]['agency']   = isset($branch['agency']) ? $branch['agency']->getName() : '';
            $raws[$i]['reseller'] = isset($branch['reseller']) ? $branch['reseller']->getName() : ''; 
        }
    }

    public function makeRaw(& $raw) {
        $ifShowAllInfo = $this->doShowVaInfo();
        $raw['startTime']      = date_format(date_create($raw['startTime']),  self::FORMAT_START_TIME);
        $raw['endTime']        = date_format(date_create($raw['endTime']), self::FORMAT_END_TIME);
        $raw['duration']       = TimeConverter::convertSecondsToMinutes($raw['duration']);
        $raw['searchType']     = VisitorAnalyticsTable::convertIsOrganic($raw['searchType']);
        $raw['visitingTime']   = 
            empty($raw['visitingTime']) 
            ? self::VALUE_UNDEFINED 
            : TimeConverter::getDateTime($raw['visitingTime'], $this->timezoneName, self::FORMAT_VISITING_TIME)
        ;

        $status = self::VALUE_UNDEFINED;
        $campaignId = $raw['PhoneCall']['campaign_id'];
        if($campaignId){
            $campaign = CampaignTable::findById($campaignId);
            if($campaign && $campaign->isOffline())
                $status = 'Offline';
        }

        foreach ($this->headers as $headerKey => $header) {

            //TODO:: rewrite it:

            if (
                    isset($header[self::HEADER_PARAMETER_IS_VA_COLUMN]) 
                && !$ifShowAllInfo 
                && $raw['va_status'] != VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE
            ) {
                //set undefined fields to its campign name
                $raw[$headerKey] = $status;
            } else {
                if (!isset($raw[$headerKey]) || (!$raw[$headerKey] && $raw[$headerKey] != 0)) {
                    $raw[$headerKey] = array_key_exists(self::HEADER_PARAMETER_UNDEFINED, $header) 
                        ? $header[self::HEADER_PARAMETER_UNDEFINED] 
                        : self::VALUE_UNDEFINED;

                    if($raw[$headerKey] == self::VALUE_UNDEFINED &&
                        ($headerKey == 'referrerSource' || $headerKey == 'url' || $headerKey == 'keyword'))
                        $raw[$headerKey] = $status;

                } elseif (isset($header[self::HEADER_PARAMETER_ABRIDGENT]) && strlen($raw[$headerKey]) > 20) {
                    $raw[$headerKey . '.short'] = substr($raw[$headerKey], 0, 15);
                } elseif($headerKey == 'callToDefault' && $raw[$headerKey] == ''){
                    // $raws[$i][$headerKey] = ($status == self::VALUE_UNDEFINED)? '' : $status;
                }else if($headerKey == 'searchType'){
                    $raw[$headerKey] = ($raw[$headerKey] == self::VALUE_UNDEFINED)
                        ? $status 
                        : $raw[$headerKey];
                }

            }

            if (isset($header[self::HEADER_PARAMETER_IS_VA_COLUMN]) && $ifShowAllInfo) {
                $raw[$headerKey . '.is_va_column'] = true;
            }
        }
            //TODO: This is a temporary and slow solution
            $advertiser = AdvertiserTable::findById($raw['PhoneCall']['advertiser_id']);

            $branchHelper = new BranchHelper();
            $branch = $branchHelper->findBranchToLevelByObjects(
                array(
                    'advertiser'=> $advertiser,
                    'company'   => $advertiser->getCompany(),
                ), 
                CompanyTable::$LEVEL_BIONIC
            );

            $raw['agency']   = isset($branch['agency']) ? $branch['agency']->getName() : '';
            $raw['reseller'] = isset($branch['reseller']) ? $branch['reseller']->getName() : '';

    }
    protected function doShowVaInfo() {
        return $this->user->getIsSuperAdmin();
    }
}