<?php

class DefaultHelper {

    public static function generate($className, $objectName = null) {

        if (empty($objectName)) {
            $objectName = sfInflector::underscore($className);
        }

        $defaultObject = new $className();

        $defaults = sfConfig::get("app_default_$objectName", array());
        foreach ($defaults as $fieldName => $value) {
            $defaultObject->set($fieldName, $value);
        }

        return $defaultObject;
    }
}
