<?php

/**
 * (PHP 4 &gt;= 4.0.2, PHP 5)<br/>
 *
 * Client URLs
 *
 * @link http://php.net/manual/en/function.curl-init.php
 * @link http://php.net/manual/en/function.curl-setopt.php
 * @link http://php.net/manual/en/function.curl-setopt-array.php
 * @link http://php.net/manual/en/function.curl-exec.php
 * @link http://php.net/manual/en/function.curl-close.php
 */
class cURL {
    private $headers;
    private $userAgent;
    private $compression;
    private $ifUseCookie;
    private $cookieFile;
    private $proxy;
    private $lastCurlErrorMsg;

    public static $METHOD_GET  = 'GET';
    public static $METHOD_POST = 'POST';

    /**
     *
     * @param boolean $ifUseCookie true if use cookie file. Default: true.
     * @param string $cookieFile cookie file. Default: 'web/cookies.txt'.
     * @param string $compression Default: 'gzip'.
     * @param string $proxy Default: ''.
     *
     * @throws Exception if file does not exist and we can't create it.
     */
    public function  __construct(
        $ifUseCookie = true,
        $cookieFile = 'cookies.txt',
        $compression = 'gzip',
        $proxy = ''
    ) {

        $this->headers = array();
        $this->addHeader('Connection: Keep-Alive');
        $this->addHeader('Accept: image/gif, image/x-bitmap, image/jpeg, ' .
            'image/pjpeg,text/xml,application/xml,application/xhtml+xml,' .
            'text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5');
        $this->addHeader('Connection: Keep-Alive');
        $this->addHeader('Charset: UTF-8');
        $this->addHeader('Content-type: application/x-www-form-urlencoded');

        $this->userAgent = 'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1;' .
            ' .NET CLR 1.0.3705; .NET CLR 1.1.4322; Media Center PC 4.0)';

        $this->compression = $compression;
        $this->proxy = $proxy;
        $this->ifUseCookie = $ifUseCookie;

        $this->setCookieFile($cookieFile);
        $this->setLastCurlErrorMsg('');
    }

    /**
     * Initializes a new cUrl GET session, executes, closes it.
     *
     * @param string $url URL to fetch
     * @param string $refer Contents of "Referer: " header in HTTP request.
     * @return string returns result of session's executing.
     */
    public function get($url, $refer = '') {

        $options = $this->configureCUrlOptions(self::$METHOD_GET, $url, $refer);
        return $this->execute($options);
    }

    /**
     * Initializes a new cUrl POST session, executes, closes it.
     *
     * @param string $url URL to fetch
     * @param string $postParams POST-params like 'param1=val1&param2=val2&...;
     * @param string $refer Contents of "Referer: " header in HTTP request.
     * @return string returns result of session's executing.
     */
    public function post($url, $postParams, $refer = '') {

        $options = $this->configureCUrlOptions(self::$METHOD_POST, $url, $refer, $postParams);
        return $this->execute($options);
    }

    /**
     * Initializes a new cUrl session, executes, closes it.
     *
     * @param array $cUrlOptions options for cURL
     * @return string returns result of session's executing.
     */
    private function execute($cUrlOptions) {
        $this->setLastCurlErrorMsg('');

        // create a new cURL resource
        $cUrlHandler = curl_init();

        // set appropriate options
        curl_setopt_array($cUrlHandler, $cUrlOptions);

        // grab URL and pass it to the browser
        $content = curl_exec($cUrlHandler);
        $this->setLastCurlErrorMsg(curl_error($cUrlHandler));
        $info = curl_getinfo($cUrlHandler);
        if ($info['http_code'] != 200) {
            $this->setLastCurlErrorMsg($info['http_code']);
        }

        // close cURL resource, and free up system resources
        curl_close($cUrlHandler);

        return $content;
    }

    /**
     *
     * @param string $methodType $METHOD_GET or $METHOD_POST
     * @param string $url URL to fetch
     * @param string $refer Contents of "Referer: " header in HTTP request.
     * @param string $postParams POST-params like 'param1=val1&param2=val2&...;
     * @return array options for cURL
     */
    private function configureCUrlOptions($methodType, $url, $refer, $postParams = '') {

        $headers = $this->headers;
//        if ($methodType == self::$METHOD_POST) {
//            $headers[] = 'Content-Length: ' . strlen($postParams);
//        }

        $cUrlOptions = array(
            CURLOPT_URL            => $url,              // URL to fetch. This can be set by curl_init.
            CURLOPT_RETURNTRANSFER => true,              // return web page
            CURLOPT_HEADER         => true,              // true to include the header in the output
            CURLOPT_HTTPHEADER     => $headers,          // array of HTTP header fields to set.
            CURLOPT_FOLLOWLOCATION => true,              // follow redirects
            CURLOPT_REFERER        => $refer,            // Contents of "Referer: " header in HTTP request.
            CURLOPT_USERAGENT      => $this->userAgent,  // who am i
            CURLOPT_CONNECTTIMEOUT => 120,               // timeout on connect
            CURLOPT_TIMEOUT        => 120,               // timeout on response
            CURLOPT_MAXREDIRS      => 30,                // stop after 30 redirects
            CURLOPT_SSL_VERIFYHOST => 0,                 // don't verify ssl
            CURLOPT_SSL_VERIFYPEER => false,             // false to stop cURL from verifying the peer's certificate
            CURLOPT_VERBOSE        => true,              // true to output verbose info. Writes output to STDERR.
//            CURLOPT_USERPWD        => '',                // "[username]:[password]" to use for the connection.
//            CURLOPT_ENCODING       => '',                // handle all encodings
//            CURLOPT_AUTOREFERER    => true,              // set referer on redirect
        );

        if ($methodType == self::$METHOD_POST) {
            $cUrlOptions[CURLOPT_POST]       = true;              // i am sending post data
            $cUrlOptions[CURLOPT_POSTFIELDS] = $postParams;       // this are my post vars.
        }

        if ($this->ifUseCookie) {
            $cUrlOptions[CURLOPT_COOKIEFILE] = $this->cookieFile; // filename containing cookie data
            $cUrlOptions[CURLOPT_COOKIEJAR]  = $this->cookieFile; // filename which saves all internal cookies.
        }

        return $cUrlOptions;
    }

    /**
     * Checks if file $cookieFile exists and open for modifications.
     * Creates this file if it does not exist and if can.
     *
     * @param string $cookieFile name of the file which contains cookie data
     *
     * @throws Exception if file does not exist and we can't create it.
     */
    private function setCookieFile($cookieFile) {

        if (!$this->ifUseCookie) {
            $this->cookieFile = '';
            return;
        }

        if (file_exists($cookieFile)) {
            $this->cookieFile = $cookieFile;
        } else {
            if (!@fopen($cookieFile, 'w')) {
                throw new Exception('The cookie file could not be opened. Make sure this directory has the correct permissions');
            }
            $this->cookieFile = $cookieFile;
            fclose($cookieFile);
        }
    }

    private function setLastCurlErrorMsg($lastCurlErrorMsg) {
        $this->lastCurlErrorMsg = $lastCurlErrorMsg;
    }
    public function getLastCurlErrorMsg() {
        return $this->lastCurlErrorMsg;
    }

    /**
     * @param string $additionalHeader additional header to request
     */
    public function addHeader($additionalHeader) {
        $this->headers[] = $additionalHeader;
    }
}
?>