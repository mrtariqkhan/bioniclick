<?php

/**
 * TimeConverter
 *
 * Unix timestamp means amount of seconds since the Unix Epoch (January 1 1970 00:00:00 GMT)
 *
 * @author konstantin
 */
class TimeConverter {

    const TIMEZONE_UTC      = 'UTC';

    const FORMAT_UNIX       = 'U'; // Seconds since the Unix Epoch (January 1 1970 00:00:00 GMT)
    const FORMAT_ALL        = 'Y-m-d H:i:s O';

    const WEEK_DAY_MONDAY   = 'mon';
    const WEEK_DAY_TUESDAY  = 'tue';
    const WEEK_DAY_WEDNESDAY= 'wed';
    const WEEK_DAY_THURSDAY = 'thu';
    const WEEK_DAY_FRIDAY   = 'fri';
    const WEEK_DAY_SATURDAY = 'sat';
    const WEEK_DAY_SUNDAY   = 'sun';

    const TIME_RANGE_DAY    = 'day';
    const TIME_RANGE_WEEK   = 'week';
    const TIME_RANGE_MONTH  = 'month';
    const TIME_RANGE_YEAR   = 'year';


    /**
     * Returns unix timestamp
     *
     * @param string $dateTimeString A date/time string
     * @param string $timezoneName One of timezones
     * @return integer unix timestamp
     */
    public static function getUnixTimestamp($dateTimeString = 'now', $timezoneName = null) {

        $formattedDate = self::getDateTimeFormattedByString($dateTimeString, $timezoneName, self::FORMAT_UNIX);
        $timestamp = intval($formattedDate);

        return $timestamp;
    }

    /**
     * Returns string of formatted DateTime
     *
     * @param string $dateTimeString A date/time string
     * @param string $timezoneName One of timezones
     * @param string $format Format accepted by date() function
     * @return string formatted DateTime
     */
    public static function getDateTimeFormattedByString($dateTimeString, $timezoneName, $format = self::FORMAT_ALL) {

        try {
            $dt = self::getDateTimeByString($dateTimeString, $timezoneName);
            $formattedDate = $dt->format($format);
        } catch (Exception $e) {
            $formattedDate = '';
        }

        return $formattedDate;
    }

    /**
     * Returns a new DateTime instance
     *
     * @param string $dateTimeString A date/time string
     * @param string $timezoneName One of timezones
     * @return DateTime A new DateTime instance
     */
    protected static function getDateTimeByString($dateTimeString, $timezoneName = null) {

        if (empty($timezoneName)) {
            $dt = new DateTime($dateTimeString);
        } else {
            $tz = new DateTimeZone($timezoneName);
            $dt = new DateTime($dateTimeString, $tz);
        }

        return $dt;
    }

    /**
     * Converts unix timestamp and represents it as string
     *
     * @param integer $timestamp timestamp of date for converting
     * @param string $timezoneName One of timezones
     * @param string $format Format accepted by date() function
     * @return string formatted DateTime
     */
    public static function getDateTime($timestamp, $timezoneName, $format = self::FORMAT_ALL) {

        if (empty($timestamp)) {
            return '';
        }

        $dt = new DateTime("@$timestamp");
        $tz = new DateTimeZone($timezoneName);
        $dt->setTimezone($tz);

        return $dt->format($format);
    }

    /**
     *
     * @param string $timezoneName
     * @return string time offset from $timezone to GMT0. For example: '-0500', '+0700'
     */
    public static function getOffsetFromTimezoneToUTC($timezoneName) {

        $offset = self::getDateTimeFormattedByString('now', $timezoneName, 'O');
        return $offset;
    }

    /**
     * Returns unix timestamp of today (2010-03-03 00:00:00 in Unix format)
     *
     * @param string $timezoneName One of timezones
     * @return integer unix timestamp
     */
    public static function getTodayUnixTimestamp($timezoneName) {
        return self::getUnixTimestamp('today', $timezoneName);
    }

    /**
     * Returns unix timestamp date range (2010-03-03 00:00:00 - 2010-03-03 23:59:59 in Unix format)
     *
     * @param string $timezoneName One of timezones
     * @return array('from' => unix timestamp, 'to' => unix timestamp)
     */
    public static function getYesterdayUnixTimestampRange($timezoneName) {

        $result = array();
        $result['from'] = self::getUnixTimestamp('yesterday', $timezoneName);
        $result['to']   = $result['from'] + 24 * 3600 - 1;

        return $result;
    }

    /**
     * Returns formatted DateTime of last need week day. If today is need week day then function returns today.
     *
     * @param string $timezoneName One of timezones
     * @param string $weekDay Need week day name (one from TimeConverter::WEEK_DAY_*)
     * @param string $format Format accepted by date() function
     * @return string formatted DateTime
     */
    protected static function relativeLastWeekDay($timezoneName, $weekDay, $format, $dateStringBound = 'today') {

        $dateTimeString = self::getDateTimeFormattedByString($dateStringBound, $timezoneName, 'Y-m-d 00:00:00 O');

        $dt = new DateTime($dateTimeString);
        if ($dt->format('D') != ucfirst($weekDay)) {
            $dt = new DateTime("$dateTimeString last $weekDay");
        }

        return $dt->format($format);
    }

    /**
     * Returns unix timestamp of last need week day. If today is need week day then function returns today timestamp.
     *
     * @param string $timezoneName One of timezones
     * @param string $weekDay Need week day name (one from TimeConverter::WEEK_DAY_*)
     * @return integer unix timestamp
     */
    protected static function relativeLastWeekDayUnixTimestamp($timezoneName, $weekDay) {
        return self::relativeLastWeekDay($timezoneName, $weekDay, self::FORMAT_UNIX);
    }

    /**
     * Returns unix timestamp for last Sunday. If today is Sunday then function returns today timestamp.
     *
     * @param string $timezoneName One of timezones
     * @return integer unix timestamp
     */
    public static function getLastSundayUnixTimestamp($timezoneName) {
        return self::relativeLastWeekDayUnixTimestamp($timezoneName, self::WEEK_DAY_SUNDAY);
    }

     /**
     * Returns unix timestamp for last Monday. If today is Monday then function returns today timestamp.
     *
     * @param string $timezoneName One of timezones
     * @return integer unix timestamp
     */
    public static function getLastMondayUnixTimestamp($timezoneName) {
        return self::relativeLastWeekDayUnixTimestamp($timezoneName, self::WEEK_DAY_MONDAY);
    }

    /**
     * Returns unix timestamp date range for last week (Sunday - Saturday)
     * If today is Sunday then function returns range from today's start - today's end.
     *
     * @param string $timezoneName One of timezones
     * @return array('from' => unix timestamp, 'to' => unix timestamp)
     */
    public static function getLastWeekSunUnixTimestampRange($timezoneName) {

        $lastSundayTimestamp = self::relativeLastWeekDayUnixTimestamp($timezoneName, self::WEEK_DAY_SUNDAY);

        $result = array();
        $result['to']   = $lastSundayTimestamp - 1;
        $result['from'] = $result['to'] - 3600 * 24 * 7 + 1;

        return $result;
    }

    /**
     * Returns unix timestamp date range for last week (Monday - Sunday)
     * If today is Monday then function returns range from today's start - today's end.
     *
     * @param string $timezoneName One of timezones
     * @return array('from' => unix timestamp, 'to' => unix timestamp)
     */
    public static function getLastWeekMonUnixTimestampRange($timezoneName) {

        $lastMondayTimestamp = self::relativeLastWeekDayUnixTimestamp($timezoneName, self::WEEK_DAY_MONDAY);

        $result = array();
        $result['to']   = $lastMondayTimestamp - 1;
        $result['from'] = $result['to'] - 3600 * 24 * 7 + 1;

        return $result;
    }

    /**
     * Returns unix timestamp date range for last business week (Monday - Friday)
     * If today is Monday then function returns range from today's start - today's end.
     *
     * @param string $timezoneName One of timezones
     * @return array('from' => unix timestamp, 'to' => unix timestamp)
     */
    public static function getLastBusinessWeekUnixTimestampRange($timezoneName) {

        $lastSaturdayTimestamp = self::relativeLastWeekDayUnixTimestamp($timezoneName, self::WEEK_DAY_SATURDAY);

        $result = array();
        $result['to']   = $lastSaturdayTimestamp - 1;
        $result['from'] = $result['to'] - 3600 * 24 * 5 + 1;

        return $result;
    }

    /**
     * Returns unix timestamp of the first day of the current month.
     *
     * @param string $timezoneName One of timezones
     * @return integer unix timestamp
     */
    public static function getThisMonthUnixTimestamp($timezoneName) {

        $dateTimeString = self::getDateTimeFormattedByString('today', $timezoneName, 'Y-m-01 H:i:s');
        return self::getUnixTimestamp($dateTimeString, $timezoneName);
    }

    /**
     * Returns range of unix timestamps of the first day and the last day of the next month.
     *
     * @param string $timezoneName One of timezones
     * @return array('from' => unix timestamp, 'to' => unix timestamp)
     */
    public static function getNextMonthUnixTimestampRange($timezoneName) {

        $dateTimeString = self::getDateTimeFormattedByString('today', $timezoneName, 'Y-m-01 00:00:00 O');

        $result = array();
        $result['from'] = self::getUnixTimestamp("$dateTimeString + 1 month");
        $result['to']   = self::getUnixTimestamp("$dateTimeString + 2 month - 1 second");

        return $result;
    }

    /**
     * Returns range of unix timestamps of the first day and the last day of the last month.
     *
     * @param string $timezoneName One of timezones
     * @return array('from' => unix timestamp, 'to' => unix timestamp)
     */
    public static function getLastMonthUnixTimestampRange($timezoneName) {

        $dateTimeFromString = self::getDateTimeFormattedByString('today', $timezoneName, 'Y-m-01 00:00:00 O');
        $dateTimeToString = self::getDateTimeFormattedByString('today', $timezoneName, 'Y-m-01 23:59:59 O');

        $result = array();
        $result['from'] = self::getUnixTimestamp("$dateTimeFromString - 1 month");
        $result['to']   = self::getUnixTimestamp("$dateTimeToString - 1 day");

        return $result;
    }

    /**
     * Returns range of unix timestamps of the first day and the today of this month.
     *
     * @param string $timezoneName One of timezones
     * @return array('from' => unix timestamp, 'to' => unix timestamp)
     */
    public static function getThisMonthUnixTimestampRange($timezoneName) {

        $dateTimeString = self::getDateTimeFormattedByString('today', $timezoneName, 'Y-m-01 00:00:00 O');
        $nowTimeString  = self::getDateTimeFormattedByString('now', $timezoneName, 'Y-m-d H:i:s O');

        $result = array();
        $result['from'] = self::getUnixTimestamp("$dateTimeString");
        $result['to']   = self::getUnixTimestamp("$nowTimeString");

        return $result;
    }

    /**
     * Returns unix timestamp the first day of the next month.
     *
     * @param string $timezoneName One of timezones
     * @return integer unix timestamp
     */
    public static function getNextMonthUnixTimestamp($timezoneName) {

        $dateTimeString = self::getDateTimeFormattedByString('today + 1 month', $timezoneName, 'Y-m-01 00:00:00 O');
        return self::getUnixTimestamp($dateTimeString, $timezoneName);
    }

    /**
     * Returns unix timestamp of the day, which was $N days ago.
     *
     * @param string $timezoneName One of timezones
     * @param integer $N
     * @return integer unix timestamp
     */
    public static function getNDaysAgoUnixTimestamp($timezoneName, $N) {
        return self::getUnixTimestamp("$N days ago", $timezoneName);
    }

    /**
     * Returns range of the start and the end of day, which was $fromN days ago starting from $dateStringBound.
     * If $fromN = 0, then returns range of $dateStringBound day.
     * Example: (2010-Mar-07 00:00:00 - 2010-Mar-08 23:59:59 in Unix format)
     *
     * @param string $timezoneName One of timezones
     * @param integer $fromN
     * @param string $dateStringBound
     * @return array('from' => unix timestamp, 'to' => unix timestamp, 'dtFrom' => dateTimeFrom, 'dtTo' => dateTimeTo)
     */
    public static function getRangeUnixTimestampsNDaysAgo($timezoneName, $fromN, $dateStringBound = 'today') {

        $dateTimeString = self::getDateTimeFormattedByString($dateStringBound, $timezoneName, 'Y-m-d 00:00:00 O');
        return self::getRangeUnixTimestampsNPeriodsAgo($timezoneName, $fromN, self::TIME_RANGE_DAY, $dateTimeString);
    }

    /**
     * Returns range of the start and the end of week (Monday - Sunday), which was $fromN weeks ago starting from $dateStringBound.
     * If $fromN = 0, then returns range of $dateStringBound week.
     * Example: (2011-May-02 00:00:00<Monday> - 2011-May-08 23:59:59<Sunday> in Unix format)
     *
     * @param string $timezoneName One of timezones
     * @param integer $fromN
     * @param string $dateStringBound
     * @return array('from' => unix timestamp, 'to' => unix timestamp, 'dtFrom' => dateTimeFrom, 'dtTo' => dateTimeTo)
     */
    public static function getRangeUnixTimestampsNWeeksAgo($timezoneName, $fromN, $dateStringBound = 'today') {

        $dateTimeString = self::relativeLastWeekDay($timezoneName, self::WEEK_DAY_MONDAY, 'Y-m-d 00:00:00 O', $dateStringBound);
        return self::getRangeUnixTimestampsNPeriodsAgo($timezoneName, $fromN, self::TIME_RANGE_WEEK, $dateTimeString);
    }

    /**
     * Returns range of the start and the end of month, which was $fromN months ago starting from $dateStringBound.
     * If $fromN = 0, then returns range of $dateStringBound month.
     * Example: (2010-Mar-01 00:00:00 - 2010-Mar-31 23:59:59 in Unix format)
     *
     * @param string $timezoneName One of timezones
     * @param integer $fromN
     * @param string $dateStringBound
     * @return array('from' => unix timestamp, 'to' => unix timestamp, 'dtFrom' => dateTimeFrom, 'dtTo' => dateTimeTo)
     */
    public static function getRangeUnixTimestampsNMonthsAgo($timezoneName, $fromN, $dateStringBound = 'today') {

        $dateTimeString = self::getDateTimeFormattedByString($dateStringBound, $timezoneName, 'Y-m-01 00:00:00 O');
        return self::getRangeUnixTimestampsNPeriodsAgo($timezoneName, $fromN, self::TIME_RANGE_MONTH, $dateTimeString);
    }

    /**
     * Returns range of the start and the end of year, which was $fromN years ago starting from $dateStringBound.
     * If $fromN = 0, then returns range of $dateStringBound year.
     * Example: (2010-Jan-01 00:00:00 - 2010-Dec-31 23:59:59 in Unix format)
     *
     * @param string $timezoneName One of timezones
     * @param integer $fromN
     * @param string $dateStringBound
     * @return array('from' => unix timestamp, 'to' => unix timestamp, 'dtFrom' => dateTimeFrom, 'dtTo' => dateTimeTo)
     */
    public static function getRangeUnixTimestampsNYearsAgo($timezoneName, $fromN, $dateStringBound = 'today') {

        $dateTimeString = self::getDateTimeFormattedByString($dateStringBound, $timezoneName, 'Y-01-01 00:00:00 O');
        return self::getRangeUnixTimestampsNPeriodsAgo($timezoneName, $fromN, self::TIME_RANGE_YEAR, $dateTimeString);
    }

    /**
     * Returns range of the start and the end of period, which was $fromN periods ago starting from $dateStringBound.
     * If $fromN = 0, then returns range of $dateStringBound period.
     *
     * @param string $timezoneName One of timezones
     * @param integer $fromN
     * @param string $period one from TimeConverter::TIME_RANGE_*
     * @param string $dateTimeString A date/time string
     * @return array('from' => unix timestamp, 'to' => unix timestamp, 'dtFrom' => dateTimeFrom, 'dtTo' => dateTimeTo)
     */
    protected static function getRangeUnixTimestampsNPeriodsAgo($timezoneName, $fromN, $period, $dateTimeString) {

        $dateStringFrom = "$dateTimeString -$fromN $period";

        $toN = $fromN - 1;
        if ($toN > 0) {
            $dateStringTo = "$dateTimeString -$toN $period -1 second";
            $timezoneNameTo = null;
        } elseif ($toN < 0) {
            $toN *= -1;
            $dateStringTo = "$dateTimeString +$toN $period -1 second";
            $timezoneNameTo = null;
        } else {
            $dateStringTo = ($dateTimeString == 'today') ? 'now' : "$dateTimeString -1 second";
            $timezoneNameTo = $timezoneName;
        }

        $result = array();
        $result['dtFrom']   = self::getDateTimeByString($dateStringFrom);
        $result['dtTo']     = self::getDateTimeByString($dateStringTo, $timezoneNameTo);
        $result['from']     = $result['dtFrom']->format(self::FORMAT_UNIX);
        $result['to']       = $result['dtTo']->format(self::FORMAT_UNIX);

        return $result;
    }

    public static function convertSecondsToMinutes($seconds) {

        $minutes = intval($seconds / 60);
        $secondsRest = $seconds % 60;

        if (empty($seconds) || empty($minutes)) {
            return "$secondsRest s";
        }
        if (empty($secondsRest)) {
            return "$minutes m";
        }

        return "$minutes m $secondsRest s";
    }

    protected static function makeTwoDigitNumber($num) {
        $num = ($num / 10) >= 1 ? "$num" : "0$num";
        return $num;
    }

    public static function convertSecondsToTime($seconds) {

        $seconds = intval($seconds);

        $hours = intval($seconds / 3600);
        $seconds -= $hours * 3600;

        $minutes = intval($seconds / 60);
        $seconds -= $minutes * 60;

        $hours      = self::makeTwoDigitNumber($hours);
        $minutes    = self::makeTwoDigitNumber($minutes);
        $seconds    = self::makeTwoDigitNumber($seconds);

        return "$hours:$minutes:$seconds";
    }

    public static function convertSecondsToDayTime($seconds) {

        if (is_string($seconds)) {
            $seconds = doubleval($seconds);
        }

        $days = floor($seconds / 86400);

        $timeString = gmdate('H:i:s', $seconds);

        if ($days > 1) {        // 2 days+, in plural
            return "$days days $timeString";
        } elseif ($days > 0) {  // 1 day, in singular
            return "$days day $timeString";
        }
        return $timeString;
    }

//    public static function secondsToPeriods($seconds) {
//
//        $weeks  = intval($seconds / (7 * 24 * 3600));
//        $days   = intval($seconds / (24 * 3600));
//
//        return array(
//            'w' => $weeks,
//            'd' => $days,
//        );
//
////        $weeks = intval($seconds / (7 * 24 * 3600));
////        $seconds -= $weeks * (7 * 24 * 3600);
////
////        $days = intval($seconds / (24 * 3600));
////        $seconds -= $days * (24 * 3600);
////
////        $hours = intval($seconds / 3600);
////        $seconds -= $hours * 3600;
////
////        $minutes = intval($seconds / 60);
////        $seconds -= $minutes * 60;
////
////        return array(
////            'w' => $weeks,
////            'd' => $days,
////            'h' => $hours,
////            'm' => $minutes,
////            's' => $seconds
////        );
//    }

//    public static function getNDaysAgoDateUnixTimestamp($timezoneName, $N) {
//        return self::getUnixTimestamp("today $N days ago", $timezoneName);
//    }
//
//    public static function getNWeeksAgoDateUnixTimestamp($timezoneName, $N) {
//        return self::getUnixTimestamp("today $N weeks ago", $timezoneName);
//    }
//
//    public static function getNMonthsAgoDateUnixTimestamp($timezoneName, $N) {
//        return self::getUnixTimestamp("today $N months ago", $timezoneName);
//    }
//
//    public static function getNYearsAgoDateUnixTimestamp($timezoneName, $N) {
//        return self::getUnixTimestamp("today $N years ago", $timezoneName);
//    }
//
//    public static function getNDaysAgoIncludeTodayDateUnixTimestamp($timezoneName, $N) {
//        return self::getUnixTimestamp("today $N days ago +1 day", $timezoneName);
//    }
//
//    public static function getNWeeksAgoIncludeTodayDateUnixTimestamp($timezoneName, $N) {
//        return self::getUnixTimestamp("today $N weeks ago +1 day", $timezoneName);
//    }
//
//    public static function getNMonthsAgoIncludeTodayDateUnixTimestamp($timezoneName, $N) {
//        return self::getUnixTimestamp("today $N months ago +1 day", $timezoneName);
//    }
//
//    public static function getNYearsAgoIncludeTodayDateUnixTimestamp($timezoneName, $N) {
//        return self::getUnixTimestamp("today $N years ago +1 day", $timezoneName);
//    }
}