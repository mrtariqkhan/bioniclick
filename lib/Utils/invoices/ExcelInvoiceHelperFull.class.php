<?php

class ExcelInvoiceHelperFull extends ExcelInvoiceHelperTable {

    const SHEET_NAME_AGENCIES       = 'Agencies';
    const SHEET_NAME_ADVERTISER     = 'Advertisers';

    protected function fillFile() {

        $data = $this->getData();

        $infoTotal = self::getArrayValueArray($data, InvoicesGenerator::LEVEL_TOTAL);
        switch (self::getArrayValue($infoTotal, InvoicesGenerator::INFO_COMPANY_LEVEL)) {
            case CompanyTable::$LEVEL_BIONIC:
            case CompanyTable::$LEVEL_RESELLER:
                $infoSubCompanies = self::getArrayValueArray($data, InvoicesGenerator::LEVEL_SUB_COMPANY);
                foreach ($infoSubCompanies as $info) {
                    $this->addSheetSubCompany($info);
                }

                break;
            default:
        }

        $infoSubAdvertisers = self::getArrayValueArray($data, InvoicesGenerator::LEVEL_SUB_ADVERTISER);
        foreach ($infoSubAdvertisers as $info) {
            $this->addSheetAdvertiser($info);
        }
    }

    protected function addSheetSubCompany($subData) {

        $infoTotal = self::getArrayValueArray($subData, InvoicesGenerator::LEVEL_TOTAL);
        $name       = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_NAME);
        $level      = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_COMPANY_LEVEL);
        $levelName  = CompanyTable::getLevelName($level);

        $this->addNewActiveSheet(self::generateSheetTitle($levelName, $name));
        $this->doWriteSheetHeader($subData);

        $this->doWritePartTableHeader($subData);

        switch ($level) {
            case CompanyTable::$LEVEL_RESELLER:
                $infoSubSubCompanies = self::getArrayValueArray($subData, InvoicesGenerator::LEVEL_SUB_COMPANY);
                foreach ($infoSubSubCompanies as $info) {
                    $this->doWritePartTableBlock($info, self::ITEM_NAME_PREFIX_SUB_COMPANY, self::ITEM_NAME_POSTFIX_SUB_COMPANY);
                }
                break;
            case CompanyTable::$LEVEL_AGENCY:
                break;
        }

        $infoSubAdvertisers = self::getArrayValueArray($subData, InvoicesGenerator::LEVEL_SUB_ADVERTISER);
        foreach ($infoSubAdvertisers as $info) {
            $this->doWritePartTableBlock($info, self::ITEM_NAME_PREFIX_ADVERTISER, self::ITEM_NAME_POSTFIX_ADVERTISER);
        }


        $info = self::getArrayValueArray($subData, InvoicesGenerator::LEVEL_TOTAL);
        $priceTotal = self::getArrayValue($info, InvoicesGenerator::INFO_PRICE_TOTAL);

        $col = $this->firstColNumber;
        $row = $this->firstRowNumber;

        $this->doWritePartTableTotal($priceTotal, ExcelInvoiceHelperTable::ROW_GRAND_TOTAL, $this->styleGrandTotal);


        $this->doWriteSheetFooter($subData);
    }

    protected function addSheetAdvertiser($subData) {

        $infoTotal = self::getArrayValueArray($subData, InvoicesGenerator::LEVEL_TOTAL);
        $name     = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_NAME);
        $levelName= CompanyTable::$LEVEL_NAME_ADVERTISER;

        $this->addNewActiveSheet(self::generateSheetTitle($levelName, $name));
        $this->doWriteSheetHeader($subData);


        $this->doWritePartTableHeader($subData);

        $infoNumbersAmount = self::getArrayValueArray($subData, InvoicesGenerator::LEVEL_NUMBERS_AMOUNT);
        $this->doWritePartTableBlock($infoNumbersAmount, self::ITEM_NAME_PREFIX_ADVERTISER, self::ITEM_NAME_POSTFIX_ADVERTISER_NUMBER_AMOUNT);

        $infoUnassigned = self::getArrayValueArray($subData, InvoicesGenerator::LEVEL_UNASSIGNED);
        $this->doWritePartTableBlock($infoUnassigned, self::ITEM_NAME_PREFIX_ADVERTISER, self::ITEM_NAME_POSTFIX_ADVERTISER_UNASSIGNED);

        $infoCampaigns = self::getArrayValueArray($subData, InvoicesGenerator::LEVEL_CAMPAIGN);
        foreach ($infoCampaigns as $info) {
            $this->doWritePartTableBlock($info, self::ITEM_NAME_PREFIX_CAMPAIGN, self::ITEM_NAME_POSTFIX_CAMPAIGN);
        }


        $info = self::getArrayValueArray($subData, InvoicesGenerator::LEVEL_TOTAL);
        $priceTotal = self::getArrayValue($info, InvoicesGenerator::INFO_PRICE_TOTAL);

        $col = $this->firstColNumber;
        $row = $this->firstRowNumber;

        $this->doWritePartTableTotal($priceTotal, ExcelInvoiceHelperTable::ROW_GRAND_TOTAL, $this->styleGrandTotal);


        $this->doWriteSheetFooter($subData);
    }

    protected function doWriteSheetHeader($subData) {

        $infoTotal = self::getArrayValueArray($subData, InvoicesGenerator::LEVEL_TOTAL);
        $name           = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_NAME);
        $headerTimeFrom = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_TIME_UTC_STR_FROM);
        $headerTimeTo   = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_TIME_UTC_STR_TO);

        $headerName = $name;

        $col = self::FIRST_COL;
        $row = self::FIRST_ROW + 6;
        $this->doWrite($col++, $row, $headerName, $this->styleBold);
        $row++;


        $col = self::FIRST_COL + 2;
        $row = self::FIRST_ROW + 2;
        // $this->doWrite($col, $row, 'Invoice', $this->styleBold);
        $row++;

        $this->doWrite($col, $row, 'From:');
        $this->doWrite($col+1, $row, $headerTimeFrom);
        $row++;

        $this->doWrite($col, $row, 'To:');
        $this->doWrite($col+1, $row, $headerTimeTo);
        $row += 4;

        $this->firstRowNumber = $row;
    }

    protected static function generateSheetTitle($subCompanyLevelName, $subCompanyName) {

        $subCompanyLevelName[0] = strtoupper($subCompanyLevelName[0]);
        $title = "{$subCompanyLevelName} \"{$subCompanyName}\"";

        return $title;
    }
}
