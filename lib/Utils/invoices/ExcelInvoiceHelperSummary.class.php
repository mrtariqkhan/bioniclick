<?php

class ExcelInvoiceHelperSummary extends ExcelInvoiceHelperTable {

    const SHEET_NAME_TOTAL          = 'Total';
    const RAW_PAY_BIONIC_REMINDER   = 'Payment due upon receipt. Please make all checks payable to Bionic Click, LLC.';
    const RAW_PAY_BIONIC_ADDRESS    = 'Payment Mailing Address: P.O. Box 27438 Houston, TX 77227';
    const RAW_THANK                 = 'THANK YOU FOR YOUR BUSINESS!';
    const COLUMN_MERGE_AMOUNT       = 6;

    protected function fillFile() {

        $data = $this->getData();
        $this->addSheetSummary($data);
    }

    protected function addSheetSummary($subData) {

        $this->addNewActiveSheet(self::SHEET_NAME_TOTAL);
        $this->doWriteSheetHeader($subData);

        $ifRateNeeded = false;
        $ifTotalNeeded = false;


        $this->firstColNumber += self::FIRST_COL;
        $this->firstRowNumber += 5;

        $this->doWritePartTableHeader($subData, $ifRateNeeded, $ifTotalNeeded);

        $infoTotal = self::getArrayValueArray($subData, InvoicesGenerator::LEVEL_TOTAL);
        $companyLevel = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_COMPANY_LEVEL);
        switch ($companyLevel) {
            case CompanyTable::$LEVEL_BIONIC:
            case CompanyTable::$LEVEL_RESELLER:
            case CompanyTable::$LEVEL_AGENCY:

                $infoSubCompanies = self::getArrayValueArray($subData, InvoicesGenerator::LEVEL_SUB_COMPANY);
                foreach ($infoSubCompanies as $info) {
                    $this->doWritePartTableBlock($info, self::ITEM_NAME_PREFIX_SUB_COMPANY, self::ITEM_NAME_POSTFIX_SUB_COMPANY
                        , $ifRateNeeded, $ifTotalNeeded);
                }

                $infoSubAdvertisers = self::getArrayValueArray($subData, InvoicesGenerator::LEVEL_SUB_ADVERTISER);
                foreach ($infoSubAdvertisers as $info) {
                    $this->doWritePartTableBlock($info, self::ITEM_NAME_PREFIX_ADVERTISER, self::ITEM_NAME_POSTFIX_ADVERTISER
                        , $ifRateNeeded, $ifTotalNeeded);
                }

                break;
            case CompanyTable::$LEVEL_ADVERTISER:

                $infoNumbersAmount = self::getArrayValueArray($subData, InvoicesGenerator::LEVEL_NUMBERS_AMOUNT);
                $this->doWritePartTableBlock($infoNumbersAmount, self::ITEM_NAME_PREFIX_ADVERTISER, self::ITEM_NAME_POSTFIX_ADVERTISER_NUMBER_AMOUNT
                    , $ifRateNeeded, $ifTotalNeeded);

                $infoUnassigned = self::getArrayValueArray($subData, InvoicesGenerator::LEVEL_UNASSIGNED);
                $this->doWritePartTableBlock($infoUnassigned, self::ITEM_NAME_PREFIX_ADVERTISER, self::ITEM_NAME_POSTFIX_ADVERTISER_UNASSIGNED
                    , $ifRateNeeded, $ifTotalNeeded);

                $infoCampaigns = self::getArrayValueArray($subData, InvoicesGenerator::LEVEL_CAMPAIGN);
                foreach ($infoCampaigns as $info) {
                    $this->doWritePartTableBlock($info, self::ITEM_NAME_PREFIX_CAMPAIGN, self::ITEM_NAME_POSTFIX_CAMPAIGN
                        , $ifRateNeeded, $ifTotalNeeded);
                }

                break;
        }


        $info = self::getArrayValueArray($subData, InvoicesGenerator::LEVEL_TOTAL);
        $priceTotal = self::getArrayValue($info, InvoicesGenerator::INFO_PRICE_TOTAL);

        $col = $this->firstColNumber;
        $row = $this->firstRowNumber;

        //$this->doWritePartTableTotal($priceTotal, ExcelInvoiceHelperTable::ROW_GRAND_TOTAL_DUE, $this->styleGrandTotal);


        $this->doWriteSheetFooter($subData);
        $this->postFillSheet();
    }

    protected function doWriteSheetHeader($subData) {

        $infoTotal = self::getArrayValueArray($subData, InvoicesGenerator::LEVEL_TOTAL);
        $name               = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_NAME);
        $headerTimeFrom     = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_TIME_UTC_STR_FROM);
        $headerTimeTo       = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_TIME_UTC_STR_TO);
        $invoiceTypeMoney   = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_INVOICE_TYPE_MONEY);
        $invoiceTypeDetails = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_INVOICE_TYPE_DETAILS);


        $col = self::FIRST_COL;
        $row = self::FIRST_ROW;

        if ($invoiceTypeDetails == InvoicesGeneratorBase::INVOICE_TYPE_NAME_DETAILS_SUMMARY) {
            $objDrawing = new PHPExcel_Worksheet_Drawing();
            $objDrawing
                ->setPath($this->pathToLogoDir)
                ->setCoordinates($this->convertCoordFromNumericToLiteral($col, $row))
                ->setHeight(80)
//                ->setOffsetX(10)
//                ->setRotation(20)
            ;
//            $objDrawing->getShadow()
//                ->setVisible(true)
//                ->setDirection(45)
//            ;
            $objDrawing->setWorksheet($this->phpExcel->getActiveSheet());
            $row += 6;
        }

        $this->doWrite($col, $row++, 'TO:', $this->styleCompanyToTitle);
        $this->doWrite($col, $row++, $name, $this->styleCompanyTo);


        $col = self::FIRST_COL + 2;
        $row = self::FIRST_ROW + 2;
        // $this->doWrite($col, $row, 'Invoice', $this->styleBold);
        $row++;

        $this->doWrite($col, $row, 'From:');
        $this->doWrite($col+1, $row, $headerTimeFrom);
        $row++;

        $this->doWrite($col, $row, 'To:');
        $this->doWrite($col+1, $row, $headerTimeTo);
        $row++;

        $this->firstRowNumber = $row;
    }

    protected function doWriteSheetFooter($subData) {

        $infoTotal = self::getArrayValueArray($subData, InvoicesGenerator::LEVEL_TOTAL);
        $invoiceTypeMoney   = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_INVOICE_TYPE_MONEY);
        $invoiceTypeDetails = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_INVOICE_TYPE_DETAILS);

        $col = self::FIRST_COL;
        $row = $this->firstRowNumber + 3;

        $colMergeTo = $col + self::COLUMN_MERGE_AMOUNT;
        if ($invoiceTypeMoney == InvoicesGeneratorBase::INVOICE_TYPE_NAME_HAVE_TO_PAY) {
            $isDirectChild = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_IS_COMPANY_BIONIC_DIRECT_CHILD);

            if ($isDirectChild) {
                $this->doMergeColumns($col, $colMergeTo, ++$row);
                $this->doWrite($col, $row, self::RAW_PAY_BIONIC_REMINDER, $this->stylePostScriptum);
            }

            $this->doMergeColumns($col, $colMergeTo, ++$row);
            $this->doWrite($col, $row, self::RAW_THANK, $this->styleThank);

            $this->doMergeColumns($col, $colMergeTo, ++$row);
            $this->doWrite($col, $row, '', $this->stylePostScriptum);

            if ($isDirectChild) {
                $this->doMergeColumns($col, $colMergeTo, ++$row);
                $this->doWrite($col, $row, self::RAW_PAY_BIONIC_ADDRESS, $this->stylePostScriptum);
            }
        } else {
            $this->doMergeColumns($col, $colMergeTo, ++$row);
            $this->doWrite($col, $row, self::RAW_THANK, $this->styleThank);
        }

        $this->firstRowNumber = $row;

        parent::doWriteSheetFooter($subData);
    }
}
