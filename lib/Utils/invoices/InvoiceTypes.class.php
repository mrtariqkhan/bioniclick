<?php

/**
 * Description of InvoiceType
 *
 * @author fairdev
 */
class InvoiceTypes {

    private $types;

    /**
     *
     * @param array $types key = typeCode, value = typeName
     */
    public function  __construct(array $types) {

        $this->setTypes($types);
    }


    public function toStringFormatted($format, $glue) {

        $types = $this->getTypes();
        $typesNew = array();
        foreach ($types as $invoiceType) {
            $typesNew[] = sprintf($format, $invoiceType);
        }
        $invoiceTypesString = implode($glue, $typesNew);

        return $invoiceTypesString;
    }

    public function getTypeName($typeCode) {

        $types = $this->getTypes();
        if (array_key_exists($typeCode, $types)) {
            return $types[$typeCode];
        }

        return '';
    }

    public function getTypesByName($typeNameNeed) {

        if (empty($typeNameNeed)) {
            return $this->getTypes();
        }

        $typeNameNeedUp = strtoupper($typeNameNeed);

        $types = $this->getTypes();
        foreach ($types as $typeCode => $typeName) {
            if ($typeNameNeedUp === strtoupper($typeName)) {
                return array(
                    $typeCode => $typeName
                );
            }
        }

        return array();
    }


    public function getTypes() {
        return $this->types;
    }

    public function setTypes($types) {
        $this->types = $types;
    }
}

