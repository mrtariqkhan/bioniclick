<?php

require_once(dirname(__FILE__) . '/../invoices/InvoicesGeneratorBase.class.php');

class InvoicesGenerator extends InvoicesGeneratorBase {

    const LEVEL_SUB_COMPANY     = 'sub company';
    const LEVEL_SUB_ADVERTISER  = 'sub advertiser';
    const LEVEL_CAMPAIGN        = 'campaign';
    const LEVEL_UNASSIGNED      = 'unassigned';
    const LEVEL_NUMBERS_AMOUNT  = 'numbers amount block';
    const LEVEL_TOTAL           = 'total';

    const INFO_EMAIL_TO                         = 'email';
    const INFO_NAME                             = 'object name';
    const INFO_COMPANY_LEVEL                    = 'company level';
    const INFO_IS_COMPANY_BIONIC_DIRECT_CHILD   = 'is direct bionic child';
    const INFO_TIME_UTC_STR_FROM                = 'Time From';          // in GMT0
    const INFO_TIME_UTC_STR_TO                  = 'Time To';            // in GMT0
    const INFO_INVOICE_TYPE_MONEY               = 'Invoice Type Money';
    const INFO_INVOICE_TYPE_DETAILS             = 'Invoice Type Details';

    const INFO_AMOUNT_NUMBERS_LOCAL             = 'Local numbers';      //Local numbers amount
    const INFO_AMOUNT_NUMBERS_TOLL_FREE         = 'Toll-free numbers';  //Toll Free numbers amount
    const INFO_AMOUNT_MINUTES_LOCAL             = 'Local minutes';      //Local minutes amount
    const INFO_AMOUNT_MINUTES_TOLL_FREE         = 'Toll-free minutes';  //Toll Free minutes amount

    const INFO_RATE_PER_MONTH_NUMBER_LOCAL      = 'Local number per month rate';
    const INFO_RATE_PER_MONTH_NUMBER_TOLL_FREE  = 'Toll Free number per month rate';
    const INFO_RATE_PER_MINUTE_LOCAL            = 'Local minute rate';
    const INFO_RATE_PER_MINUTE_TOLL_FREE        = 'Toll Free minute rate';

    const INFO_PRICE_NUMBERS_LOCAL              = 'Local numbers price';
    const INFO_PRICE_NUMBERS_TOLL_FREE          = 'Toll Free numbers price';
    const INFO_PRICE_MINUTES_LOCAL              = 'Local minutes price';
    const INFO_PRICE_MINUTES_TOLL_FREE          = 'Toll Free minutes price';

    const INFO_PRICE_TOTAL                      = 'total price';

    const INFO_NEED_NUMBERS_AMOUNT              = 'numbers amount is need';
    const INFO_NEED_CALL_TIME_AMOUNT            = 'call time amount is need';


    const DOUBLE_DECIMALS   = 3;


    protected $listToRound;

    /**
     *
     * @param type $company company or Advertiser
     * @param integer $invoiceTypeMoney one of InvoicesGeneratorBase::INVOICE_TYPE_HAVE_TO_
     * @param integer $invoiceTypeDetails one of InvoicesGeneratorBase::INVOICE_TYPE_DETAILS_
     * @param array $range array('from' => timestampFrom, 'to' => timestampTo).
     */
    public function __construct($company, $invoiceTypeMoney, $invoiceTypeDetails, $range) {

        parent::__construct($company, $invoiceTypeMoney, $invoiceTypeDetails, $range);

        $this->configureListToRound();
    }

    protected function configureNode(
        $obj,
        $infoObjectTotal, 
        $infoSubCompanies   = array(),
        $infoSubAdvertisers = array(),
        $infoCampaigns      = array(),
        $infoUnassigned     = array(),
        $infoNumbersAmount  = array()
    ) {

        $infoObjectTotal[self::INFO_PRICE_TOTAL] =
              $infoObjectTotal[self::INFO_PRICE_NUMBERS_LOCAL]
            + $infoObjectTotal[self::INFO_PRICE_NUMBERS_TOLL_FREE]
            + $infoObjectTotal[self::INFO_PRICE_MINUTES_LOCAL]
            + $infoObjectTotal[self::INFO_PRICE_MINUTES_TOLL_FREE]
        ;
        $infoObjectTotalRounded = $this->getRounded($infoObjectTotal);

        $infoNode = array(
            self::INFO_NAME                => $obj->getName(),
            self::INFO_COMPANY_LEVEL       => $obj->getLevel(),
            self::INFO_EMAIL_TO            => $obj->getEmailAddress(),
            self::INFO_TIME_UTC_STR_FROM   => $this->timeFromString,
            self::INFO_TIME_UTC_STR_TO     => $this->timeToString,
            self::INFO_INVOICE_TYPE_MONEY  => $this->getTypeMoney(),
            self::INFO_INVOICE_TYPE_DETAILS=> $this->getTypeDetails(),
            self::INFO_IS_COMPANY_BIONIC_DIRECT_CHILD => $obj->isDirectBionicChild()
        );
        $infoTotal = array_merge($infoObjectTotalRounded, $infoNode);


        $info = array(
            self::LEVEL_TOTAL              => $infoTotal,
            self::LEVEL_SUB_COMPANY        => $infoSubCompanies,
            self::LEVEL_SUB_ADVERTISER     => $infoSubAdvertisers,
            self::LEVEL_CAMPAIGN           => $infoCampaigns,
            self::LEVEL_UNASSIGNED         => $infoUnassigned,
            self::LEVEL_NUMBERS_AMOUNT     => $infoNumbersAmount,
        );
        return $info;
    }

    protected function configureTableBlockObj(
        &$parentInfoTotal,
        $obj,
        $forciblePackage,
        $needNumbersAmount = true,
        $needCallTimeAmount = true,
        $methodNameCalcLocalNumbers = 'calcLocalNumbers',
        $methodNameCalcTollNumbers  = 'calcTollNumbers',
        $methodNameCalcTollCallTime = 'calcTollCallTime',
        $methodNameCalcLocalCallTime= 'calcLocalCallTime'
    ) {

        if (!$needNumbersAmount) {
            $methodNameCalcLocalNumbers = null;
            $methodNameCalcTollNumbers  = null;
        }
        if (!$needCallTimeAmount) {
            $methodNameCalcTollCallTime = null;
            $methodNameCalcLocalCallTime= null;
        }

        $numbersLocal   = empty($methodNameCalcLocalNumbers)  ? 0 : $obj->$methodNameCalcLocalNumbers($this->timeFrom, $this->timeTo);
        $numbersToll    = empty($methodNameCalcTollNumbers)   ? 0 : $obj->$methodNameCalcTollNumbers($this->timeFrom, $this->timeTo);
        $timeToll       = empty($methodNameCalcTollCallTime)  ? 0 : $obj->$methodNameCalcTollCallTime($this->timeFrom, $this->timeTo);
        $timeLocal      = empty($methodNameCalcLocalCallTime) ? 0 : $obj->$methodNameCalcLocalCallTime($this->timeFrom, $this->timeTo);

        return $this->configureTableBlock(
            $parentInfoTotal,
            $obj,
            $numbersLocal,
            $numbersToll,
            $timeLocal,
            $timeToll,
            $forciblePackage,
            $needNumbersAmount,
            $needCallTimeAmount
        );
    }

    protected function configureTableBlockAdvertiserUnassigned(&$parentInfoTotal, $advertiser, $forciblePackage) {

        return $this->configureTableBlockObj(
            $parentInfoTotal,
            $advertiser,
            $forciblePackage,
            false,
            true,
            null,
            null,
            'calcTollCallTimeUnassigned',
            'calcLocalCallTimeUnassigned'
        );
    }

    protected function configureTableBlockCampaign(&$parentInfoTotal, $campaign, $forciblePackage) {

        return $this->configureTableBlockObj(
            $parentInfoTotal,
            $campaign,
            $forciblePackage,
            false,
            true
        );
    }

    protected function configureTableBlockAdvertiserNumbersAmount(&$parentInfoTotal, $advertiser, $forciblePackage) {

        return $this->configureTableBlockObj(
            $parentInfoTotal,
            $advertiser,
            $forciblePackage,
            true,
            false
        );
    }


    protected function configureTableBlock(
        &$parentInfoTotal,
        $obj,
        $amountNumbersLocal,
        $amountNumbersToll,
        $amountMinutesLocal,
        $amountMinutesToll,
        $forciblePackage,
        $needNumbersAmount,
        $needCallTimeAmount
    ) {

        $package = empty($forciblePackage) ? $obj->getPackage() : $forciblePackage;

        $priceTotal = 0;
        $infoObjectTotal = array(
            self::INFO_PRICE_TOTAL                     => $priceTotal,
        );

        if ($needCallTimeAmount) {
            $ratePerMinuteNumberLocal   = $package->getLocalNumberPerMinute();
            $ratePerMinuteNumberToll    = $package->getTollFreeNumberPerMinute();

            $priceMinutesLocal = $amountMinutesLocal * $ratePerMinuteNumberLocal;
            $priceMinutesToll  = $amountMinutesToll  * $ratePerMinuteNumberToll;

            $priceTotal += $priceMinutesLocal + $priceMinutesToll;

            $infoObjectTotal = array_merge($infoObjectTotal, array(
                self::INFO_AMOUNT_MINUTES_LOCAL            => intval($amountMinutesLocal),
                self::INFO_AMOUNT_MINUTES_TOLL_FREE        => intval($amountMinutesToll),

                self::INFO_RATE_PER_MINUTE_LOCAL           => $ratePerMinuteNumberLocal,
                self::INFO_RATE_PER_MINUTE_TOLL_FREE       => $ratePerMinuteNumberToll,

                self::INFO_PRICE_MINUTES_LOCAL             => $priceMinutesLocal,
                self::INFO_PRICE_MINUTES_TOLL_FREE         => $priceMinutesToll,

                self::INFO_PRICE_TOTAL                     => $priceTotal,
            ));
        }

        if ($needNumbersAmount) {
            $ratePerMonthNumberLocal    = $package->getLocalNumberMonthlyFee();
            $ratePerMonthNumberToll     = $package->getTollFreeNumberMonthlyFee();

            $priceNumbersLocal = $amountNumbersLocal * $ratePerMonthNumberLocal;
            $priceNumbersToll  = $amountNumbersToll  * $ratePerMonthNumberToll;

            $priceTotal += $priceNumbersLocal + $priceNumbersToll;

            $infoObjectTotal = array_merge($infoObjectTotal, array(
                self::INFO_AMOUNT_NUMBERS_LOCAL            => intval($amountNumbersLocal),
                self::INFO_AMOUNT_NUMBERS_TOLL_FREE        => intval($amountNumbersToll),

                self::INFO_RATE_PER_MONTH_NUMBER_LOCAL     => $ratePerMonthNumberLocal,
                self::INFO_RATE_PER_MONTH_NUMBER_TOLL_FREE => $ratePerMonthNumberToll,

                self::INFO_PRICE_NUMBERS_LOCAL             => $priceNumbersLocal,
                self::INFO_PRICE_NUMBERS_TOLL_FREE         => $priceNumbersToll,

                self::INFO_PRICE_TOTAL                     => $priceTotal,
            ));
        }

        $parentInfoTotal = $this->updateParentTotalNode($parentInfoTotal, $infoObjectTotal);
        $infoObjectTotalRounded = $this->getRounded($infoObjectTotal);

        $infoNode = array(
            self::INFO_NAME                 => $obj->getName(),
//            self::INFO_COMPANY_LEVEL       => $obj->getLevel(),
//            self::INFO_EMAIL_TO            => $obj->getEmailAddress(),
            self::INFO_TIME_UTC_STR_FROM    => $this->timeFromString,
            self::INFO_TIME_UTC_STR_TO      => $this->timeToString,
            self::INFO_INVOICE_TYPE_MONEY   => $this->getTypeMoney(),
            self::INFO_INVOICE_TYPE_DETAILS => $this->getTypeDetails(),
            self::INFO_NEED_NUMBERS_AMOUNT  => $needNumbersAmount,
            self::INFO_NEED_CALL_TIME_AMOUNT=> $needCallTimeAmount,
        );
        $infoTotal = array_merge($infoObjectTotalRounded, $infoNode);

        $info = array(
            self::LEVEL_TOTAL              => $infoTotal,
        );

        return $info;
    }

    protected function getRounded($arr) {

        //NOTE:: ExcelInvoiceHelper roundes themself
        return $arr;

//        $arrRounded = $arr;
//        foreach ($this->listToRound as $itemName) {
//            if (array_key_exists($itemName, $arr)) {
//                $arrRounded[$itemName] = self::getDouble($arr[$itemName]);
//            }
//        }
//
//        return $arrRounded;
    }

    public static function getDouble($var) {
        $res = round(doubleval($var), self::DOUBLE_DECIMALS);
        $res = number_format($res, self::DOUBLE_DECIMALS);
        return $res;
//        return round($var, self::DOUBLE_DECIMALS, PHP_ROUND_HALF_UP);
//        return number_format($var, self::DOUBLE_DECIMALS);
    }

    protected function configureNodeTotalList() {

        $this->nodeTotalList = array(
            self::INFO_AMOUNT_NUMBERS_LOCAL,
            self::INFO_AMOUNT_NUMBERS_TOLL_FREE,
            self::INFO_AMOUNT_MINUTES_LOCAL,
            self::INFO_AMOUNT_MINUTES_TOLL_FREE,

            self::INFO_PRICE_NUMBERS_LOCAL,
            self::INFO_PRICE_NUMBERS_TOLL_FREE,
            self::INFO_PRICE_MINUTES_LOCAL,
            self::INFO_PRICE_MINUTES_TOLL_FREE,
        );
    }

    protected function configureListToRound() {

        $this->listToRound = array(
            self::INFO_AMOUNT_NUMBERS_LOCAL,
            self::INFO_AMOUNT_NUMBERS_TOLL_FREE,
            self::INFO_AMOUNT_MINUTES_LOCAL,
            self::INFO_AMOUNT_MINUTES_TOLL_FREE,

            self::INFO_PRICE_NUMBERS_LOCAL,
            self::INFO_PRICE_NUMBERS_TOLL_FREE,
            self::INFO_PRICE_MINUTES_LOCAL,
            self::INFO_PRICE_MINUTES_TOLL_FREE,

            self::INFO_RATE_PER_MONTH_NUMBER_LOCAL,
            self::INFO_RATE_PER_MONTH_NUMBER_TOLL_FREE,
            self::INFO_RATE_PER_MINUTE_LOCAL,
            self::INFO_RATE_PER_MINUTE_TOLL_FREE,

            self::INFO_PRICE_TOTAL,
        );
    }
}
