<?php

/**
Format of statistics for Reseller:

$info = array(
    InvoicesGenerator::LEVEL_TOTAL => array(
        InvoicesGenerator::INFO_NAME                => 'resellerName1',
        InvoicesGenerator::INFO_COMPANY_LEVEL       => lev,
        InvoicesGenerator::INFO_EMAIL_TO            => e,
        InvoicesGenerator::INFO_TIME_UTC_STR_FROM   => tf,
        InvoicesGenerator::INFO_TIME_UTC_STR_TO     => tt,
        InvoicesGenerator::INFO_INVOICE_TYPE_MONEY  => it,
        InvoicesGenerator::INFO_INVOICE_TYPE_DETAILS=> jt,
 
        InvoicesGenerator::INFO_AMOUNT_NUMBERS_LOCAL            => x,
        InvoicesGenerator::INFO_AMOUNT_NUMBERS_TOLL_FREE        => y,
        InvoicesGenerator::INFO_AMOUNT_MINUTES_LOCAL            => z,
        InvoicesGenerator::INFO_AMOUNT_MINUTES_TOLL_FREE        => zz,

        InvoicesGenerator::INFO_PRICE_NUMBERS_LOCAL             => xx,
        InvoicesGenerator::INFO_PRICE_NUMBERS_TOLL_FREE         => yy,
        InvoicesGenerator::INFO_PRICE_MINUTES_LOCAL             => zz,
        InvoicesGenerator::INFO_PRICE_MINUTES_TOLL_FREE         => zzzz,

        InvoicesGenerator::INFO_PRICE_TOTAL                     => t,
    ),
    InvoicesGenerator::LEVEL_SUB_COMPANY => array(
        0 => array(
            InvoicesGenerator::LEVEL_TOTAL => array(
                InvoicesGenerator::INFO_NAME                => 'agName1',
                InvoicesGenerator::INFO_COMPANY_LEVEL       => lev,
                InvoicesGenerator::INFO_EMAIL_TO            => e,
                InvoicesGenerator::INFO_TIME_UTC_STR_FROM   => tf,
                InvoicesGenerator::INFO_TIME_UTC_STR_TO     => tt,
                InvoicesGenerator::INFO_INVOICE_TYPE_MONEY  => it,
                InvoicesGenerator::INFO_INVOICE_TYPE_DETAILS=> jt,
 
                InvoicesGenerator::INFO_AMOUNT_NUMBERS_LOCAL            => x,
                InvoicesGenerator::INFO_AMOUNT_NUMBERS_TOLL_FREE        => y,
                InvoicesGenerator::INFO_AMOUNT_MINUTES_LOCAL            => z,
                InvoicesGenerator::INFO_AMOUNT_MINUTES_TOLL_FREE        => zz,

                InvoicesGenerator::INFO_PRICE_NUMBERS_LOCAL             => xx,
                InvoicesGenerator::INFO_PRICE_NUMBERS_TOLL_FREE         => yy,
                InvoicesGenerator::INFO_PRICE_MINUTES_LOCAL             => zz,
                InvoicesGenerator::INFO_PRICE_MINUTES_TOLL_FREE         => zzzz,

                InvoicesGenerator::INFO_PRICE_TOTAL                     => t,
            )

            InvoicesGenerator::LEVEL_SUB_ADVERTISER => array(
                0 => array(
                    InvoicesGenerator::LEVEL_TOTAL => array(
                        InvoicesGenerator::INFO_NAME                            => 'advName1',
                        InvoicesGenerator::INFO_TIME_UTC_STR_FROM               => tf,
                        InvoicesGenerator::INFO_TIME_UTC_STR_TO                 => tt,
                        InvoicesGenerator::INFO_INVOICE_TYPE_MONEY              => it,
                        InvoicesGenerator::INFO_INVOICE_TYPE_DETAILS            => jt,
 
                        InvoicesGenerator::INFO_AMOUNT_NUMBERS_LOCAL            => x,
                        InvoicesGenerator::INFO_AMOUNT_NUMBERS_TOLL_FREE        => y,
                        InvoicesGenerator::INFO_AMOUNT_MINUTES_LOCAL            => z,
                        InvoicesGenerator::INFO_AMOUNT_MINUTES_TOLL_FREE        => zz,

                        InvoicesGenerator::INFO_PRICE_NUMBERS_LOCAL             => xx,
                        InvoicesGenerator::INFO_PRICE_NUMBERS_TOLL_FREE         => yy,
                        InvoicesGenerator::INFO_PRICE_MINUTES_LOCAL             => zz,
                        InvoicesGenerator::INFO_PRICE_MINUTES_TOLL_FREE         => zzzz,

                        InvoicesGenerator::INFO_PRICE_TOTAL                     => t,
                    )
                ),
                ...
                N => array(...)
            )
        ),
        ...
        N => array(...)
    ),
    InvoicesGenerator::LEVEL_SUB_ADVERTISER => array(
        0 => array(
            InvoicesGenerator::LEVEL_TOTAL => array(
                InvoicesGenerator::INFO_NAME         => 'advName1',
 
                InvoicesGenerator::INFO_AMOUNT_NUMBERS_LOCAL            => x,
                InvoicesGenerator::INFO_AMOUNT_NUMBERS_TOLL_FREE        => y,
                InvoicesGenerator::INFO_AMOUNT_MINUTES_LOCAL            => z,
                InvoicesGenerator::INFO_AMOUNT_MINUTES_TOLL_FREE        => zz,

                InvoicesGenerator::INFO_PRICE_NUMBERS_LOCAL             => xx,
                InvoicesGenerator::INFO_PRICE_NUMBERS_TOLL_FREE         => yy,
                InvoicesGenerator::INFO_PRICE_MINUTES_LOCAL             => zz,
                InvoicesGenerator::INFO_PRICE_MINUTES_TOLL_FREE         => zzzz,

                InvoicesGenerator::INFO_PRICE_TOTAL                     => t,
            )
 
            InvoicesGenerator::LEVEL_CAMPAIGN => array(
                0 => array(
                    InvoicesGenerator::LEVEL_TOTAL => array(
                        InvoicesGenerator::INFO_NAME                            => 'camName1',
                        InvoicesGenerator::INFO_TIME_UTC_STR_FROM               => tf,
                        InvoicesGenerator::INFO_TIME_UTC_STR_TO                 => tt,
                        InvoicesGenerator::INFO_INVOICE_TYPE_MONEY              => it,
                        InvoicesGenerator::INFO_INVOICE_TYPE_DETAILS            => jt,

                        InvoicesGenerator::INFO_AMOUNT_NUMBERS_LOCAL            => x,
                        InvoicesGenerator::INFO_AMOUNT_NUMBERS_TOLL_FREE        => y,
                        InvoicesGenerator::INFO_AMOUNT_MINUTES_LOCAL            => z,
                        InvoicesGenerator::INFO_AMOUNT_MINUTES_TOLL_FREE        => zz,

                        InvoicesGenerator::INFO_RATE_PER_MONTH_NUMBER_LOCAL     => a,
                        InvoicesGenerator::INFO_RATE_PER_MONTH_NUMBER_TOLL_FREE => b,
                        InvoicesGenerator::INFO_RATE_PER_MINUTE_LOCAL           => c,
                        InvoicesGenerator::INFO_RATE_PER_MINUTE_TOLL_FREE       => d,

                        InvoicesGenerator::INFO_PRICE_NUMBERS_LOCAL             => xx,
                        InvoicesGenerator::INFO_PRICE_NUMBERS_TOLL_FREE         => yy,
                        InvoicesGenerator::INFO_PRICE_MINUTES_LOCAL             => zz,
                        InvoicesGenerator::INFO_PRICE_MINUTES_TOLL_FREE         => zzzz,

                        InvoicesGenerator::INFO_PRICE_TOTAL                     => t,
                    )
                ),
                ...
                N => array(...)
            ),
            InvoicesGenerator::LEVEL_UNASSIGNED => array(
                InvoicesGenerator::LEVEL_TOTAL => array(
                    InvoicesGenerator::INFO_NAME                            => 'AdvName1',
                    InvoicesGenerator::INFO_TIME_UTC_STR_FROM               => tf,
                    InvoicesGenerator::INFO_TIME_UTC_STR_TO                 => tt,
                    InvoicesGenerator::INFO_INVOICE_TYPE_MONEY              => it,
                    InvoicesGenerator::INFO_INVOICE_TYPE_DETAILS            => jt,

                    InvoicesGenerator::INFO_AMOUNT_NUMBERS_LOCAL            => x,
                    InvoicesGenerator::INFO_AMOUNT_NUMBERS_TOLL_FREE        => y,
                    InvoicesGenerator::INFO_AMOUNT_MINUTES_LOCAL            => z,
                    InvoicesGenerator::INFO_AMOUNT_MINUTES_TOLL_FREE        => zz,

                    InvoicesGenerator::INFO_RATE_PER_MONTH_NUMBER_LOCAL     => a,
                    InvoicesGenerator::INFO_RATE_PER_MONTH_NUMBER_TOLL_FREE => b,
                    InvoicesGenerator::INFO_RATE_PER_MINUTE_LOCAL           => c,
                    InvoicesGenerator::INFO_RATE_PER_MINUTE_TOLL_FREE       => d,

                    InvoicesGenerator::INFO_PRICE_NUMBERS_LOCAL             => xx,
                    InvoicesGenerator::INFO_PRICE_NUMBERS_TOLL_FREE         => yy,
                    InvoicesGenerator::INFO_PRICE_MINUTES_LOCAL             => zz,
                    InvoicesGenerator::INFO_PRICE_MINUTES_TOLL_FREE         => zzzz,

                    InvoicesGenerator::INFO_PRICE_TOTAL                     => t,
                )
            )
        ),
        ...
        N => array(...)
    )
);

 */

class InvoicesGeneratorFull extends InvoicesGenerator {

    protected function configureAdvertiserSubCampaignInfo(&$parentInfoTotal, $campaign, $forciblePackage) {

        return $this->configureTableBlockCampaign(
            $parentInfoTotal, 
            $campaign, 
            $forciblePackage
        );
    }

    protected function configureAdvertiserUnassignedInfo(&$parentInfoTotal, $advertiser, $forciblePackage) {

        return $this->configureTableBlockAdvertiserUnassigned(
            $parentInfoTotal, 
            $advertiser, 
            $forciblePackage
        );
    }

    protected function configureAdvertiserNumbersAmountInfo(&$parentInfoTotal, $advertiser, $forciblePackage) {

        return $this->configureTableBlockAdvertiserNumbersAmount(
            $parentInfoTotal, 
            $advertiser, 
            $forciblePackage
        );
    }

    protected function configureCompanySubCompanyInfo(&$parentInfoTotal, $subCompany, $forciblePackage) {

        switch ($subCompany->getLevel()) {
            case CompanyTable::$LEVEL_RESELLER:
            case CompanyTable::$LEVEL_AGENCY:
                $result = $this->generateDataSubCompany($parentInfoTotal, $subCompany, $forciblePackage);
                break;
            default:
                $result = array();
        }

        return $result;
    }

    protected function generateDataSubCompany(&$parentInfoTotal, Company $subCompany, $forciblePackage = null) {

        $infoObjectTotal = $this->initTotalNode();

        if ($this->invoiceTypeMoney == self::INVOICE_TYPE_HAVE_TO_GET) {
            $forciblePackage = $subCompany->getPackage();
        }


        $infoSubCompanies = array();
        $subSubCompanies = $subCompany->findSubCompaniesFirst(false);
        foreach ($subSubCompanies as $subSubCompany) {
            $infoSubCompanies[] = $this->configureTableBlockObj($infoObjectTotal, $subSubCompany, $forciblePackage);
        }

        $infoSubAdvertisers = array();
        $subAdvertisers = $subCompany->findSubAdvertisersFirst(false);
        foreach ($subAdvertisers as $subAdvertiser) {
            $infoSubAdvertisers[] = $this->configureTableBlockObj($infoObjectTotal, $subAdvertiser, $forciblePackage);
        }


        $info = $this->configureNode($subCompany, $infoObjectTotal, $infoSubCompanies, $infoSubAdvertisers);
        $parentInfoTotal = $this->updateParentTotalNode($parentInfoTotal, $infoObjectTotal);
        return $info;
    }

    protected function configureCompanySubAdvertiserInfo(&$parentInfoTotal, $advertiser, $forciblePackage) {

        $infoObjectTotal = $this->initTotalNode();

        if ($this->invoiceTypeMoney == self::INVOICE_TYPE_HAVE_TO_GET) {
            $forciblePackage = $advertiser->getPackage();
        }


        $infoCampaigns = array();
        $campaigns = $advertiser->findCampaigns();
        foreach ($campaigns as $campaign) {
            $infoCampaigns[] = $this->configureTableBlockCampaign($infoObjectTotal, $campaign, $forciblePackage);
        }

        $infoUnassigned = $this->configureTableBlockAdvertiserUnassigned($infoObjectTotal, $advertiser, $forciblePackage);

        $infoNumbersAmount = $this->configureAdvertiserNumbersAmountInfo($infoObjectTotal, $advertiser, $forciblePackage);   
        
        $info = $this->configureNode($advertiser, $infoObjectTotal, array(), array(), $infoCampaigns, $infoUnassigned, $infoNumbersAmount);
        $parentInfoTotal = $this->updateParentTotalNode($parentInfoTotal, $infoObjectTotal);
        return $info;
    }
}
