<?php

/**
Format of statistics for Resellers:

$info = array(
    InvoicesGenerator::LEVEL_TOTAL => array(
        InvoicesGenerator::INFO_NAME                => 'resellerName1',
        InvoicesGenerator::INFO_COMPANY_LEVEL       => lev,
        InvoicesGenerator::INFO_EMAIL_TO            => e,
        InvoicesGenerator::INFO_TIME_UTC_STR_FROM   => tf,
        InvoicesGenerator::INFO_TIME_UTC_STR_TO     => tt,
        InvoicesGenerator::INFO_INVOICE_TYPE_MONEY  => it,
        InvoicesGenerator::INFO_INVOICE_TYPE_DETAILS=> jt,
 
        InvoicesGenerator::INFO_AMOUNT_NUMBERS_LOCAL            => x,
        InvoicesGenerator::INFO_AMOUNT_NUMBERS_TOLL_FREE        => y,
        InvoicesGenerator::INFO_AMOUNT_MINUTES_LOCAL            => z,
        InvoicesGenerator::INFO_AMOUNT_MINUTES_TOLL_FREE        => zz,

        InvoicesGenerator::INFO_PRICE_NUMBERS_LOCAL             => xx,
        InvoicesGenerator::INFO_PRICE_NUMBERS_TOLL_FREE         => yy,
        InvoicesGenerator::INFO_PRICE_MINUTES_LOCAL             => zz,
        InvoicesGenerator::INFO_PRICE_MINUTES_TOLL_FREE         => zzzz,

        InvoicesGenerator::INFO_PRICE_TOTAL                     => t,
    ),
    InvoicesGenerator::LEVEL_SUB_COMPANY => array(
        0 => array(
            InvoicesGenerator::LEVEL_TOTAL => array(
                InvoicesGenerator::INFO_NAME         => 'agName1',

                InvoicesGenerator::INFO_AMOUNT_NUMBERS_LOCAL            => x,
                InvoicesGenerator::INFO_AMOUNT_NUMBERS_TOLL_FREE        => y,
                InvoicesGenerator::INFO_AMOUNT_MINUTES_LOCAL            => z,
                InvoicesGenerator::INFO_AMOUNT_MINUTES_TOLL_FREE        => zz,

                InvoicesGenerator::INFO_RATE_PER_MONTH_NUMBER_LOCAL     => a,
                InvoicesGenerator::INFO_RATE_PER_MONTH_NUMBER_TOLL_FREE => b,
                InvoicesGenerator::INFO_RATE_PER_MINUTE_LOCAL           => c,
                InvoicesGenerator::INFO_RATE_PER_MINUTE_TOLL_FREE       => d,

                InvoicesGenerator::INFO_PRICE_NUMBERS_LOCAL             => xx,
                InvoicesGenerator::INFO_PRICE_NUMBERS_TOLL_FREE         => yy,
                InvoicesGenerator::INFO_PRICE_MINUTES_LOCAL             => zz,
                InvoicesGenerator::INFO_PRICE_MINUTES_TOLL_FREE         => zzzz,

                InvoicesGenerator::INFO_PRICE_TOTAL                     => t,
            ),
        ),
        ...
        N => array(...)
    ),
    InvoicesGenerator::LEVEL_SUB_ADVERTISER => array(
        0 => array(
            InvoicesGenerator::LEVEL_TOTAL => array(
                InvoicesGenerator::INFO_NAME         => 'advName1',

                InvoicesGenerator::INFO_AMOUNT_NUMBERS_LOCAL            => x,
                InvoicesGenerator::INFO_AMOUNT_NUMBERS_TOLL_FREE        => y,
                InvoicesGenerator::INFO_AMOUNT_MINUTES_LOCAL            => z,
                InvoicesGenerator::INFO_AMOUNT_MINUTES_TOLL_FREE        => zz,

                InvoicesGenerator::INFO_RATE_PER_MONTH_NUMBER_LOCAL     => a,
                InvoicesGenerator::INFO_RATE_PER_MONTH_NUMBER_TOLL_FREE => b,
                InvoicesGenerator::INFO_RATE_PER_MINUTE_LOCAL           => c,
                InvoicesGenerator::INFO_RATE_PER_MINUTE_TOLL_FREE       => d,

                InvoicesGenerator::INFO_PRICE_NUMBERS_LOCAL             => xx,
                InvoicesGenerator::INFO_PRICE_NUMBERS_TOLL_FREE         => yy,
                InvoicesGenerator::INFO_PRICE_MINUTES_LOCAL             => zz,
                InvoicesGenerator::INFO_PRICE_MINUTES_TOLL_FREE         => zzzz,

                InvoicesGenerator::INFO_PRICE_TOTAL                     => t,
            ),
        ),
        ...
        N => array(...)
    )
)

 */

class InvoicesGeneratorSummary extends InvoicesGenerator {

    protected function configureAdvertiserSubCampaignInfo(&$parentInfoTotal, $campaign, $forciblePackage) {

        return $this->configureTableBlockCampaign(
            $parentInfoTotal, 
            $campaign, 
            $forciblePackage
        );
    }

    protected function configureAdvertiserUnassignedInfo(&$parentInfoTotal, $advertiser, $forciblePackage) {

        return $this->configureTableBlockAdvertiserUnassigned(
            $parentInfoTotal, 
            $advertiser, 
            $forciblePackage
        );
    }

    protected function configureAdvertiserNumbersAmountInfo(&$parentInfoTotal, $advertiser, $forciblePackage) {

        return $this->configureTableBlockAdvertiserNumbersAmount(
            $parentInfoTotal, 
            $advertiser, 
            $forciblePackage
        );
    }

    protected function configureCompanySubCompanyInfo(&$parentInfoTotal, $company, $forciblePackage) {

        return $this->configureTableBlockObj(
            $parentInfoTotal,
            $company,
            $forciblePackage
        );
    }

    protected function configureCompanySubAdvertiserInfo(&$parentInfoTotal, $advertiser, $forciblePackage) {

        return $this->configureTableBlockObj(
            $parentInfoTotal,
            $advertiser,
            $forciblePackage
        );
    }
}
