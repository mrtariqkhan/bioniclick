<?php

abstract class ExcelInvoiceHelper extends FileGeneratorBase {

    const HASH_ALGO                 = 'sha1';
    const FILE_EXTENSION            = '.xls';
    const SF_DIR_TO_GENERATE        = 'sf_upload_dir';
    const PATH_TO_DIR_ADDITIONAL    = 'tmpInvoices/';
    const PATH_TO_LOGO_ADDITIONAL   = 'images/logo.png';

    const FIRST_COL                 = 0;
    const FIRST_ROW                 = 1;
    const FORMATTING_FIRST_COL      = 2;
    const FORMATTING_FIRST_ROW      = 5;

    const SHEET_FIRST_INDEX         = 0;
    const SHEET_TITLE_MAX_LENGTH    = 31; //see PHPExcel_Worksheet::_checkSheetTitle mehod

//    const FORMAT_NUMBER_0       = '0.0';
    const FORMAT_CURRENCY_USD   = '"$"#,###0.000_-';


    protected $data = array();

    protected $pathToDir;
    protected $pathToLogoDir;
    protected $pIndex;
    protected $phpExcel;
    protected $firstColNumber;
    protected $firstRowNumber;
    protected $headerTableStyle;

    protected $styleDefault;
    protected $styleInnerBorder;
    protected $styleHeaderTable;
    protected $styleTotal;
    protected $styleGrandTotal;
    protected $styleBold;
    protected $styleMoney;
    protected $styleAmount;
    protected $styleTimeAmount;
    protected $stylePostScriptum;
    protected $styleThank;
    protected $styleCompanyToTitle;
    protected $styleCompanyTo;

    /**
     *
     * @param array $data 
     */
    public function __construct($data) {

        $this->setData($data);

        $this->setFileExtension(self::FILE_EXTENSION);
        $this->setSfDirToGenerate(self::SF_DIR_TO_GENERATE);
        $this->generatePathToDirAdditional(self::PATH_TO_DIR_ADDITIONAL);

        parent::__construct();

        $this->pathToLogoDir    = sfConfig::get('sf_web_dir') . DIRECTORY_SEPARATOR . self::PATH_TO_LOGO_ADDITIONAL;

        $this->styleDefault         = $this->configureStyleDefault();
        $this->styleInnerBorder     = $this->configureStyleInnerBorder();
        $this->styleHeaderTable     = $this->configureStyleHeaderTable();
        $this->styleTotal           = $this->configureStyleTotal();
        $this->styleGrandTotal      = $this->configureStyleGrandTotal();
        $this->styleBold            = $this->configureStyleBold();
        $this->styleMoney           = $this->configureStyleMoney();
        $this->styleAmount          = $this->configureStyleAmount();
        $this->styleTimeAmount      = $this->configureStyleTimeAmount();
        $this->stylePostScriptum    = $this->configureStylePostScriptum();
        $this->styleThank           = $this->configureStyleThank();
        $this->styleCompanyTo       = $this->configureStyleCompanyTo();
        $this->styleCompanyToTitle  = $this->configureStyleCompanyToTitle();
    }

    protected function preFillFile() {
        
        $this->phpExcel = new PHPExcel();

        $this->pIndex           = null;

        $this->firstColNumber   = self::FORMATTING_FIRST_COL;
        $this->firstRowNumber   = self::FORMATTING_FIRST_ROW;
    }

    protected function postFillFile() {

        $this->generateFileName();
        $this->deleteFile();

        $this->phpExcel->setActiveSheetIndex(self::SHEET_FIRST_INDEX);

        $writer = new PHPExcel_Writer_Excel5($this->phpExcel);
        $writer->save($this->getPathToFile());

        return $this->getFileName();
    }

    protected function addNewActiveSheet($sheetTitle) {

        $sheetTitle = self::correctSheetTitle($sheetTitle);

        $this->firstColNumber = self::FORMATTING_FIRST_COL;
        $this->firstRowNumber = self::FORMATTING_FIRST_ROW;

        if (is_null($this->pIndex)) {
            //NOTE:: first sheet exists always.
            $this->pIndex = self::SHEET_FIRST_INDEX;
        } else {
            $this->pIndex++;
            $this->phpExcel->createSheet($this->pIndex);
        }

        $this->phpExcel->setActiveSheetIndex($this->pIndex);

        $this->phpExcel->getActiveSheet()
            ->setTitle($sheetTitle)
        ;

        $this->phpExcel->getDefaultStyle()->applyFromArray($this->styleDefault);
    }

    protected function postFillSheet() {

        $activeSheet = $this->phpExcel->getActiveSheet();

        $highestColumnIndex = $this->getSheetHighestColumnIndex();
        for ($colIndex = self::FIRST_COL; $colIndex < $highestColumnIndex; ++$colIndex) {
            $colName = PHPExcel_Cell::stringFromColumnIndex($colIndex);
            $activeSheet->getColumnDimension($colName)->setAutoSize(true);
        }
    }

    protected function doWriteSheetFooter($subData) {
        $this->postFillSheet();
    }

    protected function doWriteAmount($col, $row, $content, $style = array(), $pIndex = null) {
        $this->doWrite($col, $row, $content, array_merge($style, $this->styleAmount), $pIndex);
    }

    protected function doWriteTimeAmount($col, $row, $content, $style = array(), $pIndex = null) {
        $this->doWrite($col, $row, $content, array_merge($style, $this->styleTimeAmount), $pIndex);
    }

    protected function doWriteMoney($col, $row, $content, $style = array(), $pIndex = null) {
        $this->doWrite($col, $row, $content, array_merge($style, $this->styleMoney), $pIndex);
    }

    /**
     *
     * @param int $col belongs to [0, ...]
     * @param int $row belongs to [1 ...]
     * @param string $content
     * @param array $style cell's style
     * @param int $pIndex
     */
    protected function doWrite($col, $row, $content, $style = array(), $pIndex = null) {

        $pIndex = empty($pIndex) ? $this->pIndex : $pIndex;

        $this->phpExcel
            ->setActiveSheetIndex($pIndex)
            ->setCellValueByColumnAndRow($col, $row, $content)
        ;

        if (!empty($style)) {
            $this->doStyleCell($style, $col, $row);
        }
    }

    /**
     *
     * @param int $colFrom belongs to [0, ...]
     * @param int $colTo belongs to [0, ...]
     * @param int $row belongs to [1 ...]
     */
    protected function doMergeColumns($colFrom, $colTo, $row) {

        $this->phpExcel->getActiveSheet()->mergeCells(
            $this->convertRangeFromNumericToLiteral($colFrom, $row, $colTo, $row)
        );
    }

    /**
     * changes style of rectangle
     * @param array $style
     * @param type $leftTopCol
     * @param type $leftTopRow
     * @param type $rightBottomCol
     * @param type $rightBottomRow
     * @param integer $pIndex
     */
    protected function doStyleRectangle(
        $style,
        $leftTopCol, $leftTopRow,
        $rightBottomCol, $rightBottomRow,
        $pIndex = null
    ) {

        $pIndex = empty($pIndex) ? $this->pIndex : $pIndex;

        $this->phpExcel
            ->setActiveSheetIndex($pIndex)
            ->getStyle(
                $this->convertRangeFromNumericToLiteral($leftTopCol, $leftTopRow, $rightBottomCol, $rightBottomRow)
            )
            ->applyFromArray($style)
        ;
    }

    /**
     * changes style of cell
     * @param array $style
     * @param type $leftTopCol
     * @param type $leftTopRow
     * @param type $pIndex
     */
    protected function doStyleCell(
        $style,
        $leftTopCol, $leftTopRow,
        $pIndex = null
    ) {

        $pIndex = empty($pIndex) ? $this->pIndex : $pIndex;

        $this->phpExcel
            ->setActiveSheetIndex($pIndex)
            ->getStyle($this->convertCoordFromNumericToLiteral($leftTopCol, $leftTopRow))
            ->applyFromArray($style)
        ;
    }

    /**
     *
     * @param type $col belongs to [0, ...]
     * @param type $row belongs to [1 ...]
     * @return type
     */
    protected function convertCoordFromNumericToLiteral($col, $row) {
        return PHPExcel_Cell::stringFromColumnIndex($col) . $row;
    }

    protected function convertRangeFromNumericToLiteral($leftTopCol, $leftTopRow, $rightBottomCol, $rightBottomRow) {

        $leftTop        = $this->convertCoordFromNumericToLiteral($leftTopCol, $leftTopRow);
        $rightBottom    = $this->convertCoordFromNumericToLiteral($rightBottomCol, $rightBottomRow);

        return "$leftTop:$rightBottom";
    }

    protected function getSheetHighestColumnIndex() {

        $activeSheet = $this->phpExcel->getActiveSheet();

        $highestColumn = $activeSheet->getHighestColumn();
        $highestColumnIndex = PHPExcel_Cell::columnIndexFromString($highestColumn);

        return $highestColumnIndex;
    }

    protected function configureStyleDefault() {
        return array(
            'font' => array(
                'name' => 'Calibri',
                'size' => 11,
            ),
        );
    }

    protected function configureStyleCompanyTo() {

        $style = $this->styleDefault;
        $style['font']['size'] = 12;

        return $style;
    }

    protected function configureStyleCompanyToTitle() {
        return $this->combineStyles($this->styleBold, $this->styleCompanyTo);
    }

    protected function configureStyleInnerBorder() {
        return array(
            'borders' => array(
                'outline' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
                ),
                'vertical' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
                ),
            ),
        );
    }

    protected function configureStyleHeaderTable() {
        return array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_CENTER,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
                ),
            ),
        );
    }

    protected function configureStyleTotal() {
        return array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            ),
        );
    }

    protected function configureStyleGrandTotal() {
        return array(
            'font' => array(
                'bold' => true,
            ),
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            ),
            'borders' => array(
                'allborders' => array(
                    'style' => PHPExcel_Style_Border::BORDER_THIN,
                    'color' => array('argb' => '00000000'),
                ),
            ),
        );
    }

    protected function configureStyleBold() {
        return array(
            'font' => array(
                'bold' => true,
            ),
        );
    }

    protected function configureStyleMoney() {
        return array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            ),
            'numberformat' => array (
//                'code' => PHPExcel_Style_NumberFormat::FORMAT_CURRENCY_USD_SIMPLE
                'code' => self::FORMAT_CURRENCY_USD
            ),
        );
    }

    protected function configureStyleAmount() {
        return array(
            'alignment' => array(
                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
            ),
            'numberformat' => array (
                'code' => PHPExcel_Style_NumberFormat::FORMAT_NUMBER
            ),
        );
    }

    protected function configureStyleTimeAmount() {
        return $this->configureStyleAmount();
//        return array(
//            'alignment' => array(
//                'horizontal' => PHPExcel_Style_Alignment::HORIZONTAL_RIGHT,
//            ),
//            'numberformat' => array (
//                'code' => self::FORMAT_NUMBER_0
//            ),
//        );
    }

    protected function configureStylePostScriptum() {
        return array(
            'font' => array(
                'name' => 'Arial',
                'size' => 10,
            ),
        );
    }

    protected function configureStyleThank() {
        return $this->combineStyles($this->styleBold, $this->stylePostScriptum);
    }

    protected function combineStyles($style1, $style2) {

        $style = array_merge($style1, $style2);
        foreach ($style1 as $key => $style1Part) {
            if (array_key_exists($key, $style2)) {
                $style2Part = $style2[$key];
                $style[$key] = array_merge($style1Part, $style2Part);
            }
        }

        return $style;
    }

    protected function generatePathToDirAdditional($pathToDirAdditional) {
        $this->setPathToDirAdditional($pathToDirAdditional);
    }

    protected function generateFileNameUnique() {

        //TODO:: uncomment when we will need hash:
//        $today = TimeConverter::getTodayUnixTimestamp(TimeConverter::TIMEZONE_UTC);
//        $fileNameUnHash = " $today $invoiceType $companyName $companyEmail";
//        $fileNameGenerated = hash(self::HASH_ALGO, $fileNameUnHash);
        $fileNameGenerated = '';

        return $fileNameGenerated;
    }
    protected function generateFileNamePrefix() {
        //NOTE:: extend it
        return 'new file';
    }

    protected static function correctSheetTitle($title) {

        if (strlen($title) > self::SHEET_TITLE_MAX_LENGTH) {
            $title = substr($title, 0, self::SHEET_TITLE_MAX_LENGTH - 3) . '...';
        }

        return $title;
    }

    public function getData() {
        return $this->data;
    }
    protected function setData($data) {
        $this->data = $data;
    }
}
