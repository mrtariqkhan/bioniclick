<?php

require_once(dirname(__FILE__) . '/../invoices/InvoiceTypes.class.php');
require_once(dirname(__FILE__) . '/../../model/doctrine/CompanyTable.class.php');

class InvoicesGeneratorBase {

    const INVOICE_TYPE_HAVE_TO_PAY = 1;
    const INVOICE_TYPE_HAVE_TO_GET = 2;

    const INVOICE_TYPE_NAME_HAVE_TO_PAY = 'Pay';
    const INVOICE_TYPE_NAME_HAVE_TO_GET = 'Get';


    const INVOICE_TYPE_DETAILS_SUMMARY  = 1;
    const INVOICE_TYPE_DETAILS_FULL     = 2;

    const INVOICE_TYPE_NAME_DETAILS_SUMMARY = 'Summary';
    const INVOICE_TYPE_NAME_DETAILS_FULL    = 'Full';


    protected $nodeTotalList;
    protected $company;
    protected $companyLevel;

    protected $invoiceTypeMoney;
    protected $invoiceTypeDetails;

    protected $timeFrom;
    protected $timeTo;
    protected $timeFromString;
    protected $timeToString;


    public function __construct($company, $invoiceTypeMoney, $invoiceTypeDetails, $range) {

        $this->company      = $company;
        $this->companyLevel = $company->getLevel();

        $this->invoiceTypeMoney     = $invoiceTypeMoney;
        $this->invoiceTypeDetails   = $invoiceTypeDetails;

        $this->timeFrom = $range['from'];
        $this->timeTo   = $range['to'];
        $this->timeFromString   = TimeConverter::getDateTime($this->timeFrom, TimeConverter::TIMEZONE_UTC);
        $this->timeToString     = TimeConverter::getDateTime($this->timeTo, TimeConverter::TIMEZONE_UTC);

        $this->configureNodeTotalList();
    }

    public static function getTypesLevel() {

        return new InvoiceTypes(
            CompanyTable::getLevels()
        );
    }

    public static function getTypesMoney($companyLevel = -1) {

        switch ($companyLevel) {
            case CompanyTable::$LEVEL_ADVERTISER:
                return new InvoiceTypes(
                    array(
                        self::INVOICE_TYPE_HAVE_TO_PAY => self::INVOICE_TYPE_NAME_HAVE_TO_PAY,
                    )
                );
            case CompanyTable::$LEVEL_BIONIC:
                return new InvoiceTypes(
                    array(
                        self::INVOICE_TYPE_HAVE_TO_GET => self::INVOICE_TYPE_NAME_HAVE_TO_GET,
                    )
                );
            default:
                return new InvoiceTypes(
                    array(
                        self::INVOICE_TYPE_HAVE_TO_GET => self::INVOICE_TYPE_NAME_HAVE_TO_GET,
                        self::INVOICE_TYPE_HAVE_TO_PAY => self::INVOICE_TYPE_NAME_HAVE_TO_PAY,
                    )
                );
        }
    }

    public static function getTypesDetails($companyLevel = -1) {

        switch ($companyLevel) {
            case CompanyTable::$LEVEL_ADVERTISER:
                return new InvoiceTypes(
                    array(
                        self::INVOICE_TYPE_DETAILS_SUMMARY => self::INVOICE_TYPE_NAME_DETAILS_SUMMARY,
                    )
                );
            default:
                return new InvoiceTypes(
                    array(
                        self::INVOICE_TYPE_DETAILS_SUMMARY => self::INVOICE_TYPE_NAME_DETAILS_SUMMARY,
                        self::INVOICE_TYPE_DETAILS_FULL    => self::INVOICE_TYPE_NAME_DETAILS_FULL,
                    )
                );
        }
    }

    public function generateData() {

        switch ($this->companyLevel) {
            case CompanyTable::$LEVEL_BIONIC:
            case CompanyTable::$LEVEL_RESELLER:
            case CompanyTable::$LEVEL_AGENCY:

                if ($this->invoiceTypeMoney == self::INVOICE_TYPE_HAVE_TO_PAY
                    && CompanyTable::$LEVEL_BIONIC == $this->companyLevel
                ) {
                    throw new Exception('Bionic has no parent company so it cannot pay money so it cannot have pay-invoice.');
                }

                return $this->generateDataForCompany();
            case CompanyTable::$LEVEL_ADVERTISER:

                if ($this->invoiceTypeMoney == self::INVOICE_TYPE_HAVE_TO_GET) {
                    throw new Exception('Advertiser has no child companies so it cannot get money so it cannot have get-invoice.');
                }
                if ($this->invoiceTypeDetails == self::INVOICE_TYPE_DETAILS_FULL) {
                    throw new Exception('Advertiser has no child companies so it cannot have full-invoice.');
                }

                return $this->generateDataForAdvertiser();
            default:
        }

        return array();
    }

    protected function generateDataForCompany() {

        $infoObjectTotal = $this->initTotalNode();

        $forciblePackage = null;
        if ($this->invoiceTypeMoney == self::INVOICE_TYPE_HAVE_TO_PAY) {
            $forciblePackage = $this->company->getPackage();
        }


        $infoSubCompanies = array();
        $subCompanies = $this->company->findSubCompaniesFirstForInvoice($this->timeFrom, $this->timeTo);
        foreach ($subCompanies as $subCompany) {
            $infoSubCompanies[] = $this->configureCompanySubCompanyInfo($infoObjectTotal, $subCompany, $forciblePackage);
        }

        $infoSubAdvertisers = array();
        $subAdvertisers = $this->company->findSubAdvertisersFirstForInvoice($this->timeFrom, $this->timeTo);
        foreach ($subAdvertisers as $subAdvertiser) {
            $infoSubAdvertisers[] = $this->configureCompanySubAdvertiserInfo($infoObjectTotal, $subAdvertiser, $forciblePackage);
        }

        $info = $this->configureNode($this->company, $infoObjectTotal, $infoSubCompanies, $infoSubAdvertisers);
        return $info;
    }

    protected function generateDataForAdvertiser() {

        $advertiser = $this->company;

        $infoObjectTotal = $this->initTotalNode();

        $forciblePackage = $advertiser->getPackage();

        $infoCampaigns = array();
        $campaigns = $advertiser->findCampaignsForInvoice($this->timeFrom, $this->timeTo);
        foreach ($campaigns as $campaign) {
            $infoCampaigns[] = $this->configureAdvertiserSubCampaignInfo($infoObjectTotal, $campaign, $forciblePackage);
        }

        $infoUnassigned = $this->configureAdvertiserUnassignedInfo($infoObjectTotal, $advertiser, $forciblePackage);
      
        $infoNumbersAmount = $this->configureAdvertiserNumbersAmountInfo($infoObjectTotal, $advertiser, $forciblePackage);        

        $info = $this->configureNode($advertiser, $infoObjectTotal, array(), array(), $infoCampaigns, $infoUnassigned, $infoNumbersAmount);

        return $info;
    }

    protected function configureCompanySubCompanyInfo(&$parentInfoTotal, $company, $forciblePackage) {
        //NOTE:: extend it
        return array();
    }

    protected function configureCompanySubAdvertiserInfo(&$parentInfoTotal, $advertiser, $forciblePackage) {
        //NOTE:: extend it
        return array();
    }

    protected function configureAdvertiserSubCampaignInfo(&$parentInfoTotal, $campaign, $forciblePackage) {
        //NOTE:: extend it
        return array();
    }

    protected function configureAdvertiserUnassignedInfo(&$parentInfoTotal, $advertiser, $forciblePackage) {
        //NOTE:: extend it
        return array();
    }

    protected function configureAdvertiserNumbersAmountInfo(&$parentInfoTotal, $advertiser, $forciblePackage) {
        //NOTE:: extend it
        return array();
    }


    protected function configureNode(
        $obj,
        $infoObjectTotal, 
        $infoSubCompanies   = array(),
        $infoSubAdvertisers = array(),
        $infoCampaigns      = array(),
        $infoUnassigned     = array(),
        $infoNumbersAmount  = array()
    ) {
        return array();
    }

    protected function initTotalNode() {

        $infoObjectTotal = array();
        foreach ($this->nodeTotalList as $itemName) {
            $infoObjectTotal[$itemName] = 0;
        }

        return $infoObjectTotal;
    }

    protected function updateParentTotalNode($parentInfoTotal, $infoObjectTotal) {

        foreach ($this->nodeTotalList as $itemName) {
            $parentInfoTotal[$itemName] += array_key_exists($itemName, $infoObjectTotal) ? $infoObjectTotal[$itemName] : 0;
        }

        return $parentInfoTotal;
    }

    protected function getTypeMoney() {
        return self::getTypesMoney($this->companyLevel)->getTypeName($this->invoiceTypeMoney);
    }

    protected function getTypeDetails() {
        return self::getTypesDetails($this->companyLevel)->getTypeName($this->invoiceTypeDetails);
    }

    protected function configureNodeTotalList() {
        $this->nodeTotalList = array();
    }
}
