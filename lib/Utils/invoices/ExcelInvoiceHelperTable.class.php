<?php

abstract class ExcelInvoiceHelperTable extends ExcelInvoiceHelper {

    const COLUMN_ACCOUNT        = 'Account';
    const COLUMN_ITEM           = 'Item';
    const COLUMN_QUANTITY       = 'Quantity';
    const COLUMN_RATE           = 'Rate';
    const COLUMN_TOTAL_DUE      = 'Total Due';

    const ROW_GRAND_TOTAL_DUE   = 'TOTAL AMOUNT DUE';
    const ROW_GRAND_TOTAL       = 'GRAND TOTAL';
    const ROW_TOTAL             = 'Total';


    const ITEM_NAME_PREFIX_SUB_COMPANY  = '"';
    const ITEM_NAME_PREFIX_ADVERTISER   = '"';
    const ITEM_NAME_PREFIX_CAMPAIGN     = '"';

    const ITEM_NAME_POSTFIX_SUB_COMPANY = '" Company';
    const ITEM_NAME_POSTFIX_ADVERTISER  = '" Company';
    const ITEM_NAME_POSTFIX_CAMPAIGN    = '" Campaign';
    const ITEM_NAME_POSTFIX_ADVERTISER_UNASSIGNED   = '" Company (unassigned phone numbers)';
    const ITEM_NAME_POSTFIX_ADVERTISER_NUMBER_AMOUNT= '" Company (all phone numbers)';

    protected function doWritePartTableHeader($subData, $ifNeedRate = true, $ifTotalNeeded = true) {

        $col = $this->firstColNumber;
        $row = $this->firstRowNumber;

        $this->doWrite($col++, $row, self::COLUMN_ACCOUNT, $this->styleHeaderTable);
        $this->doWrite($col++, $row, self::COLUMN_ITEM, $this->styleHeaderTable);
        $this->doWrite($col++, $row, self::COLUMN_QUANTITY, $this->styleHeaderTable);
        if ($ifNeedRate) {
            $this->doWrite($col++, $row, self::COLUMN_RATE, $this->styleHeaderTable);
        }
        if ($ifTotalNeeded) {
            $this->doWrite($col++, $row, self::COLUMN_TOTAL_DUE, $this->styleHeaderTable);
        }
        $this->firstRowNumber++;
    }

    /**
     *
     * @param array $subData
     * @param string $itemNamePrefix one from ExcelInvoiceHelper::ITEM_NAME_PREFIX_*
     * @param string $itemNamePostfix one from ExcelInvoiceHelper::ITEM_NAME_POSTFIX_*
     */
    protected function doWritePartTableBlock(array $subData, $itemNamePrefix, $itemNamePostfix, $ifNeedRate = true, $ifTotalNeeded = true) {

        $leftTopCol = $this->firstColNumber;
        $leftTopRow = $this->firstRowNumber;

        $col = $this->firstColNumber;
        $row = $this->firstRowNumber;

        $infoTotal = self::getArrayValueArray($subData, InvoicesGenerator::LEVEL_TOTAL);
        $account = $itemNamePrefix . self::getArrayValue($infoTotal, InvoicesGenerator::INFO_NAME) . $itemNamePostfix;
        $this->doWrite($col++, $row, $account);
        $this->firstRowNumber++;

        $needNumbersAmount = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_NEED_NUMBERS_AMOUNT);
        $needCallTimeAmount = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_NEED_CALL_TIME_AMOUNT);

        if ($needNumbersAmount) {
            $col = $this->firstColNumber;
            $row = $this->firstRowNumber;

            $this->doWrite($col++, $row, '');
            $this->doWrite($col++, $row, InvoicesGenerator::INFO_AMOUNT_NUMBERS_LOCAL);
            $this->doWriteAmount($col++, $row, self::getArrayValue($infoTotal, InvoicesGenerator::INFO_AMOUNT_NUMBERS_LOCAL));
            if ($ifNeedRate)
                $this->doWriteMoney($col++, $row, self::getArrayValue($infoTotal, InvoicesGenerator::INFO_RATE_PER_MONTH_NUMBER_LOCAL));

            if ($ifTotalNeeded)
                $this->doWriteMoney($col++, $row, self::getArrayValue($infoTotal, InvoicesGenerator::INFO_PRICE_NUMBERS_LOCAL));
            $this->firstRowNumber++;


            $col = $this->firstColNumber;
            $row = $this->firstRowNumber;

            $this->doWrite($col++, $row, '');
            $this->doWrite($col++, $row, InvoicesGenerator::INFO_AMOUNT_NUMBERS_TOLL_FREE);
            $this->doWriteAmount($col++, $row, self::getArrayValue($infoTotal, InvoicesGenerator::INFO_AMOUNT_NUMBERS_TOLL_FREE));
            if ($ifNeedRate)
                $this->doWriteMoney($col++, $row, self::getArrayValue($infoTotal, InvoicesGenerator::INFO_RATE_PER_MONTH_NUMBER_TOLL_FREE));
            if ($ifTotalNeeded)
                $this->doWriteMoney($col++, $row, self::getArrayValue($infoTotal, InvoicesGenerator::INFO_PRICE_NUMBERS_TOLL_FREE));
            $this->firstRowNumber++;
        }

        if ($needCallTimeAmount) {
            $col = $this->firstColNumber;
            $row = $this->firstRowNumber;

            $this->doWrite($col++, $row, '');
            $this->doWrite($col++, $row, InvoicesGenerator::INFO_AMOUNT_MINUTES_LOCAL);
            $this->doWriteTimeAmount($col++, $row, self::getArrayValue($infoTotal, InvoicesGenerator::INFO_AMOUNT_MINUTES_LOCAL));
            if ($ifNeedRate)
                $this->doWriteMoney($col++, $row, self::getArrayValue($infoTotal, InvoicesGenerator::INFO_RATE_PER_MINUTE_LOCAL));
            if ($ifTotalNeeded)
                $this->doWriteMoney($col++, $row, self::getArrayValue($infoTotal, InvoicesGenerator::INFO_PRICE_MINUTES_LOCAL));
            $this->firstRowNumber++;


            $col = $this->firstColNumber;
            $row = $this->firstRowNumber;

            $this->doWrite($col++, $row, '');
            $this->doWrite($col++, $row, InvoicesGenerator::INFO_AMOUNT_MINUTES_TOLL_FREE);
            $this->doWriteTimeAmount($col++, $row, self::getArrayValue($infoTotal, InvoicesGenerator::INFO_AMOUNT_MINUTES_TOLL_FREE));
            if ($ifNeedRate)
                $this->doWriteMoney($col++, $row, self::getArrayValue($infoTotal, InvoicesGenerator::INFO_RATE_PER_MINUTE_TOLL_FREE));
            if ($ifTotalNeeded)
                $this->doWriteMoney($col++, $row, self::getArrayValue($infoTotal, InvoicesGenerator::INFO_PRICE_MINUTES_TOLL_FREE));
            $this->firstRowNumber++;

            $col = $this->firstColNumber;
            $row = $this->firstRowNumber;
        }

        if ($ifTotalNeeded){
            $priceTotal = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_PRICE_TOTAL);
            $this->doWritePartTableTotal($priceTotal, ExcelInvoiceHelperTable::ROW_TOTAL, $this->styleTotal);
        }

        if ($ifTotalNeeded && $ifNeedRate)
            $col = $this->firstColNumber + 5;
        else if ($ifTotalNeeded || $ifNeedRate)
            $col = $this->firstColNumber + 4;
        else
            $col = $this->firstColNumber + 3;

        $rightBottomCol = $col - $leftTopCol + 1;
        $rightBottomRow = $this->firstRowNumber;
        $this->doStyleRectangle($this->styleInnerBorder, $leftTopCol, $leftTopRow, $rightBottomCol, $rightBottomRow);

        $this->firstRowNumber++;
    }

    protected function doWritePartTableTotal($priceTotal, $rawTitle, $style, $ifNeedRate = true) {

        $col = $this->firstColNumber;
        $row = $this->firstRowNumber;

        $this->doWrite($col++, $row, $rawTitle, $style);
        $this->doWrite($col++, $row, '', $style);
        $this->doWrite($col++, $row, '', $style);
        if ($ifNeedRate) {
            $this->doWrite($col++, $row, '', $style);
        }
        $this->doWriteMoney($col++, $row, $priceTotal, $style);

        $this->firstRowNumber++;
    }

    protected static function getArrayValue($subData, $key) {

//        $value = $data[$key];
//        return $value;

        $value = array_key_exists($key, $subData) ? $subData[$key] : '';

        return $value;
    }

    protected static function getArrayValueArray($subData, $key) {

//        $value = $data[$key];
//        return $value;

        $value = is_array($subData) ? $subData : array();
        $value = array_key_exists($key, $value) ? $value[$key] : array();
        $value = empty($value) ? array() : $value;

        return $value;
    }

    protected function generateFileNamePrefix() {

        $data = $this->getData();

        $infoTotal = self::getArrayValueArray($data, InvoicesGenerator::LEVEL_TOTAL);
        $name               = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_NAME);
        $timeFromString     = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_TIME_UTC_STR_FROM);
        $invoiceTypeMoney   = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_INVOICE_TYPE_MONEY);
        $invoiceTypeDetails = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_INVOICE_TYPE_DETAILS);

        $month = TimeConverter::getDateTimeFormattedByString($timeFromString, TimeConverter::TIMEZONE_UTC, 'Y-m');
        $prefix = $month . ' ' . ucfirst($name) . ' ' . strtoupper($invoiceTypeMoney) . ' ' . strtoupper($invoiceTypeDetails);

        return $prefix;
    }

    protected function generatePathToDirAdditional($pathToDirAdditional) {

        $data = $this->getData();

        $infoTotal = self::getArrayValueArray($data, InvoicesGenerator::LEVEL_TOTAL);
        $level = self::getArrayValue($infoTotal, InvoicesGenerator::INFO_COMPANY_LEVEL);

        $levelName = CompanyTable::getLevelName($level);

        $subPathToDirAdditional = $levelName;
        if (!empty($subPathToDirAdditional) && $levelName != CompanyTable::$LEVEL_NAME_ALL) {
            $pathToDirAdditional = $pathToDirAdditional . $subPathToDirAdditional . DIRECTORY_SEPARATOR;
        }

        $this->setPathToDirAdditional($pathToDirAdditional);
    }
}
