<?php

class IsAllowableHelper {

    /**
     *
     * @param <any> $object
     * @param sfGuardUser $sfGuardUser
     * @param bool $forUpdating true if check if object is allowable for updating, deleting ...
     * @return boolean true if object's owner is $sfGuardUser or some sub-user (from company sub-tree of $sfGuardUser)
     */
    public static function isAllowable($object, $sfGuardUser, $forUpdating = true) {
        
        if (empty($object)) {
            return false;
        }
        
        if (!method_exists($object, 'isAllowable')) {
            return true;
        }
        
        $allowable = $object->isAllowable($sfGuardUser, $forUpdating);
        if ($allowable && $forUpdating) {
            $allowable = WasDeletedHelper::ifCanUpdate($object);
        }

        return $allowable;
    }
}
