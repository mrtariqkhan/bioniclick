<?php

class BreadCrumb {

    const CLASS_NAME_COMMON = 'icon ';

    protected $routeName    = '';
    protected $routeParams  = array();
    protected $nameCommon   = '';
    protected $name         = '';
    protected $className    = '';

    /**
     *
     * @param string $routeName
     * @param array $routeParams
     * @param string $name
     * @param string $nameCommon
     * @param string $className
     */
    public function __construct(
        $routeName,
        $routeParams,
        $nameCommon,
        $name,
        $className
    ) {

        $this->setRouteName($routeName);
        $this->setRouteParams($routeParams);
        $this->setNameCommon($nameCommon);
        $this->setName($name);
        $this->setClassName(self::CLASS_NAME_COMMON . $className);
    }

    public function getTitle() {
        return $this->getNameCommon() . ': ' . $this->getName();
    }



    public function getRouteName() {
        return $this->routeName;
    }
    public function setRouteName($routeName) {
        $this->routeName = $routeName;
    }

    public function getRouteParams() {
        return $this->routeParams;
    }
    public function setRouteParams($routeParams) {
        $this->routeParams = $routeParams;
    }

    public function getNameCommon() {
        return $this->nameCommon;
    }
    public function setNameCommon($nameCommon) {
        $this->nameCommon = $nameCommon;
    }

    public function getName() {
        return $this->name;
    }
    public function setName($name) {
        $this->name = $name;
    }

    public function getClassName() {
        return $this->className;
    }
    public function setClassName($className) {
        $this->className = $className;
    }
}
