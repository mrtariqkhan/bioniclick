<?php

/**
 * Description of TaskHelper
 *
 * @author fairdev
 */
class TaskHelper {

    /**
     *
     * @param string $taskClassName
     * @return sfBaseTask 
     */
    public static function createTask(
        $taskClassName
    ) {

        $dispatcher = sfContext::getInstance()->getEventDispatcher();
        $formatter = new sfFormatter();
        $task = new $taskClassName($dispatcher, $formatter);

        return $task;
    }

    /**
     *
     * @param sfBaseTask $task
     * @param array $taskArguments
     * @param array $taskOptionsAdditional
     * @param string $env need environment
     * @return sfBaseTask 
     */
    public static function runTask(
        sfBaseTask $task,
        $taskArguments = array(),
        $taskOptionsAdditional = array(), 
        $env = 'dev'
    ) {

        $taskOptions = array_merge(
            array(
                'application'   => 'backend',
                'env'           => $env,
                'connection'    => 'doctrine',
            ),
            $taskOptionsAdditional
        );
        
        chdir(sfConfig::get('sf_root_dir')); 
        $task->run(
            $taskArguments,
            $taskOptions
        );

        return $task;
    }

    /**
     *
     * @param string $taskClassName
     * @param array $taskArguments
     * @param array $taskOptionsAdditional
     * @param string $env need environment
     * @return sfBaseTask 
     */
    public static function createAndRunTask(
        $taskClassName,
        $taskArguments = array(),
        $taskOptionsAdditional = array(), 
        $env = 'dev'
    ) {

        $task = self::createTask($taskClassName);

        self::runTask($task, $taskArguments, $taskOptionsAdditional, $env);

        return $task;
    }
}

