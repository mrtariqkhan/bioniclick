<?php

class ChartCallSummaryHelper extends ChartHelperByTzNameBase {

    const AMOUNT_DAYS   = 7;
    const AMOUNT_WEEKS  = 8;
    const AMOUNT_MONTHS = 12;
    const AMOUNT_YEARS  = 10;

    const AMOUNT_MAX_DAYS   = 7;
    const AMOUNT_MAX_WEEKS  = 8;
    const AMOUNT_MAX_MONTHS = 12;
    const AMOUNT_MAX_YEARS  = 10;

    const TYPE_PER_DAY      = 'Day';
    const TYPE_PER_WEEK     = 'Week';
    const TYPE_PER_MONTH    = 'Month';
    const TYPE_PER_YEAR     = 'Year';

    const FORMAT_PER_DAY    = 'Y M d (D)';
    const FORMAT_PER_WEEK   = 'Y M d';
    const FORMAT_PER_MONTH  = 'Y M d';
    const FORMAT_PER_YEAR   = 'Y M d';

    const USE_TWO_TICKS_PER_DAY     = false;
    const USE_TWO_TICKS_PER_WEEK    = true;
    const USE_TWO_TICKS_PER_MONTH   = true;
    const USE_TWO_TICKS_PER_YEAR    = true;

    const TICK_FORMAT_TWO = '%s - %s  ';
    const TICK_FORMAT_ONE = '%s  ';

    public function findData() {

        $filterValues = $this->getFilterValues();

        $type = array_key_exists('per_type', $filterValues) ? $filterValues['per_type'] : null;
        switch ($type) {
            case 'per_week':
                $result = $this->findDataPerWeek();
                break;
            case 'per_month':
                $result = $this->findDataPerMonth();
                break;
            case 'per_year':
                $result = $this->findDataPerYear();
                break;
            default:
                //NOTE:: 'per_day' or default
                $result = $this->findDataPerDay();
        }

        return $result;
    }



    protected function findDataPerYear() {
        return $this->findDataPerType(
            self::TYPE_PER_YEAR,
            self::AMOUNT_YEARS,
            self::FORMAT_PER_YEAR,
            self::USE_TWO_TICKS_PER_YEAR
        );
    }

    protected function findDataPerMonth() {
        return $this->findDataPerType(
            self::TYPE_PER_MONTH,
            self::AMOUNT_MONTHS,
            self::FORMAT_PER_MONTH,
            self::USE_TWO_TICKS_PER_MONTH
        );
    }

    protected function findDataPerWeek() {
        return $this->findDataPerType(
            self::TYPE_PER_WEEK,
            self::AMOUNT_WEEKS,
            self::FORMAT_PER_WEEK,
            self::USE_TWO_TICKS_PER_WEEK
        );
    }

    protected function findDataPerDay() {
        return $this->findDataPerType(
            self::TYPE_PER_DAY,
            self::AMOUNT_DAYS,
            self::FORMAT_PER_DAY,
            self::USE_TWO_TICKS_PER_DAY
        );
    }

    /**
     *
     * @param string $type one from self::TYPE_PER_*
     * @param integer $amount one from self::AMOUNT_*
     * @param array $tickFormat
     * @param boolean $useTwoTicks true if need both ticks
     * @return array
     */
    protected function findDataPerType(
        $type, 
        $amount, 
        $tickFormat, 
        $useTwoTicks = true
    ) {

        if ($amount <= 0) {
            throw new Exception('Amount of ticks should be positive integer');
        }

        $timezoneName = $this->getTzName();
        $filterValues = $this->getFilterValues();

        $timestampBoundFrom = $filterValues['from'];
        $timestampBoundTo   = $filterValues['to'];
        $nowTimestamp = TimeConverter::getUnixTimestamp();

        if (!empty($timestampBoundFrom) 
            && (empty($timestampBoundTo) || $timestampBoundTo > $nowTimestamp)
        ) {
            $timestampBoundTo = $nowTimestamp;
        }

        $dateStringBound = 'today';
        if (!empty($timestampBoundFrom) && !empty($timestampBoundTo)) {
            $dateStringBound = TimeConverter::getDateTime($timestampBoundTo, $timezoneName);

            switch ($type) {
                case self::TYPE_PER_WEEK:
                    $amount = self::AMOUNT_MAX_WEEKS;
                    break;
                case self::TYPE_PER_MONTH:
                    $amount = self::AMOUNT_MAX_MONTHS;
                    break;
                case self::TYPE_PER_YEAR:
                    $amount = self::AMOUNT_MAX_YEARS;
                    break;
                default:
                    //NOTE:: 'per_day' or default
                    $amount = self::AMOUNT_MAX_DAYS;
            }
        }

        $method = "TimeConverter::getRangeUnixTimestampsN{$type}sAgo";
        $timeRange = array(
            'from'  => '',
            'to'    => '',
        );
        $graphData = array();
        for ($i = 0; $i < $amount; ++$i) {

            $timestamps  = call_user_func_array($method, array($timezoneName, $i, $dateStringBound));

            $timestampFrom  = $timestamps['from'];
            $timestampTo    = $timestamps['to'];
            $dtFrom         = $timestamps['dtFrom'];
            $dtTo           = $timestamps['dtTo'];

            $tick   = self::getTick($tickFormat, $dtFrom, $dtTo, $useTwoTicks);
            $value  = $this->findDataByDateRange($timestampFrom, $timestampTo);

            $graphData[$tick] = $value;

            if ($i == 0) {
                $timeRange['to'] = $dtTo->format(self::RESULT_PARAMETER_FORMAT_TIME);
            }
            if ($timestampFrom <= $timestampBoundFrom || $i == $amount - 1) {
                $timeRange['from'] = $dtFrom->format(self::RESULT_PARAMETER_FORMAT_TIME);
            }

            if ($timestampFrom <= $timestampBoundFrom) {
                break;
            }
        }

        $graphData = array_reverse($graphData);

        $result = array(
            self::RESULT_PARAMETER_GRAPH_DATA => $graphData,
            self::RESULT_PARAMETER_TIME_RANGE => $timeRange,
        );
        

        return $result;
    }

    protected static function getTick(
        $tickFormat,
        $dtFrom,
        $dtTo,
        $useTwoTicks = true
    ) {

        $tick = '';
        if ($useTwoTicks) {
            $tick = sprintf(
                self::TICK_FORMAT_TWO,
                $dtFrom->format($tickFormat),
                $dtTo->format($tickFormat)
            );
        } else {
            $tick = sprintf(
                self::TICK_FORMAT_ONE,
                $dtFrom->format($tickFormat)
            );
        }

        return $tick;
    }

    protected function findDataByDateRange(
        $timestampFrom,
        $timestampTo
    ) {

        $queryBuilder = new QueryBuilderCallSummary(
            $this->getAdvertiser(),
            $this->getCompany(),
            $this->getTzName(),
            $this->getFilterValues()
        );
        $sqlQueryString = $queryBuilder->getSqlQueryString($timestampFrom, $timestampTo);

        $result = array();
        if (!empty($sqlQueryString)) {

            $conn = Doctrine_Manager::getInstance()->getConnection('doctrine');
            $queryResult = $conn->fetchAssoc($sqlQueryString, array());
            $queryResultFirst = $queryResult[0];

            $queryResultFirst['all_calls_count']= intval($queryResultFirst['all_calls_count']);
            $queryResultFirst['callers_count']  = intval($queryResultFirst['callers_count']);

            $result['Total Callers']    = $queryResultFirst['all_calls_count'];
            $result['Unique Callers']   = $queryResultFirst['callers_count'];
            $result['Repeat Callers']   = $queryResultFirst['all_calls_count'] - $queryResultFirst['callers_count'];
        }

        return $result;
    }
}
