<?php

class QueryBuilderUniqueCallers extends QueryBuilderBase {

    public function getSqlQueryString() {

        $sqlQueryString = '';

        $advertiser = $this->getAdvertiser();
        $company    = $this->getCompany();

        if (!empty($advertiser)) {
            $sqlQueryString = $this->createQueryStringAdvertiser();
        } else {

            $companyLevel = $company->getLevel();

            switch ($companyLevel) {
                case CompanyTable::$LEVEL_BIONIC:
                    $sqlQueryString = $this->createQueryStringBionic();
                    break;
                default:
                    $sqlQueryString = $this->createQueryStringSellerCompany();
            }
        }

        return $sqlQueryString;
    }

    protected function createQueryString(
        $subSqlQueryString,
        $needToAddWhere
    ) {

        $sqlQueryString = '';

        if (!empty($subSqlQueryString)) {

            $subSqlQueryString = $this->addFilteringByTicStartTimeArr($subSqlQueryString, $needToAddWhere);

            $sqlQueryString =
                "
                    SELECT
                        COUNT(tmp.calls_count) unique_callers,
                        calls_count
                    FROM (" .
                        $subSqlQueryString .

                        " GROUP BY
                            twilio_caller_phone_number_id
                    ) AS tmp
                    GROUP BY
                        calls_count
                    ORDER BY
                        calls_count
                "
            ;
        }

        return $sqlQueryString;
    }

    protected function createQueryStringAdvertiser() {

        $advertiserId = $this->getAdvertiserId();
        $subSqlQueryString =
            "
                SELECT
                    COUNT(tic.twilio_caller_phone_number_id) calls_count
                FROM
                    twilio_incoming_call tic
                    INNER JOIN phone_call pc
                        ON
                            tic.id = pc.id
                            AND pc.advertiser_id = {$advertiserId}
            "
        ;

        return $this->createQueryString($subSqlQueryString, true);
    }

    protected function createQueryStringSellerCompany() {

        //TODO:: CompanyIncomingNumberPool: test it

        $company = $this->getCompany();

        $subSqlQueryString =
            "
                SELECT
                    COUNT(tic.twilio_caller_phone_number_id) calls_count
                FROM
                    twilio_incoming_call tic
                    INNER JOIN phone_call pc
                        ON tic.id = pc.id
                    INNER JOIN advertiser adv 
                        ON pc.advertiser_id = adv.id
                    INNER JOIN company com
                        ON
                            com.id = adv.company_id
                            AND (com.lft BETWEEN {$company->getLft()} AND {$company->getRgt()})
             "
        ;

        return $this->createQueryString($subSqlQueryString, true);
    }

    protected function createQueryStringBionic() {

        //TODO:: CompanyIncomingNumberPool: test it

        $subSqlQueryString =
            "
                SELECT
                    COUNT(tic.twilio_caller_phone_number_id) calls_count
                FROM
                    twilio_incoming_call tic
                    INNER JOIN phone_call pc
                        ON
                            pc.id = tic.id
                            AND pc.advertiser_id IS NOT NULL
             "
        ;

        return $this->createQueryString($subSqlQueryString, true);
    }
}
