<?php

class QueryBuilderCallSummary extends QueryBuilderByTzNameBase {

    public function getSqlQueryString(
        $timestampFrom,
        $timestampTo
    ) {

        $sqlQueryString = '';

        $advertiser = $this->getAdvertiser();
        $company    = $this->getCompany();

        if (!empty($advertiser)) {
            $sqlQueryString = $this->createQueryStringAdvertiser($timestampFrom, $timestampTo);
        } else {

            $companyLevel = $company->getLevel();

            switch ($companyLevel) {
                case CompanyTable::$LEVEL_BIONIC:
                    $sqlQueryString = $this->createQueryStringBionic($timestampFrom, $timestampTo);
                    break;
                default:
                    $sqlQueryString = $this->createQueryStringSellerCompany($timestampFrom, $timestampTo);
            }
        }

        return $sqlQueryString;
    }

    protected static function createQueryString(
        $subSqlQueryString,
        $timestampFrom,
        $timestampTo,
        $needToAddWhere
    ) {

        $sqlQueryString = '';

        if (!empty($subSqlQueryString)) {

            $subSqlQueryString = self::addFilteringByTicStartTime($subSqlQueryString, $timestampFrom, $timestampTo, $needToAddWhere);

            $sqlQueryString =
                "
                    SELECT
                        COUNT(tmp.caller) callers_count,
                        SUM(calls_count) all_calls_count
                    FROM (
                        $subSqlQueryString

                        GROUP BY
                            tic.twilio_caller_phone_number_id
                    ) AS tmp
                "
            ;
        }

        return $sqlQueryString;
    }

    protected function createQueryStringAdvertiser(
        $timestampFrom,
        $timestampTo
    ) {

        $advertiserId = $this->getAdvertiserId();
        $subSqlQueryString =
            "
                SELECT
                    COUNT(tic.id) calls_count,
                    tic.twilio_caller_phone_number_id caller
                FROM
                    twilio_incoming_call tic
                    INNER JOIN phone_call pc
                        ON
                            tic.id = pc.id
                            AND pc.advertiser_id = {$advertiserId}
            "
        ;

        return self::createQueryString($subSqlQueryString, $timestampFrom, $timestampTo, true);
    }

    protected function createQueryStringSellerCompany(
        $timestampFrom,
        $timestampTo
    ) {

        $company    = $this->getCompany();

        $subSqlQueryString =
            "
                SELECT
                    COUNT(tic.id) calls_count,
                    tic.twilio_caller_phone_number_id caller
                FROM
                    twilio_incoming_call tic
                    INNER JOIN phone_call pc
                        ON tic.id = pc.id
                    INNER JOIN advertiser adv
                        ON pc.advertiser_id = adv.id
                    INNER JOIN company com
                        ON
                            com.id = adv.company_id
                            AND (com.lft BETWEEN {$company->getLft()} AND {$company->getRgt()})
            "
        ;

        return self::createQueryString($subSqlQueryString, $timestampFrom, $timestampTo, true);
    }

    protected function createQueryStringBionic(
        $timestampFrom,
        $timestampTo
    ) {

        //TODO:: CompanyIncomingNumberPool: test it

        $subSqlQueryString =
            "
                SELECT
                    COUNT(tic.id) calls_count,
                    tic.twilio_caller_phone_number_id caller
                FROM
                    twilio_incoming_call tic
                    INNER JOIN phone_call pc
                        ON
                            tic.id = pc.id
                            AND pc.advertiser_id IS NOT NULL
            "
        ;

        return self::createQueryString($subSqlQueryString, $timestampFrom, $timestampTo, true);
    }
}
