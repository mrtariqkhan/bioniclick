<?php

class QueryBuilderCallsBySearchType extends QueryBuilderCallsByParameter {

    protected function createQueryStringAdvertiser() {

        $advertiserId = $this->getAdvertiserId();
        $subSqlQueryString =
            "
                SELECT
                    COUNT(tic.id) AS " . self::RESULT_VALUE_CALLS_COUNT . "
                    , IF(c.type='". self::OFFLINE."', '". self::OFFLINE."', IF (pc.phone_number_was_default or va.id IS NULL or va.analytics_time < (tic.start_time - 3600) or va.is_organic IS NULL, '". self::OTHERS."'
                    ,  va.is_organic)) AS " . self::RESULT_VALUE_BY . "
                FROM
                    twilio_incoming_call tic
                    INNER JOIN twilio_incoming_phone_number tipn ON tipn.id = tic.twilio_incoming_phone_number_id
                    INNER JOIN phone_call pc ON pc.id = tic.id
                    LEFT JOIN campaign c ON c.id = pc.campaign_id
                    LEFT JOIN visitor_analytics va ON va.id = pc.visitor_analytics_id
                WHERE
                    pc.advertiser_id = {$advertiserId}
            "
        ;

        return $this->createQueryString($subSqlQueryString, false);
    }

    protected function createQueryStringSellerCompany() {

        $company = $this->getCompany();
        $subSqlQueryString =
            "
                SELECT
                    COUNT(tic.id) AS " . self::RESULT_VALUE_CALLS_COUNT . ",
                    IF(c.type ='". self::OFFLINE."','". self::OFFLINE."', IF (pc.phone_number_was_default or va.id IS NULL or va.analytics_time < (tic.start_time - 3600) or va.is_organic IS NULL, '". self::OTHERS."', va.is_organic )) AS " . self::RESULT_VALUE_BY . "
                FROM
                    twilio_incoming_call tic
                    INNER JOIN twilio_incoming_phone_number tipn ON tipn.id = tic.twilio_incoming_phone_number_id
                    INNER JOIN phone_call pc ON pc.id = tic.id
                    LEFT JOIN campaign c ON pc.campaign_id = c.id                       
                    LEFT JOIN advertiser adv ON adv.id = pc.advertiser_id
                    INNER JOIN company com ON com.id = adv.company_id
                    LEFT JOIN visitor_analytics va ON va.id = pc.visitor_analytics_id
                WHERE
                    (com.lft BETWEEN {$company->getLft()} AND {$company->getRgt()})
            "
        ;

        return $this->createQueryString($subSqlQueryString, false);
    }

    protected function createQueryStringBionic() {

        $subSqlQueryString =
            "
                SELECT
                      COUNT(tic.id) AS " . self::RESULT_VALUE_CALLS_COUNT . "
                    , IF ( va.is_organic IS NULL ,IF(c.type ='". self::OFFLINE."','". self::OFFLINE."','". self::OTHERS."' ),va.is_organic) AS " . self::RESULT_VALUE_BY . "
                FROM
                    twilio_incoming_call tic
                    INNER JOIN twilio_incoming_phone_number tipn ON tipn.id = tic.twilio_incoming_phone_number_id
                    INNER JOIN phone_call pc ON pc.id = tic.id
                    LEFT JOIN campaign c ON pc.campaign_id = c.id                    
                    LEFT JOIN visitor_analytics va ON va.id = pc.visitor_analytics_id
            "
        ;

        return $this->createQueryString($subSqlQueryString, true);
    }
}
