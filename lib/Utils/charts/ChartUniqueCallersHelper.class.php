<?php

class ChartUniqueCallersHelper extends ChartHelperBase {

    const TICK_FORMAT_SIMPLE    = '%s';
    const TICK_FORMAT_LESS_THEN = '< %s';
    const TICK_FORMAT_MORE_THEN = '> %s';

    public function findData(
        $fromCallsCount = 1,
        $toCallsCount = 5
    ) {

        $queryBuilder = new QueryBuilderUniqueCallers(
            $this->getAdvertiser(),
            $this->getCompany(),
            $this->getFilterValues()
        );
        $sqlQueryString = $queryBuilder->getSqlQueryString();

        $queryResult = array();
        if (!empty($sqlQueryString)) {
            $conn = Doctrine_Manager::getInstance()->getConnection('doctrine');
            $queryResult = $conn->fetchAssoc($sqlQueryString, array());
        }

        return self::buildDataArray($queryResult, $fromCallsCount, $toCallsCount);
    }

    /**
     * Inserts insert "x Call" or "x Calls" into $str instead of one '%s'
     * 
     * @param number $num number in "x Call" or "x Calls"
     * @param string $str result string format. It is string with one '%s' to insert "x Call" or "x Calls".
     * @return string 
     */
    public static function getTick($num, $str = self::TICK_FORMAT_SIMPLE) {

        if ($num == 1) {
            return sprintf($str, "$num Call");
        }
        return sprintf($str, "$num Calls");
    }

    protected static function buildDataArray(
        array $queryResult,
        $fromCallsCount = 1,
        $toCallsCount = 5
    ) {

        $result = array();

        //NOTE:: initialise $result
        if ($fromCallsCount != 1) {
            $result[self::getTick($fromCallsCount, self::TICK_FORMAT_LESS_THEN)] = 0;
        }
        for ($i = $fromCallsCount; $i <= $toCallsCount; $i++) {
            $result[self::getTick($i)] = 0;
        }
        $result[self::getTick($toCallsCount, self::TICK_FORMAT_MORE_THEN)] = 0;

        //NOTE:: calculate beforeSum
        $beforeSum = null;
        if ($fromCallsCount != 1) {
            $beforeSum = 0;
            foreach ($queryResult as $key => $value) {
                if ($value['calls_count'] < $fromCallsCount) {
                    $beforeSum += $value['unique_callers'];
                    unset($queryResult[$key]);
                } else {
                    break;
                }
            }
        }
        if (!is_null($beforeSum)) {
            $result[self::getTick($fromCallsCount, self::TICK_FORMAT_LESS_THEN)] = $beforeSum;
        }

        //NOTE:: calculate values between $fromCallsCount and $toCallsCount
        foreach ($queryResult as $key => $value) {
            if ($value['calls_count'] <= $toCallsCount) {
                $result[self::getTick($value['calls_count'])] = $value['unique_callers'];
                unset($queryResult[$key]);
            } else {
                break;
            }
        }

        //NOTE:: calculate afterSum
        $afterSum = 0;
        foreach ($queryResult as $value) {
            $afterSum += $value['unique_callers'];
        }
        $result[self::getTick($toCallsCount, self::TICK_FORMAT_MORE_THEN)] = $afterSum;

//        //NOTE:: Uncomment if necessary
//        foreach ($result as $key => $value) {
//            if ($value == 0) {
//                unset($result[$key]);
//            }
//        }

        return $result;
    }
}
