<?php

class QueryBuilderCallsByUrl extends QueryBuilderCallsByParameter {

    const DIRECT_URL = 'Direct';


    protected function createQueryStringAdvertiser() {

        $advertiserId = $this->getAdvertiserId();
        $subSqlQueryString =
            "
                SELECT
                      COUNT(tic.id) AS " . self::RESULT_VALUE_CALLS_COUNT . "
                    , IF(c.type ='". self::OFFLINE."','". self::OFFLINE."', IF(pc.phone_number_was_default or va.id IS NULL or va.analytics_time < (tic.start_time - 3600), '". self::OTHERS."',  IF(
                        va.traffic_source IS NULL
                        ,IF(pg.page_url IS NULL or pg.page_url = 0,'". self::OTHERS."', CONCAT('" . CampaignTable::PROTOCOL_HTTP . "', cam.domain, '/', pg.page_url, '?cmbid=', cm.api_key))
                        , va.traffic_source
                    ))) AS " . self::RESULT_VALUE_BY . "
                FROM
                    twilio_incoming_call tic
                    INNER JOIN twilio_incoming_phone_number tipn ON tipn.id = tic.twilio_incoming_phone_number_id
                    INNER JOIN phone_call pc ON pc.id = tic.id
                    LEFT JOIN campaign c ON pc.campaign_id = c.id
                    LEFT JOIN visitor_analytics va ON va.id = pc.visitor_analytics_id
                    LEFT JOIN combo cm ON cm.id = va.combo_id
                    LEFT JOIN page pg ON pg.id = cm.page_id
                    LEFT JOIN campaign cam ON cam.id = pg.campaign_id
                WHERE
                    pc.advertiser_id = {$advertiserId}
            "
        ;

        return $this->createQueryString($subSqlQueryString, false);
    }

    protected function createQueryStringSellerCompany() {

        $company = $this->getCompany();
        $subSqlQueryString =
            "
                SELECT
                    COUNT(tic.id) AS " . self::RESULT_VALUE_CALLS_COUNT . ",
                    IF(cam.type ='". self::OFFLINE."', '". self::OFFLINE."', 
                    IF(pc.phone_number_was_default or va.analytics_time < (tic.start_time - 3600), '". self::OTHERS."', 
                    IF(va.traffic_source IS NULL, IF(pg.page_url IS NULL or pg.page_url = 0, '". self::OTHERS."', CONCAT('" . CampaignTable::PROTOCOL_HTTP . "', cam.domain, '/', pg.page_url, '?cmbid=', cm.api_key)), va.traffic_source))) AS " . self::RESULT_VALUE_BY . "
                FROM
                    twilio_incoming_call tic
                    INNER JOIN twilio_incoming_phone_number tipn ON tipn.id = tic.twilio_incoming_phone_number_id
                    INNER JOIN phone_call pc ON pc.id = tic.id
                    LEFT JOIN campaign cam ON pc.campaign_id = cam.id                   
                    INNER JOIN advertiser adv ON adv.id = pc.advertiser_id
                    INNER JOIN company com ON com.id = adv.company_id
                    LEFT JOIN visitor_analytics va ON va.id = pc.visitor_analytics_id
                    LEFT JOIN combo cm ON cm.id = va.combo_id
                    LEFT JOIN page pg ON pg.id = cm.page_id
                WHERE
                    (com.lft BETWEEN {$company->getLft()} AND {$company->getRgt()})
            "
        ;

        return $this->createQueryString($subSqlQueryString, false);
    }

    protected function createQueryStringBionic() {

        $subSqlQueryString =
            "
                SELECT
                      COUNT(tic.id) AS " . self::RESULT_VALUE_CALLS_COUNT . "
                    , IF(cam.type ='". self::OFFLINE."','". self::OFFLINE."',  IF(
                        va.traffic_source IS NULL
                        ,IF(pg.page_url IS NULL,'". self::OTHERS."', CONCAT('" . CampaignTable::PROTOCOL_HTTP . "', cam.domain, '/', pg.page_url, '?cmbid=', cm.api_key))
                        , va.traffic_source
                    )) AS " . self::RESULT_VALUE_BY . "
                FROM
                    twilio_incoming_call tic
                    INNER JOIN twilio_incoming_phone_number tipn ON tipn.id = tic.twilio_incoming_phone_number_id
                    INNER JOIN phone_call pc ON pc.id = tic.id
                    LEFT JOIN campaign cam ON pc.campaign_id = cam.id
                    LEFT JOIN visitor_analytics va ON va.id = pc.visitor_analytics_id
                    LEFT JOIN combo cm ON cm.id = va.combo_id
                    LEFT JOIN page pg ON pg.id = cm.page_id
            "
        ;

        return $this->createQueryString($subSqlQueryString, true);
    }
}
