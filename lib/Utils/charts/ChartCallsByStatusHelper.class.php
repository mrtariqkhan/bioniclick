<?php

class ChartCallsByStatusHelper extends ChartCallsByParameterHelper {
    
    public function findData(
        $undefinedIsNeed = false,
        $maxRawsCount = 10
    ) {
        $queryResult = $this->findQueryResultByParameter(
            'QueryBuilderCallsByStatus', 
            $undefinedIsNeed, 
            $maxRawsCount
        );
        return $this->buildDataArray($queryResult, $undefinedIsNeed, $maxRawsCount);
    }

    protected function buildDataArray(
        array $queryResult,
        $undefinedIsNeed,
        $maxRawsCount,
        $maxTickLen = self::MAX_TICK_LENGTH
    ) {

        $callStatusesAll = TwilioIncomingCallTable::findUsefulStatuses();

        $result = array();
        if (count($queryResult) > 0) {
            $othersCount = 0;
            $rowNum = 0;
            $i = 0;
            $parameterCountPrevious = null;
            foreach ($queryResult as $row) {
                $parameterValue = $row[QueryBuilderCallsByStatus::RESULT_VALUE_BY];
                $parameterCount = $row[QueryBuilderCallsByStatus::RESULT_VALUE_CALLS_COUNT];

                if (is_null($parameterValue)) {
                    if (!$undefinedIsNeed) {
                        continue;
                    }
                    $parameterValue = self::UNDEFINED." Call Status";
                }

                $rowNum++;
                if (is_null($maxRawsCount) || $rowNum <= $maxRawsCount) {
                    if (is_null($parameterCountPrevious) || $parameterCountPrevious != $parameterCount) {
                        $i++;
                    }
                    $tick = self::getTick($i, $parameterValue, $maxTickLen);
                    $result[$tick] = $parameterCount;
                    $parameterCountPrevious = $parameterCount;

                } else {
                    $othersCount += $parameterCount;
                }
                unset($callStatusesAll[$parameterValue]);
            }

            foreach ($callStatusesAll as $parameterValue) {
                if ($rowNum++ < $maxRawsCount) {
                    $tick = self::getTick($i, $parameterValue, $maxTickLen);
                    $result[$tick] = 0;
                    $i++;

                }
            }

            if ($othersCount > 0) {
                $result[self::OTHERS_RAW_NAME] = $othersCount;
            }
        }

        return $result;
    }
}
