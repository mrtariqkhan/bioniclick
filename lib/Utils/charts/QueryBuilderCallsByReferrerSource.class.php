<?php

class QueryBuilderCallsByReferrerSource extends QueryBuilderCallsByParameter {

    const DIRECT_REFERRER_SOURCE = 'Direct';

    protected function createQueryStringAdvertiser() {

        $advertiserId = $this->getAdvertiserId();
        $subSqlQueryString =
            "
                SELECT
                      COUNT(tic.id) AS " . self::RESULT_VALUE_CALLS_COUNT . "
                      ,IF(c.type ='". self::OFFLINE."' ,'". self::OFFLINE."', IF(pc.phone_number_was_default, '". self::OTHERS."', IF(va.id IS NULL, '". self::OTHERS."', 
                      IF(va.analytics_time < (tic.start_time - 3600), '". self::OTHERS."', IF(va.is_organic = '2', '". self::DIRECT."'
                      ,IF(rc.url IS NULL, '". self::OTHERS."', rc.url)))))) AS " . self::RESULT_VALUE_BY . "
                FROM
                    twilio_incoming_call tic
                    INNER JOIN twilio_incoming_phone_number tipn ON tipn.id = tic.twilio_incoming_phone_number_id
                    INNER JOIN phone_call pc ON pc.id = tic.id
                    LEFT JOIN campaign c ON c.id = pc.campaign_id
                    LEFT JOIN visitor_analytics va ON va.id = pc.visitor_analytics_id
                    LEFT JOIN referrer_source rc ON rc.id = va.referrer_source_id
                WHERE
                    pc.advertiser_id = {$advertiserId}
            "
        ;

        return $this->createQueryString($subSqlQueryString, false);
    }

    protected function createQueryStringSellerCompany() {

        $company = $this->getCompany();
        $subSqlQueryString =
            "
                SELECT
                      COUNT(tic.id) AS " . self::RESULT_VALUE_CALLS_COUNT . "
                    , IF (c.type='". self::OFFLINE."', '". self::OFFLINE."', 
                    IF (pc.phone_number_was_default or va.id IS NULL or va.analytics_time < (tic.start_time - 3600), '". self::OTHERS."', 
                    IF(va.is_organic ='" . VisitorAnalyticsTable::TYPE_DIRECT . "', '" . self::DIRECT_REFERRER_SOURCE . "' ,
                    IF(rc.url IS NULL, '". self::OTHERS."', rc.url)))) AS " . self::RESULT_VALUE_BY . "
                FROM
                    twilio_incoming_call tic
                    INNER JOIN twilio_incoming_phone_number tipn ON tipn.id = tic.twilio_incoming_phone_number_id
                    INNER JOIN phone_call pc ON pc.id = tic.id
                    LEFT JOIN campaign c ON c.id = pc.campaign_id
                    LEFT JOIN advertiser adv ON adv.id = pc.advertiser_id
                    LEFT JOIN company com ON com.id = adv.company_id
                    LEFT JOIN visitor_analytics va ON va.id = pc.visitor_analytics_id
                    LEFT JOIN referrer_source rc ON rc.id = va.referrer_source_id
                WHERE
                    (com.lft BETWEEN {$company->getLft()} AND {$company->getRgt()})
            "
        ;

        return $this->createQueryString($subSqlQueryString, false);
    }

    protected function createQueryStringBionic() {

        $subSqlQueryString =
            "
                SELECT
                      COUNT(tic.id) AS " . self::RESULT_VALUE_CALLS_COUNT . "
                    ,IF(c.type ='" . self::OFFLINE . "' ,'" . self::OFFLINE . "', 
                    IF(
                        va.is_organic = '" . VisitorAnalyticsTable::TYPE_DIRECT . "'
                        , '" . self::DIRECT_REFERRER_SOURCE . "'
                        ,IF(rc.url IS NULL, 'Others',rc.url)
                    ) )AS " . self::RESULT_VALUE_BY . "
                FROM
                    twilio_incoming_call tic
                    INNER JOIN twilio_incoming_phone_number tipn ON tipn.id = tic.twilio_incoming_phone_number_id
                    INNER JOIN phone_call pc ON pc.id = tic.id
                    LEFT JOIN campaign c ON pc.campaign_id = c.id
                    LEFT JOIN visitor_analytics va ON va.id = pc.visitor_analytics_id
                    LEFT JOIN referrer_source rc ON rc.id = va.referrer_source_id
            "
        ;

        return $this->createQueryString($subSqlQueryString, true);
    }
}
