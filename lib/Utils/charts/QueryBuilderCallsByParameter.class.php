<?php

abstract class QueryBuilderCallsByParameter extends QueryBuilderByTzDifferenceBase {

    const TYPE_BY_STATUS            = 'by status';
    const TYPE_BY_REFERRER_SOURCE   = 'by referrer source';
    const TYPE_BY_SEARCH_TYPE       = 'by search type';
    const TYPE_BY_URL               = 'by url';
    const TYPE_BY_KEYWORD           = 'by keyword';
    const OFFLINE                   = 'Offline';
    const OTHERS                   = 'Others';
    const DIRECT                   = 'Direct';    
    const PPC                   = 'PPC';    

    protected $visitorAnalyticsTypeCode   = null;

    const RESULT_VALUE_CALLS_COUNT  = 'calls_count';
    const RESULT_VALUE_BY           = 'by_parameter';


    /**
     *
     * @param type $advertiser null or Advertiser
     * @param type $company null or Company
     * @param string $tzDiff time offset from need $timezone to GMT0. For example: '-0500', '+0700'
     * @param array $filterValues
     */
    public function __construct(
        $advertiser,
        $company,
        $tzDiff,
        $filterValues,
        $visitorAnalyticsTypeCode = VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE
    ) {

        parent::__construct($advertiser, $company, $filterValues, $filterValues);

        $this->setVisitorAnalyticsTypeCode($visitorAnalyticsTypeCode);
    }
    
    public function getSqlQueryString() {

        $advertiser = $this->getAdvertiser();
        $company    = $this->getCompany();

        $sqlQueryString = '';

        if (!empty($advertiser)) {
            $sqlQueryString = $this->createQueryStringAdvertiser();
        } else {

            $companyLevel = $company->getLevel();

            switch ($companyLevel) {
                case CompanyTable::$LEVEL_AGENCY:
                case CompanyTable::$LEVEL_RESELLER:
                    $sqlQueryString = $this->createQueryStringSellerCompany();
                    break;
                case CompanyTable::$LEVEL_BIONIC:
                    $sqlQueryString = $this->createQueryStringBionic();
                    break;
                default:
            }
        }

        return $sqlQueryString;
    }

    protected function createQueryString(
        $subSqlQueryString,
        $needToAddWhere,
        $isVisitorAnalyticsParameter = true
    ) {

        $sqlQueryString = '';

        if (!empty($subSqlQueryString)) {

            $subSqlQueryString = $this->addFilteringByTicStartTimeArr(
                $subSqlQueryString, 
                $needToAddWhere
            );

            if ($isVisitorAnalyticsParameter) {
                $vaTypeCode = $this->getVisitorAnalyticsTypeCode();
                switch ($vaTypeCode) {
                    case VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE:
                    case VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE_BUT_OLD:
                    case VisitorAnalyticsTable::TYPE_CODE_NO_SUITABLE:
                    case VisitorAnalyticsTable::TYPE_CODE_CALL_TO_DEFAULT:
                        /*$subSqlQueryString .= 
                            ' ' 
                            . ($needToAddWhere ? 'WHERE' : 'AND')
                            . ' (' . VisitorAnalyticsTable::createConditionToSwitchTypeCode() . " = $vaTypeCode)"
                        ;*/
                        break;
                    default:
                }
            }

            $sqlQueryString =
                "
                SELECT
                      tmp." . self::RESULT_VALUE_CALLS_COUNT. "
                    , tmp." . self::RESULT_VALUE_BY. "
                FROM (
                    $subSqlQueryString

                    GROUP BY
                        " . self::RESULT_VALUE_BY. "
                ) AS tmp

                ORDER BY
                      tmp." . self::RESULT_VALUE_CALLS_COUNT. " DESC
                    , tmp." . self::RESULT_VALUE_BY. " ASC
                "
            ;
        }
        //print_r($sqlQueryString);
        return $sqlQueryString;
    }

    abstract protected function createQueryStringAdvertiser();
    abstract protected function createQueryStringSellerCompany();
    abstract protected function createQueryStringBionic(); 


    public function getVisitorAnalyticsTypeCode() {
        return $this->visitorAnalyticsTypeCode;
    }


    public function setVisitorAnalyticsTypeCode($visitorAnalyticsTypeCode) {
        $this->visitorAnalyticsTypeCode = $visitorAnalyticsTypeCode;
    }

}
