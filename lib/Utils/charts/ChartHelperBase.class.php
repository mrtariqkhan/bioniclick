<?php

class ChartHelperBase extends ChartFilteredHelper {

    protected $advertiser   = null;
    protected $company      = null;


    /**
     *
     * @param type $advertiser null or Advertiser
     * @param type $company null or Company
     * @param array $filterValues
     */
    public function __construct(
        $advertiser,
        $company,
        $filterValues
    ) {

        parent::__construct($filterValues);

        $this->setAdvertiser($advertiser);
        $this->setCompany($company);
    }


    public function getAdvertiser() {
        return $this->advertiser;
    }

    public function getCompany() {
        return $this->company;
    }


    public function setAdvertiser($advertiser) {
        $this->advertiser = $advertiser;
    }

    public function setCompany($company) {
        $this->company = $company;
    }

}
