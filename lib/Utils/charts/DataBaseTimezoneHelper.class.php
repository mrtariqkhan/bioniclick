<?php

class DataBaseTimezoneHelper {

    const DEFAULT_OFFSET        = '+00:00';
    const PATTERN_DB_DIFF_TIME  = '/^([+-])?(\d\d):(\d\d):(\d\d)$/';
    const PATTERN_OFFSET        = '/^([+-])?(\d\d)(\d\d)$/';

    public static function findOffset() {

        $result = self::DEFAULT_OFFSET;

        $sqlQueryString = 'SELECT TIMEDIFF(NOW(), UTC_TIMESTAMP()) AS currnet_offset';

        $conn = Doctrine_Manager::getInstance()->getConnection('doctrine');
        $queryResult = $conn->fetchOne($sqlQueryString, array());

        if (!empty($queryResult)) {
            $diffTime = $queryResult;

            $matches = array();
            if (preg_match(self::PATTERN_DB_DIFF_TIME, $diffTime, $matches)) {
                $sign   = $matches[1];
                $hours  = $matches[2];
                $minutes= $matches[3];

                $sign = empty($sign) ? '+' : $sign;

                $result = "{$sign}{$hours}:{$minutes}";
            }
        }

        return $result;
    }

    /**
     *
     * @param int $hours
     * @return string offset 
     */
    public static function convertOffset($offset) {

        $result = self::DEFAULT_OFFSET;

        $matches = array();
        if (preg_match(self::PATTERN_OFFSET, $offset, $matches)) {
            $sign   = $matches[1];
            $hours  = $matches[2];
            $minutes= $matches[3];

            $sign = empty($sign) ? '+' : $sign;

            $result = "{$sign}{$hours}:{$minutes}";
        }

        return $result;
    }

    /**
     *
     * @param string $unixDateString
     * @param string $needTzDiff
     * @return string 
     */
    public static function configureConvertedFromUnixDate($unixDateString, $needTzDiff) {

        $offset     = self::findOffset();
        $needOffset = self::convertOffset($needTzDiff);

        if ($offset !== $needOffset) {
            return "CONVERT_TZ($unixDateString, '$offset', '$needOffset')";
        }

        return $unixDateString;
    }

}
