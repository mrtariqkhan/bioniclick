<?php

class QueryBuilderTopCustomers extends QueryBuilderBase {
    
    public function getSqlQueryString() {

        $sqlQueryString = '';

        $advertiser = $this->getAdvertiser();
        $company    = $this->getCompany();

        if (!empty($advertiser)) {
            $sqlQueryString = $this->createQueryStringAdvertiser();
        } else {

            $companyLevel = $company->getLevel();

            switch ($companyLevel) {
                case CompanyTable::$LEVEL_AGENCY:
                    $sqlQueryString = $this->createQueryStringAgency();
                    break;
                case CompanyTable::$LEVEL_RESELLER:
                    $sqlQueryString = $this->createQueryStringReseller();
                    break;
                case CompanyTable::$LEVEL_BIONIC:
                    $sqlQueryString = $this->createQueryStringBionic();
                    break;
                default:
            }
        }

        return $sqlQueryString;
    }

    protected function createQueryString(
        $subSqlQueryString,
        $needToAddWhere
    ) {

        $sqlQueryString = '';

        if (!empty($subSqlQueryString)) {

            $subSqlQueryString = $this->addFilteringByTicStartTimeArr($subSqlQueryString, $needToAddWhere);

            $sqlQueryString =
                "
                SELECT
                      tmp.calls_count
                    , tmp.name
                    , tmp.customer_kind
                FROM (
                    $subSqlQueryString

                    GROUP BY
                        id, name
                    HAVING
                        calls_count > 0
                ) AS tmp

                ORDER BY
                    tmp.calls_count DESC
                "
            ;
        }

        return $sqlQueryString;
    }

    protected function createQueryStringAdvertiser() {

        $advertiserId = $this->getAdvertiserId();

        $campaignKindCondition = $this->configureCampaignCondition();
        $campaignKindPartWhere  = empty($campaignKindCondition) 
            ? '' 
            : "AND $campaignKindCondition"
        ;

        $subSqlQueryString =
            "
                SELECT
                      COUNT(tic.id) AS calls_count
                    , cam.id AS id
                    , cam.name AS name
                    , 'campaign' AS customer_kind
                FROM
                    twilio_incoming_call tic
                    INNER JOIN twilio_incoming_phone_number tipn ON tipn.id = tic.twilio_incoming_phone_number_id
                    INNER JOIN phone_call pc ON pc.id = tic.id
                    LEFT JOIN campaign cam ON cam.id = pc.campaign_id
                WHERE
                    pc.advertiser_id = {$advertiserId}
                    $campaignKindPartWhere
            "
        ;

        return $this->createQueryString($subSqlQueryString, false);
    }

    protected function createQueryStringAgency() {

        //TODO:: CompanyIncomingNumberPool: test it
        $agencyCompanyId = $this->getCompanyId();

        $campaignKindCondition = $this->configureCampaignCondition();
        $campaignKindPartWhere  = empty($campaignKindCondition) 
            ? '' 
            : "AND $campaignKindCondition"
        ;
        $campaignKindPartFrom   = empty($campaignKindCondition) 
            ? '' 
            : "INNER JOIN campaign cam ON cam.id = pc.campaign_id"
        ;

//        $subSqlQueryString =
//            "
//                SELECT
//                      COUNT(tic.id) AS calls_count
//                    , adv.id AS id
//                    , adv.name AS name
//                    , 'advertiser' AS customer_kind
//                FROM
//                    twilio_incoming_call tic
//                    INNER JOIN twilio_incoming_phone_number tipn ON tipn.id = tic.twilio_incoming_phone_number_id
//                    INNER JOIN advertiser_incoming_number_pool ainp ON tipn.id = ainp.twilio_incoming_phone_number_id
//                    INNER JOIN advertiser adv ON adv.id = ainp.advertiser_id
//                WHERE
//                    adv.company_id = $agencyCompanyId
//            "
//        ;

        $subSqlQueryString =
            "
                SELECT
                      COUNT(tic.id) AS calls_count
                    , tmpUnion.id AS id
                    , tmpUnion.name AS name
                    , tmpUnion.customer_kind AS customer_kind
                FROM
                    twilio_incoming_call tic
                    INNER JOIN twilio_incoming_phone_number tipn ON tipn.id = tic.twilio_incoming_phone_number_id
                    INNER JOIN (

                        /* calls to Advertisers of Agency: */
                        (
                            SELECT
                                  pc.id AS pc_id
                                , adv.id AS id
                                , adv.name AS name
                                , 'advertiser' AS customer_kind
                            FROM
                                phone_call pc
                                INNER JOIN advertiser adv ON adv.id = pc.advertiser_id
                                $campaignKindPartFrom
                            WHERE
                                adv.company_id = $agencyCompanyId
                                $campaignKindPartWhere
                        )
                    ) tmpUnion ON tmpUnion.pc_id = tic.id
            "
        ;

        return $this->createQueryString($subSqlQueryString, false);
    }

    protected function createQueryStringReseller() {

        //TODO:: CompanyIncomingNumberPool: test it
        $agencyCompanyLevel = CompanyTable::$LEVEL_AGENCY;
        $company = $this->getCompany();
        $companyId = $company->getId();

        $campaignKindCondition = $this->configureCampaignCondition();
        $campaignKindPartWhere  = empty($campaignKindCondition) 
            ? '' 
            : "AND $campaignKindCondition"
        ;
        $campaignKindPartFrom   = empty($campaignKindCondition) 
            ? '' 
            : "INNER JOIN campaign cam ON cam.id = pc.campaign_id"
        ;

        $subSqlQueryString =
            "
                SELECT
                      COUNT(tic.id) AS calls_count
                    , tmpUnion.id AS id
                    , tmpUnion.name AS name
                    , tmpUnion.customer_kind AS customer_kind
                FROM
                    twilio_incoming_call tic
                    INNER JOIN twilio_incoming_phone_number tipn ON tipn.id = tic.twilio_incoming_phone_number_id
                    INNER JOIN (

                        /* calls to Advertisers of Reseller: */
                        (
                            SELECT
                                  pc.id AS pc_id
                                , adv.id AS id
                                , adv.name AS name
                                , 'advertiser' AS customer_kind
                            FROM
                                phone_call pc
                                INNER JOIN advertiser adv ON adv.id = pc.advertiser_id
                                $campaignKindPartFrom
                            WHERE
                                adv.company_id = $companyId
                                $campaignKindPartWhere
                        )

                        /* calls to Agencies of Reseller: */
                        UNION
                        (
                            SELECT
                                  tmpCustomerCompanyCalls.pc_id AS pc_id
                                , tmpCustomerCompanyCalls.id AS id
                                , tmpCustomerCompanyCalls.name AS name
                                , 'company' AS customer_kind
                            FROM
                                (

                                    /* calls to Advertisers of Agencies of Reseller: */
                                    (
                                        SELECT
                                              pc.id AS pc_id
                                            , com.id AS id
                                            , com.name AS name
                                        FROM
                                            phone_call pc
                                            INNER JOIN advertiser adv ON adv.id = pc.advertiser_id
                                            INNER JOIN company com ON com.id = adv.company_id
                                            $campaignKindPartFrom
                                        WHERE
                                                (com.lft BETWEEN {$company->getLft()} AND {$company->getRgt()})
                                            AND (com.level = $agencyCompanyLevel)
                                            $campaignKindPartWhere
                                    )
                                ) tmpCustomerCompanyCalls
                        )
                    ) tmpUnion ON tmpUnion.pc_id = tic.id
            "
        ;

        return $this->createQueryString($subSqlQueryString, true);
    }

    protected function createQueryStringBionic() {

        //TODO:: CompanyIncomingNumberPool: test it

        $resellerCustomersLevel = CompanyTable::$LEVEL_RESELLER;
        $agencyCompanyLevel = CompanyTable::$LEVEL_AGENCY;

        $company = $this->getCompany();
        $companyId = $company->getId();

        $campaignKindCondition = $this->configureCampaignCondition();
        $campaignKindPartWhere  = empty($campaignKindCondition) 
            ? '' 
            : "AND $campaignKindCondition"
        ;
        $campaignKindPartWhereFull  = empty($campaignKindCondition) 
            ? '' 
            : "WHERE $campaignKindCondition"
        ;
        $campaignKindPartFrom   = empty($campaignKindCondition) 
            ? '' 
            : "INNER JOIN campaign cam ON cam.id = pc.campaign_id"
        ;

        $partUnassignedBionic = empty($campaignKindCondition) 
            ?
                "
                        UNION
                        (
                            SELECT
                                  pc.id AS pc_id
                                , NULL AS id
                                , NULL AS name
                                , NULL AS customer_kind
                            FROM
                                phone_call pc
                            WHERE
                                pc.advertiser_id IS NULL
                        )
                " 
            : ''
        ;

        $subSqlQueryString =
            "
                SELECT
                      COUNT(tic.id) AS calls_count
                    , tmpUnion.id AS id
                    , tmpUnion.name AS name
                    , tmpUnion.customer_kind AS customer_kind
                FROM
                    twilio_incoming_call tic
                    INNER JOIN twilio_incoming_phone_number tipn ON tipn.id = tic.twilio_incoming_phone_number_id
                    INNER JOIN (

                        /* calls to Advertisers of Bionic: */
                        (
                            SELECT
                                  pc.id AS pc_id
                                , adv.id AS id
                                , adv.name AS name
                                , 'advertiser' AS customer_kind
                            FROM
                                phone_call pc
                                INNER JOIN advertiser adv ON adv.id = pc.advertiser_id
                                $campaignKindPartFrom
                            WHERE
                                adv.company_id = $companyId
                                $campaignKindPartWhere
                        )

                        /* calls to Bionic (Unassignment): */
                        $partUnassignedBionic

                        /* calls to Resellers of Bionic: */
                        UNION
                        (

                            SELECT
                                  tmpCustomerCompanyCalls.pc_id AS pc_id
                                , tmpCustomerCompanyCalls.id AS id
                                , tmpCustomerCompanyCalls.name AS name
                                , 'company' AS customer_kind
                            FROM
                                (
                                    (
                                        SELECT
                                              tmpResellersCalls.pc_id AS pc_id
                                            , com.id AS id
                                            , com.name AS name
                                        FROM
                                            company com
                                            LEFT JOIN (

                                                /* calls to Advertisers of Resellers of Bionic: */
                                                (
                                                    SELECT
                                                          pc.id AS pc_id
                                                        , adv.company_id AS company_id
                                                    FROM
                                                        phone_call pc
                                                        INNER JOIN advertiser adv ON adv.id = pc.advertiser_id
                                                        $campaignKindPartFrom
                                                    $campaignKindPartWhereFull
                                                )
                                            ) tmpResellersCalls ON tmpResellersCalls.company_id = com.id
                                        WHERE
                                                (com.lft BETWEEN {$company->getLft()} AND {$company->getRgt()})
                                            AND (com.level = $resellerCustomersLevel)
                                    )
                                    UNION
                                    (
                                        SELECT
                                              tmpResellersAgenciesCalls.pc_id AS pc_id
                                            , com.id AS id
                                            , com.name AS name
                                        FROM
                                            company com
                                            LEFT JOIN (

                                                /* calls to Advertisers of Agencies of Resellers of Bionic: */
                                                (
                                                    SELECT
                                                          pc.id AS pc_id
                                                        , com.id AS company_id
                                                        , com.lft AS company_lft
                                                        , com.level AS company_level
                                                    FROM
                                                        phone_call pc
                                                        INNER JOIN advertiser adv ON adv.id = pc.advertiser_id
                                                        INNER JOIN company com ON com.id = adv.company_id
                                                        $campaignKindPartFrom
                                                    $campaignKindPartWhereFull
                                                )
                                            ) tmpResellersAgenciesCalls
                                                ON
                                                    (tmpResellersAgenciesCalls.company_lft BETWEEN com.lft AND com.rgt)
                                                AND (tmpResellersAgenciesCalls.company_level = $agencyCompanyLevel)
                                        WHERE
                                                (com.lft BETWEEN {$company->getLft()} AND {$company->getRgt()})
                                            AND (com.level = $resellerCustomersLevel)
                                    )
                                ) tmpCustomerCompanyCalls
                        )
                    ) tmpUnion ON tmpUnion.pc_id = tic.id
            "
        ;

        return $this->createQueryString($subSqlQueryString, true);
    }

    protected function configureCampaignCondition() {

        $condition = '';

        $campaignKindCode = $this->getFilterValue('kind');

        if ($campaignKindCode != CampaignTable::KIND_CODE_ALL) {
            $campaignKind = CampaignTable::getKindByCode($campaignKindCode);
            if (is_null($campaignKind)) {
                $condition = "cam.kind IS NULL";
            } else {
                $condition = "cam.kind LIKE '$campaignKind'";
            }
        }

        return $condition;
    }
}
