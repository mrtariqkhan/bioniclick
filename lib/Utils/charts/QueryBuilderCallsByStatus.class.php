<?php

class QueryBuilderCallsByStatus extends QueryBuilderCallsByParameter {

    protected function createQueryStringAdvertiser() {

        $advertiserId = $this->getAdvertiserId();
        $subSqlQueryString =
            "
                SELECT
                      COUNT(tic.id) AS " . self::RESULT_VALUE_CALLS_COUNT . "
                    , IF(trc.id IS NOT NULL, trc.call_status, tic.call_status) AS " . self::RESULT_VALUE_BY . "
                FROM
                    twilio_incoming_call tic
                    INNER JOIN twilio_incoming_phone_number tipn ON tipn.id = tic.twilio_incoming_phone_number_id
                    INNER JOIN phone_call pc ON pc.id = tic.id
                    LEFT JOIN campaign cam ON cam.id = pc.campaign_id
                    LEFT JOIN twilio_redialed_call trc ON trc.twilio_incoming_call_id = tic.id
                WHERE
                    pc.advertiser_id = {$advertiserId}
            "
        ;

        return $this->createQueryString($subSqlQueryString, false, false);
    }

    protected function createQueryStringSellerCompany() {

        $company = $this->getCompany();
        $subSqlQueryString =
            "
                SELECT
                      COUNT(tic.id) AS " . self::RESULT_VALUE_CALLS_COUNT . "
                    , IF(trc.id IS NOT NULL, trc.call_status, tic.call_status) AS " . self::RESULT_VALUE_BY . "
                FROM
                    twilio_incoming_call tic
                    INNER JOIN twilio_incoming_phone_number tipn ON tipn.id = tic.twilio_incoming_phone_number_id
                    INNER JOIN phone_call pc ON pc.id = tic.id
                    INNER JOIN advertiser adv ON adv.id = pc.advertiser_id
                    INNER JOIN company com ON com.id = adv.company_id
                    LEFT JOIN twilio_redialed_call trc ON trc.twilio_incoming_call_id = tic.id
                WHERE
                    (com.lft BETWEEN {$company->getLft()} AND {$company->getRgt()})
            "
        ;

        return $this->createQueryString($subSqlQueryString, false, false);
    }

    protected function createQueryStringBionic() {

        $subSqlQueryString =
            "
                SELECT
                      COUNT(tic.id) AS " . self::RESULT_VALUE_CALLS_COUNT . "
                    , IF(trc.id IS NOT NULL, trc.call_status, tic.call_status) AS " . self::RESULT_VALUE_BY . "
                FROM
                    twilio_incoming_call tic
                    INNER JOIN twilio_incoming_phone_number tipn ON tipn.id = tic.twilio_incoming_phone_number_id
                    INNER JOIN phone_call pc ON pc.id = tic.id
                    LEFT JOIN twilio_redialed_call trc ON trc.twilio_incoming_call_id = tic.id
            "
        ;

        return $this->createQueryString($subSqlQueryString, true, false);
    }
}
