<?php

class ChartCallStatusHelper extends ChartHelperByTzDifferenceBase {

    const AMOUNT_PER_DAY        = 31;
    const AMOUNT_PER_WEEK_DAY   = 7;
    const AMOUNT_PER_HOUR       = 24;

    const WEEK_DAY_SUNDAY       = 'Sunday';
    const WEEK_DAY_MONDAY       = 'Monday';
    const WEEK_DAY_TUESDAY      = 'Tuesday';
    const WEEK_DAY_WEDNESDAY    = 'Wednesday';
    const WEEK_DAY_THURSDAY     = 'Thursday';
    const WEEK_DAY_FRIDAY       = 'Friday';
    const WEEK_DAY_SATURDAY     = 'Saturday';

    public function findData() {

        $filterValues = $this->getFilterValues();

        $type = array_key_exists('per_type', $filterValues) ? $filterValues['per_type'] : null;
        switch ($type) {
            case 'per_week_day':
                $result = $this->findDataPerWeekDay($filterValues);
                break;
            case 'per_hour':
                $result = $this->findDataPerHour($filterValues);
                break;
            default:
                //NOTE:: 'per_day' or default
                $result = $this->findDataPerDay($filterValues);
        }

        return $result;
    }



    protected function findDataPerHour() {
        return $this->findDataPerType(
            QueryBuilderCallStatus::TYPE_PER_HOUR
        );
    }

    protected function findDataPerWeekDay() {
        return $this->findDataPerType(
            QueryBuilderCallStatus::TYPE_PER_WEEK_DAY
        );
    }

    protected function findDataPerDay() {
        return $this->findDataPerType(
            QueryBuilderCallStatus::TYPE_PER_DAY
        );
    }

    /**
     *
     * @param string $type one from self::TYPE_PER_*
     * @return array
     */
    protected function findDataPerType(
        $type
    ) {

        $queryBuilder = new QueryBuilderCallStatus(
            $this->getAdvertiser(),
            $this->getCompany(),
            $this->getTzDiff(),
            $this->getFilterValues()
        );
//        $query = $queryBuilder->getQuery($type);
//
//        $queryResult = $query->fetchArray();

        $sqlQueryString = $queryBuilder->getSqlQueryString($type);
        unset($queryBuilder);

        $queryResult = array();
        if (!empty($sqlQueryString)) {
            $conn = Doctrine_Manager::getInstance()->getConnection('doctrine');
            $queryResult = $conn->fetchAssoc($sqlQueryString, array());
        }

        return self::buildDataArray($queryResult, $type);
    }

    protected static function buildDataArray(
        array $queryResult, 
        $type
    ) {

        $callStatusesArrayEmpty = array();
        $callStatusesAll = TwilioIncomingCallTable::findUsefulStatuses();
        foreach ($callStatusesAll as $callStatus) {
            $callStatusesArrayEmpty[$callStatus] = 0;
        }

        switch ($type) {
            case QueryBuilderCallStatus::TYPE_PER_WEEK_DAY:
                $periodNames = array(
                    self::WEEK_DAY_SUNDAY,
                    self::WEEK_DAY_MONDAY,
                    self::WEEK_DAY_TUESDAY,
                    self::WEEK_DAY_WEDNESDAY,
                    self::WEEK_DAY_THURSDAY,
                    self::WEEK_DAY_FRIDAY,
                    self::WEEK_DAY_SATURDAY
                );
                break;
            case QueryBuilderCallStatus::TYPE_PER_HOUR:
                $periodNames = array();
                for ($i = 0; $i < self::AMOUNT_PER_HOUR; ++$i) {
                    $periodNames[] = $i;
                }
                break;
            default:
                //as QueryBuilderCallStatus::TYPE_PER_DAY:
                $periodNames = array();
                for ($i = 1; $i <= self::AMOUNT_PER_DAY; ++$i) {
                    $periodNames[] = $i;
                }
        }

        $graphData = array();
        foreach ($periodNames as $periodName) {
            $tick = self::getTick($type, $periodName);
            $graphData[$tick] = $callStatusesArrayEmpty;
        }

//        $sum = $callStatusesArrayEmpty;
        if (count($queryResult) > 0) {
            foreach ($queryResult as $row) {
                $countValue = $row['calls_amount'];
                $periodName = $row['period_name'];
                $callStatus = $row['call_status'];

                $tick = self::getTick($type, $periodName);
                $graphData[$tick][$callStatus] = $countValue;

//                $sum[$callStatus] += $countValue;
            }
        }

        return array(
            self::RESULT_PARAMETER_GRAPH_DATA => $graphData,
        );
    }

    protected static function getTick(
        $type,
        $periodName
    ) {

        switch ($type) {
            case QueryBuilderCallStatus::TYPE_PER_WEEK_DAY:
                $tick = $periodName;
                break;
            case QueryBuilderCallStatus::TYPE_PER_HOUR:
                $periodName = str_pad($periodName, 2, '0', STR_PAD_LEFT);
                $tick = "$periodName hour";
                break;
            default:
                //as QueryBuilderCallStatus::TYPE_PER_DAY:
                $tick = "$periodName day";
        }

        return "$tick ";
    }
}
