<?php

class QueryBuilderCallStatus extends QueryBuilderByTzDifferenceBase {

    const TYPE_PER_DAY          = 'Day';
    const TYPE_PER_WEEK_DAY     = 'WeekDay';
    const TYPE_PER_HOUR         = 'Hour';


      //TODO:: think why doesn't work with Doctrine. Result contains one raw always.
      //Use getSqlQueryString method instead yet
    public function getQuery(
        $type = self::TYPE_PER_DAY
    ) {

        $alias = 'tic';
        $periodInfo = $this->getPeriodInfoToSelect($type, $alias);

        $query = TwilioIncomingCallTable::getInstance()->createQuery($alias)
            ->innerJoin("$alias.TwilioIncomingPhoneNumber tipn")
            ->innerJoin("$alias.PhoneCall pc")
            ->select("COUNT($alias.id) calls_amount")
            ->addSelect("$alias.call_status")
            ->addSelect("{$periodInfo['num']} period_num")
            ->addSelect("{$periodInfo['name']} period_name")
            ->groupBy('period_num')
            ->addGroupBy('call_status')
            ->orderBy('period_num ASC')
        ;

        $query = $this->addFilteringByTicStartTimeArrToQuery($query);

        $advertiser = $this->getAdvertiser();
        $company    = $this->getCompany();

        if (!empty($advertiser)) {
            $advertiserId = $advertiser->getId();
            $query
                ->andWhere("pc.advertiser_id = $advertiserId")
            ;
        } else {
            if ($company->isBionicCompany()) {
                $query
                    ->innerJoin('pc.Advertiser adv')
                ;
            } else {
                $query
                    ->innerJoin('pc.Advertiser adv')
                    ->innerJoin('adv.Company com')
                    ->andWhere("com.lft BETWEEN {$company->getLft()} AND {$company->getRgt()}")
                ;
            }
        }

        return $query;
    }

    /**
     *
     * Method works with calls to campaign, to advertiser. Method Does not work with calls to Bionicclick
     * 
     * @param string $type one of self::TYPE_PER_*
     * @return type 
     */
    public function getSqlQueryString(
        $type = self::TYPE_PER_DAY
    ) {

        $alias = 'tic';
        $periodInfo = $this->getPeriodInfoToSelect($type, $alias);

        $sqlQueryString = 
                "
SELECT 
	$alias.id AS id,
	IF(trc.id IS NOT NULL, trc.call_status, $alias.call_status) AS call_status, 
	COUNT($alias.id) AS calls_amount, 
	{$periodInfo['num']} AS period_num, 
	{$periodInfo['name']} AS period_name 
FROM 
	twilio_incoming_call $alias
	INNER JOIN twilio_incoming_phone_number tipn ON $alias.twilio_incoming_phone_number_id = tipn.id 
	INNER JOIN phone_call pc ON $alias.id = pc.id 
	LEFT JOIN twilio_redialed_call trc ON $alias.id = trc.twilio_incoming_call_id 
        ";

        $advertiser = $this->getAdvertiser();
        $company    = $this->getCompany();

        if (!empty($advertiser)) {
            $advertiserId = $advertiser->getId();
        $sqlQueryString .= "
WHERE 
    pc.advertiser_id = $advertiserId
        ";
        } else {
            if ($company->isBionicCompany()) {
                $sqlQueryString .= "
        INNER JOIN advertiser adv ON adv.id = pc.advertiser_id 
                ";
            } else {
                $sqlQueryString .= "
	INNER JOIN advertiser adv ON adv.id = pc.advertiser_id 
	INNER JOIN company com ON com.id = adv.company_id 
WHERE 
    com.lft BETWEEN {$company->getLft()} AND {$company->getRgt()}
                ";
            }
        }

        $needToAddWhere = false;
        $sqlQueryString = $this->addFilteringByTicStartTimeArr($sqlQueryString, $needToAddWhere);
        $sqlQueryString .= "
GROUP BY 
          period_num
	, call_status 
ORDER BY 
	period_num ASC
        ";

        return $sqlQueryString;
    }

    /**
     *
     * @param string $type one of self::TYPE_PER_*
     * @param string $alias
     * @return type 
     */
    protected function getPeriodInfoToSelect(
        $type,
        $alias
    ) {

        $needTzDiff = $this->getTzDiff();

        $alias = 'tic';
        $unixDateString = "FROM_UNIXTIME($alias.start_time)";
        $dateString = DataBaseTimezoneHelper::configureConvertedFromUnixDate($unixDateString, $needTzDiff);

        switch ($type) {
            case self::TYPE_PER_WEEK_DAY:
                $selectPeriodNum    = "DAYOFWEEK($dateString)";
                $selectPeriodName   = "DAYNAME($dateString)";
                break;
            case self::TYPE_PER_HOUR:
                $selectPeriodNum    = "HOUR($dateString)";
                $selectPeriodName   = "HOUR($dateString)";
                break;
            default:
                //as self::TYPE_PER_DAY:
                $selectPeriodNum    = "DAYOFMONTH($dateString)";
                $selectPeriodName   = "DAYOFMONTH($dateString)";
        }

        return array(
            'num'   => $selectPeriodNum,
            'name'  => $selectPeriodName,
        );
    }

}
