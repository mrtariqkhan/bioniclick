<?php

class ChartTopCustomersHelper extends ChartHelperBase {

    const MAX_TICK_LENGTH = 60;
    
    public function findData(
        $maxRawsCount = 10
    ) {

        $queryBuilder = new QueryBuilderTopCustomers(
            $this->getAdvertiser(),
            $this->getCompany(),
            $this->getFilterValues()
        );
        $sqlQueryString = $queryBuilder->getSqlQueryString();

        $queryResult = array();
        if (!empty($sqlQueryString)) {
            $conn = Doctrine_Manager::getInstance()->getConnection('doctrine');
            $queryResult = $conn->fetchAssoc($sqlQueryString, array());
        }

        return self::buildDataArray($queryResult, $maxRawsCount);
    }

    protected static function buildDataArray(
        array $queryResult,
        $maxRawsCount,
        $othersColumnName = 'Others'
    ) {

        $result = array();
        if (count($queryResult) > 0) {
            $othersCount = 0;
            $rowNum = 0;
            $i = 1;
            foreach ($queryResult as $row) {
                if ($rowNum++ < $maxRawsCount) {

                    if (isset($row['name'])) {
                        $objectKind = ucfirst($row['customer_kind']);
                        $objectName = "$objectKind '{$row['name']}'";
                    } else {
                        $objectName = 'Unassigned';
                    }
                    $tick = self::getTick($i, $objectName);
                    $result[$tick] = $row['calls_count'];
                    $i++;

                } else {
                    $othersCount += $row['calls_count'];
                }
            }

            if ($othersCount > 0) {
                $result[$othersColumnName] = $othersCount;
            }
        }

        return $result;
    }

    protected static function getTick($i, $tick) {

        if (strlen($tick) > self::MAX_TICK_LENGTH) {
            $tick = "$i. " . substr($tick, 0, self::MAX_TICK_LENGTH) . '...';
        } else {
            $tick = "$i. $tick";
        }

        return $tick;
    }
}
