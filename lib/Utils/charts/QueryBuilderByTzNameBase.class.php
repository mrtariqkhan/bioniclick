<?php

class QueryBuilderByTzNameBase extends QueryBuilderBase {

    protected $tzName   = null;


    /**
     *
     * @param type $advertiser null or Advertiser
     * @param type $company null or Company
     * @param string $tzName time offset from need $timezone to GMT0. For example: '-0500', '+0700'
     * @param array $filterValues
     */
    public function __construct(
        $advertiser,
        $company,
        $tzName,
        $filterValues
    ) {

        parent::__construct($advertiser, $company, $filterValues);

        $this->setTzName($tzName);
    }


    public function getTzName() {
        return $this->tzName;
    }


    public function setTzName($tzName) {
        $this->tzName = $tzName;
    }
}
