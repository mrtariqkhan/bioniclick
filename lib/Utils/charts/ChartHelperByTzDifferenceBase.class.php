<?php

class ChartHelperByTzDifferenceBase extends ChartHelperBase {

    protected $tzDiff   = null;


    /**
     *
     * @param type $advertiser null or Advertiser
     * @param type $company null or Company
     * @param string $tzDiff time offset from need $timezone to GMT0. For example: '-0500', '+0700'
     * @param array $filterValues
     */
    public function __construct(
        $advertiser,
        $company,
        $tzDiff,
        $filterValues
    ) {

        parent::__construct($advertiser, $company, $filterValues);

        $this->setTzDiff($tzDiff);
    }


    public function getTzDiff() {
        return $this->tzDiff;
    }


    public function setTzDiff($tzDiff) {
        $this->tzDiff = $tzDiff;
    }
}
