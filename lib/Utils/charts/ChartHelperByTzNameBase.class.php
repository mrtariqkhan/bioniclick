<?php

class ChartHelperByTzNameBase extends ChartHelperBase {

    protected $tzName   = null;


    /**
     *
     * @param type $advertiser null or Advertiser
     * @param type $company null or Company
     * @param string $tzName
     * @param array $filterValues
     */
    public function __construct(
        $advertiser,
        $company,
        $tzName,
        $filterValues
    ) {

        parent::__construct($advertiser, $company, $filterValues);

        $this->setTzName($tzName);
    }


    public function getTzName() {
        return $this->tzName;
    }


    public function setTzName($tzName) {
        $this->tzName = $tzName;
    }
}
