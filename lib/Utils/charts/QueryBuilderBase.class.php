<?php

class QueryBuilderBase extends QueryBuilderFiltered {

    protected $advertiser   = null;
    protected $company      = null;


    /**
     *
     * @param type $advertiser null or Advertiser
     * @param type $company null or Company
     * @param string $needTzDiff time offset from need $timezone to GMT0. For example: '-0500', '+0700'
     * @param array $filterValues
     */
    public function __construct(
        $advertiser,
        $company,
        $filterValues
    ) {

        parent::__construct($filterValues);

        $this->setAdvertiser($advertiser);
        $this->setCompany($company);
    }


    public function getAdvertiser() {
        return $this->advertiser;
    }

    public function getCompany() {
        return $this->company;
    }


    public function setAdvertiser($advertiser) {
        $this->advertiser = $advertiser;
    }

    public function setCompany($company) {
        $this->company = $company;
    }


    public function getAdvertiserId() {

        if (empty($this->advertiser)) {
            return null;
        }
        return $this->advertiser->getId();
    }

    public function getCompanyId() {

        if (empty($this->company)) {
            return null;
        }
        return $this->company->getId();
    }
}
