<?php

class QueryBuilderFiltered {

    const RESULT_PARAMETER_GRAPH_DATA = 'graph_data';
    const RESULT_PARAMETER_TIME_RANGE = 'time_range';
    const RESULT_PARAMETER_FORMAT_TIME = 'Y M d H:i:s O';


    protected $filterValues = array();


    /**
     *
     * @param array $filterValues
     */
    public function __construct(
        $filterValues
    ) {

        $this->setFilterValues($filterValues);
    }


    protected function addFilteringByTicStartTimeArrToQuery(
        Doctrine_Query $query
    ) {

        $from = $this->getFilterValue('from');
        $to = $this->getFilterValue('to');

        if (!empty($from) || !empty($to)) {

            $alias = $query->getRootAlias();

            if (!empty($from)) {
                $query->andWhere("$alias.start_time >= $from");
            }
            if (!empty($to)) {
                $query->andWhere("$alias.start_time <= $to");
            }
        }

        return $query;
    }

    protected function addFilteringByTicStartTimeArr(
        $subSqlQueryString,
        &$needToAddWhere
    ) {

        $from = $this->getFilterValue('from');
        $to = $this->getFilterValue('to');

        if (
            (!empty($from) || !empty($to)) 
            && !empty($subSqlQueryString)
        ) {

            $subSqlQueryWhere = array();

            if (!empty($from)) {
                $subSqlQueryWhere[] = "tic.start_time >= $from";
            }
            if (!empty($to)) {
                $subSqlQueryWhere[] = "tic.start_time <= $to";
            }

            if (!empty($subSqlQueryWhere)) {
                $subSqlQueryString .= ($needToAddWhere) ? ' WHERE ' : ' AND ';
                $subSqlQueryString .= implode(' AND ', $subSqlQueryWhere);
                $needToAddWhere = false;
            }
        }

        return $subSqlQueryString;
    }

    protected static function addFilteringByTicStartTime(
        $subSqlQueryString,
        $from,
        $to,
        $needToAddWhere
    ) {

        $subSqlQueryString .= ($needToAddWhere) ? ' WHERE ' : ' AND ';
        $subSqlQueryString .= "tic.start_time BETWEEN $from AND $to";

        return $subSqlQueryString;
    }


    public function getFilterValue($name, $default = null) {

        $filterValues = $this->getFilterValues();

        if (array_key_exists($name, $filterValues) && !empty($filterValues[$name])) {
            return $filterValues[$name];
        }

        return $default;
    }

    public function getFilterValues() {
        return $this->filterValues;
    }


    public function setFilterValues($filterValues) {
        $this->filterValues = $filterValues;
    }
}
