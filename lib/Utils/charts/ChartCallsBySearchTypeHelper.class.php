<?php

class ChartCallsBySearchTypeHelper extends ChartCallsByParameterHelper {
    
    public function findData(
        $undefinedIsNeed = false,
        $maxRawsCount = 10
    ) {
        $queryResult = $this->findQueryResultByParameter(
            'QueryBuilderCallsBySearchType', 
            $undefinedIsNeed, 
            $maxRawsCount
        );
        return $this->buildDataArray($queryResult, $undefinedIsNeed, $maxRawsCount);
    }

    protected function buildDataArray(
        array $queryResult,
        $undefinedIsNeed,
        $maxRawsCount,
        $maxTickLen = self::MAX_TICK_LENGTH
    ) {
        $result = array();


        if (count($queryResult) > 0) {
            $othersCount = 0;
            $rowNum = 0;
            $i = 0;
            $parameterCountPrevious = null;
            $v=NULL;
            foreach ($queryResult as $key => $row) {
                if($row["by_parameter"] == self::OTHERS_RAW_NAME)
                {
                    $v = $row;
                    unset($queryResult[$key]);

                }
            }

            foreach ($queryResult as $row) {
                $parameterValue = $row[QueryBuilderCallsBySearchType::RESULT_VALUE_BY];
                $parameterCount = $row[QueryBuilderCallsBySearchType::RESULT_VALUE_CALLS_COUNT];

                if (is_null($parameterValue)) {
                    if (!$undefinedIsNeed) {
                        continue;
                    }
                    $parameterValue = self::UNDEFINED." Search Type";
                } else {
                    $parameterValue = VisitorAnalyticsTable::convertIsOrganic($parameterValue);
                }

                $rowNum++;
                if (is_null($maxRawsCount) || $rowNum <= $maxRawsCount) {
                    if (is_null($parameterCountPrevious) || $parameterCountPrevious != $parameterCount) {
                        $i++;
                    }
                    $tick = self::getTick($i, $parameterValue, $maxTickLen);
                    $result[$tick] = $parameterCount;
                    $parameterCountPrevious = $parameterCount;

                } else {
                    $othersCount += $parameterCount;
                }
            }

            if ($othersCount > 0) {
                $result[self::OTHERS_RAW_NAME] = $othersCount;
            }
            if (isset($result[self::OTHERS_RAW_NAME]) ) {
                $result[self::OTHERS_RAW_NAME] = $result[self::OTHERS_RAW_NAME] + $v[QueryBuilderCallsByKeyword::RESULT_VALUE_CALLS_COUNT];
            }
            else if($v[QueryBuilderCallsByKeyword::RESULT_VALUE_CALLS_COUNT] > 0){
                $result[self::OTHERS_RAW_NAME] = $v[QueryBuilderCallsByKeyword::RESULT_VALUE_CALLS_COUNT];
            }
                        
        }

        return $result;
    }
}
