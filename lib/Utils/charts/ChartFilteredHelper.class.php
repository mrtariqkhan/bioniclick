<?php

class ChartFilteredHelper {

    const RESULT_PARAMETER_GRAPH_DATA = 'graph_data';
    const RESULT_PARAMETER_TIME_RANGE = 'time_range';
    const RESULT_PARAMETER_FORMAT_TIME = 'M d Y O'; //'Y M d H:i:s O';


    protected $filterValues = array();


    /**
     *
     * @param array $filterValues
     */
    public function __construct(
        $filterValues
    ) {

        $this->setFilterValues($filterValues);
    }


    public function getFilterValues() {
        return $this->filterValues;
    }


    public function setFilterValues($filterValues) {
        $this->filterValues = $filterValues;
    }
}
