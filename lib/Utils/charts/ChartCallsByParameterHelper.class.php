<?php

abstract class ChartCallsByParameterHelper extends ChartHelperByTzDifferenceBase {

    const MAX_TICK_LENGTH   = null;
    const UNDEFINED         = 'Undefined';
    const DIRECT            = 'Direct';
    const OTHERS_RAW_NAME   = 'Others';

    public static $LINKS_ESCAPE = array(
        self::UNDEFINED,
        self::DIRECT,
        self::OTHERS_RAW_NAME,
    );


    protected $user   = null;


    /**
     *
     * @param type $advertiser null or Advertiser
     * @param type $company null or Company
     * @param string $tzDiff time offset from need $timezone to GMT0. For example: '-0500', '+0700'
     * @param array $filterValues
     * @param sfGuardUser $user
     */
    public function __construct(
        $advertiser,
        $company,
        $tzDiff,
        $filterValues,
        $user
    ) {

        parent::__construct($advertiser, $company, $filterValues, $filterValues);

        $this->setUser($user);
    }


    /**
     * @param boolean $undefinedIsNeed
     * @param integer $maxRawsCount
     * @return array 
     */
    abstract public function findData(
        $undefinedIsNeed = false, 
        $maxRawsCount = 10
    );

    /**
     *
     * @param string $classNameQueryBuilderCallsByParameter
     * @param boolean $undefinedIsNeed
     * @param integer $maxRawsCount
     * @return array 
     */
    protected function findQueryResultByParameter(
        $classNameQueryBuilderCallsByParameter,
        $undefinedIsNeed,
        $maxRawsCount
    ) {

        $visitorAnalyticsTypeCode = $this->getUser()->getIsSuperAdmin() 
            ? VisitorAnalyticsTable::TYPE_CODE_UNDEFINED 
            : VisitorAnalyticsTable::TYPE_CODE_IS_SUITABLE
        ;

        $queryBuilder = new $classNameQueryBuilderCallsByParameter(
            $this->getAdvertiser(),
            $this->getCompany(),
            $this->getTzDiff(),
            $this->getFilterValues(),
            $visitorAnalyticsTypeCode
        );
        $sqlQueryString = $queryBuilder->getSqlQueryString();
        unset($queryBuilder);
        $queryResult = array();
        if (!empty($sqlQueryString)) {
            $conn = Doctrine_Manager::getInstance()->getConnection('doctrine');
            $queryResult = $conn->fetchAssoc($sqlQueryString, array());
        }

        return $queryResult;
    }

    protected static function getTick($i, $tick, $maxTickLen) {

        if (!empty($maxTickLen) && (strlen($tick) > $maxTickLen)) {
            $tick = "$i. " . substr($tick, 0, $maxTickLen) . '...';
        } else {
            $tick = "$i. " . $tick;
        }

        return $tick;
    }


    public function getUser() {
        return $this->user;
    }


    public function setUser($user) {
        $this->user = $user;
    }
}
