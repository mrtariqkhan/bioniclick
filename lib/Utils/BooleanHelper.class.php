<?php

class BooleanHelper {

    public static function getAsString($booleanValue) {

        if ($booleanValue) {
            return 'Yes';
        }
        return 'No';
    }
}
