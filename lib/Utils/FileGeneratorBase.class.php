<?php

/**
 * Description of FileGeneratorBase
 *
 * @author fairdev
 */
abstract class FileGeneratorBase {

    protected $sfDirToGenerate      = 'sf_upload_dir';
    protected $pathToDirAdditional  = '';
    protected $fileExtension        = '';

    protected $pathToDir    = '';
    protected $pathToFile   = '';
    protected $fileName     = '';

    const MODE = 0777;


    abstract protected function preFillFile();
    abstract protected function fillFile();
    abstract protected function postFillFile();

    abstract protected function generateFileNameUnique();
    abstract protected function generateFileNamePrefix();


    public function __construct() {

        $this->generatePathToDir();
//        $this->generateFileName();
    }

    public function generateFile() {

        $this->preFillFile();

        try {
            $this->fillFile();
        } catch (Exception $e) {
            $this->postFillFile();
            throw $e;
        }

        $result = $this->postFillFile();

        $this->chmodFile();

        return $result;
    }

    public function removeFolder() {
        $this->removeFolderRecursively($this->getPathToDir());
    }

    protected function removeFolderRecursively($directoryName, $empty = false) {

        if (substr($directoryName, -1) == '/') {
            $directoryName = substr($directoryName, 0, -1);
        }

        if (!file_exists($directoryName)) {
            throw new Exception("$directoryName file does not exist.");
        } elseif (!is_dir($directoryName)) {
            throw new Exception("$directoryName is not directory.");
        } elseif (!is_readable($directoryName)) {
            throw new Exception("$directoryName is not readable.");
        } else {
            $directoryHandle = opendir($directoryName);
            while ($contents = readdir($directoryHandle)) {
                if ($contents != '.' && $contents != '..') {
                    $path = $directoryName . "/" . $contents;

                    if(is_dir($path)) {
                        $this->removeFolderRecursively($path);
                    } else {
                        unlink($path);
                    }
                }
            }

            closedir($directoryHandle);

            if ($empty == false) {
                if (!rmdir($directoryName)) {
                    throw new Exception("$directoryName has not been deleted.");
                }
            }
        }
    }

    protected function generatePathToDir() {

        $pathBase = sfConfig::get($this->getSfDirToGenerate());
        $pathAdd = $this->getPathToDirAdditional();

        $pathToDir = $pathBase . DIRECTORY_SEPARATOR . $pathAdd;
        $this->setPathToDir($pathToDir);
        $this->generateFolder($pathToDir, $pathBase, $pathAdd);
    }

    protected function generateFolder($pathToDir, $pathBase, $pathAdd) {

        if (!file_exists($pathToDir)) {

            $pathToCreate = $pathBase;

            $pathAddParts = explode(DIRECTORY_SEPARATOR, $pathAdd);
            foreach ($pathAddParts as $i => $pathAddPart) {
                if (empty($pathAddPart)) {
                    break;
                }

                $pathToCreate .= DIRECTORY_SEPARATOR . $pathAddPart;

                if (!file_exists($pathToCreate)) {
                    mkdir($pathToCreate, self::MODE, true);
                    chmod($pathToCreate, self::MODE);
                }

                if (!is_readable($pathToCreate)) {
                    throw new Exception("Folder $pathToCreate is not readable.");
                } elseif (!is_writable($pathToCreate)) {
                    throw new Exception("Folder $pathToCreate is not writable.");
                }
            }

        }

        if (!is_readable($pathToDir)) {
            throw new Exception("Folder $pathToDir is not readable.");
        } elseif (!is_writable($pathToDir)) {
            throw new Exception("Folder $pathToDir is not writable.");
        }
    }

    /**
     * Call this method before file's creating.
     */
    protected function deleteFile() {

        $pathToFile = $this->getPathToFile();

        if (!file_exists($pathToFile)) {
            return;
        }

        if (!is_readable($pathToFile)) {
            throw new Exception("File $pathToFile is not readable.");
        } elseif (!is_writable($pathToFile)) {
            throw new Exception("File $pathToFile is not writable.");
        }

        $fh = fopen($pathToFile, 'w');
        if (!$fh) {
            throw new Exception("Can't open file $pathToFile");
        }
        fclose($fh);

        unlink($pathToFile);
    }

    protected function chmodFile() {

        $pathToFile = $this->getPathToFile();
        if (!file_exists($pathToFile)) {
            throw new Exception("File $pathToFile has not been created.");
        }

        chmod($pathToFile, self::MODE);

        if (!is_readable($pathToFile)) {
            throw new Exception("File $pathToFile is not readable.");
        } elseif (!is_writable($pathToFile)) {
            throw new Exception("File $pathToFile is not writable.");
        }
    }

    protected function generateFileName() {

        $prefix             = $this->generateFileNamePrefix();
        $fileNameGenerated  = $this->generateFileNameUnique();

        $fileName = $prefix . $fileNameGenerated . $this->getFileExtension();

        $this->setFileName($fileName);
    }

    protected function generatePathToFile() {

        $pathToFile = $this->getPathToDir() . $this->getFileName();
        $this->setPathToFile($pathToFile);
    }


    public function getFileExtension() {
        return $this->fileExtension;
    }
    protected function setFileExtension($fileExtension) {
        $this->fileExtension = $fileExtension;
    }

    public function getSfDirToGenerate() {
        return $this->sfDirToGenerate;
    }
    protected function setSfDirToGenerate($sfDirToGenerate) {
        $this->sfDirToGenerate = $sfDirToGenerate;
    }

    public function getPathToDirAdditional() {
        return $this->pathToDirAdditional;
    }
    protected function setPathToDirAdditional($pathToDirAdditional) {
        $this->pathToDirAdditional = $pathToDirAdditional;
    }


    public function getPathToDir() {
        return $this->pathToDir;
    }
    protected function setPathToDir($pathToDir) {
        $this->pathToDir = $pathToDir;
    }

    public function getFileName() {
        return $this->fileName;
    }
    protected function setFileName($fileName) {
        $this->fileName = $fileName;
        $this->generatePathToFile();
    }

    public function getPathToFile() {
        return $this->pathToFile;
    }
    protected function setPathToFile($pathToFile) {
        $this->pathToFile = $pathToFile;
    }
}

