<?php

class BranchHelperUser extends BranchHelper {

    const ALIAS_PROFILE = 'profile';
    const ALIAS_ADV     = 'adv';
    const ALIAS_COM     = 'com';
    const ALIAS_ADV_COM = 'advCom';
    const ALIAS_COM_SOME= 'COMPANY_ALIAS';

    protected function createQueryByAdvertiserId($query, $advertiserId) {

        $query = $this->addJoin($query);
        $query->andWhere(self::ALIAS_ADV . ".id = $advertiserId");
        return $query;
    }

    protected function createQueryByAgency($query, $agency, $filterValues = array()) {

        $aliasComSome = self::ALIAS_COM_SOME;

        $query = $this->addJoin($query);
        $query = $this->filterByAdvertiserName($query, $filterValues);

        $query = $this->addQueryCondition($query, "$aliasComSome.lft BETWEEN {$agency->getLft()} AND {$agency->getRgt()}");

        return $query;
    }

    protected function createQueryByReseller($query, $reseller, $filterValues = array()) {

        $aliasComSome = self::ALIAS_COM_SOME;

        $query = $this->addJoin($query);
        $query = $this->filterByAdvertiserName($query, $filterValues);

        $query = $this->addQueryCondition($query, "$aliasComSome.lft BETWEEN {$reseller->getLft()} AND {$reseller->getRgt()}");

        if (empty($filterValues)) {
            return $query;
        }

        if (array_key_exists(CompanyTable::$LEVEL_NAME_AGENCY, $filterValues)) {
            $level = CompanyTable::$LEVEL_AGENCY;
            $valueAgency = mysql_escape_string($filterValues[CompanyTable::$LEVEL_NAME_AGENCY]);
            $query = $this->addQueryCondition($query, "$aliasComSome.level = {$level}");
            $query = $this->addQueryCondition($query, "$aliasComSome.name LIKE '%$valueAgency%'");
        }

        return $query;
    }
    
    
    
    
    
    protected function createQueryByBionic($query, $filterValues = array()) {

        $aliasComSome = self::ALIAS_COM_SOME;
        $query = $this->addJoin($query);
        $query = $this->filterByAdvertiserName($query, $filterValues);

        if (    empty($filterValues)
            || (    !array_key_exists(CompanyTable::$LEVEL_NAME_AGENCY, $filterValues)
                &&  !array_key_exists(CompanyTable::$LEVEL_NAME_RESELLER, $filterValues)
            )
        ) {
            return $query;
        }

        if (array_key_exists(CompanyTable::$LEVEL_NAME_AGENCY, $filterValues)
            && array_key_exists(CompanyTable::$LEVEL_NAME_RESELLER, $filterValues)
        ) {
            $valueResellser = mysql_escape_string($filterValues[CompanyTable::$LEVEL_NAME_RESELLER]);
            $valueAgency = mysql_escape_string($filterValues[CompanyTable::$LEVEL_NAME_AGENCY]);
            $subQuery = $query->createSubquery()
                ->addFrom('Company resellers,')
                ->addFrom('Company agencies')
                ->addSelect('resellers.id')
                ->andWhere('resellers.level = ' . CompanyTable::$LEVEL_RESELLER)
                ->andWhere('agencies.level = '  . CompanyTable::$LEVEL_AGENCY)
                ->andWhere("resellers.name LIKE '%$valueResellser%'")
                ->andWhere("agencies.name LIKE '%$valueAgency%'")
                ->andWhere('agencies.lft BETWEEN resellers.lft AND resellers.rgt')
            ;
            $subQuery = $this->addQueryCondition($subQuery, "$aliasComSome.id = agencies.id");
            $subQueryString = $subQuery->getDql();
            $query->andWhere('EXISTS (' . $subQueryString . ')');
            return $query;
        } elseif (array_key_exists(CompanyTable::$LEVEL_NAME_AGENCY, $filterValues)) {
            $valueAgency = mysql_escape_string($filterValues[CompanyTable::$LEVEL_NAME_AGENCY]);
            $subQuery = $query->createSubquery()
                ->addFrom('Company resellers,')
                ->addFrom('Company agencies')
                ->addSelect('resellers.id')
                ->andWhere('resellers.level = ' . CompanyTable::$LEVEL_RESELLER)
                ->andWhere('agencies.level = '  . CompanyTable::$LEVEL_AGENCY)
                ->andWhere("agencies.name LIKE '%$valueAgency%'")
                ->andWhere('agencies.lft BETWEEN resellers.lft AND resellers.rgt')
            ;
            $subQuery = $this->addQueryCondition($subQuery, "$aliasComSome.id = agencies.id");
            $subQueryString = $subQuery->getDql();
            $query->andWhere('EXISTS (' . $subQueryString . ')');
            return $query;            
        } elseif (array_key_exists(CompanyTable::$LEVEL_NAME_RESELLER, $filterValues)) {
            $valueResellser = mysql_escape_string($filterValues[CompanyTable::$LEVEL_NAME_RESELLER]);
            $subQueryForAgencies = $query->createSubquery()
                ->addFrom('Company resellers,')
                ->addFrom('Company agencies')
                ->addSelect('resellers.id')
                ->andWhere('resellers.level = ' . CompanyTable::$LEVEL_RESELLER)
                ->andWhere('agencies.level = '  . CompanyTable::$LEVEL_AGENCY)
                ->andWhere('agencies.lft BETWEEN resellers.lft AND resellers.rgt')
                ->andWhere("resellers.name LIKE '%$valueResellser%'")                
            ;            
            $subQueryForAgencies = $this->addQueryCondition(
                 $subQueryForAgencies, 
                 "$aliasComSome.id = agencies.id OR $aliasComSome.id = resellers.id"
            );
            $subQueryForResellers = $query->createSubquery()
                ->addFrom('Company resellers1')                
                ->addSelect('resellers1.id')
                ->andWhere('resellers1.lft + 1 = resellers1.rgt')
                ->andWhere('resellers1.level = ' . CompanyTable::$LEVEL_RESELLER)                
                ->andWhere("resellers1.name LIKE '%$valueResellser%'")
            ;
            $subQueryForResellers = $this->addQueryCondition(
                 $subQueryForResellers, 
                 "$aliasComSome.id = resellers1.id"
            );
            $subQueryForAgenciesString = $subQueryForAgencies->getDql();
            $subQueryForResellersString = $subQueryForResellers->getDql();
            $query->andWhere('EXISTS (' . $subQueryForAgenciesString . ') OR EXISTS (' . $subQueryForResellersString . ')');
            
            return $query;
        }
    }   
    
    
//    protected function filterBionicSubQuery($subQuery, $filterValues) {
//
//        if (array_key_exists(CompanyTable::$LEVEL_NAME_RESELLER, $filterValues)) {
//            $subQuery->andWhere("resellers.name LIKE '%{$filterValues[CompanyTable::$LEVEL_NAME_RESELLER]}%'");
//        }
//
//        if (array_key_exists(CompanyTable::$LEVEL_NAME_AGENCY, $filterValues)) {
//            $subQuery->andWhere("agencies.name LIKE '%{$filterValues[CompanyTable::$LEVEL_NAME_AGENCY]}%'");
//        }
//
//        return $subQuery;
//    }
//    
//
//    protected function addSpecificBionicSubQuery($subQuery, $filterValues) {
//
//        $aliasComSome = self::ALIAS_COM_SOME;
//
//        if (array_key_exists(CompanyTable::$LEVEL_NAME_AGENCY, $filterValues)
//            && array_key_exists(CompanyTable::$LEVEL_NAME_RESELLER, $filterValues)
//        ) {
//            $subQuery->andWhere('agencies.lft BETWEEN resellers.lft AND resellers.rgt');
//            $subQuery = $this->addQueryCondition($subQuery, "$aliasComSome.id = agencies.id");
//
//        } elseif (array_key_exists(CompanyTable::$LEVEL_NAME_AGENCY, $filterValues)) {
//            $subQuery->andWhere('agencies.lft BETWEEN resellers.lft AND resellers.rgt');
//            $subQuery = $this->addQueryCondition($subQuery, "$aliasComSome.id = agencies.id");
//
//        } elseif (array_key_exists(CompanyTable::$LEVEL_NAME_RESELLER, $filterValues)) {
//            $subQuery->andWhere('((agencies.lft BETWEEN resellers.lft AND resellers.rgt) OR (resellers.lft + 1 = resellers.rgt))');
//            $subQuery = $this->addQueryCondition($subQuery, "$aliasComSome.id = agencies.id OR $aliasComSome.id = resellers.id");
//        }
//
//        return $subQuery;
//    }

    /**
     *
     * @param <type> $query
     * @param <type> $condition can contain self::ALIAS_COM_SOME string which will be replaced by each need alias
     * @return <type>
     */
    protected function addQueryCondition($query, $condition) {

        $aliasComSome = self::ALIAS_COM_SOME;
        $condition = "($aliasComSome.id IS NULL OR ($aliasComSome.id IS NOT NULL AND ($condition)))";

        $conditionCom       = str_replace($aliasComSome, self::ALIAS_COM, $condition);
        $conditionAdvCom    = str_replace($aliasComSome, self::ALIAS_ADV_COM, $condition);

        $query
            ->andWhere($conditionCom)
            ->andWhere($conditionAdvCom)
        ;

        return $query;
    }

    protected function addJoin($query) {

        $query
            ->leftJoin(self::ALIAS_PROFILE . ".Advertiser " . self::ALIAS_ADV)
            ->leftJoin(self::ALIAS_PROFILE . ".Company " . self::ALIAS_COM)
            ->leftJoin(self::ALIAS_ADV . ".Company " . self::ALIAS_ADV_COM)
        ;

        return $query;
    }
}
