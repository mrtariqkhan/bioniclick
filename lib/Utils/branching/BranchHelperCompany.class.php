<?php

class BranchHelperCompany extends BranchHelperBase {

    public function __construct() {

        $this->setLevelNames(array(
            CompanyTable::$LEVEL_NAME_AGENCY,
            CompanyTable::$LEVEL_NAME_RESELLER,
            CompanyTable::$LEVEL_NAME_BIONIC,
        ));
    }

    public function getBranchingHeadersForUserLevel($level) {

        $headers = array();
        switch ($level) {
            case CompanyTable::$LEVEL_BIONIC:
                $headers = array(
                    CompanyTable::$LEVEL_NAME_AGENCY     => 'Agency',
                    CompanyTable::$LEVEL_NAME_RESELLER   => 'Reseller',
                );
                break;
            case CompanyTable::$LEVEL_RESELLER:
                $headers = array(
                    CompanyTable::$LEVEL_NAME_AGENCY     => 'Agency',
                );
                break;
            case CompanyTable::$LEVEL_AGENCY:
                $headers = array(
                    CompanyTable::$LEVEL_NAME_AGENCY     => 'Agency',
                );
                break;
            default:
        }

        return $headers;
    }


    protected function createQueryByAdvertiserId($query, $advertiserId) {
        return $query;
    }

    protected function createQueryByAgency($query, $agency, $filterValues = array()) {

        $query->andWhere("com.lft BETWEEN {$agency->getLft()} AND {$agency->getRgt()}");

        return $query;
    }

    protected function createQueryByReseller($query, $reseller, $filterValues = array()) {

        $query->andWhere("com.lft BETWEEN {$reseller->getLft()} AND {$reseller->getRgt()}");

        if (empty($filterValues)) {
            return $query;
        }

        if (array_key_exists(CompanyTable::$LEVEL_NAME_AGENCY, $filterValues)) {
            $level = CompanyTable::$LEVEL_AGENCY;
            $query->andWhere("com.level = {$level}");
            $value = mysql_escape_string($filterValues[CompanyTable::$LEVEL_NAME_AGENCY]);
            $query->andWhere("com.name LIKE '%$value%'");
        }

        return $query;
    }

    protected function createQueryByBionic($query, $filterValues = array()) {

        if (    empty($filterValues)
            || (    !array_key_exists(CompanyTable::$LEVEL_NAME_AGENCY, $filterValues)
                &&  !array_key_exists(CompanyTable::$LEVEL_NAME_RESELLER, $filterValues)
            )
        ) {
            return $query;
        }
        
        if (array_key_exists(CompanyTable::$LEVEL_NAME_AGENCY, $filterValues)
            && array_key_exists(CompanyTable::$LEVEL_NAME_RESELLER, $filterValues)
        ) {
            $valueResellser = mysql_escape_string($filterValues[CompanyTable::$LEVEL_NAME_RESELLER]);
            $valueAgency = mysql_escape_string($filterValues[CompanyTable::$LEVEL_NAME_AGENCY]);
            $subQuery = $query->createSubquery()
                ->addFrom('Company resellers,')
                ->addFrom('Company agencies')
                ->addSelect('resellers.id')
                ->andWhere('resellers.level = ' . CompanyTable::$LEVEL_RESELLER)
                ->andWhere('agencies.level = '  . CompanyTable::$LEVEL_AGENCY)
                ->andWhere('agencies.lft BETWEEN resellers.lft AND resellers.rgt')
                ->andWhere("resellers.name LIKE '%$valueResellser%'")
                ->andWhere("agencies.name LIKE '%$valueAgency%'")
                ->andWhere('com.id = agencies.id')
            ;
            $subQueryString = $subQuery->getDql();
            $query->andWhere('EXISTS (' . $subQueryString . ')');
            return $query;            
        } elseif (array_key_exists(CompanyTable::$LEVEL_NAME_AGENCY, $filterValues)) {
            $valueAgency = mysql_escape_string($filterValues[CompanyTable::$LEVEL_NAME_AGENCY]);
            $query
                ->andWhere("name LIKE '%$valueAgency%'")
                ->andWhere('level = 2')
            ;
            return $query;
        } elseif (array_key_exists(CompanyTable::$LEVEL_NAME_RESELLER, $filterValues)) {
            $valueResellser = mysql_escape_string($filterValues[CompanyTable::$LEVEL_NAME_RESELLER]);
            $subQueryForAgencies = $query->createSubquery()
                ->addFrom('Company resellers,')
                ->addFrom('Company agencies')
                ->addSelect('resellers.id')
                ->andWhere('resellers.level = ' . CompanyTable::$LEVEL_RESELLER)
                ->andWhere('agencies.level = '  . CompanyTable::$LEVEL_AGENCY)
                ->andWhere("resellers.name LIKE '%$valueResellser%'")
                ->andWhere('agencies.lft BETWEEN resellers.lft AND resellers.rgt')
                ->andWhere('com.id = agencies.id')
            ;
            $subQueryForResellers = $query->createSubquery()
                ->addFrom('Company resellers1')                
                ->addSelect('resellers1.id')
                ->andWhere('resellers1.level = ' . CompanyTable::$LEVEL_RESELLER)
                ->andWhere("resellers1.name LIKE '%$valueResellser%'")
                ->andWhere('com.id = resellers1.id')
            ;
            $subQueryForAgenciesString = $subQueryForAgencies->getDql();
            $subQueryForResellersString = $subQueryForResellers->getDql();
            $query->andWhere('EXISTS (' . $subQueryForAgenciesString . ') OR EXISTS (' . $subQueryForResellersString . ')');
        }
        
        return $query;
    }
//
//    protected function filterBionicSubQuery($subQuery, $filterValues) {
//
//        if (array_key_exists(CompanyTable::$LEVEL_NAME_RESELLER, $filterValues)) {
//            $subQuery->andWhere("resellers.name LIKE '%{$filterValues[CompanyTable::$LEVEL_NAME_RESELLER]}%'");
//        }
//
//        if (array_key_exists(CompanyTable::$LEVEL_NAME_AGENCY, $filterValues)) {
//            $subQuery->andWhere("agencies.name LIKE '%{$filterValues[CompanyTable::$LEVEL_NAME_AGENCY]}%'");
//        }
//
//        return $subQuery;
//    }
//
//    /**
//     * Override this method to change BionicSubQuery.
//     * @param <type> $subQuery
//     * @return <type>
//     */
//    protected function addSpecificBionicSubQuery($subQuery, $filterValues) {
//
//        if (array_key_exists(CompanyTable::$LEVEL_NAME_AGENCY, $filterValues)
//            && array_key_exists(CompanyTable::$LEVEL_NAME_RESELLER, $filterValues)
//        ) {
//            $subQuery
//                ->andWhere('agencies.lft BETWEEN resellers.lft AND resellers.rgt')
//
//                ->andWhere('com.id = agencies.id')
//            ;
//        } elseif (array_key_exists(CompanyTable::$LEVEL_NAME_AGENCY, $filterValues)) {
//            $subQuery
//                ->andWhere('agencies.lft BETWEEN resellers.lft AND resellers.rgt')
//
//                ->andWhere('com.id = agencies.id')
//            ;
//        } elseif (array_key_exists(CompanyTable::$LEVEL_NAME_RESELLER, $filterValues)) {
//            $subQuery
//                ->andWhere('((agencies.lft BETWEEN resellers.lft AND resellers.rgt) OR (resellers.lft + 1 = resellers.rgt))')
//
//                ->andWhere('(com.id = agencies.id OR com.id = resellers.id)')
//            ;
//        }
//
//        return $subQuery;
//    }
}
