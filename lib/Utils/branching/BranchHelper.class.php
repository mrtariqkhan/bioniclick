<?php

class BranchHelper extends BranchHelperBase {

    public function __construct() {

        $this->setLevelNames(array(
            CompanyTable::$LEVEL_NAME_ADVERTISER,
            CompanyTable::$LEVEL_NAME_AGENCY,
            CompanyTable::$LEVEL_NAME_RESELLER,
            CompanyTable::$LEVEL_NAME_BIONIC,
        ));
    }

    public static function findAdvertiserIdsInBranch($user, $filterValues) {

        $ids = AdvertiserTable::findAdvertiserIdsInBranch($user, $filterValues);
        return $ids;
    }

    public function getBranchingHeadersForUserLevel($level) {

        $headers = array();
        switch ($level) {
            case CompanyTable::$LEVEL_BIONIC:
                $headers = array(
                    CompanyTable::$LEVEL_NAME_ADVERTISER => 'Advertiser',
                    CompanyTable::$LEVEL_NAME_AGENCY     => 'Agency',
                    CompanyTable::$LEVEL_NAME_RESELLER   => 'Reseller',
                );
                break;
            case CompanyTable::$LEVEL_RESELLER:
                $headers = array(
                    CompanyTable::$LEVEL_NAME_ADVERTISER => 'Advertiser',
                    CompanyTable::$LEVEL_NAME_AGENCY     => 'Agency',
                );
                break;
            case CompanyTable::$LEVEL_AGENCY:
                $headers = array(
                    CompanyTable::$LEVEL_NAME_ADVERTISER => 'Advertiser',
                );
                break;
            default:
        }

        return $headers;
    }

    protected function createQueryByAdvertiserId($query, $advertiserId) {

        $query->andWhere("adv.id = $advertiserId");

        return $query;
    }

    protected function createQueryByAgency($query, $agency, $filterValues = array()) {

        $query = $this->addJoin($query);
        $query = $this->filterByAdvertiserName($query, $filterValues);

        $query->andWhere("com.lft BETWEEN {$agency->getLft()} AND {$agency->getRgt()}");

        return $query;
    }

    protected function createQueryByReseller($query, $reseller, $filterValues = array()) {

        $query = $this->addJoin($query);
        $query = $this->filterByAdvertiserName($query, $filterValues);

        $query->andWhere("com.lft BETWEEN {$reseller->getLft()} AND {$reseller->getRgt()}");

        if (empty($filterValues)) {
            return $query;
        }

        if (array_key_exists(CompanyTable::$LEVEL_NAME_AGENCY, $filterValues)) {
            $level = CompanyTable::$LEVEL_AGENCY;
            $query->andWhere("com.level = {$level}");
            $value = mysql_escape_string($filterValues[CompanyTable::$LEVEL_NAME_AGENCY]);
            $query->andWhere("com.name LIKE '%$value%'");
        }

        return $query;
    }

    protected function createQueryByBionic($query, $filterValues = array()) {

        $query = $this->addJoin($query);
        $query = $this->filterByAdvertiserName($query, $filterValues);

        if (    empty($filterValues)
            || (    !array_key_exists(CompanyTable::$LEVEL_NAME_AGENCY, $filterValues)
                &&  !array_key_exists(CompanyTable::$LEVEL_NAME_RESELLER, $filterValues)
            )
        ) {
            return $query;
        }

        $subQuery = $query->createSubquery()
            ->addFrom('Company resellers,')
            ->addFrom('Company agencies')
            ->addSelect('resellers.id')
            ->andWhere('resellers.level = ' . CompanyTable::$LEVEL_RESELLER)
            ->andWhere('agencies.level = '  . CompanyTable::$LEVEL_AGENCY)
        ;

        $subQuery = $this->filterBionicSubQuery($subQuery, $filterValues);
        $subQuery = $this->addSpecificBionicSubQuery($subQuery, $filterValues);

        $subQueryString = $subQuery->getDql();
        $query->andWhere('EXISTS (' . $subQueryString . ')');

        return $query;
    }

    protected function addSpecificBionicSubQuery($subQuery, $filterValues) {

        if (array_key_exists(CompanyTable::$LEVEL_NAME_AGENCY, $filterValues)) {

            if (array_key_exists(CompanyTable::$LEVEL_NAME_AGENCY, $filterValues)
                && array_key_exists(CompanyTable::$LEVEL_NAME_RESELLER, $filterValues)
            ) {
                $subQuery
                    ->andWhere('agencies.lft BETWEEN resellers.lft AND resellers.rgt')

                    ->andWhere('com.id = agencies.id')
                ;
            } elseif (array_key_exists(CompanyTable::$LEVEL_NAME_AGENCY, $filterValues)) {
                $subQuery
                    ->andWhere('agencies.lft BETWEEN resellers.lft AND resellers.rgt')

                    ->andWhere('com.id = agencies.id')
                ;
            } elseif (array_key_exists(CompanyTable::$LEVEL_NAME_RESELLER, $filterValues)) {
                $subQuery
                    ->andWhere('((agencies.lft BETWEEN resellers.lft AND resellers.rgt) OR (resellers.lft + 1 = resellers.rgt))')

                    ->andWhere('(com.id = agencies.id OR com.id = resellers.id)')
                ;
            }

            return $subQuery;
        }

        if (array_key_exists(CompanyTable::$LEVEL_NAME_RESELLER, $filterValues)) {
            $subQuery
//                ->andWhere('((agencies.lft BETWEEN resellers.lft AND resellers.rgt) OR (resellers.lft + 1 = resellers.rgt))')

                ->andWhere('(com.id = agencies.id OR com.id = resellers.id)')
            ;

            $subSubQuery = $subQuery->createSubquery()
                ->select('adv1.id')
                ->from('Advertiser adv1')
                ->andWhere('adv1.company_id = resellers.id')
            ;

            $subSubQueryString = $subSubQuery->getDql();
            $subQuery->andWhere("((agencies.lft BETWEEN resellers.lft AND resellers.rgt) OR (resellers.lft + 1 = resellers.rgt AND EXISTS ($subSubQueryString)))");

            return $subQuery;
        }
    }

    protected function addJoin($query) {

        $query->leftJoin('adv.Company com');

        return $query;
    }

    protected function filterByAdvertiserName($query, $filterValues) {

        if (empty($filterValues)) {
            return $query;
        }

        if (array_key_exists(CompanyTable::$LEVEL_NAME_ADVERTISER, $filterValues)) {
            $value = mysql_escape_string($filterValues[CompanyTable::$LEVEL_NAME_ADVERTISER]);
            $query->andWhere("adv.name LIKE '%$value%'");
        }

        return $query;
    }

    protected function filterBionicSubQuery($subQuery, $filterValues) {

        if (array_key_exists(CompanyTable::$LEVEL_NAME_RESELLER, $filterValues)) {
            $value = mysql_escape_string($filterValues[CompanyTable::$LEVEL_NAME_RESELLER]);
            $subQuery->andWhere("resellers.name LIKE '%$value%'");
        }

        if (array_key_exists(CompanyTable::$LEVEL_NAME_AGENCY, $filterValues)) {
            $value = mysql_escape_string($filterValues[CompanyTable::$LEVEL_NAME_AGENCY]);
            $subQuery->andWhere("agencies.name LIKE '%$value%'");
        }

        return $subQuery;
    }
}
