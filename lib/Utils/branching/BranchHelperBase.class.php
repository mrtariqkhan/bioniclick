<?php

abstract class BranchHelperBase {

    const SIMPLE_FILTER_FIELD_TYPE_STRING   = 'string';
    const SIMPLE_FILTER_FIELD_TYPE_OTHER    = 'non-string';

    protected $levelNames = array();

    abstract public function getBranchingHeadersForUserLevel($level);

    abstract protected function createQueryByAdvertiserId($query, $advertiserId);
    abstract protected function createQueryByAgency($query, $agency, $filterValues = array());
    abstract protected function createQueryByReseller($query, $reseller, $filterValues = array());
    abstract protected function createQueryByBionic($query, $filterValues = array());

    /**
     *
     * @param <type> $query must contain relation to need table with alias (Advertiser -> 'adv', Company -> 'com')
     * @param <type> $user
     * @param array $filterValues may contain filter values for branching
     * @param array $simpleFilterFieldNames
     * @return <type> query
     */
    public function createQueryBranch($query, $user, $filterValues, $simpleFilterFieldNames = array()) {

        $role = $user->getRole();
        switch ($role) {
            case CompanyTable::$LEVEL_NAME_ADVERTISER:
                $query = $this->createQueryByAdvertiserId($query, $user->getAdvertiserId());
                break;
            case CompanyTable::$LEVEL_NAME_AGENCY:
                $query = $this->createQueryByAgency($query, $user->getFirstCompany(), $filterValues);
                break;
            case CompanyTable::$LEVEL_NAME_RESELLER:
                $query = $this->createQueryByReseller($query, $user->getFirstCompany(), $filterValues);
                break;
            case CompanyTable::$LEVEL_NAME_BIONIC:
                $query = $this->createQueryByBionic($query, $filterValues);
                break;
        }


        foreach ($simpleFilterFieldNames as $type => $simpleFilterFieldNamesTyped) {
            switch ($type) {
                case self::SIMPLE_FILTER_FIELD_TYPE_OTHER:
                    foreach ($simpleFilterFieldNamesTyped as $fieldQueryName => $filterName) {
                        if (array_key_exists($filterName, $filterValues)
                            && (!empty($filterValues[$filterName]) || $filterValues[$filterName] === 0 || $filterValues[$filterName] === false)
                        ) {
                            $query->andWhere("$fieldQueryName = ?", $filterValues[$filterName]);
                        }
                    }
                    break;
                case self::SIMPLE_FILTER_FIELD_TYPE_STRING:
                    foreach ($simpleFilterFieldNamesTyped as $fieldQueryName => $filterName) {
                        if (array_key_exists($filterName, $filterValues) && !empty($filterValues[$filterName])) {
                            $value = mysql_escape_string($filterValues[$filterName]);
                            $query->andWhere("($fieldQueryName LIKE '%$value%')");
                        }
                    }
                    break;
                default:
            }
        }

        return $query;
    }

    /**
     *
     * @param sfGuardUser $user
     * @param <> $objectList
     * @param array $objectMethodsHeaders
     * @param array $escapeHeaders
     * @return array
     */
    public function makeRaws(sfGuardUser $user, $objectList, $objectMethodsHeaders = array(), $escapeHeaders = array()) {

        $level = $user->getLevel();

        $branchingHeaders = $this->getBranchingHeadersForUserLevelNeed($level, $escapeHeaders);

        $raws = array();
        foreach ($objectList as $object) {

            $branch = $this->findBranchToLevel($object, $level);

            $objectBranchCells = array();
            foreach ($branchingHeaders as $branchObjectCode => $headerTitle) {
                $branchObject = array_key_exists($branchObjectCode, $branch)
                    ? $branch[$branchObjectCode]
                    : null
                ;

                //FIXME:: use overrided toString method later if need.
                $objectBranchCells[$branchObjectCode] = empty($branchObject)
                    ? ''
                    : $branchObject->getName()
                ;
            }

            $objectCells = array();
            foreach ($objectMethodsHeaders as $objectMethodName => $headerTitle) {
                $objectCells[$objectMethodName] = $object->$objectMethodName();
            }

            $objectCommonCells = array_merge(
                $objectCells,
                $objectBranchCells
            );

            $objectId = $object->getId();
            $raws[$objectId] = $objectCommonCells;
        }

        $headers = array_merge($objectMethodsHeaders, $branchingHeaders);

        $result = array();
        $result['headers']  = $headers;
        $result['raws']     = $raws;

        return $result;
    }

    public function findBranchToLevelByObjects(array $branchObjects, $level) {

        $needLevelNames = $this->getLevelNames();

        $branch = array();
        foreach ($needLevelNames as $levelName) {
            $branch[$levelName] = null;
        }


        if (array_search(CompanyTable::$LEVEL_NAME_ADVERTISER, $needLevelNames) !== false) {
            $branch[CompanyTable::$LEVEL_NAME_ADVERTISER] = $branchObjects['advertiser'];
        }

        $company = $branchObjects['company'];
        if (empty($company)) {
            return $branch;
        }


        $node = $company->getNode();
        do {
            $currentCompanyLevel = $node->getLevel();
            if ($currentCompanyLevel <= $level) {
                break;
            }

            switch ($currentCompanyLevel) {
                case CompanyTable::$LEVEL_BIONIC:
                case CompanyTable::$LEVEL_RESELLER:
                case CompanyTable::$LEVEL_AGENCY:
                    $levelName = CompanyTable::getLevelName($currentCompanyLevel);
                    $branch[$levelName] = $node->getRecord();
                    break;
                default:
            }

            if (!$node->hasParent()) {
                break;
            }
            $node = $node->getParent()->getNode();

        } while (true);

        return $branch;
    }

    public function findBranchToLevel($object, $level) {

        $branchObjects = array(
            'advertiser'=> null,
            'company'   => null,
        );

        $needLevelNames = $this->getLevelNames();

        if (array_search(CompanyTable::$LEVEL_NAME_ADVERTISER, $needLevelNames) !== false) {
            $branchObjects['advertiser'] = $this->callObjectMethod($object, 'getBranchAdvertiser');
        }

        $branchObjects['company'] = $this->callObjectMethod($object, 'getBranchCompany');

        return $this->findBranchToLevelByObjects($branchObjects, $level);
    }

    protected function callObjectMethod($object, $methodName) {

        if (!method_exists($object, $methodName)) {
            throw new Exception("Object has no method {$methodName}().");
        }

        return $object->$methodName();
    }

    protected function getBranchingHeadersForUserLevelNeed($level, $escapeHeaders = array()) {

        $headers = $this->getBranchingHeadersForUserLevel($level);

        foreach ($escapeHeaders as $escapeHeader) {
            unset($headers[$escapeHeader]);
        }

        return $headers;
    }

    protected function getLevelNames() {
        return $this->levelNames;
    }
    protected function setLevelNames(array $levelNames) {
        $this->levelNames = $levelNames;
    }
}
