<?php

/**
 * Description of fdBaseFilter
 *
 * @author konstantin
 */
class fdBaseFilter extends BaseForm {
    /**
     * @var array: key - filtered value's name, value - array of
     * form's fields
     */
    private $filterFields = array();

    public function getFiterFields() {
        return $this->filterFields;
    }

    public function setFiterFields(array $filterFields) {
        $this->filterFields = $filterFields;
    }

    public function addFilterField(array $filterFields) {
        $this->filterFields = array_merge($this->filterFields, $filterFields);
    }

    protected function processValuesAll($cleanedValues) {

        $filteredValues = $this->processValues($cleanedValues);

        $embeddedFilteredValuesAll = array();

        $embeddedForms = $this->getEmbeddedForms();
        foreach ($embeddedForms as $name => $embeddedForm) {
            if (array_key_exists($name, $cleanedValues)) {
                $embeddedCleanedValues = $cleanedValues[$name];
                $embeddedFilteredValuesAll[] = $embeddedForm->processValues($embeddedCleanedValues);
            }
        }

        foreach ($embeddedFilteredValuesAll as $embeddedFilteredValues) {
            $filteredValues = array_merge($filteredValues, $embeddedFilteredValues);
        }

        return $filteredValues;
    }

    protected function processValues($cleanedValues) {
        $filteredValues = array();
        foreach ($this->filterFields as $key => $formFields) {
            if (!method_exists($this, $method = sprintf('build%sValue', sfInflector::camelize($key)))) {
                $method = 'defaultProcess';
            }            
            $args = self::getSubArray($formFields, $cleanedValues);
            if (false === $ret = $this->$method($args)) {

            } else {
                $filteredValues[$key] = $ret;
            }
        }        
        return $filteredValues;
    }

    /**
     *
     * @param array $values
     * @param boolean $isCleaned true if You are sure that $values are valid
     * @return array
     */
    public function getFilteredValues($values, $isCleaned = false) {
        $result = array();
        if (!$isCleaned) {
            $this->bind($values);
            if ($this->isValid()) {
                $result = $this->processValuesAll($this->getValues());
            }
        } else {
            $result = $this->processValuesAll($values);
        }

        return $result;
    }
    
    protected function defaultProcess(array $args) {
        $ret = array_shift($args);
        if ($ret != '') {
            return $ret;
        }
        return false;
    }

    protected static function getSubArray(array $keys, array $array) {
        $result = array();
        foreach ($keys as $key) {
            $result[$key] = array_key_exists($key, $array) ? $array[$key] : '';
        }
        return $result;
    }


    protected function configureFieldChoice(
        $fieldName, 
        array $choices, 
        $default = null,
        $label = null
    ) {

        $keys = array_keys($choices);

        $this
            ->setWidget(
                $fieldName,
                new sfWidgetFormChoice(
                    array(
                        'choices' => $choices,
                    )
                )
            )
            ->setValidator(
                $fieldName,
                new sfValidatorChoice(
                    array(
                        'required' => true, 
                        'choices' => $keys,
                    )
                )
            )
        ;

        if (!is_null($default)) {
            $this->getWidget($fieldName)->setDefault($default);
        }
        if (!is_null($label)) {
            $this->getWidget($fieldName)->setLabel($label);
        }
    }
}
