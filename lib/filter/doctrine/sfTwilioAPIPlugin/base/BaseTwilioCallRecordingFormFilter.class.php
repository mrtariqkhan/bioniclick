<?php

/**
 * TwilioCallRecording filter form base class.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseTwilioCallRecordingFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'sid'                     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'twilio_incoming_call_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioIncomingCall'), 'add_empty' => true)),
      'file_url'                => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'date_created'            => new sfWidgetFormFilterInput(),
      'date_updated'            => new sfWidgetFormFilterInput(),
      'duration'                => new sfWidgetFormFilterInput(),
      'dollar_amount'           => new sfWidgetFormFilterInput(),
      'name'                    => new sfWidgetFormFilterInput(),
      'description'             => new sfWidgetFormFilterInput(),
      'gender'                  => new sfWidgetFormFilterInput(),
      'created_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'sid'                     => new sfValidatorPass(array('required' => false)),
      'twilio_incoming_call_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TwilioIncomingCall'), 'column' => 'id')),
      'file_url'                => new sfValidatorPass(array('required' => false)),
      'date_created'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'date_updated'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'duration'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'dollar_amount'           => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'name'                    => new sfValidatorPass(array('required' => false)),
      'description'             => new sfValidatorPass(array('required' => false)),
      'gender'                  => new sfValidatorPass(array('required' => false)),
      'created_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('twilio_call_recording_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TwilioCallRecording';
  }

  public function getFields()
  {
    return array(
      'id'                      => 'Number',
      'sid'                     => 'Text',
      'twilio_incoming_call_id' => 'ForeignKey',
      'file_url'                => 'Text',
      'date_created'            => 'Number',
      'date_updated'            => 'Number',
      'duration'                => 'Number',
      'dollar_amount'           => 'Number',
      'name'                    => 'Text',
      'description'             => 'Text',
      'gender'                  => 'Text',
      'created_at'              => 'Date',
      'updated_at'              => 'Date',
    );
  }
}
