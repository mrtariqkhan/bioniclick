<?php

/**
 * TwilioRedialedCall filter form base class.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseTwilioRedialedCallFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'segment_sid'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'twilio_incoming_call_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ParentCall'), 'add_empty' => true)),
      'called_number'           => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'call_status'             => new sfWidgetFormChoice(array('choices' => array('' => '', 'not yet dialed' => 'not yet dialed', 'in-progress' => 'in-progress', 'completed' => 'completed', 'busy' => 'busy', 'failed' => 'failed', 'no-answer' => 'no-answer'))),
      'start_time'              => new sfWidgetFormFilterInput(),
      'end_time'                => new sfWidgetFormFilterInput(),
      'duration'                => new sfWidgetFormFilterInput(),
      'price'                   => new sfWidgetFormFilterInput(),
      'created_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'segment_sid'             => new sfValidatorPass(array('required' => false)),
      'twilio_incoming_call_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('ParentCall'), 'column' => 'id')),
      'called_number'           => new sfValidatorPass(array('required' => false)),
      'call_status'             => new sfValidatorChoice(array('required' => false, 'choices' => array('not yet dialed' => 'not yet dialed', 'in-progress' => 'in-progress', 'completed' => 'completed', 'busy' => 'busy', 'failed' => 'failed', 'no-answer' => 'no-answer'))),
      'start_time'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'end_time'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'duration'                => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'price'                   => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'created_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('twilio_redialed_call_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TwilioRedialedCall';
  }

  public function getFields()
  {
    return array(
      'id'                      => 'Number',
      'segment_sid'             => 'Text',
      'twilio_incoming_call_id' => 'ForeignKey',
      'called_number'           => 'Text',
      'call_status'             => 'Enum',
      'start_time'              => 'Number',
      'end_time'                => 'Number',
      'duration'                => 'Number',
      'price'                   => 'Number',
      'created_at'              => 'Date',
      'updated_at'              => 'Date',
    );
  }
}
