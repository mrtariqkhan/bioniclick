<?php

/**
 * TwilioIncomingPhoneNumber filter form base class.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseTwilioIncomingPhoneNumberFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'sid'                       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'twilio_account_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioAccount'), 'add_empty' => true)),
      'url'                       => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'http_method_type'          => new sfWidgetFormChoice(array('choices' => array('' => '', 'GET' => 'GET', 'POST' => 'POST'))),
      'phone_number'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'friendly_name'             => new sfWidgetFormFilterInput(),
      'twilio_local_area_code_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioLocalAreaCode'), 'add_empty' => true)),
      'was_deleted'               => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'purchased_date'            => new sfWidgetFormFilterInput(),
      'created_at'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'sid'                       => new sfValidatorPass(array('required' => false)),
      'twilio_account_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TwilioAccount'), 'column' => 'id')),
      'url'                       => new sfValidatorPass(array('required' => false)),
      'http_method_type'          => new sfValidatorChoice(array('required' => false, 'choices' => array('GET' => 'GET', 'POST' => 'POST'))),
      'phone_number'              => new sfValidatorPass(array('required' => false)),
      'friendly_name'             => new sfValidatorPass(array('required' => false)),
      'twilio_local_area_code_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TwilioLocalAreaCode'), 'column' => 'id')),
      'was_deleted'               => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'purchased_date'            => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('twilio_incoming_phone_number_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TwilioIncomingPhoneNumber';
  }

  public function getFields()
  {
    return array(
      'id'                        => 'Number',
      'sid'                       => 'Text',
      'twilio_account_id'         => 'ForeignKey',
      'url'                       => 'Text',
      'http_method_type'          => 'Enum',
      'phone_number'              => 'Text',
      'friendly_name'             => 'Text',
      'twilio_local_area_code_id' => 'ForeignKey',
      'was_deleted'               => 'Boolean',
      'purchased_date'            => 'Number',
      'created_at'                => 'Date',
      'updated_at'                => 'Date',
    );
  }
}
