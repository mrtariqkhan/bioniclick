<?php

/**
 * TwilioIncomingCall filter form base class.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseTwilioIncomingCallFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'sid'                             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'twilio_account_id'               => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioAccount'), 'add_empty' => true)),
      'twilio_caller_phone_number_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioCallerPhoneNumber'), 'add_empty' => true)),
      'twilio_incoming_phone_number_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioIncomingPhoneNumber'), 'add_empty' => true)),
      'caller_country'                  => new sfWidgetFormFilterInput(),
      'caller_zip'                      => new sfWidgetFormFilterInput(),
      'caller_state'                    => new sfWidgetFormFilterInput(),
      'twilio_city_id'                  => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioCity'), 'add_empty' => true)),
      'call_status'                     => new sfWidgetFormChoice(array('choices' => array('' => '', 'not yet dialed' => 'not yet dialed', 'in-progress' => 'in-progress', 'completed' => 'completed', 'busy' => 'busy', 'failed' => 'failed', 'no-answer' => 'no-answer'))),
      'start_time'                      => new sfWidgetFormFilterInput(),
      'end_time'                        => new sfWidgetFormFilterInput(),
      'duration'                        => new sfWidgetFormFilterInput(),
      'price'                           => new sfWidgetFormFilterInput(),
      'created_at'                      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'                      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'sid'                             => new sfValidatorPass(array('required' => false)),
      'twilio_account_id'               => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TwilioAccount'), 'column' => 'id')),
      'twilio_caller_phone_number_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TwilioCallerPhoneNumber'), 'column' => 'id')),
      'twilio_incoming_phone_number_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TwilioIncomingPhoneNumber'), 'column' => 'id')),
      'caller_country'                  => new sfValidatorPass(array('required' => false)),
      'caller_zip'                      => new sfValidatorPass(array('required' => false)),
      'caller_state'                    => new sfValidatorPass(array('required' => false)),
      'twilio_city_id'                  => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TwilioCity'), 'column' => 'id')),
      'call_status'                     => new sfValidatorChoice(array('required' => false, 'choices' => array('not yet dialed' => 'not yet dialed', 'in-progress' => 'in-progress', 'completed' => 'completed', 'busy' => 'busy', 'failed' => 'failed', 'no-answer' => 'no-answer'))),
      'start_time'                      => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'end_time'                        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'duration'                        => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'price'                           => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'created_at'                      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('twilio_incoming_call_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TwilioIncomingCall';
  }

  public function getFields()
  {
    return array(
      'id'                              => 'Number',
      'sid'                             => 'Text',
      'twilio_account_id'               => 'ForeignKey',
      'twilio_caller_phone_number_id'   => 'ForeignKey',
      'twilio_incoming_phone_number_id' => 'ForeignKey',
      'caller_country'                  => 'Text',
      'caller_zip'                      => 'Text',
      'caller_state'                    => 'Text',
      'twilio_city_id'                  => 'ForeignKey',
      'call_status'                     => 'Enum',
      'start_time'                      => 'Number',
      'end_time'                        => 'Number',
      'duration'                        => 'Number',
      'price'                           => 'Number',
      'created_at'                      => 'Date',
      'updated_at'                      => 'Date',
    );
  }
}
