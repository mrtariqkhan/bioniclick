<?php

/**
 * TwilioLocalAreaCode filter form base class.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseTwilioLocalAreaCodeFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'code'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'phone_number_type' => new sfWidgetFormChoice(array('choices' => array('' => '', 'local' => 'local', 'toll free' => 'toll free'))),
      'twilio_state_id'   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioState'), 'add_empty' => true)),
      'created_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'        => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'code'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'phone_number_type' => new sfValidatorChoice(array('required' => false, 'choices' => array('local' => 'local', 'toll free' => 'toll free'))),
      'twilio_state_id'   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TwilioState'), 'column' => 'id')),
      'created_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'        => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('twilio_local_area_code_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'TwilioLocalAreaCode';
  }

  public function getFields()
  {
    return array(
      'id'                => 'Number',
      'code'              => 'Number',
      'phone_number_type' => 'Enum',
      'twilio_state_id'   => 'ForeignKey',
      'created_at'        => 'Date',
      'updated_at'        => 'Date',
    );
  }
}
