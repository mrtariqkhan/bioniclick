<?php

/**
 * TwilioIncomingPhoneNumber filter form.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrinePluginFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class TwilioIncomingPhoneNumberFormFilter extends PluginTwilioIncomingPhoneNumberFormFilter {

    const FIELD_NAME_SID                        = 'sid';
    const FIELD_NAME_TWILIO_ACCOUNT_ID          = 'twilio_account_id';
    const FIELD_NAME_PHONE_NUMBER_TYPE          = 'phone_number_type';
    const FIELD_NAME_URL                        = 'url';
    const FIELD_NAME_HTTP_METHOD_TYPE           = 'http_method_type';
    const FIELD_NAME_PHONE_COUNTRY_ID           = 'phone_number_country_id';
    const FIELD_NAME_TWILIO_LOCAL_AREA_CODE_ID  = 'twilio_local_area_code_id';
    const FIELD_NAME_PHONE_NUMBER               = 'phone_number';
    const FIELD_NAME_FRIENDLY_NAME              = 'friendly_name';
    const FIELD_NAME_WAS_DELETED                = 'was_deleted';

    const OPTION_USER = 'user';

    const LABEL_TWILIO_ACCOUNT_ID   = 'Account';
    const LABEL_HTTP_METHOD_TYPE    = 'Method type';
    const LABEL_PHONE_NUMBER_TYPE   = 'Phone number type';


    public function configure() {

        $this->setBionicFormatter();

        $addClass = $this->getOption('addClass', '');
        $addClass = empty($addClass) ? '' : "_$addClass";
        $this->getWidgetSchema()->setNameFormat("incoming_phone_number_form_filter{$addClass}[%s]");

        $this->configureFieldTwilioLocalAreaCode();


        $fields = array(
            self::FIELD_NAME_SID,
            self::FIELD_NAME_TWILIO_ACCOUNT_ID,
            self::FIELD_NAME_PHONE_NUMBER_TYPE,
            self::FIELD_NAME_URL,
            self::FIELD_NAME_HTTP_METHOD_TYPE,
            self::FIELD_NAME_PHONE_COUNTRY_ID,
            self::FIELD_NAME_TWILIO_LOCAL_AREA_CODE_ID,
            self::FIELD_NAME_PHONE_NUMBER,
            self::FIELD_NAME_FRIENDLY_NAME,
        );
        $user = $this->getOption(self::OPTION_USER, null);
        if (WasDeletedHelper::ifNeedWasDeletedColumn($user)) {
            $fields = array_merge($fields, array(self::FIELD_NAME_WAS_DELETED));
        }
        $this->useFields($fields);


        $this->getWidget(self::FIELD_NAME_FRIENDLY_NAME)->setOption('with_empty', false);

        $this->getWidgetSchema()->setLabel(self::FIELD_NAME_TWILIO_ACCOUNT_ID, self::LABEL_TWILIO_ACCOUNT_ID);
        $this->getWidgetSchema()->setLabel(self::FIELD_NAME_HTTP_METHOD_TYPE, self::LABEL_HTTP_METHOD_TYPE);
        $this->getWidgetSchema()->setLabel(self::FIELD_NAME_PHONE_NUMBER_TYPE, self::LABEL_PHONE_NUMBER_TYPE);
    }

    protected function configureFieldTwilioLocalAreaCode() {

        $this->configureFiledPhoneNumberType();
        $this->configureFieldPhoneNumberCountryId();

        $fieldName = self::FIELD_NAME_TWILIO_LOCAL_AREA_CODE_ID;

        $url = $this->getOption('ajax_url_local_area_codes', '');

        $formTagName = $this->getName();
        $phoneNumberFieldName           = self::FIELD_NAME_PHONE_NUMBER_TYPE;
        $phoneNumberCountryIdFieldName  = self::FIELD_NAME_PHONE_COUNTRY_ID;

        $autocompleterWidget = new BionicWidgetFormDoctrineJQueryAutocompleter(array(
            'model'                         => 'TwilioLocalAreaCode',
            'url'                           => $url,
            'name_form'                     => $formTagName,
            'name_phone_number'             => $phoneNumberFieldName,
            'name_phone_number_country_id'  => $phoneNumberCountryIdFieldName,
        ));

        $this->setWidget($fieldName, $autocompleterWidget);

        $this->getWidgetSchema()
            ->setLabel($fieldName, 'Area code')
            ->setHelp($fieldName, "Double-click, begin typing in field")
        ;
    }

    protected function configureFiledPhoneNumberType() {

        $fieldName = self::FIELD_NAME_PHONE_NUMBER_TYPE;
        $choices = TwilioLocalAreaCodeTable::findPhoneNumberTypesCoded();

        $this->configureFieldChoiceWithAll($fieldName, $choices);
    }

    protected function configureFieldPhoneNumberCountryId() {

        $fieldName = self::FIELD_NAME_PHONE_COUNTRY_ID;
        $choices = TwilioCountryTable::findAllNames();

        $this->configureFieldChoiceWithAll($fieldName, $choices);
    }

    public function getFields() {

        $fields = parent::getFields();

        $additionalFields = array(
            self::FIELD_NAME_PHONE_NUMBER_TYPE  => self::FIELD_NAME_PHONE_NUMBER_TYPE,
            self::FIELD_NAME_PHONE_COUNTRY_ID   => self::FIELD_NAME_PHONE_COUNTRY_ID,
        );

        $fieldsAll = array_merge($fields, $additionalFields);

        return $fieldsAll;
    }

    protected function addPhoneNumberTypeColumnQuery($query, $field, $value) {

        $rootAlias = $query->getRootAlias();
        $query->innerJoin("$rootAlias.TwilioLocalAreaCode tlac");

        if (!empty($value)) {
            $type = TwilioLocalAreaCodeTable::getNameByShortName($value);
            $type = mysql_escape_string($type);
            $query->andWhere("tlac.phone_number_type LIKE '$type'");
        }

        return $query;
    }

    protected function addPhoneNumberCountryIdColumnQuery($query, $field, $value) {

        $query->leftJoin("tlac.TwilioState ts");

        $countryId = $value;
        if (!empty($countryId)) {
            $query->andWhere("ts.twilio_country_id = $countryId");
        }

        return $query;
    }

    /**
     * See sfFormFilterDoctrine::processValues
     * This method is need because we need on $originalValues in convert%sValue methods
     * @param array $values
     * @return array 
     */
    public function processValues($values) {

        // see if the user has overridden some column setter
        $originalValues = $values;
        foreach ($originalValues as $field => $value) {
            if (method_exists($this, $method = sprintf('convert%sValue', self::camelize($field)))) {
                if (false === $ret = $this->$method($value, $originalValues)) {
                    unset($values[$field]);
                } else {
                    $values[$field] = $ret;
                }
            }
        }

        return $values;
    }

    protected function convertPhoneNumberCountryIdValue($value, $originalValues) {

        $phoneNumberType = array_key_exists(self::FIELD_NAME_PHONE_NUMBER_TYPE, $originalValues) 
            ? $originalValues[self::FIELD_NAME_PHONE_NUMBER_TYPE] 
            : null
        ;
        if (!empty($phoneNumberType) && $phoneNumberType == TwilioLocalAreaCodeTable::TYPE_SHORT_TOLL_FREE) {
            $value = false;
        }

        return $value;
    }
}
