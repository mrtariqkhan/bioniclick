<?php

/**
 * Advertiser filter form.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class AdvertiserFormFilter extends BaseAdvertiserFormFilter
{
  public function configure()
  {
     parent::configure();

        unset(
            $this['was_deleted']

        );
  }
}
