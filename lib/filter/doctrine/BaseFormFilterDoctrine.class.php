<?php

/**
 * Project filter form base class.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterBaseTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
abstract class BaseFormFilterDoctrine extends sfFormFilterDoctrine {

    const BY_ALL = 0;
    const NAME_BY_ALL = 'All';


    public function setup() {

    }

    protected function setBionicFormatter() {
        BaseFormDoctrine::setBionicFormatterToForm($this);
    }


    protected function configureFieldChoiceWithAll(
        $fieldName, 
        array $choices, 
        $default = null
    ) {

        $choicesWithAll = array(self::BY_ALL => self::NAME_BY_ALL);
        foreach ($choices as $id => $choice) {
            $choicesWithAll[$id] = $choice;
        }
        $keys = array_keys($choicesWithAll);

        $this
            ->setWidget(
                $fieldName,
                new sfWidgetFormChoice(
                    array(
                        'choices' => $choicesWithAll,
                    )
                )
            )
            ->setValidator(
                $fieldName,
                new sfValidatorChoice(
                    array(
                        'required' => true, 
                        'choices' => $keys,
                    )
                )
            )
        ;

        if (!is_null($default)) {
            $this->getWidget($fieldName)->setDefault($default);
        }
    }
}
