<?php

/**
 * Combo filter form.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class ComboFormFilter extends BaseComboFormFilter {

     public function configure() {
        parent::configure();

        unset(
//            $this['name'],
            $this['page_id'],
            $this['updated_at'],
            $this['created_at'],
            $this['was_deleted']

        );
        /*if ($this->hasDefault('campaign_id')) {
            $query = ComboTable::createQueryByUserIdAndCampaignId(null, $this->getDefault('campaign_id'));
            $this->getWidget('twilio_incoming_phone_number_id')->setOption('query', $query);
        }*/

//        $this->setWidget(
//            'api_key',
//            new sfWidgetFormFilterInput(
//                array(
//                    'query'     => ComboTable::findByNameAndApiKey(),
////                    'model'     => 'Combo',
//                    'add_empty' => false
//                )
//            )
//        );



        //$this->widgetSchema->moveField('api_key', sfWidgetFormSchema::FIRST);
        //$this->getWidgetSchema()->setLabel('api_key', 'Api key');

    }
}
