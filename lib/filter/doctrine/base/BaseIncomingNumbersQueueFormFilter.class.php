<?php

/**
 * IncomingNumbersQueue filter form base class.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseIncomingNumbersQueueFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'advertiser_id'             => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Advertiser'), 'add_empty' => true)),
      'twilio_local_area_code_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioLocalAreaCode'), 'add_empty' => true)),
      'campaign_id'               => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Campaign'), 'add_empty' => true)),
      'amount'                    => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'done_amount'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'sf_guard_user_id'          => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('User'), 'add_empty' => true)),
      'created_date'              => new sfWidgetFormFilterInput(),
      'updated_date'              => new sfWidgetFormFilterInput(),
      'status'                    => new sfWidgetFormChoice(array('choices' => array('' => '', 'Open' => 'Open', 'In Progress' => 'In Progress', 'Done' => 'Done', 'Closed' => 'Closed'))),
      'order_type'                => new sfWidgetFormChoice(array('choices' => array('' => '', 'PHONE NUMBER: PURCHASE' => 'PHONE NUMBER: PURCHASE', 'PHONE NUMBER: PURCHASE AND ASSIGN' => 'PHONE NUMBER: PURCHASE AND ASSIGN', 'PHONE NUMBER: ASSIGN' => 'PHONE NUMBER: ASSIGN', 'PHONE NUMBER: RESUME FROM CAMPAIGN' => 'PHONE NUMBER: RESUME FROM CAMPAIGN', 'PHONE NUMBER: RESUME FROM ADVERTISER' => 'PHONE NUMBER: RESUME FROM ADVERTISER'))),
      'is_blocked'                => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'author_notes'              => new sfWidgetFormFilterInput(),
      'created_at'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'                => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'advertiser_id'             => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Advertiser'), 'column' => 'id')),
      'twilio_local_area_code_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TwilioLocalAreaCode'), 'column' => 'id')),
      'campaign_id'               => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Campaign'), 'column' => 'id')),
      'amount'                    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'done_amount'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'sf_guard_user_id'          => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('User'), 'column' => 'id')),
      'created_date'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'updated_date'              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'status'                    => new sfValidatorChoice(array('required' => false, 'choices' => array('Open' => 'Open', 'In Progress' => 'In Progress', 'Done' => 'Done', 'Closed' => 'Closed'))),
      'order_type'                => new sfValidatorChoice(array('required' => false, 'choices' => array('PHONE NUMBER: PURCHASE' => 'PHONE NUMBER: PURCHASE', 'PHONE NUMBER: PURCHASE AND ASSIGN' => 'PHONE NUMBER: PURCHASE AND ASSIGN', 'PHONE NUMBER: ASSIGN' => 'PHONE NUMBER: ASSIGN', 'PHONE NUMBER: RESUME FROM CAMPAIGN' => 'PHONE NUMBER: RESUME FROM CAMPAIGN', 'PHONE NUMBER: RESUME FROM ADVERTISER' => 'PHONE NUMBER: RESUME FROM ADVERTISER'))),
      'is_blocked'                => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'author_notes'              => new sfValidatorPass(array('required' => false)),
      'created_at'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('incoming_numbers_queue_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'IncomingNumbersQueue';
  }

  public function getFields()
  {
    return array(
      'id'                        => 'Number',
      'advertiser_id'             => 'ForeignKey',
      'twilio_local_area_code_id' => 'ForeignKey',
      'campaign_id'               => 'ForeignKey',
      'amount'                    => 'Number',
      'done_amount'               => 'Number',
      'sf_guard_user_id'          => 'ForeignKey',
      'created_date'              => 'Number',
      'updated_date'              => 'Number',
      'status'                    => 'Enum',
      'order_type'                => 'Enum',
      'is_blocked'                => 'Boolean',
      'author_notes'              => 'Text',
      'created_at'                => 'Date',
      'updated_at'                => 'Date',
    );
  }
}
