<?php

/**
 * Package filter form base class.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasePackageFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'company_id'                   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Company'), 'add_empty' => true)),
      'name'                         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'monthly_fee'                  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'toll_free_number_monthly_fee' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'local_number_monthly_fee'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'toll_free_number_per_minute'  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'local_number_per_minute'      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'was_deleted'                  => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'created_at'                   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'                   => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'company_id'                   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Company'), 'column' => 'id')),
      'name'                         => new sfValidatorPass(array('required' => false)),
      'monthly_fee'                  => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'toll_free_number_monthly_fee' => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'local_number_monthly_fee'     => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'toll_free_number_per_minute'  => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'local_number_per_minute'      => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'was_deleted'                  => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'created_at'                   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                   => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('package_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Package';
  }

  public function getFields()
  {
    return array(
      'id'                           => 'Number',
      'company_id'                   => 'ForeignKey',
      'name'                         => 'Text',
      'monthly_fee'                  => 'Number',
      'toll_free_number_monthly_fee' => 'Number',
      'local_number_monthly_fee'     => 'Number',
      'toll_free_number_per_minute'  => 'Number',
      'local_number_per_minute'      => 'Number',
      'was_deleted'                  => 'Boolean',
      'created_at'                   => 'Date',
      'updated_at'                   => 'Date',
    );
  }
}
