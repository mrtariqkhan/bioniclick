<?php

/**
 * Widget filter form base class.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedInheritanceTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseWidgetFormFilter extends WidgetParentFormFilter
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema   ['html_content'] = new sfWidgetFormFilterInput();
    $this->validatorSchema['html_content'] = new sfValidatorPass(array('required' => false));

    $this->widgetSchema->setNameFormat('widget_filters[%s]');
  }

  public function getModelName()
  {
    return 'Widget';
  }

  public function getFields()
  {
    return array_merge(parent::getFields(), array(
      'html_content' => 'Text',
    ));
  }
}
