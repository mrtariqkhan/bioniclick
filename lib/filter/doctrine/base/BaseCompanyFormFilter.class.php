<?php

/**
 * Company filter form base class.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCompanyFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'                          => new sfWidgetFormFilterInput(),
      'url'                           => new sfWidgetFormFilterInput(),
      'default_phone_number'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'authorize_customer_profile_id' => new sfWidgetFormFilterInput(),
      'is_debtor'                     => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'skin'                          => new sfWidgetFormFilterInput(),
      'package_id'                    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Package'), 'add_empty' => true)),
      'was_deleted'                   => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'lft'                           => new sfWidgetFormFilterInput(),
      'rgt'                           => new sfWidgetFormFilterInput(),
      'level'                         => new sfWidgetFormFilterInput(),
      'created_at'                    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'                    => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'name'                          => new sfValidatorPass(array('required' => false)),
      'url'                           => new sfValidatorPass(array('required' => false)),
      'default_phone_number'          => new sfValidatorPass(array('required' => false)),
      'authorize_customer_profile_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_debtor'                     => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'skin'                          => new sfValidatorPass(array('required' => false)),
      'package_id'                    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Package'), 'column' => 'id')),
      'was_deleted'                   => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'lft'                           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'rgt'                           => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'level'                         => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'                    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                    => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('company_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Company';
  }

  public function getFields()
  {
    return array(
      'id'                            => 'Number',
      'name'                          => 'Text',
      'url'                           => 'Text',
      'default_phone_number'          => 'Text',
      'authorize_customer_profile_id' => 'Number',
      'is_debtor'                     => 'Boolean',
      'skin'                          => 'Text',
      'package_id'                    => 'ForeignKey',
      'was_deleted'                   => 'Boolean',
      'lft'                           => 'Number',
      'rgt'                           => 'Number',
      'level'                         => 'Number',
      'created_at'                    => 'Date',
      'updated_at'                    => 'Date',
    );
  }
}
