<?php

/**
 * WidgetParent filter form base class.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseWidgetParentFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'html_type'   => new sfWidgetFormChoice(array('choices' => array('' => '', 'div' => 'div', 'span' => 'span'))),
      'html_id'     => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'combo_id'    => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Combo'), 'add_empty' => true)),
      'was_deleted' => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'created_at'  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'html_type'   => new sfValidatorChoice(array('required' => false, 'choices' => array('div' => 'div', 'span' => 'span'))),
      'html_id'     => new sfValidatorPass(array('required' => false)),
      'combo_id'    => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Combo'), 'column' => 'id')),
      'was_deleted' => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'created_at'  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('widget_parent_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'WidgetParent';
  }

  public function getFields()
  {
    return array(
      'id'          => 'Number',
      'html_type'   => 'Enum',
      'html_id'     => 'Text',
      'combo_id'    => 'ForeignKey',
      'was_deleted' => 'Boolean',
      'created_at'  => 'Date',
      'updated_at'  => 'Date',
    );
  }
}
