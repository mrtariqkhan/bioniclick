<?php

/**
 * Campaign filter form base class.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCampaignFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'name'                  => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'domain'                => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'api_key'               => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'conversion_url'        => new sfWidgetFormFilterInput(),
      'is_enabled'            => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'always_convert'        => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'is_recordings_enabled' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'dynamic_number_expiry' => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'advertiser_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Advertiser'), 'add_empty' => true)),
      'kind'                  => new sfWidgetFormChoice(array('choices' => array('' => '', 'Web Pages' => 'Web Pages', 'Yellow Pages' => 'Yellow Pages', 'Billboards' => 'Billboards', 'Email' => 'Email', 'Other' => 'Other'))),
      'was_deleted'           => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'created_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'name'                  => new sfValidatorPass(array('required' => false)),
      'domain'                => new sfValidatorPass(array('required' => false)),
      'api_key'               => new sfValidatorPass(array('required' => false)),
      'conversion_url'        => new sfValidatorPass(array('required' => false)),
      'is_enabled'            => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'always_convert'        => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'is_recordings_enabled' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'dynamic_number_expiry' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'advertiser_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Advertiser'), 'column' => 'id')),
      'kind'                  => new sfValidatorChoice(array('required' => false, 'choices' => array('Web Pages' => 'Web Pages', 'Yellow Pages' => 'Yellow Pages', 'Billboards' => 'Billboards', 'Email' => 'Email', 'Other' => 'Other'))),
      'was_deleted'           => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'created_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('campaign_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'Campaign';
  }

  public function getFields()
  {
    return array(
      'id'                    => 'Number',
      'name'                  => 'Text',
      'domain'                => 'Text',
      'api_key'               => 'Text',
      'conversion_url'        => 'Text',
      'is_enabled'            => 'Boolean',
      'always_convert'        => 'Boolean',
      'is_recordings_enabled' => 'Number',
      'dynamic_number_expiry' => 'Number',
      'advertiser_id'         => 'ForeignKey',
      'kind'                  => 'Enum',
      'was_deleted'           => 'Boolean',
      'created_at'            => 'Date',
      'updated_at'            => 'Date',
    );
  }
}
