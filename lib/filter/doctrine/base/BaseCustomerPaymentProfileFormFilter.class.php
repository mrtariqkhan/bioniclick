<?php

/**
 * CustomerPaymentProfile filter form base class.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCustomerPaymentProfileFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'authorize_customer_payment_profile_id' => new sfWidgetFormFilterInput(),
      'credit_card_type'                      => new sfWidgetFormChoice(array('choices' => array('' => '', 'Visa' => 'Visa', 'Mastercard' => 'Mastercard', 'Amex' => 'Amex'))),
      'card_code'                             => new sfWidgetFormFilterInput(),
      'advertiser_id'                         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Advertiser'), 'add_empty' => true)),
      'company_id'                            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Company'), 'add_empty' => true)),
      'priority'                              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'was_temporary_revoked'                 => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'created_at'                            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'                            => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'authorize_customer_payment_profile_id' => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'credit_card_type'                      => new sfValidatorChoice(array('required' => false, 'choices' => array('Visa' => 'Visa', 'Mastercard' => 'Mastercard', 'Amex' => 'Amex'))),
      'card_code'                             => new sfValidatorPass(array('required' => false)),
      'advertiser_id'                         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Advertiser'), 'column' => 'id')),
      'company_id'                            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Company'), 'column' => 'id')),
      'priority'                              => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'was_temporary_revoked'                 => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'created_at'                            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                            => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('customer_payment_profile_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CustomerPaymentProfile';
  }

  public function getFields()
  {
    return array(
      'id'                                    => 'Number',
      'authorize_customer_payment_profile_id' => 'Number',
      'credit_card_type'                      => 'Enum',
      'card_code'                             => 'Text',
      'advertiser_id'                         => 'ForeignKey',
      'company_id'                            => 'ForeignKey',
      'priority'                              => 'Number',
      'was_temporary_revoked'                 => 'Boolean',
      'created_at'                            => 'Date',
      'updated_at'                            => 'Date',
    );
  }
}
