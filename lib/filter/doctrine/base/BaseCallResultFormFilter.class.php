<?php

/**
 * CallResult filter form base class.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseCallResultFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'twilio_incoming_call_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioIncomingCall'), 'add_empty' => true)),
      'rating'                  => new sfWidgetFormChoice(array('choices' => array('' => '', 'Sales Call' => 'Sales Call', 'Service Call' => 'Service Call', 'Billing Call' => 'Billing Call', 'Complaint Call' => 'Complaint Call', 'Other Call' => 'Other Call', 'Call Not Answered' => 'Call Not Answered'))),
      'sex'                     => new sfWidgetFormChoice(array('choices' => array('' => '', 'Male' => 'Male', 'Female' => 'Female'))),
      'first_name'              => new sfWidgetFormFilterInput(),
      'last_name'               => new sfWidgetFormFilterInput(),
      'address'                 => new sfWidgetFormFilterInput(),
      'zip'                     => new sfWidgetFormFilterInput(),
      'phone'                   => new sfWidgetFormFilterInput(),
      'revenue'                 => new sfWidgetFormFilterInput(),
      'invoice_number'          => new sfWidgetFormFilterInput(),
      'note'                    => new sfWidgetFormFilterInput(),
      'created_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'              => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'twilio_incoming_call_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TwilioIncomingCall'), 'column' => 'id')),
      'rating'                  => new sfValidatorChoice(array('required' => false, 'choices' => array('Sales Call' => 'Sales Call', 'Service Call' => 'Service Call', 'Billing Call' => 'Billing Call', 'Complaint Call' => 'Complaint Call', 'Other Call' => 'Other Call', 'Call Not Answered' => 'Call Not Answered'))),
      'sex'                     => new sfValidatorChoice(array('required' => false, 'choices' => array('Male' => 'Male', 'Female' => 'Female'))),
      'first_name'              => new sfValidatorPass(array('required' => false)),
      'last_name'               => new sfValidatorPass(array('required' => false)),
      'address'                 => new sfValidatorPass(array('required' => false)),
      'zip'                     => new sfValidatorPass(array('required' => false)),
      'phone'                   => new sfValidatorPass(array('required' => false)),
      'revenue'                 => new sfValidatorPass(array('required' => false)),
      'invoice_number'          => new sfValidatorPass(array('required' => false)),
      'note'                    => new sfValidatorPass(array('required' => false)),
      'created_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'              => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('call_result_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'CallResult';
  }

  public function getFields()
  {
    return array(
      'id'                      => 'Number',
      'twilio_incoming_call_id' => 'ForeignKey',
      'rating'                  => 'Enum',
      'sex'                     => 'Enum',
      'first_name'              => 'Text',
      'last_name'               => 'Text',
      'address'                 => 'Text',
      'zip'                     => 'Text',
      'phone'                   => 'Text',
      'revenue'                 => 'Text',
      'invoice_number'          => 'Text',
      'note'                    => 'Text',
      'created_at'              => 'Date',
      'updated_at'              => 'Date',
    );
  }
}
