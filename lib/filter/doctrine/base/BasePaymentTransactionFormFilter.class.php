<?php

/**
 * PaymentTransaction filter form base class.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BasePaymentTransactionFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'customer_payment_profile_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('CustomerPaymentProfile'), 'add_empty' => true)),
      'customer_first_name'         => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'customer_last_name'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'customer_email'              => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'customer_company'            => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'card_number'                 => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'card_first_name'             => new sfWidgetFormFilterInput(),
      'card_last_name'              => new sfWidgetFormFilterInput(),
      'card_address'                => new sfWidgetFormFilterInput(),
      'card_zip'                    => new sfWidgetFormFilterInput(),
      'amount'                      => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'                  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'                  => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'customer_payment_profile_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('CustomerPaymentProfile'), 'column' => 'id')),
      'customer_first_name'         => new sfValidatorPass(array('required' => false)),
      'customer_last_name'          => new sfValidatorPass(array('required' => false)),
      'customer_email'              => new sfValidatorPass(array('required' => false)),
      'customer_company'            => new sfValidatorPass(array('required' => false)),
      'card_number'                 => new sfValidatorPass(array('required' => false)),
      'card_first_name'             => new sfValidatorPass(array('required' => false)),
      'card_last_name'              => new sfValidatorPass(array('required' => false)),
      'card_address'                => new sfValidatorPass(array('required' => false)),
      'card_zip'                    => new sfValidatorPass(array('required' => false)),
      'amount'                      => new sfValidatorSchemaFilter('text', new sfValidatorNumber(array('required' => false))),
      'created_at'                  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                  => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('payment_transaction_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'PaymentTransaction';
  }

  public function getFields()
  {
    return array(
      'id'                          => 'Number',
      'customer_payment_profile_id' => 'ForeignKey',
      'customer_first_name'         => 'Text',
      'customer_last_name'          => 'Text',
      'customer_email'              => 'Text',
      'customer_company'            => 'Text',
      'card_number'                 => 'Text',
      'card_first_name'             => 'Text',
      'card_last_name'              => 'Text',
      'card_address'                => 'Text',
      'card_zip'                    => 'Text',
      'amount'                      => 'Number',
      'created_at'                  => 'Date',
      'updated_at'                  => 'Date',
    );
  }
}
