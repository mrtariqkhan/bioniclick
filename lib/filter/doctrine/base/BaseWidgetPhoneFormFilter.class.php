<?php

/**
 * WidgetPhone filter form base class.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedInheritanceTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseWidgetPhoneFormFilter extends WidgetParentFormFilter
{
  protected function setupInheritance()
  {
    parent::setupInheritance();

    $this->widgetSchema   ['html_content_1'] = new sfWidgetFormFilterInput();
    $this->validatorSchema['html_content_1'] = new sfValidatorPass(array('required' => false));

    $this->widgetSchema   ['html_content_2'] = new sfWidgetFormFilterInput();
    $this->validatorSchema['html_content_2'] = new sfValidatorPass(array('required' => false));

    $this->widgetSchema   ['physical_phone_number_id'] = new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PhysicalPhoneNumber'), 'add_empty' => true));
    $this->validatorSchema['physical_phone_number_id'] = new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PhysicalPhoneNumber'), 'column' => 'id'));

    $this->widgetSchema   ['number_format_id'] = new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('NumberFormat'), 'add_empty' => true));
    $this->validatorSchema['number_format_id'] = new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('NumberFormat'), 'column' => 'id'));

    $this->widgetSchema->setNameFormat('widget_phone_filters[%s]');
  }

  public function getModelName()
  {
    return 'WidgetPhone';
  }

  public function getFields()
  {
    return array_merge(parent::getFields(), array(
      'html_content_1' => 'Text',
      'html_content_2' => 'Text',
      'physical_phone_number_id' => 'ForeignKey',
      'number_format_id' => 'ForeignKey',
    ));
  }
}
