<?php

/**
 * AnalyticsPhoneNumber filter form base class.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseAnalyticsPhoneNumberFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'visitor_analytics_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('VisitorAnalytics'), 'add_empty' => true)),
      'twilio_incoming_phone_number_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioIncomingPhoneNumber'), 'add_empty' => true)),
      'created_at'                      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'                      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'visitor_analytics_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('VisitorAnalytics'), 'column' => 'id')),
      'twilio_incoming_phone_number_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TwilioIncomingPhoneNumber'), 'column' => 'id')),
      'created_at'                      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('analytics_phone_number_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'AnalyticsPhoneNumber';
  }

  public function getFields()
  {
    return array(
      'id'                              => 'Number',
      'visitor_analytics_id'            => 'ForeignKey',
      'twilio_incoming_phone_number_id' => 'ForeignKey',
      'created_at'                      => 'Date',
      'updated_at'                      => 'Date',
    );
  }
}
