<?php

/**
 * AdvertiserIncomingNumberPool filter form base class.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseAdvertiserIncomingNumberPoolFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'advertiser_id'                   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Advertiser'), 'add_empty' => true)),
      'twilio_incoming_phone_number_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('TwilioIncomingPhoneNumber'), 'add_empty' => true)),
      'campaign_id'                     => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Campaign'), 'add_empty' => true)),
      'is_default'                      => new sfWidgetFormChoice(array('choices' => array('' => 'yes or no', 1 => 'yes', 0 => 'no'))),
      'purchased_at'                    => new sfWidgetFormFilterInput(),
      'created_at'                      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'                      => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'advertiser_id'                   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Advertiser'), 'column' => 'id')),
      'twilio_incoming_phone_number_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('TwilioIncomingPhoneNumber'), 'column' => 'id')),
      'campaign_id'                     => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Campaign'), 'column' => 'id')),
      'is_default'                      => new sfValidatorChoice(array('required' => false, 'choices' => array('', 1, 0))),
      'purchased_at'                    => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'                      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                      => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('advertiser_incoming_number_pool_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'AdvertiserIncomingNumberPool';
  }

  public function getFields()
  {
    return array(
      'id'                              => 'Number',
      'advertiser_id'                   => 'ForeignKey',
      'twilio_incoming_phone_number_id' => 'ForeignKey',
      'campaign_id'                     => 'ForeignKey',
      'is_default'                      => 'Boolean',
      'purchased_at'                    => 'Number',
      'created_at'                      => 'Date',
      'updated_at'                      => 'Date',
    );
  }
}
