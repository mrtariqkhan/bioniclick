<?php

/**
 * VisitorAnalytics filter form base class.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterGeneratedTemplate.php 29570 2010-05-21 14:49:47Z Kris.Wallsmith $
 */
abstract class BaseVisitorAnalyticsFormFilter extends BaseFormFilterDoctrine
{
  public function setup()
  {
    $this->setWidgets(array(
      'analytics_ip_id'            => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('AnalyticsIp'), 'add_empty' => true)),
      'analytics_time_id'          => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'referrer_source_id'         => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('ReferrerSource'), 'add_empty' => true)),
      'combo_id'                   => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('Combo'), 'add_empty' => true)),
      'phone_number_assignment_id' => new sfWidgetFormDoctrineChoice(array('model' => $this->getRelatedModelName('PhoneNumberAssignment'), 'add_empty' => true)),
      'keyword'                    => new sfWidgetFormFilterInput(),
      'traffic_source'             => new sfWidgetFormFilterInput(),
      'time_on_page'               => new sfWidgetFormFilterInput(),
      'is_organic'                 => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'analytics_time'             => new sfWidgetFormFilterInput(array('with_empty' => false)),
      'created_at'                 => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
      'updated_at'                 => new sfWidgetFormFilterDate(array('from_date' => new sfWidgetFormDate(), 'to_date' => new sfWidgetFormDate())),
    ));

    $this->setValidators(array(
      'analytics_ip_id'            => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('AnalyticsIp'), 'column' => 'id')),
      'analytics_time_id'          => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'referrer_source_id'         => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('ReferrerSource'), 'column' => 'id')),
      'combo_id'                   => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('Combo'), 'column' => 'id')),
      'phone_number_assignment_id' => new sfValidatorDoctrineChoice(array('required' => false, 'model' => $this->getRelatedModelName('PhoneNumberAssignment'), 'column' => 'id')),
      'keyword'                    => new sfValidatorPass(array('required' => false)),
      'traffic_source'             => new sfValidatorPass(array('required' => false)),
      'time_on_page'               => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'is_organic'                 => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'analytics_time'             => new sfValidatorSchemaFilter('text', new sfValidatorInteger(array('required' => false))),
      'created_at'                 => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
      'updated_at'                 => new sfValidatorDateRange(array('required' => false, 'from_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 00:00:00')), 'to_date' => new sfValidatorDateTime(array('required' => false, 'datetime_output' => 'Y-m-d 23:59:59')))),
    ));

    $this->widgetSchema->setNameFormat('visitor_analytics_filters[%s]');

    $this->errorSchema = new sfValidatorErrorSchema($this->validatorSchema);

    $this->setupInheritance();

    parent::setup();
  }

  public function getModelName()
  {
    return 'VisitorAnalytics';
  }

  public function getFields()
  {
    return array(
      'id'                         => 'Number',
      'analytics_ip_id'            => 'ForeignKey',
      'analytics_time_id'          => 'Number',
      'referrer_source_id'         => 'ForeignKey',
      'combo_id'                   => 'ForeignKey',
      'phone_number_assignment_id' => 'ForeignKey',
      'keyword'                    => 'Text',
      'traffic_source'             => 'Text',
      'time_on_page'               => 'Number',
      'is_organic'                 => 'Number',
      'analytics_time'             => 'Number',
      'created_at'                 => 'Date',
      'updated_at'                 => 'Date',
    );
  }
}
