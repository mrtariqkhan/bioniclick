<?php

/**
 * AdvertiserIncomingNumberPool filter form.
 *
 * @package    bionic
 * @subpackage filter
 * @author     Your name here
 * @version    SVN: $Id: sfDoctrineFormFilterTemplate.php 23810 2009-11-12 11:07:44Z Kris.Wallsmith $
 */
class AdvertiserIncomingNumberPoolFormFilter extends BaseAdvertiserIncomingNumberPoolFormFilter {

//    public function configure() {
//
//        parent::configure();
//        $this->useFields(array(
//            'twilio_incoming_phone_number_id',
//            'campaign_id'
//        ));
//
//        $user = $this->getOption('user');
//        $advertisersIds = $user->findSubAdvertisersIds();
//
//        $this->configureFieldCampaign($advertisersIds);
//        $this->configureFieldIncomingPhoneNumber($advertisersIds);
//
//        $this->setFieldsOrder();
//    }
//
//    protected function configureFieldCampaign($advertisersIds) {
//
//        $fieldName = 'campaign_id';
//
//        $query = CampaignTable::createQueryByAdvertiserId($advertisersIds);
//        $this->getWidget($fieldName)
//            ->setOption('query', $query)
//            ->setOption('order_by', array('name', 'asc'))
//        ;
//        $this->getValidator($fieldName)
//            ->setOption('query', $query)
//        ;
//        $this->getWidgetSchema()->setLabel($fieldName, 'Campaign');
//    }
//
//    protected function configureFieldIncomingPhoneNumber($advertisersIds) {
//
//        $fieldName = 'twilio_incoming_phone_number_id';
//
//        $query = TwilioIncomingPhoneNumberTable::createQueryByAdvertiserId($advertisersIds);
//        $this->getWidget($fieldName)
//            ->setOption('query', $query)
//            ->setOption('order_by', array('phone_number', 'asc'))
//        ;
//        $this->getValidator($fieldName)
//            ->setOption('query', $query)
//        ;
//        $this->getWidgetSchema()->setLabel($fieldName, 'Incoming phone number');
//    }
//
//    protected function setFieldsOrder() {
//        $this->widgetSchema->moveField('campaign_id', sfWidgetFormSchema::FIRST);
//    }
}
