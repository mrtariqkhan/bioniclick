<?php

/**
 * BionicWidgetFormSelectCheckboxTabled represents an array of checkboxes.
 * 
 *    //NOTE:: does not work with grouped checkboxes yet
 */
class BionicWidgetFormSelectCheckboxTabled extends sfWidgetFormSelectCheckbox {

    /**
    * Constructor.
    *
    * Available options:
    *
    *  * titles:           The tiltes of methods to use to display object values array(title1, title2,...)
    *  * container-class:  The class for table container
    *
    */
    protected function configure($options = array(), $attributes = array()) {

        parent::configure($options, $attributes);

        $this->addOption('titles', array());
        $this->addOption('container-class', 'form-checkbox-list-tabled-wrap');

        $this->setOption('class', 'form-checkbox-list-tabled table');
    }

    /**
     *
     * @param type $widget
     * @param type $inputs array(id => array('input' => value, 'labels' => array(title => value))) 
     * @return string 
     */
    public function formatter($widget, $inputs) {

        $titles = $this->getOption('titles', array());
        $titlesAll = array_merge(array(''), array_values($titles));

        $headTr = '';
        $ths = array();
        foreach ($titlesAll as $i => $title) {
            $class = '';
            if ($i == 0) {
                $class = "$class first";
            }
            if ($i == count($titlesAll) - 1) {
                $class = "$class last";
            }

            $ths[] = $this->renderContentTag('th', $title, array('class' => $class));
        }
        $headTr = $this->renderContentTag('tr', implode(' ', $ths));
        $head = $this->renderContentTag('thead', $headTr);

        $i = 1;
        $rowClass = '';
        $trsArray = array();
        foreach ($inputs as $input) {
            $rowClass = ($i%2) ? 'odd' : 'even';
            $tds = array();

            $tds[] = $this->renderContentTag('td', $input['input']);
            $labels = $input['labels'];

            foreach ($titles as $title ){
                $label = array_key_exists($title, $labels) ? $labels[$title] : '';
                $tds[] = $this->renderContentTag('td', $label);
            }

            $trsArray[] = $this->renderContentTag('tr', implode(' ', $tds), array('class' => $rowClass));

            $i++;
        }
        $trs = empty($trsArray) ? '' : implode(' ', $trsArray);
        $body = $this->renderContentTag('tbody', $trs);

        $table = empty($trsArray) ? '' : $this->renderContentTag('table', $head . $body, array('class' => $this->getOption('class')));

        $result = $this->renderContentTag('div', $table, array('class' => $this->getOption('container-class')));

        return $result;
    }

    /**
     * 
     * @param type $name
     * @param type $value
     * @param type $choices   is (array[key] = array(title => value))
     * @param type $attributes
     * @return string 
     */
    protected function formatChoices($name, $value, $choices, $attributes) {

        $inputs = array();
        foreach ($choices as $key => $choiceLabels)  {
            $baseAttributes = array(
                'name'  => $name,
                'type'  => 'checkbox',
                'value' => self::escapeOnce($key),
                'id'    => $id = $this->generateId($name, self::escapeOnce($key)),
            );

            if ((is_array($value) && in_array(strval($key), $value)) || strval($key) == strval($value)) {
                $baseAttributes['checked'] = 'checked';
            }

            $labels = array();
            foreach ($choiceLabels as $title => $label) {
                $labels[$title] = $this->renderContentTag('label', self::escapeOnce($label), array('for' => $id));
            }
            $inputs[$id] = array(
                'input'         => $this->renderTag('input', array_merge($baseAttributes, $attributes)),
                'labels'        => $labels,
            );
        }

        return call_user_func($this->getOption('formatter'), $this, $inputs);
    }

    //NOTE:: does not work with grouped checkboxes
    public function render($name, $value = null, $attributes = array(), $errors = array()) {

        if ('[]' != substr($name, -2)) {
            $name .= '[]';
        }

        if (null === $value) {
            $value = array();
        }

        $choices = $this->getChoices();

        $choices = is_array(current($choices)) ? $choices : array();

        return $this->formatChoices($name, $value, $choices, $attributes);
    }
}
