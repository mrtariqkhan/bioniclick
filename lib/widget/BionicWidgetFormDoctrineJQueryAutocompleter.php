<?php

class BionicWidgetFormDoctrineJQueryAutocompleter extends sfWidgetFormDoctrineJQueryAutocompleter {

    public function __construct($options = array(), $attributes = array()) {

        parent::__construct($options, $attributes);

        $this->setOptionConfig();
    }

    protected function configure($options = array(), $attributes = array()) {

        //TODO:: think if necessary name_form, name_phone_number parameters
        $this->addRequiredOption('name_form');
        $this->addRequiredOption('name_phone_number');
        $this->addRequiredOption('name_phone_number_country_id');
        $this->addOption('advertiser_id', 0);
        $this->addOption('name_number_count', '');
        $this->addOption('url_number_count', '');
        $this->addOption('text_number_count_prefix', '');
        $this->addOption('text_number_count_postfix', '');

        parent::configure($options, $attributes);
    }

    protected function setOptionConfig() {

        $formTagName                    = $this->getOption('name_form');
        $phoneNumberTypeFieldName       = $this->getOption('name_phone_number');
        $phoneNumberCountryIdFieldName  = $this->getOption('name_phone_number_country_id');
        $advertiserId                   = $this->getOption('advertiser_id', 0);

        $elementIdPhoneNumberType       = "{$formTagName}_{$phoneNumberTypeFieldName}";
        $elementIdPhoneNumberCountryId  = "{$formTagName}_{$phoneNumberCountryIdFieldName}";

        $tollShort = TwilioLocalAreaCodeTable::TYPE_SHORT_TOLL_FREE;

        //NOTE:: see /doctrine/plugins/sfFormExtraPlugin/web/js/jquery.autocompleter.js variable $.Autocompleter.defaults
        $config = <<<EOF
{
//    inputClass: "ac_input",
//    resultsClass: "ac_results",
//    loadingClass: "ac_loading",
    minChars: 0, //1,
//    delay: 400,
//    matchCase: false,
//    matchSubset: true,
//    matchContains: false,
    cacheLength: 0, //10,
    max: 100,
//    mustMatch: false,
    extraParams: {
        phone_number_type_short : function () {
            //NOTE:: this is temporary solution, because we have no element from form in this method
            var result = '';
            var phoneTypeElement = jQuery('#$elementIdPhoneNumberType:visible');
            if (phoneTypeElement.length > 0) {
                result = phoneTypeElement.find('option:selected').val();
            }
            return result;
        }
        , phone_number_country_id : function () {
            //NOTE:: this is temporary solution, because we have no element from form in this method
            var result = '';
            var phoneTypeElement = jQuery('#$elementIdPhoneNumberType:visible');
            if (phoneTypeElement.length > 0 && phoneTypeElement.val() != '$tollShort') {
                var phoneCountryElement = jQuery('#$elementIdPhoneNumberCountryId:visible');
                if (phoneCountryElement.length > 0) {
                    result = phoneCountryElement.find('option:selected').val();
                }
            }
            return result;
        }
        , advertiser_id : function () {
            return $advertiserId;
        }
    },
//    selectFirst: true,
//    formatItem: function(row) { return row[0]; },
//    formatMatch: null,
//    autoFill: false,
//    width: 0,
//    multiple: false,
//    multipleSeparator: ", ",
//    highlight: function(value, term) {
//        return value.replace(new RegExp("(?![^&;]+;)(?!<[^<>]*)(" + term.replace(/([\^\$\(\)\[\]\{\}\*\.\+\?\|\\])/gi, "\\$1") + ")(?![^<>]*>)(?![^&;]+;)", "gi"), "<strong>$1</strong>");
//    },
//    scroll: true,
    scrollHeight: 300 //100
}
EOF
;
        $this->setOption('config', $config);
    }

    /**
     * See sfWidgetFormJQueryAutocompleter::render method
     */
    public function render($name, $value = null, $attributes = array(), $errors = array()) {

        $formTagName                    = $this->getOption('name_form');
        $phoneNumberFieldName           = $this->getOption('name_phone_number');
        $phoneNumberCountryIdFieldName  = $this->getOption('name_phone_number_country_id');
        $numberCountFieldName           = $this->getOption('name_number_count', '');
        $urlNumberCount                 = $this->getOption('url_number_count', '');
        $advertiserId                   = $this->getOption('advertiser_id', null);

        $textNumberCountPrefix          = $this->getOption('text_number_count_prefix', '');
        $textNumberCountPostfix         = $this->getOption('text_number_count_postfix', '');

        $elementId                      = $this->generateId($name);
        $elementIdAutocompleted         = $this->generateId('autocomplete_' . $name);
        $elementIdPhoneNumberType       = "{$formTagName}_{$phoneNumberFieldName}";
        $elementIdPhoneNumberCountryId  = "{$formTagName}_{$phoneNumberCountryIdFieldName}";
        $elementIdNumberCount           = "{$formTagName}_{$numberCountFieldName}";

        $tollShort = TwilioLocalAreaCodeTable::TYPE_SHORT_TOLL_FREE;

        $url    = $this->getOption('url');
        $config = $this->getOption('config');


        $visibleValue = $this->getOption('value_callback')
            ? call_user_func($this->getOption('value_callback'), $value)
            : $value
        ;

        return
            $this->renderTag('input', array('type' => 'hidden', 'name' => $name, 'value' => $value))
            . sfWidgetFormInput::render('autocomplete_'.$name, $visibleValue, $attributes, $errors)
            . <<<EOF
<script type="text/javascript">
    jQuery(document).ready(function() {


        jQuery("#$elementIdAutocompleted")
        .autocomplete(
            '$url',
            jQuery.extend(
                {},
                {
                    dataType: 'json',
                    parse: function(data) {
                        var parsed = [];
                        for (key in data) {
                            parsed[parsed.length] = { data: [ data[key], key ], value: data[key], result: data[key] };
                        }
                        return parsed;
                    }
                },
                $config
            )
        )
        .result(function(event, data) {
            var formElem = jQuery(this).parents('form:first');
            bionicclickChangeElementIdValue(data[1], formElem);
        });

        jQuery("#$elementIdPhoneNumberType").livequery(function() {
            var formElem = jQuery(this).parents('form:first');
            var formCountryElem = formElem.find('#$elementIdPhoneNumberCountryId');
            var formCountryContainerElem = (formCountryElem.length > 0) ? formCountryElem.parents('div:first') : jQuery('');

            if (formCountryContainerElem.length > 0) {
                if (jQuery(this).val() == '$tollShort') {
                    formCountryContainerElem.hide();
                } else {
                    formCountryContainerElem.show();
                }
            }
        });

        jQuery("#$elementIdNumberCount").livequery(function() {

            //NOTE:: when form is loaded make initialisation of count info element if there is selected LocalAreaCode

            var formElem = jQuery(this).parents('form:first');

            var formLocalCodeElem = formElem.find('#$elementId');
            var localAreaCodeId = (formLocalCodeElem.length > 0) ? formLocalCodeElem.val() : '';

            bionicclickShowPhoneNumbersCountOnArea(localAreaCodeId, formElem);
        });

        jQuery('#$elementIdPhoneNumberType').live('change', function() {
            var formElem = jQuery(this).parents('form:first');
            var formCountryElem         = formElem.find('#$elementIdPhoneNumberCountryId');
            var formAutocompleterElem   = formElem.find('#$elementIdAutocompleted');

            var formCountryContainerElem = (formCountryElem.length > 0) ? formCountryElem.parents('div:first') : jQuery('');
            if (formCountryContainerElem.length > 0) {
                if (jQuery(this).val() == '$tollShort') {
                    formCountryContainerElem.hide(200);
                } else {
                    formCountryContainerElem.show(200);
                }
            }

            if (formAutocompleterElem.length > 0) {
                jQuery(formAutocompleterElem).val('');
            }
            bionicclickChangeElementIdValue('', formElem);
        });

        jQuery("#$elementIdPhoneNumberCountryId").live('change', function() {
            var formElem = jQuery(this).parents('form:first');
            var formAutocompleteLocalCodeElem = formElem.find('#$elementIdAutocompleted');
            if (formAutocompleteLocalCodeElem.length > 0) {
                jQuery(formAutocompleteLocalCodeElem).val('');
            }

            bionicclickChangeElementIdValue('', formElem);
        });

        jQuery("#$elementIdAutocompleted").live('keydown', function(event) {

            var formElem = jQuery(this).parents('form:first');
            var formCountInfoElement = formElem.find('#$elementIdNumberCount').parents('div:first').find('span');
            if (formCountInfoElement.length > 0) {
                formCountInfoElement.html('$textNumberCountPrefix' + 0 + '$textNumberCountPostfix');
            }

            var KEY = {
                DEL: 46,
                BACKSPACE: 8
            };

            var keyCode = event.keyCode;
            if (keyCode == KEY.DEL || keyCode == KEY.BACKSPACE) {
                var formElem = jQuery(this).parents('form:first');
                bionicclickChangeElementIdValue('', formElem);
            }
        });

        function bionicclickChangeElementIdValue(localAreaCodeId, formElem) {
            var formLocalCodeElem = formElem.find('#$elementId');
            if (formLocalCodeElem.length > 0) {
                jQuery(formLocalCodeElem).val(localAreaCodeId);
            }

            bionicclickShowPhoneNumbersCountOnArea(localAreaCodeId, formElem);
        }

        function bionicclickShowPhoneNumbersCountOnArea(localAreaCodeId, formElem) {

            var formCountInfoElement = formElem.find('#$elementIdNumberCount').parents('div:first').find('span');
            if (formCountInfoElement.length <= 0) {
                return;
            }
            formCountInfoElement.html('$textNumberCountPrefix' + 0 + '$textNumberCountPostfix');

            if (localAreaCodeId.length <= 0) {
                return;
            }

            var urlPhoneNumbersCount = '$urlNumberCount';
            var advertiserId = $advertiserId;
            if (!urlPhoneNumbersCount || !advertiserId) {
                return;
            }

            var formLocalCodeElem = formElem.find('#$elementId');
            var localAreaCodeId = (formLocalCodeElem.length > 0) ? formLocalCodeElem.val() : '';
            var params = {
                local_area_code_id: localAreaCodeId,
                advertiser_id: advertiserId
            };

            jQuery.ajax(
                {
                    type: 'GET',
                    url: urlPhoneNumbersCount,
                    dataType: 'json',
                    cache: false,
                    data: params,
                    success: function(response, statusText, xhr) {
                        var count = 0;
                        if (response.count) {
                            count = response.count;
                        }
                        formCountInfoElement.html('$textNumberCountPrefix' + count + '$textNumberCountPostfix');
                    }
                }
            );
        }

    });
</script>
EOF
        ;
    }
}
