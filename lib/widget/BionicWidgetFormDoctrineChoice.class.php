<?php 

/**
 * BionicWidgetFormDoctrineChoice represents a choice widget for a model.
 */
class BionicWidgetFormDoctrineChoice extends sfWidgetFormDoctrineChoice {

    public function __construct($options = array(), $attributes = array()) {

        parent::__construct($options, $attributes);

        $titles = array();
        $methods = $this->getOption('methods');
        foreach ($methods as $methodInfo) {
            $titles[] = $methodInfo['title'];
        }

        $this->setOption('renderer_class', 'BionicWidgetFormSelectCheckboxTabled');
        $this->setOption(
            'renderer_options', 
            array(
                'titles' => $titles,
            )
        );
    }

    /**
    * Constructor.
    *
    * Available options:
    *
    *  * methods:           The methods to use to display object values array(methodName => array('title' => methodTitle, 'parameters' => array()))
    *
    */
    protected function configure($options = array(), $attributes = array()) {

        parent::configure($options, $attributes);

        $this->addOption('methods', array());
    }

    /**
    * Returns the choices associated to the model.
    *
    * @return array An array of choices (array[key] = array(title => value))
    */
    public function getChoices() {

        $choices = array();
        if (false !== $this->getOption('add_empty')) {
            $choices[''] = true === $this->getOption('add_empty') ? '' : $this->translate($this->getOption('add_empty'));
        }

        if (null === $this->getOption('table_method')) {
            $query = 
                null === $this->getOption('query') 
                ? Doctrine_Core::getTable($this->getOption('model'))->createQuery() 
                : $this->getOption('query')
            ;

            if ($order = $this->getOption('order_by')) {
                $query->addOrderBy($order[0] . ' ' . $order[1]);
            }
            $objects = $query->execute();
        } else {
            $tableMethod = $this->getOption('table_method');
            $results = Doctrine_Core::getTable($this->getOption('model'))->$tableMethod();

            if ($results instanceof Doctrine_Query) {
                $objects = $results->execute();
            } elseif ($results instanceof Doctrine_Collection) {
                $objects = $results;
            } elseif ($results instanceof Doctrine_Record) {
                $objects = new Doctrine_Collection($this->getOption('model'));
                $objects[] = $results;
            } else {
                $objects = array();
            }
        }

        $methods = $this->getOption('methods');
        $keyMethod = $this->getOption('key_method');

        foreach ($objects as $object) {
            $values = array();
            foreach ($methods as $methodName => $methodInfo) {
                $title = $methodInfo['title'];
                $value = null;
                if (array_key_exists('parameters', $methodInfo)) {
                    $value = $object->$methodName($methodInfo['parameters']);
                } else {
                    $value = $object->$methodName();
                }
                $values[$title] = $value;
            }
            $choices[$object->$keyMethod()] = $values;
        }

        return $choices;
    }
}