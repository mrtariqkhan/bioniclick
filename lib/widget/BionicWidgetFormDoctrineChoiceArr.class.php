<?php

///**
// * BionicWidgetFormDoctrineChoiceArr represents a choice widget for a model.
// */
//class BionicWidgetFormDoctrineChoiceArr extends sfWidgetFormDoctrineChoice {
//
//    public function __construct($options = array(), $attributes = array()) {
//
//        parent::__construct($options, $attributes);
//
//        $titles = array();
//        $fields = $this->getOption('fields');
//        foreach ($fields as $info) {
//            $titles[] = $info['title'];
//        }
//
//        $this->setOption('renderer_class', 'BionicWidgetFormSelectCheckboxTabled');
//        $this->setOption(
//            'renderer_options', 
//            array(
//                'titles' => $titles,
//            )
//        );
//    }
//
//    /**
//    * Constructor.
//    *
//    * Available options:
//    *
//    *  * fields:           The fields to use to display object values array(methodName => array('title' => methodTitle, 'parameters' => array()))
//    *  * key_field:        The key field (by defaul is 'id')
//    *
//    */
//    protected function configure($options = array(), $attributes = array()) {
//
//        parent::configure($options, $attributes);
//
//        $this->addOption('fields', array());
//        $this->addOption('key_field', 'id');
//    }
//
//    /**
//    * Returns the choices associated to the model.
//    *
//    * @return array An array of choices (array[key] = array(title => value))
//    */
//    public function getChoices() {
//
//        $choices = array();
//        if (false !== $this->getOption('add_empty')) {
//            $choices[''] = true === $this->getOption('add_empty') ? '' : $this->translate($this->getOption('add_empty'));
//        }
//
//        if (null === $this->getOption('table_method')) {
//            $query = 
//                null === $this->getOption('query') 
//                ? Doctrine_Core::getTable($this->getOption('model'))->createQuery() 
//                : $this->getOption('query')
//            ;
//
//            if ($order = $this->getOption('order_by')) {
//                $query->addOrderBy($order[0] . ' ' . $order[1]);
//            }
//            $objectsAsArr = $query->execute(array(), Doctrine_Core::HYDRATE_ARRAY);
//        } else {
//            $tableMethod = $this->getOption('table_method');
//            $results = Doctrine_Core::getTable($this->getOption('model'))->$tableMethod();
//
//            if ($results instanceof Doctrine_Query) {
//                $objectsAsArr = $results->execute();
//            } elseif ($results instanceof Doctrine_Collection) {
//                $objectsAsArr = $results;
//            } elseif ($results instanceof Doctrine_Record) {
//                $objectsAsArr = new Doctrine_Collection($this->getOption('model'));
//                $objectsAsArr[] = $results;
//            } else {
//                $objectsAsArr = array();
//            }
//        }
//
//        $fields = $this->getOption('fields');
//        $keyField = $this->getOption('key_field');
//
//        foreach ($objectsAsArr as $objectAsArr) {
//            $values = array();
//            foreach ($fields as $fieldName => $info) {
//                $title = $info['title'];
//                $value = null;
//                if (array_key_exists($fieldName, $objectAsArr)) {
//                    $value = $objectAsArr[$fieldName];
//                }
//                $values[$title] = $value;
//            }
//
//            $key = array_key_exists($keyField, $objectAsArr) ? $objectAsArr[$keyField] : 0;
//            $choices[$key] = $values;
//        }
//
//        return $choices;
//    }
//}