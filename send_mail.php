<?php 
//define the receiver of the email 
$userId = $argv[1];
$baseUrl =  dirname(__FILE__) . '/web/uploads/temp/';
$encodedMailData = file_get_contents($baseUrl . 'mail' . $userId);
$mailData = json_decode($encodedMailData);

/*$from = $mailData->from; 
$to = $mailData->to; 
$subject = $mailData->subject; 
*/
$from_mail="support@bionicclick.com";
$from_name= "Bionic Click";
//$mail_to= $mailData->to ? $mailData->to : "matt@mattbecker.com";
$mail_to= $mailData->to ? $mailData->to : "hazem.hagrass@badrit.com";
$reply_to = "support@bionicclick.com";
$subject=$mailData->subject;
$message=$mailData->body;

$attachment_path = $baseUrl . 'screenshot' . $userId . '.pdf'; 
//create a boundary string. It must be unique 
//so we use the MD5 algorithm to generate a random hash 
 
/* Attachment File */
    // Attachment location
    $file_name = "bionicclick_snapshot.pdf";
     
    // Read the file content
    $content = chunk_split(base64_encode(file_get_contents($attachment_path)));
/* Set the email header */
    // Generate a boundary
    $boundary = md5(uniqid(time()));
     
    // Email header
    $header = "From: ".$from_name." <".$from_mail.">\r\n";
    $header .= "Reply-To: ".$reply_to."\r\n";
    $header .= "MIME-Version: 1.0\r\n";
     
    // Multipart wraps the Email Content and Attachment
    $header .= "Content-Type: multipart/mixed; boundary=\"".$boundary."\"\r\n";
    $header .= "This is a multi-part message in MIME format.\r\n";
    $header .= "--".$boundary."\r\n";
     
    // Email content
    // Content-type can be text/plain or text/html
    $header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
    $header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
    $header .= "$message\r\n";
    $header .= "--".$boundary."\r\n";
     
    // Attachment
    // Edit content type for different file extensions
    $header .= "Content-Type: application/xml; name=\"".$file_name."\"\r\n";
    $header .= "Content-Transfer-Encoding: base64\r\n";
    $header .= "Content-Disposition: attachment; filename=\"".$file_name."\"\r\n\r\n";
    $header .= $content."\r\n";
    $header .= "--".$boundary."--";
    // Send email

    $mail_sent = mail($mail_to, $subject, "", $header);
    echo $mail_sent ? "success" : "failed"; 
 
?>
