// Create new tag Bionicclick_framework_1.5.1 from branches/Doctrine
svn copy http://svn.bionicclick.com:8080/rotation_sf_repos/Rotation/branches/Doctrine \
    http://svn.bionicclick.com:8080/rotation_sf_repos/Rotation/tags/Bionicclick_framework_1.5.1

// Switch from branches/Doctrine to tag Bionicclick_framework_1.5.1
svn switch http://svn.bionicclick.com:8080/rotation_sf_repos/Rotation/tags/Bionicclick_framework_1.5.1

// Merge tags and your working copy. After merging you should commit changes (see next command)
svn merge --depth=infinity http://svn.bionicclick.com:8080/rotation_sf_repos/Rotation/tags/Bionicclick_framework_1.5.1 .

//Comment after merge should begin with "MERGE:"
svn commit -m "MERGE: your message"





svn switch http://svn.bionicclick.com:8080/rotation_sf_repos/Rotation/trunk
svn switch http://svn.bionicclick.com:8080/rotation_sf_repos/Rotation/branches/Doctrine
svn switch http://svn.bionicclick.com:8080/rotation_sf_repos/Rotation/tags/Bionicclick_framework_1.5.1



// Merge branch and your working copy. After merging you should commit changes (see next comand)
svn merge --depth=infinity http://svn.bionicclick.com:8080/rotation_sf_repos/Rotation/branches/Doctrine .

//Comment after merge should begin with "MERGE:"
svn commit -m "MERGE: your message"





svn delete http://svn.bionicclick.com:8080/rotation_sf_repos/Rotation/tags/production_15_07_2011_last -m"Unnecessary tag has been deleted"


------ PRODUCTION: -----------------------------------------------

// Create new tag production_3_6___07_02_2012 from trunk
svn copy http://svn.bionicclick.com:8080/rotation_sf_repos/Rotation/trunk \
    http://svn.bionicclick.com:8080/rotation_sf_repos/Rotation/tags/production_3_6___07_02_2012


svn switch http://svn.bionicclick.com:8080/rotation_sf_repos/Rotation/tags/production_3_6___07_02_2012

svn switch http://svn.bionicclick.com:8080/rotation_sf_repos/Rotation/trunk


// Merge tags and your working copy. After merging you should commit changes (see next command)
svn merge --depth=infinity http://svn.bionicclick.com:8080/rotation_sf_repos/Rotation/tags/production_3_6___07_02_2012 .

//Comment after merge should begin with "MERGE:" 
svn commit -m "MERGE: your message" 