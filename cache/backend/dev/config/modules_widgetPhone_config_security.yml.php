<?php
// auto-generated by sfSecurityConfigHandler
// date: 2015/05/12 08:51:59
$this->security = array (
  'all' => 
  array (
    'is_secure' => true,
  ),
  'index' => 
  array (
    'is_secure' => true,
    'credentials' => 
    array (
      0 => 
      array (
        0 => 'superadmin',
        1 => 'widgetPhone_read',
      ),
    ),
  ),
  'new' => 
  array (
    'is_secure' => true,
    'credentials' => 
    array (
      0 => 
      array (
        0 => 'superadmin',
        1 => 'widgetPhone_create',
      ),
    ),
  ),
  'create' => 
  array (
    'is_secure' => true,
    'credentials' => 
    array (
      0 => 
      array (
        0 => 'superadmin',
        1 => 'widgetPhone_create',
      ),
    ),
  ),
  'edit' => 
  array (
    'is_secure' => true,
    'credentials' => 
    array (
      0 => 
      array (
        0 => 'superadmin',
        1 => 'widgetPhone_update',
      ),
    ),
  ),
  'update' => 
  array (
    'is_secure' => true,
    'credentials' => 
    array (
      0 => 
      array (
        0 => 'superadmin',
        1 => 'widgetPhone_update',
      ),
    ),
  ),
  'delete' => 
  array (
    'is_secure' => true,
    'credentials' => 
    array (
      0 => 
      array (
        0 => 'superadmin',
        1 => 'widgetPhone_delete',
      ),
    ),
  ),
  'list' => 
  array (
    'is_secure' => true,
    'credentials' => 
    array (
      0 => 
      array (
        0 => 'superadmin',
        1 => 'widgetPhone_read',
      ),
    ),
  ),
  'filter' => 
  array (
    'is_secure' => true,
    'credentials' => 
    array (
      0 => 
      array (
        0 => 'superadmin',
        1 => 'widgetPhone_filter',
      ),
    ),
  ),
  'visualpage' => 
  array (
    'is_secure' => true,
    'credentials' => 
    array (
      0 => 
      array (
        0 => 'superadmin',
        1 => 'widgetPhone_read',
      ),
    ),
  ),
  'visualpagecreate' => 
  array (
    'is_secure' => true,
    'credentials' => 
    array (
      0 => 
      array (
        0 => 'superadmin',
        1 => 'widgetPhone_create',
      ),
    ),
  ),
  'visualpageupdate' => 
  array (
    'is_secure' => true,
    'credentials' => 
    array (
      0 => 
      array (
        0 => 'superadmin',
        1 => 'widgetPhone_update',
      ),
    ),
  ),
);
