<?php
// auto-generated by sfSecurityConfigHandler
// date: 2015/01/12 19:10:15
$this->security = array (
  'all' => 
  array (
    'is_secure' => true,
  ),
  'index' => 
  array (
    'is_secure' => true,
    'credentials' => 
    array (
      0 => 
      array (
        0 => 'superadmin',
        1 => 'page_read',
      ),
    ),
  ),
  'new' => 
  array (
    'is_secure' => true,
    'credentials' => 
    array (
      0 => 
      array (
        0 => 'superadmin',
        1 => 'page_create',
      ),
    ),
  ),
  'create' => 
  array (
    'is_secure' => true,
    'credentials' => 
    array (
      0 => 
      array (
        0 => 'superadmin',
        1 => 'page_create',
      ),
    ),
  ),
  'edit' => 
  array (
    'is_secure' => true,
    'credentials' => 
    array (
      0 => 
      array (
        0 => 'superadmin',
        1 => 'page_update',
      ),
    ),
  ),
  'update' => 
  array (
    'is_secure' => true,
    'credentials' => 
    array (
      0 => 
      array (
        0 => 'superadmin',
        1 => 'page_update',
      ),
    ),
  ),
  'delete' => 
  array (
    'is_secure' => true,
    'credentials' => 
    array (
      0 => 
      array (
        0 => 'superadmin',
        1 => 'page_delete',
      ),
    ),
  ),
  'instructions' => 
  array (
    'is_secure' => true,
    'credentials' => 
    array (
      0 => 
      array (
        0 => 'superadmin',
        1 => 'page_instructions',
      ),
    ),
  ),
);
