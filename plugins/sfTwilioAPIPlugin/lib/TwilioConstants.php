<?php

class TwilioConstants {

    /** Base Twilio API URI */
    const API_URI_BASE = "https://api.twilio.com";
    /** Default Twilio API version to use. */
    const DEFAULT_API_VERSION = '2008-08-01';
    /** Command line for manipulate with phone numbers */
    const TWILIO_REQUEST_PATTERN = "/%s/Accounts/%s/%s";
    /** Query string suffix for manipulate with incoming phone numbers */
    const INCOMING_NUMBER_QUERY_STRING_SUFFIX = "IncomingPhoneNumbers";
    /** Number type for local phone numbers */
    const LOCAL_NUMBER_TYPE = "local";
    /** Number type for local phone numbers */
    const TOLLFREE_NUMBER_TYPE = "toll free";
    /** Query string suffix for local phone numbers */
    const LOCAL_NUMBER_QUERY_STRING_SUFFIX = "/Local";
    /** Query string suffix for toll free phone numbers */
    const TOLLFREE_NUMBER_QUERY_STRING_SUFFIX = "/TollFree";
    /** Query string suffix for manipulate with outgoing numbers */
    const OUTGOING_CALLER_QUERY_STRING_SUFFIX = "OutgoingCallerIds";
    /** Query string suffix for manipulate with calls */
    const CALL_QUERY_STRING_SUFFIX = "Calls";
    /** Query string suffix for manipulate with recordings */
    const RECORD_QUERY_STRING_SUFFIX = "Recordings";

    const DEFAULT_CALL_REGISTER_URL = "http://demo.twilio.com/welcome";

    const SID_LENGTH        = 34;
    const PHONE_MIN_LENGTH  = 10;
    const PHONE_MAX_LENGTH  = 22;
    const MAXIMAL_TIMEOUT   = 999;

    /** Valid option names for making calls */
    const CALL_METHOD_OPTION        = 'Method';
    const CALL_SEND_DIGITS_OPTION   = 'SendDigits';
    const CALL_IF_MACHINE_OPTION    = 'IfMachine';
    const CALL_TIMEOUT_OPTION       = 'Timeout';

    const DELAY_IN_SECONDS      = 1;
    const REQUEST_ATTEMPT_COUNT = 20;        
}