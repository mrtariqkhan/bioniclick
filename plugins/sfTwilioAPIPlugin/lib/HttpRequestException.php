<?php
    /* HttpRequestAPI throws HttpRequestException on error.
     * Useful to catch this exception separately from general PHP
     * exceptions, if you want.
     */
    class HttpRequestException extends Exception {}
?>