<?php
require "TwilioConstants.php";
require "TwilioPhoneNumberAPIException.php";
require "HttpRequestAPI.php";

class TwilioPhoneNumberAPI {

//    /**
//     * Retrieves list of local Twilio incoming numbers.
//     *
//     * @param $accountSid Twilio Account SID
//     * @param $authToken Twilio Account AuthToken
//     * @param $apiVersion Twilio API version to use
//     * 
//     * @return the array of key-value arrays with local Twilio incoming numbers
//     */
//    public static function getLocalIncomingPhoneNumbers(
//        $accountSid,
//        $authToken,
//        $apiVersion = TwilioConstants::DEFAULT_API_VERSION
//    ) {
//
//        return self::getIncomingPhoneNumbersBySuffix(
//            $accountSid,
//            $authToken,
//            $apiVersion,
//            TwilioConstants::INCOMING_NUMBER_QUERY_STRING_SUFFIX
//                . TwilioConstants::LOCAL_NUMBER_QUERY_STRING_SUFFIX
//        );
//    }
//
//    /**
//     * Retrieves list of toll free Twilio incoming numbers.
//     * 
//     * @param $accountSid Twilio Account SID
//     * @param $authToken Twilio Account AuthToken
//     * @param $apiVersion Twilio API version to use
//     *
//     * @return the array of key-value arrays with toll free Twilio incoming numbers
//     */
//    public static function getTollFreeIncomingPhoneNumbers(
//        $accountSid,
//        $authToken,
//        $apiVersion = TwilioConstants::DEFAULT_API_VERSION
//    ) {
//
//        return self::getIncomingPhoneNumbersBySuffix(
//            $accountSid,
//            $authToken,
//            $apiVersion,
//            TwilioConstants::INCOMING_NUMBER_QUERY_STRING_SUFFIX
//                . TwilioConstants::TOLLFREE_NUMBER_QUERY_STRING_SUFFIX
//        );
//    }

    /**
     * Retrieves list of all Twilio incoming numbers - local and toll free.
     *
     * @param $accountSid Twilio Account SID
     * @param $authToken Twilio Account AuthToken
     * @param $apiVersion Twilio API version to use
     *
     * @return the array of key-value arrays with incoming numbers
     */
    public static function getAllIncomingPhoneNumbers(
        $accountSid,
        $authToken,
        $apiVersion = TwilioConstants::DEFAULT_API_VERSION
    ) {

        return self::getIncomingPhoneNumbersBySuffix(
            $accountSid,
            $authToken,
            $apiVersion,
            TwilioConstants::INCOMING_NUMBER_QUERY_STRING_SUFFIX
        );

        return $numbers;
    }

    /**
     * Retrieves list of Twilio incoming numbers by concrete type.
     * 
     * @param $accountSid Twilio Account SID
     * @param $authToken Twilio Account AuthToken
     * @param $apiVersion Twilio API version to use
     * @param $typeSuffix suffix for URL for request numbers of corresponding type
     * 
     * @return the array of key-value arrays with incoming numbers
     */
    public static function getIncomingPhoneNumbersBySuffix(
        $accountSid,
        $authToken,
        $apiVersion,
        $typeSuffix
    ) {

        $incomingNumbers = array();
        if (!empty($typeSuffix)) {
            $httpRequest = new HttpRequest($accountSid, $authToken);

            $path = sprintf(
                TwilioConstants::TWILIO_REQUEST_PATTERN,
                $apiVersion,
                $accountSid,
                $typeSuffix
            );
            $path = TwilioConstants::API_URI_BASE . $path;
            $response = $httpRequest->doGet($path);

            if ($response->IsError) {
                throw new TwilioPhoneNumberAPIException($response->ErrorMessage);
            } else {
                $vars = array();
                $pagesTotalCount = (integer)$response->ResponseXml->IncomingPhoneNumbers['numpages'];
                for ($page = 0; $page < $pagesTotalCount; $page++) {
                    $vars["page"] = $page;
                    $response = $httpRequest->doGet($path, $vars);

                    if ($response->IsError) {
                        throw new TwilioPhoneNumberAPIException($response->ErrorMessage);
                    } else {
                        $numbers =
                            $response->
                            ResponseXml->
                            IncomingPhoneNumbers->
                            IncomingPhoneNumber
                        ;
                        foreach ($numbers as $number) {
                            $incomingNumbers[] = (array)$number;
                        }
                    }
                }
            }
        }

        return $incomingNumbers;
    }

    /**
     * Makes request to Twilio to create or update incoming phone number.
     * 
     * @param $accountSid Twilio Account SID
     * @param $authToken Twilio Account AuthToken
     * @param $apiVersion Twilio API version to use
     * @param $sid Twilio phone number Sid for update or null for create
     * @param $numberType incoming number type for register, currently "local" or "toll free" (lowercased)
     * @param $url URL of web application to handle Twilio requests creating when call will income
     * @param $method HTTP method ("GET" or "POST") for sending data from Twilio to web application
     * @param $areaCode US local area phone code (3 digits)
     * @param $friendlyName human readable description of new incoming phone number
     * @return array of two elements: phone number Sid and phone number itself returned by Twilio 
     *         (keys 'phoneSid', 'phoneNumber') or null if unsuccessfull
     */
    public static function sendIncomingPhoneNumber(
        $accountSid,
        $authToken,
        $sid,
        $url,
        $friendlyName = null,
        $numberType = TwilioConstants::TOLLFREE_NUMBER_TYPE,
        $areaCode = null,
        $method = HttpRequest::POST_METHOD,
        $apiVersion = TwilioConstants::DEFAULT_API_VERSION
    ) {

        $errorMessage = '';
        if (!empty($sid)) {
            $errorMessage = self::isSidValid($sid);
        }
        if (!empty($errorMessage)) {
            throw new TwilioPhoneNumberAPIException($errorMessage);
        }
        
        $url = trim($url);
        if (empty($url)) {
            throw new TwilioPhoneNumberAPIException('url must be not empty');
        }
        if (!in_array($method, array(HttpRequest::POST_METHOD, HttpRequest::GET_METHOD))) {
            $methodPost = HttpRequest::POST_METHOD;
            $methodGet  = HttpRequest::GET_METHOD;
            throw new TwilioPhoneNumberAPIException(
                "Method value must be one of {$methodPost} or {$methodGet}"
            );
        }

        $path = self::createPathString(
            $apiVersion, $accountSid, $numberType, $sid
        );
        $result = self::doSendNumberInfo(
            $accountSid, $authToken, $path, $url, $method, $areaCode, $friendlyName
        );

        return $result;
    }

    /**
     * Makes request to Twilio to create or update incoming phone number.
     *
     * @param $accountSid Twilio Account SID
     * @param $authToken Twilio Account AuthToken
     * @param $sid Twilio phone number Sid for update or null for create
     * @param $numberType incoming number type for register, currently "local" or "toll free" (lowercased)
     * @param $apiVersion Twilio API version to use
     * 
     * @return true if deleted successfully and false otherwise
     */
    public static function deleteIncomingPhoneNumber(
        $accountSid,
        $authToken,
        $sid,
        $apiVersion = TwilioConstants::DEFAULT_API_VERSION
    ) {

        $path = self::createPathStringDelete($apiVersion, $accountSid, $sid);

        $isSuccessfull = self::doDeleteNumber(
            $accountSid,
            $authToken,
            $path
        );

        return $isSuccessfull;
    }

    protected static function doSendNumberInfo(
        $accountSid,
        $authToken,
        $path,
        $url,
        $method,
        $areaCode,
        $friendlyName
    ) {

        // Will hold POST parameters we'll send to Twilio
        $data = array();
        $data['Url'] = $url;
        if (!empty($method)) {
            $data['Method'] = $method;
        }
        if (!empty($areaCode)) {
            $data['AreaCode'] = $areaCode;
        }
        if (!empty($friendlyName)) {
            $data['FriendlyName'] = $friendlyName;
        }

        $httpRequest = new HttpRequest($accountSid, $authToken);
        $response = $httpRequest->doPost($path, $data);

        $resultArray = array();
        if ($response->IsError) {
            $xml = new SimpleXMLElement($response->ResponseText);
            $errorCode = $xml->children()->children()->Code;
            $errorMessage = $xml->children()->children()->Message;
            if (!empty($errorCode)) {
                $resultArray['error'] = (string)$errorCode;
                $resultArray['message'] = (string)$errorMessage;
            } else {
                $resultArray['error'] = $response->ErrorMessage;
            }
        } else {
            $resultArray['sid'] = 
                (string)$response->ResponseXml->IncomingPhoneNumber->Sid
            ;
            $resultArray['phone_number'] = 
                (string)$response->ResponseXml->IncomingPhoneNumber->PhoneNumber
            ;
            $resultArray['friendly_name'] = 
                (string)$response->ResponseXml->IncomingPhoneNumber->FriendlyName
            ;
        }

        return $resultArray;
    }

    protected static function doDeleteNumber(
        $accountSid,
        $authToken,
        $path
    ) {

        $httpRequest = new HttpRequest($accountSid, $authToken);
        $response = $httpRequest->doDelete($path);

        $isSuccessfull = true;
        if ($response->IsError) {
            $isSuccessfull = false;
        }

        return $isSuccessfull;
    }

    protected static function isSidValid($sid) {

        $errorMessage = null;
        if (empty($sid)) {
            $errorMessage = 'Sid must be not empty';
        } else if (strlen($sid) != TwilioConstants::SID_LENGTH) {
            $length = TwilioConstants::SID_LENGTH;
            $errorMessage = "Sid must be of {$length} length";
        }

        return $errorMessage;
    }

    public static function createPathString(
        $apiVersion,
        $accountSid,
        $numberType,
        $sid = null
    ) {

        $queryString = TwilioConstants::INCOMING_NUMBER_QUERY_STRING_SUFFIX;
        if (empty($sid)) {
            if ($numberType == TwilioConstants::LOCAL_NUMBER_TYPE) {
                $queryString = $queryString . TwilioConstants::LOCAL_NUMBER_QUERY_STRING_SUFFIX;
            } else {
                $queryString = $queryString . TwilioConstants::TOLLFREE_NUMBER_QUERY_STRING_SUFFIX;
            }
        }
        $path = TwilioConstants::API_URI_BASE . sprintf(
            TwilioConstants::TWILIO_REQUEST_PATTERN,
            $apiVersion,
            $accountSid,
            $queryString
        );
        if (!empty($sid)) {
            $path .= "/$sid";
        }

        return $path;
    }

    protected static function createPathStringDelete(
        $apiVersion,
        $accountSid,
        $sid
    ) {

        $queryString = TwilioConstants::INCOMING_NUMBER_QUERY_STRING_SUFFIX;

        $errorMessage = self::isSidValid($sid);
        if (!empty($errorMessage)) {
            throw new TwilioPhoneNumberAPIException($errorMessage);
        }

        $path = TwilioConstants::API_URI_BASE . sprintf(
            TwilioConstants::TWILIO_REQUEST_PATTERN,
            $apiVersion,
            $accountSid,
            $queryString
        );

        $path .= "/{$sid}";

        return $path;
    }
}