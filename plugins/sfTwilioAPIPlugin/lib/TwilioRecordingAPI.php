<?php

require_once "TwilioConstants.php";
require_once "TwilioRecordingAPIException.php";
require_once "HttpRequestAPI.php";

/**
 * Provides methods to connect Twilio API for registering recordings of calls. 
 * 
 * @author Tarasenko Anna
 */
class TwilioRecordingAPI {

    /** Twilio API version to use. */
    protected $_apiVersion;
    /** Holds the Twilio Account SID. */
    protected $_accountSid;
    /** Holds the Twilio Account AuthToken. */
    protected $_authToken;
    /** Holds the HttpRequest object. */
    protected $_httpRequest;

    /**
     * Creates an instance using parameters of Twilio account and URLs of the parent application that 
     * uses the TwilioCallAPI.
     * 
     * @param $accountSid Twilio Account Sid given after registering in Twilio
     * @param $authToken Twilio "password" (Auth Token) given after registering in Twilio
     * @param $incomingCallCreateUrl URL of parent web application for registering incoming calls
     * @param $outgoingCallCreateUrl URL of parent web application for registering outgoing calls
     * @param $apiVersion string representation of Twilio API version adding to the API URL, 
     * has the default value and may be omitted.
     * @return nothing
     */
    public function __construct($accountSid, $authToken, $apiVersion = TwilioConstants::DEFAULT_API_VERSION) {

        $this->setAccountSid($accountSid);
        $this->setAuthToken($authToken);
        if ($apiVersion != null) {
            $this->setApiVersion($apiVersion);
        }

        $this->_httpRequest = new HttpRequest($accountSid, $authToken);
    }

    /**
     * Retrieves details of recordings for a call from Twilio by given CallSid.
     * 
     * @param $callSid unique Twilio identifier of a call (CallSid)
     * @return the key-value array of recording parameters
     */
    public function getRecordingsInfo($callSid) {

        $path = sprintf(TwilioConstants::TWILIO_REQUEST_PATTERN, $this->getApiVersion(), $this->getAccountSid(), TwilioConstants::CALL_QUERY_STRING_SUFFIX);
        $path = TwilioConstants::API_URI_BASE . $path;

        if (isset($callSid)) {
            if (strlen($callSid) != TwilioConstants::SID_LENGTH) {
                $length = TwilioConstants::SID_LENGTH;
                throw new TwilioRecordingAPIException("Call Sid must be of {$length} length");
            } else {
                $path .= "/$callSid/" . TwilioConstants::RECORD_QUERY_STRING_SUFFIX;
            }
        } else {
            throw new TwilioRecordingAPIException('Call Sid must be not null');
        }
        Loggable::putLogMessage(ApplicationLogger::TWILIO_LOG_TYPE, "Get recording info from: {$path}", sfLogger::INFO);
        $response = $this->_httpRequest->doGet($path);

        if ($response->IsError) {
            throw new TwilioCallAPIException($response->ErrorMessage);
        } else {
            $recordings = array();
            foreach ($response->ResponseXml->Recordings->Recording as $recording) {
                $recordingWithURL = (array)$recording;
                $recordingURL = sprintf(TwilioConstants::TWILIO_REQUEST_PATTERN, TwilioConstants::DEFAULT_API_VERSION, $this->getAccountSid(), TwilioConstants::RECORD_QUERY_STRING_SUFFIX);
                $recordingURL = TwilioConstants::API_URI_BASE . $recordingURL . '/' . $recordingWithURL['Sid'];
                $recordingWithURL['RecordingUrl'] = $recordingURL;
                $recordings[] = $recordingWithURL;
            }
        }

        return $recordings;
    }

    /**
     * Retrieves details of a single recording from Twilio by given RecordingSid.
     * 
     * @param $recordingSid unique Twilio identifier of a recording (RecordingSid)
     * @return the key-value array of call parameters
     */
    public function getRecordingInfo($recordingSid) {

        $path = sprintf(TwilioConstants::TWILIO_REQUEST_PATTERN, $this->getApiVersion(), $this->getAccountSid(), TwilioConstants::RECORD_QUERY_STRING_SUFFIX);
        $path = TwilioConstants::API_URI_BASE . $path;

        $recordingInfo = null;
        if (isset($recordingSid)) {
            if (strlen($recordingSid) != TwilioConstants::SID_LENGTH) {
                $length = TwilioConstants::SID_LENGTH;
                throw new TwilioRecordingAPIException("Recording Sid must be of {$length} length");
            } else {
                $path .= "/$recordingSid.xml";
            }

            $response = $this->_httpRequest->doGet($path);

            if ($response->IsError) {
                throw new TwilioRecordingAPIException($response->ErrorMessage);
            } else {
                $recordingInfo = (array)$response->ResponseXml->Recording;
            }
        } else {
            throw new TwilioRecordingAPIException('Recording Sid must be not null');
        }

        return $recordingInfo;
    }

//    /**
//     * Sends the given data of incoming or outgoing call to the application URL has been set when 
//     * constructing the object.
//     * 
//     * @param $callParameters the key-value array with Twilio HTTP request parameters
//     * @return the TwiML response for sending to Twilio, saying error message 
//     * if it has occured or an empty well-formed response
//     */
/*
//    public function handleRecording($recordingParameters) {
//
//        if (!isset($recordingParameters) || !is_array($recordingParameters) || empty($recordingParameters)) {
//            throw new TwilioRecordingAPIException('Malformed recording parameters array!');
//        }
//
//        $recordingUrl = $recordingParameters['RecordingUrl'];
//        //var_dump($recordingUrl);
//        $slashPos = strrpos($recordingUrl, '/');
//        //var_dump($slashPos);
//        $recordingSid = substr($recordingUrl, $slashPos + 1, TwilioConstants::SID_LENGTH);
//        //var_dump($recordingSid);
//        $duration = $recordingParameters['Duration'];
//        $digits = $recordingParameters['Digits'];
//
//        $recordingInfoArray = $this->getRecordingInfo($recordingSid);
//        if (!isset($recordingInfoArray) || !is_array($recordingInfoArray) || (count($recordingInfoArray) != 1) || !is_array($recordingInfoArray[0])) {
//            throw new TwilioRecordingAPIException('Getting call info for $recordingSid from VoIP server has failed!');
//        }
//
//        $recordingInfo = $recordingInfoArray[0];
//
//        if (!isset($recordingInfo['Sid']) || ($recordingInfo['Sid'] != $recordingSid)) {
//            throw new TwilioRecordingAPIException("VoIP server Recording Sid doesn't match with the request RecordingSid!");
//        }
//        if (!isset($recordingInfo['AccountSid']) || ($recordingInfo['AccountSid'] != $this->getAccountSid())) {
//            throw new TwilioRecordingAPIException("VoIP server Recordings AccountSid response doesn't match with the request AccountSid!");
//        }
//
//        $recordingInfoToSave = array();
//        $recordingInfoToSave['recording_sid'] = $recordingSid;
//        $recordingInfoToSave['account_sid'] = $this->getAccountSid();
//        $recordingInfoToSave['call_sid'] = $recordingInfo['CallSid'];
//        $recordingInfoToSave['file_url'] = $recordingUrl;
//        $recordingInfoToSave['date_created'] = $recordingInfo['DateCreated'];
//        $recordingInfoToSave['date_updated'] = $recordingInfo['DateUpdated'];
//        $recordingInfoToSave['duration'] = $duration;
//
//        $urlToSaveRecording = $this->getRecordingCreateUrl();
//        if (isset($urlToSaveRecording)) {
//            $response = $this->_httpRequest->doPost($urlToSaveRecording, $recordingInfoToSave);
//            //var_dump($response);
//            $returnValue = false;
//            if ($response->IsError) {
//                $returnValue = "<?xml version=\"1.0\"?><Response><Say>{$response->ErrorMessage}</Say></Response>";
//            } elseif (isset($response->ResponseXml) && isset($response->ResponseXml->TwiMLBody)) {
//                $twiML = $response->ResponseXml->TwiMLBody->children()->asXML();
//                //var_dump($twiML);
//                //$twiMLStr = $twiML;
//                $returnValue = "<?xml version=\"1.0\"?><Response>$twiML</Response>";
//            }
//        }
//
//        return $returnValue;
//    }
*/
    public function getApiVersion() {
        return $this->_apiVersion;
    }

    public function setApiVersion($value) {
        $this->_apiVersion = $value;
        return $this;
    }

    public function getAccountSid() {
        return $this->_accountSid;
    }

    public function setAccountSid($value) {
        $this->_accountSid = $value;
        return $this;
    }

    public function getAuthToken() {
        return $this->_authToken;
    }

    public function setAuthToken($value) {
        $this->_authToken = $value;
        return $this;
    }
}
