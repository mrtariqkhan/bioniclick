<?php

require "TwilioConstants.php";
require "TwilioCallAPIException.php";
require "HttpRequestAPI.php";

/**
 * Provides methods to connect Twilio API for making outgoing calls, registering incoming and outgoing
 * calls by web application using the TwilioCallAPI and retrieving call details.
 * 
 * @author Tarasenko Anna
 */
class TwilioCallAPI {

    /**
     * Info from: http://www.twilio.com/docs/api/2008-08-01/rest/call
     * An integer representing the status of the call.
     **/
    static public $STATUSES = array(
        0 => 'not yet dialed',
        1 => 'in-progress',
        2 => 'completed',
        3 => 'busy',
        4 => 'failed',
        5 => 'no-answer',
    );

    /**
     * Retrieves details of a single call from Twilio by given CallSid.
     * 
     * @param $callSid unique Twilio identifier of a call (CallSid)
     * @return the key-value array of call parameters
     */
    public static function getCallInfo(
        $accountSid,
        $authToken,
        $callSid,
        $callSegmentSid = null,
        $apiVersion = TwilioConstants::DEFAULT_API_VERSION
    ) {
        $path = sprintf(TwilioConstants::TWILIO_REQUEST_PATTERN, $apiVersion, $accountSid, TwilioConstants::CALL_QUERY_STRING_SUFFIX);
        $path = TwilioConstants::API_URI_BASE . $path;

        if (isset($callSid)) {
            if (strlen($callSid) != TwilioConstants::SID_LENGTH) {
                $length = TwilioConstants::SID_LENGTH;
                throw new TwilioCallAPIException("Call Sid must be of {$length} length");
            } else {
                $path .= "/$callSid/Segments";
                $isSingle = true;
                if (isset($callSegmentSid)) {
                    if (strlen($callSegmentSid) != TwilioConstants::SID_LENGTH) {
                        $length = TwilioConstants::SID_LENGTH;
                        throw new TwilioCallAPIException("Call segment Sid must be of {$length} length");
                    } else {
                        $path .= "/$callSegmentSid";
                    }
                }
            }
        } else {
            throw new TwilioCallAPIException('Call Sid must be not empty');
        }

        $complete = false;
        $attemptNumber = 0;
        $httpRequest = new HttpRequest($accountSid, $authToken);
        while ((!$complete) && ($attemptNumber++ < TwilioConstants::REQUEST_ATTEMPT_COUNT)) {
            try {
                $response = $httpRequest->doGet($path);

                if ($response->IsError) {
                    throw new TwilioCallAPIException($response->ErrorMessage);
                } else {
                    $calls = array();
                    foreach ($response->ResponseXml->Calls->Call as $call) {
                        $calls[] = (array)$call;
                    }
                    if (count($calls) > 0) {
                        Loggable::putLogMessage(ApplicationLogger::TWILIO_LOG_TYPE, "Got call info from url = " . $path, sfLogger::DEBUG);
                        Loggable::putArrayLogMessage(ApplicationLogger::TWILIO_LOG_TYPE, sfLogger::DEBUG, $calls, "\n Additional call info:");                            
                        $complete = true;
                    }
                }
            } catch (Exception $e) {
                Loggable::putExceptionFullLogMessage($e, ApplicationLogger::TWILIO_LOG_TYPE);
            }
            if (!$complete) {
                sleep(TwilioConstants::DELAY_IN_SECONDS);
            }
        }

        return $calls;
    }        

//    /**
//     * Makes an outgoing call from Twilio to given phone number.
//     * 
//     * @param $outgoingPhoneNumber US phone number to call from, must be one of validated by Twilio outgoing caller numbers
//     * @param $calledPhoneNumber US phone number to call to
//     * @param $url URL of application for handling callback from Twilio
//     * @param $options call options from the list 'Method', 'SendDigits', 'IfMachine', 'Timeout'
//     * @return Twilio call Sid or null if unseccessfull
//     */
//    public static function makeOutgoingCall(
//        $accountSid,
//        $authToken,
//        $outgoingPhoneNumber,
//        $calledPhoneNumber,
//        array $options = array(),
//        $url = TwilioConstants::DEFAULT_CALL_REGISTER_URL,
//        $apiVersion = TwilioConstants::DEFAULT_API_VERSION
//    ) {
//        if (!isset($outgoingPhoneNumber)) {
//            throw new TwilioCallAPIException('outgoingPhoneNumber must be not empty');
//        } elseif (strlen($outgoingPhoneNumber) < TwilioConstants::PHONE_MIN_LENGTH || strlen($outgoingPhoneNumber) > TwilioConstants::PHONE_MAX_LENGTH) {
//            $minLength = TwilioConstants::PHONE_MIN_LENGTH;
//            $maxLength = TwilioConstants::PHONE_MAX_LENGTH;
//            throw new TwilioCallAPIException("outgoingPhoneNumber length must be between {$minLength} and {$maxLength}");
//        }
//
//        if (!isset($calledPhoneNumber)) {
//            throw new TwilioCallAPIException('calledPhoneNumber must be not empty');
//        } elseif (strlen($calledPhoneNumber) < TwilioConstants::PHONE_MIN_LENGTH /*|| strlen($calledPhoneNumber) > TwilioConstants::PHONE_MAX_LENGTH*/) {
//            $minLength = TwilioConstants::PHONE_MIN_LENGTH;
//            throw new TwilioCallAPIException("calledPhoneNumber must be of minimum {$minLength} length");
//        }
//
//        $path = sprintf(TwilioConstants::TWILIO_REQUEST_PATTERN, $apiVersion, $accountSid, TwilioConstants::CALL_QUERY_STRING_SUFFIX);
//
//        $path = TwilioConstants::API_URI_BASE . $path;
//
//        // Will hold what we send to Twilio
//        $data = array();
//        $data['Caller'] = $outgoingPhoneNumber;
//        $data['Called'] = $calledPhoneNumber;
//        $data['Url']    = $url;
//
//        // Check $options
//        if (is_array($options) && !empty($options)) {
//            $optionsArray = array(
//                TwilioConstants::CALL_METHOD_OPTION, 
//                TwilioConstants::CALL_SEND_DIGITS_OPTION, 
//                TwilioConstants::CALL_IF_MACHINE_OPTION, 
//                TwilioConstants::CALL_TIMEOUT_OPTION
//            );
//            foreach ($optionsArray as $option) {
//                if (isset($options[$option]) && !empty($options[$option])) {
//                    $value = $options[$option];
//                    switch ($option) {
//                        case TwilioConstants::CALL_METHOD_OPTION:
//                            $value = strtoupper($value);
//                            if (!in_array($value, array(HttpRequest::POST_METHOD, HttpRequest::GET_METHOD))) {
//                                $methodPost = HttpRequest::POST_METHOD;
//                                $methodGet  = HttpRequest::GET_METHOD;
//                                throw new TwilioCallAPIException("Method value must be one of {$methodPost} or {$methodGet}");
//                            }
//                            break;
//                        case TwilioConstants::CALL_SEND_DIGITS_OPTION:
//                            if (!preg_match("/^(\d|#|\*)*$/i", $value)) {
//                                throw new TwilioCallAPIException("SendDigits can only contain digits, '*', and '#'");
//                            }
//                            $value = urlencode($value);
//                            break;
//                        case TwilioConstants::CALL_IF_MACHINE_OPTION:
//                            $value = ucfirst($value);
//                            if (!in_array($value, array('Continue', 'Hangup'))) {
//                                throw new TwilioCallAPIException("IfMachine value must be one of 'Continue' or 'Hangup'");
//                            }
//                            break;
//                        case TwilioConstants::CALL_TIMEOUT_OPTION:
//                            $value = (int) $value;
//                            if ($value > TwilioConstants::MAXIMAL_TIMEOUT) {
//                                $maxTimeout  = TwilioConstants::MAXIMAL_TIMEOUT;
//                                throw (new TwilioCallAPIException("Timeout value cannot be greater than {$maxTimeout} seconds"));
//                            }
//                            break;
//                    }
//                    $data[$option] = $value;
//                }
//            }
//        }
//
////        Loggable::putLogMessage(ApplicationLogger::TWILIO_LOG_TYPE, "Outgoing call: {$path}", sfLogger::INFO);
//
//        $httpRequest = new HttpRequest($accountSid, $authToken);
//        $response = $httpRequest->doPost($path, $data);
//
//        $callSid = null;
//        if ($response->IsError) {
//            throw new TwilioCallAPIException($response->ErrorMessage);
//        } else {
//            $callSid = $response->ResponseXml->Call->Sid;
//        }
//
//        return $callSid;
//    }

}