<?php
    /* TwilioPhoneNumberAPI class throws TwilioPhoneNumberAPIException on error.
     * Useful to catch this exception separately from general PHP
     * exceptions, if you want.
     */
    class TwilioPhoneNumberAPIException extends Exception {}
?>