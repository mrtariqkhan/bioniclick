<?php
    /* TwilioCallAPI classes throws TwilioCallAPIException on error.
     * Useful to catch this exception separately from general PHP
     * exceptions, if you want.
     */
    class TwilioCallAPIException extends Exception {}
?>