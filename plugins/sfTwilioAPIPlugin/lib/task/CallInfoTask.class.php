<?php

class CallInfoTask extends sfBaseTask {

    protected function configure() {

        $this->addOptions(array(
            new sfCommandOption('application', null, sfCommandOption::PARAMETER_REQUIRED, 'The application name', 'backend'),
            new sfCommandOption('env', null, sfCommandOption::PARAMETER_REQUIRED, 'The environment', 'dev'),
            new sfCommandOption('connection', null, sfCommandOption::PARAMETER_REQUIRED, 'The connection name', 'doctrine'),
        ));

        $this->namespace        = 'twilio';
        $this->name             = 'call_info';
        $this->briefDescription = '';
        $this->detailedDescription = <<<EOF
The [CallInfo|INFO] task does things.
Call it with:

  [php symfony twilio:call_info|INFO]
EOF;
    }

    protected function execute($arguments = array(), $options = array()) {

        $configuration = ProjectConfiguration::getApplicationConfiguration(
            $options['application'],
            $options['env'],
            isset($debug) ? $debug : true
        );
        sfContext::createInstance($configuration);

        $this->exitIfMust();

        // initialize the database connection
        $databaseManager = new sfDatabaseManager($this->configuration);
        $connection = $databaseManager->getDatabase(
            $options['connection']
            ? $options['connection']
            : null
        )->getConnection();

        $interval = sfConfig::get('app_call_info_interval', 5);
        $interval = intval($interval);

        $this->log("Start");

        $callsForUpdate = TwilioCallQueueTable::getInstance()->createQuery()->execute();
        $amountToUpdate = $callsForUpdate->count();
        $this->log("$amountToUpdate call(s) SHOULD be updated");

        $tz = new DateTimeZone('UTC');
        $dt = new DateTime('now', $tz);
        $nowUnixTimestamp = $dt->format('U');
        $nowUnixTimestamp = intval($nowUnixTimestamp);

        $updatedCount = 0;
        $i = 0;
        foreach ($callsForUpdate as $callForUpdate) {
            $i++;
            $call = null;
            try {
                $call = $callForUpdate->getTwilioIncomingCall();
                $addedAt = intval($callForUpdate->getAddedAt());

                if ($addedAt + $interval <= $nowUnixTimestamp) {
                    $this->log("  $i. An attempt to update call (Sid={$call->getSid()})");

                    $callSegments  = $this->findCallSegments($call);
                    $recordingList = $this->findCallRecordingList($call);

                    $call->updateBySegmentsAndRecordings($callSegments, $recordingList);

                    $updatedCount++;
                    $callForUpdate->delete();
                }
            } catch (Exception $e) {
                $sidInfo = empty($call) ? '' : "(Sid={$call->getSid()})";
                $this->log("! $i. FAIL while updating call $sidInfo: {$e->getMessage()}");
            }
        }

        $this->log("$updatedCount call(s) have been sucessfully updated");
        $this->log("Done");
    }

    /**
     * NOTE:: do not remove this method. It is for testing also.
     * @param PluginTwilioIncomingCall $call
     */
    public function findCallSegments($call) {
        return $call->findSegmentsFromTwilio();
    }

    /**
     * NOTE:: do not remove this method. It is for testing also.
     * @param PluginTwilioIncomingCall $call
     */
    public function findCallRecordingList($call) {
        return $call->findRecordingListFromTwilio();
    }

    protected function exitIfMust() {

        $taskMayBeExecuted = sfConfig::get('app_task_may_be_executed', false);
        $taskMayBeExecuted = ($taskMayBeExecuted == 'true') ? true : false;
        if (!$taskMayBeExecuted) {
            $this->log('');
            $this->log('! Task cannot be executed yet. Please, execute it later.');
            $this->log('');
            die;
        }
    }
}
