<?php
    /* TwilioRecordingAPI class throws TwilioRecordingAPIException on error.
     * Useful to catch this exception separately from general PHP
     * exceptions, if you want.
     */
    class TwilioRecordingAPIException extends Exception {}
?>