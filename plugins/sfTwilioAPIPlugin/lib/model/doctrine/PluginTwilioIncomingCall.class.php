<?php

/**
 * PluginTwilioIncomingCall
 * 
 * This class has been auto-generated by the Doctrine ORM Framework
 * 
 * @package    ##PACKAGE##
 * @subpackage ##SUBPACKAGE##
 * @author     ##NAME## <##EMAIL##>
 * @version    SVN: $Id: Builder.php 7490 2010-03-29 19:53:27Z jwage $
 */
abstract class PluginTwilioIncomingCall extends BaseTwilioIncomingCall {

    /**
     * usage:
     *   $callSegments  = $call->findSegmentsFromTwilio();
     *   $recordingList = $call->findRecordingListFromTwilio();
     *   $call->updateBySegmentsAndRecordings($callSegments, $recordingList);
     */
    public function updateBySegmentsAndRecordings($callSegments, $recordingList) {

        TwilioIncomingCallTable::updateIncomingCallBySegmentsAndRecordings($this, $callSegments, $recordingList);

        $this->save();
    }

    /**
     * NOTE:: do not remove this method. It is for testing also.
     */
    public function findSegmentsFromTwilio() {

        $twilioAccount = $this->getTwilioAccount();
        $accountSid = $twilioAccount->getSid();
        $authToken  = $twilioAccount->getAuthToken();

        $callSegments = TwilioCallAPI::getCallInfo(
            $accountSid,
            $authToken,
            $this->getSid()
        );

        return $callSegments;
    }

    /**
     * NOTE:: do not remove this method. It is for testing also.
     */
    public function findRecordingListFromTwilio() {

        $twilioAccount = $this->getTwilioAccount();
        $accountSid = $twilioAccount->getSid();
        $authToken  = $twilioAccount->getAuthToken();

        $recordingAPI = new TwilioRecordingAPI($accountSid, $authToken);
        $recordingList = null;
        $recordingList = $recordingAPI->getRecordingsInfo($this->getSid());

        return $recordingList;
    }
}