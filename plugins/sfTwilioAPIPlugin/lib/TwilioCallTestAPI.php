<?php

require_once 'TwilioCallAPI.php';

/**
 * Description of TwilioCallTestAPI
 *
 * @author Kirill chEbba Chebunin <iam at chebba.org>
 */
class TwilioCallTestAPI extends TwilioCallAPI {

    protected static $call = array();
    protected static $callSegment = array();

    public function getCallInfo($callSid, $callSegmentSid = null) {

        Loggable::putLogMessage(
            ApplicationLogger::TWILIO_LOG_TYPE,
            "Try to get call info: ($callSid, $callSegmentSid)",
            sfLogger::INFO
        );
        Loggable::putLogMessage(
            ApplicationLogger::TWILIO_LOG_TYPE,
            sprintf(
                "CurrentState: Call %s CallSegment %s",
                print_r(self::$call, true),
                print_r(self::$callSegment, true)
            ),
            sfLogger::INFO
        );

        $calls = array();
        if (self::$call['Sid'] == $callSid) {
            $calls[] = self::$call;
            if (self::$callSegment && self::$callSegment['Sid'] == $callSid) {
                $calls[] = self::$callSegment;
            }
        }

        Loggable::putLogMessage(
            ApplicationLogger::TWILIO_LOG_TYPE,
            sprintf('Return: %s', print_r($calls, true)),
            sfLogger::DEBUG
        );
        
        return $calls;
    }

    public static function setCall(array $data) {
        self::$call = $data;
    }

    public static function getCall() {
        return self::$call;
    }

    public static function getCallSegment() {
        return self::$callSegment;
    }

    public static function setCallSegment(array $data) {
        self::$callSegment = $data;
    }
}

