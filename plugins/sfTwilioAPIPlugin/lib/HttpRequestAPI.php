<?php

// ensure Curl is installed
if (!extension_loaded("curl")) {
    throw (new Exception("Curl extension is required for HttpRequest to work"));
}

require "HttpRequestException.php";

/* 
 * Provides all the REST response data 
 * Before using the reponse, check IsError to see if an exception 
 * occurred with the data sent to Twilio
 * ResponseXml will contain a SimpleXml object with the response xml
 * ResponseText contains the raw string response
 * Url and QueryString are from the request
 * HttpStatus is the response code of the request
 */
class HttpResponse {

    public $ResponseText;
    public $ResponseXml;
    public $HttpStatus;
    public $Url;
    public $QueryString;
    public $IsError;
    public $ErrorMessage;

    public function __construct($url, $text, $status, $isXml) {

        preg_match('/([^?]+)\??(.*)/', $url, $matches);
        $this->Url = $matches[1];
        $this->QueryString = $matches[2];
        $this->ResponseText = $text;
        $this->HttpStatus = $status;
        $this->IsError = false;

        if ($this->HttpStatus >= 400) {
            $this->IsError = true;
            $this->ErrorMessage = 'Server received the HttpCode ' . $this->HttpStatus;
        } elseif ($this->HttpStatus != 204 && $isXml) {

            //echo "before: $url, $text, $status, $isXml<br/>";
            $xml = @simplexml_load_string($text);
            if ($xml) {
                $this->ResponseXml = $xml;
                if (isset($this->ResponseXml->Error)) {
                    $this->ErrorMessage = (string)$this->ResponseXml->Error;
                    $this->IsError = true;
                }
            } else {
                $this->ResponseXml = null;
            }
        }
    }
}
    
/**
 * Provides creating and executing HTTP request and retrieving the response.
 */
class HttpRequest {

    const GET_METHOD    = 'GET';
    const POST_METHOD   = 'POST';
    const PUT_METHOD    = 'PUT';
    const DELETE_METHOD = 'DELETE';

    protected $_username;
    protected $_password;

    /*
     *   Creates an instance of the HttpRequest using given credentials.
     *    
     *   $username the login name of the user if needs
     *   $password the password of the user if needs
     */
    public function __construct($username = null, $password = null) {

        if (!empty($username)) {
            $this->_username = $username;
        }
        if (!empty($password)) {
            $this->_password = $password;
        }
    }

    public function doGet($path, $vars = array()) {
        return $this->request($path, $method = self::GET_METHOD, $vars);
    }

    public function doPost($path, $vars = array()) {
        return $this->request($path, $method = self::POST_METHOD, $vars);
    }

    public function doDelete($path) {
        return $this->request($path, $method = self::DELETE_METHOD);
    }

    /*
     * sendRequest
     *   Sends a HTTP Request to the given URL and returns the response
     *   $url : the URL
     *   $method : the HTTP method to use, defaults to GET
     *   $vars : for POST or PUT, a key/value associative array of data to
     * send, for GET will be appended to the URL as query params
     */
    public function request($url, $method = self::GET_METHOD, $vars = array()) {

        $encoded = '';
        foreach ($vars as $key => $value) {
            $encoded .= "$key=" . urlencode($value) . '&';
        }
        $encoded = substr($encoded, 0, -1);

        // if GET and vars, append them
        if ($method == self::GET_METHOD && !empty($vars)) {
            $url .= (FALSE === strpos($url, '?') ? '?' : '&') . $encoded;
        }

        // initialize a new curl object            
        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        curl_setopt($curl, CURLOPT_RETURNTRANSFER, TRUE);
        switch (strtoupper($method)) {
            case self::GET_METHOD:
                curl_setopt($curl, CURLOPT_HTTPGET, TRUE);
                break;
            case self::POST_METHOD:
                curl_setopt($curl, CURLOPT_POST, TRUE);
                curl_setopt($curl, CURLOPT_POSTFIELDS, $encoded);
                break;
            case self::DELETE_METHOD:
                curl_setopt($curl, CURLOPT_CUSTOMREQUEST, self::DELETE_METHOD);
                break;
            default:
                throw new HttpRequestException("Unknown method $method");
                break;
        }

        // send credentials
        $pwd = "{$this->_username}:{$this->_password}";
        curl_setopt($curl, CURLOPT_USERPWD, $pwd);

        // do the request. If FALSE, then an exception occurred
        if (FALSE === ($result = curl_exec($curl))) {
            throw new HttpRequestException('Curl failed with error ' . curl_error($curl));
        }

        // Get response parameters
        $responseCode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        $responseType = curl_getinfo($curl, CURLINFO_CONTENT_TYPE);
        $isXml = (strpos(strtolower($responseType), 'xml') != false);

        return new HttpResponse($url, $result, $responseCode, $isXml);
    }
}
