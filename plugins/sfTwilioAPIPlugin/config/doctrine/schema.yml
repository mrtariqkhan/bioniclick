actAs:
  Timestampable:
    created:
        name: created_at
        type: timestamp
        format: Y-m-d H:i:s
        options:
          notnull: false
    updated:
      name: updated_at
      type: timestamp
      format: Y-m-d H:i:s
      options:
        notnull: false

TwilioAccount:
  columns:
    id:
      type: integer
      primary: true
      autoincrement: true
    sid:
      type: string(34)
      notnull: true
      unique: true
    auth_token:
      type: string(34)
      notnull: true
    friendly_name:
      type: string(255)
      notnull: true
    status:
      type: integer(4)
      notnull: true

TwilioCity:
  columns:
    id:
      type: integer
      primary: true
      autoincrement: true
    name:
      type: string(255)      
    sub_zip_code:
      type: string(2)
      notnull: true      

TwilioLocalAreaCode:
  columns:
    id:
      type: integer
      primary: true
      autoincrement: true
    code:
      type: integer(3)
      notnull: true
      unique: true
    phone_number_type:
      type: enum
      length: 9
      values: ['local', 'toll free']
      notnull: true
    twilio_state_id:
      type: integer      
  relations:
    TwilioState:
      class: TwilioState
      local: twilio_state_id
      foreign: id
      foreignAlias: TwilioLocalAreaCodes

TwilioIncomingPhoneNumber:
  columns:
    id:
      type: integer
      primary: true
      autoincrement: true
    sid:
      type: string(34)
      notnull: true
      unique: true
    twilio_account_id:
      type: integer
      notnull: true
    url:
      type: string(255)
      notnull: true
    http_method_type:
      type: enum
      length: 4
      values: ['GET', 'POST']
      notnull: true
    phone_number:
      type: string(22)
      notnull: true
    friendly_name:
      type: string(255)
    twilio_local_area_code_id:
      type: integer
      notnull: true
    was_deleted:
      type: boolean
      notnull: true
      default: 0
  relations:
    TwilioAccount:
      class: TwilioAccount
      local: twilio_account_id
      foreign: id
      foreignAlias: TwilioIncomingPhoneNumbers
    TwilioLocalAreaCode:
      class: TwilioLocalAreaCode
      local: twilio_local_area_code_id
      foreign: id
      foreignAlias: TwilioIncomingPhoneNumbers

TwilioCallerPhoneNumber:
  columns:
    id:
      type: integer
      primary: true
      autoincrement: true
    phone_number:
      type: string(22)
      notnull: true
      unique: true
    is_blocked:
      type: boolean
      notnull: true
      default: 0
    reason:
      type: string(300)

TwilioIncomingCall:
  columns:
    id:
      type: integer
      primary: true
      autoincrement: true
    sid:
      type: string(34)
      notnull: true
      unique: true
    twilio_account_id:
      type: integer
      notnull: true
    twilio_caller_phone_number_id:
      type: integer
      notnull: true
    twilio_incoming_phone_number_id:
      type: integer
      notnull: true
    caller_country:
      type: string(255)
    caller_zip:
      type: string(6)
    caller_state:
      type: string(255)
    twilio_city_id:
      type: integer
    call_status:
      type: enum
      length: 15
      values: ['not yet dialed', 'in-progress', 'completed', 'busy', 'failed', 'no-answer']
      notnull: false
    start_time:
      type: integer(11)
    end_time:
      type: integer(11)
    duration:
      type: integer
      default: NULL
      notnull: false
    price:
      type: float
      default: NULL
      notnull: false
    sent_email_alert:
      type: tinyint(1)
  relations:
    TwilioAccount:
      class: TwilioAccount
      local: twilio_account_id
      foreign: id
      foreignAlias: TwilioIncomingCalls
    TwilioCallerPhoneNumber:
      class: TwilioCallerPhoneNumber
      local: twilio_caller_phone_number_id
      foreign: id
      foreignAlias: TwilioIncomingCalls
    TwilioIncomingPhoneNumber:
      class: TwilioIncomingPhoneNumber
      local: twilio_incoming_phone_number_id
      foreign: id
      foreignAlias: TwilioIncomingCalls
    TwilioCity:
      class: TwilioCity
      local: twilio_city_id
      foreign: id
      foreignAlias: TwilioIncomingCalls

TwilioRedialedCall:
  columns:
    id:
      type: integer
      primary: true
      autoincrement: true
    segment_sid:
      type: string(34)
      notnull: true
      unique: true
    twilio_incoming_call_id:
      type: integer
      notnull: true
    called_number:
      type: string(22)
      notnull: true
    call_status:
      type: enum
      length: 15
      values: ['not yet dialed', 'in-progress', 'completed', 'busy', 'failed', 'no-answer']
      notnull: false
      default: 'completed'
    start_time:
      type: integer(11)
    end_time:
      type: integer(11)
    duration:
      type: integer
      default: 0
    price:
      type: float
      default: 0
  relations:
    ParentCall:
      class: TwilioIncomingCall
      local: twilio_incoming_call_id
      foreign: id
      foreignAlias: TwilioRedialedCall
      type: one
      foreignType: one

TwilioCallRecording:
  columns:
    id:
      type: integer
      primary: true
      autoincrement: true
    sid:
      type: string(34)
      notnull: true
      unique: true
    twilio_incoming_call_id:
      type: integer
      notnull: true
    file_url:
      type: string(255)
      notnull: true
    date_created:
      type: integer(11)
    date_updated:
      type: integer(11)
    duration:
      type: integer
      default: 0
    dollar_amount:
      type: float
    name:
      type: string(50)
    description:
      type: text
    gender:
      type: string(6)
  relations:
    TwilioIncomingCall:
      class: TwilioIncomingCall
      local: twilio_incoming_call_id
      foreign: id
      foreignAlias: TwilioCallRecording
      type: one
      foreignType: one

TwilioCallQueue:
  columns:
    id:
      type: integer
      primary: true
      autoincrement: true
    twilio_incoming_call_id:
      type: integer
      notnull: true
    added_at:
      type: integer(11)
  relations:
    TwilioIncomingCall:
      class: TwilioIncomingCall
      local: twilio_incoming_call_id
      foreign: id
      foreignAlias: TwilioCallQueue

TwilioState:
  columns:
    code:
      type: varchar(2)      
    name:
      type: varchar(75)
      notnull: true
    twilio_country_id:
      type: integer
      notnull: true
  relations:
    TwilioCountry:
      class: TwilioCountry
      local: twilio_country_id
      foreign: id
      foreignAlias: TwilioStates
      

TwilioCountry:
  columns:
    id:
      type: integer
      primary: true
      autoincrement: true
    name:
      type: string(20)
      notnull: true    