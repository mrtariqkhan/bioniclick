#!/bin/sh
# Note, that '#' is comment

pathToRootDir='..';
pathToSymfony="$pathToRootDir/symfony"
fileNameSynchronyzingLog="$pathToRootDir/log/synchronyzing.log"

echo "Synchronyzing started. Look at $fileNameSynchronyzingLog later"
php $pathToSymfony bionic:inc_num_sync --direction=to --delete=true --belong=true >> $fileNameSynchronyzingLog
echo "Synchronyzing has been finished."
