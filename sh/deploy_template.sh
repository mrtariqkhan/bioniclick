# Note, that '#' is comment

# To use this sh you must define variables and call this sh:
#
# user_opt=''   #MySQL user
# pass_opt=''   #MySQL user's password
# db_name=''    #MySQL database's name
# . ./deploy_template.sh

version='5_2'
needDumpLoad=true
needMigrations=true
pathToRootDir='..';
pathToSymfony="$pathToRootDir/symfony"
fileNameDump="$pathToRootDir/data/sql/dumpsAndMigrations/dump_stable_$version.sql"     #dump file name with create statements
fileNameZippedDump="$fileNameDump.gz"                                                  #zipped dump file name with create statements
fileNameMigrations="$pathToRootDir/data/sql/dumpsAndMigrations/migration_$version.sql" #migrations file name
fileNameSynchronyzingLog="$pathToRootDir/log/synchronyzing.log"


echo ">>"
echo ">> Input parameters:"
echo ">>  user                  : '$user_opt'."
echo ">>  database              : '$db_name'."
echo ">>  STABLE dump version   : '$version'."
echo ">>  if do load dump       : '$needDumpLoad'."
echo ">>  if do migration       : '$needMigrations'."
echo ">>"

read -p ">> Do You want to execute script with such parameters (y/n)?"
if [ $REPLY != "y" ];
    then
        echo ">> Exiting..."
        echo ""
        exit 1
fi
echo ">>"


if [ $needDumpLoad == "true" ];
    then
        if [ ! -r $fileNameZippedDump ];
            then
                echo ">> File $fileNameZippedDump does not exist or is not readable."
                echo ">> Exiting..."
                echo ""
                exit 1
        fi

    echo ">> Attempt to unzip file $fileNameZippedDump to $fileNameDump"
    gunzip -c $fileNameZippedDump > $fileNameDump

    if [ ! -r $fileNameDump ];
        then
            echo ">> File $fileNameDump does not exist or is not readable."
            echo ">> Exiting..."
            echo ""
            exit 1
    fi
fi


if [ $needMigrations == "true" ];
    then
        if [ ! -f $fileNameSynchronyzingLog ];
        then
            echo ">> An attempt to create $fileNameSynchronyzingLog file."
            echo >> $fileNameSynchronyzingLog
        fi

        if [ ! -w $fileNameSynchronyzingLog ];
            then
                echo ">> File $fileNameSynchronyzingLog does not exist or is not writable."
                echo ">> Exiting..."
                echo ""
                exit 1
        fi


        if [ ! -r $fileNameMigrations ];
            then
                echo ">> File $fileNameMigrations does not exist or is not readable."
                echo ">> Exiting..."
                echo ""
                exit 1
        fi
fi


mysql -u$user_opt -p$pass_opt -e "DROP DATABASE IF EXISTS $db_name;"
mysql -u$user_opt -p$pass_opt -e "CREATE DATABASE $db_name;"
echo ">>"


php $pathToSymfony doctrine:insert-sql
echo ">>"


echo ">> Dump's apply started (from $fileNameDump)."
mysql -u$user_opt -p$pass_opt --database=$db_name < $fileNameDump
echo ">> Dump has been successfully applied."
echo ">>"


php $pathToSymfony c
echo ">>"


if [ $needMigrations == "true" ];
    then
        echo ">> Synchronyzing started. Look at $fileNameSynchronyzingLog later"
        php $pathToSymfony bionic:inc_num_sync --direction=to --delete=true --belong=true >> $fileNameSynchronyzingLog
        echo ">> Synchronyzing has been finished."
        echo ">>"


        echo ">> Migration's apply started (from $fileNameMigrations)."
        mysql -u$user_opt -p$pass_opt --database=$db_name < $fileNameMigrations
        echo ">> Migration script has been successfully applied."
        echo ">>"

        # . ./restoreNumberLog.sh
fi

echo ">> Done!"
echo ""
