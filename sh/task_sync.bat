:: Note, that '::' is comment

SET pathToRootDir=..
SET pathToSymfony=%pathToRootDir%/symfony
SET fileNameSynchronyzingLog=%pathToRootDir%/log/synchronyzing.log


echo "Synchronyzing started. Look at %fileNameSynchronyzingLog% later"
php %pathToSymfony% bionic:inc_num_sync --direction=to --delete=true --belong=true >> %fileNameSynchronyzingLog%
echo "Synchronyzing has been finished."

pause