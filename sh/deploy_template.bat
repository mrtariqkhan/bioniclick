:: Note, that '::' is comment

:: To use this bat you must define variables and call this bat:
::
:: @ SET user_opt=   ::MySQL user
:: @ SET pass_opt=   ::MySQL user's password
:: @ SET db_name=    ::MySQL database's name
:: @ CALL deploy_template.bat


@ SET version=5_2
@ SET pathToRootDir=..
@ SET pathToSymfony=%pathToRootDir%/symfony
@ SET fileNameDump=%pathToRootDir%/data/sql/dumpsAndMigrations/dump_stable_%version%.sql
@ SET fileNameMigrations=%pathToRootDir%/data/sql/dumpsAndMigrations/migration_%version%.sql
@ SET fileNameSynchronyzingLog=%pathToRootDir%/log/synchronyzing.log


@ echo
@ echo  Input parameters:
@ echo   user                : '%user_opt%'.
@ echo   database            : '%db_name%'.
@ echo   STABLE dump version : '%version%'.



:: wait for user's choice from Y/N (y/n).
:: ERRORLEVEL will keep the index number of user's result
:: we must analyse ERRORLEVEL in decreasing order.
@CHOICE /C:YN /N /M " Do You want to execute script with such parameters (y/n)?"
@ IF ERRORLEVEL == 2 GOTO CHOICE_PARAMETERS_NO
@ IF ERRORLEVEL == 1 GOTO CHOICE_PARAMETERS_YES
GOTO CHOICE_PARAMETERS_NO

:CHOICE_PARAMETERS_NO
@ echo Exiting...
EXIT

:CHOICE_PARAMETERS_YES



@ mysql -u%user_opt% -p%pass_opt% -e "DROP DATABASE IF EXISTS %db_name%;"
@ mysql -u%user_opt% -p%pass_opt% -e "CREATE DATABASE %db_name%;"
@ echo


@ php %pathToSymfony% doctrine:insert-sql
@ echo


@ echo  Dump's apply started (from %fileNameDump%).
@ mysql -u%user_opt% -p%pass_opt% --database=%db_name% < %fileNameDump%
@ echo  Dump has been successfully applied.
@ echo


@ php %pathToSymfony% c
@ echo


@ echo  Synchronyzing started. Look at %fileNameSynchronyzingLog% later
@ php %pathToSymfony% bionic:inc_num_sync --direction=to --delete=true --belong=true >> %fileNameSynchronyzingLog%
@ echo  Synchronyzing has been finished.
@ echo


@ echo  Migration's apply started (from %fileNameMigrations%).
@ mysql -u%user_opt% -p%pass_opt% --database=%db_name% < %fileNameMigrations%
@ echo  Migration script has been successfully applied.
@ echo


@ echo  Done!
@ echo

pause