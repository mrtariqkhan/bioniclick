# Note, that '#' is comment

outputToFile='true'
#outputToFile='false'


pathToRootDir='..';
partOfTask="$pathToRootDir/symfony phpunit:runtest "
fileNameTestLog="$pathToRootDir/log/test_results.log"

testsArray=(
    '/unit/IncomingCallsActionsTest/IncomingCallsActionsSaveDataTest.php'
    '/unit/model/doctrine/PhoneCallTable/CreatePhoneCall/*'
    '/unit/model/doctrine/PhoneCallTable/UpdatePhoneCall/*'
    '/unit/model/doctrine/VisitorAnalyticsTable/FindLastForNumberTillTime/*'
    '/unit/model/doctrine/sfTwilioAPIPlugin/TwilioIncomingPhoneNumber/*'
    '/unit/model/doctrine/sfTwilioAPIPlugin/TwilioIncomingPhoneNumber/*'
    '/unit/Utils/charts/*'
#    ''
)



echo "Testing started. "
if [ $outputToFile == 'true' ];
    then
        echo " Look at $fileNameTestLog later"
        echo "============================================================================="  >> $fileNameTestLog
    else
        echo " Look at here later"
        echo "============================================================================="
fi



arrayLength=${#testsArray[*]}
for (( i=1; i<($arrayLength+1); i++ ))
do
    testCase=${testsArray[$i-1]}

    if [ $outputToFile == 'true' ];
        then
            echo "#$i from $arrayLength: '$testCase'" >> $fileNameTestLog
            php $partOfTask $testCase >> $fileNameTestLog
            echo "" >> $fileNameTestLog
            echo "" >> $fileNameTestLog
        else
            echo "#$i from $arrayLength: '$testCase'"
            php $partOfTask $testCase
            echo ""
            echo ""
    fi

done


echo "Testing has been finished."



