
fileNameArray=(
    '../log/access.log'
    '../log/access.log'
    '../log/backend_dev.log'
    '../log/backend_test.log'
    '../log/common.log'
    '../log/error.log'
    '../log/synchronyzing.log'
    '../log/test_results.log'
    '../log/twilio.log'
    '../log/email.log'
#    ''
)

arrayLength=${#fileNameArray[*]}
for (( i=1; i<$arrayLength; i++ ))
do
    fileName=${fileNameArray[$i]}

    if [ ! -w $fileName ];
    then
        echo ">> ! File $fileName does not exist or is not writable."
    else
        echo '' > $fileName
        echo ">>   File $fileName has been cleaned."
    fi

done