# Note, that '#' is comment

# To use this sh you must define variables and call this sh:
#
# absolutePathToSymfony=''
# absolutePathToLogFile=''
# paramUrl=''
# paramFriendlyName=''
# . ./task_buy_numbers_template.sh

php $absolutePathToSymfony bionic:bn --url=$paramUrl --friendlyName=$paramFriendlyName --id=$paramId --do_use_binp=$paramDoUseBinp --do_use_voip=$paramDoUseVoip >> $absolutePathToLogFile
